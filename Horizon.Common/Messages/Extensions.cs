﻿using System.Linq;
using Horizon.Common.Driver;
using Horizon.Common.Modules;
using Horizon.Common.Modules.Fights;
using Horizon.Common.Network;
using Horizon.Helpers;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Connection;
using Horizon.Protocol.Messages.Game.Approach;
using Horizon.Protocol.Messages.Game.Basic;
using Horizon.Protocol.Messages.Game.Character.Choice;
using Horizon.Protocol.Messages.Game.Character.Creation;
using Horizon.Protocol.Messages.Game.Character.Deletion;
using Horizon.Protocol.Messages.Game.Character.Stats;
using Horizon.Protocol.Messages.Game.Chat;
using Horizon.Protocol.Messages.Game.Chat.Smiley;
using Horizon.Protocol.Messages.Game.Context;
using Horizon.Protocol.Messages.Game.Context.Fight;
using Horizon.Protocol.Messages.Game.Context.Roleplay;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Emote;
using Horizon.Protocol.Messages.Game.Inventory.Items;
using Horizon.Protocol.Types.Connection;
using Horizon.Protocol.Types.Game.Context;
using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Common.Messages
{
    public static class Extensions
    {
        public static void SendIdentificationFailedMessage(this Client e, IdentificationFailureReasonEnum reason)
        {
            e.Send(new IdentificationFailedMessage((byte)reason));
        }

        public static void SendIdentificationSuccessMessage(this Client e)
        {
            e.Send(new IdentificationSuccessMessage(e.Account.Role > GameHierarchyEnum.PLAYER, false, e.Account.Username, e.Account.Nickname, e.Account.Key, 0, e.Account.SecretQuestion, 0, 0, 0, 1));
        }

        public static void SendServersListMessage(this Client e)
        {
            e.Send(new ServersListMessage(new[] { new GameServerInformations(false, true, 36, (byte)GameServerTypeEnum.SERVER_TYPE_CLASSICAL, (byte)ServerStatusEnum.ONLINE, (byte)ServerCompletionEnum.COMPLETION_RECOMANDATED, (byte)e.Characters.Length, 5, 0) }, 0, false));
        }

        public static void SendSelectedServerDataMessage(this Client e)
        {
            e.Send(new SelectedServerDataMessage(36, "127.0.0.1", new[] { 5555 }, e.Characters.Length < 5, Tickets.Encode(e.Account.Ticket)));
        }

        public static void SendBasicTimeMessage(this Client e)
        {
            e.Send(new BasicTimeMessage(Clock.Long, Clock.Zone));
        }

        public static void SendBasicNoOperationMessage(this Client e)
        {
            e.Send(new BasicNoOperationMessage());
        }

        public static void SendServerSettingsMessage(this Client e)
        {
            e.Send(new ServerSettingsMessage(false, false, "fr", 0, (byte)GameServerTypeEnum.SERVER_TYPE_CLASSICAL, 30, 200));
        }

        public static void SendAccountCapabilitiesMessage(this Client e)
        {
            e.Send(new AccountCapabilitiesMessage(true, true, e.Account.Key, 262143, 262143, (byte)e.Account.Role, 0));
        }

        public static void SendCharacterCreationResultMessage(this Client e, CharacterCreationResultEnum result)
        {
            e.Send(new CharacterCreationResultMessage((byte)result));
        }

        public static void SendCharactersListMessage(this Client e)
        {
            e.Send(new CharactersListMessage(e.Characters.Select(x => x.Informations).ToArray(), false));
        }

        public static void SendInventoryWeightMessage(this Client e)
        {
            e.Send(new InventoryWeightMessage(0, 1000));
        }

        public static void SendTextInformationMessage(this Client e, TextInformationTypeEnum msgType, short msgId)
        {
            e.Send(new TextInformationMessage((byte)msgType, msgId, new string[0]));
        }

        public static void SendGameContextDestroyMessage(this Client e)
        {
            e.Send(new GameContextDestroyMessage());
        }

        public static void SendGameContextCreateMessage(this Client e, GameContextEnum context)
        {
            e.Send(new GameContextCreateMessage((byte)context));
        }

        public static void SendLifePointsRegenBeginMessage(this Client e)
        {
            e.Send(new LifePointsRegenBeginMessage(5));
        }

        public static void SendCurrentMapMessage(this Client e)
        {
            e.Send(new CurrentMapMessage(e.Character.Location.Map, "649ae451ca33ec53bbcbcc33becf15f4"));
        }

        public static void SendCharacterStatsListMessage(this Client e)
        {
            e.Send(new CharacterStatsListMessage(e.Character.Characteristics));
        }

        public static void SendGameMapChangeOrientationMessage(this Client e, CharacterMongoDB character)
        {
            e.Send(new GameMapChangeOrientationMessage(new ActorOrientation(character.GameRolePlayCharacterInformations.ContextualId, character.GameRolePlayCharacterInformations.Disposition.Direction)));
        }

        public static void SendGameMapMovementMessage(this Client e, GameMapMovementRequestMessage message, CharacterMongoDB character)
        {
            e.Send(new GameMapMovementMessage(message.KeyMovements, 0, character.GameRolePlayCharacterInformations.ContextualId));
        }

        public static void SendChatSmileyMessage(this Client e, ChatSmileyRequestMessage message, CharacterMongoDB character)
        {
            e.Send(new ChatSmileyMessage(character.GameRolePlayCharacterInformations.ContextualId, message.SmileyId, character.Account));
        }

        public static void SendEmotePlayMessage(this Client e, EmotePlayRequestMessage message, CharacterMongoDB character)
        {
            e.Send(new EmotePlayMessage(message.EmoteId, Clock.Long, character.GameRolePlayCharacterInformations.ContextualId, character.Account));
        }

        public static void SendChatServerMessage(this Client e, ChatClientMultiMessage message, CharacterMongoDB character)
        {
            e.Send(new ChatServerMessage(message.Channel, message.Content, Clock.Integer, "A", character.GameRolePlayCharacterInformations.ContextualId, character.GameRolePlayCharacterInformations.Name, "", character.Account));
        }

        public static void SendCharacterDeletionErrorMessage(this Client e, CharacterDeletionErrorEnum error)
        {
            e.Send(new CharacterDeletionErrorMessage((byte)error));
        }

        public static void SendCharacterCanBeCreatedResultMessage(this Client e)
        {
            e.Send(new CharacterCanBeCreatedResultMessage(e.Characters.Length < 5));
        }

        public static void SendGameRolePlayShowActorMessage(this Client e, GameRolePlayActorInformations actor)
        {
            e.Send(new GameRolePlayShowActorMessage(actor));
        }

        public static void SendGameContextRemoveElementMessage(this Client e, GameRolePlayActorInformations actor)
        {
            e.Send(new GameContextRemoveElementMessage(actor.ContextualId));
        }

        public static void SendGameFightStartingMessage(this Client e, Fight fight)
        {
            e.Send(new GameFightStartingMessage((byte)fight.FightType, fight.Key, fight.Teams[TeamEnum.TEAM_CHALLENGER].LeaderId, fight.Teams[TeamEnum.TEAM_DEFENDER].LeaderId));
        }

        public static void SendGameFightJoinMessage(this Client e, Fight fight, short timeMaxBeforeStart)
        {
            e.Send(new GameFightJoinMessage(true, false, true, false, timeMaxBeforeStart, (byte)fight.FightType));
        }

        public static void SendGameFightPlacementPossiblePositionsMessage(this Client e, Context context)
        {
            e.Send(new GameFightPlacementPossiblePositionsMessage(context.Record.Positions.PositionsForChallengers, context.Record.Positions.PositionsForDefenders, (byte)TeamEnum.TEAM_CHALLENGER));
        }

        public static void SendGameFightOptionStateUpdateMessage(this Client e, Fight fight, FightOptionsEnum option, bool state)
        {
            e.Send(new GameFightOptionStateUpdateMessage(fight.Key, (byte)TeamEnum.TEAM_CHALLENGER, (byte)option, state));
        }

        public static void SendLifePointsRegenEndMessage(this Client e)
        {
            e.Send(new LifePointsRegenEndMessage(e.Character.Characteristics.LifePoints, e.Character.Characteristics.MaxLifePoints, 0));
        }

    }
}