﻿using System;
using System.Collections.Concurrent;

namespace Horizon.Common.Memory
{
    public abstract class Storage<TKey, TValue>
    {
        internal TKey Key { get; set; }

        internal static readonly ConcurrentDictionary<TKey, TValue> Data = new ConcurrentDictionary<TKey, TValue>();

        internal readonly Random Random = new Random();
    }
}