﻿namespace Horizon.Common.Memory
{
    public abstract class StorageLockable<TKey, TValue> : Storage<TKey, TValue>
    {
        internal readonly object Locker = new object();
    }
}