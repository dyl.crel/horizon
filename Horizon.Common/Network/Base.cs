﻿using System;
using System.Net;
using System.Net.Sockets;
using Horizon.Protocol;

namespace Horizon.Common.Network
{
    public abstract class Base
    {
        public enum Event
        {
            Connect,
            Disconnect,
            Send,
            Receive
        }

        public Socket Socket { get; set; }

        public byte[] Buffer { get; set; }

        public bool Authentication =>
            ((IPEndPoint)Socket.LocalEndPoint).Port == 443;

        public bool Connected
        {
            get
            {
                if (Socket == null || !Socket.Connected)
                    return false;
                try
                {
                    if (Socket.Poll(0, SelectMode.SelectRead))
                    {
                        if (Socket.Receive(new byte[1], SocketFlags.Peek) == 0)
                            return false;
                    }

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public void Send(NetworkMessage e)
        {
            if (Connected)
            {
                using (var writer = new BigEndianWriter(e.Bytes))
                {
                    Socket.Send(writer.Data, 0, writer.Data.Length, SocketFlags.None);
                }

                Log(Event.Send, e.GetType().Name);
            }
        }

        public void Log(Event e)
        {
            if (e == Event.Connect)
            {
                Console.WriteLine("{0} Connected. {1}", Socket.RemoteEndPoint, Environment.NewLine);
            }
            else if (e == Event.Disconnect)
            {
                Console.WriteLine("{0} Disconnected. {1}", Socket.RemoteEndPoint, Environment.NewLine);
            }
        }

        public void Log(Event e, object s)
        {
            if (e == Event.Send)
            {
                Console.WriteLine("[SND] {0}. {1}", s, Environment.NewLine);
            }
            else if (e == Event.Receive)
            {
                Console.WriteLine("[RCV] {0}. {1}", s, Environment.NewLine);
            }
        }
    }
}