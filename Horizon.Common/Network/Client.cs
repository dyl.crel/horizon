﻿using Horizon.Common.Driver;
using Horizon.Protocol.Messages.Connection;
using Horizon.Protocol.Messages.Game.Approach;
using Horizon.Protocol.Messages.Handshake;

namespace Horizon.Common.Network
{
    public sealed class Client : Base
    {
        public AccountMongoDB Account { get; set; }

        public CharacterMongoDB Character { get; set; }

        public CharacterMongoDB[] Characters => CharactersMongoDB.Select(Account.Key);

        public Client Connect()
        {
            Log(Event.Connect);

            Send(new ProtocolRequired(1912, 1912));

            if (Authentication)
            {
                Send(new HelloConnectMessage("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789", new sbyte[] { 68, 9, 58, 124, 20, 90, -33, -37, 54, -111, -81, 74, -22, 1, -25, -4, -83, 108, -40, 89, 105, 86, 1, 74, -23, -1, 48, 74, 79, -73, 85, 5, 36, 12, -3, 110, -124, -11, -17, 37, -28, -94, -1, -18, -53, -47, -86, -93, -106, 60, 40, 73, 112, 6, -91, -101, -43, -37, -27, 96, -74, 41, 66, -15, -48, 0, -14, -10, 99, 62, 106, -113, -97, -45, -8, -62, 95, -4, -2, 72, 78, 4, -102, -86, -95, 22, -15, -110, -126, -98, -9, 80, 81, 40, 28, -47, 47, 46, -14, 15, -123, -89, 49, -69, -64, -55, -11, 97, -14, 107, 30, 76, -88, 38, -18, -122, 78, 3, -77, 43, -66, 97, 100, -5, 85, 119, -72, -3, -9, -72, -36, 94, -64, 24, 104, 93, -72, 80, 111, -6, -109, -6, 78, 114, 77, -86, -20, 24, 115, -118, 115, -4, 110, -68, 20, -87, 31, 95, -11, 74, -41, -15, 68, 71, 5, -103, -95, 28, 90, 108, -50, 57, -1, -2, 30, 114, -14, -59, 114, -22, 52, -39, 84, -31, 7, -47, -76, 10, 18, -71, -82, 122, -17, 116, 99, 36, 10, -3, 73, -20, -73, 90, -50, 70, -55, -26, -31, -11, -19, 108, -67, -75, 29, -5, 20, -46, -92, 37, 101, -87, 80, -72, -71, -55, 58, 21, 49, -63, -51, 94, 124, -101, 43, -53, -110, 111, -90, 53, -24, -15, 49, 36, -104, 5, 25, -56, -14, 47, -79, 74, 80, -39, 10, 2, 107, -1, 101, 93, -56, 115, -32, 45, 25, 77, 70, 27, 118, 56, -112, -103, -10, 106, 65, 81, 120, -77, 45, 19, 107, 84, -112, -110, -8, -93, 120, -63, -120, 63, -71, -11, -94, 56, -49, -117, 113, -74, -120, -47, 35, -43, -80, 4, -102, -89, -81 }));
            }
            else
            {
                Send(new HelloGameMessage());
            }

            return this;
        }

        public Client Disconnect()
        {
            Log(Event.Disconnect);

            Character?.Context.Leave(this);

            return this;
        }

        public void Dispose()
        {
            Socket.Disconnect(false);
        }
    }
}