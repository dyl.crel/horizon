﻿using System;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Data.Items;
using Horizon.Protocol.Types.Game.Data.Items.Effects;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Horizon.Common.Driver
{
    public static class Database
    {
        private static readonly IMongoDatabase Base = new MongoClient().GetDatabase("Horizon");

        public static readonly IMongoCollection<AccountMongoDB> Accounts = Base.GetCollection<AccountMongoDB>("Accounts");
        public static readonly IMongoCollection<CharacterMongoDB> Characters = Base.GetCollection<CharacterMongoDB>("Characters");
        public static readonly IMongoCollection<BreedMongoDB> Breeds = Base.GetCollection<BreedMongoDB>("Breeds");
        public static readonly IMongoCollection<ContextMongoDB> Contexts = Base.GetCollection<ContextMongoDB>("Contexts");

        public static void Register()
        {
            BsonClassMap.RegisterClassMap<SpellItem>();
            BsonClassMap.RegisterClassMap<ObjectItem>();
            BsonClassMap.RegisterClassMap<ObjectEffectInteger>();
            BsonClassMap.RegisterClassMap<GameRolePlayGroupMonsterInformations>();
        }

        public static void Insert<TDocument>(this TDocument document)
        {
            switch (document)
            {
                case AccountMongoDB e:
                    Accounts.InsertOne(e);
                    break;
                case CharacterMongoDB e:
                    Characters.InsertOne(e);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public static void Delete<TDocument>(this TDocument document)
        {
            switch (document)
            {
                case AccountMongoDB e:
                    Accounts.DeleteOne(x => x.Key == e.Key);
                    break;
                case CharacterMongoDB e:
                    Characters.DeleteOne(x => x.Key == e.Key);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public static void Push<TDocument>(this TDocument document, Action<TDocument> action = null)
        {
            action?.Invoke(document);

            switch (document)
            {
                case AccountMongoDB e:
                    Accounts.ReplaceOne(x => x.Key == e.Key, e);
                    break;
                case CharacterMongoDB e:
                    Characters.ReplaceOne(x => x.Key == e.Key, e);
                    break;
                case ContextMongoDB e:
                    Contexts.ReplaceOne(x => x.Key == e.Key, e);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}