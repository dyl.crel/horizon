﻿using System.Collections.Generic;
using Horizon.Common.Driver.Game.Action;
using Horizon.Common.Modules;
using Horizon.Common.Modules.Fights;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Types.Game.Character.Alignment;
using Horizon.Protocol.Types.Game.Character.Characteristic;
using Horizon.Protocol.Types.Game.Character.Choice;
using Horizon.Protocol.Types.Game.Character.Restriction;
using Horizon.Protocol.Types.Game.Context;
using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Data.Items;
using Horizon.Protocol.Types.Game.Interactive;
using MongoDB.Bson.Serialization.Attributes;

namespace Horizon.Common.Driver
{
    public class AccountMongoDB : Document<short>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nickname { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretAnswer { get; set; }
        public GameHierarchyEnum Role { get; set; }
        public string Ticket { get; set; }
    }

    public class CharacterMongoDB : Document<short>
    {
        public short Account { get; set; }
        public CharacterBaseInformations Informations { get; set; }
        public CharacterCharacteristicsInformations Characteristics { get; set; }
        public Location Location { get; set; }
        public List<Item> Items { get; set; }
        public List<byte> Emotes { get; set; }

        [BsonIgnore]
        public Context Context { get; set; }

        [BsonIgnore]
        public Fight Fight { get; set; }

        [BsonIgnore]
        public GameRolePlayCharacterInformations GameRolePlayCharacterInformations
        => new GameRolePlayCharacterInformations(Informations.Id, Informations.EntityLook, new EntityDispositionInformations(Location.Cell, (byte)Location.Direction), Informations.Name, new HumanInformations(new ActorRestrictionsInformations(), Informations.Sex, new HumanOption[0]), Account, new ActorAlignmentInformations(Characteristics.AlignmentInfos.AlignmentSide, Characteristics.AlignmentInfos.AlignmentValue, Characteristics.AlignmentInfos.AlignmentGrade, Characteristics.AlignmentInfos.CharacterPower));

        [BsonIgnore]
        public GameFightMinimalStatsPreparation GameFightMinimalStatsPreparation
            => new GameFightMinimalStatsPreparation
            {
                LifePoints = Characteristics.LifePoints,
                MaxLifePoints = Characteristics.MaxLifePoints,
                BaseMaxLifePoints = Characteristics.MaxLifePoints,
                PermanentDamagePercent = Characteristics.PermanentDamagePercent.Sum(),
                ShieldPoints = 0,
                ActionPoints = Characteristics.ActionPoints.Sum(),
                MaxActionPoints = Characteristics.ActionPoints.Sum(),
                MovementPoints = Characteristics.MovementPoints.Sum(),
                MaxMovementPoints = Characteristics.MovementPoints.Sum(),
                Summoner = 0,
                Summoned = false,
                NeutralElementResistPercent = Characteristics.NeutralElementResistPercent.Sum(),
                EarthElementResistPercent = Characteristics.EarthElementResistPercent.Sum(),
                WaterElementResistPercent = Characteristics.WaterElementResistPercent.Sum(),
                AirElementResistPercent = Characteristics.AirElementResistPercent.Sum(),
                FireElementResistPercent = Characteristics.FireElementResistPercent.Sum(),
                NeutralElementReduction = Characteristics.NeutralElementReduction.Sum(),
                EarthElementReduction = Characteristics.EarthElementReduction.Sum(),
                WaterElementReduction = Characteristics.WaterElementReduction.Sum(),
                AirElementReduction = Characteristics.AirElementReduction.Sum(),
                FireElementReduction = Characteristics.FireElementReduction.Sum(),
                CriticalDamageFixedResist = Characteristics.CriticalDamageReduction.Sum(),
                PushDamageFixedResist = Characteristics.PushDamageReduction.Sum(),
                PvpNeutralElementResistPercent = Characteristics.PvpNeutralElementResistPercent.Sum(),
                PvpEarthElementResistPercent = Characteristics.PvpEarthElementResistPercent.Sum(),
                PvpWaterElementResistPercent = Characteristics.PvpWaterElementResistPercent.Sum(),
                PvpAirElementResistPercent = Characteristics.PvpAirElementResistPercent.Sum(),
                PvpFireElementResistPercent = Characteristics.PvpFireElementResistPercent.Sum(),
                PvpNeutralElementReduction = Characteristics.PvpNeutralElementReduction.Sum(),
                PvpEarthElementReduction = Characteristics.PvpEarthElementReduction.Sum(),
                PvpWaterElementReduction = Characteristics.PvpWaterElementReduction.Sum(),
                PvpAirElementReduction = Characteristics.PvpAirElementReduction.Sum(),
                PvpFireElementReduction = Characteristics.PvpFireElementReduction.Sum(),
                DodgePALostProbability = Characteristics.DodgePALostProbability.Sum(),
                DodgePMLostProbability = Characteristics.DodgePMLostProbability.Sum(),
                TackleBlock = Characteristics.TackleBlock.Sum(),
                TackleEvade = Characteristics.TackleEvade.Sum(),
                FixedDamageReflection = Characteristics.Reflect.Sum(),
                InvisibilityState = (byte)GameActionFightInvisibilityStateEnum.VISIBLE,
                MeleeDamageReceivedPercent = Characteristics.MeleeDamageReceivedPercent.Sum(),
                RangedDamageReceivedPercent = Characteristics.RangedDamageReceivedPercent.Sum(),
                WeaponDamageReceivedPercent = Characteristics.WeaponDamageReceivedPercent.Sum(),
                SpellDamageReceivedPercent = Characteristics.SpellDamageReceivedPercent.Sum(),
                Initiative = Characteristics.Initiative.Sum()
            };
    }

    public class ContextMongoDB : Document<int>
    {
        public short Area { get; set; }
        public int[] Sides { get; set; }
        public GameRolePlayActorInformations[] Actors { get; set; }
        public FightStartingPositions Positions { get; set; }
        public Action[] Interactives { get; set; } 
        public StatedElement[] Stated { get; set; }
    }

    public class BreedMongoDB : Document<byte>
    {
        public Look Male { get; set; }
        public Look Female { get; set; }
    }

    public class Location
    {
        public int Map { get; set; }
        public short Cell { get; set; }
        public DirectionsEnum Direction { get; set; }
    }

    public class Look
    {
        public short Skin { get; set; }
        public int[] Colors { get; set; }
        public short Scale { get; set; }
        public Head[] Heads { get; set; }
    }

    public class Head
    {
        public int Id { get; set; }
        public short Skin { get; set; }
    }
}