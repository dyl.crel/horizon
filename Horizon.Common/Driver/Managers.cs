﻿using System.Linq;
using MongoDB.Driver;

namespace Horizon.Common.Driver
{
    public static class AccountsMongoDB
    {
        public static AccountMongoDB Select(string ticket)
        {
            return Database.Accounts.Find(x => x.Ticket == ticket).Limit(1).SingleOrDefault();
        }

        public static AccountMongoDB Select(string username, string password)
        {
            return Database.Accounts.Find(x => x.Username == username && x.Password == password).Limit(1).SingleOrDefault();
        }
    }

    public static class CharactersMongoDB
    {
        public static short Key
        {
            get
            {
                var e = Database.Characters.Find(x => x.Key > 0).ToEnumerable().Select(x => (int)x.Key).ToArray();

                if (e.Length > 0)
                {
                    return (short)Enumerable.Range(1, e.Max() + 1).Except(e).First();
                }

                return 1;
            }
        }

        public static CharacterMongoDB Select(string name)
        {
            return Database.Characters.Find(x => x.Informations.Name == name).Limit(1).SingleOrDefault();
        }

        public static CharacterMongoDB[] Select(short account)
        {
            return Database.Characters.Find(x => x.Account == account).Limit(5).ToList().ToArray();
        }
    }

    public static class ContextsMongoDB
    {
        public static ContextMongoDB Select(int key)
        {
            return Database.Contexts.Find(x => x.Key == key).Limit(1).SingleOrDefault();
        }

        public static ContextMongoDB Select(double key)
        {
            return Database.Contexts.Find(x => x.Key == (int)key).Limit(1).SingleOrDefault();
        }
    }

    public static class BreedsMongoDB
    {
        public static BreedMongoDB Select(byte key)
        {
            return Database.Breeds.Find(x => x.Key == key).Limit(1).SingleOrDefault();
        }
    }
}