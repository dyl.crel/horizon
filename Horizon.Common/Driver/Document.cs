﻿using MongoDB.Bson.Serialization.Attributes;

namespace Horizon.Common.Driver
{
    public class Document<TKey>
    {
        [BsonId]
        public TKey Key { get; set; }
    }
}