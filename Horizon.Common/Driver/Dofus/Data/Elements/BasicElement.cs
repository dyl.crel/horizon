﻿using Horizon.Protocol;
using System;

namespace Jaeger.Dofus.D2P.Data.Elements
{
    public abstract class BasicElement
    {
        protected BasicElement(Cell cell)
        {
            Cell = cell;
        }

        public static BasicElement GetElementFromType(int elementType, Cell cell)
        {
            switch (elementType)
            {
                case (int)ElementTypesEnum.Graphical:
                    return new GraphicalElement(cell);
                case (int)ElementTypesEnum.Sound:
                    return new SoundElement(cell);
                default:
                    throw new Exception($"Un élément de type inconnu {elementType} a été trouvé sur la celulle {cell.CellId}!");
            }
        }

        public Cell Cell { get; }

        public abstract void FromRaw(BigEndianReader param1, int param2);

        public abstract int ElementType { get; }
    }
}