﻿using Jaeger.Dofus.D2P.Data;
using Jaeger.Dofus.D2P.Data.Elements;

namespace DBSynchroniser.Interactives
{
    public class IdentifiableElement
    {
        public IdentifiableElement(GraphicalElement element, Map map)
        {
            Element = element;
            Map = map;
        }

        public GraphicalElement Element
        {
            get;
            set;
        }
        
        public Map Map
        {
            get;
            set;
        }

        public bool Ignore
        {
            get;
            set;
        }
    }
}