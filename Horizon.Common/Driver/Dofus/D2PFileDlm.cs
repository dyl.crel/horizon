﻿using Horizon.Protocol;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using System.Text;

namespace Jaeger.Dofus.D2P
{
    public class D2PFileDlm
    {
        private readonly object _checkLock;
        private readonly FileStream _d2PFileStream;

        private readonly Dictionary<string, int[]> _filenameDataDictionnary;
        private readonly BigEndianReader _reader;

        public D2PFileDlm(string d2PFilePath)
        {
            _d2PFileStream = new FileStream(d2PFilePath, FileMode.Open, FileAccess.Read);
            _reader = new BigEndianReader(_d2PFileStream);
            _filenameDataDictionnary = new Dictionary<string, int[]>();
            _checkLock = RuntimeHelpers.GetObjectValue(new object());
            var num = Convert.ToByte(_reader.ReadByte() + _reader.ReadByte());
            if (num != 3) return;
            _d2PFileStream.Position = _d2PFileStream.Length - 24;
            var num2 = Convert.ToInt32(_reader.ReadInt());
            _reader.ReadInt();
            var num3 = Convert.ToInt32(_reader.ReadInt());
            var num4 = Convert.ToInt32(_reader.ReadInt());
            var num1 = Convert.ToInt32(_reader.ReadInt());
            var num9 = Convert.ToInt32(_reader.ReadInt());
            _d2PFileStream.Position = num3;
            var num5 = num4;
            var i = 1;
            while (i <= num5)
            {
                var key = _reader.ReadUTF();
                var num7 = _reader.ReadInt() + num2;
                var num8 = _reader.ReadInt();
                _filenameDataDictionnary.Add(key, new[]
                {
                    num7,
                    num8
                });
                i += 1;
            }
        }

        public bool ExistsDlm(string dlmName)
        {
            return _filenameDataDictionnary.ContainsKey(dlmName);
        }

        public byte[] ReadFile(string fileName)
        {
            lock (_checkLock)
            {
                var numArray = _filenameDataDictionnary[fileName];
                _d2PFileStream.Position = numArray[0];
                return _reader.ReadBytes(numArray[1]);
            }
        }
    }
}