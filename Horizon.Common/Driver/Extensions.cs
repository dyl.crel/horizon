﻿using Horizon.Protocol.Types.Game.Character.Characteristic;

namespace Horizon.Common.Driver
{
    public static class Extensions
    {
        public static short Sum(this CharacterBaseCharacteristic e)
        {
            return (short)(e.Base + e.Additionnal + e.ObjectsAndMountBonus + e.AlignGiftBonus + e.ContextModif);
        }
    }
}