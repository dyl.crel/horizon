﻿using System;
using System.Collections.Generic;
using System.Text;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Common.Driver.Game.Action
{
    public class ActionTeleport : Action
    {
        public override short Id { get; set; }
        public override InteractiveElement Interactive { get; set; }
        public int Map { get; set; }
        public short CellId { get; set; }
        public string[] Params { get; set; }

        public ActionTeleport(short id, InteractiveElement interactive,int map,short cellid, string[] parameters)
        {
            Id = id;
            Interactive = interactive;
            Map = map;
            CellId = cellid;
            Params = parameters;
        }

        public override void Execute()
        {

        }
    }
}
