﻿using Horizon.Protocol.Types.Game.Interactive;
using System;
using System.Collections.Generic;
using System.Text;

namespace Horizon.Common.Driver.Game.Action
{
    public abstract class Action
    {
        public abstract short Id { get; set; }
        public abstract InteractiveElement Interactive { get; set; }

        public abstract void Execute();
    }
}
