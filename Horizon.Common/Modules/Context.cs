﻿using System.Collections.Generic;
using System.Linq;
using Horizon.Common.Driver;
using Horizon.Common.Memory;
using Horizon.Common.Messages;
using Horizon.Common.Network;
using Horizon.Protocol.Messages.Game.Context;
using Horizon.Protocol.Messages.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Common.Modules
{
    public class Context : StorageLockable<int, Context>
    {
        public List<Client> Clients { get; set; }
        
        public ContextMongoDB Record
            => ContextsMongoDB.Select(Key);

        public GameRolePlayActorInformations[] GameRolePlayActorInformations
            => Clients.Select(x => (GameRolePlayActorInformations)x.Character.GameRolePlayCharacterInformations).Concat(Record.Actors).ToArray();

        public void Enter(Client e)
        {
            lock (Locker)
            {
                if (Clients.Contains(e))
                {
                    e.SendBasicNoOperationMessage();
                }
                else
                {
                    Clients.ForEach(c => c.SendGameRolePlayShowActorMessage(e.Character.GameRolePlayCharacterInformations));

                    Clients.Add(e);

                    e.Send(new MapComplementaryInformationsDataMessage(Record.Area, Record.Key,
                        new HouseInformations[0], GameRolePlayActorInformations, Record.Interactives.Select(c => c.Interactive).ToArray(),
                        Record.Stated, new MapObstacle[0], new FightCommonInformations[0], false,
                        Record.Positions));
                }
            }
        }

        public void Leave(Client e)
        {
            lock (Locker)
            {
                if (Clients.Contains(e))
                {
                    Clients.Remove(e);

                    Clients.ForEach(c => c.SendGameContextRemoveElementMessage(e.Character.GameRolePlayCharacterInformations));
                }
                else
                {
                    e.SendBasicNoOperationMessage();
                }
            }
        }

        public static Context Instance(Location e)
        {
            return Data.GetOrAdd(e.Map, new Context { Key = e.Map, Clients = new List<Client>() });
        }
    }
}