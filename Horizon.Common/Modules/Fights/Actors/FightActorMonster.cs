﻿using Horizon.Protocol.Types.Game.Context;
using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Common.Modules.Fights.Actors
{
    public class FightActorMonster : FightActor
    {
        public short CreatureGenericId { get; set; }

        public byte CreatureGrade { get; set; }

        public override GameFightFighterInformations GameFightFighterInformations
            => new GameFightMonsterInformations(Key, Look, new FightEntityDispositionInformations(Cell, (byte)Direction, 0), (byte)Team, 0, Alive, Stats, new short[0], CreatureGenericId, CreatureGrade, 0);
    }
}