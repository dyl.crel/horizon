﻿using Horizon.Protocol.Enums;
using Horizon.Protocol.Types.Game.Context;
using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Common.Modules.Fights.Actors
{
    public abstract class FightActor
    {
        public double Key { get; set; }

        public EntityLook Look { get; set; }

        public short Cell { get; set; }

        public DirectionsEnum Direction { get; set; }

        public TeamEnum Team { get; set; }

        public bool Alive { get; set; }

        public GameFightMinimalStats Stats { get; set; }

        public abstract GameFightFighterInformations GameFightFighterInformations { get; }

        public IdentifiedEntityDispositionInformations IdentifiedEntityDispositionInformations
            => new IdentifiedEntityDispositionInformations(Cell, (byte)Direction, Key);
    }
}