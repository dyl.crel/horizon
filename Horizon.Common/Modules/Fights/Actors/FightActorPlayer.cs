﻿using Horizon.Common.Network;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Types.Game.Character.Alignment;
using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Context;
using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Common.Modules.Fights.Actors
{
    public class FightActorPlayer : FightActor
    {
        public Client Client { get; set; }

        public string Name { get; set; }

        public short Level { get; set; }

        public ActorAlignmentInformations AlignmentInfos { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public override GameFightFighterInformations GameFightFighterInformations
            => new GameFightCharacterInformations(Key, Look, new FightEntityDispositionInformations(Cell, (byte)Direction, 0), (byte)Team, 0, Alive, Stats, new short[0], Name, new PlayerStatus((byte)PlayerStatusEnum.PLAYER_STATUS_AVAILABLE), 0, 0, false, Level, AlignmentInfos, Breed, Sex);
    }
}