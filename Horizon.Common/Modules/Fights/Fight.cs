﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Horizon.Common.Memory;
using Horizon.Common.Messages;
using Horizon.Common.Modules.Fights.Actors;
using Horizon.Common.Network;
using Horizon.Helpers;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Game.Context;
using Horizon.Protocol.Messages.Game.Context.Fight;
using Horizon.Protocol.Messages.Game.Context.Fight.Character;
using Horizon.Protocol.Messages.Game.Idol;
using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Idol;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Common.Modules.Fights
{
    public class Fight : Storage<short, Fight>
    {
        public List<FightActor> Actors { get; set; }

        public Context Context { get; set; }

        public FightTypeEnum FightType { get; set; }

        public List<List<short>> Cells { get; set; }

        public GameRolePlayGroupMonsterInformations GameRolePlayGroupMonsterInformations { get; set; }

        public Dictionary<TeamEnum, FightTeamInformations> Teams { get; set; }

        public Dictionary<TeamEnum, FightOptionsInformations> Options { get; set; }

        public List<Client> Clients
            => Actors.OfType<FightActorPlayer>().Select(x => x.Client).ToList();

        public void Enter(Client e)
        {
            Context.Leave(e);

            InsertFightActorPlayer(e);

            e.SendGameContextDestroyMessage();
            e.SendGameContextCreateMessage(GameContextEnum.FIGHT);
            e.SendCharacterStatsListMessage();
            e.SendGameFightStartingMessage(this);
            e.SendGameFightJoinMessage(this, 450);
            e.SendGameFightPlacementPossiblePositionsMessage(Context);
            e.SendGameFightOptionStateUpdateMessage(this, FightOptionsEnum.FIGHT_OPTION_SET_CLOSED, Options[0].IsClosed);
            e.SendGameFightOptionStateUpdateMessage(this, FightOptionsEnum.FIGHT_OPTION_SET_TO_PARTY_ONLY, Options[0].IsRestrictedToPartyOnly);
            e.SendGameFightOptionStateUpdateMessage(this, FightOptionsEnum.FIGHT_OPTION_ASK_FOR_HELP, Options[0].IsAskingForHelp);
            e.SendGameFightOptionStateUpdateMessage(this, FightOptionsEnum.FIGHT_OPTION_SET_SECRET, Options[0].IsSecret);
            e.Send(new IdolFightPreparationUpdateMessage(0, new Idol[0]));
            e.SendLifePointsRegenEndMessage();

            foreach (var a in Actors)
            {
                Clients.ForEach(c =>
                {
                    c.Send(new GameEntitiesDispositionMessage(new[] { a.IdentifiedEntityDispositionInformations }));
                    c.Send(new GameFightShowFighterMessage(a.GameFightFighterInformations));
                    c.Send(new GameFightUpdateTeamMessage(Key, Teams[a.Team]));
                });
            }
        }

        private void InsertFightActorPlayer(Client e)
        {
            Actors.Add(new FightActorPlayer
            {
                Key = e.Character.Informations.Id,
                Look = e.Character.Informations.EntityLook,
                Cell = RandomFightCell(TeamEnum.TEAM_CHALLENGER),
                Direction = DirectionsEnum.DIRECTION_NORTH,
                Team = TeamEnum.TEAM_CHALLENGER,
                Alive = true,
                Stats = e.Character.GameFightMinimalStatsPreparation,
                Client = e,
                Name = e.Character.Informations.Name,
                Level = e.Character.Informations.Level,
                AlignmentInfos = e.Character.Characteristics.AlignmentInfos,
                Breed = e.Character.Informations.Breed,
                Sex = e.Character.Informations.Sex
            });

            var T = new FightTeamMemberInformations[Teams[TeamEnum.TEAM_CHALLENGER].TeamMembers.Length + 1];
            Array.Copy(Teams[TeamEnum.TEAM_CHALLENGER].TeamMembers, T, Teams[TeamEnum.TEAM_CHALLENGER].TeamMembers.Length);

            T[T.Length - 1] = new FightTeamMemberCharacterInformations(e.Character.Informations.Id, e.Character.Informations.Name, e.Character.Informations.Level);
        }

        private void InsertFightActorMonster(double key, EntityLook look, short creatureGenericId, byte creatureGrade)
        {
            Actors.Add(new FightActorMonster
            {
                Key = key,
                Look = look,
                Cell = RandomFightCell(TeamEnum.TEAM_DEFENDER),
                Direction = DirectionsEnum.DIRECTION_SOUTH,
                Team = TeamEnum.TEAM_DEFENDER,
                Alive = true,
                Stats = new GameFightMinimalStatsPreparation(),
                CreatureGenericId = creatureGenericId,
                CreatureGrade = creatureGrade
            });

            var T = new FightTeamMemberInformations[Teams[TeamEnum.TEAM_DEFENDER].TeamMembers.Length + 1];
            Array.Copy(Teams[TeamEnum.TEAM_DEFENDER].TeamMembers, T, Teams[TeamEnum.TEAM_DEFENDER].TeamMembers.Length);

            T[T.Length - 1] = new FightTeamMemberMonsterInformations(key, creatureGenericId, creatureGrade);
        }

        private short RandomFightCell(TeamEnum team)
        {
            if (team == TeamEnum.TEAM_CHALLENGER)
            {
                var e = Cells[0][(short)Random.Next(0, Cells[0].Count + 1)];

                Cells[0].Remove(e);

                return e;
            }

            if (team == TeamEnum.TEAM_DEFENDER)
            {
                var e = Cells[1][(short)Random.Next(0, Cells[0].Count + 1)];

                Cells[1].Remove(e);

                return e;
            }

            throw new NotSupportedException();
        }

        public static Fight Instance(Client e, GameRolePlayGroupMonsterInformations informations)
        {
            var fight = new Fight
            {
                Key = Data.Provide(),
                Actors = new List<FightActor>(),
                Context = e.Character.Context,
                FightType = FightTypeEnum.FIGHT_TYPE_PvM,
                Cells = new List<List<short>>(2)
                {
                    e.Character.Context.Record.Positions.PositionsForChallengers.ToList(),
                    e.Character.Context.Record.Positions.PositionsForDefenders.ToList(),
                },
                GameRolePlayGroupMonsterInformations = informations,
                Teams = new Dictionary<TeamEnum, FightTeamInformations>(2)
                {
                    {TeamEnum.TEAM_CHALLENGER, new FightTeamInformations((byte)TeamEnum.TEAM_CHALLENGER, e.Character.Informations.Id, (sbyte)AlignmentSideEnum.ALIGNMENT_WITHOUT, (byte)TeamTypeEnum.TEAM_TYPE_PLAYER, 0, new FightTeamMemberInformations[0])},
                    {TeamEnum.TEAM_DEFENDER, new FightTeamInformations((byte)TeamEnum.TEAM_DEFENDER, informations.ContextualId, (sbyte)AlignmentSideEnum.ALIGNMENT_WITHOUT, (byte)TeamTypeEnum.TEAM_TYPE_MONSTER, 0, new FightTeamMemberInformations[0])}
                },
                Options = new Dictionary<TeamEnum, FightOptionsInformations>(2)
                {
                    {TeamEnum.TEAM_CHALLENGER, new FightOptionsInformations()},
                    {TeamEnum.TEAM_CHALLENGER, new FightOptionsInformations()}
            }
            };

            var index = 0;

            fight.InsertFightActorMonster(--index, informations.Look, (short)informations.StaticInfos.MainCreatureLightInfos.GenericId, informations.StaticInfos.MainCreatureLightInfos.Grade);

            foreach (var underling in informations.StaticInfos.Underlings)
            {
                fight.InsertFightActorMonster(--index, underling.Look, (short)underling.GenericId, underling.Grade);
            }

            if (Data.TryAdd(fight.Key, fight))
            {
                return fight;
            }

            throw new InvalidDataException();
        }
    }
}