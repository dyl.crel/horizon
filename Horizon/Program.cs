﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBSynchroniser.Interactives;
using Horizon.Common.Driver;
using Horizon.Network;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Types.Game.Context;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Interactive;
using Horizon.Protocol.Types.Game.Look;
using Jaeger.Dofus.D2P;
using Jaeger.Dofus.D2P.Data;
using Jaeger.Dofus.D2P.Data.Elements;
using MongoDB.Driver;

namespace Horizon
{
    internal sealed class Program
    {
        private static void Main()
        {
            Console.Title = "Horizon";

            #region Config DB
            //var acc = new AccountMongoDB
            //{
            //    Key = 1,
            //    Username = "Solace",
            //    Password = "revolution6",
            //    Nickname = "Solace",
            //    SecretQuestion = "Dummy?",
            //    SecretAnswer = "Dummy!",
            //    Role = GameHierarchyEnum.ADMIN
            //};
            //var identifiableElementsByMap = new Dictionary<int, List<IdentifiableElement>>();
            //List<string> programStatic = new List<string>();
            //List<Map> map = new List<Map>();

            //var contexts = Database.Contexts.Find(FilterDefinition<ContextMongoDB>.Empty).ToList();
            //D2pManager.Setup(@"C:\Users\dylan.crelot\AppData\Local\Ankama\zaap\dofus\content\maps");

            //foreach (var context in contexts)
            //{
            //    programStatic.Add(context.Key.ToString());
            //}
            //foreach (var mapId in programStatic)
            //{
            //    D2pManager.FromId(double.Parse(mapId));
            //}
            //map = D2pManager._map;
            //foreach (var item in map)
            //{
            //    if (item != null)
            //    {
            //        var elements = (from layer in item.Layers
            //                        from cell in layer.Cells
            //                        from element in cell.Elements.OfType<GraphicalElement>()
            //                        where element.Identifier != 0
            //                        select new IdentifiableElement(element, item)).ToList();
            //        identifiableElementsByMap.Add(item.Id, elements);
            //    }
            //}
            //var identificableElements = identifiableElementsByMap.Values.SelectMany(x => x).GroupBy(x => x.Map.Id).ToDictionary(x => x.Key, x => x.ToList());
            //foreach (var keyPair in identificableElements)
            //{
            //    List<InteractiveElement> element = new List<InteractiveElement>();
            //    List<StatedElement> stated = new List<StatedElement>();
            //    foreach (var item in keyPair.Value)
            //    {
            //        element.Add(new InteractiveElement((int)item.Element.Identifier, 314, new InteractiveElementSkill[] { new InteractiveElementSkill(184, 312) }, new InteractiveElementSkill[0], true));
            //        stated.Add(new StatedElement((int)item.Element.Identifier, (short)item.Element.Cell.CellId, 0, true));
            //    }
            //    foreach (var context in contexts.Where(c => c.Key == keyPair.Key))
            //    {
            //        context.Push(x =>
            //        {
            //            x.Interactives = element.ToArray();
            //            //x.Stated = stated.ToArray();
            //        });
            //    }
            //}

            ////acc.Insert();

            //var contexts = Database.Contexts.Find(FilterDefinition<ContextMongoDB>.Empty).ToList();

            //foreach (var context in contexts)
            //{
            //    context.Push(x =>
            //    {
            //        x.Positions = new Protocol.Types.Game.Context.Fight.FightStartingPositions(new short[0], new short[0]);
            //        x.Actors = new Protocol.Types.Game.Context.Roleplay.GameRolePlayActorInformations[0];
            //    }
            //    );
            //}

            //var context = ContextsMongoDB.Select(160957952);

            //var actors = new GameRolePlayActorInformations[]
            //{
            //    new GameRolePlayGroupMonsterInformations(-20000,
            //        new EntityLook(3254, new short[0], new int[0], new short[0], new SubEntity[0]),
            //        new EntityDispositionInformations(145, 1), false, false, false, 0, 0, -1,
            //        (sbyte) AlignmentSideEnum.ALIGNMENT_WITHOUT,
            //        new GroupMonsterStaticInformations(new MonsterInGroupLightInformations(3271, 3, 3),
            //            new[] {new MonsterInGroupInformations(3272, 3, 3, new EntityLook(3255, new short[0], new int[0], new short[0], new SubEntity[0])) }))
            //};

            //context.Push(x => x.Actors = actors);
            #endregion

            Messages.Setup();

            Database.Register();

            Listeners.Start();

            Console.ReadKey();
        }
    }
}