﻿using Horizon.Common.Network;
using Horizon.Protocol.Messages.Common.Basic;

namespace Horizon.Handlers
{
    public static class BasicHandler
    {
        public static void BasicPingMessage(Client e, BasicPingMessage message)
        {
            e.Send(new BasicPongMessage(message.Quiet));
        }
    }
}