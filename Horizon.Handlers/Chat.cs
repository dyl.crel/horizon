﻿using Horizon.Common.Messages;
using Horizon.Common.Network;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Game.Chat;
using Horizon.Protocol.Messages.Game.Chat.Smiley;

namespace Horizon.Handlers
{
    public static class ChatHandler
    {
        public static void ChatSmileyRequestMessage(Client e, ChatSmileyRequestMessage message)
        {
            e.Character.Context.Clients.ForEach(c => c.SendChatSmileyMessage(message, e.Character));
        }

        public static void ChatClientMultiMessage(Client e, ChatClientMultiMessage message)
        {
            if (message.Channel == (byte)ChatChannelsMultiEnum.CHANNEL_GLOBAL)
            {
                e.Character.Context.Clients.ForEach(c => c.SendChatServerMessage(message, e.Character));
            }
        }
    }
}