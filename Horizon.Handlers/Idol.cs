﻿using Horizon.Common.Network;
using Horizon.Protocol.Messages.Game.Idol;
using Horizon.Protocol.Types.Game.Idol;

namespace Horizon.Handlers
{
    public static class IdolHandler
    {
        public static void IdolPartyRegisterRequestMessage(Client e, IdolPartyRegisterRequestMessage message)
        {
            e.Send(new IdolListMessage(new short[0], new short[0], new PartyIdol[0]));
        }
    }
}