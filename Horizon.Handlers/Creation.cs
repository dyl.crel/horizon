﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Horizon.Common.Driver;
using Horizon.Common.Messages;
using Horizon.Common.Network;
using Horizon.Helpers;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Game.Character.Creation;
using Horizon.Protocol.Messages.Game.Character.Deletion;
using Horizon.Protocol.Types.Game.Character.Alignment;
using Horizon.Protocol.Types.Game.Character.Characteristic;
using Horizon.Protocol.Types.Game.Character.Choice;
using Horizon.Protocol.Types.Game.Data.Items;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Handlers
{
    public static class CreationHandler
    {
        public static void CharacterCreationRequestMessage(Client e, CharacterCreationRequestMessage message)
        {
            if (e.Characters.Length == 5)
            {
                e.SendCharacterCreationResultMessage(CharacterCreationResultEnum.ERR_TOO_MANY_CHARACTERS);
                return;
            }

            if (!Regex.IsMatch(message.Name, "^[A-Za-z-.]{2,20}$"))
            {
                e.SendCharacterCreationResultMessage(CharacterCreationResultEnum.ERR_INVALID_NAME);
                return;
            }

            if (CharactersMongoDB.Select(message.Name) != null)
            {
                e.SendCharacterCreationResultMessage(CharacterCreationResultEnum.ERR_NAME_ALREADY_EXISTS);
                return;
            }

            var breed = BreedsMongoDB.Select(message.Breed);

            if (breed == null)
            {
                e.SendCharacterCreationResultMessage(CharacterCreationResultEnum.ERR_NO_REASON);
                return;
            }

            Head head;

            if (message.Sex)
            {
                head = breed.Female.Heads.FirstOrDefault(x => x.Id == message.CosmeticId);
            }
            else
            {
                head = breed.Male.Heads.FirstOrDefault(x => x.Id == message.CosmeticId);
            }

            if (head == null)
            {
                e.SendCharacterCreationResultMessage(CharacterCreationResultEnum.ERR_NO_REASON);
                return;
            }

            for (var I = 0; I < message.Colors.Length; I++)
            {
                if (message.Colors[I] == -1)
                {
                    message.Colors[I] = message.Sex ? breed.Female.Colors[I] : breed.Male.Colors[I];
                }

                message.Colors[I] = (I + 1 & 255) << 24 | message.Colors[I] & 16777215;
            }

            var characterKey = CharactersMongoDB.Key;

            e.Character = new CharacterMongoDB
            {
                Key = characterKey,
                Account = e.Account.Key,
                Informations = new CharacterBaseInformations
                {
                    Id = characterKey,
                    Name = message.Name,
                    Level = 1,
                    EntityLook = new EntityLook
                    {
                        BonesId = 1,
                        Skins = new[] { message.Sex ? breed.Female.Skin : breed.Male.Skin, head.Skin },
                        IndexedColors = message.Colors,
                        Scales = new[] { message.Sex ? breed.Female.Scale : breed.Male.Scale },
                        Subentities = new SubEntity[0]
                    },
                    Breed = message.Breed,
                    Sex = message.Sex
                },
                Characteristics = new CharacterCharacteristicsInformations
                {
                    Experience = 0,
                    ExperienceLevelFloor = 0,
                    ExperienceNextLevelFloor = 110,
                    ExperienceBonusLimit = 0,
                    Kamas = 0,
                    StatsPoints = 0,
                    AdditionnalPoints = 0,
                    SpellsPoints = 0,
                    AlignmentInfos = new ActorExtendedAlignmentInformations(0, 0, 0, characterKey + 1, 0, 0, 500, (byte)AggressableStatusEnum.NON_AGGRESSABLE),
                    LifePoints = 55,
                    MaxLifePoints = 55,
                    EnergyPoints = 10000,
                    MaxEnergyPoints = 10000,
                    ActionPointsCurrent = 6,
                    MovementPointsCurrent = 3,
                    Initiative = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Prospecting = new CharacterBaseCharacteristic(100, 0, 0, 0, 0),
                    ActionPoints = new CharacterBaseCharacteristic(6, 0, 0, 0, 0),
                    MovementPoints = new CharacterBaseCharacteristic(3, 0, 0, 0, 0),
                    Strength = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Vitality = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Wisdom = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Chance = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Agility = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Intelligence = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    Range = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    SummonableCreaturesBoost = new CharacterBaseCharacteristic(1, 0, 0, 0, 0),
                    Reflect = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    CriticalHit = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    CriticalHitWeapon = 5,
                    CriticalMiss = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    HealBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    AllDamagesBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    WeaponDamagesBonusPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    DamagesBonusPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    TrapBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    TrapBonusPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    GlyphBonusPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    RuneBonusPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PermanentDamagePercent = new CharacterBaseCharacteristic(10, 0, 0, 0, 0),
                    TackleBlock = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    TackleEvade = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PAAttack = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PMAttack = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PushDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    CriticalDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    NeutralDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    EarthDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    WaterDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    AirDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    FireDamageBonus = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    DodgePALostProbability = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    DodgePMLostProbability = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    NeutralElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    EarthElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    WaterElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    AirElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    FireElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    NeutralElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    EarthElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    WaterElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    AirElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    FireElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PushDamageReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    CriticalDamageReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpNeutralElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpEarthElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpWaterElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpAirElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpFireElementResistPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpNeutralElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpEarthElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpWaterElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpAirElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    PvpFireElementReduction = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    MeleeDamageDonePercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    MeleeDamageReceivedPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    RangedDamageDonePercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    RangedDamageReceivedPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    WeaponDamageDonePercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    WeaponDamageReceivedPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    SpellDamageDonePercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    SpellDamageReceivedPercent = new CharacterBaseCharacteristic(0, 0, 0, 0, 0),
                    SpellModifications = new CharacterSpellModification[0],
                    ProbationTime = 0
                },
                Location = new Location
                {
                    Map = 154010883,
                    Cell = 370,
                    Direction = DirectionsEnum.DIRECTION_SOUTH_EAST
                },
                Items = new List<Item>(),
                Emotes = new List<byte> { 1 }
            };

            e.Character.Insert();

            e.SendCharacterCreationResultMessage(CharacterCreationResultEnum.OK);
            e.SendCharactersListMessage();
        }

        public static void CharacterDeletionRequestMessage(Client e, CharacterDeletionRequestMessage message)
        {
            var character = e.Characters.FirstOrDefault(x => x.Key == message.CharacterId);

            if (character == null)
            {
                e.SendCharacterDeletionErrorMessage(CharacterDeletionErrorEnum.DEL_ERR_NO_REASON);
                return;
            }

            if (character.Informations.Level > 19)
            {
                if ((character.Key + "~" + e.Account.SecretAnswer).ToHash() != message.SecretAnswerHash)
                {
                    e.SendCharacterDeletionErrorMessage(CharacterDeletionErrorEnum.DEL_ERR_BAD_SECRET_ANSWER);
                    return;
                }
            }

            character.Delete();
            
            e.SendCharactersListMessage();
        }

        public static void CharacterCanBeCreatedRequestMessage(Client e, CharacterCanBeCreatedRequestMessage message)
        {
            e.SendCharacterCanBeCreatedResultMessage();
        }
    }
}