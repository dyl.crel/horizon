﻿using Horizon.Common.Network;
using Horizon.Protocol.Messages.Game.Friend;
using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Handlers
{
    public static class FriendHandler
    {
        public static void FriendsGetListMessage(Client e, FriendsGetListMessage message)
        {
            e.Send(new FriendsListMessage(new FriendInformations[0]));
        }

        public static void IgnoredGetListMessage(Client e, IgnoredGetListMessage message)
        {
            e.Send(new IgnoredListMessage(new IgnoredInformations[0]));
        }
    }
}