﻿using Horizon.Common.Driver;
using Horizon.Common.Messages;
using Horizon.Common.Network;
using Horizon.Helpers;
using Horizon.Protocol.Messages.Game.Approach;
using Horizon.Protocol.Messages.Secure;
using Horizon.Protocol.Types.Game.Approach;

namespace Horizon.Handlers
{
    public static class ApproachHandler
    {
        public static void AuthenticationTicketMessage(Client e, AuthenticationTicketMessage message)
        {
            e.Account = AccountsMongoDB.Select(Tickets.Decode(message.Ticket));

            if (e.Account == null)
            {
                e.Send(new AuthenticationTicketRefusedMessage());
            }
            else
            {
                e.Send(new AuthenticationTicketAcceptedMessage());
                e.SendBasicTimeMessage();
                e.SendServerSettingsMessage();
                e.Send(new ServerOptionalFeaturesMessage(new byte[] { 1, 3, 7, 12, 23 }));
                e.Send(new ServerSessionConstantsMessage(new ServerSessionConstant[]
                {
                    new ServerSessionConstantInteger(1, 3900000), new ServerSessionConstantLong(2, 7200000),
                    new ServerSessionConstantInteger(3, 30), new ServerSessionConstantLong(4, 86400000),
                    new ServerSessionConstantLong(5, 60000), new ServerSessionConstantInteger(6, 100),
                    new ServerSessionConstantLong(7, 2000)
                }));
                e.SendAccountCapabilitiesMessage();
                e.Send(new TrustStatusMessage(true, false));
            }
        }

        public static void ReloginTokenRequestMessage(Client e, ReloginTokenRequestMessage message)
        {
            e.Send(new ReloginTokenStatusMessage(false, new byte[0]));
        }
    }
}