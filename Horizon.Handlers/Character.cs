﻿using System.Linq;
using Horizon.Common.Messages;
using Horizon.Common.Network;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Game.Achievement;
using Horizon.Protocol.Messages.Game.Almanach;
using Horizon.Protocol.Messages.Game.Character.Choice;
using Horizon.Protocol.Messages.Game.Character.Stats;
using Horizon.Protocol.Messages.Game.Chat.Community;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Emote;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Houses;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Job;
using Horizon.Protocol.Messages.Game.Dare;
using Horizon.Protocol.Messages.Game.Friend;
using Horizon.Protocol.Messages.Game.Idol;
using Horizon.Protocol.Messages.Game.Initialization;
using Horizon.Protocol.Messages.Game.Inventory.Items;
using Horizon.Protocol.Messages.Game.Inventory.Spells;
using Horizon.Protocol.Messages.Game.Prism;
using Horizon.Protocol.Messages.Game.Pvp;
using Horizon.Protocol.Messages.Game.Startup;
using Horizon.Protocol.Messages.Web.Ankabox;
using Horizon.Protocol.Types.Game.Achievement;
using Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena;
using Horizon.Protocol.Types.Game.Context.Roleplay.Job;
using Horizon.Protocol.Types.Game.Dare;
using Horizon.Protocol.Types.Game.Data.Items;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Idol;
using Horizon.Protocol.Types.Game.Interactive.Skill;
using Horizon.Protocol.Types.Game.Prism;
using Horizon.Protocol.Types.Game.Startup;

namespace Horizon.Handlers
{
    public static class CharacterHandler
    {   
        public static void CharactersListRequestMessage(Client e, CharactersListRequestMessage message)
        {
            e.SendCharactersListMessage();
        }

        public static void CharacterFirstSelectionMessage(Client e, CharacterFirstSelectionMessage message)
        {
            CharacterSelectionMessage(e, message);
        }

        public static void CharacterSelectionMessage(Client e, CharacterSelectionMessage message)
        {
            if (e.Character == null)
            {
                e.Character = e.Characters.FirstOrDefault(x => x.Key == message.Id);
            }

            if (e.Character == null)
            {
                e.Send(new CharacterSelectedErrorMessage());
            }
            else
            {
                var Descriptions = new JobDescription[]
                                {
                    new JobDescription(36, new SkillActionDescription[] {new SkillActionDescriptionCollect(124, 30, 1, 7), new SkillActionDescriptionCollect(301, 30, 1, 1), new SkillActionDescriptionCraft(135, 100)}),
                    new JobDescription(11, new SkillActionDescription[] { new SkillActionDescriptionCraft(20, 100) }),
                    new JobDescription(65, new SkillActionDescription[] { new SkillActionDescriptionCraft(171, 100) }),
                    new JobDescription(15, new SkillActionDescription[] { new SkillActionDescriptionCraft(13, 100) }),
                    new JobDescription(26, new SkillActionDescription[] { new SkillActionDescriptionCollect(300, 30, 1, 1), new SkillActionDescriptionCollect(68, 30, 1, 7), new SkillActionDescriptionCraft(23, 100) }),
                    new JobDescription(62, new SkillActionDescription[] { new SkillActionDescriptionCraft(164, 5) }),
                    new JobDescription(48, new SkillActionDescription[] { new SkillActionDescriptionCraft(118, 5) }),
                    new JobDescription(41, new SkillActionDescription[] { new SkillActionDescriptionCraft(134, 100) }),
                    new JobDescription(16, new SkillActionDescription[] { new SkillActionDescriptionCraft(12, 100) }),
                    new JobDescription(27, new SkillActionDescription[] { new SkillActionDescriptionCraft(63, 100) }),
                    new JobDescription(2, new SkillActionDescription[] { new SkillActionDescriptionCollect(6, 30, 1, 7), new SkillActionDescriptionCraft(101, 100), new SkillActionDescriptionCollect(299, 30, 1, 1) }),
                    new JobDescription(63, new SkillActionDescription[] { new SkillActionDescriptionCraft(169, 5), new SkillActionDescriptionCraft(168, 5) }),
                    new JobDescription(13, new SkillActionDescription[] { new SkillActionDescriptionCraft(15, 100) }),
                    new JobDescription(74, new SkillActionDescription[] { new SkillActionDescriptionCraft(351, 5) }),
                    new JobDescription(24, new SkillActionDescription[] { new SkillActionDescriptionCollect(24, 30, 1, 7), new SkillActionDescriptionCraft(48, 100), new SkillActionDescriptionCollect(298, 30, 1, 1), new SkillActionDescriptionCraft(32, 100) }),
                    new JobDescription(60, new SkillActionDescription[] { new SkillActionDescriptionCraft(201, 100), new SkillActionDescriptionCraft(297, 100), new SkillActionDescriptionCraft(156, 100) }),
                    new JobDescription(28, new SkillActionDescription[] { new SkillActionDescriptionCraft(109, 100), new SkillActionDescriptionCraft(27, 100), new SkillActionDescriptionCollect(45, 30, 1, 7), new SkillActionDescriptionCraft(122, 100), new SkillActionDescriptionCollect(296, 30, 1, 1), new SkillActionDescriptionCraft(47, 100) }),
                    new JobDescription(44, new SkillActionDescription[] { new SkillActionDescriptionCraft(113, 5) }),
                    new JobDescription(64, new SkillActionDescription[] { new SkillActionDescriptionCraft(166, 5) })
                };

                var Experiences = new JobExperience[]
                {
                    new JobExperience(36, 1, 0, 0, 20),
                    new JobExperience(11, 1, 0, 0, 20),
                    new JobExperience(65, 1, 0, 0, 20),
                    new JobExperience(15, 1, 0, 0, 20),
                    new JobExperience(26, 1, 0, 0, 20),
                    new JobExperience(62, 1, 0, 0, 20),
                    new JobExperience(48, 1, 0, 0, 20),
                    new JobExperience(41, 1, 0, 0, 20),
                    new JobExperience(16, 1, 0, 0, 20),
                    new JobExperience(27, 1, 0, 0, 20),
                    new JobExperience(2, 1, 0, 0, 20),
                    new JobExperience(63, 1, 0, 0, 20),
                    new JobExperience(13, 1, 0, 0, 20),
                    new JobExperience(74, 1, 0, 0, 20),
                    new JobExperience(24, 1, 0, 0, 20),
                    new JobExperience(60, 1, 0, 0, 20),
                    new JobExperience(28, 1, 0, 0, 20),
                    new JobExperience(44, 1, 0, 0, 20),
                    new JobExperience(64, 1, 0, 0, 20)
                };

                var Settings = new JobCrafterDirectorySettings[]
                    {
                new JobCrafterDirectorySettings(36, 0, true),
                    new JobCrafterDirectorySettings(11, 0, true),
                    new JobCrafterDirectorySettings(65, 0, true),
                    new JobCrafterDirectorySettings(15, 0, true),
                    new JobCrafterDirectorySettings(26, 0, true),
                    new JobCrafterDirectorySettings(62, 0, true),
                    new JobCrafterDirectorySettings(48, 0, true),
                    new JobCrafterDirectorySettings(41, 0, true),
                    new JobCrafterDirectorySettings(16, 0, true),
                    new JobCrafterDirectorySettings(27, 0, true),
                    new JobCrafterDirectorySettings(2, 0, true),
                    new JobCrafterDirectorySettings(63, 0, true),
                    new JobCrafterDirectorySettings(13, 0, true),
                    new JobCrafterDirectorySettings(74, 0, true),
                    new JobCrafterDirectorySettings(24, 0, true),
                    new JobCrafterDirectorySettings(60, 0, true),
                    new JobCrafterDirectorySettings(28, 0, true),
                    new JobCrafterDirectorySettings(44, 0, true),
                    new JobCrafterDirectorySettings(64, 0, true)
                    };
                e.Send(new CharacterSelectedSuccessMessage(e.Character.Informations, true));
                e.Send(new SpellListMessage(true, new SpellItem[0]));
                e.Send(new InventoryContentMessage(new ObjectItem[0], e.Character.Characteristics.Kamas));
                e.Send(new RoomAvailableUpdateMessage(1));
                e.Send(new HavenBagPackListMessage(new byte[] { 1 }));
                e.Send(new EmoteListMessage(e.Character.Emotes.ToArray()));
                e.Send(new JobDescriptionMessage(Descriptions));
                e.Send(new JobExperienceMultiUpdateMessage(Experiences));
                e.Send(new JobCrafterDirectorySettingsMessage(Settings));
                e.Send(new AlignmentRankUpdateMessage(0, false));
                e.Send(new DareCreatedListMessage(new DareInformations[0], new DareVersatileInformations[0]));
                e.Send(new DareSubscribedListMessage(new DareInformations[0], new DareVersatileInformations[0]));
                e.Send(new DareWonListMessage(new double[0]));
                e.Send(new DareRewardsListMessage(new DareReward[0]));
                e.Send(new PrismsListMessage(new PrismSubareaEmptyInfo[0]));
                e.Send(new ChatCommunityChannelCommunityMessage(0));
                e.SendInventoryWeightMessage();
                e.Send(new FriendWarnOnConnectionStateMessage(true));
                e.Send(new FriendWarnOnLevelGainStateMessage(true));
                e.Send(new FriendGuildWarnOnAchievementCompleteStateMessage(true));
                e.Send(new WarnOnPermaDeathStateMessage(true));
                e.Send(new FriendStatusShareStateMessage(false));
                e.Send(new GuildMemberWarnOnConnectionStateMessage(false));
                e.SendTextInformationMessage(TextInformationTypeEnum.TEXT_INFORMATION_ERROR, 89);
                e.Send(new OnConnectionEventMessage(2));
                e.Send(new SpouseStatusMessage(false));
                e.Send(new AchievementListMessage(new AchievementAchieved[0]));
                e.Send(new GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage(new ArenaRankInfos(), new ArenaRankInfos(), new ArenaRankInfos()));
                e.Send(new CharacterCapabilitiesMessage(4095));
                e.Send(new AlmanachCalendarDateMessage(30));
                e.Send(new IdolListMessage(new short[0], new short[0], new PartyIdol[0]));
                e.Send(new CharacterExperienceGainMessage(0, 0, 0, 0));
                e.Send(new MailStatusMessage(0, 0));
                e.Send(new AccountHouseMessage(new AccountHouseInformations[0]));
                e.Send(new CharacterLoadingCompleteMessage());
                e.Send(new StartupActionsListMessage(new StartupActionAddObject[0]));
            }
        }
    }
}