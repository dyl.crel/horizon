﻿using Horizon.Common.Driver;
using Horizon.Common.Messages;
using Horizon.Common.Network;
using Horizon.Helpers;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Connection;

namespace Horizon.Handlers
{
    public static class ConnectionHandler
    {
        public static void IdentificationMessage(Client e, IdentificationMessage message)
        {
            var credentials = message.Lang.Split('@');

            e.Account = AccountsMongoDB.Select(credentials[0], credentials[1]);

            if (e.Account == null)
            {
                e.SendIdentificationFailedMessage(IdentificationFailureReasonEnum.WRONG_CREDENTIALS);
            }
            else
            {
                e.Account.Push(x => x.Ticket = Tickets.Random());

                e.SendIdentificationSuccessMessage();
                e.SendServersListMessage();
            }
        }

        public static void ServerSelectionMessage(Client e, ServerSelectionMessage message)
        {
            e.SendSelectedServerDataMessage();
            e.Dispose();
        }
    }
}