﻿using System.Linq;
using Horizon.Common.Driver;
using Horizon.Common.Messages;
using Horizon.Common.Modules;
using Horizon.Common.Modules.Fights;
using Horizon.Common.Network;
using Horizon.Protocol.Enums;
using Horizon.Protocol.Messages.Game.Context;
using Horizon.Protocol.Messages.Game.Context.Roleplay;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Emote;
using Horizon.Protocol.Messages.Game.Context.Roleplay.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Handlers
{
    public static class ContextHandler
    {
        public static void GameContextCreateRequestMessage(Client e, GameContextCreateRequestMessage message)
        {
            e.SendGameContextDestroyMessage();
            e.SendGameContextCreateMessage(GameContextEnum.ROLE_PLAY);
            e.SendLifePointsRegenBeginMessage();
            e.SendCurrentMapMessage();
            e.SendBasicTimeMessage();
            e.SendCharacterStatsListMessage();
        }

        public static void MapInformationsRequestMessage(Client e, MapInformationsRequestMessage message)
        {
            e.Character.Context = Context.Instance(e.Character.Location);
            e.Character.Context.Enter(e);
        }

        public static void GameMapMovementRequestMessage(Client e, GameMapMovementRequestMessage message)
        {
            e.Character.Push(x =>
            {
                x.Location.Cell = (short)(message.KeyMovements.Last() & 4095);
                x.Location.Direction = (DirectionsEnum)(message.KeyMovements.Last() >> 12);

                x.Context.Clients.ForEach(c => c.SendGameMapMovementMessage(message, e.Character));
            });
        }

        public static void GameMapMovementConfirmMessage(Client e, GameMapMovementConfirmMessage message)
        {
            e.SendBasicNoOperationMessage();
        }

        public static void ChangeMapMessage(Client e, ChangeMapMessage message)
        {
            var destination = ContextsMongoDB.Select(message.MapId);

            if (destination == null)
            {
                e.SendBasicNoOperationMessage();
            }
            else
            {
                e.Character.Context.Leave(e);

                if (e.Character.Context.Record.Sides[0] == destination.Key)

                    e.Character.Push(x =>
                    {
                        x.Location.Map = destination.Key;
                        x.Location.Cell += 532;
                    });

                else if (e.Character.Context.Record.Sides[1] == destination.Key)

                    e.Character.Push(x =>
                    {
                        x.Location.Map = destination.Key;
                        x.Location.Cell -= 13;
                    });

                else if (e.Character.Context.Record.Sides[2] == destination.Key)

                    e.Character.Push(x =>
                    {
                        x.Location.Map = destination.Key;
                        x.Location.Cell -= 532;
                    });

                else if (e.Character.Context.Record.Sides[3] == destination.Key)

                    e.Character.Push(x =>
                    {
                        x.Location.Map = destination.Key;
                        x.Location.Cell += 13;
                    });

                e.SendCurrentMapMessage();
                e.SendBasicTimeMessage();
            }
        }

        public static void GameMapChangeOrientationRequestMessage(Client e, GameMapChangeOrientationRequestMessage message)
        {
            e.Character.Push(x =>
            {
                x.Location.Direction = (DirectionsEnum)message.Direction;
            });

            e.Character.Context.Clients.ForEach(c => c.SendGameMapChangeOrientationMessage(e.Character));
        }

        public static void EmotePlayRequestMessage(Client e, EmotePlayRequestMessage message)
        {
            if (e.Character.Emotes.Contains(message.EmoteId))
            {
                e.Character.Context.Clients.ForEach(c => c.SendEmotePlayMessage(message, e.Character));
            }
            else
            {
                e.SendBasicNoOperationMessage();
            }
        }

        public static void GameRolePlayAttackMonsterRequestMessage(Client e, GameRolePlayAttackMonsterRequestMessage message)
        {
            var gameRolePlayActorInformations = e.Character.Context.GameRolePlayActorInformations
                .OfType<GameRolePlayGroupMonsterInformations>()
                .FirstOrDefault(x => (int)x.ContextualId == (int)message.MonsterGroupId);

            if (gameRolePlayActorInformations == null)
            {
                e.SendBasicNoOperationMessage();
            }
            else
            {
                e.Character.Context.Clients.ForEach(c => c.SendGameRolePlayShowActorMessage(gameRolePlayActorInformations));
                e.Character.Context.Clients.ForEach(c => c.SendGameContextRemoveElementMessage(gameRolePlayActorInformations));

                e.Character.Fight = Fight.Instance(e, gameRolePlayActorInformations);
                e.Character.Fight.Enter(e);
            }
        }
    }
}