﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Horizon.Common.Network;
using Horizon.Handlers;
using Horizon.Protocol;
using Event = Horizon.Common.Network.Base.Event;

namespace Horizon.Network
{
    public static class Messages
    {
        private static readonly List<MethodInfo> Methods = new List<MethodInfo>();
        private static readonly List<NetworkMessage> Instances = new List<NetworkMessage>();

        public static void Setup()
        {
            foreach (var e in Assembly.GetAssembly(typeof(ConnectionHandler)).GetTypes())
            {
                Methods.AddRange(e.GetMethods().Where(x => x.GetParameters().Length == 2));
            }

            foreach (var e in Assembly.GetAssembly(typeof(NetworkMessage)).GetTypes().Where(x => Methods.Select(n => n.Name).Contains(x.Name)))
            {
                Instances.Add((NetworkMessage)Activator.CreateInstance(e));
            }
        }

        internal static void Process(Client e, BigEndianReader reader)
        {
            var protocol = (short)(reader.ReadShort() >> 2);

            var message = Instances.FirstOrDefault(x => x.Protocol == protocol);

            if (message == null)
            {
                e.Log(Event.Receive, protocol.ToString());
            }
            else
            {
                e.Log(Event.Receive, message.GetType().Name);

                message.Deserialize(reader);
                Methods.First(x => x.Name == message.GetType().Name).Invoke(null, new object[] { e, message });
            }
        }
    }
}