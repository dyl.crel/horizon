﻿using System.Collections.Generic;
using System.Linq;
using Horizon.Common.Network;

namespace Horizon.Network
{
    public static class Listeners
    {
        private static readonly Listener A = new Listener
        {
            Port = 443,
            Clients = new List<Client>()
        };

        private static readonly Listener B = new Listener
        {
            Port = 5555,
            Clients = new List<Client>()
        };

        public static void Start()
        {
            A.Listen();
            B.Listen();
        }

        public static Client[] Find(short e)
        {
            return A.Clients.Where(x => x.Account.Key == e).ToArray();
        }
    }
}