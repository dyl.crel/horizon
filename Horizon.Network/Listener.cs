﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Horizon.Common.Network;
using Horizon.Protocol;

namespace Horizon.Network
{
    public sealed class Listener
    {
        public short Port { get; set; }

        public List<Client> Clients { get; set; }

        public void Listen()
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new IPEndPoint(IPAddress.Any, Port));
            socket.Listen(12);
            socket.BeginAccept(AsynchronousAccept, socket);
        }

        private void AsynchronousAccept(IAsyncResult result)
        {
            var socket = (Socket)result.AsyncState;

            var e = new Client
            {
                Socket = socket.EndAccept(result),
                Buffer = new byte[512]
            };

            Clients.Add(e.Connect());

            BeginReceive(e);

            BeginAccept(socket);
        }

        private void AsynchronousReceive(IAsyncResult result)
        {
            var e = (Client)result.AsyncState;

            if (e.Connected)
            {
                var bytesCount = e.Socket.EndReceive(result, out var errorCode);

                if (errorCode == SocketError.Success && bytesCount > 0)
                {
                    var messageData = new byte[bytesCount];

                    Array.Copy(e.Buffer, 0, messageData, 0, messageData.Length);

                    using (var reader = new BigEndianReader(messageData))
                    {
                        Messages.Process(e, reader);
                    }

                    BeginReceive(e);
                }
                else
                {
                    Clients.Remove(e.Disconnect());

                }
            }
            else
            {
                Clients.Remove(e.Disconnect());
            }
        }

        private void BeginAccept(Socket e)
        {
            e.BeginAccept(AsynchronousAccept, e);
        }

        private void BeginReceive(Base e)
        {
            e.Socket.BeginReceive(e.Buffer, 0, e.Buffer.Length, SocketFlags.None, AsynchronousReceive, e);
        }
    }
}