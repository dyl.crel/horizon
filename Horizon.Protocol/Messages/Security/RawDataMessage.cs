namespace Horizon.Protocol.Messages.Security
{
    public class RawDataMessage : NetworkMessage
    {
        public override short Protocol => 6253;

        public byte[] Content { get; set; }

        public RawDataMessage()
        {
        }

        public RawDataMessage(byte[] content)
        {
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Content.Length);
            for (var i = 0; i < Content.Length; i++)
            {
                writer.WriteByte(Content[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Content = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Content.Length; i++)
            {
                Content[i] = reader.ReadByte();
            }
        }
    }
}