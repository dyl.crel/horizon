namespace Horizon.Protocol.Messages.Security
{
    public class CheckIntegrityMessage : NetworkMessage
    {
        public override short Protocol => 6372;

        public byte[] Data { get; set; }

        public CheckIntegrityMessage()
        {
        }

        public CheckIntegrityMessage(byte[] data)
        {
            Data = data;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Data.Length);
            for (var i = 0; i < Data.Length; i++)
            {
                writer.WriteByte(Data[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Data = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Data.Length; i++)
            {
                Data[i] = reader.ReadByte();
            }
        }
    }
}