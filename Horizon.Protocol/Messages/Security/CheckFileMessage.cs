namespace Horizon.Protocol.Messages.Security
{
    public class CheckFileMessage : NetworkMessage
    {
        public override short Protocol => 6156;

        public string FilenameHash { get; set; }

        public byte Type { get; set; }

        public string Value { get; set; }

        public CheckFileMessage()
        {
        }

        public CheckFileMessage(string filenameHash, byte type, string value)
        {
            FilenameHash = filenameHash;
            Type = type;
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(FilenameHash);
            writer.WriteByte(Type);
            writer.WriteUTF(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FilenameHash = reader.ReadUTF();
            Type = reader.ReadByte();
            Value = reader.ReadUTF();
        }
    }
}