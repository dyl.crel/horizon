namespace Horizon.Protocol.Messages.Security
{
    public class ClientKeyMessage : NetworkMessage
    {
        public override short Protocol => 5607;

        public string Key { get; set; }

        public ClientKeyMessage()
        {
        }

        public ClientKeyMessage(string key)
        {
            Key = key;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Key);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Key = reader.ReadUTF();
        }
    }
}