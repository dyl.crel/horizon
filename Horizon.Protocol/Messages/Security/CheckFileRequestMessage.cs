namespace Horizon.Protocol.Messages.Security
{
    public class CheckFileRequestMessage : NetworkMessage
    {
        public override short Protocol => 6154;

        public string Filename { get; set; }

        public byte Type { get; set; }

        public CheckFileRequestMessage()
        {
        }

        public CheckFileRequestMessage(string filename, byte type)
        {
            Filename = filename;
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Filename);
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Filename = reader.ReadUTF();
            Type = reader.ReadByte();
        }
    }
}