namespace Horizon.Protocol.Messages.Common.Basic
{
    public class BasicStatMessage : NetworkMessage
    {
        public override short Protocol => 6530;

        public double TimeSpent { get; set; }

        public short StatId { get; set; }

        public BasicStatMessage()
        {
        }

        public BasicStatMessage(double timeSpent, short statId)
        {
            TimeSpent = timeSpent;
            StatId = statId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(TimeSpent);
            writer.WriteVarShort(StatId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TimeSpent = reader.ReadDouble();
            StatId = reader.ReadVarShort();
        }
    }
}