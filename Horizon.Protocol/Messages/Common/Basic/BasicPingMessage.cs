namespace Horizon.Protocol.Messages.Common.Basic
{
    public class BasicPingMessage : NetworkMessage
    {
        public override short Protocol => 182;

        public bool Quiet { get; set; }

        public BasicPingMessage()
        {
        }

        public BasicPingMessage(bool quiet)
        {
            Quiet = quiet;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Quiet);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quiet = reader.ReadBoolean();
        }
    }
}