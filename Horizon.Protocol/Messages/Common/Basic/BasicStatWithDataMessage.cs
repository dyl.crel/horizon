using Horizon.Protocol.Types.Common.Basic;

namespace Horizon.Protocol.Messages.Common.Basic
{
    public class BasicStatWithDataMessage : BasicStatMessage
    {
        public override short Protocol => 6573;

        public StatisticData[] Datas { get; set; }

        public BasicStatWithDataMessage()
        {
        }

        public BasicStatWithDataMessage(double timeSpent, short statId, StatisticData[] datas) : base(timeSpent, statId)
        {
            Datas = datas;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Datas.Length);
            for (var i = 0; i < Datas.Length; i++)
            {
                writer.WriteShort(Datas[i].Protocol);
                Datas[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Datas = new StatisticData[reader.ReadShort()];
            for (var i = 0; i < Datas.Length; i++)
            {
                Datas[i] = ProtocolTypesManager.Instance<StatisticData>(reader.ReadShort());
                Datas[i].Deserialize(reader);
            }
        }
    }
}