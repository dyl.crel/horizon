namespace Horizon.Protocol.Messages.Common.Basic
{
    public class BasicPongMessage : NetworkMessage
    {
        public override short Protocol => 183;

        public bool Quiet { get; set; }

        public BasicPongMessage()
        {
        }

        public BasicPongMessage(bool quiet)
        {
            Quiet = quiet;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Quiet);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quiet = reader.ReadBoolean();
        }
    }
}