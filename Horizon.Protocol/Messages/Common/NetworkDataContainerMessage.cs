namespace Horizon.Protocol.Messages.Common
{
    public class NetworkDataContainerMessage : NetworkMessage
    {
        public override short Protocol => 2;

        public byte[] Content { get; set; }

        public NetworkDataContainerMessage()
        {
        }

        public NetworkDataContainerMessage(byte[] content)
        {
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBytes(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Content = reader.ReadBytes(0);
        }
    }
}