namespace Horizon.Protocol.Messages.Subscription
{
    public class AccountInformationsUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6740;

        public double SubscriptionEndDate { get; set; }

        public double UnlimitedRestatEndDate { get; set; }

        public AccountInformationsUpdateMessage()
        {
        }

        public AccountInformationsUpdateMessage(double subscriptionEndDate, double unlimitedRestatEndDate)
        {
            SubscriptionEndDate = subscriptionEndDate;
            UnlimitedRestatEndDate = unlimitedRestatEndDate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(SubscriptionEndDate);
            writer.WriteDouble(UnlimitedRestatEndDate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubscriptionEndDate = reader.ReadDouble();
            UnlimitedRestatEndDate = reader.ReadDouble();
        }
    }
}