namespace Horizon.Protocol.Messages.Authorized
{
    public class ConsoleMessage : NetworkMessage
    {
        public override short Protocol => 75;

        public byte Type { get; set; }

        public string Content { get; set; }

        public ConsoleMessage()
        {
        }

        public ConsoleMessage(byte type, string content)
        {
            Type = type;
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
            writer.WriteUTF(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
            Content = reader.ReadUTF();
        }
    }
}