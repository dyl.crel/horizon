namespace Horizon.Protocol.Messages.Authorized
{
    public class ConsoleCommandsListMessage : NetworkMessage
    {
        public override short Protocol => 6127;

        public string[] Aliases { get; set; }

        public string[] Args { get; set; }

        public string[] Descriptions { get; set; }

        public ConsoleCommandsListMessage()
        {
        }

        public ConsoleCommandsListMessage(string[] aliases, string[] args, string[] descriptions)
        {
            Aliases = aliases;
            Args = args;
            Descriptions = descriptions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Aliases.Length);
            for (var i = 0; i < Aliases.Length; i++)
            {
                writer.WriteUTF(Aliases[i]);
            }
            writer.WriteShort(Args.Length);
            for (var i = 0; i < Args.Length; i++)
            {
                writer.WriteUTF(Args[i]);
            }
            writer.WriteShort(Descriptions.Length);
            for (var i = 0; i < Descriptions.Length; i++)
            {
                writer.WriteUTF(Descriptions[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Aliases = new string[reader.ReadShort()];
            for (var i = 0; i < Aliases.Length; i++)
            {
                Aliases[i] = reader.ReadUTF();
            }
            Args = new string[reader.ReadShort()];
            for (var i = 0; i < Args.Length; i++)
            {
                Args[i] = reader.ReadUTF();
            }
            Descriptions = new string[reader.ReadShort()];
            for (var i = 0; i < Descriptions.Length; i++)
            {
                Descriptions[i] = reader.ReadUTF();
            }
        }
    }
}