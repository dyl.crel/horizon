namespace Horizon.Protocol.Messages.Authorized
{
    public class AdminCommandMessage : NetworkMessage
    {
        public override short Protocol => 76;

        public string Content { get; set; }

        public AdminCommandMessage()
        {
        }

        public AdminCommandMessage(string content)
        {
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Content = reader.ReadUTF();
        }
    }
}