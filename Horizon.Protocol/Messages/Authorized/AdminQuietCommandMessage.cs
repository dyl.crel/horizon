namespace Horizon.Protocol.Messages.Authorized
{
    public class AdminQuietCommandMessage : AdminCommandMessage
    {
        public override short Protocol => 5662;

        public AdminQuietCommandMessage()
        {
        }

        public AdminQuietCommandMessage(string content) : base(content)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}