namespace Horizon.Protocol.Messages.Connection
{
    public class HelloConnectMessage : NetworkMessage
    {
        public override short Protocol => 3;

        public string Salt { get; set; }

        public sbyte[] Key { get; set; }

        public HelloConnectMessage()
        {
        }

        public HelloConnectMessage(string salt, sbyte[] key)
        {
            Salt = salt;
            Key = key;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Salt);
            writer.WriteVarInt(Key.Length);
            for (var i = 0; i < Key.Length; i++)
            {
                writer.WriteSByte(Key[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Salt = reader.ReadUTF();
            Key = new sbyte[reader.ReadVarInt()];
            for (var i = 0; i < Key.Length; i++)
            {
                Key[i] = reader.ReadSByte();
            }
        }
    }
}