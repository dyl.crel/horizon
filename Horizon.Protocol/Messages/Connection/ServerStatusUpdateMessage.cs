using Horizon.Protocol.Types.Connection;

namespace Horizon.Protocol.Messages.Connection
{
    public class ServerStatusUpdateMessage : NetworkMessage
    {
        public override short Protocol => 50;

        public GameServerInformations Server { get; set; }

        public ServerStatusUpdateMessage()
        {
        }

        public ServerStatusUpdateMessage(GameServerInformations server)
        {
            Server = server;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Server.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Server = new GameServerInformations();
            Server.Deserialize(reader);
        }
    }
}