namespace Horizon.Protocol.Messages.Connection
{
    public class SelectedServerRefusedMessage : NetworkMessage
    {
        public override short Protocol => 41;

        public short ServerId { get; set; }

        public byte Error { get; set; }

        public byte ServerStatus { get; set; }

        public SelectedServerRefusedMessage()
        {
        }

        public SelectedServerRefusedMessage(short serverId, byte error, byte serverStatus)
        {
            ServerId = serverId;
            Error = error;
            ServerStatus = serverStatus;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ServerId);
            writer.WriteByte(Error);
            writer.WriteByte(ServerStatus);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ServerId = reader.ReadVarShort();
            Error = reader.ReadByte();
            ServerStatus = reader.ReadByte();
        }
    }
}