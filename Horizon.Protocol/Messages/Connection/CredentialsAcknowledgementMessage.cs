namespace Horizon.Protocol.Messages.Connection
{
    public class CredentialsAcknowledgementMessage : NetworkMessage
    {
        public override short Protocol => 6314;

        public CredentialsAcknowledgementMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}