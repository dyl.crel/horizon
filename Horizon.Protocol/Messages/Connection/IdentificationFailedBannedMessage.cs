namespace Horizon.Protocol.Messages.Connection
{
    public class IdentificationFailedBannedMessage : IdentificationFailedMessage
    {
        public override short Protocol => 6174;

        public double BanEndDate { get; set; }

        public IdentificationFailedBannedMessage()
        {
        }

        public IdentificationFailedBannedMessage(byte reason, double banEndDate) : base(reason)
        {
            BanEndDate = banEndDate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(BanEndDate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            BanEndDate = reader.ReadDouble();
        }
    }
}