namespace Horizon.Protocol.Messages.Connection
{
    public class MigratedServerListMessage : NetworkMessage
    {
        public override short Protocol => 6731;

        public short[] MigratedServerIds { get; set; }

        public MigratedServerListMessage()
        {
        }

        public MigratedServerListMessage(short[] migratedServerIds)
        {
            MigratedServerIds = migratedServerIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(MigratedServerIds.Length);
            for (var i = 0; i < MigratedServerIds.Length; i++)
            {
                writer.WriteVarShort(MigratedServerIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MigratedServerIds = new short[reader.ReadShort()];
            for (var i = 0; i < MigratedServerIds.Length; i++)
            {
                MigratedServerIds[i] = reader.ReadVarShort();
            }
        }
    }
}