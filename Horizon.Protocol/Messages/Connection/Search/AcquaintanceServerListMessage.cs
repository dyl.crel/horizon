namespace Horizon.Protocol.Messages.Connection.Search
{
    public class AcquaintanceServerListMessage : NetworkMessage
    {
        public override short Protocol => 6142;

        public short[] Servers { get; set; }

        public AcquaintanceServerListMessage()
        {
        }

        public AcquaintanceServerListMessage(short[] servers)
        {
            Servers = servers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Servers.Length);
            for (var i = 0; i < Servers.Length; i++)
            {
                writer.WriteVarShort(Servers[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Servers = new short[reader.ReadShort()];
            for (var i = 0; i < Servers.Length; i++)
            {
                Servers[i] = reader.ReadVarShort();
            }
        }
    }
}