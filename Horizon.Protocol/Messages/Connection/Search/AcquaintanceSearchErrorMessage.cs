namespace Horizon.Protocol.Messages.Connection.Search
{
    public class AcquaintanceSearchErrorMessage : NetworkMessage
    {
        public override short Protocol => 6143;

        public byte Reason { get; set; }

        public AcquaintanceSearchErrorMessage()
        {
        }

        public AcquaintanceSearchErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}