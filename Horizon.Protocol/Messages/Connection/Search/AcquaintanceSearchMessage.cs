namespace Horizon.Protocol.Messages.Connection.Search
{
    public class AcquaintanceSearchMessage : NetworkMessage
    {
        public override short Protocol => 6144;

        public string Nickname { get; set; }

        public AcquaintanceSearchMessage()
        {
        }

        public AcquaintanceSearchMessage(string nickname)
        {
            Nickname = nickname;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Nickname);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Nickname = reader.ReadUTF();
        }
    }
}