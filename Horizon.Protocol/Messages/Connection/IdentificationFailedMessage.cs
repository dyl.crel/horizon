namespace Horizon.Protocol.Messages.Connection
{
    public class IdentificationFailedMessage : NetworkMessage
    {
        public override short Protocol => 20;

        public byte Reason { get; set; }

        public IdentificationFailedMessage()
        {
        }

        public IdentificationFailedMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}