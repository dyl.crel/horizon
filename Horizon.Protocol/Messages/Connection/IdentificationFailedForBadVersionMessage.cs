using Horizon.Protocol.Types.Version;

namespace Horizon.Protocol.Messages.Connection
{
    public class IdentificationFailedForBadVersionMessage : IdentificationFailedMessage
    {
        public override short Protocol => 21;

        public Version RequiredVersion { get; set; }

        public IdentificationFailedForBadVersionMessage()
        {
        }

        public IdentificationFailedForBadVersionMessage(byte reason, Version requiredVersion) : base(reason)
        {
            RequiredVersion = requiredVersion;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            RequiredVersion.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            RequiredVersion = new Version();
            RequiredVersion.Deserialize(reader);
        }
    }
}