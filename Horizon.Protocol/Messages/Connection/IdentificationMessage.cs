using Horizon.Protocol.Types.Version;

namespace Horizon.Protocol.Messages.Connection
{
    public class IdentificationMessage : NetworkMessage
    {
        public override short Protocol => 4;

        public bool Autoconnect { get; set; }

        public bool UseCertificate { get; set; }

        public bool UseLoginToken { get; set; }

        public VersionExtended Version { get; set; }

        public string Lang { get; set; }

        public byte[] Credentials { get; set; }

        public short ServerId { get; set; }

        public long SessionOptionalSalt { get; set; }

        public short[] FailedAttempts { get; set; }

        public IdentificationMessage()
        {
        }

        public IdentificationMessage(bool autoconnect, bool useCertificate, bool useLoginToken, VersionExtended version, string lang, byte[] credentials, short serverId, long sessionOptionalSalt, short[] failedAttempts)
        {
            Autoconnect = autoconnect;
            UseCertificate = useCertificate;
            UseLoginToken = useLoginToken;
            Version = version;
            Lang = lang;
            Credentials = credentials;
            ServerId = serverId;
            SessionOptionalSalt = sessionOptionalSalt;
            FailedAttempts = failedAttempts;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Autoconnect);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, UseCertificate);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, UseLoginToken);
            writer.WriteByte(flag0);
            Version.Serialize(writer);
            writer.WriteUTF(Lang);
            writer.WriteVarInt(Credentials.Length);
            for (var i = 0; i < Credentials.Length; i++)
            {
                writer.WriteByte(Credentials[i]);
            }
            writer.WriteShort(ServerId);
            writer.WriteVarLong(SessionOptionalSalt);
            writer.WriteShort(FailedAttempts.Length);
            for (var i = 0; i < FailedAttempts.Length; i++)
            {
                writer.WriteVarShort(FailedAttempts[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Autoconnect = BooleanByteWrapper.GetFlag(flag0, 0);
            UseCertificate = BooleanByteWrapper.GetFlag(flag0, 1);
            UseLoginToken = BooleanByteWrapper.GetFlag(flag0, 2);
            Version = new VersionExtended();
            Version.Deserialize(reader);
            Lang = reader.ReadUTF();
            Credentials = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Credentials.Length; i++)
            {
                Credentials[i] = reader.ReadByte();
            }
            ServerId = reader.ReadShort();
            SessionOptionalSalt = reader.ReadVarLong();
            FailedAttempts = new short[reader.ReadShort()];
            for (var i = 0; i < FailedAttempts.Length; i++)
            {
                FailedAttempts[i] = reader.ReadVarShort();
            }
        }
    }
}