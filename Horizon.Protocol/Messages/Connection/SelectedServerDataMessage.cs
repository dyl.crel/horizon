namespace Horizon.Protocol.Messages.Connection
{
    public class SelectedServerDataMessage : NetworkMessage
    {
        public override short Protocol => 42;

        public short ServerId { get; set; }

        public string Address { get; set; }

        public int[] Ports { get; set; }

        public bool CanCreateNewCharacter { get; set; }

        public byte[] Ticket { get; set; }

        public SelectedServerDataMessage()
        {
        }

        public SelectedServerDataMessage(short serverId, string address, int[] ports, bool canCreateNewCharacter, byte[] ticket)
        {
            ServerId = serverId;
            Address = address;
            Ports = ports;
            CanCreateNewCharacter = canCreateNewCharacter;
            Ticket = ticket;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ServerId);
            writer.WriteUTF(Address);
            writer.WriteShort(Ports.Length);
            for (var i = 0; i < Ports.Length; i++)
            {
                writer.WriteInt(Ports[i]);
            }
            writer.WriteBoolean(CanCreateNewCharacter);
            writer.WriteVarInt(Ticket.Length);
            for (var i = 0; i < Ticket.Length; i++)
            {
                writer.WriteByte(Ticket[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ServerId = reader.ReadVarShort();
            Address = reader.ReadUTF();
            Ports = new int[reader.ReadShort()];
            for (var i = 0; i < Ports.Length; i++)
            {
                Ports[i] = reader.ReadInt();
            }
            CanCreateNewCharacter = reader.ReadBoolean();
            Ticket = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Ticket.Length; i++)
            {
                Ticket[i] = reader.ReadByte();
            }
        }
    }
}