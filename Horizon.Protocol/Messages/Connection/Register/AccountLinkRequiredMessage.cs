namespace Horizon.Protocol.Messages.Connection.Register
{
    public class AccountLinkRequiredMessage : NetworkMessage
    {
        public override short Protocol => 6607;

        public AccountLinkRequiredMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}