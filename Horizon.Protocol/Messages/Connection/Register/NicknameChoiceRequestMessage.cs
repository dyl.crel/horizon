namespace Horizon.Protocol.Messages.Connection.Register
{
    public class NicknameChoiceRequestMessage : NetworkMessage
    {
        public override short Protocol => 5639;

        public string Nickname { get; set; }

        public NicknameChoiceRequestMessage()
        {
        }

        public NicknameChoiceRequestMessage(string nickname)
        {
            Nickname = nickname;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Nickname);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Nickname = reader.ReadUTF();
        }
    }
}