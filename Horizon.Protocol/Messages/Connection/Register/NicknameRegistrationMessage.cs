namespace Horizon.Protocol.Messages.Connection.Register
{
    public class NicknameRegistrationMessage : NetworkMessage
    {
        public override short Protocol => 5640;

        public NicknameRegistrationMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}