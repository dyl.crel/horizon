namespace Horizon.Protocol.Messages.Connection.Register
{
    public class NicknameAcceptedMessage : NetworkMessage
    {
        public override short Protocol => 5641;

        public NicknameAcceptedMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}