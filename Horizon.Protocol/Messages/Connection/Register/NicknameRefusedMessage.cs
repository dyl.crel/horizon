namespace Horizon.Protocol.Messages.Connection.Register
{
    public class NicknameRefusedMessage : NetworkMessage
    {
        public override short Protocol => 5638;

        public byte Reason { get; set; }

        public NicknameRefusedMessage()
        {
        }

        public NicknameRefusedMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}