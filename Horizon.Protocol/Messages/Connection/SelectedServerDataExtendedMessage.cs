using Horizon.Protocol.Types.Connection;

namespace Horizon.Protocol.Messages.Connection
{
    public class SelectedServerDataExtendedMessage : SelectedServerDataMessage
    {
        public override short Protocol => 6469;

        public GameServerInformations[] Servers { get; set; }

        public SelectedServerDataExtendedMessage()
        {
        }

        public SelectedServerDataExtendedMessage(short serverId, string address, int[] ports, bool canCreateNewCharacter, byte[] ticket, GameServerInformations[] servers) : base(serverId, address, ports, canCreateNewCharacter, ticket)
        {
            Servers = servers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Servers.Length);
            for (var i = 0; i < Servers.Length; i++)
            {
                Servers[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Servers = new GameServerInformations[reader.ReadShort()];
            for (var i = 0; i < Servers.Length; i++)
            {
                Servers[i] = new GameServerInformations();
                Servers[i].Deserialize(reader);
            }
        }
    }
}