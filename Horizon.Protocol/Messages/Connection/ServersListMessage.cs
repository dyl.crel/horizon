using Horizon.Protocol.Types.Connection;

namespace Horizon.Protocol.Messages.Connection
{
    public class ServersListMessage : NetworkMessage
    {
        public override short Protocol => 30;

        public GameServerInformations[] Servers { get; set; }

        public short AlreadyConnectedToServerId { get; set; }

        public bool CanCreateNewCharacter { get; set; }

        public ServersListMessage()
        {
        }

        public ServersListMessage(GameServerInformations[] servers, short alreadyConnectedToServerId, bool canCreateNewCharacter)
        {
            Servers = servers;
            AlreadyConnectedToServerId = alreadyConnectedToServerId;
            CanCreateNewCharacter = canCreateNewCharacter;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Servers.Length);
            for (var i = 0; i < Servers.Length; i++)
            {
                Servers[i].Serialize(writer);
            }
            writer.WriteVarShort(AlreadyConnectedToServerId);
            writer.WriteBoolean(CanCreateNewCharacter);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Servers = new GameServerInformations[reader.ReadShort()];
            for (var i = 0; i < Servers.Length; i++)
            {
                Servers[i] = new GameServerInformations();
                Servers[i].Deserialize(reader);
            }
            AlreadyConnectedToServerId = reader.ReadVarShort();
            CanCreateNewCharacter = reader.ReadBoolean();
        }
    }
}