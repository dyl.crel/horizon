namespace Horizon.Protocol.Messages.Connection
{
    public class IdentificationSuccessMessage : NetworkMessage
    {
        public override short Protocol => 22;

        public bool HasRights { get; set; }

        public bool WasAlreadyConnected { get; set; }

        public string Login { get; set; }

        public string Nickname { get; set; }

        public int AccountId { get; set; }

        public byte CommunityId { get; set; }

        public string SecretQuestion { get; set; }

        public double AccountCreation { get; set; }

        public double SubscriptionElapsedDuration { get; set; }

        public double SubscriptionEndDate { get; set; }

        public byte HavenbagAvailableRoom { get; set; }

        public IdentificationSuccessMessage()
        {
        }

        public IdentificationSuccessMessage(bool hasRights, bool wasAlreadyConnected, string login, string nickname, int accountId, byte communityId, string secretQuestion, double accountCreation, double subscriptionElapsedDuration, double subscriptionEndDate, byte havenbagAvailableRoom)
        {
            HasRights = hasRights;
            WasAlreadyConnected = wasAlreadyConnected;
            Login = login;
            Nickname = nickname;
            AccountId = accountId;
            CommunityId = communityId;
            SecretQuestion = secretQuestion;
            AccountCreation = accountCreation;
            SubscriptionElapsedDuration = subscriptionElapsedDuration;
            SubscriptionEndDate = subscriptionEndDate;
            HavenbagAvailableRoom = havenbagAvailableRoom;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, HasRights);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, WasAlreadyConnected);
            writer.WriteByte(flag0);
            writer.WriteUTF(Login);
            writer.WriteUTF(Nickname);
            writer.WriteInt(AccountId);
            writer.WriteByte(CommunityId);
            writer.WriteUTF(SecretQuestion);
            writer.WriteDouble(AccountCreation);
            writer.WriteDouble(SubscriptionElapsedDuration);
            writer.WriteDouble(SubscriptionEndDate);
            writer.WriteByte(HavenbagAvailableRoom);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            HasRights = BooleanByteWrapper.GetFlag(flag0, 0);
            WasAlreadyConnected = BooleanByteWrapper.GetFlag(flag0, 1);
            Login = reader.ReadUTF();
            Nickname = reader.ReadUTF();
            AccountId = reader.ReadInt();
            CommunityId = reader.ReadByte();
            SecretQuestion = reader.ReadUTF();
            AccountCreation = reader.ReadDouble();
            SubscriptionElapsedDuration = reader.ReadDouble();
            SubscriptionEndDate = reader.ReadDouble();
            HavenbagAvailableRoom = reader.ReadByte();
        }
    }
}