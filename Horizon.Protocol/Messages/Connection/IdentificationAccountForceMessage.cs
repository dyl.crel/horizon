using Horizon.Protocol.Types.Version;

namespace Horizon.Protocol.Messages.Connection
{
    public class IdentificationAccountForceMessage : IdentificationMessage
    {
        public override short Protocol => 6119;

        public string ForcedAccountLogin { get; set; }

        public IdentificationAccountForceMessage()
        {
        }

        public IdentificationAccountForceMessage(bool autoconnect, bool useCertificate, bool useLoginToken, VersionExtended version, string lang, byte[] credentials, short serverId, long sessionOptionalSalt, short[] failedAttempts, string forcedAccountLogin) : base(autoconnect, useCertificate, useLoginToken, version, lang, credentials, serverId, sessionOptionalSalt, failedAttempts)
        {
            ForcedAccountLogin = forcedAccountLogin;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(ForcedAccountLogin);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ForcedAccountLogin = reader.ReadUTF();
        }
    }
}