namespace Horizon.Protocol.Messages.Connection
{
    public class ServerSelectionMessage : NetworkMessage
    {
        public override short Protocol => 40;

        public short ServerId { get; set; }

        public ServerSelectionMessage()
        {
        }

        public ServerSelectionMessage(short serverId)
        {
            ServerId = serverId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ServerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ServerId = reader.ReadVarShort();
        }
    }
}