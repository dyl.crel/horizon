namespace Horizon.Protocol.Messages.Server.Basic
{
    public class SystemMessageDisplayMessage : NetworkMessage
    {
        public override short Protocol => 189;

        public bool HangUp { get; set; }

        public short MsgId { get; set; }

        public string[] Parameters { get; set; }

        public SystemMessageDisplayMessage()
        {
        }

        public SystemMessageDisplayMessage(bool hangUp, short msgId, string[] parameters)
        {
            HangUp = hangUp;
            MsgId = msgId;
            Parameters = parameters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(HangUp);
            writer.WriteVarShort(MsgId);
            writer.WriteShort(Parameters.Length);
            for (var i = 0; i < Parameters.Length; i++)
            {
                writer.WriteUTF(Parameters[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HangUp = reader.ReadBoolean();
            MsgId = reader.ReadVarShort();
            Parameters = new string[reader.ReadShort()];
            for (var i = 0; i < Parameters.Length; i++)
            {
                Parameters[i] = reader.ReadUTF();
            }
        }
    }
}