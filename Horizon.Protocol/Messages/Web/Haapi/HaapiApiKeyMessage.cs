namespace Horizon.Protocol.Messages.Web.Haapi
{
    public class HaapiApiKeyMessage : NetworkMessage
    {
        public override short Protocol => 6649;

        public string Token { get; set; }

        public HaapiApiKeyMessage()
        {
        }

        public HaapiApiKeyMessage(string token)
        {
            Token = token;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Token);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Token = reader.ReadUTF();
        }
    }
}