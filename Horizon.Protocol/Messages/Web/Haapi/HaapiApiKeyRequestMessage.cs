namespace Horizon.Protocol.Messages.Web.Haapi
{
    public class HaapiApiKeyRequestMessage : NetworkMessage
    {
        public override short Protocol => 6648;

        public HaapiApiKeyRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}