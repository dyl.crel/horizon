namespace Horizon.Protocol.Messages.Web.Haapi
{
    public class HaapiTokenRequestMessage : NetworkMessage
    {
        public override short Protocol => 6766;

        public HaapiTokenRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}