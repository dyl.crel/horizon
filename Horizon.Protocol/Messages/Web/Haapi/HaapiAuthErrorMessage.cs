namespace Horizon.Protocol.Messages.Web.Haapi
{
    public class HaapiAuthErrorMessage : NetworkMessage
    {
        public override short Protocol => 6768;

        public byte Type { get; set; }

        public HaapiAuthErrorMessage()
        {
        }

        public HaapiAuthErrorMessage(byte type)
        {
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
        }
    }
}