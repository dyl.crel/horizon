namespace Horizon.Protocol.Messages.Web.Haapi
{
    public class HaapiSessionMessage : NetworkMessage
    {
        public override short Protocol => 6769;

        public string Key { get; set; }

        public byte Type { get; set; }

        public HaapiSessionMessage()
        {
        }

        public HaapiSessionMessage(string key, byte type)
        {
            Key = key;
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Key);
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Key = reader.ReadUTF();
            Type = reader.ReadByte();
        }
    }
}