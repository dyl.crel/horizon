namespace Horizon.Protocol.Messages.Web.Haapi
{
    public class HaapiTokenMessage : NetworkMessage
    {
        public override short Protocol => 6767;

        public string Token { get; set; }

        public HaapiTokenMessage()
        {
        }

        public HaapiTokenMessage(string token)
        {
            Token = token;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Token);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Token = reader.ReadUTF();
        }
    }
}