namespace Horizon.Protocol.Messages.Web.Ankabox
{
    public class NewMailMessage : MailStatusMessage
    {
        public override short Protocol => 6292;

        public int[] SendersAccountId { get; set; }

        public NewMailMessage()
        {
        }

        public NewMailMessage(short unread, short total, int[] sendersAccountId) : base(unread, total)
        {
            SendersAccountId = sendersAccountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(SendersAccountId.Length);
            for (var i = 0; i < SendersAccountId.Length; i++)
            {
                writer.WriteInt(SendersAccountId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SendersAccountId = new int[reader.ReadShort()];
            for (var i = 0; i < SendersAccountId.Length; i++)
            {
                SendersAccountId[i] = reader.ReadInt();
            }
        }
    }
}