namespace Horizon.Protocol.Messages.Web.Ankabox
{
    public class MailStatusMessage : NetworkMessage
    {
        public override short Protocol => 6275;

        public short Unread { get; set; }

        public short Total { get; set; }

        public MailStatusMessage()
        {
        }

        public MailStatusMessage(short unread, short total)
        {
            Unread = unread;
            Total = total;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Unread);
            writer.WriteVarShort(Total);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Unread = reader.ReadVarShort();
            Total = reader.ReadVarShort();
        }
    }
}