using Horizon.Protocol.Types.Game.Entity;

namespace Horizon.Protocol.Messages.Game.Entity
{
    public class EntitiesInformationMessage : NetworkMessage
    {
        public override short Protocol => 6775;

        public EntityInformation[] Entities { get; set; }

        public EntitiesInformationMessage()
        {
        }

        public EntitiesInformationMessage(EntityInformation[] entities)
        {
            Entities = entities;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Entities.Length);
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Entities = new EntityInformation[reader.ReadShort()];
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i] = new EntityInformation();
                Entities[i].Deserialize(reader);
            }
        }
    }
}