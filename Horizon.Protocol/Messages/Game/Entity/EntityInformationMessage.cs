using Horizon.Protocol.Types.Game.Entity;

namespace Horizon.Protocol.Messages.Game.Entity
{
    public class EntityInformationMessage : NetworkMessage
    {
        public override short Protocol => 6771;

        public EntityInformation Entity { get; set; }

        public EntityInformationMessage()
        {
        }

        public EntityInformationMessage(EntityInformation entity)
        {
            Entity = entity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Entity.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Entity = new EntityInformation();
            Entity.Deserialize(reader);
        }
    }
}