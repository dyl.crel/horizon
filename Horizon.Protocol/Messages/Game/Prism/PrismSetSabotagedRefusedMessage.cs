namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismSetSabotagedRefusedMessage : NetworkMessage
    {
        public override short Protocol => 6466;

        public short SubAreaId { get; set; }

        public byte Reason { get; set; }

        public PrismSetSabotagedRefusedMessage()
        {
        }

        public PrismSetSabotagedRefusedMessage(short subAreaId, byte reason)
        {
            SubAreaId = subAreaId;
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            Reason = reader.ReadByte();
        }
    }
}