namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismModuleExchangeRequestMessage : NetworkMessage
    {
        public override short Protocol => 6531;

        public PrismModuleExchangeRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}