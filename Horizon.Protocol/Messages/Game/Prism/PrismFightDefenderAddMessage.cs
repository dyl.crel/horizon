using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightDefenderAddMessage : NetworkMessage
    {
        public override short Protocol => 5895;

        public short SubAreaId { get; set; }

        public short FightId { get; set; }

        public CharacterMinimalPlusLookInformations Defender { get; set; }

        public PrismFightDefenderAddMessage()
        {
        }

        public PrismFightDefenderAddMessage(short subAreaId, short fightId, CharacterMinimalPlusLookInformations defender)
        {
            SubAreaId = subAreaId;
            FightId = fightId;
            Defender = defender;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteVarShort(FightId);
            writer.WriteShort(Defender.Protocol);
            Defender.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            FightId = reader.ReadVarShort();
            Defender = ProtocolTypesManager.Instance<CharacterMinimalPlusLookInformations>(reader.ReadShort());
            Defender.Deserialize(reader);
        }
    }
}