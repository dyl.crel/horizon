namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismInfoInValidMessage : NetworkMessage
    {
        public override short Protocol => 5859;

        public byte Reason { get; set; }

        public PrismInfoInValidMessage()
        {
        }

        public PrismInfoInValidMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}