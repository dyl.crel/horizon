namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightDefenderLeaveMessage : NetworkMessage
    {
        public override short Protocol => 5892;

        public short SubAreaId { get; set; }

        public short FightId { get; set; }

        public long FighterToRemoveId { get; set; }

        public PrismFightDefenderLeaveMessage()
        {
        }

        public PrismFightDefenderLeaveMessage(short subAreaId, short fightId, long fighterToRemoveId)
        {
            SubAreaId = subAreaId;
            FightId = fightId;
            FighterToRemoveId = fighterToRemoveId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteVarShort(FightId);
            writer.WriteVarLong(FighterToRemoveId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            FightId = reader.ReadVarShort();
            FighterToRemoveId = reader.ReadVarLong();
        }
    }
}