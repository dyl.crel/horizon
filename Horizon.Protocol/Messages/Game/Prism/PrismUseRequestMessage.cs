namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismUseRequestMessage : NetworkMessage
    {
        public override short Protocol => 6041;

        public byte ModuleToUse { get; set; }

        public PrismUseRequestMessage()
        {
        }

        public PrismUseRequestMessage(byte moduleToUse)
        {
            ModuleToUse = moduleToUse;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ModuleToUse);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ModuleToUse = reader.ReadByte();
        }
    }
}