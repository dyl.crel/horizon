namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightRemovedMessage : NetworkMessage
    {
        public override short Protocol => 6453;

        public short SubAreaId { get; set; }

        public PrismFightRemovedMessage()
        {
        }

        public PrismFightRemovedMessage(short subAreaId)
        {
            SubAreaId = subAreaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
        }
    }
}