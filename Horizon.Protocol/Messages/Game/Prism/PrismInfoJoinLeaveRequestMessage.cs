namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismInfoJoinLeaveRequestMessage : NetworkMessage
    {
        public override short Protocol => 5844;

        public bool Join { get; set; }

        public PrismInfoJoinLeaveRequestMessage()
        {
        }

        public PrismInfoJoinLeaveRequestMessage(bool join)
        {
            Join = join;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Join);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Join = reader.ReadBoolean();
        }
    }
}