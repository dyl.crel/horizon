using Horizon.Protocol.Types.Game.Prism;

namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismsListUpdateMessage : PrismsListMessage
    {
        public override short Protocol => 6438;

        public PrismsListUpdateMessage()
        {
        }

        public PrismsListUpdateMessage(PrismSubareaEmptyInfo[] prisms) : base(prisms)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}