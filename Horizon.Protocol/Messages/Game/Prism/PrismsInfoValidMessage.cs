using Horizon.Protocol.Types.Game.Prism;

namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismsInfoValidMessage : NetworkMessage
    {
        public override short Protocol => 6451;

        public PrismFightersInformation[] Fights { get; set; }

        public PrismsInfoValidMessage()
        {
        }

        public PrismsInfoValidMessage(PrismFightersInformation[] fights)
        {
            Fights = fights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Fights.Length);
            for (var i = 0; i < Fights.Length; i++)
            {
                Fights[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Fights = new PrismFightersInformation[reader.ReadShort()];
            for (var i = 0; i < Fights.Length; i++)
            {
                Fights[i] = new PrismFightersInformation();
                Fights[i].Deserialize(reader);
            }
        }
    }
}