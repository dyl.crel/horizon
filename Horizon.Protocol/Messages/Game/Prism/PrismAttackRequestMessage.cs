namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismAttackRequestMessage : NetworkMessage
    {
        public override short Protocol => 6042;

        public PrismAttackRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}