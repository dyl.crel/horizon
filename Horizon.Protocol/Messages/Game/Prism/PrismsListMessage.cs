using Horizon.Protocol.Types.Game.Prism;

namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismsListMessage : NetworkMessage
    {
        public override short Protocol => 6440;

        public PrismSubareaEmptyInfo[] Prisms { get; set; }

        public PrismsListMessage()
        {
        }

        public PrismsListMessage(PrismSubareaEmptyInfo[] prisms)
        {
            Prisms = prisms;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Prisms.Length);
            for (var i = 0; i < Prisms.Length; i++)
            {
                writer.WriteShort(Prisms[i].Protocol);
                Prisms[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Prisms = new PrismSubareaEmptyInfo[reader.ReadShort()];
            for (var i = 0; i < Prisms.Length; i++)
            {
                Prisms[i] = ProtocolTypesManager.Instance<PrismSubareaEmptyInfo>(reader.ReadShort());
                Prisms[i].Deserialize(reader);
            }
        }
    }
}