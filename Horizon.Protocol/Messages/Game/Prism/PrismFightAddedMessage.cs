using Horizon.Protocol.Types.Game.Prism;

namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightAddedMessage : NetworkMessage
    {
        public override short Protocol => 6452;

        public PrismFightersInformation Fight { get; set; }

        public PrismFightAddedMessage()
        {
        }

        public PrismFightAddedMessage(PrismFightersInformation fight)
        {
            Fight = fight;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Fight.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Fight = new PrismFightersInformation();
            Fight.Deserialize(reader);
        }
    }
}