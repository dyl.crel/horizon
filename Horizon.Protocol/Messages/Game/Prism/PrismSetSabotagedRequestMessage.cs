namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismSetSabotagedRequestMessage : NetworkMessage
    {
        public override short Protocol => 6468;

        public short SubAreaId { get; set; }

        public PrismSetSabotagedRequestMessage()
        {
        }

        public PrismSetSabotagedRequestMessage(short subAreaId)
        {
            SubAreaId = subAreaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
        }
    }
}