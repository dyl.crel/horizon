using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightAttackerAddMessage : NetworkMessage
    {
        public override short Protocol => 5893;

        public short SubAreaId { get; set; }

        public short FightId { get; set; }

        public CharacterMinimalPlusLookInformations Attacker { get; set; }

        public PrismFightAttackerAddMessage()
        {
        }

        public PrismFightAttackerAddMessage(short subAreaId, short fightId, CharacterMinimalPlusLookInformations attacker)
        {
            SubAreaId = subAreaId;
            FightId = fightId;
            Attacker = attacker;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteVarShort(FightId);
            writer.WriteShort(Attacker.Protocol);
            Attacker.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            FightId = reader.ReadVarShort();
            Attacker = ProtocolTypesManager.Instance<CharacterMinimalPlusLookInformations>(reader.ReadShort());
            Attacker.Deserialize(reader);
        }
    }
}