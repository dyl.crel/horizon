namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightJoinLeaveRequestMessage : NetworkMessage
    {
        public override short Protocol => 5843;

        public short SubAreaId { get; set; }

        public bool Join { get; set; }

        public PrismFightJoinLeaveRequestMessage()
        {
        }

        public PrismFightJoinLeaveRequestMessage(short subAreaId, bool join)
        {
            SubAreaId = subAreaId;
            Join = join;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteBoolean(Join);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            Join = reader.ReadBoolean();
        }
    }
}