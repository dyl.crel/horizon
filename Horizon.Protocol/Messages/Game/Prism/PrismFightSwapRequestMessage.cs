namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightSwapRequestMessage : NetworkMessage
    {
        public override short Protocol => 5901;

        public short SubAreaId { get; set; }

        public long TargetId { get; set; }

        public PrismFightSwapRequestMessage()
        {
        }

        public PrismFightSwapRequestMessage(short subAreaId, long targetId)
        {
            SubAreaId = subAreaId;
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteVarLong(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            TargetId = reader.ReadVarLong();
        }
    }
}