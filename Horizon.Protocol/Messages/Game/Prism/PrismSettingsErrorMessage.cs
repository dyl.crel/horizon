namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismSettingsErrorMessage : NetworkMessage
    {
        public override short Protocol => 6442;

        public PrismSettingsErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}