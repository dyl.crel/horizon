namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismSettingsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6437;

        public short SubAreaId { get; set; }

        public byte StartDefenseTime { get; set; }

        public PrismSettingsRequestMessage()
        {
        }

        public PrismSettingsRequestMessage(short subAreaId, byte startDefenseTime)
        {
            SubAreaId = subAreaId;
            StartDefenseTime = startDefenseTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteByte(StartDefenseTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            StartDefenseTime = reader.ReadByte();
        }
    }
}