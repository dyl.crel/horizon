namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismFightStateUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6040;

        public byte State { get; set; }

        public PrismFightStateUpdateMessage()
        {
        }

        public PrismFightStateUpdateMessage(byte state)
        {
            State = state;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(State);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            State = reader.ReadByte();
        }
    }
}