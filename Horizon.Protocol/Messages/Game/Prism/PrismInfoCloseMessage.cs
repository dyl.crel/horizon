namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismInfoCloseMessage : NetworkMessage
    {
        public override short Protocol => 5853;

        public PrismInfoCloseMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}