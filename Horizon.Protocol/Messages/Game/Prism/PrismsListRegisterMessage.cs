namespace Horizon.Protocol.Messages.Game.Prism
{
    public class PrismsListRegisterMessage : NetworkMessage
    {
        public override short Protocol => 6441;

        public byte Listen { get; set; }

        public PrismsListRegisterMessage()
        {
        }

        public PrismsListRegisterMessage(byte listen)
        {
            Listen = listen;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Listen);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Listen = reader.ReadByte();
        }
    }
}