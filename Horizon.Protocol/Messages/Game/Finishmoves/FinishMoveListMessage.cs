using Horizon.Protocol.Types.Game.Finishmoves;

namespace Horizon.Protocol.Messages.Game.Finishmoves
{
    public class FinishMoveListMessage : NetworkMessage
    {
        public override short Protocol => 6704;

        public FinishMoveInformations[] FinishMoves { get; set; }

        public FinishMoveListMessage()
        {
        }

        public FinishMoveListMessage(FinishMoveInformations[] finishMoves)
        {
            FinishMoves = finishMoves;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FinishMoves.Length);
            for (var i = 0; i < FinishMoves.Length; i++)
            {
                FinishMoves[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FinishMoves = new FinishMoveInformations[reader.ReadShort()];
            for (var i = 0; i < FinishMoves.Length; i++)
            {
                FinishMoves[i] = new FinishMoveInformations();
                FinishMoves[i].Deserialize(reader);
            }
        }
    }
}