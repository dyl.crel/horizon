namespace Horizon.Protocol.Messages.Game.Finishmoves
{
    public class FinishMoveSetRequestMessage : NetworkMessage
    {
        public override short Protocol => 6703;

        public int FinishMoveId { get; set; }

        public bool FinishMoveState { get; set; }

        public FinishMoveSetRequestMessage()
        {
        }

        public FinishMoveSetRequestMessage(int finishMoveId, bool finishMoveState)
        {
            FinishMoveId = finishMoveId;
            FinishMoveState = finishMoveState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(FinishMoveId);
            writer.WriteBoolean(FinishMoveState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FinishMoveId = reader.ReadInt();
            FinishMoveState = reader.ReadBoolean();
        }
    }
}