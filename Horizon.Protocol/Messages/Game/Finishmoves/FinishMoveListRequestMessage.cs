namespace Horizon.Protocol.Messages.Game.Finishmoves
{
    public class FinishMoveListRequestMessage : NetworkMessage
    {
        public override short Protocol => 6702;

        public FinishMoveListRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}