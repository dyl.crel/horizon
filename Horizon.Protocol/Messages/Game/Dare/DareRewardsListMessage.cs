using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareRewardsListMessage : NetworkMessage
    {
        public override short Protocol => 6677;

        public DareReward[] Rewards { get; set; }

        public DareRewardsListMessage()
        {
        }

        public DareRewardsListMessage(DareReward[] rewards)
        {
            Rewards = rewards;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Rewards.Length);
            for (var i = 0; i < Rewards.Length; i++)
            {
                Rewards[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Rewards = new DareReward[reader.ReadShort()];
            for (var i = 0; i < Rewards.Length; i++)
            {
                Rewards[i] = new DareReward();
                Rewards[i].Deserialize(reader);
            }
        }
    }
}