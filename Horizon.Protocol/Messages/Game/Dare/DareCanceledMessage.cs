namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareCanceledMessage : NetworkMessage
    {
        public override short Protocol => 6679;

        public double DareId { get; set; }

        public DareCanceledMessage()
        {
        }

        public DareCanceledMessage(double dareId)
        {
            DareId = dareId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
        }
    }
}