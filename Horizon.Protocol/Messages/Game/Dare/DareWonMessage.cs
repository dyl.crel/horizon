namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareWonMessage : NetworkMessage
    {
        public override short Protocol => 6681;

        public double DareId { get; set; }

        public DareWonMessage()
        {
        }

        public DareWonMessage(double dareId)
        {
            DareId = dareId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
        }
    }
}