using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareInformationsMessage : NetworkMessage
    {
        public override short Protocol => 6656;

        public DareInformations DareFixedInfos { get; set; }

        public DareVersatileInformations DareVersatilesInfos { get; set; }

        public DareInformationsMessage()
        {
        }

        public DareInformationsMessage(DareInformations dareFixedInfos, DareVersatileInformations dareVersatilesInfos)
        {
            DareFixedInfos = dareFixedInfos;
            DareVersatilesInfos = dareVersatilesInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            DareFixedInfos.Serialize(writer);
            DareVersatilesInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareFixedInfos = new DareInformations();
            DareFixedInfos.Deserialize(reader);
            DareVersatilesInfos = new DareVersatileInformations();
            DareVersatilesInfos.Deserialize(reader);
        }
    }
}