using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareCreatedListMessage : NetworkMessage
    {
        public override short Protocol => 6663;

        public DareInformations[] DaresFixedInfos { get; set; }

        public DareVersatileInformations[] DaresVersatilesInfos { get; set; }

        public DareCreatedListMessage()
        {
        }

        public DareCreatedListMessage(DareInformations[] daresFixedInfos, DareVersatileInformations[] daresVersatilesInfos)
        {
            DaresFixedInfos = daresFixedInfos;
            DaresVersatilesInfos = daresVersatilesInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(DaresFixedInfos.Length);
            for (var i = 0; i < DaresFixedInfos.Length; i++)
            {
                DaresFixedInfos[i].Serialize(writer);
            }
            writer.WriteShort(DaresVersatilesInfos.Length);
            for (var i = 0; i < DaresVersatilesInfos.Length; i++)
            {
                DaresVersatilesInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DaresFixedInfos = new DareInformations[reader.ReadShort()];
            for (var i = 0; i < DaresFixedInfos.Length; i++)
            {
                DaresFixedInfos[i] = new DareInformations();
                DaresFixedInfos[i].Deserialize(reader);
            }
            DaresVersatilesInfos = new DareVersatileInformations[reader.ReadShort()];
            for (var i = 0; i < DaresVersatilesInfos.Length; i++)
            {
                DaresVersatilesInfos[i] = new DareVersatileInformations();
                DaresVersatilesInfos[i].Deserialize(reader);
            }
        }
    }
}