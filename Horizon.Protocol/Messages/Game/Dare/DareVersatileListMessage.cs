using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareVersatileListMessage : NetworkMessage
    {
        public override short Protocol => 6657;

        public DareVersatileInformations[] Dares { get; set; }

        public DareVersatileListMessage()
        {
        }

        public DareVersatileListMessage(DareVersatileInformations[] dares)
        {
            Dares = dares;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Dares.Length);
            for (var i = 0; i < Dares.Length; i++)
            {
                Dares[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Dares = new DareVersatileInformations[reader.ReadShort()];
            for (var i = 0; i < Dares.Length; i++)
            {
                Dares[i] = new DareVersatileInformations();
                Dares[i].Deserialize(reader);
            }
        }
    }
}