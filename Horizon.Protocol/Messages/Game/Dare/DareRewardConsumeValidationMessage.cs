namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareRewardConsumeValidationMessage : NetworkMessage
    {
        public override short Protocol => 6675;

        public double DareId { get; set; }

        public byte Type { get; set; }

        public DareRewardConsumeValidationMessage()
        {
        }

        public DareRewardConsumeValidationMessage(double dareId, byte type)
        {
            DareId = dareId;
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
            Type = reader.ReadByte();
        }
    }
}