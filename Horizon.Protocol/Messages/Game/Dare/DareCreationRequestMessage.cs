using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareCreationRequestMessage : NetworkMessage
    {
        public override short Protocol => 6665;

        public bool IsPrivate { get; set; }

        public bool IsForGuild { get; set; }

        public bool IsForAlliance { get; set; }

        public bool NeedNotifications { get; set; }

        public long SubscriptionFee { get; set; }

        public long Jackpot { get; set; }

        public short MaxCountWinners { get; set; }

        public int DelayBeforeStart { get; set; }

        public int Duration { get; set; }

        public DareCriteria[] Criterions { get; set; }

        public DareCreationRequestMessage()
        {
        }

        public DareCreationRequestMessage(bool isPrivate, bool isForGuild, bool isForAlliance, bool needNotifications, long subscriptionFee, long jackpot, short maxCountWinners, int delayBeforeStart, int duration, DareCriteria[] criterions)
        {
            IsPrivate = isPrivate;
            IsForGuild = isForGuild;
            IsForAlliance = isForAlliance;
            NeedNotifications = needNotifications;
            SubscriptionFee = subscriptionFee;
            Jackpot = jackpot;
            MaxCountWinners = maxCountWinners;
            DelayBeforeStart = delayBeforeStart;
            Duration = duration;
            Criterions = criterions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, IsPrivate);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsForGuild);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, IsForAlliance);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, NeedNotifications);
            writer.WriteByte(flag0);
            writer.WriteVarLong(SubscriptionFee);
            writer.WriteVarLong(Jackpot);
            writer.WriteShort(MaxCountWinners);
            writer.WriteInt(DelayBeforeStart);
            writer.WriteInt(Duration);
            writer.WriteShort(Criterions.Length);
            for (var i = 0; i < Criterions.Length; i++)
            {
                Criterions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            IsPrivate = BooleanByteWrapper.GetFlag(flag0, 0);
            IsForGuild = BooleanByteWrapper.GetFlag(flag0, 1);
            IsForAlliance = BooleanByteWrapper.GetFlag(flag0, 2);
            NeedNotifications = BooleanByteWrapper.GetFlag(flag0, 3);
            SubscriptionFee = reader.ReadVarLong();
            Jackpot = reader.ReadVarLong();
            MaxCountWinners = reader.ReadShort();
            DelayBeforeStart = reader.ReadInt();
            Duration = reader.ReadInt();
            Criterions = new DareCriteria[reader.ReadShort()];
            for (var i = 0; i < Criterions.Length; i++)
            {
                Criterions[i] = new DareCriteria();
                Criterions[i].Deserialize(reader);
            }
        }
    }
}