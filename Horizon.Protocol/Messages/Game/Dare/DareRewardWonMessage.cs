using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareRewardWonMessage : NetworkMessage
    {
        public override short Protocol => 6678;

        public DareReward Reward { get; set; }

        public DareRewardWonMessage()
        {
        }

        public DareRewardWonMessage(DareReward reward)
        {
            Reward = reward;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Reward.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reward = new DareReward();
            Reward.Deserialize(reader);
        }
    }
}