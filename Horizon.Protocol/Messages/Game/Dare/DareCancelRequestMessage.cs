namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareCancelRequestMessage : NetworkMessage
    {
        public override short Protocol => 6680;

        public double DareId { get; set; }

        public DareCancelRequestMessage()
        {
        }

        public DareCancelRequestMessage(double dareId)
        {
            DareId = dareId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
        }
    }
}