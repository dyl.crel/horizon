namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareInformationsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6659;

        public double DareId { get; set; }

        public DareInformationsRequestMessage()
        {
        }

        public DareInformationsRequestMessage(double dareId)
        {
            DareId = dareId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
        }
    }
}