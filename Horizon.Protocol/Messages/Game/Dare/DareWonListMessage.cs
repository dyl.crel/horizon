namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareWonListMessage : NetworkMessage
    {
        public override short Protocol => 6682;

        public double[] DareId { get; set; }

        public DareWonListMessage()
        {
        }

        public DareWonListMessage(double[] dareId)
        {
            DareId = dareId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(DareId.Length);
            for (var i = 0; i < DareId.Length; i++)
            {
                writer.WriteDouble(DareId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = new double[reader.ReadShort()];
            for (var i = 0; i < DareId.Length; i++)
            {
                DareId[i] = reader.ReadDouble();
            }
        }
    }
}