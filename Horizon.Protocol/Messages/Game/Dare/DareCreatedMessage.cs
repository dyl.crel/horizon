using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareCreatedMessage : NetworkMessage
    {
        public override short Protocol => 6668;

        public DareInformations DareInfos { get; set; }

        public bool NeedNotifications { get; set; }

        public DareCreatedMessage()
        {
        }

        public DareCreatedMessage(DareInformations dareInfos, bool needNotifications)
        {
            DareInfos = dareInfos;
            NeedNotifications = needNotifications;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            DareInfos.Serialize(writer);
            writer.WriteBoolean(NeedNotifications);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareInfos = new DareInformations();
            DareInfos.Deserialize(reader);
            NeedNotifications = reader.ReadBoolean();
        }
    }
}