namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareErrorMessage : NetworkMessage
    {
        public override short Protocol => 6667;

        public byte Error { get; set; }

        public DareErrorMessage()
        {
        }

        public DareErrorMessage(byte error)
        {
            Error = error;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Error);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Error = reader.ReadByte();
        }
    }
}