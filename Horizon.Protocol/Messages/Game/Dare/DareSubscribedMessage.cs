using Horizon.Protocol.Types.Game.Dare;

namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareSubscribedMessage : NetworkMessage
    {
        public override short Protocol => 6660;

        public bool Success { get; set; }

        public bool Subscribe { get; set; }

        public double DareId { get; set; }

        public DareVersatileInformations DareVersatilesInfos { get; set; }

        public DareSubscribedMessage()
        {
        }

        public DareSubscribedMessage(bool success, bool subscribe, double dareId, DareVersatileInformations dareVersatilesInfos)
        {
            Success = success;
            Subscribe = subscribe;
            DareId = dareId;
            DareVersatilesInfos = dareVersatilesInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Success);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Subscribe);
            writer.WriteByte(flag0);
            writer.WriteDouble(DareId);
            DareVersatilesInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag0, 0);
            Subscribe = BooleanByteWrapper.GetFlag(flag0, 1);
            DareId = reader.ReadDouble();
            DareVersatilesInfos = new DareVersatileInformations();
            DareVersatilesInfos.Deserialize(reader);
        }
    }
}