namespace Horizon.Protocol.Messages.Game.Dare
{
    public class DareSubscribeRequestMessage : NetworkMessage
    {
        public override short Protocol => 6666;

        public double DareId { get; set; }

        public bool Subscribe { get; set; }

        public DareSubscribeRequestMessage()
        {
        }

        public DareSubscribeRequestMessage(double dareId, bool subscribe)
        {
            DareId = dareId;
            Subscribe = subscribe;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
            writer.WriteBoolean(Subscribe);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
            Subscribe = reader.ReadBoolean();
        }
    }
}