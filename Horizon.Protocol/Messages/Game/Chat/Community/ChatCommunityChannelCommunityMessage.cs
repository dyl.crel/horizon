namespace Horizon.Protocol.Messages.Game.Chat.Community
{
    public class ChatCommunityChannelCommunityMessage : NetworkMessage
    {
        public override short Protocol => 6730;

        public short CommunityId { get; set; }

        public ChatCommunityChannelCommunityMessage()
        {
        }

        public ChatCommunityChannelCommunityMessage(short communityId)
        {
            CommunityId = communityId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CommunityId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CommunityId = reader.ReadShort();
        }
    }
}