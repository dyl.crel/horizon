namespace Horizon.Protocol.Messages.Game.Chat.Community
{
    public class ChatCommunityChannelSetCommunityRequestMessage : NetworkMessage
    {
        public override short Protocol => 6729;

        public short CommunityId { get; set; }

        public ChatCommunityChannelSetCommunityRequestMessage()
        {
        }

        public ChatCommunityChannelSetCommunityRequestMessage(short communityId)
        {
            CommunityId = communityId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CommunityId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CommunityId = reader.ReadShort();
        }
    }
}