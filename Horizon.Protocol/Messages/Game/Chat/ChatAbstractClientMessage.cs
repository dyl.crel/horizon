namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatAbstractClientMessage : NetworkMessage
    {
        public override short Protocol => 850;

        public string Content { get; set; }

        public ChatAbstractClientMessage()
        {
        }

        public ChatAbstractClientMessage(string content)
        {
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Content = reader.ReadUTF();
        }
    }
}