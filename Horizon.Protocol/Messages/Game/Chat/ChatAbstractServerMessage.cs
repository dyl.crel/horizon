namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatAbstractServerMessage : NetworkMessage
    {
        public override short Protocol => 880;

        public byte Channel { get; set; }

        public string Content { get; set; }

        public int Timestamp { get; set; }

        public string Fingerprint { get; set; }

        public ChatAbstractServerMessage()
        {
        }

        public ChatAbstractServerMessage(byte channel, string content, int timestamp, string fingerprint)
        {
            Channel = channel;
            Content = content;
            Timestamp = timestamp;
            Fingerprint = fingerprint;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Channel);
            writer.WriteUTF(Content);
            writer.WriteInt(Timestamp);
            writer.WriteUTF(Fingerprint);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Channel = reader.ReadByte();
            Content = reader.ReadUTF();
            Timestamp = reader.ReadInt();
            Fingerprint = reader.ReadUTF();
        }
    }
}