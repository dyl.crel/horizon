namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatClientMultiMessage : ChatAbstractClientMessage
    {
        public override short Protocol => 861;

        public byte Channel { get; set; }

        public ChatClientMultiMessage()
        {
        }

        public ChatClientMultiMessage(string content, byte channel) : base(content)
        {
            Channel = channel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Channel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Channel = reader.ReadByte();
        }
    }
}