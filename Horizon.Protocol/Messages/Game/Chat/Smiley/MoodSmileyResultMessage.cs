namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class MoodSmileyResultMessage : NetworkMessage
    {
        public override short Protocol => 6196;

        public byte ResultCode { get; set; }

        public short SmileyId { get; set; }

        public MoodSmileyResultMessage()
        {
        }

        public MoodSmileyResultMessage(byte resultCode, short smileyId)
        {
            ResultCode = resultCode;
            SmileyId = smileyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ResultCode);
            writer.WriteVarShort(SmileyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ResultCode = reader.ReadByte();
            SmileyId = reader.ReadVarShort();
        }
    }
}