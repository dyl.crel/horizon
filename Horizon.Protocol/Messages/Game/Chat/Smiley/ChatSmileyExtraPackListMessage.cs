namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class ChatSmileyExtraPackListMessage : NetworkMessage
    {
        public override short Protocol => 6596;

        public byte[] PackIds { get; set; }

        public ChatSmileyExtraPackListMessage()
        {
        }

        public ChatSmileyExtraPackListMessage(byte[] packIds)
        {
            PackIds = packIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PackIds.Length);
            for (var i = 0; i < PackIds.Length; i++)
            {
                writer.WriteByte(PackIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PackIds = new byte[reader.ReadShort()];
            for (var i = 0; i < PackIds.Length; i++)
            {
                PackIds[i] = reader.ReadByte();
            }
        }
    }
}