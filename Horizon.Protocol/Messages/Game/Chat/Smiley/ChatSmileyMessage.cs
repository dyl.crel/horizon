namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class ChatSmileyMessage : NetworkMessage
    {
        public override short Protocol => 801;

        public double EntityId { get; set; }

        public short SmileyId { get; set; }

        public int AccountId { get; set; }

        public ChatSmileyMessage()
        {
        }

        public ChatSmileyMessage(double entityId, short smileyId, int accountId)
        {
            EntityId = entityId;
            SmileyId = smileyId;
            AccountId = accountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(EntityId);
            writer.WriteVarShort(SmileyId);
            writer.WriteInt(AccountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EntityId = reader.ReadDouble();
            SmileyId = reader.ReadVarShort();
            AccountId = reader.ReadInt();
        }
    }
}