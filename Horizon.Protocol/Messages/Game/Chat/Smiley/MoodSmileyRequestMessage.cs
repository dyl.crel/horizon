namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class MoodSmileyRequestMessage : NetworkMessage
    {
        public override short Protocol => 6192;

        public short SmileyId { get; set; }

        public MoodSmileyRequestMessage()
        {
        }

        public MoodSmileyRequestMessage(short smileyId)
        {
            SmileyId = smileyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SmileyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SmileyId = reader.ReadVarShort();
        }
    }
}