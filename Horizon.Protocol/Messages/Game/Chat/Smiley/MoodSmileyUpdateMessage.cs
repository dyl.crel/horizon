namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class MoodSmileyUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6388;

        public int AccountId { get; set; }

        public long PlayerId { get; set; }

        public short SmileyId { get; set; }

        public MoodSmileyUpdateMessage()
        {
        }

        public MoodSmileyUpdateMessage(int accountId, long playerId, short smileyId)
        {
            AccountId = accountId;
            PlayerId = playerId;
            SmileyId = smileyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteVarLong(PlayerId);
            writer.WriteVarShort(SmileyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AccountId = reader.ReadInt();
            PlayerId = reader.ReadVarLong();
            SmileyId = reader.ReadVarShort();
        }
    }
}