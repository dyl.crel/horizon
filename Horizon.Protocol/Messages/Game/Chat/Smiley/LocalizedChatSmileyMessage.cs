namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class LocalizedChatSmileyMessage : ChatSmileyMessage
    {
        public override short Protocol => 6185;

        public short CellId { get; set; }

        public LocalizedChatSmileyMessage()
        {
        }

        public LocalizedChatSmileyMessage(double entityId, short smileyId, int accountId, short cellId) : base(entityId, smileyId, accountId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CellId = reader.ReadVarShort();
        }
    }
}