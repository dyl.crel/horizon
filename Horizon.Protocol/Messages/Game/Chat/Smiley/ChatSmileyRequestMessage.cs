namespace Horizon.Protocol.Messages.Game.Chat.Smiley
{
    public class ChatSmileyRequestMessage : NetworkMessage
    {
        public override short Protocol => 800;

        public short SmileyId { get; set; }

        public ChatSmileyRequestMessage()
        {
        }

        public ChatSmileyRequestMessage(short smileyId)
        {
            SmileyId = smileyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SmileyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SmileyId = reader.ReadVarShort();
        }
    }
}