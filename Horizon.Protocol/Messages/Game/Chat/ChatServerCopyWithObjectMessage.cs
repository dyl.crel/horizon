using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatServerCopyWithObjectMessage : ChatServerCopyMessage
    {
        public override short Protocol => 884;

        public ObjectItem[] Objects { get; set; }

        public ChatServerCopyWithObjectMessage()
        {
        }

        public ChatServerCopyWithObjectMessage(byte channel, string content, int timestamp, string fingerprint, long receiverId, string receiverName, ObjectItem[] objects) : base(channel, content, timestamp, fingerprint, receiverId, receiverName)
        {
            Objects = objects;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Objects.Length);
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Objects = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i] = new ObjectItem();
                Objects[i].Deserialize(reader);
            }
        }
    }
}