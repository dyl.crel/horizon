namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatAdminServerMessage : ChatServerMessage
    {
        public override short Protocol => 6135;

        public ChatAdminServerMessage()
        {
        }

        public ChatAdminServerMessage(byte channel, string content, int timestamp, string fingerprint, double senderId, string senderName, string prefix, int senderAccountId) : base(channel, content, timestamp, fingerprint, senderId, senderName, prefix, senderAccountId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}