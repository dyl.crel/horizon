namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatServerCopyMessage : ChatAbstractServerMessage
    {
        public override short Protocol => 882;

        public long ReceiverId { get; set; }

        public string ReceiverName { get; set; }

        public ChatServerCopyMessage()
        {
        }

        public ChatServerCopyMessage(byte channel, string content, int timestamp, string fingerprint, long receiverId, string receiverName) : base(channel, content, timestamp, fingerprint)
        {
            ReceiverId = receiverId;
            ReceiverName = receiverName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(ReceiverId);
            writer.WriteUTF(ReceiverName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ReceiverId = reader.ReadVarLong();
            ReceiverName = reader.ReadUTF();
        }
    }
}