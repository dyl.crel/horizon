namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatErrorMessage : NetworkMessage
    {
        public override short Protocol => 870;

        public byte Reason { get; set; }

        public ChatErrorMessage()
        {
        }

        public ChatErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}