namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatServerMessage : ChatAbstractServerMessage
    {
        public override short Protocol => 881;

        public double SenderId { get; set; }

        public string SenderName { get; set; }

        public string Prefix { get; set; }

        public int SenderAccountId { get; set; }

        public ChatServerMessage()
        {
        }

        public ChatServerMessage(byte channel, string content, int timestamp, string fingerprint, double senderId, string senderName, string prefix, int senderAccountId) : base(channel, content, timestamp, fingerprint)
        {
            SenderId = senderId;
            SenderName = senderName;
            Prefix = prefix;
            SenderAccountId = senderAccountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(SenderId);
            writer.WriteUTF(SenderName);
            writer.WriteUTF(Prefix);
            writer.WriteInt(SenderAccountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SenderId = reader.ReadDouble();
            SenderName = reader.ReadUTF();
            Prefix = reader.ReadUTF();
            SenderAccountId = reader.ReadInt();
        }
    }
}