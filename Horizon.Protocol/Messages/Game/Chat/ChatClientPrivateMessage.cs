namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatClientPrivateMessage : ChatAbstractClientMessage
    {
        public override short Protocol => 851;

        public string Receiver { get; set; }

        public ChatClientPrivateMessage()
        {
        }

        public ChatClientPrivateMessage(string content, string receiver) : base(content)
        {
            Receiver = receiver;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Receiver);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Receiver = reader.ReadUTF();
        }
    }
}