using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatClientMultiWithObjectMessage : ChatClientMultiMessage
    {
        public override short Protocol => 862;

        public ObjectItem[] Objects { get; set; }

        public ChatClientMultiWithObjectMessage()
        {
        }

        public ChatClientMultiWithObjectMessage(string content, byte channel, ObjectItem[] objects) : base(content, channel)
        {
            Objects = objects;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Objects.Length);
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Objects = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i] = new ObjectItem();
                Objects[i].Deserialize(reader);
            }
        }
    }
}