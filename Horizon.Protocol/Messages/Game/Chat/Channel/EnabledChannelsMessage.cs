namespace Horizon.Protocol.Messages.Game.Chat.Channel
{
    public class EnabledChannelsMessage : NetworkMessage
    {
        public override short Protocol => 892;

        public byte[] Channels { get; set; }

        public byte[] Disallowed { get; set; }

        public EnabledChannelsMessage()
        {
        }

        public EnabledChannelsMessage(byte[] channels, byte[] disallowed)
        {
            Channels = channels;
            Disallowed = disallowed;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Channels.Length);
            for (var i = 0; i < Channels.Length; i++)
            {
                writer.WriteByte(Channels[i]);
            }
            writer.WriteShort(Disallowed.Length);
            for (var i = 0; i < Disallowed.Length; i++)
            {
                writer.WriteByte(Disallowed[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Channels = new byte[reader.ReadShort()];
            for (var i = 0; i < Channels.Length; i++)
            {
                Channels[i] = reader.ReadByte();
            }
            Disallowed = new byte[reader.ReadShort()];
            for (var i = 0; i < Disallowed.Length; i++)
            {
                Disallowed[i] = reader.ReadByte();
            }
        }
    }
}