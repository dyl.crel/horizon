namespace Horizon.Protocol.Messages.Game.Chat.Channel
{
    public class ChannelEnablingChangeMessage : NetworkMessage
    {
        public override short Protocol => 891;

        public byte Channel { get; set; }

        public bool Enable { get; set; }

        public ChannelEnablingChangeMessage()
        {
        }

        public ChannelEnablingChangeMessage(byte channel, bool enable)
        {
            Channel = channel;
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Channel);
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Channel = reader.ReadByte();
            Enable = reader.ReadBoolean();
        }
    }
}