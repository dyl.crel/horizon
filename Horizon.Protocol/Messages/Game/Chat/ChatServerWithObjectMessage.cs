using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Chat
{
    public class ChatServerWithObjectMessage : ChatServerMessage
    {
        public override short Protocol => 883;

        public ObjectItem[] Objects { get; set; }

        public ChatServerWithObjectMessage()
        {
        }

        public ChatServerWithObjectMessage(byte channel, string content, int timestamp, string fingerprint, double senderId, string senderName, string prefix, int senderAccountId, ObjectItem[] objects) : base(channel, content, timestamp, fingerprint, senderId, senderName, prefix, senderAccountId)
        {
            Objects = objects;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Objects.Length);
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Objects = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i] = new ObjectItem();
                Objects[i].Deserialize(reader);
            }
        }
    }
}