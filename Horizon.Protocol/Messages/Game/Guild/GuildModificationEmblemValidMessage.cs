using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildModificationEmblemValidMessage : NetworkMessage
    {
        public override short Protocol => 6328;

        public GuildEmblem GuildEmblem { get; set; }

        public GuildModificationEmblemValidMessage()
        {
        }

        public GuildModificationEmblemValidMessage(GuildEmblem guildEmblem)
        {
            GuildEmblem = guildEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            GuildEmblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildEmblem = new GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
    }
}