namespace Horizon.Protocol.Messages.Game.Guild
{
    public class ChallengeFightJoinRefusedMessage : NetworkMessage
    {
        public override short Protocol => 5908;

        public long PlayerId { get; set; }

        public byte Reason { get; set; }

        public ChallengeFightJoinRefusedMessage()
        {
        }

        public ChallengeFightJoinRefusedMessage(long playerId, byte reason)
        {
            PlayerId = playerId;
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
            Reason = reader.ReadByte();
        }
    }
}