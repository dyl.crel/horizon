using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildMotdSetRequestMessage : SocialNoticeSetRequestMessage
    {
        public override short Protocol => 6588;

        public string Content { get; set; }

        public GuildMotdSetRequestMessage()
        {
        }

        public GuildMotdSetRequestMessage(string content)
        {
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Content = reader.ReadUTF();
        }
    }
}