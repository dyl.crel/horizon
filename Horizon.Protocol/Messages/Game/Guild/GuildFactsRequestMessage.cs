namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildFactsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6404;

        public int GuildId { get; set; }

        public GuildFactsRequestMessage()
        {
        }

        public GuildFactsRequestMessage(int guildId)
        {
            GuildId = guildId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(GuildId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildId = reader.ReadVarInt();
        }
    }
}