namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildKickRequestMessage : NetworkMessage
    {
        public override short Protocol => 5887;

        public long KickedId { get; set; }

        public GuildKickRequestMessage()
        {
        }

        public GuildKickRequestMessage(long kickedId)
        {
            KickedId = kickedId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(KickedId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            KickedId = reader.ReadVarLong();
        }
    }
}