using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildCreationValidMessage : NetworkMessage
    {
        public override short Protocol => 5546;

        public string GuildName { get; set; }

        public GuildEmblem GuildEmblem { get; set; }

        public GuildCreationValidMessage()
        {
        }

        public GuildCreationValidMessage(string guildName, GuildEmblem guildEmblem)
        {
            GuildName = guildName;
            GuildEmblem = guildEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(GuildName);
            GuildEmblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildName = reader.ReadUTF();
            GuildEmblem = new GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
    }
}