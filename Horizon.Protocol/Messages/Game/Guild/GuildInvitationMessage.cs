namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInvitationMessage : NetworkMessage
    {
        public override short Protocol => 5551;

        public long TargetId { get; set; }

        public GuildInvitationMessage()
        {
        }

        public GuildInvitationMessage(long targetId)
        {
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TargetId = reader.ReadVarLong();
        }
    }
}