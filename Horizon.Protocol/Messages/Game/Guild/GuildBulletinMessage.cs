using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildBulletinMessage : BulletinMessage
    {
        public override short Protocol => 6689;

        public GuildBulletinMessage()
        {
        }

        public GuildBulletinMessage(string content, int timestamp, long memberId, string memberName, int lastNotifiedTimestamp) : base(content, timestamp, memberId, memberName, lastNotifiedTimestamp)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}