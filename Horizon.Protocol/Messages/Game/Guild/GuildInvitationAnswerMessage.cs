namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInvitationAnswerMessage : NetworkMessage
    {
        public override short Protocol => 5556;

        public bool Accept { get; set; }

        public GuildInvitationAnswerMessage()
        {
        }

        public GuildInvitationAnswerMessage(bool accept)
        {
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Accept = reader.ReadBoolean();
        }
    }
}