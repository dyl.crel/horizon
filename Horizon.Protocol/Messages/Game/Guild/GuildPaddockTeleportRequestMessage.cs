namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildPaddockTeleportRequestMessage : NetworkMessage
    {
        public override short Protocol => 5957;

        public double PaddockId { get; set; }

        public GuildPaddockTeleportRequestMessage()
        {
        }

        public GuildPaddockTeleportRequestMessage(double paddockId)
        {
            PaddockId = paddockId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(PaddockId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PaddockId = reader.ReadDouble();
        }
    }
}