namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildCreationResultMessage : NetworkMessage
    {
        public override short Protocol => 5554;

        public byte Result { get; set; }

        public GuildCreationResultMessage()
        {
        }

        public GuildCreationResultMessage(byte result)
        {
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Result = reader.ReadByte();
        }
    }
}