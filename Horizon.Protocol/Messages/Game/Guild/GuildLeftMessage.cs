namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildLeftMessage : NetworkMessage
    {
        public override short Protocol => 5562;

        public GuildLeftMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}