using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildJoinedMessage : NetworkMessage
    {
        public override short Protocol => 5564;

        public GuildInformations GuildInfo { get; set; }

        public int MemberRights { get; set; }

        public GuildJoinedMessage()
        {
        }

        public GuildJoinedMessage(GuildInformations guildInfo, int memberRights)
        {
            GuildInfo = guildInfo;
            MemberRights = memberRights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            GuildInfo.Serialize(writer);
            writer.WriteVarInt(MemberRights);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildInfo = new GuildInformations();
            GuildInfo.Deserialize(reader);
            MemberRights = reader.ReadVarInt();
        }
    }
}