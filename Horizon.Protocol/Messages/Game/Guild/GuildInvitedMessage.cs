using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInvitedMessage : NetworkMessage
    {
        public override short Protocol => 5552;

        public long RecruterId { get; set; }

        public string RecruterName { get; set; }

        public BasicGuildInformations GuildInfo { get; set; }

        public GuildInvitedMessage()
        {
        }

        public GuildInvitedMessage(long recruterId, string recruterName, BasicGuildInformations guildInfo)
        {
            RecruterId = recruterId;
            RecruterName = recruterName;
            GuildInfo = guildInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(RecruterId);
            writer.WriteUTF(RecruterName);
            GuildInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RecruterId = reader.ReadVarLong();
            RecruterName = reader.ReadUTF();
            GuildInfo = new BasicGuildInformations();
            GuildInfo.Deserialize(reader);
        }
    }
}