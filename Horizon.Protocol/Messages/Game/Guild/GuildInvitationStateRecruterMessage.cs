namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInvitationStateRecruterMessage : NetworkMessage
    {
        public override short Protocol => 5563;

        public string RecrutedName { get; set; }

        public byte InvitationState { get; set; }

        public GuildInvitationStateRecruterMessage()
        {
        }

        public GuildInvitationStateRecruterMessage(string recrutedName, byte invitationState)
        {
            RecrutedName = recrutedName;
            InvitationState = invitationState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(RecrutedName);
            writer.WriteByte(InvitationState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RecrutedName = reader.ReadUTF();
            InvitationState = reader.ReadByte();
        }
    }
}