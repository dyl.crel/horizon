namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInfosUpgradeMessage : NetworkMessage
    {
        public override short Protocol => 5636;

        public byte MaxTaxCollectorsCount { get; set; }

        public byte TaxCollectorsCount { get; set; }

        public short TaxCollectorLifePoints { get; set; }

        public short TaxCollectorDamagesBonuses { get; set; }

        public short TaxCollectorPods { get; set; }

        public short TaxCollectorProspecting { get; set; }

        public short TaxCollectorWisdom { get; set; }

        public short BoostPoints { get; set; }

        public short[] SpellId { get; set; }

        public short[] SpellLevel { get; set; }

        public GuildInfosUpgradeMessage()
        {
        }

        public GuildInfosUpgradeMessage(byte maxTaxCollectorsCount, byte taxCollectorsCount, short taxCollectorLifePoints, short taxCollectorDamagesBonuses, short taxCollectorPods, short taxCollectorProspecting, short taxCollectorWisdom, short boostPoints, short[] spellId, short[] spellLevel)
        {
            MaxTaxCollectorsCount = maxTaxCollectorsCount;
            TaxCollectorsCount = taxCollectorsCount;
            TaxCollectorLifePoints = taxCollectorLifePoints;
            TaxCollectorDamagesBonuses = taxCollectorDamagesBonuses;
            TaxCollectorPods = taxCollectorPods;
            TaxCollectorProspecting = taxCollectorProspecting;
            TaxCollectorWisdom = taxCollectorWisdom;
            BoostPoints = boostPoints;
            SpellId = spellId;
            SpellLevel = spellLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(MaxTaxCollectorsCount);
            writer.WriteByte(TaxCollectorsCount);
            writer.WriteVarShort(TaxCollectorLifePoints);
            writer.WriteVarShort(TaxCollectorDamagesBonuses);
            writer.WriteVarShort(TaxCollectorPods);
            writer.WriteVarShort(TaxCollectorProspecting);
            writer.WriteVarShort(TaxCollectorWisdom);
            writer.WriteVarShort(BoostPoints);
            writer.WriteShort(SpellId.Length);
            for (var i = 0; i < SpellId.Length; i++)
            {
                writer.WriteVarShort(SpellId[i]);
            }
            writer.WriteShort(SpellLevel.Length);
            for (var i = 0; i < SpellLevel.Length; i++)
            {
                writer.WriteShort(SpellLevel[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MaxTaxCollectorsCount = reader.ReadByte();
            TaxCollectorsCount = reader.ReadByte();
            TaxCollectorLifePoints = reader.ReadVarShort();
            TaxCollectorDamagesBonuses = reader.ReadVarShort();
            TaxCollectorPods = reader.ReadVarShort();
            TaxCollectorProspecting = reader.ReadVarShort();
            TaxCollectorWisdom = reader.ReadVarShort();
            BoostPoints = reader.ReadVarShort();
            SpellId = new short[reader.ReadShort()];
            for (var i = 0; i < SpellId.Length; i++)
            {
                SpellId[i] = reader.ReadVarShort();
            }
            SpellLevel = new short[reader.ReadShort()];
            for (var i = 0; i < SpellLevel.Length; i++)
            {
                SpellLevel[i] = reader.ReadShort();
            }
        }
    }
}