using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GuildFightPlayersEnemiesListMessage : NetworkMessage
    {
        public override short Protocol => 5928;

        public double FightId { get; set; }

        public CharacterMinimalPlusLookInformations[] PlayerInfo { get; set; }

        public GuildFightPlayersEnemiesListMessage()
        {
        }

        public GuildFightPlayersEnemiesListMessage(double fightId, CharacterMinimalPlusLookInformations[] playerInfo)
        {
            FightId = fightId;
            PlayerInfo = playerInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(FightId);
            writer.WriteShort(PlayerInfo.Length);
            for (var i = 0; i < PlayerInfo.Length; i++)
            {
                PlayerInfo[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadDouble();
            PlayerInfo = new CharacterMinimalPlusLookInformations[reader.ReadShort()];
            for (var i = 0; i < PlayerInfo.Length; i++)
            {
                PlayerInfo[i] = new CharacterMinimalPlusLookInformations();
                PlayerInfo[i].Deserialize(reader);
            }
        }
    }
}