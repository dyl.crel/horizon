namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GuildFightJoinRequestMessage : NetworkMessage
    {
        public override short Protocol => 5717;

        public double TaxCollectorId { get; set; }

        public GuildFightJoinRequestMessage()
        {
        }

        public GuildFightJoinRequestMessage(double taxCollectorId)
        {
            TaxCollectorId = taxCollectorId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(TaxCollectorId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TaxCollectorId = reader.ReadDouble();
        }
    }
}