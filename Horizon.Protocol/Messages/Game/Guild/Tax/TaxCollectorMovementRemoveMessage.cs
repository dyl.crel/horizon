namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorMovementRemoveMessage : NetworkMessage
    {
        public override short Protocol => 5915;

        public double CollectorId { get; set; }

        public TaxCollectorMovementRemoveMessage()
        {
        }

        public TaxCollectorMovementRemoveMessage(double collectorId)
        {
            CollectorId = collectorId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(CollectorId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CollectorId = reader.ReadDouble();
        }
    }
}