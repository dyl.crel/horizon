namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GuildFightLeaveRequestMessage : NetworkMessage
    {
        public override short Protocol => 5715;

        public double TaxCollectorId { get; set; }

        public long CharacterId { get; set; }

        public GuildFightLeaveRequestMessage()
        {
        }

        public GuildFightLeaveRequestMessage(double taxCollectorId, long characterId)
        {
            TaxCollectorId = taxCollectorId;
            CharacterId = characterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(TaxCollectorId);
            writer.WriteVarLong(CharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TaxCollectorId = reader.ReadDouble();
            CharacterId = reader.ReadVarLong();
        }
    }
}