namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GameRolePlayTaxCollectorFightRequestMessage : NetworkMessage
    {
        public override short Protocol => 5954;

        public GameRolePlayTaxCollectorFightRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}