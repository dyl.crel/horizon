namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorStateUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6455;

        public double UniqueId { get; set; }

        public byte State { get; set; }

        public TaxCollectorStateUpdateMessage()
        {
        }

        public TaxCollectorStateUpdateMessage(double uniqueId, byte state)
        {
            UniqueId = uniqueId;
            State = state;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(UniqueId);
            writer.WriteByte(State);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            UniqueId = reader.ReadDouble();
            State = reader.ReadByte();
        }
    }
}