using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GuildFightPlayersHelpersJoinMessage : NetworkMessage
    {
        public override short Protocol => 5720;

        public double FightId { get; set; }

        public CharacterMinimalPlusLookInformations PlayerInfo { get; set; }

        public GuildFightPlayersHelpersJoinMessage()
        {
        }

        public GuildFightPlayersHelpersJoinMessage(double fightId, CharacterMinimalPlusLookInformations playerInfo)
        {
            FightId = fightId;
            PlayerInfo = playerInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(FightId);
            PlayerInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadDouble();
            PlayerInfo = new CharacterMinimalPlusLookInformations();
            PlayerInfo.Deserialize(reader);
        }
    }
}