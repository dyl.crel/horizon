using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorListMessage : AbstractTaxCollectorListMessage
    {
        public override short Protocol => 5930;

        public byte NbcollectorMax { get; set; }

        public TaxCollectorFightersInformation[] FightersInformations { get; set; }

        public byte InfoType { get; set; }

        public TaxCollectorListMessage()
        {
        }

        public TaxCollectorListMessage(TaxCollectorInformations[] informations, byte nbcollectorMax, TaxCollectorFightersInformation[] fightersInformations, byte infoType) : base(informations)
        {
            NbcollectorMax = nbcollectorMax;
            FightersInformations = fightersInformations;
            InfoType = infoType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(NbcollectorMax);
            writer.WriteShort(FightersInformations.Length);
            for (var i = 0; i < FightersInformations.Length; i++)
            {
                FightersInformations[i].Serialize(writer);
            }
            writer.WriteByte(InfoType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NbcollectorMax = reader.ReadByte();
            FightersInformations = new TaxCollectorFightersInformation[reader.ReadShort()];
            for (var i = 0; i < FightersInformations.Length; i++)
            {
                FightersInformations[i] = new TaxCollectorFightersInformation();
                FightersInformations[i].Deserialize(reader);
            }
            InfoType = reader.ReadByte();
        }
    }
}