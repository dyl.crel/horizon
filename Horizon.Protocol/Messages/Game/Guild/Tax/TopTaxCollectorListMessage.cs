using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TopTaxCollectorListMessage : AbstractTaxCollectorListMessage
    {
        public override short Protocol => 6565;

        public bool IsDungeon { get; set; }

        public TopTaxCollectorListMessage()
        {
        }

        public TopTaxCollectorListMessage(TaxCollectorInformations[] informations, bool isDungeon) : base(informations)
        {
            IsDungeon = isDungeon;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(IsDungeon);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            IsDungeon = reader.ReadBoolean();
        }
    }
}