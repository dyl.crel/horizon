using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class AbstractTaxCollectorListMessage : NetworkMessage
    {
        public override short Protocol => 6568;

        public TaxCollectorInformations[] Informations { get; set; }

        public AbstractTaxCollectorListMessage()
        {
        }

        public AbstractTaxCollectorListMessage(TaxCollectorInformations[] informations)
        {
            Informations = informations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Informations.Length);
            for (var i = 0; i < Informations.Length; i++)
            {
                writer.WriteShort(Informations[i].Protocol);
                Informations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Informations = new TaxCollectorInformations[reader.ReadShort()];
            for (var i = 0; i < Informations.Length; i++)
            {
                Informations[i] = ProtocolTypesManager.Instance<TaxCollectorInformations>(reader.ReadShort());
                Informations[i].Deserialize(reader);
            }
        }
    }
}