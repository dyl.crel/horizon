using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorAttackedMessage : NetworkMessage
    {
        public override short Protocol => 5918;

        public short FirstNameId { get; set; }

        public short LastNameId { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public BasicGuildInformations Guild { get; set; }

        public TaxCollectorAttackedMessage()
        {
        }

        public TaxCollectorAttackedMessage(short firstNameId, short lastNameId, short worldX, short worldY, double mapId, short subAreaId, BasicGuildInformations guild)
        {
            FirstNameId = firstNameId;
            LastNameId = lastNameId;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            Guild = guild;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            Guild.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FirstNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            Guild = new BasicGuildInformations();
            Guild.Deserialize(reader);
        }
    }
}