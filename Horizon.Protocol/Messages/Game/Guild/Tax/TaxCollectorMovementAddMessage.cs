using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorMovementAddMessage : NetworkMessage
    {
        public override short Protocol => 5917;

        public TaxCollectorInformations Informations { get; set; }

        public TaxCollectorMovementAddMessage()
        {
        }

        public TaxCollectorMovementAddMessage(TaxCollectorInformations informations)
        {
            Informations = informations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Informations.Protocol);
            Informations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Informations = ProtocolTypesManager.Instance<TaxCollectorInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
    }
}