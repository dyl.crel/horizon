using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorMovementMessage : NetworkMessage
    {
        public override short Protocol => 5633;

        public byte MovementType { get; set; }

        public TaxCollectorBasicInformations BasicInfos { get; set; }

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public TaxCollectorMovementMessage()
        {
        }

        public TaxCollectorMovementMessage(byte movementType, TaxCollectorBasicInformations basicInfos, long playerId, string playerName)
        {
            MovementType = movementType;
            BasicInfos = basicInfos;
            PlayerId = playerId;
            PlayerName = playerName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(MovementType);
            BasicInfos.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MovementType = reader.ReadByte();
            BasicInfos = new TaxCollectorBasicInformations();
            BasicInfos.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
        }
    }
}