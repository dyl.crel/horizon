using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorMovementsOfflineMessage : NetworkMessage
    {
        public override short Protocol => 6611;

        public TaxCollectorMovement[] Movements { get; set; }

        public TaxCollectorMovementsOfflineMessage()
        {
        }

        public TaxCollectorMovementsOfflineMessage(TaxCollectorMovement[] movements)
        {
            Movements = movements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Movements.Length);
            for (var i = 0; i < Movements.Length; i++)
            {
                Movements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Movements = new TaxCollectorMovement[reader.ReadShort()];
            for (var i = 0; i < Movements.Length; i++)
            {
                Movements[i] = new TaxCollectorMovement();
                Movements[i].Deserialize(reader);
            }
        }
    }
}