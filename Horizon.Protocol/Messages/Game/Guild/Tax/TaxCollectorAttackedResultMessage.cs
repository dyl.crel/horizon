using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Guild.Tax;

namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorAttackedResultMessage : NetworkMessage
    {
        public override short Protocol => 5635;

        public bool DeadOrAlive { get; set; }

        public TaxCollectorBasicInformations BasicInfos { get; set; }

        public BasicGuildInformations Guild { get; set; }

        public TaxCollectorAttackedResultMessage()
        {
        }

        public TaxCollectorAttackedResultMessage(bool deadOrAlive, TaxCollectorBasicInformations basicInfos, BasicGuildInformations guild)
        {
            DeadOrAlive = deadOrAlive;
            BasicInfos = basicInfos;
            Guild = guild;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(DeadOrAlive);
            BasicInfos.Serialize(writer);
            Guild.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DeadOrAlive = reader.ReadBoolean();
            BasicInfos = new TaxCollectorBasicInformations();
            BasicInfos.Deserialize(reader);
            Guild = new BasicGuildInformations();
            Guild.Deserialize(reader);
        }
    }
}