namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class TaxCollectorErrorMessage : NetworkMessage
    {
        public override short Protocol => 5634;

        public byte Reason { get; set; }

        public TaxCollectorErrorMessage()
        {
        }

        public TaxCollectorErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}