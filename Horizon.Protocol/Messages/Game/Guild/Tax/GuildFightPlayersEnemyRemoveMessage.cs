namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GuildFightPlayersEnemyRemoveMessage : NetworkMessage
    {
        public override short Protocol => 5929;

        public double FightId { get; set; }

        public long PlayerId { get; set; }

        public GuildFightPlayersEnemyRemoveMessage()
        {
        }

        public GuildFightPlayersEnemyRemoveMessage(double fightId, long playerId)
        {
            FightId = fightId;
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(FightId);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadDouble();
            PlayerId = reader.ReadVarLong();
        }
    }
}