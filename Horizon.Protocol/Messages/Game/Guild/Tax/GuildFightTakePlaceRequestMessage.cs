namespace Horizon.Protocol.Messages.Game.Guild.Tax
{
    public class GuildFightTakePlaceRequestMessage : GuildFightJoinRequestMessage
    {
        public override short Protocol => 6235;

        public int ReplacedCharacterId { get; set; }

        public GuildFightTakePlaceRequestMessage()
        {
        }

        public GuildFightTakePlaceRequestMessage(double taxCollectorId, int replacedCharacterId) : base(taxCollectorId)
        {
            ReplacedCharacterId = replacedCharacterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(ReplacedCharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ReplacedCharacterId = reader.ReadInt();
        }
    }
}