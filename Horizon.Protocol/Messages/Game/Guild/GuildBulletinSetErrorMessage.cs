using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildBulletinSetErrorMessage : SocialNoticeSetErrorMessage
    {
        public override short Protocol => 6691;

        public GuildBulletinSetErrorMessage()
        {
        }

        public GuildBulletinSetErrorMessage(byte reason) : base(reason)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}