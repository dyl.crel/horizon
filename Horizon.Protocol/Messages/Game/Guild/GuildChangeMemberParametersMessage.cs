namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildChangeMemberParametersMessage : NetworkMessage
    {
        public override short Protocol => 5549;

        public long MemberId { get; set; }

        public short Rank { get; set; }

        public byte ExperienceGivenPercent { get; set; }

        public int Rights { get; set; }

        public GuildChangeMemberParametersMessage()
        {
        }

        public GuildChangeMemberParametersMessage(long memberId, short rank, byte experienceGivenPercent, int rights)
        {
            MemberId = memberId;
            Rank = rank;
            ExperienceGivenPercent = experienceGivenPercent;
            Rights = rights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(MemberId);
            writer.WriteVarShort(Rank);
            writer.WriteByte(ExperienceGivenPercent);
            writer.WriteVarInt(Rights);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MemberId = reader.ReadVarLong();
            Rank = reader.ReadVarShort();
            ExperienceGivenPercent = reader.ReadByte();
            Rights = reader.ReadVarInt();
        }
    }
}