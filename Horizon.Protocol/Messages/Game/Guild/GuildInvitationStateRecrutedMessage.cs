namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInvitationStateRecrutedMessage : NetworkMessage
    {
        public override short Protocol => 5548;

        public byte InvitationState { get; set; }

        public GuildInvitationStateRecrutedMessage()
        {
        }

        public GuildInvitationStateRecrutedMessage(byte invitationState)
        {
            InvitationState = invitationState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(InvitationState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InvitationState = reader.ReadByte();
        }
    }
}