using Horizon.Protocol.Types.Game.Character;
using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildFactsMessage : NetworkMessage
    {
        public override short Protocol => 6415;

        public int CreationDate { get; set; }

        public short NbTaxCollectors { get; set; }

        public CharacterMinimalGuildPublicInformations[] Members { get; set; }

        public GuildFactSheetInformations Infos { get; set; }

        public GuildFactsMessage()
        {
        }

        public GuildFactsMessage(int creationDate, short nbTaxCollectors, CharacterMinimalGuildPublicInformations[] members, GuildFactSheetInformations infos)
        {
            CreationDate = creationDate;
            NbTaxCollectors = nbTaxCollectors;
            Members = members;
            Infos = infos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(CreationDate);
            writer.WriteVarShort(NbTaxCollectors);
            writer.WriteShort(Members.Length);
            for (var i = 0; i < Members.Length; i++)
            {
                Members[i].Serialize(writer);
            }
            writer.WriteShort(Infos.Protocol);
            Infos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CreationDate = reader.ReadInt();
            NbTaxCollectors = reader.ReadVarShort();
            Members = new CharacterMinimalGuildPublicInformations[reader.ReadShort()];
            for (var i = 0; i < Members.Length; i++)
            {
                Members[i] = new CharacterMinimalGuildPublicInformations();
                Members[i].Deserialize(reader);
            }
            Infos = ProtocolTypesManager.Instance<GuildFactSheetInformations>(reader.ReadShort());
            Infos.Deserialize(reader);
        }
    }
}