using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildMembershipMessage : GuildJoinedMessage
    {
        public override short Protocol => 5835;

        public GuildMembershipMessage()
        {
        }

        public GuildMembershipMessage(GuildInformations guildInfo, int memberRights) : base(guildInfo, memberRights)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}