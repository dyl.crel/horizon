namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildPaddockRemovedMessage : NetworkMessage
    {
        public override short Protocol => 5955;

        public double PaddockId { get; set; }

        public GuildPaddockRemovedMessage()
        {
        }

        public GuildPaddockRemovedMessage(double paddockId)
        {
            PaddockId = paddockId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(PaddockId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PaddockId = reader.ReadDouble();
        }
    }
}