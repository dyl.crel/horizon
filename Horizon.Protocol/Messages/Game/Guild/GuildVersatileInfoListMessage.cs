using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildVersatileInfoListMessage : NetworkMessage
    {
        public override short Protocol => 6435;

        public GuildVersatileInformations[] Guilds { get; set; }

        public GuildVersatileInfoListMessage()
        {
        }

        public GuildVersatileInfoListMessage(GuildVersatileInformations[] guilds)
        {
            Guilds = guilds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Guilds.Length);
            for (var i = 0; i < Guilds.Length; i++)
            {
                writer.WriteShort(Guilds[i].Protocol);
                Guilds[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Guilds = new GuildVersatileInformations[reader.ReadShort()];
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i] = ProtocolTypesManager.Instance<GuildVersatileInformations>(reader.ReadShort());
                Guilds[i].Deserialize(reader);
            }
        }
    }
}