namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildModificationStartedMessage : NetworkMessage
    {
        public override short Protocol => 6324;

        public bool CanChangeName { get; set; }

        public bool CanChangeEmblem { get; set; }

        public GuildModificationStartedMessage()
        {
        }

        public GuildModificationStartedMessage(bool canChangeName, bool canChangeEmblem)
        {
            CanChangeName = canChangeName;
            CanChangeEmblem = canChangeEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, CanChangeName);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, CanChangeEmblem);
            writer.WriteByte(flag0);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            CanChangeName = BooleanByteWrapper.GetFlag(flag0, 0);
            CanChangeEmblem = BooleanByteWrapper.GetFlag(flag0, 1);
        }
    }
}