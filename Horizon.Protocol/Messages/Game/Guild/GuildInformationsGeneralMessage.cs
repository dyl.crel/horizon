namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInformationsGeneralMessage : NetworkMessage
    {
        public override short Protocol => 5557;

        public bool AbandonnedPaddock { get; set; }

        public byte Level { get; set; }

        public long ExpLevelFloor { get; set; }

        public long Experience { get; set; }

        public long ExpNextLevelFloor { get; set; }

        public int CreationDate { get; set; }

        public short NbTotalMembers { get; set; }

        public short NbConnectedMembers { get; set; }

        public GuildInformationsGeneralMessage()
        {
        }

        public GuildInformationsGeneralMessage(bool abandonnedPaddock, byte level, long expLevelFloor, long experience, long expNextLevelFloor, int creationDate, short nbTotalMembers, short nbConnectedMembers)
        {
            AbandonnedPaddock = abandonnedPaddock;
            Level = level;
            ExpLevelFloor = expLevelFloor;
            Experience = experience;
            ExpNextLevelFloor = expNextLevelFloor;
            CreationDate = creationDate;
            NbTotalMembers = nbTotalMembers;
            NbConnectedMembers = nbConnectedMembers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(AbandonnedPaddock);
            writer.WriteByte(Level);
            writer.WriteVarLong(ExpLevelFloor);
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExpNextLevelFloor);
            writer.WriteInt(CreationDate);
            writer.WriteVarShort(NbTotalMembers);
            writer.WriteVarShort(NbConnectedMembers);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AbandonnedPaddock = reader.ReadBoolean();
            Level = reader.ReadByte();
            ExpLevelFloor = reader.ReadVarLong();
            Experience = reader.ReadVarLong();
            ExpNextLevelFloor = reader.ReadVarLong();
            CreationDate = reader.ReadInt();
            NbTotalMembers = reader.ReadVarShort();
            NbConnectedMembers = reader.ReadVarShort();
        }
    }
}