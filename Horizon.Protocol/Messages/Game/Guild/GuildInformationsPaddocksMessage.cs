using Horizon.Protocol.Types.Game.Paddock;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInformationsPaddocksMessage : NetworkMessage
    {
        public override short Protocol => 5959;

        public byte NbPaddockMax { get; set; }

        public PaddockContentInformations[] PaddocksInformations { get; set; }

        public GuildInformationsPaddocksMessage()
        {
        }

        public GuildInformationsPaddocksMessage(byte nbPaddockMax, PaddockContentInformations[] paddocksInformations)
        {
            NbPaddockMax = nbPaddockMax;
            PaddocksInformations = paddocksInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(NbPaddockMax);
            writer.WriteShort(PaddocksInformations.Length);
            for (var i = 0; i < PaddocksInformations.Length; i++)
            {
                PaddocksInformations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NbPaddockMax = reader.ReadByte();
            PaddocksInformations = new PaddockContentInformations[reader.ReadShort()];
            for (var i = 0; i < PaddocksInformations.Length; i++)
            {
                PaddocksInformations[i] = new PaddockContentInformations();
                PaddocksInformations[i].Deserialize(reader);
            }
        }
    }
}