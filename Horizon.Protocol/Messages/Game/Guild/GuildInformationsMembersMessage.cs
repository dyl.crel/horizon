using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInformationsMembersMessage : NetworkMessage
    {
        public override short Protocol => 5558;

        public GuildMember[] Members { get; set; }

        public GuildInformationsMembersMessage()
        {
        }

        public GuildInformationsMembersMessage(GuildMember[] members)
        {
            Members = members;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Members.Length);
            for (var i = 0; i < Members.Length; i++)
            {
                Members[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Members = new GuildMember[reader.ReadShort()];
            for (var i = 0; i < Members.Length; i++)
            {
                Members[i] = new GuildMember();
                Members[i].Deserialize(reader);
            }
        }
    }
}