namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildCharacsUpgradeRequestMessage : NetworkMessage
    {
        public override short Protocol => 5706;

        public byte CharaTypeTarget { get; set; }

        public GuildCharacsUpgradeRequestMessage()
        {
        }

        public GuildCharacsUpgradeRequestMessage(byte charaTypeTarget)
        {
            CharaTypeTarget = charaTypeTarget;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(CharaTypeTarget);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CharaTypeTarget = reader.ReadByte();
        }
    }
}