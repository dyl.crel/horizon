namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildGetInformationsMessage : NetworkMessage
    {
        public override short Protocol => 5550;

        public byte InfoType { get; set; }

        public GuildGetInformationsMessage()
        {
        }

        public GuildGetInformationsMessage(byte infoType)
        {
            InfoType = infoType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(InfoType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InfoType = reader.ReadByte();
        }
    }
}