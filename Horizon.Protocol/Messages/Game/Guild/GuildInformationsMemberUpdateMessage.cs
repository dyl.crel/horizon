using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInformationsMemberUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5597;

        public GuildMember Member { get; set; }

        public GuildInformationsMemberUpdateMessage()
        {
        }

        public GuildInformationsMemberUpdateMessage(GuildMember member)
        {
            Member = member;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Member.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Member = new GuildMember();
            Member.Deserialize(reader);
        }
    }
}