namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInvitationByNameMessage : NetworkMessage
    {
        public override short Protocol => 6115;

        public string Name { get; set; }

        public GuildInvitationByNameMessage()
        {
        }

        public GuildInvitationByNameMessage(string name)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
        }
    }
}