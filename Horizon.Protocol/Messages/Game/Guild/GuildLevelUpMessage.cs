namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildLevelUpMessage : NetworkMessage
    {
        public override short Protocol => 6062;

        public byte NewLevel { get; set; }

        public GuildLevelUpMessage()
        {
        }

        public GuildLevelUpMessage(byte newLevel)
        {
            NewLevel = newLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(NewLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NewLevel = reader.ReadByte();
        }
    }
}