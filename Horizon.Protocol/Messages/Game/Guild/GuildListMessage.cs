using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildListMessage : NetworkMessage
    {
        public override short Protocol => 6413;

        public GuildInformations[] Guilds { get; set; }

        public GuildListMessage()
        {
        }

        public GuildListMessage(GuildInformations[] guilds)
        {
            Guilds = guilds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Guilds.Length);
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Guilds = new GuildInformations[reader.ReadShort()];
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i] = new GuildInformations();
                Guilds[i].Deserialize(reader);
            }
        }
    }
}