using Horizon.Protocol.Types.Game.Character;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildInAllianceFactsMessage : GuildFactsMessage
    {
        public override short Protocol => 6422;

        public BasicNamedAllianceInformations AllianceInfos { get; set; }

        public GuildInAllianceFactsMessage()
        {
        }

        public GuildInAllianceFactsMessage(int creationDate, short nbTaxCollectors, CharacterMinimalGuildPublicInformations[] members, GuildFactSheetInformations infos, BasicNamedAllianceInformations allianceInfos) : base(creationDate, nbTaxCollectors, members, infos)
        {
            AllianceInfos = allianceInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AllianceInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceInfos = new BasicNamedAllianceInformations();
            AllianceInfos.Deserialize(reader);
        }
    }
}