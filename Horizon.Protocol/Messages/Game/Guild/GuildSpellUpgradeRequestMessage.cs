namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildSpellUpgradeRequestMessage : NetworkMessage
    {
        public override short Protocol => 5699;

        public int SpellId { get; set; }

        public GuildSpellUpgradeRequestMessage()
        {
        }

        public GuildSpellUpgradeRequestMessage(int spellId)
        {
            SpellId = spellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(SpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadInt();
        }
    }
}