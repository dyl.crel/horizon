namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildMemberOnlineStatusMessage : NetworkMessage
    {
        public override short Protocol => 6061;

        public long MemberId { get; set; }

        public bool Online { get; set; }

        public GuildMemberOnlineStatusMessage()
        {
        }

        public GuildMemberOnlineStatusMessage(long memberId, bool online)
        {
            MemberId = memberId;
            Online = online;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(MemberId);
            writer.WriteBoolean(Online);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MemberId = reader.ReadVarLong();
            Online = reader.ReadBoolean();
        }
    }
}