using Horizon.Protocol.Types.Game.House;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildHousesInformationMessage : NetworkMessage
    {
        public override short Protocol => 5919;

        public HouseInformationsForGuild[] HousesInformations { get; set; }

        public GuildHousesInformationMessage()
        {
        }

        public GuildHousesInformationMessage(HouseInformationsForGuild[] housesInformations)
        {
            HousesInformations = housesInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(HousesInformations.Length);
            for (var i = 0; i < HousesInformations.Length; i++)
            {
                HousesInformations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HousesInformations = new HouseInformationsForGuild[reader.ReadShort()];
            for (var i = 0; i < HousesInformations.Length; i++)
            {
                HousesInformations[i] = new HouseInformationsForGuild();
                HousesInformations[i].Deserialize(reader);
            }
        }
    }
}