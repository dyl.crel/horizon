namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildMemberLeavingMessage : NetworkMessage
    {
        public override short Protocol => 5923;

        public bool Kicked { get; set; }

        public long MemberId { get; set; }

        public GuildMemberLeavingMessage()
        {
        }

        public GuildMemberLeavingMessage(bool kicked, long memberId)
        {
            Kicked = kicked;
            MemberId = memberId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Kicked);
            writer.WriteVarLong(MemberId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Kicked = reader.ReadBoolean();
            MemberId = reader.ReadVarLong();
        }
    }
}