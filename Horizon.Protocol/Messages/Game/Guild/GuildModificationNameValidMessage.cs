namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildModificationNameValidMessage : NetworkMessage
    {
        public override short Protocol => 6327;

        public string GuildName { get; set; }

        public GuildModificationNameValidMessage()
        {
        }

        public GuildModificationNameValidMessage(string guildName)
        {
            GuildName = guildName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(GuildName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildName = reader.ReadUTF();
        }
    }
}