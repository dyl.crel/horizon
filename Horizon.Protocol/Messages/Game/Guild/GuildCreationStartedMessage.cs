namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildCreationStartedMessage : NetworkMessage
    {
        public override short Protocol => 5920;

        public GuildCreationStartedMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}