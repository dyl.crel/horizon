using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildModificationValidMessage : NetworkMessage
    {
        public override short Protocol => 6323;

        public string GuildName { get; set; }

        public GuildEmblem GuildEmblem { get; set; }

        public GuildModificationValidMessage()
        {
        }

        public GuildModificationValidMessage(string guildName, GuildEmblem guildEmblem)
        {
            GuildName = guildName;
            GuildEmblem = guildEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(GuildName);
            GuildEmblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildName = reader.ReadUTF();
            GuildEmblem = new GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
    }
}