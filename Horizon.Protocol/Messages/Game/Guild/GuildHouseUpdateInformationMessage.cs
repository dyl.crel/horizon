using Horizon.Protocol.Types.Game.House;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildHouseUpdateInformationMessage : NetworkMessage
    {
        public override short Protocol => 6181;

        public HouseInformationsForGuild HousesInformations { get; set; }

        public GuildHouseUpdateInformationMessage()
        {
        }

        public GuildHouseUpdateInformationMessage(HouseInformationsForGuild housesInformations)
        {
            HousesInformations = housesInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            HousesInformations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HousesInformations = new HouseInformationsForGuild();
            HousesInformations.Deserialize(reader);
        }
    }
}