using Horizon.Protocol.Types.Game.Paddock;

namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildPaddockBoughtMessage : NetworkMessage
    {
        public override short Protocol => 5952;

        public PaddockContentInformations PaddockInfo { get; set; }

        public GuildPaddockBoughtMessage()
        {
        }

        public GuildPaddockBoughtMessage(PaddockContentInformations paddockInfo)
        {
            PaddockInfo = paddockInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            PaddockInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PaddockInfo = new PaddockContentInformations();
            PaddockInfo.Deserialize(reader);
        }
    }
}