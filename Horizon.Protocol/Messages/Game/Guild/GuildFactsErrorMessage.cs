namespace Horizon.Protocol.Messages.Game.Guild
{
    public class GuildFactsErrorMessage : NetworkMessage
    {
        public override short Protocol => 6424;

        public int GuildId { get; set; }

        public GuildFactsErrorMessage()
        {
        }

        public GuildFactsErrorMessage(int guildId)
        {
            GuildId = guildId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(GuildId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildId = reader.ReadVarInt();
        }
    }
}