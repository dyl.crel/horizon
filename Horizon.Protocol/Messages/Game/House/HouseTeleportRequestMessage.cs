namespace Horizon.Protocol.Messages.Game.House
{
    public class HouseTeleportRequestMessage : NetworkMessage
    {
        public override short Protocol => 6726;

        public int HouseId { get; set; }

        public int HouseInstanceId { get; set; }

        public HouseTeleportRequestMessage()
        {
        }

        public HouseTeleportRequestMessage(int houseId, int houseInstanceId)
        {
            HouseId = houseId;
            HouseInstanceId = houseInstanceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(HouseInstanceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            HouseInstanceId = reader.ReadInt();
        }
    }
}