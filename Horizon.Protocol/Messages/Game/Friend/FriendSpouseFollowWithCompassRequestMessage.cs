namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendSpouseFollowWithCompassRequestMessage : NetworkMessage
    {
        public override short Protocol => 5606;

        public bool Enable { get; set; }

        public FriendSpouseFollowWithCompassRequestMessage()
        {
        }

        public FriendSpouseFollowWithCompassRequestMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}