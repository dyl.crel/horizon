using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendAddedMessage : NetworkMessage
    {
        public override short Protocol => 5599;

        public FriendInformations FriendAdded { get; set; }

        public FriendAddedMessage()
        {
        }

        public FriendAddedMessage(FriendInformations friendAdded)
        {
            FriendAdded = friendAdded;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FriendAdded.Protocol);
            FriendAdded.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FriendAdded = ProtocolTypesManager.Instance<FriendInformations>(reader.ReadShort());
            FriendAdded.Deserialize(reader);
        }
    }
}