namespace Horizon.Protocol.Messages.Game.Friend
{
    public class AcquaintancesGetListMessage : NetworkMessage
    {
        public override short Protocol => 6819;

        public AcquaintancesGetListMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}