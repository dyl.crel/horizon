using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5924;

        public FriendInformations FriendUpdated { get; set; }

        public FriendUpdateMessage()
        {
        }

        public FriendUpdateMessage(FriendInformations friendUpdated)
        {
            FriendUpdated = friendUpdated;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FriendUpdated.Protocol);
            FriendUpdated.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FriendUpdated = ProtocolTypesManager.Instance<FriendInformations>(reader.ReadShort());
            FriendUpdated.Deserialize(reader);
        }
    }
}