namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredDeleteRequestMessage : NetworkMessage
    {
        public override short Protocol => 5680;

        public int AccountId { get; set; }

        public bool Session { get; set; }

        public IgnoredDeleteRequestMessage()
        {
        }

        public IgnoredDeleteRequestMessage(int accountId, bool session)
        {
            AccountId = accountId;
            Session = session;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteBoolean(Session);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AccountId = reader.ReadInt();
            Session = reader.ReadBoolean();
        }
    }
}