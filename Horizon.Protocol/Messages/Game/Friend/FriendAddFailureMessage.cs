namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendAddFailureMessage : NetworkMessage
    {
        public override short Protocol => 5600;

        public byte Reason { get; set; }

        public FriendAddFailureMessage()
        {
        }

        public FriendAddFailureMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}