namespace Horizon.Protocol.Messages.Game.Friend
{
    public class SpouseGetInformationsMessage : NetworkMessage
    {
        public override short Protocol => 6355;

        public SpouseGetInformationsMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}