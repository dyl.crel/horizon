namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendsGetListMessage : NetworkMessage
    {
        public override short Protocol => 4001;

        public FriendsGetListMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}