namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendDeleteResultMessage : NetworkMessage
    {
        public override short Protocol => 5601;

        public bool Success { get; set; }

        public string Name { get; set; }

        public FriendDeleteResultMessage()
        {
        }

        public FriendDeleteResultMessage(bool success, string name)
        {
            Success = success;
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Success);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Success = reader.ReadBoolean();
            Name = reader.ReadUTF();
        }
    }
}