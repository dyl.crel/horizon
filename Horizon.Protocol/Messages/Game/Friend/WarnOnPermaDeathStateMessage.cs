namespace Horizon.Protocol.Messages.Game.Friend
{
    public class WarnOnPermaDeathStateMessage : NetworkMessage
    {
        public override short Protocol => 6513;

        public bool Enable { get; set; }

        public WarnOnPermaDeathStateMessage()
        {
        }

        public WarnOnPermaDeathStateMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}