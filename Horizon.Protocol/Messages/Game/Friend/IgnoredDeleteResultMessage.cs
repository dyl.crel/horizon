namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredDeleteResultMessage : NetworkMessage
    {
        public override short Protocol => 5677;

        public bool Success { get; set; }

        public bool Session { get; set; }

        public string Name { get; set; }

        public IgnoredDeleteResultMessage()
        {
        }

        public IgnoredDeleteResultMessage(bool success, bool session, string name)
        {
            Success = success;
            Session = session;
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Success);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Session);
            writer.WriteByte(flag0);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag0, 0);
            Session = BooleanByteWrapper.GetFlag(flag0, 1);
            Name = reader.ReadUTF();
        }
    }
}