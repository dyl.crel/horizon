namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendSetStatusShareMessage : NetworkMessage
    {
        public override short Protocol => 6822;

        public bool Share { get; set; }

        public FriendSetStatusShareMessage()
        {
        }

        public FriendSetStatusShareMessage(bool share)
        {
            Share = share;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Share);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Share = reader.ReadBoolean();
        }
    }
}