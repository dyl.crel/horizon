using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredListMessage : NetworkMessage
    {
        public override short Protocol => 5674;

        public IgnoredInformations[] IgnoredList { get; set; }

        public IgnoredListMessage()
        {
        }

        public IgnoredListMessage(IgnoredInformations[] ignoredList)
        {
            IgnoredList = ignoredList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(IgnoredList.Length);
            for (var i = 0; i < IgnoredList.Length; i++)
            {
                writer.WriteShort(IgnoredList[i].Protocol);
                IgnoredList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IgnoredList = new IgnoredInformations[reader.ReadShort()];
            for (var i = 0; i < IgnoredList.Length; i++)
            {
                IgnoredList[i] = ProtocolTypesManager.Instance<IgnoredInformations>(reader.ReadShort());
                IgnoredList[i].Deserialize(reader);
            }
        }
    }
}