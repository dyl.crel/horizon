namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendJoinRequestMessage : NetworkMessage
    {
        public override short Protocol => 5605;

        public string Name { get; set; }

        public FriendJoinRequestMessage()
        {
        }

        public FriendJoinRequestMessage(string name)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
        }
    }
}