namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendWarnOnLevelGainStateMessage : NetworkMessage
    {
        public override short Protocol => 6078;

        public bool Enable { get; set; }

        public FriendWarnOnLevelGainStateMessage()
        {
        }

        public FriendWarnOnLevelGainStateMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}