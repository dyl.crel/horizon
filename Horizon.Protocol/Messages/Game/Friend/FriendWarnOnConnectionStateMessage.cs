namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendWarnOnConnectionStateMessage : NetworkMessage
    {
        public override short Protocol => 5630;

        public bool Enable { get; set; }

        public FriendWarnOnConnectionStateMessage()
        {
        }

        public FriendWarnOnConnectionStateMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}