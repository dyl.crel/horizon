namespace Horizon.Protocol.Messages.Game.Friend
{
    public class GuildMemberSetWarnOnConnectionMessage : NetworkMessage
    {
        public override short Protocol => 6159;

        public bool Enable { get; set; }

        public GuildMemberSetWarnOnConnectionMessage()
        {
        }

        public GuildMemberSetWarnOnConnectionMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}