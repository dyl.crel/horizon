namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredGetListMessage : NetworkMessage
    {
        public override short Protocol => 5676;

        public IgnoredGetListMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}