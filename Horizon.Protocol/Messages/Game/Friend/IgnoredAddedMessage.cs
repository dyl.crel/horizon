using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredAddedMessage : NetworkMessage
    {
        public override short Protocol => 5678;

        public bool Session { get; set; }

        public IgnoredInformations IgnoreAdded { get; set; }

        public IgnoredAddedMessage()
        {
        }

        public IgnoredAddedMessage(bool session, IgnoredInformations ignoreAdded)
        {
            Session = session;
            IgnoreAdded = ignoreAdded;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Session);
            writer.WriteShort(IgnoreAdded.Protocol);
            IgnoreAdded.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Session = reader.ReadBoolean();
            IgnoreAdded = ProtocolTypesManager.Instance<IgnoredInformations>(reader.ReadShort());
            IgnoreAdded.Deserialize(reader);
        }
    }
}