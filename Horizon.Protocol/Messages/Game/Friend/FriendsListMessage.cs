using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendsListMessage : NetworkMessage
    {
        public override short Protocol => 4002;

        public FriendInformations[] FriendsList { get; set; }

        public FriendsListMessage()
        {
        }

        public FriendsListMessage(FriendInformations[] friendsList)
        {
            FriendsList = friendsList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FriendsList.Length);
            for (var i = 0; i < FriendsList.Length; i++)
            {
                writer.WriteShort(FriendsList[i].Protocol);
                FriendsList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FriendsList = new FriendInformations[reader.ReadShort()];
            for (var i = 0; i < FriendsList.Length; i++)
            {
                FriendsList[i] = ProtocolTypesManager.Instance<FriendInformations>(reader.ReadShort());
                FriendsList[i].Deserialize(reader);
            }
        }
    }
}