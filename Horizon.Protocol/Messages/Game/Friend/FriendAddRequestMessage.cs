namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendAddRequestMessage : NetworkMessage
    {
        public override short Protocol => 4004;

        public string Name { get; set; }

        public FriendAddRequestMessage()
        {
        }

        public FriendAddRequestMessage(string name)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
        }
    }
}