namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendSetWarnOnLevelGainMessage : NetworkMessage
    {
        public override short Protocol => 6077;

        public bool Enable { get; set; }

        public FriendSetWarnOnLevelGainMessage()
        {
        }

        public FriendSetWarnOnLevelGainMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}