namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendSetWarnOnConnectionMessage : NetworkMessage
    {
        public override short Protocol => 5602;

        public bool Enable { get; set; }

        public FriendSetWarnOnConnectionMessage()
        {
        }

        public FriendSetWarnOnConnectionMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}