namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredAddRequestMessage : NetworkMessage
    {
        public override short Protocol => 5673;

        public string Name { get; set; }

        public bool Session { get; set; }

        public IgnoredAddRequestMessage()
        {
        }

        public IgnoredAddRequestMessage(string name, bool session)
        {
            Name = name;
            Session = session;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteBoolean(Session);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            Session = reader.ReadBoolean();
        }
    }
}