namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendDeleteRequestMessage : NetworkMessage
    {
        public override short Protocol => 5603;

        public int AccountId { get; set; }

        public FriendDeleteRequestMessage()
        {
        }

        public FriendDeleteRequestMessage(int accountId)
        {
            AccountId = accountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AccountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AccountId = reader.ReadInt();
        }
    }
}