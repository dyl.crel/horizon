using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class AcquaintanceAddedMessage : NetworkMessage
    {
        public override short Protocol => 6818;

        public AcquaintanceInformation AcquaintanceAdded { get; set; }

        public AcquaintanceAddedMessage()
        {
        }

        public AcquaintanceAddedMessage(AcquaintanceInformation acquaintanceAdded)
        {
            AcquaintanceAdded = acquaintanceAdded;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(AcquaintanceAdded.Protocol);
            AcquaintanceAdded.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AcquaintanceAdded = ProtocolTypesManager.Instance<AcquaintanceInformation>(reader.ReadShort());
            AcquaintanceAdded.Deserialize(reader);
        }
    }
}