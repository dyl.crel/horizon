namespace Horizon.Protocol.Messages.Game.Friend
{
    public class GuildMemberWarnOnConnectionStateMessage : NetworkMessage
    {
        public override short Protocol => 6160;

        public bool Enable { get; set; }

        public GuildMemberWarnOnConnectionStateMessage()
        {
        }

        public GuildMemberWarnOnConnectionStateMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}