namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendStatusShareStateMessage : NetworkMessage
    {
        public override short Protocol => 6823;

        public bool Share { get; set; }

        public FriendStatusShareStateMessage()
        {
        }

        public FriendStatusShareStateMessage(bool share)
        {
            Share = share;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Share);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Share = reader.ReadBoolean();
        }
    }
}