namespace Horizon.Protocol.Messages.Game.Friend
{
    public class FriendSpouseJoinRequestMessage : NetworkMessage
    {
        public override short Protocol => 5604;

        public FriendSpouseJoinRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}