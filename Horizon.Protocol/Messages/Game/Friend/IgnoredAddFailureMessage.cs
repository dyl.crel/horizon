namespace Horizon.Protocol.Messages.Game.Friend
{
    public class IgnoredAddFailureMessage : NetworkMessage
    {
        public override short Protocol => 5679;

        public byte Reason { get; set; }

        public IgnoredAddFailureMessage()
        {
        }

        public IgnoredAddFailureMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}