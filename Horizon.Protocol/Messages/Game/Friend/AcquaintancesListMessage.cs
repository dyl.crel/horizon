using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class AcquaintancesListMessage : NetworkMessage
    {
        public override short Protocol => 6820;

        public AcquaintanceInformation[] AcquaintanceList { get; set; }

        public AcquaintancesListMessage()
        {
        }

        public AcquaintancesListMessage(AcquaintanceInformation[] acquaintanceList)
        {
            AcquaintanceList = acquaintanceList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(AcquaintanceList.Length);
            for (var i = 0; i < AcquaintanceList.Length; i++)
            {
                writer.WriteShort(AcquaintanceList[i].Protocol);
                AcquaintanceList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AcquaintanceList = new AcquaintanceInformation[reader.ReadShort()];
            for (var i = 0; i < AcquaintanceList.Length; i++)
            {
                AcquaintanceList[i] = ProtocolTypesManager.Instance<AcquaintanceInformation>(reader.ReadShort());
                AcquaintanceList[i].Deserialize(reader);
            }
        }
    }
}