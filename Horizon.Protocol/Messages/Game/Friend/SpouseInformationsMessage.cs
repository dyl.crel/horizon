using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Messages.Game.Friend
{
    public class SpouseInformationsMessage : NetworkMessage
    {
        public override short Protocol => 6356;

        public FriendSpouseInformations Spouse { get; set; }

        public SpouseInformationsMessage()
        {
        }

        public SpouseInformationsMessage(FriendSpouseInformations spouse)
        {
            Spouse = spouse;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Spouse.Protocol);
            Spouse.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Spouse = ProtocolTypesManager.Instance<FriendSpouseInformations>(reader.ReadShort());
            Spouse.Deserialize(reader);
        }
    }
}