namespace Horizon.Protocol.Messages.Game.Friend
{
    public class SpouseStatusMessage : NetworkMessage
    {
        public override short Protocol => 6265;

        public bool HasSpouse { get; set; }

        public SpouseStatusMessage()
        {
        }

        public SpouseStatusMessage(bool hasSpouse)
        {
            HasSpouse = hasSpouse;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(HasSpouse);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HasSpouse = reader.ReadBoolean();
        }
    }
}