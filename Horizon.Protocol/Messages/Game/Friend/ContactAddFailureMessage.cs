namespace Horizon.Protocol.Messages.Game.Friend
{
    public class ContactAddFailureMessage : NetworkMessage
    {
        public override short Protocol => 6821;

        public byte Reason { get; set; }

        public ContactAddFailureMessage()
        {
        }

        public ContactAddFailureMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}