namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarSwapRequestMessage : NetworkMessage
    {
        public override short Protocol => 6230;

        public byte BarType { get; set; }

        public byte FirstSlot { get; set; }

        public byte SecondSlot { get; set; }

        public ShortcutBarSwapRequestMessage()
        {
        }

        public ShortcutBarSwapRequestMessage(byte barType, byte firstSlot, byte secondSlot)
        {
            BarType = barType;
            FirstSlot = firstSlot;
            SecondSlot = secondSlot;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(BarType);
            writer.WriteByte(FirstSlot);
            writer.WriteByte(SecondSlot);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BarType = reader.ReadByte();
            FirstSlot = reader.ReadByte();
            SecondSlot = reader.ReadByte();
        }
    }
}