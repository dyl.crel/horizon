namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarRemoveErrorMessage : NetworkMessage
    {
        public override short Protocol => 6222;

        public byte Error { get; set; }

        public ShortcutBarRemoveErrorMessage()
        {
        }

        public ShortcutBarRemoveErrorMessage(byte error)
        {
            Error = error;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Error);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Error = reader.ReadByte();
        }
    }
}