namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarAddRequestMessage : NetworkMessage
    {
        public override short Protocol => 6225;

        public byte BarType { get; set; }

        public Types.Game.Shortcut.Shortcut Shortcut { get; set; }

        public ShortcutBarAddRequestMessage()
        {
        }

        public ShortcutBarAddRequestMessage(byte barType, Types.Game.Shortcut.Shortcut shortcut)
        {
            BarType = barType;
            Shortcut = shortcut;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(BarType);
            writer.WriteShort(Shortcut.Protocol);
            Shortcut.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BarType = reader.ReadByte();
            Shortcut = ProtocolTypesManager.Instance<Types.Game.Shortcut.Shortcut>(reader.ReadShort());
            Shortcut.Deserialize(reader);
        }
    }
}