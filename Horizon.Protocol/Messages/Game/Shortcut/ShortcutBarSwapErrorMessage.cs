namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarSwapErrorMessage : NetworkMessage
    {
        public override short Protocol => 6226;

        public byte Error { get; set; }

        public ShortcutBarSwapErrorMessage()
        {
        }

        public ShortcutBarSwapErrorMessage(byte error)
        {
            Error = error;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Error);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Error = reader.ReadByte();
        }
    }
}