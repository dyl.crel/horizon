namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarAddErrorMessage : NetworkMessage
    {
        public override short Protocol => 6227;

        public byte Error { get; set; }

        public ShortcutBarAddErrorMessage()
        {
        }

        public ShortcutBarAddErrorMessage(byte error)
        {
            Error = error;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Error);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Error = reader.ReadByte();
        }
    }
}