namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarContentMessage : NetworkMessage
    {
        public override short Protocol => 6231;

        public byte BarType { get; set; }

        public Types.Game.Shortcut.Shortcut[] Shortcuts { get; set; }

        public ShortcutBarContentMessage()
        {
        }

        public ShortcutBarContentMessage(byte barType, Types.Game.Shortcut.Shortcut[] shortcuts)
        {
            BarType = barType;
            Shortcuts = shortcuts;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(BarType);
            writer.WriteShort(Shortcuts.Length);
            for (var i = 0; i < Shortcuts.Length; i++)
            {
                writer.WriteShort(Shortcuts[i].Protocol);
                Shortcuts[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BarType = reader.ReadByte();
            Shortcuts = new Types.Game.Shortcut.Shortcut[reader.ReadShort()];
            for (var i = 0; i < Shortcuts.Length; i++)
            {
                Shortcuts[i] = ProtocolTypesManager.Instance<Types.Game.Shortcut.Shortcut>(reader.ReadShort());
                Shortcuts[i].Deserialize(reader);
            }
        }
    }
}