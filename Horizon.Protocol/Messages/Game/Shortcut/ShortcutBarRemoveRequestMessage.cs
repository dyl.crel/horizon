namespace Horizon.Protocol.Messages.Game.Shortcut
{
    public class ShortcutBarRemoveRequestMessage : NetworkMessage
    {
        public override short Protocol => 6228;

        public byte BarType { get; set; }

        public byte Slot { get; set; }

        public ShortcutBarRemoveRequestMessage()
        {
        }

        public ShortcutBarRemoveRequestMessage(byte barType, byte slot)
        {
            BarType = barType;
            Slot = slot;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(BarType);
            writer.WriteByte(Slot);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BarType = reader.ReadByte();
            Slot = reader.ReadByte();
        }
    }
}