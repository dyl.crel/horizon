namespace Horizon.Protocol.Messages.Game.Pvp
{
    public class UpdateSelfAgressableStatusMessage : NetworkMessage
    {
        public override short Protocol => 6456;

        public byte Status { get; set; }

        public int ProbationTime { get; set; }

        public UpdateSelfAgressableStatusMessage()
        {
        }

        public UpdateSelfAgressableStatusMessage(byte status, int probationTime)
        {
            Status = status;
            ProbationTime = probationTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Status);
            writer.WriteInt(ProbationTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Status = reader.ReadByte();
            ProbationTime = reader.ReadInt();
        }
    }
}