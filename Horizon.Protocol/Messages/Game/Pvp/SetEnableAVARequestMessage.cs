namespace Horizon.Protocol.Messages.Game.Pvp
{
    public class SetEnableAVARequestMessage : NetworkMessage
    {
        public override short Protocol => 6443;

        public bool Enable { get; set; }

        public SetEnableAVARequestMessage()
        {
        }

        public SetEnableAVARequestMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}