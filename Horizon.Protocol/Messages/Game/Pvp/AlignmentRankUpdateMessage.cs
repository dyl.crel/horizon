namespace Horizon.Protocol.Messages.Game.Pvp
{
    public class AlignmentRankUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6058;

        public byte AlignmentRank { get; set; }

        public bool Verbose { get; set; }

        public AlignmentRankUpdateMessage()
        {
        }

        public AlignmentRankUpdateMessage(byte alignmentRank, bool verbose)
        {
            AlignmentRank = alignmentRank;
            Verbose = verbose;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(AlignmentRank);
            writer.WriteBoolean(Verbose);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AlignmentRank = reader.ReadByte();
            Verbose = reader.ReadBoolean();
        }
    }
}