namespace Horizon.Protocol.Messages.Game.Pvp
{
    public class SetEnablePVPRequestMessage : NetworkMessage
    {
        public override short Protocol => 1810;

        public bool Enable { get; set; }

        public SetEnablePVPRequestMessage()
        {
        }

        public SetEnablePVPRequestMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}