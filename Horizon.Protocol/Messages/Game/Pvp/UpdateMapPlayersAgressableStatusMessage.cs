namespace Horizon.Protocol.Messages.Game.Pvp
{
    public class UpdateMapPlayersAgressableStatusMessage : NetworkMessage
    {
        public override short Protocol => 6454;

        public long[] PlayerIds { get; set; }

        public byte[] Enable { get; set; }

        public UpdateMapPlayersAgressableStatusMessage()
        {
        }

        public UpdateMapPlayersAgressableStatusMessage(long[] playerIds, byte[] enable)
        {
            PlayerIds = playerIds;
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PlayerIds.Length);
            for (var i = 0; i < PlayerIds.Length; i++)
            {
                writer.WriteVarLong(PlayerIds[i]);
            }
            writer.WriteShort(Enable.Length);
            for (var i = 0; i < Enable.Length; i++)
            {
                writer.WriteByte(Enable[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerIds = new long[reader.ReadShort()];
            for (var i = 0; i < PlayerIds.Length; i++)
            {
                PlayerIds[i] = reader.ReadVarLong();
            }
            Enable = new byte[reader.ReadShort()];
            for (var i = 0; i < Enable.Length; i++)
            {
                Enable[i] = reader.ReadByte();
            }
        }
    }
}