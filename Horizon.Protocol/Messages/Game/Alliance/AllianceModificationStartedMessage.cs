namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceModificationStartedMessage : NetworkMessage
    {
        public override short Protocol => 6444;

        public bool CanChangeName { get; set; }

        public bool CanChangeTag { get; set; }

        public bool CanChangeEmblem { get; set; }

        public AllianceModificationStartedMessage()
        {
        }

        public AllianceModificationStartedMessage(bool canChangeName, bool canChangeTag, bool canChangeEmblem)
        {
            CanChangeName = canChangeName;
            CanChangeTag = canChangeTag;
            CanChangeEmblem = canChangeEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, CanChangeName);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, CanChangeTag);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, CanChangeEmblem);
            writer.WriteByte(flag0);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            CanChangeName = BooleanByteWrapper.GetFlag(flag0, 0);
            CanChangeTag = BooleanByteWrapper.GetFlag(flag0, 1);
            CanChangeEmblem = BooleanByteWrapper.GetFlag(flag0, 2);
        }
    }
}