using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AlliancePartialListMessage : AllianceListMessage
    {
        public override short Protocol => 6427;

        public AlliancePartialListMessage()
        {
        }

        public AlliancePartialListMessage(AllianceFactSheetInformations[] alliances) : base(alliances)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}