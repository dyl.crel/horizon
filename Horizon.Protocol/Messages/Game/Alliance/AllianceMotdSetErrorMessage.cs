using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceMotdSetErrorMessage : SocialNoticeSetErrorMessage
    {
        public override short Protocol => 6683;

        public AllianceMotdSetErrorMessage()
        {
        }

        public AllianceMotdSetErrorMessage(byte reason) : base(reason)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}