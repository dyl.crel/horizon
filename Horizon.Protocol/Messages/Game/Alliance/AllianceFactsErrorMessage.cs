namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceFactsErrorMessage : NetworkMessage
    {
        public override short Protocol => 6423;

        public int AllianceId { get; set; }

        public AllianceFactsErrorMessage()
        {
        }

        public AllianceFactsErrorMessage(int allianceId)
        {
            AllianceId = allianceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(AllianceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceId = reader.ReadVarInt();
        }
    }
}