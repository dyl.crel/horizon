using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class KohUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6439;

        public AllianceInformations[] Alliances { get; set; }

        public short[] AllianceNbMembers { get; set; }

        public int[] AllianceRoundWeigth { get; set; }

        public byte[] AllianceMatchScore { get; set; }

        public BasicAllianceInformations[] AllianceMapWinners { get; set; }

        public int AllianceMapWinnerScore { get; set; }

        public int AllianceMapMyAllianceScore { get; set; }

        public double NextTickTime { get; set; }

        public KohUpdateMessage()
        {
        }

        public KohUpdateMessage(AllianceInformations[] alliances, short[] allianceNbMembers, int[] allianceRoundWeigth, byte[] allianceMatchScore, BasicAllianceInformations[] allianceMapWinners, int allianceMapWinnerScore, int allianceMapMyAllianceScore, double nextTickTime)
        {
            Alliances = alliances;
            AllianceNbMembers = allianceNbMembers;
            AllianceRoundWeigth = allianceRoundWeigth;
            AllianceMatchScore = allianceMatchScore;
            AllianceMapWinners = allianceMapWinners;
            AllianceMapWinnerScore = allianceMapWinnerScore;
            AllianceMapMyAllianceScore = allianceMapMyAllianceScore;
            NextTickTime = nextTickTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Alliances.Length);
            for (var i = 0; i < Alliances.Length; i++)
            {
                Alliances[i].Serialize(writer);
            }
            writer.WriteShort(AllianceNbMembers.Length);
            for (var i = 0; i < AllianceNbMembers.Length; i++)
            {
                writer.WriteVarShort(AllianceNbMembers[i]);
            }
            writer.WriteShort(AllianceRoundWeigth.Length);
            for (var i = 0; i < AllianceRoundWeigth.Length; i++)
            {
                writer.WriteVarInt(AllianceRoundWeigth[i]);
            }
            writer.WriteShort(AllianceMatchScore.Length);
            for (var i = 0; i < AllianceMatchScore.Length; i++)
            {
                writer.WriteByte(AllianceMatchScore[i]);
            }
            writer.WriteShort(AllianceMapWinners.Length);
            for (var i = 0; i < AllianceMapWinners.Length; i++)
            {
                AllianceMapWinners[i].Serialize(writer);
            }
            writer.WriteVarInt(AllianceMapWinnerScore);
            writer.WriteVarInt(AllianceMapMyAllianceScore);
            writer.WriteDouble(NextTickTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Alliances = new AllianceInformations[reader.ReadShort()];
            for (var i = 0; i < Alliances.Length; i++)
            {
                Alliances[i] = new AllianceInformations();
                Alliances[i].Deserialize(reader);
            }
            AllianceNbMembers = new short[reader.ReadShort()];
            for (var i = 0; i < AllianceNbMembers.Length; i++)
            {
                AllianceNbMembers[i] = reader.ReadVarShort();
            }
            AllianceRoundWeigth = new int[reader.ReadShort()];
            for (var i = 0; i < AllianceRoundWeigth.Length; i++)
            {
                AllianceRoundWeigth[i] = reader.ReadVarInt();
            }
            AllianceMatchScore = new byte[reader.ReadShort()];
            for (var i = 0; i < AllianceMatchScore.Length; i++)
            {
                AllianceMatchScore[i] = reader.ReadByte();
            }
            AllianceMapWinners = new BasicAllianceInformations[reader.ReadShort()];
            for (var i = 0; i < AllianceMapWinners.Length; i++)
            {
                AllianceMapWinners[i] = new BasicAllianceInformations();
                AllianceMapWinners[i].Deserialize(reader);
            }
            AllianceMapWinnerScore = reader.ReadVarInt();
            AllianceMapMyAllianceScore = reader.ReadVarInt();
            NextTickTime = reader.ReadDouble();
        }
    }
}