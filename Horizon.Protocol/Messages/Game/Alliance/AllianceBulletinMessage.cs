using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceBulletinMessage : BulletinMessage
    {
        public override short Protocol => 6690;

        public AllianceBulletinMessage()
        {
        }

        public AllianceBulletinMessage(string content, int timestamp, long memberId, string memberName, int lastNotifiedTimestamp) : base(content, timestamp, memberId, memberName, lastNotifiedTimestamp)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}