using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceBulletinSetErrorMessage : SocialNoticeSetErrorMessage
    {
        public override short Protocol => 6692;

        public AllianceBulletinSetErrorMessage()
        {
        }

        public AllianceBulletinSetErrorMessage(byte reason) : base(reason)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}