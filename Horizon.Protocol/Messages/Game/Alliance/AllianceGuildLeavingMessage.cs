namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceGuildLeavingMessage : NetworkMessage
    {
        public override short Protocol => 6399;

        public bool Kicked { get; set; }

        public int GuildId { get; set; }

        public AllianceGuildLeavingMessage()
        {
        }

        public AllianceGuildLeavingMessage(bool kicked, int guildId)
        {
            Kicked = kicked;
            GuildId = guildId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Kicked);
            writer.WriteVarInt(GuildId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Kicked = reader.ReadBoolean();
            GuildId = reader.ReadVarInt();
        }
    }
}