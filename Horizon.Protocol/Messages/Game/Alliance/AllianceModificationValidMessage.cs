using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceModificationValidMessage : NetworkMessage
    {
        public override short Protocol => 6450;

        public string AllianceName { get; set; }

        public string AllianceTag { get; set; }

        public GuildEmblem Alliancemblem { get; set; }

        public AllianceModificationValidMessage()
        {
        }

        public AllianceModificationValidMessage(string allianceName, string allianceTag, GuildEmblem alliancemblem)
        {
            AllianceName = allianceName;
            AllianceTag = allianceTag;
            Alliancemblem = alliancemblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(AllianceName);
            writer.WriteUTF(AllianceTag);
            Alliancemblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceName = reader.ReadUTF();
            AllianceTag = reader.ReadUTF();
            Alliancemblem = new GuildEmblem();
            Alliancemblem.Deserialize(reader);
        }
    }
}