namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceKickRequestMessage : NetworkMessage
    {
        public override short Protocol => 6400;

        public int KickedId { get; set; }

        public AllianceKickRequestMessage()
        {
        }

        public AllianceKickRequestMessage(int kickedId)
        {
            KickedId = kickedId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(KickedId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            KickedId = reader.ReadVarInt();
        }
    }
}