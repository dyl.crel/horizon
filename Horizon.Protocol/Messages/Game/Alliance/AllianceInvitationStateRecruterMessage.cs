namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInvitationStateRecruterMessage : NetworkMessage
    {
        public override short Protocol => 6396;

        public string RecrutedName { get; set; }

        public byte InvitationState { get; set; }

        public AllianceInvitationStateRecruterMessage()
        {
        }

        public AllianceInvitationStateRecruterMessage(string recrutedName, byte invitationState)
        {
            RecrutedName = recrutedName;
            InvitationState = invitationState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(RecrutedName);
            writer.WriteByte(InvitationState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RecrutedName = reader.ReadUTF();
            InvitationState = reader.ReadByte();
        }
    }
}