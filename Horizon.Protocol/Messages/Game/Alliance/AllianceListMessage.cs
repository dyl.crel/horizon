using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceListMessage : NetworkMessage
    {
        public override short Protocol => 6408;

        public AllianceFactSheetInformations[] Alliances { get; set; }

        public AllianceListMessage()
        {
        }

        public AllianceListMessage(AllianceFactSheetInformations[] alliances)
        {
            Alliances = alliances;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Alliances.Length);
            for (var i = 0; i < Alliances.Length; i++)
            {
                Alliances[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Alliances = new AllianceFactSheetInformations[reader.ReadShort()];
            for (var i = 0; i < Alliances.Length; i++)
            {
                Alliances[i] = new AllianceFactSheetInformations();
                Alliances[i].Deserialize(reader);
            }
        }
    }
}