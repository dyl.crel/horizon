using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceVersatileInfoListMessage : NetworkMessage
    {
        public override short Protocol => 6436;

        public AllianceVersatileInformations[] Alliances { get; set; }

        public AllianceVersatileInfoListMessage()
        {
        }

        public AllianceVersatileInfoListMessage(AllianceVersatileInformations[] alliances)
        {
            Alliances = alliances;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Alliances.Length);
            for (var i = 0; i < Alliances.Length; i++)
            {
                Alliances[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Alliances = new AllianceVersatileInformations[reader.ReadShort()];
            for (var i = 0; i < Alliances.Length; i++)
            {
                Alliances[i] = new AllianceVersatileInformations();
                Alliances[i].Deserialize(reader);
            }
        }
    }
}