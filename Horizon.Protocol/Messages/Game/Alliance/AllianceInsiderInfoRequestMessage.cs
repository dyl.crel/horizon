namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInsiderInfoRequestMessage : NetworkMessage
    {
        public override short Protocol => 6417;

        public AllianceInsiderInfoRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}