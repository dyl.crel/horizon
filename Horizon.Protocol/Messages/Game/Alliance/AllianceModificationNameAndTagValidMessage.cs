namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceModificationNameAndTagValidMessage : NetworkMessage
    {
        public override short Protocol => 6449;

        public string AllianceName { get; set; }

        public string AllianceTag { get; set; }

        public AllianceModificationNameAndTagValidMessage()
        {
        }

        public AllianceModificationNameAndTagValidMessage(string allianceName, string allianceTag)
        {
            AllianceName = allianceName;
            AllianceTag = allianceTag;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(AllianceName);
            writer.WriteUTF(AllianceTag);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceName = reader.ReadUTF();
            AllianceTag = reader.ReadUTF();
        }
    }
}