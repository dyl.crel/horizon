namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceCreationStartedMessage : NetworkMessage
    {
        public override short Protocol => 6394;

        public AllianceCreationStartedMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}