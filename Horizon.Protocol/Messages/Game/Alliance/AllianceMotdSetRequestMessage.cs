using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceMotdSetRequestMessage : SocialNoticeSetRequestMessage
    {
        public override short Protocol => 6687;

        public string Content { get; set; }

        public AllianceMotdSetRequestMessage()
        {
        }

        public AllianceMotdSetRequestMessage(string content)
        {
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Content = reader.ReadUTF();
        }
    }
}