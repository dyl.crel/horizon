namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceFactsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6409;

        public int AllianceId { get; set; }

        public AllianceFactsRequestMessage()
        {
        }

        public AllianceFactsRequestMessage(int allianceId)
        {
            AllianceId = allianceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(AllianceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceId = reader.ReadVarInt();
        }
    }
}