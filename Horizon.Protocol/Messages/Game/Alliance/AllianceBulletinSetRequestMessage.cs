using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceBulletinSetRequestMessage : SocialNoticeSetRequestMessage
    {
        public override short Protocol => 6693;

        public string Content { get; set; }

        public bool NotifyMembers { get; set; }

        public AllianceBulletinSetRequestMessage()
        {
        }

        public AllianceBulletinSetRequestMessage(string content, bool notifyMembers)
        {
            Content = content;
            NotifyMembers = notifyMembers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Content);
            writer.WriteBoolean(NotifyMembers);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Content = reader.ReadUTF();
            NotifyMembers = reader.ReadBoolean();
        }
    }
}