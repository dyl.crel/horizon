using Horizon.Protocol.Messages.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceMotdMessage : SocialNoticeMessage
    {
        public override short Protocol => 6685;

        public AllianceMotdMessage()
        {
        }

        public AllianceMotdMessage(string content, int timestamp, long memberId, string memberName) : base(content, timestamp, memberId, memberName)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}