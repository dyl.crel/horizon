namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceChangeGuildRightsMessage : NetworkMessage
    {
        public override short Protocol => 6426;

        public int GuildId { get; set; }

        public byte Rights { get; set; }

        public AllianceChangeGuildRightsMessage()
        {
        }

        public AllianceChangeGuildRightsMessage(int guildId, byte rights)
        {
            GuildId = guildId;
            Rights = rights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(GuildId);
            writer.WriteByte(Rights);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildId = reader.ReadVarInt();
            Rights = reader.ReadByte();
        }
    }
}