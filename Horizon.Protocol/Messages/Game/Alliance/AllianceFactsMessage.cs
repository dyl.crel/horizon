using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceFactsMessage : NetworkMessage
    {
        public override short Protocol => 6414;

        public GuildInAllianceInformations[] Guilds { get; set; }

        public short[] ControlledSubareaIds { get; set; }

        public long LeaderCharacterId { get; set; }

        public string LeaderCharacterName { get; set; }

        public AllianceFactSheetInformations Infos { get; set; }

        public AllianceFactsMessage()
        {
        }

        public AllianceFactsMessage(GuildInAllianceInformations[] guilds, short[] controlledSubareaIds, long leaderCharacterId, string leaderCharacterName, AllianceFactSheetInformations infos)
        {
            Guilds = guilds;
            ControlledSubareaIds = controlledSubareaIds;
            LeaderCharacterId = leaderCharacterId;
            LeaderCharacterName = leaderCharacterName;
            Infos = infos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Guilds.Length);
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i].Serialize(writer);
            }
            writer.WriteShort(ControlledSubareaIds.Length);
            for (var i = 0; i < ControlledSubareaIds.Length; i++)
            {
                writer.WriteVarShort(ControlledSubareaIds[i]);
            }
            writer.WriteVarLong(LeaderCharacterId);
            writer.WriteUTF(LeaderCharacterName);
            writer.WriteShort(Infos.Protocol);
            Infos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Guilds = new GuildInAllianceInformations[reader.ReadShort()];
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i] = new GuildInAllianceInformations();
                Guilds[i].Deserialize(reader);
            }
            ControlledSubareaIds = new short[reader.ReadShort()];
            for (var i = 0; i < ControlledSubareaIds.Length; i++)
            {
                ControlledSubareaIds[i] = reader.ReadVarShort();
            }
            LeaderCharacterId = reader.ReadVarLong();
            LeaderCharacterName = reader.ReadUTF();
            Infos = ProtocolTypesManager.Instance<AllianceFactSheetInformations>(reader.ReadShort());
            Infos.Deserialize(reader);
        }
    }
}