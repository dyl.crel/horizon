using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceMembershipMessage : AllianceJoinedMessage
    {
        public override short Protocol => 6390;

        public AllianceMembershipMessage()
        {
        }

        public AllianceMembershipMessage(AllianceInformations allianceInfo, bool enabled, int leadingGuildId) : base(allianceInfo, enabled, leadingGuildId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}