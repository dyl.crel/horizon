using Horizon.Protocol.Types.Game.Prism;
using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInsiderInfoMessage : NetworkMessage
    {
        public override short Protocol => 6403;

        public AllianceFactSheetInformations AllianceInfos { get; set; }

        public GuildInsiderFactSheetInformations[] Guilds { get; set; }

        public PrismSubareaEmptyInfo[] Prisms { get; set; }

        public AllianceInsiderInfoMessage()
        {
        }

        public AllianceInsiderInfoMessage(AllianceFactSheetInformations allianceInfos, GuildInsiderFactSheetInformations[] guilds, PrismSubareaEmptyInfo[] prisms)
        {
            AllianceInfos = allianceInfos;
            Guilds = guilds;
            Prisms = prisms;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            AllianceInfos.Serialize(writer);
            writer.WriteShort(Guilds.Length);
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i].Serialize(writer);
            }
            writer.WriteShort(Prisms.Length);
            for (var i = 0; i < Prisms.Length; i++)
            {
                writer.WriteShort(Prisms[i].Protocol);
                Prisms[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceInfos = new AllianceFactSheetInformations();
            AllianceInfos.Deserialize(reader);
            Guilds = new GuildInsiderFactSheetInformations[reader.ReadShort()];
            for (var i = 0; i < Guilds.Length; i++)
            {
                Guilds[i] = new GuildInsiderFactSheetInformations();
                Guilds[i].Deserialize(reader);
            }
            Prisms = new PrismSubareaEmptyInfo[reader.ReadShort()];
            for (var i = 0; i < Prisms.Length; i++)
            {
                Prisms[i] = ProtocolTypesManager.Instance<PrismSubareaEmptyInfo>(reader.ReadShort());
                Prisms[i].Deserialize(reader);
            }
        }
    }
}