namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceLeftMessage : NetworkMessage
    {
        public override short Protocol => 6398;

        public AllianceLeftMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}