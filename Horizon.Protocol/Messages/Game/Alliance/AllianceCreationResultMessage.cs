namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceCreationResultMessage : NetworkMessage
    {
        public override short Protocol => 6391;

        public byte Result { get; set; }

        public AllianceCreationResultMessage()
        {
        }

        public AllianceCreationResultMessage(byte result)
        {
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Result = reader.ReadByte();
        }
    }
}