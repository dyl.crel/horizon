using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceJoinedMessage : NetworkMessage
    {
        public override short Protocol => 6402;

        public AllianceInformations AllianceInfo { get; set; }

        public bool Enabled { get; set; }

        public int LeadingGuildId { get; set; }

        public AllianceJoinedMessage()
        {
        }

        public AllianceJoinedMessage(AllianceInformations allianceInfo, bool enabled, int leadingGuildId)
        {
            AllianceInfo = allianceInfo;
            Enabled = enabled;
            LeadingGuildId = leadingGuildId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            AllianceInfo.Serialize(writer);
            writer.WriteBoolean(Enabled);
            writer.WriteVarInt(LeadingGuildId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceInfo = new AllianceInformations();
            AllianceInfo.Deserialize(reader);
            Enabled = reader.ReadBoolean();
            LeadingGuildId = reader.ReadVarInt();
        }
    }
}