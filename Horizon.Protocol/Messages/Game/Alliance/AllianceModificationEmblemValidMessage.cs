using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceModificationEmblemValidMessage : NetworkMessage
    {
        public override short Protocol => 6447;

        public GuildEmblem Alliancemblem { get; set; }

        public AllianceModificationEmblemValidMessage()
        {
        }

        public AllianceModificationEmblemValidMessage(GuildEmblem alliancemblem)
        {
            Alliancemblem = alliancemblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Alliancemblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Alliancemblem = new GuildEmblem();
            Alliancemblem.Deserialize(reader);
        }
    }
}