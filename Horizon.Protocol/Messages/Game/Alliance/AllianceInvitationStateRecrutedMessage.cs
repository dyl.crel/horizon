namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInvitationStateRecrutedMessage : NetworkMessage
    {
        public override short Protocol => 6392;

        public byte InvitationState { get; set; }

        public AllianceInvitationStateRecrutedMessage()
        {
        }

        public AllianceInvitationStateRecrutedMessage(byte invitationState)
        {
            InvitationState = invitationState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(InvitationState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InvitationState = reader.ReadByte();
        }
    }
}