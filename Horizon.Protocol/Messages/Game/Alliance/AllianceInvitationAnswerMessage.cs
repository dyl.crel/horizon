namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInvitationAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6401;

        public bool Accept { get; set; }

        public AllianceInvitationAnswerMessage()
        {
        }

        public AllianceInvitationAnswerMessage(bool accept)
        {
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Accept = reader.ReadBoolean();
        }
    }
}