namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInvitationMessage : NetworkMessage
    {
        public override short Protocol => 6395;

        public long TargetId { get; set; }

        public AllianceInvitationMessage()
        {
        }

        public AllianceInvitationMessage(long targetId)
        {
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TargetId = reader.ReadVarLong();
        }
    }
}