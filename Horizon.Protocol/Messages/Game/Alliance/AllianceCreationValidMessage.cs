using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceCreationValidMessage : NetworkMessage
    {
        public override short Protocol => 6393;

        public string AllianceName { get; set; }

        public string AllianceTag { get; set; }

        public GuildEmblem AllianceEmblem { get; set; }

        public AllianceCreationValidMessage()
        {
        }

        public AllianceCreationValidMessage(string allianceName, string allianceTag, GuildEmblem allianceEmblem)
        {
            AllianceName = allianceName;
            AllianceTag = allianceTag;
            AllianceEmblem = allianceEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(AllianceName);
            writer.WriteUTF(AllianceTag);
            AllianceEmblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceName = reader.ReadUTF();
            AllianceTag = reader.ReadUTF();
            AllianceEmblem = new GuildEmblem();
            AllianceEmblem.Deserialize(reader);
        }
    }
}