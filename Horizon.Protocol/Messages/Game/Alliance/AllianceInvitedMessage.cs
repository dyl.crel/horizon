using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Alliance
{
    public class AllianceInvitedMessage : NetworkMessage
    {
        public override short Protocol => 6397;

        public long RecruterId { get; set; }

        public string RecruterName { get; set; }

        public BasicNamedAllianceInformations AllianceInfo { get; set; }

        public AllianceInvitedMessage()
        {
        }

        public AllianceInvitedMessage(long recruterId, string recruterName, BasicNamedAllianceInformations allianceInfo)
        {
            RecruterId = recruterId;
            RecruterName = recruterName;
            AllianceInfo = allianceInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(RecruterId);
            writer.WriteUTF(RecruterName);
            AllianceInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RecruterId = reader.ReadVarLong();
            RecruterName = reader.ReadUTF();
            AllianceInfo = new BasicNamedAllianceInformations();
            AllianceInfo.Deserialize(reader);
        }
    }
}