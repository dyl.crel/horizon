namespace Horizon.Protocol.Messages.Game.Approach
{
    public class ServerSettingsMessage : NetworkMessage
    {
        public override short Protocol => 6340;

        public bool IsMonoAccount { get; set; }

        public bool HasFreeAutopilot { get; set; }

        public string Lang { get; set; }

        public byte Community { get; set; }

        public byte GameType { get; set; }

        public short ArenaLeaveBanTime { get; set; }

        public int ItemMaxLevel { get; set; }

        public ServerSettingsMessage()
        {
        }

        public ServerSettingsMessage(bool isMonoAccount, bool hasFreeAutopilot, string lang, byte community, byte gameType, short arenaLeaveBanTime, int itemMaxLevel)
        {
            IsMonoAccount = isMonoAccount;
            HasFreeAutopilot = hasFreeAutopilot;
            Lang = lang;
            Community = community;
            GameType = gameType;
            ArenaLeaveBanTime = arenaLeaveBanTime;
            ItemMaxLevel = itemMaxLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, IsMonoAccount);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, HasFreeAutopilot);
            writer.WriteByte(flag0);
            writer.WriteUTF(Lang);
            writer.WriteByte(Community);
            writer.WriteByte(GameType);
            writer.WriteVarShort(ArenaLeaveBanTime);
            writer.WriteInt(ItemMaxLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            IsMonoAccount = BooleanByteWrapper.GetFlag(flag0, 0);
            HasFreeAutopilot = BooleanByteWrapper.GetFlag(flag0, 1);
            Lang = reader.ReadUTF();
            Community = reader.ReadByte();
            GameType = reader.ReadByte();
            ArenaLeaveBanTime = reader.ReadVarShort();
            ItemMaxLevel = reader.ReadInt();
        }
    }
}