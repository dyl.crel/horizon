namespace Horizon.Protocol.Messages.Game.Approach
{
    public class ReloginTokenRequestMessage : NetworkMessage
    {
        public override short Protocol => 6540;

        public ReloginTokenRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}