using Horizon.Protocol.Types.Game.Approach;

namespace Horizon.Protocol.Messages.Game.Approach
{
    public class ServerSessionConstantsMessage : NetworkMessage
    {
        public override short Protocol => 6434;

        public ServerSessionConstant[] Variables { get; set; }

        public ServerSessionConstantsMessage()
        {
        }

        public ServerSessionConstantsMessage(ServerSessionConstant[] variables)
        {
            Variables = variables;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Variables.Length);
            for (var i = 0; i < Variables.Length; i++)
            {
                writer.WriteShort(Variables[i].Protocol);
                Variables[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Variables = new ServerSessionConstant[reader.ReadShort()];
            for (var i = 0; i < Variables.Length; i++)
            {
                Variables[i] = ProtocolTypesManager.Instance<ServerSessionConstant>(reader.ReadShort());
                Variables[i].Deserialize(reader);
            }
        }
    }
}