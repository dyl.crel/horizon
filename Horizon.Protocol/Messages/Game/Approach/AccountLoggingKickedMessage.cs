namespace Horizon.Protocol.Messages.Game.Approach
{
    public class AccountLoggingKickedMessage : NetworkMessage
    {
        public override short Protocol => 6029;

        public short Days { get; set; }

        public byte Hours { get; set; }

        public byte Minutes { get; set; }

        public AccountLoggingKickedMessage()
        {
        }

        public AccountLoggingKickedMessage(short days, byte hours, byte minutes)
        {
            Days = days;
            Hours = hours;
            Minutes = minutes;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Days);
            writer.WriteByte(Hours);
            writer.WriteByte(Minutes);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Days = reader.ReadVarShort();
            Hours = reader.ReadByte();
            Minutes = reader.ReadByte();
        }
    }
}