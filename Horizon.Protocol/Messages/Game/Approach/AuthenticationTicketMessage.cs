namespace Horizon.Protocol.Messages.Game.Approach
{
    public class AuthenticationTicketMessage : NetworkMessage
    {
        public override short Protocol => 110;

        public string Lang { get; set; }

        public string Ticket { get; set; }

        public AuthenticationTicketMessage()
        {
        }

        public AuthenticationTicketMessage(string lang, string ticket)
        {
            Lang = lang;
            Ticket = ticket;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Lang);
            writer.WriteUTF(Ticket);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Lang = reader.ReadUTF();
            Ticket = reader.ReadUTF();
        }
    }
}