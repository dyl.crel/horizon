namespace Horizon.Protocol.Messages.Game.Approach
{
    public class ServerOptionalFeaturesMessage : NetworkMessage
    {
        public override short Protocol => 6305;

        public byte[] Features { get; set; }

        public ServerOptionalFeaturesMessage()
        {
        }

        public ServerOptionalFeaturesMessage(byte[] features)
        {
            Features = features;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Features.Length);
            for (var i = 0; i < Features.Length; i++)
            {
                writer.WriteByte(Features[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Features = new byte[reader.ReadShort()];
            for (var i = 0; i < Features.Length; i++)
            {
                Features[i] = reader.ReadByte();
            }
        }
    }
}