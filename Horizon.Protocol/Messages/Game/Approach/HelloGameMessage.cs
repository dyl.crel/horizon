namespace Horizon.Protocol.Messages.Game.Approach
{
    public class HelloGameMessage : NetworkMessage
    {
        public override short Protocol => 101;

        public HelloGameMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}