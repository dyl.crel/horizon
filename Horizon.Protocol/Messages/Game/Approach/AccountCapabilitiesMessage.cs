namespace Horizon.Protocol.Messages.Game.Approach
{
    public class AccountCapabilitiesMessage : NetworkMessage
    {
        public override short Protocol => 6216;

        public bool TutorialAvailable { get; set; }

        public bool CanCreateNewCharacter { get; set; }

        public int AccountId { get; set; }

        public int BreedsVisible { get; set; }

        public int BreedsAvailable { get; set; }

        public byte Status { get; set; }

        public double UnlimitedRestatEndDate { get; set; }

        public AccountCapabilitiesMessage()
        {
        }

        public AccountCapabilitiesMessage(bool tutorialAvailable, bool canCreateNewCharacter, int accountId, int breedsVisible, int breedsAvailable, byte status, double unlimitedRestatEndDate)
        {
            TutorialAvailable = tutorialAvailable;
            CanCreateNewCharacter = canCreateNewCharacter;
            AccountId = accountId;
            BreedsVisible = breedsVisible;
            BreedsAvailable = breedsAvailable;
            Status = status;
            UnlimitedRestatEndDate = unlimitedRestatEndDate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, TutorialAvailable);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, CanCreateNewCharacter);
            writer.WriteByte(flag0);
            writer.WriteInt(AccountId);
            writer.WriteVarInt(BreedsVisible);
            writer.WriteVarInt(BreedsAvailable);
            writer.WriteByte(Status);
            writer.WriteDouble(UnlimitedRestatEndDate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            TutorialAvailable = BooleanByteWrapper.GetFlag(flag0, 0);
            CanCreateNewCharacter = BooleanByteWrapper.GetFlag(flag0, 1);
            AccountId = reader.ReadInt();
            BreedsVisible = reader.ReadVarInt();
            BreedsAvailable = reader.ReadVarInt();
            Status = reader.ReadByte();
            UnlimitedRestatEndDate = reader.ReadDouble();
        }
    }
}