namespace Horizon.Protocol.Messages.Game.Approach
{
    public class AuthenticationTicketRefusedMessage : NetworkMessage
    {
        public override short Protocol => 112;

        public AuthenticationTicketRefusedMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}