namespace Horizon.Protocol.Messages.Game.Approach
{
    public class ReloginTokenStatusMessage : NetworkMessage
    {
        public override short Protocol => 6539;

        public bool ValidToken { get; set; }

        public byte[] Ticket { get; set; }

        public ReloginTokenStatusMessage()
        {
        }

        public ReloginTokenStatusMessage(bool validToken, byte[] ticket)
        {
            ValidToken = validToken;
            Ticket = ticket;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(ValidToken);
            writer.WriteVarInt(Ticket.Length);
            for (var i = 0; i < Ticket.Length; i++)
            {
                writer.WriteByte(Ticket[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ValidToken = reader.ReadBoolean();
            Ticket = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Ticket.Length; i++)
            {
                Ticket[i] = reader.ReadByte();
            }
        }
    }
}