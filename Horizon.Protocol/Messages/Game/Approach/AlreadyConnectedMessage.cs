namespace Horizon.Protocol.Messages.Game.Approach
{
    public class AlreadyConnectedMessage : NetworkMessage
    {
        public override short Protocol => 109;

        public AlreadyConnectedMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}