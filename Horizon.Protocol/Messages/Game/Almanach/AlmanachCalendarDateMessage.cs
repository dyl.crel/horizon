namespace Horizon.Protocol.Messages.Game.Almanach
{
    public class AlmanachCalendarDateMessage : NetworkMessage
    {
        public override short Protocol => 6341;

        public int Date { get; set; }

        public AlmanachCalendarDateMessage()
        {
        }

        public AlmanachCalendarDateMessage(int date)
        {
            Date = date;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Date);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Date = reader.ReadInt();
        }
    }
}