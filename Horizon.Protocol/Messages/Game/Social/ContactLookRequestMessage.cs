namespace Horizon.Protocol.Messages.Game.Social
{
    public class ContactLookRequestMessage : NetworkMessage
    {
        public override short Protocol => 5932;

        public byte RequestId { get; set; }

        public byte ContactType { get; set; }

        public ContactLookRequestMessage()
        {
        }

        public ContactLookRequestMessage(byte requestId, byte contactType)
        {
            RequestId = requestId;
            ContactType = contactType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(RequestId);
            writer.WriteByte(ContactType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadByte();
            ContactType = reader.ReadByte();
        }
    }
}