namespace Horizon.Protocol.Messages.Game.Social
{
    public class SocialNoticeSetRequestMessage : NetworkMessage
    {
        public override short Protocol => 6686;

        public SocialNoticeSetRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}