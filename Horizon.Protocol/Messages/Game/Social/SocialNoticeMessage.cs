namespace Horizon.Protocol.Messages.Game.Social
{
    public class SocialNoticeMessage : NetworkMessage
    {
        public override short Protocol => 6688;

        public string Content { get; set; }

        public int Timestamp { get; set; }

        public long MemberId { get; set; }

        public string MemberName { get; set; }

        public SocialNoticeMessage()
        {
        }

        public SocialNoticeMessage(string content, int timestamp, long memberId, string memberName)
        {
            Content = content;
            Timestamp = timestamp;
            MemberId = memberId;
            MemberName = memberName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Content);
            writer.WriteInt(Timestamp);
            writer.WriteVarLong(MemberId);
            writer.WriteUTF(MemberName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Content = reader.ReadUTF();
            Timestamp = reader.ReadInt();
            MemberId = reader.ReadVarLong();
            MemberName = reader.ReadUTF();
        }
    }
}