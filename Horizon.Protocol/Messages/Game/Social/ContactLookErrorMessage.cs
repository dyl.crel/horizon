namespace Horizon.Protocol.Messages.Game.Social
{
    public class ContactLookErrorMessage : NetworkMessage
    {
        public override short Protocol => 6045;

        public int RequestId { get; set; }

        public ContactLookErrorMessage()
        {
        }

        public ContactLookErrorMessage(int requestId)
        {
            RequestId = requestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(RequestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadVarInt();
        }
    }
}