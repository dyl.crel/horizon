namespace Horizon.Protocol.Messages.Game.Social
{
    public class SocialNoticeSetErrorMessage : NetworkMessage
    {
        public override short Protocol => 6684;

        public byte Reason { get; set; }

        public SocialNoticeSetErrorMessage()
        {
        }

        public SocialNoticeSetErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}