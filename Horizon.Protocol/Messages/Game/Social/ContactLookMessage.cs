using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Messages.Game.Social
{
    public class ContactLookMessage : NetworkMessage
    {
        public override short Protocol => 5934;

        public int RequestId { get; set; }

        public string PlayerName { get; set; }

        public long PlayerId { get; set; }

        public EntityLook Look { get; set; }

        public ContactLookMessage()
        {
        }

        public ContactLookMessage(int requestId, string playerName, long playerId, EntityLook look)
        {
            RequestId = requestId;
            PlayerName = playerName;
            PlayerId = playerId;
            Look = look;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(RequestId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarLong(PlayerId);
            Look.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadVarInt();
            PlayerName = reader.ReadUTF();
            PlayerId = reader.ReadVarLong();
            Look = new EntityLook();
            Look.Deserialize(reader);
        }
    }
}