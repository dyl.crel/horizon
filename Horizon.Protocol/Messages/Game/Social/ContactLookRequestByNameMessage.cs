namespace Horizon.Protocol.Messages.Game.Social
{
    public class ContactLookRequestByNameMessage : ContactLookRequestMessage
    {
        public override short Protocol => 5933;

        public string PlayerName { get; set; }

        public ContactLookRequestByNameMessage()
        {
        }

        public ContactLookRequestByNameMessage(byte requestId, byte contactType, string playerName) : base(requestId, contactType)
        {
            PlayerName = playerName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(PlayerName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerName = reader.ReadUTF();
        }
    }
}