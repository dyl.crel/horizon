namespace Horizon.Protocol.Messages.Game.Social
{
    public class ContactLookRequestByIdMessage : ContactLookRequestMessage
    {
        public override short Protocol => 5935;

        public long PlayerId { get; set; }

        public ContactLookRequestByIdMessage()
        {
        }

        public ContactLookRequestByIdMessage(byte requestId, byte contactType, long playerId) : base(requestId, contactType)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}