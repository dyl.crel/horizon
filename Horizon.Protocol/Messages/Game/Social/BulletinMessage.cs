namespace Horizon.Protocol.Messages.Game.Social
{
    public class BulletinMessage : SocialNoticeMessage
    {
        public override short Protocol => 6695;

        public int LastNotifiedTimestamp { get; set; }

        public BulletinMessage()
        {
        }

        public BulletinMessage(string content, int timestamp, long memberId, string memberName, int lastNotifiedTimestamp) : base(content, timestamp, memberId, memberName)
        {
            LastNotifiedTimestamp = lastNotifiedTimestamp;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(LastNotifiedTimestamp);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LastNotifiedTimestamp = reader.ReadInt();
        }
    }
}