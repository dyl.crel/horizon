namespace Horizon.Protocol.Messages.Game.Look
{
    public class AccessoryPreviewErrorMessage : NetworkMessage
    {
        public override short Protocol => 6521;

        public byte Error { get; set; }

        public AccessoryPreviewErrorMessage()
        {
        }

        public AccessoryPreviewErrorMessage(byte error)
        {
            Error = error;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Error);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Error = reader.ReadByte();
        }
    }
}