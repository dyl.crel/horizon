using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Messages.Game.Look
{
    public class AccessoryPreviewMessage : NetworkMessage
    {
        public override short Protocol => 6517;

        public EntityLook Look { get; set; }

        public AccessoryPreviewMessage()
        {
        }

        public AccessoryPreviewMessage(EntityLook look)
        {
            Look = look;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Look.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Look = new EntityLook();
            Look.Deserialize(reader);
        }
    }
}