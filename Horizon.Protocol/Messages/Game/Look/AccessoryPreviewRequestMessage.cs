namespace Horizon.Protocol.Messages.Game.Look
{
    public class AccessoryPreviewRequestMessage : NetworkMessage
    {
        public override short Protocol => 6518;

        public short[] GenericId { get; set; }

        public AccessoryPreviewRequestMessage()
        {
        }

        public AccessoryPreviewRequestMessage(short[] genericId)
        {
            GenericId = genericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(GenericId.Length);
            for (var i = 0; i < GenericId.Length; i++)
            {
                writer.WriteVarShort(GenericId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GenericId = new short[reader.ReadShort()];
            for (var i = 0; i < GenericId.Length; i++)
            {
                GenericId[i] = reader.ReadVarShort();
            }
        }
    }
}