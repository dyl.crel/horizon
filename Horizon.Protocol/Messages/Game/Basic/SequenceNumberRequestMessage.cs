namespace Horizon.Protocol.Messages.Game.Basic
{
    public class SequenceNumberRequestMessage : NetworkMessage
    {
        public override short Protocol => 6316;

        public SequenceNumberRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}