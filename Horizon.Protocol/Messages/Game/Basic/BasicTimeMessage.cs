namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicTimeMessage : NetworkMessage
    {
        public override short Protocol => 175;

        public double Timestamp { get; set; }

        public short TimezoneOffset { get; set; }

        public BasicTimeMessage()
        {
        }

        public BasicTimeMessage(double timestamp, short timezoneOffset)
        {
            Timestamp = timestamp;
            TimezoneOffset = timezoneOffset;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Timestamp);
            writer.WriteShort(TimezoneOffset);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Timestamp = reader.ReadDouble();
            TimezoneOffset = reader.ReadShort();
        }
    }
}