namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicNoOperationMessage : NetworkMessage
    {
        public override short Protocol => 176;

        public BasicNoOperationMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}