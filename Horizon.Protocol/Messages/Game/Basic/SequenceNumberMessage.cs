namespace Horizon.Protocol.Messages.Game.Basic
{
    public class SequenceNumberMessage : NetworkMessage
    {
        public override short Protocol => 6317;

        public short Number { get; set; }

        public SequenceNumberMessage()
        {
        }

        public SequenceNumberMessage(short number)
        {
            Number = number;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Number);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Number = reader.ReadShort();
        }
    }
}