namespace Horizon.Protocol.Messages.Game.Basic
{
    public class NumericWhoIsMessage : NetworkMessage
    {
        public override short Protocol => 6297;

        public long PlayerId { get; set; }

        public int AccountId { get; set; }

        public NumericWhoIsMessage()
        {
        }

        public NumericWhoIsMessage(long playerId, int accountId)
        {
            PlayerId = playerId;
            AccountId = accountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteInt(AccountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
            AccountId = reader.ReadInt();
        }
    }
}