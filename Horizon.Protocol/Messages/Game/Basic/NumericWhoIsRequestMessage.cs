namespace Horizon.Protocol.Messages.Game.Basic
{
    public class NumericWhoIsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6298;

        public long PlayerId { get; set; }

        public NumericWhoIsRequestMessage()
        {
        }

        public NumericWhoIsRequestMessage(long playerId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
        }
    }
}