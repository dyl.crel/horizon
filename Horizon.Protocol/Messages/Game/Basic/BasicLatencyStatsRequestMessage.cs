namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicLatencyStatsRequestMessage : NetworkMessage
    {
        public override short Protocol => 5816;

        public BasicLatencyStatsRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}