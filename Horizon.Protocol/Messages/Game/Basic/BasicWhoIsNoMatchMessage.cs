namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicWhoIsNoMatchMessage : NetworkMessage
    {
        public override short Protocol => 179;

        public string Search { get; set; }

        public BasicWhoIsNoMatchMessage()
        {
        }

        public BasicWhoIsNoMatchMessage(string search)
        {
            Search = search;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Search);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Search = reader.ReadUTF();
        }
    }
}