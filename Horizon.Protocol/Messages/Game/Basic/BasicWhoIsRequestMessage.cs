namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicWhoIsRequestMessage : NetworkMessage
    {
        public override short Protocol => 181;

        public bool Verbose { get; set; }

        public string Search { get; set; }

        public BasicWhoIsRequestMessage()
        {
        }

        public BasicWhoIsRequestMessage(bool verbose, string search)
        {
            Verbose = verbose;
            Search = search;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Verbose);
            writer.WriteUTF(Search);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Verbose = reader.ReadBoolean();
            Search = reader.ReadUTF();
        }
    }
}