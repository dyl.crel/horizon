namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicWhoAmIRequestMessage : NetworkMessage
    {
        public override short Protocol => 5664;

        public bool Verbose { get; set; }

        public BasicWhoAmIRequestMessage()
        {
        }

        public BasicWhoAmIRequestMessage(bool verbose)
        {
            Verbose = verbose;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Verbose);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Verbose = reader.ReadBoolean();
        }
    }
}