namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicAckMessage : NetworkMessage
    {
        public override short Protocol => 6362;

        public int Seq { get; set; }

        public short LastPacketId { get; set; }

        public BasicAckMessage()
        {
        }

        public BasicAckMessage(int seq, short lastPacketId)
        {
            Seq = seq;
            LastPacketId = lastPacketId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Seq);
            writer.WriteVarShort(LastPacketId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Seq = reader.ReadVarInt();
            LastPacketId = reader.ReadVarShort();
        }
    }
}