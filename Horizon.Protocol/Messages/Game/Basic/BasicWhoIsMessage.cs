using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicWhoIsMessage : NetworkMessage
    {
        public override short Protocol => 180;

        public bool Self { get; set; }

        public bool Verbose { get; set; }

        public byte Position { get; set; }

        public string AccountNickname { get; set; }

        public int AccountId { get; set; }

        public string PlayerName { get; set; }

        public long PlayerId { get; set; }

        public short AreaId { get; set; }

        public short ServerId { get; set; }

        public short OriginServerId { get; set; }

        public AbstractSocialGroupInfos[] SocialGroups { get; set; }

        public byte PlayerState { get; set; }

        public BasicWhoIsMessage()
        {
        }

        public BasicWhoIsMessage(bool self, bool verbose, byte position, string accountNickname, int accountId, string playerName, long playerId, short areaId, short serverId, short originServerId, AbstractSocialGroupInfos[] socialGroups, byte playerState)
        {
            Self = self;
            Verbose = verbose;
            Position = position;
            AccountNickname = accountNickname;
            AccountId = accountId;
            PlayerName = playerName;
            PlayerId = playerId;
            AreaId = areaId;
            ServerId = serverId;
            OriginServerId = originServerId;
            SocialGroups = socialGroups;
            PlayerState = playerState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Self);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Verbose);
            writer.WriteByte(flag0);
            writer.WriteByte(Position);
            writer.WriteUTF(AccountNickname);
            writer.WriteInt(AccountId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarLong(PlayerId);
            writer.WriteShort(AreaId);
            writer.WriteShort(ServerId);
            writer.WriteShort(OriginServerId);
            writer.WriteShort(SocialGroups.Length);
            for (var i = 0; i < SocialGroups.Length; i++)
            {
                writer.WriteShort(SocialGroups[i].Protocol);
                SocialGroups[i].Serialize(writer);
            }
            writer.WriteByte(PlayerState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Self = BooleanByteWrapper.GetFlag(flag0, 0);
            Verbose = BooleanByteWrapper.GetFlag(flag0, 1);
            Position = reader.ReadByte();
            AccountNickname = reader.ReadUTF();
            AccountId = reader.ReadInt();
            PlayerName = reader.ReadUTF();
            PlayerId = reader.ReadVarLong();
            AreaId = reader.ReadShort();
            ServerId = reader.ReadShort();
            OriginServerId = reader.ReadShort();
            SocialGroups = new AbstractSocialGroupInfos[reader.ReadShort()];
            for (var i = 0; i < SocialGroups.Length; i++)
            {
                SocialGroups[i] = ProtocolTypesManager.Instance<AbstractSocialGroupInfos>(reader.ReadShort());
                SocialGroups[i].Deserialize(reader);
            }
            PlayerState = reader.ReadByte();
        }
    }
}