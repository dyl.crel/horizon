namespace Horizon.Protocol.Messages.Game.Basic
{
    public class TextInformationMessage : NetworkMessage
    {
        public override short Protocol => 780;

        public byte MsgType { get; set; }

        public short MsgId { get; set; }

        public string[] Parameters { get; set; }

        public TextInformationMessage()
        {
        }

        public TextInformationMessage(byte msgType, short msgId, string[] parameters)
        {
            MsgType = msgType;
            MsgId = msgId;
            Parameters = parameters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(MsgType);
            writer.WriteVarShort(MsgId);
            writer.WriteShort(Parameters.Length);
            for (var i = 0; i < Parameters.Length; i++)
            {
                writer.WriteUTF(Parameters[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MsgType = reader.ReadByte();
            MsgId = reader.ReadVarShort();
            Parameters = new string[reader.ReadShort()];
            for (var i = 0; i < Parameters.Length; i++)
            {
                Parameters[i] = reader.ReadUTF();
            }
        }
    }
}