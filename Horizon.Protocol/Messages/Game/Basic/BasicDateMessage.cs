namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicDateMessage : NetworkMessage
    {
        public override short Protocol => 177;

        public byte Day { get; set; }

        public byte Month { get; set; }

        public short Year { get; set; }

        public BasicDateMessage()
        {
        }

        public BasicDateMessage(byte day, byte month, short year)
        {
            Day = day;
            Month = month;
            Year = year;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Day);
            writer.WriteByte(Month);
            writer.WriteShort(Year);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Day = reader.ReadByte();
            Month = reader.ReadByte();
            Year = reader.ReadShort();
        }
    }
}