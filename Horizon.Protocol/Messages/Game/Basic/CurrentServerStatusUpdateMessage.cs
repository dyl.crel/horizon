namespace Horizon.Protocol.Messages.Game.Basic
{
    public class CurrentServerStatusUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6525;

        public byte Status { get; set; }

        public CurrentServerStatusUpdateMessage()
        {
        }

        public CurrentServerStatusUpdateMessage(byte status)
        {
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Status);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Status = reader.ReadByte();
        }
    }
}