namespace Horizon.Protocol.Messages.Game.Basic
{
    public class BasicLatencyStatsMessage : NetworkMessage
    {
        public override short Protocol => 5663;

        public short Latency { get; set; }

        public short SampleCount { get; set; }

        public short Max { get; set; }

        public BasicLatencyStatsMessage()
        {
        }

        public BasicLatencyStatsMessage(short latency, short sampleCount, short max)
        {
            Latency = latency;
            SampleCount = sampleCount;
            Max = max;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Latency);
            writer.WriteVarShort(SampleCount);
            writer.WriteVarShort(Max);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Latency = reader.ReadShort();
            SampleCount = reader.ReadVarShort();
            Max = reader.ReadVarShort();
        }
    }
}