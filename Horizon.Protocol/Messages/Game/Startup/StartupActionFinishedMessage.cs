namespace Horizon.Protocol.Messages.Game.Startup
{
    public class StartupActionFinishedMessage : NetworkMessage
    {
        public override short Protocol => 1304;

        public bool Success { get; set; }

        public bool AutomaticAction { get; set; }

        public int ActionId { get; set; }

        public StartupActionFinishedMessage()
        {
        }

        public StartupActionFinishedMessage(bool success, bool automaticAction, int actionId)
        {
            Success = success;
            AutomaticAction = automaticAction;
            ActionId = actionId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Success);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, AutomaticAction);
            writer.WriteByte(flag0);
            writer.WriteInt(ActionId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag0, 0);
            AutomaticAction = BooleanByteWrapper.GetFlag(flag0, 1);
            ActionId = reader.ReadInt();
        }
    }
}