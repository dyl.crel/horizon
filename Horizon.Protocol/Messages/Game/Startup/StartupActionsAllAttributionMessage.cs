namespace Horizon.Protocol.Messages.Game.Startup
{
    public class StartupActionsAllAttributionMessage : NetworkMessage
    {
        public override short Protocol => 6537;

        public long CharacterId { get; set; }

        public StartupActionsAllAttributionMessage()
        {
        }

        public StartupActionsAllAttributionMessage(long characterId)
        {
            CharacterId = characterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(CharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CharacterId = reader.ReadVarLong();
        }
    }
}