using Horizon.Protocol.Types.Game.Startup;

namespace Horizon.Protocol.Messages.Game.Startup
{
    public class StartupActionAddMessage : NetworkMessage
    {
        public override short Protocol => 6538;

        public StartupActionAddObject NewAction { get; set; }

        public StartupActionAddMessage()
        {
        }

        public StartupActionAddMessage(StartupActionAddObject newAction)
        {
            NewAction = newAction;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            NewAction.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NewAction = new StartupActionAddObject();
            NewAction.Deserialize(reader);
        }
    }
}