namespace Horizon.Protocol.Messages.Game.Startup
{
    public class StartupActionsExecuteMessage : NetworkMessage
    {
        public override short Protocol => 1302;

        public StartupActionsExecuteMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}