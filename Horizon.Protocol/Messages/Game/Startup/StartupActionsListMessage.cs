using Horizon.Protocol.Types.Game.Startup;

namespace Horizon.Protocol.Messages.Game.Startup
{
    public class StartupActionsListMessage : NetworkMessage
    {
        public override short Protocol => 1301;

        public StartupActionAddObject[] Actions { get; set; }

        public StartupActionsListMessage()
        {
        }

        public StartupActionsListMessage(StartupActionAddObject[] actions)
        {
            Actions = actions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Actions.Length);
            for (var i = 0; i < Actions.Length; i++)
            {
                Actions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Actions = new StartupActionAddObject[reader.ReadShort()];
            for (var i = 0; i < Actions.Length; i++)
            {
                Actions[i] = new StartupActionAddObject();
                Actions[i].Deserialize(reader);
            }
        }
    }
}