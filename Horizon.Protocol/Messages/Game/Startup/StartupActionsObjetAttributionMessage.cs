namespace Horizon.Protocol.Messages.Game.Startup
{
    public class StartupActionsObjetAttributionMessage : NetworkMessage
    {
        public override short Protocol => 1303;

        public int ActionId { get; set; }

        public long CharacterId { get; set; }

        public StartupActionsObjetAttributionMessage()
        {
        }

        public StartupActionsObjetAttributionMessage(int actionId, long characterId)
        {
            ActionId = actionId;
            CharacterId = characterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(ActionId);
            writer.WriteVarLong(CharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActionId = reader.ReadInt();
            CharacterId = reader.ReadVarLong();
        }
    }
}