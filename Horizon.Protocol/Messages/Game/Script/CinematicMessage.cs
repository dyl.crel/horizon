namespace Horizon.Protocol.Messages.Game.Script
{
    public class CinematicMessage : NetworkMessage
    {
        public override short Protocol => 6053;

        public short CinematicId { get; set; }

        public CinematicMessage()
        {
        }

        public CinematicMessage(short cinematicId)
        {
            CinematicId = cinematicId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CinematicId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CinematicId = reader.ReadVarShort();
        }
    }
}