namespace Horizon.Protocol.Messages.Game.Script
{
    public class URLOpenMessage : NetworkMessage
    {
        public override short Protocol => 6266;

        public byte UrlId { get; set; }

        public URLOpenMessage()
        {
        }

        public URLOpenMessage(byte urlId)
        {
            UrlId = urlId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(UrlId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            UrlId = reader.ReadByte();
        }
    }
}