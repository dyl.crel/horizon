namespace Horizon.Protocol.Messages.Game.Initialization
{
    public class OnConnectionEventMessage : NetworkMessage
    {
        public override short Protocol => 5726;

        public byte EventType { get; set; }

        public OnConnectionEventMessage()
        {
        }

        public OnConnectionEventMessage(byte eventType)
        {
            EventType = eventType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(EventType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EventType = reader.ReadByte();
        }
    }
}