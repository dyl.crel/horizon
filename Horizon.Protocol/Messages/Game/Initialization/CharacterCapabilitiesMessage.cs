namespace Horizon.Protocol.Messages.Game.Initialization
{
    public class CharacterCapabilitiesMessage : NetworkMessage
    {
        public override short Protocol => 6339;

        public int GuildEmblemSymbolCategories { get; set; }

        public CharacterCapabilitiesMessage()
        {
        }

        public CharacterCapabilitiesMessage(int guildEmblemSymbolCategories)
        {
            GuildEmblemSymbolCategories = guildEmblemSymbolCategories;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(GuildEmblemSymbolCategories);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildEmblemSymbolCategories = reader.ReadVarInt();
        }
    }
}