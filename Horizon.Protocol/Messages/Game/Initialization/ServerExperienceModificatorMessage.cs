namespace Horizon.Protocol.Messages.Game.Initialization
{
    public class ServerExperienceModificatorMessage : NetworkMessage
    {
        public override short Protocol => 6237;

        public short ExperiencePercent { get; set; }

        public ServerExperienceModificatorMessage()
        {
        }

        public ServerExperienceModificatorMessage(short experiencePercent)
        {
            ExperiencePercent = experiencePercent;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ExperiencePercent);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExperiencePercent = reader.ReadVarShort();
        }
    }
}