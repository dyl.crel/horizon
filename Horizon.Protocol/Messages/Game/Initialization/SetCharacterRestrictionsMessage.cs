using Horizon.Protocol.Types.Game.Character.Restriction;

namespace Horizon.Protocol.Messages.Game.Initialization
{
    public class SetCharacterRestrictionsMessage : NetworkMessage
    {
        public override short Protocol => 170;

        public double ActorId { get; set; }

        public ActorRestrictionsInformations Restrictions { get; set; }

        public SetCharacterRestrictionsMessage()
        {
        }

        public SetCharacterRestrictionsMessage(double actorId, ActorRestrictionsInformations restrictions)
        {
            ActorId = actorId;
            Restrictions = restrictions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(ActorId);
            Restrictions.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActorId = reader.ReadDouble();
            Restrictions = new ActorRestrictionsInformations();
            Restrictions.Deserialize(reader);
        }
    }
}