namespace Horizon.Protocol.Messages.Game.Modificator
{
    public class AreaFightModificatorUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6493;

        public int SpellPairId { get; set; }

        public AreaFightModificatorUpdateMessage()
        {
        }

        public AreaFightModificatorUpdateMessage(int spellPairId)
        {
            SpellPairId = spellPairId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(SpellPairId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellPairId = reader.ReadInt();
        }
    }
}