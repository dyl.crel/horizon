namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolPartyRegisterRequestMessage : NetworkMessage
    {
        public override short Protocol => 6582;

        public bool Register { get; set; }

        public IdolPartyRegisterRequestMessage()
        {
        }

        public IdolPartyRegisterRequestMessage(bool register)
        {
            Register = register;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Register);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Register = reader.ReadBoolean();
        }
    }
}