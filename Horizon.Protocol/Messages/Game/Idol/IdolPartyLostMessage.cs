namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolPartyLostMessage : NetworkMessage
    {
        public override short Protocol => 6580;

        public short IdolId { get; set; }

        public IdolPartyLostMessage()
        {
        }

        public IdolPartyLostMessage(short idolId)
        {
            IdolId = idolId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(IdolId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IdolId = reader.ReadVarShort();
        }
    }
}