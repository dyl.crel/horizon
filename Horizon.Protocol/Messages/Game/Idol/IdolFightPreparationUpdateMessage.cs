namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolFightPreparationUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6586;

        public byte IdolSource { get; set; }

        public Types.Game.Idol.Idol[] Idols { get; set; }

        public IdolFightPreparationUpdateMessage()
        {
        }

        public IdolFightPreparationUpdateMessage(byte idolSource, Types.Game.Idol.Idol[] idols)
        {
            IdolSource = idolSource;
            Idols = idols;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(IdolSource);
            writer.WriteShort(Idols.Length);
            for (var i = 0; i < Idols.Length; i++)
            {
                writer.WriteShort(Idols[i].Protocol);
                Idols[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IdolSource = reader.ReadByte();
            Idols = new Types.Game.Idol.Idol[reader.ReadShort()];
            for (var i = 0; i < Idols.Length; i++)
            {
                Idols[i] = ProtocolTypesManager.Instance<Types.Game.Idol.Idol>(reader.ReadShort());
                Idols[i].Deserialize(reader);
            }
        }
    }
}