using Horizon.Protocol.Types.Game.Idol;

namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolPartyRefreshMessage : NetworkMessage
    {
        public override short Protocol => 6583;

        public PartyIdol PartyIdol { get; set; }

        public IdolPartyRefreshMessage()
        {
        }

        public IdolPartyRefreshMessage(PartyIdol partyIdol)
        {
            PartyIdol = partyIdol;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            PartyIdol.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PartyIdol = new PartyIdol();
            PartyIdol.Deserialize(reader);
        }
    }
}