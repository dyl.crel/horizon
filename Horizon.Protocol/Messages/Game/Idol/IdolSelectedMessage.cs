namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolSelectedMessage : NetworkMessage
    {
        public override short Protocol => 6581;

        public bool Activate { get; set; }

        public bool Party { get; set; }

        public short IdolId { get; set; }

        public IdolSelectedMessage()
        {
        }

        public IdolSelectedMessage(bool activate, bool party, short idolId)
        {
            Activate = activate;
            Party = party;
            IdolId = idolId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Activate);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Party);
            writer.WriteByte(flag0);
            writer.WriteVarShort(IdolId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Activate = BooleanByteWrapper.GetFlag(flag0, 0);
            Party = BooleanByteWrapper.GetFlag(flag0, 1);
            IdolId = reader.ReadVarShort();
        }
    }
}