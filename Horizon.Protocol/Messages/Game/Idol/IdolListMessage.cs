using Horizon.Protocol.Types.Game.Idol;

namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolListMessage : NetworkMessage
    {
        public override short Protocol => 6585;

        public short[] ChosenIdols { get; set; }

        public short[] PartyChosenIdols { get; set; }

        public PartyIdol[] PartyIdols { get; set; }

        public IdolListMessage()
        {
        }

        public IdolListMessage(short[] chosenIdols, short[] partyChosenIdols, PartyIdol[] partyIdols)
        {
            ChosenIdols = chosenIdols;
            PartyChosenIdols = partyChosenIdols;
            PartyIdols = partyIdols;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ChosenIdols.Length);
            for (var i = 0; i < ChosenIdols.Length; i++)
            {
                writer.WriteVarShort(ChosenIdols[i]);
            }
            writer.WriteShort(PartyChosenIdols.Length);
            for (var i = 0; i < PartyChosenIdols.Length; i++)
            {
                writer.WriteVarShort(PartyChosenIdols[i]);
            }
            writer.WriteShort(PartyIdols.Length);
            for (var i = 0; i < PartyIdols.Length; i++)
            {
                writer.WriteShort(PartyIdols[i].Protocol);
                PartyIdols[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ChosenIdols = new short[reader.ReadShort()];
            for (var i = 0; i < ChosenIdols.Length; i++)
            {
                ChosenIdols[i] = reader.ReadVarShort();
            }
            PartyChosenIdols = new short[reader.ReadShort()];
            for (var i = 0; i < PartyChosenIdols.Length; i++)
            {
                PartyChosenIdols[i] = reader.ReadVarShort();
            }
            PartyIdols = new PartyIdol[reader.ReadShort()];
            for (var i = 0; i < PartyIdols.Length; i++)
            {
                PartyIdols[i] = ProtocolTypesManager.Instance<PartyIdol>(reader.ReadShort());
                PartyIdols[i].Deserialize(reader);
            }
        }
    }
}