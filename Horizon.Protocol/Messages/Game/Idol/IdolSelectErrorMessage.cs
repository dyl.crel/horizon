namespace Horizon.Protocol.Messages.Game.Idol
{
    public class IdolSelectErrorMessage : NetworkMessage
    {
        public override short Protocol => 6584;

        public bool Activate { get; set; }

        public bool Party { get; set; }

        public byte Reason { get; set; }

        public short IdolId { get; set; }

        public IdolSelectErrorMessage()
        {
        }

        public IdolSelectErrorMessage(bool activate, bool party, byte reason, short idolId)
        {
            Activate = activate;
            Party = party;
            Reason = reason;
            IdolId = idolId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Activate);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Party);
            writer.WriteByte(flag0);
            writer.WriteByte(Reason);
            writer.WriteVarShort(IdolId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Activate = BooleanByteWrapper.GetFlag(flag0, 0);
            Party = BooleanByteWrapper.GetFlag(flag0, 1);
            Reason = reader.ReadByte();
            IdolId = reader.ReadVarShort();
        }
    }
}