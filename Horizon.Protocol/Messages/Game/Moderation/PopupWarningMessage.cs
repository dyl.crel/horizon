namespace Horizon.Protocol.Messages.Game.Moderation
{
    public class PopupWarningMessage : NetworkMessage
    {
        public override short Protocol => 6134;

        public byte LockDuration { get; set; }

        public string Author { get; set; }

        public string Content { get; set; }

        public PopupWarningMessage()
        {
        }

        public PopupWarningMessage(byte lockDuration, string author, string content)
        {
            LockDuration = lockDuration;
            Author = author;
            Content = content;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(LockDuration);
            writer.WriteUTF(Author);
            writer.WriteUTF(Content);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            LockDuration = reader.ReadByte();
            Author = reader.ReadUTF();
            Content = reader.ReadUTF();
        }
    }
}