namespace Horizon.Protocol.Messages.Game.Actions
{
    public class GameActionNoopMessage : NetworkMessage
    {
        public override short Protocol => 1002;

        public GameActionNoopMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}