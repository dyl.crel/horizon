namespace Horizon.Protocol.Messages.Game.Actions
{
    public class AbstractGameActionMessage : NetworkMessage
    {
        public override short Protocol => 1000;

        public short ActionId { get; set; }

        public double SourceId { get; set; }

        public AbstractGameActionMessage()
        {
        }

        public AbstractGameActionMessage(short actionId, double sourceId)
        {
            ActionId = actionId;
            SourceId = sourceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ActionId);
            writer.WriteDouble(SourceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActionId = reader.ReadVarShort();
            SourceId = reader.ReadDouble();
        }
    }
}