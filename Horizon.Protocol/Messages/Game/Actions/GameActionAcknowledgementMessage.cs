namespace Horizon.Protocol.Messages.Game.Actions
{
    public class GameActionAcknowledgementMessage : NetworkMessage
    {
        public override short Protocol => 957;

        public bool Valid { get; set; }

        public byte ActionId { get; set; }

        public GameActionAcknowledgementMessage()
        {
        }

        public GameActionAcknowledgementMessage(bool valid, byte actionId)
        {
            Valid = valid;
            ActionId = actionId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Valid);
            writer.WriteByte(ActionId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Valid = reader.ReadBoolean();
            ActionId = reader.ReadByte();
        }
    }
}