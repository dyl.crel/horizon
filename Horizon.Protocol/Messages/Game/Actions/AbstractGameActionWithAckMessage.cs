namespace Horizon.Protocol.Messages.Game.Actions
{
    public class AbstractGameActionWithAckMessage : AbstractGameActionMessage
    {
        public override short Protocol => 1001;

        public short WaitAckId { get; set; }

        public AbstractGameActionWithAckMessage()
        {
        }

        public AbstractGameActionWithAckMessage(short actionId, double sourceId, short waitAckId) : base(actionId, sourceId)
        {
            WaitAckId = waitAckId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WaitAckId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WaitAckId = reader.ReadShort();
        }
    }
}