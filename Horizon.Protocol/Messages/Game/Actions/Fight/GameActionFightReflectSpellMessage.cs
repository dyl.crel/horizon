namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightReflectSpellMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5531;

        public double TargetId { get; set; }

        public GameActionFightReflectSpellMessage()
        {
        }

        public GameActionFightReflectSpellMessage(short actionId, double sourceId, double targetId) : base(actionId, sourceId)
        {
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
        }
    }
}