namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightSpellCooldownVariationMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6219;

        public double TargetId { get; set; }

        public short SpellId { get; set; }

        public short Value { get; set; }

        public GameActionFightSpellCooldownVariationMessage()
        {
        }

        public GameActionFightSpellCooldownVariationMessage(short actionId, double sourceId, double targetId, short spellId, short value) : base(actionId, sourceId)
        {
            TargetId = targetId;
            SpellId = spellId;
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarShort(SpellId);
            writer.WriteVarShort(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            SpellId = reader.ReadVarShort();
            Value = reader.ReadVarShort();
        }
    }
}