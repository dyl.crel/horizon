namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightTackledMessage : AbstractGameActionMessage
    {
        public override short Protocol => 1004;

        public double[] TacklersIds { get; set; }

        public GameActionFightTackledMessage()
        {
        }

        public GameActionFightTackledMessage(short actionId, double sourceId, double[] tacklersIds) : base(actionId, sourceId)
        {
            TacklersIds = tacklersIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(TacklersIds.Length);
            for (var i = 0; i < TacklersIds.Length; i++)
            {
                writer.WriteDouble(TacklersIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TacklersIds = new double[reader.ReadShort()];
            for (var i = 0; i < TacklersIds.Length; i++)
            {
                TacklersIds[i] = reader.ReadDouble();
            }
        }
    }
}