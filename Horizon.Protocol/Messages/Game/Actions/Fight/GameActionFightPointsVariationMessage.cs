namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightPointsVariationMessage : AbstractGameActionMessage
    {
        public override short Protocol => 1030;

        public double TargetId { get; set; }

        public short Delta { get; set; }

        public GameActionFightPointsVariationMessage()
        {
        }

        public GameActionFightPointsVariationMessage(short actionId, double sourceId, double targetId, short delta) : base(actionId, sourceId)
        {
            TargetId = targetId;
            Delta = delta;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(Delta);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            Delta = reader.ReadShort();
        }
    }
}