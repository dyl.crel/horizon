namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightActivateGlyphTrapMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6545;

        public short MarkId { get; set; }

        public bool Active { get; set; }

        public GameActionFightActivateGlyphTrapMessage()
        {
        }

        public GameActionFightActivateGlyphTrapMessage(short actionId, double sourceId, short markId, bool active) : base(actionId, sourceId)
        {
            MarkId = markId;
            Active = active;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MarkId);
            writer.WriteBoolean(Active);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MarkId = reader.ReadShort();
            Active = reader.ReadBoolean();
        }
    }
}