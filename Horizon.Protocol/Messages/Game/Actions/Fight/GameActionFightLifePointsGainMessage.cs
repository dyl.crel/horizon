namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightLifePointsGainMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6311;

        public double TargetId { get; set; }

        public int Delta { get; set; }

        public GameActionFightLifePointsGainMessage()
        {
        }

        public GameActionFightLifePointsGainMessage(short actionId, double sourceId, double targetId, int delta) : base(actionId, sourceId)
        {
            TargetId = targetId;
            Delta = delta;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(Delta);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            Delta = reader.ReadVarInt();
        }
    }
}