namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightReduceDamagesMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5526;

        public double TargetId { get; set; }

        public int Amount { get; set; }

        public GameActionFightReduceDamagesMessage()
        {
        }

        public GameActionFightReduceDamagesMessage(short actionId, double sourceId, double targetId, int amount) : base(actionId, sourceId)
        {
            TargetId = targetId;
            Amount = amount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(Amount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            Amount = reader.ReadVarInt();
        }
    }
}