namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightUnmarkCellsMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5570;

        public short MarkId { get; set; }

        public GameActionFightUnmarkCellsMessage()
        {
        }

        public GameActionFightUnmarkCellsMessage(short actionId, double sourceId, short markId) : base(actionId, sourceId)
        {
            MarkId = markId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MarkId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MarkId = reader.ReadShort();
        }
    }
}