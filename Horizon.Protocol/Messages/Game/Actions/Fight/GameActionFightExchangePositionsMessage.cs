namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightExchangePositionsMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5527;

        public double TargetId { get; set; }

        public short CasterCellId { get; set; }

        public short TargetCellId { get; set; }

        public GameActionFightExchangePositionsMessage()
        {
        }

        public GameActionFightExchangePositionsMessage(short actionId, double sourceId, double targetId, short casterCellId, short targetCellId) : base(actionId, sourceId)
        {
            TargetId = targetId;
            CasterCellId = casterCellId;
            TargetCellId = targetCellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(CasterCellId);
            writer.WriteShort(TargetCellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            CasterCellId = reader.ReadShort();
            TargetCellId = reader.ReadShort();
        }
    }
}