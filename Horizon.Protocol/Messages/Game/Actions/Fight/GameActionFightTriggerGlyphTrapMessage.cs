namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightTriggerGlyphTrapMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5741;

        public short MarkId { get; set; }

        public short MarkImpactCell { get; set; }

        public double TriggeringCharacterId { get; set; }

        public short TriggeredSpellId { get; set; }

        public GameActionFightTriggerGlyphTrapMessage()
        {
        }

        public GameActionFightTriggerGlyphTrapMessage(short actionId, double sourceId, short markId, short markImpactCell, double triggeringCharacterId, short triggeredSpellId) : base(actionId, sourceId)
        {
            MarkId = markId;
            MarkImpactCell = markImpactCell;
            TriggeringCharacterId = triggeringCharacterId;
            TriggeredSpellId = triggeredSpellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MarkId);
            writer.WriteVarShort(MarkImpactCell);
            writer.WriteDouble(TriggeringCharacterId);
            writer.WriteVarShort(TriggeredSpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MarkId = reader.ReadShort();
            MarkImpactCell = reader.ReadVarShort();
            TriggeringCharacterId = reader.ReadDouble();
            TriggeredSpellId = reader.ReadVarShort();
        }
    }
}