namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightCastRequestMessage : NetworkMessage
    {
        public override short Protocol => 1005;

        public short SpellId { get; set; }

        public short CellId { get; set; }

        public GameActionFightCastRequestMessage()
        {
        }

        public GameActionFightCastRequestMessage(short spellId, short cellId)
        {
            SpellId = spellId;
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadVarShort();
            CellId = reader.ReadShort();
        }
    }
}