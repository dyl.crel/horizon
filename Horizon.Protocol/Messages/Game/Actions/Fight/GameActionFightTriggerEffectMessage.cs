namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightTriggerEffectMessage : GameActionFightDispellEffectMessage
    {
        public override short Protocol => 6147;

        public GameActionFightTriggerEffectMessage()
        {
        }

        public GameActionFightTriggerEffectMessage(short actionId, double sourceId, double targetId, bool verboseCast, int boostUID) : base(actionId, sourceId, targetId, verboseCast, boostUID)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}