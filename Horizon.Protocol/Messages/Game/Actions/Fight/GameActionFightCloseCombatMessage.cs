namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightCloseCombatMessage : AbstractGameActionFightTargetedAbilityMessage
    {
        public override short Protocol => 6116;

        public short WeaponGenericId { get; set; }

        public GameActionFightCloseCombatMessage()
        {
        }

        public GameActionFightCloseCombatMessage(short actionId, double sourceId, bool silentCast, bool verboseCast, double targetId, short destinationCellId, byte critical, short weaponGenericId) : base(actionId, sourceId, silentCast, verboseCast, targetId, destinationCellId, critical)
        {
            WeaponGenericId = weaponGenericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(WeaponGenericId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WeaponGenericId = reader.ReadVarShort();
        }
    }
}