namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class AbstractGameActionFightTargetedAbilityMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6118;

        public bool SilentCast { get; set; }

        public bool VerboseCast { get; set; }

        public double TargetId { get; set; }

        public short DestinationCellId { get; set; }

        public byte Critical { get; set; }

        public AbstractGameActionFightTargetedAbilityMessage()
        {
        }

        public AbstractGameActionFightTargetedAbilityMessage(short actionId, double sourceId, bool silentCast, bool verboseCast, double targetId, short destinationCellId, byte critical) : base(actionId, sourceId)
        {
            SilentCast = silentCast;
            VerboseCast = verboseCast;
            TargetId = targetId;
            DestinationCellId = destinationCellId;
            Critical = critical;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, SilentCast);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, VerboseCast);
            writer.WriteByte(flag0);
            writer.WriteDouble(TargetId);
            writer.WriteShort(DestinationCellId);
            writer.WriteByte(Critical);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            SilentCast = BooleanByteWrapper.GetFlag(flag0, 0);
            VerboseCast = BooleanByteWrapper.GetFlag(flag0, 1);
            TargetId = reader.ReadDouble();
            DestinationCellId = reader.ReadShort();
            Critical = reader.ReadByte();
        }
    }
}