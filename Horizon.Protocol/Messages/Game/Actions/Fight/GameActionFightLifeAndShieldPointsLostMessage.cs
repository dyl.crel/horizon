namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightLifeAndShieldPointsLostMessage : GameActionFightLifePointsLostMessage
    {
        public override short Protocol => 6310;

        public short ShieldLoss { get; set; }

        public GameActionFightLifeAndShieldPointsLostMessage()
        {
        }

        public GameActionFightLifeAndShieldPointsLostMessage(short actionId, double sourceId, double targetId, int loss, int permanentDamages, int elementId, short shieldLoss) : base(actionId, sourceId, targetId, loss, permanentDamages, elementId)
        {
            ShieldLoss = shieldLoss;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ShieldLoss);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ShieldLoss = reader.ReadVarShort();
        }
    }
}