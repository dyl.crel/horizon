namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightKillMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5571;

        public double TargetId { get; set; }

        public GameActionFightKillMessage()
        {
        }

        public GameActionFightKillMessage(short actionId, double sourceId, double targetId) : base(actionId, sourceId)
        {
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
        }
    }
}