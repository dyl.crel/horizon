namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightSpellCastMessage : AbstractGameActionFightTargetedAbilityMessage
    {
        public override short Protocol => 1010;

        public short SpellId { get; set; }

        public short SpellLevel { get; set; }

        public short[] PortalsIds { get; set; }

        public GameActionFightSpellCastMessage()
        {
        }

        public GameActionFightSpellCastMessage(short actionId, double sourceId, bool silentCast, bool verboseCast, double targetId, short destinationCellId, byte critical, short spellId, short spellLevel, short[] portalsIds) : base(actionId, sourceId, silentCast, verboseCast, targetId, destinationCellId, critical)
        {
            SpellId = spellId;
            SpellLevel = spellLevel;
            PortalsIds = portalsIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SpellId);
            writer.WriteShort(SpellLevel);
            writer.WriteShort(PortalsIds.Length);
            for (var i = 0; i < PortalsIds.Length; i++)
            {
                writer.WriteShort(PortalsIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadVarShort();
            SpellLevel = reader.ReadShort();
            PortalsIds = new short[reader.ReadShort()];
            for (var i = 0; i < PortalsIds.Length; i++)
            {
                PortalsIds[i] = reader.ReadShort();
            }
        }
    }
}