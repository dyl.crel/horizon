using Horizon.Protocol.Types.Game.Actions.Fight;

namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightMarkCellsMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5540;

        public GameActionMark Mark { get; set; }

        public GameActionFightMarkCellsMessage()
        {
        }

        public GameActionFightMarkCellsMessage(short actionId, double sourceId, GameActionMark mark) : base(actionId, sourceId)
        {
            Mark = mark;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Mark.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Mark = new GameActionMark();
            Mark.Deserialize(reader);
        }
    }
}