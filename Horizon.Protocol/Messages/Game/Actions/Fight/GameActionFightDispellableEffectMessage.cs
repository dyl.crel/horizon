using Horizon.Protocol.Types.Game.Actions.Fight;

namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightDispellableEffectMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6070;

        public AbstractFightDispellableEffect Effect { get; set; }

        public GameActionFightDispellableEffectMessage()
        {
        }

        public GameActionFightDispellableEffectMessage(short actionId, double sourceId, AbstractFightDispellableEffect effect) : base(actionId, sourceId)
        {
            Effect = effect;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Effect.Protocol);
            Effect.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Effect = ProtocolTypesManager.Instance<AbstractFightDispellableEffect>(reader.ReadShort());
            Effect.Deserialize(reader);
        }
    }
}