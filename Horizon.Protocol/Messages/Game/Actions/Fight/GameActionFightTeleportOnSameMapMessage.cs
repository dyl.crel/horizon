namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightTeleportOnSameMapMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5528;

        public double TargetId { get; set; }

        public short CellId { get; set; }

        public GameActionFightTeleportOnSameMapMessage()
        {
        }

        public GameActionFightTeleportOnSameMapMessage(short actionId, double sourceId, double targetId, short cellId) : base(actionId, sourceId)
        {
            TargetId = targetId;
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            CellId = reader.ReadShort();
        }
    }
}