namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightLifePointsLostMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6312;

        public double TargetId { get; set; }

        public int Loss { get; set; }

        public int PermanentDamages { get; set; }

        public int ElementId { get; set; }

        public GameActionFightLifePointsLostMessage()
        {
        }

        public GameActionFightLifePointsLostMessage(short actionId, double sourceId, double targetId, int loss, int permanentDamages, int elementId) : base(actionId, sourceId)
        {
            TargetId = targetId;
            Loss = loss;
            PermanentDamages = permanentDamages;
            ElementId = elementId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(Loss);
            writer.WriteVarInt(PermanentDamages);
            writer.WriteVarInt(ElementId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            Loss = reader.ReadVarInt();
            PermanentDamages = reader.ReadVarInt();
            ElementId = reader.ReadVarInt();
        }
    }
}