namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightVanishMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6217;

        public double TargetId { get; set; }

        public GameActionFightVanishMessage()
        {
        }

        public GameActionFightVanishMessage(short actionId, double sourceId, double targetId) : base(actionId, sourceId)
        {
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
        }
    }
}