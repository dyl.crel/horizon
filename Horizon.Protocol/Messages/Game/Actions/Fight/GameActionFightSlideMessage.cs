namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightSlideMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5525;

        public double TargetId { get; set; }

        public short StartCellId { get; set; }

        public short EndCellId { get; set; }

        public GameActionFightSlideMessage()
        {
        }

        public GameActionFightSlideMessage(short actionId, double sourceId, double targetId, short startCellId, short endCellId) : base(actionId, sourceId)
        {
            TargetId = targetId;
            StartCellId = startCellId;
            EndCellId = endCellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(StartCellId);
            writer.WriteShort(EndCellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            StartCellId = reader.ReadShort();
            EndCellId = reader.ReadShort();
        }
    }
}