namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightDodgePointLossMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5828;

        public double TargetId { get; set; }

        public short Amount { get; set; }

        public GameActionFightDodgePointLossMessage()
        {
        }

        public GameActionFightDodgePointLossMessage(short actionId, double sourceId, double targetId, short amount) : base(actionId, sourceId)
        {
            TargetId = targetId;
            Amount = amount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarShort(Amount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            Amount = reader.ReadVarShort();
        }
    }
}