using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightSummonMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5825;

        public GameFightFighterInformations[] Summons { get; set; }

        public GameActionFightSummonMessage()
        {
        }

        public GameActionFightSummonMessage(short actionId, double sourceId, GameFightFighterInformations[] summons) : base(actionId, sourceId)
        {
            Summons = summons;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Summons.Length);
            for (var i = 0; i < Summons.Length; i++)
            {
                writer.WriteShort(Summons[i].Protocol);
                Summons[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Summons = new GameFightFighterInformations[reader.ReadShort()];
            for (var i = 0; i < Summons.Length; i++)
            {
                Summons[i] = ProtocolTypesManager.Instance<GameFightFighterInformations>(reader.ReadShort());
                Summons[i].Deserialize(reader);
            }
        }
    }
}