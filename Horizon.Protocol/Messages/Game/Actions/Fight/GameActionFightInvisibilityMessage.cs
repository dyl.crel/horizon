namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightInvisibilityMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5821;

        public double TargetId { get; set; }

        public byte State { get; set; }

        public GameActionFightInvisibilityMessage()
        {
        }

        public GameActionFightInvisibilityMessage(short actionId, double sourceId, double targetId, byte state) : base(actionId, sourceId)
        {
            TargetId = targetId;
            State = state;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteByte(State);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            State = reader.ReadByte();
        }
    }
}