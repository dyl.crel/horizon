using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightChangeLookMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5532;

        public double TargetId { get; set; }

        public EntityLook EntityLook { get; set; }

        public GameActionFightChangeLookMessage()
        {
        }

        public GameActionFightChangeLookMessage(short actionId, double sourceId, double targetId, EntityLook entityLook) : base(actionId, sourceId)
        {
            TargetId = targetId;
            EntityLook = entityLook;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            EntityLook.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            EntityLook = new EntityLook();
            EntityLook.Deserialize(reader);
        }
    }
}