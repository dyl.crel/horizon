namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightDispellSpellMessage : GameActionFightDispellMessage
    {
        public override short Protocol => 6176;

        public short SpellId { get; set; }

        public GameActionFightDispellSpellMessage()
        {
        }

        public GameActionFightDispellSpellMessage(short actionId, double sourceId, double targetId, bool verboseCast, short spellId) : base(actionId, sourceId, targetId, verboseCast)
        {
            SpellId = spellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadVarShort();
        }
    }
}