namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightDispellMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5533;

        public double TargetId { get; set; }

        public bool VerboseCast { get; set; }

        public GameActionFightDispellMessage()
        {
        }

        public GameActionFightDispellMessage(short actionId, double sourceId, double targetId, bool verboseCast) : base(actionId, sourceId)
        {
            TargetId = targetId;
            VerboseCast = verboseCast;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteBoolean(VerboseCast);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            VerboseCast = reader.ReadBoolean();
        }
    }
}