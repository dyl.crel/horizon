namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightStealKamaMessage : AbstractGameActionMessage
    {
        public override short Protocol => 5535;

        public double TargetId { get; set; }

        public long Amount { get; set; }

        public GameActionFightStealKamaMessage()
        {
        }

        public GameActionFightStealKamaMessage(short actionId, double sourceId, double targetId, long amount) : base(actionId, sourceId)
        {
            TargetId = targetId;
            Amount = amount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarLong(Amount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            Amount = reader.ReadVarLong();
        }
    }
}