namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightNoSpellCastMessage : NetworkMessage
    {
        public override short Protocol => 6132;

        public int SpellLevelId { get; set; }

        public GameActionFightNoSpellCastMessage()
        {
        }

        public GameActionFightNoSpellCastMessage(int spellLevelId)
        {
            SpellLevelId = spellLevelId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(SpellLevelId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellLevelId = reader.ReadVarInt();
        }
    }
}