namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightCastOnTargetRequestMessage : NetworkMessage
    {
        public override short Protocol => 6330;

        public short SpellId { get; set; }

        public double TargetId { get; set; }

        public GameActionFightCastOnTargetRequestMessage()
        {
        }

        public GameActionFightCastOnTargetRequestMessage(short spellId, double targetId)
        {
            SpellId = spellId;
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadVarShort();
            TargetId = reader.ReadDouble();
        }
    }
}