namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightDispellEffectMessage : GameActionFightDispellMessage
    {
        public override short Protocol => 6113;

        public int BoostUID { get; set; }

        public GameActionFightDispellEffectMessage()
        {
        }

        public GameActionFightDispellEffectMessage(short actionId, double sourceId, double targetId, bool verboseCast, int boostUID) : base(actionId, sourceId, targetId, verboseCast)
        {
            BoostUID = boostUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(BoostUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            BoostUID = reader.ReadInt();
        }
    }
}