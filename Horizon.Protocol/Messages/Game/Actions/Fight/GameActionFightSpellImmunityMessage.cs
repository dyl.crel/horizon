namespace Horizon.Protocol.Messages.Game.Actions.Fight
{
    public class GameActionFightSpellImmunityMessage : AbstractGameActionMessage
    {
        public override short Protocol => 6221;

        public double TargetId { get; set; }

        public short SpellId { get; set; }

        public GameActionFightSpellImmunityMessage()
        {
        }

        public GameActionFightSpellImmunityMessage(short actionId, double sourceId, double targetId, short spellId) : base(actionId, sourceId)
        {
            TargetId = targetId;
            SpellId = spellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarShort(SpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            SpellId = reader.ReadVarShort();
        }
    }
}