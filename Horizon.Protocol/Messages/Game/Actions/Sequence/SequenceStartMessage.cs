namespace Horizon.Protocol.Messages.Game.Actions.Sequence
{
    public class SequenceStartMessage : NetworkMessage
    {
        public override short Protocol => 955;

        public byte SequenceType { get; set; }

        public double AuthorId { get; set; }

        public SequenceStartMessage()
        {
        }

        public SequenceStartMessage(byte sequenceType, double authorId)
        {
            SequenceType = sequenceType;
            AuthorId = authorId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(SequenceType);
            writer.WriteDouble(AuthorId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SequenceType = reader.ReadByte();
            AuthorId = reader.ReadDouble();
        }
    }
}