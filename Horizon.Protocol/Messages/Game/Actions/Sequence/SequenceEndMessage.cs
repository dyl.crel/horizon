namespace Horizon.Protocol.Messages.Game.Actions.Sequence
{
    public class SequenceEndMessage : NetworkMessage
    {
        public override short Protocol => 956;

        public short ActionId { get; set; }

        public double AuthorId { get; set; }

        public byte SequenceType { get; set; }

        public SequenceEndMessage()
        {
        }

        public SequenceEndMessage(short actionId, double authorId, byte sequenceType)
        {
            ActionId = actionId;
            AuthorId = authorId;
            SequenceType = sequenceType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ActionId);
            writer.WriteDouble(AuthorId);
            writer.WriteByte(SequenceType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActionId = reader.ReadVarShort();
            AuthorId = reader.ReadDouble();
            SequenceType = reader.ReadByte();
        }
    }
}