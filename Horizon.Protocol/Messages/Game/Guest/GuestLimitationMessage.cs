namespace Horizon.Protocol.Messages.Game.Guest
{
    public class GuestLimitationMessage : NetworkMessage
    {
        public override short Protocol => 6506;

        public byte Reason { get; set; }

        public GuestLimitationMessage()
        {
        }

        public GuestLimitationMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}