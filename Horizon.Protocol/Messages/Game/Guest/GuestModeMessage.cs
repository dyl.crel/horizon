namespace Horizon.Protocol.Messages.Game.Guest
{
    public class GuestModeMessage : NetworkMessage
    {
        public override short Protocol => 6505;

        public bool Active { get; set; }

        public GuestModeMessage()
        {
        }

        public GuestModeMessage(bool active)
        {
            Active = active;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Active);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Active = reader.ReadBoolean();
        }
    }
}