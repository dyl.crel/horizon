namespace Horizon.Protocol.Messages.Game.Subscriber
{
    public class SubscriptionZoneMessage : NetworkMessage
    {
        public override short Protocol => 5573;

        public bool Active { get; set; }

        public SubscriptionZoneMessage()
        {
        }

        public SubscriptionZoneMessage(bool active)
        {
            Active = active;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Active);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Active = reader.ReadBoolean();
        }
    }
}