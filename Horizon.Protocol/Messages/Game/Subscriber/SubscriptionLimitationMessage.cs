namespace Horizon.Protocol.Messages.Game.Subscriber
{
    public class SubscriptionLimitationMessage : NetworkMessage
    {
        public override short Protocol => 5542;

        public byte Reason { get; set; }

        public SubscriptionLimitationMessage()
        {
        }

        public SubscriptionLimitationMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}