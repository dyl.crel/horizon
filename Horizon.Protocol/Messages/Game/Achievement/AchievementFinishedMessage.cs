using Horizon.Protocol.Types.Game.Achievement;

namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementFinishedMessage : NetworkMessage
    {
        public override short Protocol => 6208;

        public AchievementAchievedRewardable Achievement { get; set; }

        public AchievementFinishedMessage()
        {
        }

        public AchievementFinishedMessage(AchievementAchievedRewardable achievement)
        {
            Achievement = achievement;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Achievement.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Achievement = new AchievementAchievedRewardable();
            Achievement.Deserialize(reader);
        }
    }
}