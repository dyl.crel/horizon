using Horizon.Protocol.Types.Game.Achievement;

namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementListMessage : NetworkMessage
    {
        public override short Protocol => 6205;

        public AchievementAchieved[] FinishedAchievements { get; set; }

        public AchievementListMessage()
        {
        }

        public AchievementListMessage(AchievementAchieved[] finishedAchievements)
        {
            FinishedAchievements = finishedAchievements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FinishedAchievements.Length);
            for (var i = 0; i < FinishedAchievements.Length; i++)
            {
                writer.WriteShort(FinishedAchievements[i].Protocol);
                FinishedAchievements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FinishedAchievements = new AchievementAchieved[reader.ReadShort()];
            for (var i = 0; i < FinishedAchievements.Length; i++)
            {
                FinishedAchievements[i] = ProtocolTypesManager.Instance<AchievementAchieved>(reader.ReadShort());
                FinishedAchievements[i].Deserialize(reader);
            }
        }
    }
}