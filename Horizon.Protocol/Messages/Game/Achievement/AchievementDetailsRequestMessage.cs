namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementDetailsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6380;

        public short AchievementId { get; set; }

        public AchievementDetailsRequestMessage()
        {
        }

        public AchievementDetailsRequestMessage(short achievementId)
        {
            AchievementId = achievementId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(AchievementId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AchievementId = reader.ReadVarShort();
        }
    }
}