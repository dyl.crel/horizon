namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementDetailedListRequestMessage : NetworkMessage
    {
        public override short Protocol => 6357;

        public short CategoryId { get; set; }

        public AchievementDetailedListRequestMessage()
        {
        }

        public AchievementDetailedListRequestMessage(short categoryId)
        {
            CategoryId = categoryId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CategoryId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CategoryId = reader.ReadVarShort();
        }
    }
}