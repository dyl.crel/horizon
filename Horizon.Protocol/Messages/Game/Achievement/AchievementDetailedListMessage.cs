namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementDetailedListMessage : NetworkMessage
    {
        public override short Protocol => 6358;

        public Types.Game.Achievement.Achievement[] StartedAchievements { get; set; }

        public Types.Game.Achievement.Achievement[] FinishedAchievements { get; set; }

        public AchievementDetailedListMessage()
        {
        }

        public AchievementDetailedListMessage(Types.Game.Achievement.Achievement[] startedAchievements, Types.Game.Achievement.Achievement[] finishedAchievements)
        {
            StartedAchievements = startedAchievements;
            FinishedAchievements = finishedAchievements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(StartedAchievements.Length);
            for (var i = 0; i < StartedAchievements.Length; i++)
            {
                StartedAchievements[i].Serialize(writer);
            }
            writer.WriteShort(FinishedAchievements.Length);
            for (var i = 0; i < FinishedAchievements.Length; i++)
            {
                FinishedAchievements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            StartedAchievements = new Types.Game.Achievement.Achievement[reader.ReadShort()];
            for (var i = 0; i < StartedAchievements.Length; i++)
            {
                StartedAchievements[i] = new Types.Game.Achievement.Achievement();
                StartedAchievements[i].Deserialize(reader);
            }
            FinishedAchievements = new Types.Game.Achievement.Achievement[reader.ReadShort()];
            for (var i = 0; i < FinishedAchievements.Length; i++)
            {
                FinishedAchievements[i] = new Types.Game.Achievement.Achievement();
                FinishedAchievements[i].Deserialize(reader);
            }
        }
    }
}