using Horizon.Protocol.Types.Game.Achievement;

namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementFinishedInformationMessage : AchievementFinishedMessage
    {
        public override short Protocol => 6381;

        public string Name { get; set; }

        public long PlayerId { get; set; }

        public AchievementFinishedInformationMessage()
        {
        }

        public AchievementFinishedInformationMessage(AchievementAchievedRewardable achievement, string name, long playerId) : base(achievement)
        {
            Name = name;
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            PlayerId = reader.ReadVarLong();
        }
    }
}