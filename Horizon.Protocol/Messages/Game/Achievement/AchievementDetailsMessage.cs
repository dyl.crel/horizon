namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementDetailsMessage : NetworkMessage
    {
        public override short Protocol => 6378;

        public Types.Game.Achievement.Achievement Achievement { get; set; }

        public AchievementDetailsMessage()
        {
        }

        public AchievementDetailsMessage(Types.Game.Achievement.Achievement achievement)
        {
            Achievement = achievement;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Achievement.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Achievement = new Types.Game.Achievement.Achievement();
            Achievement.Deserialize(reader);
        }
    }
}