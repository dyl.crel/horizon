namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class FriendGuildWarnOnAchievementCompleteStateMessage : NetworkMessage
    {
        public override short Protocol => 6383;

        public bool Enable { get; set; }

        public FriendGuildWarnOnAchievementCompleteStateMessage()
        {
        }

        public FriendGuildWarnOnAchievementCompleteStateMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}