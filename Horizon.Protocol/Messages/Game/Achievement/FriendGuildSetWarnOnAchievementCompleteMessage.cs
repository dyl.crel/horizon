namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class FriendGuildSetWarnOnAchievementCompleteMessage : NetworkMessage
    {
        public override short Protocol => 6382;

        public bool Enable { get; set; }

        public FriendGuildSetWarnOnAchievementCompleteMessage()
        {
        }

        public FriendGuildSetWarnOnAchievementCompleteMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}