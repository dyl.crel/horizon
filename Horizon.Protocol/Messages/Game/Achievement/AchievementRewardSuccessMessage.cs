namespace Horizon.Protocol.Messages.Game.Achievement
{
    public class AchievementRewardSuccessMessage : NetworkMessage
    {
        public override short Protocol => 6376;

        public short AchievementId { get; set; }

        public AchievementRewardSuccessMessage()
        {
        }

        public AchievementRewardSuccessMessage(short achievementId)
        {
            AchievementId = achievementId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(AchievementId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AchievementId = reader.ReadShort();
        }
    }
}