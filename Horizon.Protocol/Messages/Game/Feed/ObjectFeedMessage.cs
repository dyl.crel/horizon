using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Feed
{
    public class ObjectFeedMessage : NetworkMessage
    {
        public override short Protocol => 6290;

        public int ObjectUID { get; set; }

        public ObjectItemQuantity[] Meal { get; set; }

        public ObjectFeedMessage()
        {
        }

        public ObjectFeedMessage(int objectUID, ObjectItemQuantity[] meal)
        {
            ObjectUID = objectUID;
            Meal = meal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteShort(Meal.Length);
            for (var i = 0; i < Meal.Length; i++)
            {
                Meal[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Meal = new ObjectItemQuantity[reader.ReadShort()];
            for (var i = 0; i < Meal.Length; i++)
            {
                Meal[i] = new ObjectItemQuantity();
                Meal[i].Deserialize(reader);
            }
        }
    }
}