namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class OrnamentSelectedMessage : NetworkMessage
    {
        public override short Protocol => 6369;

        public short OrnamentId { get; set; }

        public OrnamentSelectedMessage()
        {
        }

        public OrnamentSelectedMessage(short ornamentId)
        {
            OrnamentId = ornamentId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(OrnamentId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            OrnamentId = reader.ReadVarShort();
        }
    }
}