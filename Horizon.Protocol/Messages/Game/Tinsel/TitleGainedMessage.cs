namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitleGainedMessage : NetworkMessage
    {
        public override short Protocol => 6364;

        public short TitleId { get; set; }

        public TitleGainedMessage()
        {
        }

        public TitleGainedMessage(short titleId)
        {
            TitleId = titleId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(TitleId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TitleId = reader.ReadVarShort();
        }
    }
}