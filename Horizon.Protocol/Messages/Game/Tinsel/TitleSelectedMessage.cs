namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitleSelectedMessage : NetworkMessage
    {
        public override short Protocol => 6366;

        public short TitleId { get; set; }

        public TitleSelectedMessage()
        {
        }

        public TitleSelectedMessage(short titleId)
        {
            TitleId = titleId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(TitleId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TitleId = reader.ReadVarShort();
        }
    }
}