namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitleLostMessage : NetworkMessage
    {
        public override short Protocol => 6371;

        public short TitleId { get; set; }

        public TitleLostMessage()
        {
        }

        public TitleLostMessage(short titleId)
        {
            TitleId = titleId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(TitleId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TitleId = reader.ReadVarShort();
        }
    }
}