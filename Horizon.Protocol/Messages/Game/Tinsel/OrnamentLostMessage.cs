namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class OrnamentLostMessage : NetworkMessage
    {
        public override short Protocol => 6770;

        public short OrnamentId { get; set; }

        public OrnamentLostMessage()
        {
        }

        public OrnamentLostMessage(short ornamentId)
        {
            OrnamentId = ornamentId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(OrnamentId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            OrnamentId = reader.ReadShort();
        }
    }
}