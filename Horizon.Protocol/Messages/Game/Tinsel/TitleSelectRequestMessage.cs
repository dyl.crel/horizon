namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitleSelectRequestMessage : NetworkMessage
    {
        public override short Protocol => 6365;

        public short TitleId { get; set; }

        public TitleSelectRequestMessage()
        {
        }

        public TitleSelectRequestMessage(short titleId)
        {
            TitleId = titleId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(TitleId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TitleId = reader.ReadVarShort();
        }
    }
}