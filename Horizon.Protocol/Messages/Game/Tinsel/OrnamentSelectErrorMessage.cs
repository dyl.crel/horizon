namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class OrnamentSelectErrorMessage : NetworkMessage
    {
        public override short Protocol => 6370;

        public byte Reason { get; set; }

        public OrnamentSelectErrorMessage()
        {
        }

        public OrnamentSelectErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}