namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class OrnamentSelectRequestMessage : NetworkMessage
    {
        public override short Protocol => 6374;

        public short OrnamentId { get; set; }

        public OrnamentSelectRequestMessage()
        {
        }

        public OrnamentSelectRequestMessage(short ornamentId)
        {
            OrnamentId = ornamentId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(OrnamentId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            OrnamentId = reader.ReadVarShort();
        }
    }
}