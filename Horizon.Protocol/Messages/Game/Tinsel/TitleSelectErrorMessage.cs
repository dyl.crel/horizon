namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitleSelectErrorMessage : NetworkMessage
    {
        public override short Protocol => 6373;

        public byte Reason { get; set; }

        public TitleSelectErrorMessage()
        {
        }

        public TitleSelectErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}