namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class OrnamentGainedMessage : NetworkMessage
    {
        public override short Protocol => 6368;

        public short OrnamentId { get; set; }

        public OrnamentGainedMessage()
        {
        }

        public OrnamentGainedMessage(short ornamentId)
        {
            OrnamentId = ornamentId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(OrnamentId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            OrnamentId = reader.ReadShort();
        }
    }
}