namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitlesAndOrnamentsListRequestMessage : NetworkMessage
    {
        public override short Protocol => 6363;

        public TitlesAndOrnamentsListRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}