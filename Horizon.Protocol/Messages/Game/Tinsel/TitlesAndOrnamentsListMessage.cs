namespace Horizon.Protocol.Messages.Game.Tinsel
{
    public class TitlesAndOrnamentsListMessage : NetworkMessage
    {
        public override short Protocol => 6367;

        public short[] Titles { get; set; }

        public short[] Ornaments { get; set; }

        public short ActiveTitle { get; set; }

        public short ActiveOrnament { get; set; }

        public TitlesAndOrnamentsListMessage()
        {
        }

        public TitlesAndOrnamentsListMessage(short[] titles, short[] ornaments, short activeTitle, short activeOrnament)
        {
            Titles = titles;
            Ornaments = ornaments;
            ActiveTitle = activeTitle;
            ActiveOrnament = activeOrnament;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Titles.Length);
            for (var i = 0; i < Titles.Length; i++)
            {
                writer.WriteVarShort(Titles[i]);
            }
            writer.WriteShort(Ornaments.Length);
            for (var i = 0; i < Ornaments.Length; i++)
            {
                writer.WriteVarShort(Ornaments[i]);
            }
            writer.WriteVarShort(ActiveTitle);
            writer.WriteVarShort(ActiveOrnament);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Titles = new short[reader.ReadShort()];
            for (var i = 0; i < Titles.Length; i++)
            {
                Titles[i] = reader.ReadVarShort();
            }
            Ornaments = new short[reader.ReadShort()];
            for (var i = 0; i < Ornaments.Length; i++)
            {
                Ornaments[i] = reader.ReadVarShort();
            }
            ActiveTitle = reader.ReadVarShort();
            ActiveOrnament = reader.ReadVarShort();
        }
    }
}