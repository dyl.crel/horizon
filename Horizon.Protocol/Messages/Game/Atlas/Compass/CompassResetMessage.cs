namespace Horizon.Protocol.Messages.Game.Atlas.Compass
{
    public class CompassResetMessage : NetworkMessage
    {
        public override short Protocol => 5584;

        public byte Type { get; set; }

        public CompassResetMessage()
        {
        }

        public CompassResetMessage(byte type)
        {
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
        }
    }
}