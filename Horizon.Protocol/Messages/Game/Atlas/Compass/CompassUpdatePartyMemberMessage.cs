using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Atlas.Compass
{
    public class CompassUpdatePartyMemberMessage : CompassUpdateMessage
    {
        public override short Protocol => 5589;

        public long MemberId { get; set; }

        public bool Active { get; set; }

        public CompassUpdatePartyMemberMessage()
        {
        }

        public CompassUpdatePartyMemberMessage(byte type, MapCoordinates coords, long memberId, bool active) : base(type, coords)
        {
            MemberId = memberId;
            Active = active;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(MemberId);
            writer.WriteBoolean(Active);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MemberId = reader.ReadVarLong();
            Active = reader.ReadBoolean();
        }
    }
}