using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Atlas.Compass
{
    public class CompassUpdatePvpSeekMessage : CompassUpdateMessage
    {
        public override short Protocol => 6013;

        public long MemberId { get; set; }

        public string MemberName { get; set; }

        public CompassUpdatePvpSeekMessage()
        {
        }

        public CompassUpdatePvpSeekMessage(byte type, MapCoordinates coords, long memberId, string memberName) : base(type, coords)
        {
            MemberId = memberId;
            MemberName = memberName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(MemberId);
            writer.WriteUTF(MemberName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MemberId = reader.ReadVarLong();
            MemberName = reader.ReadUTF();
        }
    }
}