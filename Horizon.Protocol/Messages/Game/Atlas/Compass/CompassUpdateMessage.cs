using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Atlas.Compass
{
    public class CompassUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5591;

        public byte Type { get; set; }

        public MapCoordinates Coords { get; set; }

        public CompassUpdateMessage()
        {
        }

        public CompassUpdateMessage(byte type, MapCoordinates coords)
        {
            Type = type;
            Coords = coords;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
            writer.WriteShort(Coords.Protocol);
            Coords.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
            Coords = ProtocolTypesManager.Instance<MapCoordinates>(reader.ReadShort());
            Coords.Deserialize(reader);
        }
    }
}