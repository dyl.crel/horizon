using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Atlas
{
    public class AtlasPointInformationsMessage : NetworkMessage
    {
        public override short Protocol => 5956;

        public AtlasPointsInformations Type { get; set; }

        public AtlasPointInformationsMessage()
        {
        }

        public AtlasPointInformationsMessage(AtlasPointsInformations type)
        {
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Type.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = new AtlasPointsInformations();
            Type.Deserialize(reader);
        }
    }
}