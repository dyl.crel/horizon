namespace Horizon.Protocol.Messages.Game.Character.Deletion
{
    public class CharacterDeletionErrorMessage : NetworkMessage
    {
        public override short Protocol => 166;

        public byte Reason { get; set; }

        public CharacterDeletionErrorMessage()
        {
        }

        public CharacterDeletionErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}