namespace Horizon.Protocol.Messages.Game.Character.Deletion
{
    public class CharacterDeletionRequestMessage : NetworkMessage
    {
        public override short Protocol => 165;

        public long CharacterId { get; set; }

        public string SecretAnswerHash { get; set; }

        public CharacterDeletionRequestMessage()
        {
        }

        public CharacterDeletionRequestMessage(long characterId, string secretAnswerHash)
        {
            CharacterId = characterId;
            SecretAnswerHash = secretAnswerHash;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(CharacterId);
            writer.WriteUTF(SecretAnswerHash);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CharacterId = reader.ReadVarLong();
            SecretAnswerHash = reader.ReadUTF();
        }
    }
}