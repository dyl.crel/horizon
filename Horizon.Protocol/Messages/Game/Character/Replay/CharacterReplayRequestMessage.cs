namespace Horizon.Protocol.Messages.Game.Character.Replay
{
    public class CharacterReplayRequestMessage : NetworkMessage
    {
        public override short Protocol => 167;

        public long CharacterId { get; set; }

        public CharacterReplayRequestMessage()
        {
        }

        public CharacterReplayRequestMessage(long characterId)
        {
            CharacterId = characterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(CharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CharacterId = reader.ReadVarLong();
        }
    }
}