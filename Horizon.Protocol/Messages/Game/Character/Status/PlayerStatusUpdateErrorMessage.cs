namespace Horizon.Protocol.Messages.Game.Character.Status
{
    public class PlayerStatusUpdateErrorMessage : NetworkMessage
    {
        public override short Protocol => 6385;

        public PlayerStatusUpdateErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}