using Horizon.Protocol.Types.Game.Character.Status;

namespace Horizon.Protocol.Messages.Game.Character.Status
{
    public class PlayerStatusUpdateRequestMessage : NetworkMessage
    {
        public override short Protocol => 6387;

        public PlayerStatus Status { get; set; }

        public PlayerStatusUpdateRequestMessage()
        {
        }

        public PlayerStatusUpdateRequestMessage(PlayerStatus status)
        {
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}