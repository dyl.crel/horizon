using Horizon.Protocol.Types.Game.Character.Status;

namespace Horizon.Protocol.Messages.Game.Character.Status
{
    public class PlayerStatusUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6386;

        public int AccountId { get; set; }

        public long PlayerId { get; set; }

        public PlayerStatus Status { get; set; }

        public PlayerStatusUpdateMessage()
        {
        }

        public PlayerStatusUpdateMessage(int accountId, long playerId, PlayerStatus status)
        {
            AccountId = accountId;
            PlayerId = playerId;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteVarLong(PlayerId);
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AccountId = reader.ReadInt();
            PlayerId = reader.ReadVarLong();
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}