namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterNameSuggestionSuccessMessage : NetworkMessage
    {
        public override short Protocol => 5544;

        public string Suggestion { get; set; }

        public CharacterNameSuggestionSuccessMessage()
        {
        }

        public CharacterNameSuggestionSuccessMessage(string suggestion)
        {
            Suggestion = suggestion;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Suggestion);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Suggestion = reader.ReadUTF();
        }
    }
}