namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterNameSuggestionRequestMessage : NetworkMessage
    {
        public override short Protocol => 162;

        public CharacterNameSuggestionRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}