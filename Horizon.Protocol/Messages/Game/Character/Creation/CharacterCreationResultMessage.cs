namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterCreationResultMessage : NetworkMessage
    {
        public override short Protocol => 161;

        public byte Result { get; set; }

        public CharacterCreationResultMessage()
        {
        }

        public CharacterCreationResultMessage(byte result)
        {
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Result = reader.ReadByte();
        }
    }
}