namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterNameSuggestionFailureMessage : NetworkMessage
    {
        public override short Protocol => 164;

        public byte Reason { get; set; }

        public CharacterNameSuggestionFailureMessage()
        {
        }

        public CharacterNameSuggestionFailureMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}