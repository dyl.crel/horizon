namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterCanBeCreatedRequestMessage : NetworkMessage
    {
        public override short Protocol => 6732;

        public CharacterCanBeCreatedRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}