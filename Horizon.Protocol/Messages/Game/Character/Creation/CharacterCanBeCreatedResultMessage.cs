namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterCanBeCreatedResultMessage : NetworkMessage
    {
        public override short Protocol => 6733;

        public bool YesYouCan { get; set; }

        public CharacterCanBeCreatedResultMessage()
        {
        }

        public CharacterCanBeCreatedResultMessage(bool yesYouCan)
        {
            YesYouCan = yesYouCan;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(YesYouCan);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            YesYouCan = reader.ReadBoolean();
        }
    }
}