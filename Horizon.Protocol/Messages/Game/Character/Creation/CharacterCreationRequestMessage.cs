namespace Horizon.Protocol.Messages.Game.Character.Creation
{
    public class CharacterCreationRequestMessage : NetworkMessage
    {
        public override short Protocol => 160;

        public string Name { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public int[] Colors { get; set; }

        public short CosmeticId { get; set; }

        public CharacterCreationRequestMessage()
        {
        }

        public CharacterCreationRequestMessage(string name, byte breed, bool sex, int[] colors, short cosmeticId)
        {
            Name = name;
            Breed = breed;
            Sex = sex;
            Colors = colors;
            CosmeticId = cosmeticId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            for (var i = 0; i < 5; i++)
            {
                writer.WriteInt(Colors[i]);
            }
            writer.WriteVarShort(CosmeticId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            Colors = new int[5];
            for (var i = 0; i < 5; i++)
            {
                Colors[i] = reader.ReadInt();
            }
            CosmeticId = reader.ReadVarShort();
        }
    }
}