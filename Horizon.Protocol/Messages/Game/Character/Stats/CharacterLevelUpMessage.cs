namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class CharacterLevelUpMessage : NetworkMessage
    {
        public override short Protocol => 5670;

        public short NewLevel { get; set; }

        public CharacterLevelUpMessage()
        {
        }

        public CharacterLevelUpMessage(short newLevel)
        {
            NewLevel = newLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(NewLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NewLevel = reader.ReadVarShort();
        }
    }
}