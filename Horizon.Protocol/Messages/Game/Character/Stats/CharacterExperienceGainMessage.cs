namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class CharacterExperienceGainMessage : NetworkMessage
    {
        public override short Protocol => 6321;

        public long ExperienceCharacter { get; set; }

        public long ExperienceMount { get; set; }

        public long ExperienceGuild { get; set; }

        public long ExperienceIncarnation { get; set; }

        public CharacterExperienceGainMessage()
        {
        }

        public CharacterExperienceGainMessage(long experienceCharacter, long experienceMount, long experienceGuild, long experienceIncarnation)
        {
            ExperienceCharacter = experienceCharacter;
            ExperienceMount = experienceMount;
            ExperienceGuild = experienceGuild;
            ExperienceIncarnation = experienceIncarnation;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(ExperienceCharacter);
            writer.WriteVarLong(ExperienceMount);
            writer.WriteVarLong(ExperienceGuild);
            writer.WriteVarLong(ExperienceIncarnation);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExperienceCharacter = reader.ReadVarLong();
            ExperienceMount = reader.ReadVarLong();
            ExperienceGuild = reader.ReadVarLong();
            ExperienceIncarnation = reader.ReadVarLong();
        }
    }
}