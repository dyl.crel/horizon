namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class ResetCharacterStatsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6739;

        public ResetCharacterStatsRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}