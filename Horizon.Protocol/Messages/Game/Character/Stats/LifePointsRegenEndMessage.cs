namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class LifePointsRegenEndMessage : UpdateLifePointsMessage
    {
        public override short Protocol => 5686;

        public int LifePointsGained { get; set; }

        public LifePointsRegenEndMessage()
        {
        }

        public LifePointsRegenEndMessage(int lifePoints, int maxLifePoints, int lifePointsGained) : base(lifePoints, maxLifePoints)
        {
            LifePointsGained = lifePointsGained;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(LifePointsGained);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LifePointsGained = reader.ReadVarInt();
        }
    }
}