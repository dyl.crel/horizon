using Horizon.Protocol.Types.Game.Character.Characteristic;

namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class FighterStatsListMessage : NetworkMessage
    {
        public override short Protocol => 6322;

        public CharacterCharacteristicsInformations Stats { get; set; }

        public FighterStatsListMessage()
        {
        }

        public FighterStatsListMessage(CharacterCharacteristicsInformations stats)
        {
            Stats = stats;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Stats.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Stats = new CharacterCharacteristicsInformations();
            Stats.Deserialize(reader);
        }
    }
}