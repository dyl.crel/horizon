using Horizon.Protocol.Types.Game.Character.Characteristic;

namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class CharacterStatsListMessage : NetworkMessage
    {
        public override short Protocol => 500;

        public CharacterCharacteristicsInformations Stats { get; set; }

        public CharacterStatsListMessage()
        {
        }

        public CharacterStatsListMessage(CharacterCharacteristicsInformations stats)
        {
            Stats = stats;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Stats.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Stats = new CharacterCharacteristicsInformations();
            Stats.Deserialize(reader);
        }
    }
}