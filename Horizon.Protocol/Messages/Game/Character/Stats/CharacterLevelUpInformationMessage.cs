namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class CharacterLevelUpInformationMessage : CharacterLevelUpMessage
    {
        public override short Protocol => 6076;

        public string Name { get; set; }

        public long Id { get; set; }

        public CharacterLevelUpInformationMessage()
        {
        }

        public CharacterLevelUpInformationMessage(short newLevel, string name, long id) : base(newLevel)
        {
            Name = name;
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            writer.WriteVarLong(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Id = reader.ReadVarLong();
        }
    }
}