namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class UpdateLifePointsMessage : NetworkMessage
    {
        public override short Protocol => 5658;

        public int LifePoints { get; set; }

        public int MaxLifePoints { get; set; }

        public UpdateLifePointsMessage()
        {
        }

        public UpdateLifePointsMessage(int lifePoints, int maxLifePoints)
        {
            LifePoints = lifePoints;
            MaxLifePoints = maxLifePoints;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            LifePoints = reader.ReadVarInt();
            MaxLifePoints = reader.ReadVarInt();
        }
    }
}