namespace Horizon.Protocol.Messages.Game.Character.Stats
{
    public class LifePointsRegenBeginMessage : NetworkMessage
    {
        public override short Protocol => 5684;

        public byte RegenRate { get; set; }

        public LifePointsRegenBeginMessage()
        {
        }

        public LifePointsRegenBeginMessage(byte regenRate)
        {
            RegenRate = regenRate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(RegenRate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RegenRate = reader.ReadByte();
        }
    }
}