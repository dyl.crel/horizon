using Horizon.Protocol.Types.Game.Character.Choice;

namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharactersListMessage : BasicCharactersListMessage
    {
        public override short Protocol => 151;

        public bool HasStartupActions { get; set; }

        public CharactersListMessage()
        {
        }

        public CharactersListMessage(CharacterBaseInformations[] characters, bool hasStartupActions) : base(characters)
        {
            HasStartupActions = hasStartupActions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(HasStartupActions);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            HasStartupActions = reader.ReadBoolean();
        }
    }
}