using Horizon.Protocol.Types.Game.Character.Choice;

namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharactersListWithRemodelingMessage : CharactersListMessage
    {
        public override short Protocol => 6550;

        public CharacterToRemodelInformations[] CharactersToRemodel { get; set; }

        public CharactersListWithRemodelingMessage()
        {
        }

        public CharactersListWithRemodelingMessage(CharacterBaseInformations[] characters, bool hasStartupActions, CharacterToRemodelInformations[] charactersToRemodel) : base(characters, hasStartupActions)
        {
            CharactersToRemodel = charactersToRemodel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(CharactersToRemodel.Length);
            for (var i = 0; i < CharactersToRemodel.Length; i++)
            {
                CharactersToRemodel[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CharactersToRemodel = new CharacterToRemodelInformations[reader.ReadShort()];
            for (var i = 0; i < CharactersToRemodel.Length; i++)
            {
                CharactersToRemodel[i] = new CharacterToRemodelInformations();
                CharactersToRemodel[i].Deserialize(reader);
            }
        }
    }
}