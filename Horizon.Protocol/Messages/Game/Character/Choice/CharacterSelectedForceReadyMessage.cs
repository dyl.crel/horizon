namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterSelectedForceReadyMessage : NetworkMessage
    {
        public override short Protocol => 6072;

        public CharacterSelectedForceReadyMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}