namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharactersListErrorMessage : NetworkMessage
    {
        public override short Protocol => 5545;

        public CharactersListErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}