using Horizon.Protocol.Messages.Game.Character.Replay;
using Horizon.Protocol.Types.Game.Character.Choice;

namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterReplayWithRemodelRequestMessage : CharacterReplayRequestMessage
    {
        public override short Protocol => 6551;

        public RemodelingInformation Remodel { get; set; }

        public CharacterReplayWithRemodelRequestMessage()
        {
        }

        public CharacterReplayWithRemodelRequestMessage(long characterId, RemodelingInformation remodel) : base(characterId)
        {
            Remodel = remodel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Remodel.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Remodel = new RemodelingInformation();
            Remodel.Deserialize(reader);
        }
    }
}