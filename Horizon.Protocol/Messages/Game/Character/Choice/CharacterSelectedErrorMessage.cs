namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterSelectedErrorMessage : NetworkMessage
    {
        public override short Protocol => 5836;

        public CharacterSelectedErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}