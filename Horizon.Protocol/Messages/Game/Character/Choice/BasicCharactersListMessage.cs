using Horizon.Protocol.Types.Game.Character.Choice;

namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class BasicCharactersListMessage : NetworkMessage
    {
        public override short Protocol => 6475;

        public CharacterBaseInformations[] Characters { get; set; }

        public BasicCharactersListMessage()
        {
        }

        public BasicCharactersListMessage(CharacterBaseInformations[] characters)
        {
            Characters = characters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Characters.Length);
            for (var i = 0; i < Characters.Length; i++)
            {
                writer.WriteShort(Characters[i].Protocol);
                Characters[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Characters = new CharacterBaseInformations[reader.ReadShort()];
            for (var i = 0; i < Characters.Length; i++)
            {
                Characters[i] = ProtocolTypesManager.Instance<CharacterBaseInformations>(reader.ReadShort());
                Characters[i].Deserialize(reader);
            }
        }
    }
}