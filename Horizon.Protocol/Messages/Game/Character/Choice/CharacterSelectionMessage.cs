namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterSelectionMessage : NetworkMessage
    {
        public override short Protocol => 152;

        public long Id { get; set; }

        public CharacterSelectionMessage()
        {
        }

        public CharacterSelectionMessage(long id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarLong();
        }
    }
}