namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterFirstSelectionMessage : CharacterSelectionMessage
    {
        public override short Protocol => 6084;

        public bool DoTutorial { get; set; }

        public CharacterFirstSelectionMessage()
        {
        }

        public CharacterFirstSelectionMessage(long id, bool doTutorial) : base(id)
        {
            DoTutorial = doTutorial;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(DoTutorial);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DoTutorial = reader.ReadBoolean();
        }
    }
}