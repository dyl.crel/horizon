using Horizon.Protocol.Types.Game.Character.Choice;

namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterSelectionWithRemodelMessage : CharacterSelectionMessage
    {
        public override short Protocol => 6549;

        public RemodelingInformation Remodel { get; set; }

        public CharacterSelectionWithRemodelMessage()
        {
        }

        public CharacterSelectionWithRemodelMessage(long id, RemodelingInformation remodel) : base(id)
        {
            Remodel = remodel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Remodel.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Remodel = new RemodelingInformation();
            Remodel.Deserialize(reader);
        }
    }
}