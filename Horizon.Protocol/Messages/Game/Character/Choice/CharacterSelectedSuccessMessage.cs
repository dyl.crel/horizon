using Horizon.Protocol.Types.Game.Character.Choice;

namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterSelectedSuccessMessage : NetworkMessage
    {
        public override short Protocol => 153;

        public CharacterBaseInformations Infos { get; set; }

        public bool IsCollectingStats { get; set; }

        public CharacterSelectedSuccessMessage()
        {
        }

        public CharacterSelectedSuccessMessage(CharacterBaseInformations infos, bool isCollectingStats)
        {
            Infos = infos;
            IsCollectingStats = isCollectingStats;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Infos.Serialize(writer);
            writer.WriteBoolean(IsCollectingStats);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Infos = new CharacterBaseInformations();
            Infos.Deserialize(reader);
            IsCollectingStats = reader.ReadBoolean();
        }
    }
}