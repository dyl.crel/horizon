namespace Horizon.Protocol.Messages.Game.Character.Choice
{
    public class CharacterSelectedForceMessage : NetworkMessage
    {
        public override short Protocol => 6068;

        public int Id { get; set; }

        public CharacterSelectedForceMessage()
        {
        }

        public CharacterSelectedForceMessage(int id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadInt();
        }
    }
}