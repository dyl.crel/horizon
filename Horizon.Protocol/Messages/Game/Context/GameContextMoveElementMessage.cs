using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextMoveElementMessage : NetworkMessage
    {
        public override short Protocol => 253;

        public EntityMovementInformations Movement { get; set; }

        public GameContextMoveElementMessage()
        {
        }

        public GameContextMoveElementMessage(EntityMovementInformations movement)
        {
            Movement = movement;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Movement.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Movement = new EntityMovementInformations();
            Movement.Deserialize(reader);
        }
    }
}