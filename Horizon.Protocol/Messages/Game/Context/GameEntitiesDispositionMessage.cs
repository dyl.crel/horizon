using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameEntitiesDispositionMessage : NetworkMessage
    {
        public override short Protocol => 5696;

        public IdentifiedEntityDispositionInformations[] Dispositions { get; set; }

        public GameEntitiesDispositionMessage()
        {
        }

        public GameEntitiesDispositionMessage(IdentifiedEntityDispositionInformations[] dispositions)
        {
            Dispositions = dispositions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Dispositions.Length);
            for (var i = 0; i < Dispositions.Length; i++)
            {
                Dispositions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Dispositions = new IdentifiedEntityDispositionInformations[reader.ReadShort()];
            for (var i = 0; i < Dispositions.Length; i++)
            {
                Dispositions[i] = new IdentifiedEntityDispositionInformations();
                Dispositions[i].Deserialize(reader);
            }
        }
    }
}