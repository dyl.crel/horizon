using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapChangeOrientationsMessage : NetworkMessage
    {
        public override short Protocol => 6155;

        public ActorOrientation[] Orientations { get; set; }

        public GameMapChangeOrientationsMessage()
        {
        }

        public GameMapChangeOrientationsMessage(ActorOrientation[] orientations)
        {
            Orientations = orientations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Orientations.Length);
            for (var i = 0; i < Orientations.Length; i++)
            {
                Orientations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Orientations = new ActorOrientation[reader.ReadShort()];
            for (var i = 0; i < Orientations.Length; i++)
            {
                Orientations[i] = new ActorOrientation();
                Orientations[i].Deserialize(reader);
            }
        }
    }
}