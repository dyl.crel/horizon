namespace Horizon.Protocol.Messages.Game.Context.Dungeon
{
    public class DungeonKeyRingUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6296;

        public short DungeonId { get; set; }

        public bool Available { get; set; }

        public DungeonKeyRingUpdateMessage()
        {
        }

        public DungeonKeyRingUpdateMessage(short dungeonId, bool available)
        {
            DungeonId = dungeonId;
            Available = available;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteBoolean(Available);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            Available = reader.ReadBoolean();
        }
    }
}