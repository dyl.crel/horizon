namespace Horizon.Protocol.Messages.Game.Context.Dungeon
{
    public class DungeonKeyRingMessage : NetworkMessage
    {
        public override short Protocol => 6299;

        public short[] Availables { get; set; }

        public short[] Unavailables { get; set; }

        public DungeonKeyRingMessage()
        {
        }

        public DungeonKeyRingMessage(short[] availables, short[] unavailables)
        {
            Availables = availables;
            Unavailables = unavailables;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Availables.Length);
            for (var i = 0; i < Availables.Length; i++)
            {
                writer.WriteVarShort(Availables[i]);
            }
            writer.WriteShort(Unavailables.Length);
            for (var i = 0; i < Unavailables.Length; i++)
            {
                writer.WriteVarShort(Unavailables[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Availables = new short[reader.ReadShort()];
            for (var i = 0; i < Availables.Length; i++)
            {
                Availables[i] = reader.ReadVarShort();
            }
            Unavailables = new short[reader.ReadShort()];
            for (var i = 0; i < Unavailables.Length; i++)
            {
                Unavailables[i] = reader.ReadVarShort();
            }
        }
    }
}