namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapNoMovementMessage : NetworkMessage
    {
        public override short Protocol => 954;

        public short CellX { get; set; }

        public short CellY { get; set; }

        public GameMapNoMovementMessage()
        {
        }

        public GameMapNoMovementMessage(short cellX, short cellY)
        {
            CellX = cellX;
            CellY = cellY;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CellX);
            writer.WriteShort(CellY);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellX = reader.ReadShort();
            CellY = reader.ReadShort();
        }
    }
}