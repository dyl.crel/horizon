namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnReadyRequestMessage : NetworkMessage
    {
        public override short Protocol => 715;

        public double Id { get; set; }

        public GameFightTurnReadyRequestMessage()
        {
        }

        public GameFightTurnReadyRequestMessage(double id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
        }
    }
}