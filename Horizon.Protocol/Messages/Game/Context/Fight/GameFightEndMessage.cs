using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightEndMessage : NetworkMessage
    {
        public override short Protocol => 720;

        public int Duration { get; set; }

        public short AgeBonus { get; set; }

        public short LootShareLimitMalus { get; set; }

        public FightResultListEntry[] Results { get; set; }

        public NamedPartyTeamWithOutcome[] NamedPartyTeamsOutcomes { get; set; }

        public GameFightEndMessage()
        {
        }

        public GameFightEndMessage(int duration, short ageBonus, short lootShareLimitMalus, FightResultListEntry[] results, NamedPartyTeamWithOutcome[] namedPartyTeamsOutcomes)
        {
            Duration = duration;
            AgeBonus = ageBonus;
            LootShareLimitMalus = lootShareLimitMalus;
            Results = results;
            NamedPartyTeamsOutcomes = namedPartyTeamsOutcomes;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Duration);
            writer.WriteShort(AgeBonus);
            writer.WriteShort(LootShareLimitMalus);
            writer.WriteShort(Results.Length);
            for (var i = 0; i < Results.Length; i++)
            {
                writer.WriteShort(Results[i].Protocol);
                Results[i].Serialize(writer);
            }
            writer.WriteShort(NamedPartyTeamsOutcomes.Length);
            for (var i = 0; i < NamedPartyTeamsOutcomes.Length; i++)
            {
                NamedPartyTeamsOutcomes[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Duration = reader.ReadInt();
            AgeBonus = reader.ReadShort();
            LootShareLimitMalus = reader.ReadShort();
            Results = new FightResultListEntry[reader.ReadShort()];
            for (var i = 0; i < Results.Length; i++)
            {
                Results[i] = ProtocolTypesManager.Instance<FightResultListEntry>(reader.ReadShort());
                Results[i].Deserialize(reader);
            }
            NamedPartyTeamsOutcomes = new NamedPartyTeamWithOutcome[reader.ReadShort()];
            for (var i = 0; i < NamedPartyTeamsOutcomes.Length; i++)
            {
                NamedPartyTeamsOutcomes[i] = new NamedPartyTeamWithOutcome();
                NamedPartyTeamsOutcomes[i].Deserialize(reader);
            }
        }
    }
}