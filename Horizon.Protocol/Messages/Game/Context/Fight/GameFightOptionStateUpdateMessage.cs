namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightOptionStateUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5927;

        public short FightId { get; set; }

        public byte TeamId { get; set; }

        public byte Option { get; set; }

        public bool State { get; set; }

        public GameFightOptionStateUpdateMessage()
        {
        }

        public GameFightOptionStateUpdateMessage(short fightId, byte teamId, byte option, bool state)
        {
            FightId = fightId;
            TeamId = teamId;
            Option = option;
            State = state;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteByte(TeamId);
            writer.WriteByte(Option);
            writer.WriteBoolean(State);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            TeamId = reader.ReadByte();
            Option = reader.ReadByte();
            State = reader.ReadBoolean();
        }
    }
}