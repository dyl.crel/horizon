namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class SlaveNoLongerControledMessage : NetworkMessage
    {
        public override short Protocol => 6807;

        public double MasterId { get; set; }

        public double SlaveId { get; set; }

        public SlaveNoLongerControledMessage()
        {
        }

        public SlaveNoLongerControledMessage(double masterId, double slaveId)
        {
            MasterId = masterId;
            SlaveId = slaveId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MasterId);
            writer.WriteDouble(SlaveId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MasterId = reader.ReadDouble();
            SlaveId = reader.ReadDouble();
        }
    }
}