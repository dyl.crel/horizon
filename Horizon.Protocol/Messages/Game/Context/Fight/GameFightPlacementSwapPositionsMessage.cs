using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementSwapPositionsMessage : NetworkMessage
    {
        public override short Protocol => 6544;

        public IdentifiedEntityDispositionInformations[] Dispositions { get; set; }

        public GameFightPlacementSwapPositionsMessage()
        {
        }

        public GameFightPlacementSwapPositionsMessage(IdentifiedEntityDispositionInformations[] dispositions)
        {
            Dispositions = dispositions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            for (var i = 0; i < 2; i++)
            {
                Dispositions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Dispositions = new IdentifiedEntityDispositionInformations[2];
            for (var i = 0; i < Dispositions.Length; i++)
            {
                Dispositions[i] = new IdentifiedEntityDispositionInformations();
                Dispositions[i].Deserialize(reader);
            }
        }
    }
}