namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementSwapPositionsOfferMessage : NetworkMessage
    {
        public override short Protocol => 6542;

        public int RequestId { get; set; }

        public double RequesterId { get; set; }

        public short RequesterCellId { get; set; }

        public double RequestedId { get; set; }

        public short RequestedCellId { get; set; }

        public GameFightPlacementSwapPositionsOfferMessage()
        {
        }

        public GameFightPlacementSwapPositionsOfferMessage(int requestId, double requesterId, short requesterCellId, double requestedId, short requestedCellId)
        {
            RequestId = requestId;
            RequesterId = requesterId;
            RequesterCellId = requesterCellId;
            RequestedId = requestedId;
            RequestedCellId = requestedCellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(RequestId);
            writer.WriteDouble(RequesterId);
            writer.WriteVarShort(RequesterCellId);
            writer.WriteDouble(RequestedId);
            writer.WriteVarShort(RequestedCellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadInt();
            RequesterId = reader.ReadDouble();
            RequesterCellId = reader.ReadVarShort();
            RequestedId = reader.ReadDouble();
            RequestedCellId = reader.ReadVarShort();
        }
    }
}