using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight.Character
{
    public class GameFightShowFighterMessage : NetworkMessage
    {
        public override short Protocol => 5864;

        public GameFightFighterInformations Informations { get; set; }

        public GameFightShowFighterMessage()
        {
        }

        public GameFightShowFighterMessage(GameFightFighterInformations informations)
        {
            Informations = informations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Informations.Protocol);
            Informations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Informations = ProtocolTypesManager.Instance<GameFightFighterInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
    }
}