using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight.Character
{
    public class GameFightShowFighterRandomStaticPoseMessage : GameFightShowFighterMessage
    {
        public override short Protocol => 6218;

        public GameFightShowFighterRandomStaticPoseMessage()
        {
        }

        public GameFightShowFighterRandomStaticPoseMessage(GameFightFighterInformations informations) : base(informations)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}