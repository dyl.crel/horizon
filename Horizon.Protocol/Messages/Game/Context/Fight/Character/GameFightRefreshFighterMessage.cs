using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context.Fight.Character
{
    public class GameFightRefreshFighterMessage : NetworkMessage
    {
        public override short Protocol => 6309;

        public GameContextActorInformations Informations { get; set; }

        public GameFightRefreshFighterMessage()
        {
        }

        public GameFightRefreshFighterMessage(GameContextActorInformations informations)
        {
            Informations = informations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Informations.Protocol);
            Informations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Informations = ProtocolTypesManager.Instance<GameContextActorInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
    }
}