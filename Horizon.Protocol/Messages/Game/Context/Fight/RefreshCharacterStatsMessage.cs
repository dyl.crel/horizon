using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class RefreshCharacterStatsMessage : NetworkMessage
    {
        public override short Protocol => 6699;

        public double FighterId { get; set; }

        public GameFightMinimalStats Stats { get; set; }

        public RefreshCharacterStatsMessage()
        {
        }

        public RefreshCharacterStatsMessage(double fighterId, GameFightMinimalStats stats)
        {
            FighterId = fighterId;
            Stats = stats;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(FighterId);
            Stats.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FighterId = reader.ReadDouble();
            Stats = new GameFightMinimalStats();
            Stats.Deserialize(reader);
        }
    }
}