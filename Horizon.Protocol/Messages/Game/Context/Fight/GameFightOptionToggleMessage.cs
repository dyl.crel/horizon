namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightOptionToggleMessage : NetworkMessage
    {
        public override short Protocol => 707;

        public byte Option { get; set; }

        public GameFightOptionToggleMessage()
        {
        }

        public GameFightOptionToggleMessage(byte option)
        {
            Option = option;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Option);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Option = reader.ReadByte();
        }
    }
}