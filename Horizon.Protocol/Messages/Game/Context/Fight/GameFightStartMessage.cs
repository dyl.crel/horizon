namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightStartMessage : NetworkMessage
    {
        public override short Protocol => 712;

        public Types.Game.Idol.Idol[] Idols { get; set; }

        public GameFightStartMessage()
        {
        }

        public GameFightStartMessage(Types.Game.Idol.Idol[] idols)
        {
            Idols = idols;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Idols.Length);
            for (var i = 0; i < Idols.Length; i++)
            {
                Idols[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Idols = new Types.Game.Idol.Idol[reader.ReadShort()];
            for (var i = 0; i < Idols.Length; i++)
            {
                Idols[i] = new Types.Game.Idol.Idol();
                Idols[i].Deserialize(reader);
            }
        }
    }
}