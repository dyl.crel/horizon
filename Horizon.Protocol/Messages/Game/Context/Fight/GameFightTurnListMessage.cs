namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnListMessage : NetworkMessage
    {
        public override short Protocol => 713;

        public double[] Ids { get; set; }

        public double[] DeadsIds { get; set; }

        public GameFightTurnListMessage()
        {
        }

        public GameFightTurnListMessage(double[] ids, double[] deadsIds)
        {
            Ids = ids;
            DeadsIds = deadsIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Ids.Length);
            for (var i = 0; i < Ids.Length; i++)
            {
                writer.WriteDouble(Ids[i]);
            }
            writer.WriteShort(DeadsIds.Length);
            for (var i = 0; i < DeadsIds.Length; i++)
            {
                writer.WriteDouble(DeadsIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Ids = new double[reader.ReadShort()];
            for (var i = 0; i < Ids.Length; i++)
            {
                Ids[i] = reader.ReadDouble();
            }
            DeadsIds = new double[reader.ReadShort()];
            for (var i = 0; i < DeadsIds.Length; i++)
            {
                DeadsIds[i] = reader.ReadDouble();
            }
        }
    }
}