namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementSwapPositionsRequestMessage : GameFightPlacementPositionRequestMessage
    {
        public override short Protocol => 6541;

        public double RequestedId { get; set; }

        public GameFightPlacementSwapPositionsRequestMessage()
        {
        }

        public GameFightPlacementSwapPositionsRequestMessage(short cellId, double requestedId) : base(cellId)
        {
            RequestedId = requestedId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(RequestedId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            RequestedId = reader.ReadDouble();
        }
    }
}