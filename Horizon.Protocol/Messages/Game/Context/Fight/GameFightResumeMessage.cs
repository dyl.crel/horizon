using Horizon.Protocol.Types.Game.Action.Fight;
using Horizon.Protocol.Types.Game.Actions.Fight;
using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightResumeMessage : GameFightSpectateMessage
    {
        public override short Protocol => 6067;

        public GameFightSpellCooldown[] SpellCooldowns { get; set; }

        public byte SummonCount { get; set; }

        public byte BombCount { get; set; }

        public GameFightResumeMessage()
        {
        }

        public GameFightResumeMessage(FightDispellableEffectExtendedInformations[] effects, GameActionMark[] marks, short gameTurn, int fightStart, Types.Game.Idol.Idol[] idols, GameFightSpellCooldown[] spellCooldowns, byte summonCount, byte bombCount) : base(effects, marks, gameTurn, fightStart, idols)
        {
            SpellCooldowns = spellCooldowns;
            SummonCount = summonCount;
            BombCount = bombCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(SpellCooldowns.Length);
            for (var i = 0; i < SpellCooldowns.Length; i++)
            {
                SpellCooldowns[i].Serialize(writer);
            }
            writer.WriteByte(SummonCount);
            writer.WriteByte(BombCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SpellCooldowns = new GameFightSpellCooldown[reader.ReadShort()];
            for (var i = 0; i < SpellCooldowns.Length; i++)
            {
                SpellCooldowns[i] = new GameFightSpellCooldown();
                SpellCooldowns[i].Deserialize(reader);
            }
            SummonCount = reader.ReadByte();
            BombCount = reader.ReadByte();
        }
    }
}