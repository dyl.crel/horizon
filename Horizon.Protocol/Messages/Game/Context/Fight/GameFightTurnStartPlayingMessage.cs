namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnStartPlayingMessage : NetworkMessage
    {
        public override short Protocol => 6465;

        public GameFightTurnStartPlayingMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}