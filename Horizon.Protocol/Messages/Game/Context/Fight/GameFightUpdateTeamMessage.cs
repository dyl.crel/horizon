using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightUpdateTeamMessage : NetworkMessage
    {
        public override short Protocol => 5572;

        public short FightId { get; set; }

        public FightTeamInformations Team { get; set; }

        public GameFightUpdateTeamMessage()
        {
        }

        public GameFightUpdateTeamMessage(short fightId, FightTeamInformations team)
        {
            FightId = fightId;
            Team = team;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            Team.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            Team = new FightTeamInformations();
            Team.Deserialize(reader);
        }
    }
}