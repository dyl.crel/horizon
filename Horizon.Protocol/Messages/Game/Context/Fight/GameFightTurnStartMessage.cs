namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnStartMessage : NetworkMessage
    {
        public override short Protocol => 714;

        public double Id { get; set; }

        public int WaitTime { get; set; }

        public GameFightTurnStartMessage()
        {
        }

        public GameFightTurnStartMessage(double id, int waitTime)
        {
            Id = id;
            WaitTime = waitTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
            writer.WriteVarInt(WaitTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
            WaitTime = reader.ReadVarInt();
        }
    }
}