namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightStartingMessage : NetworkMessage
    {
        public override short Protocol => 700;

        public byte FightType { get; set; }

        public short FightId { get; set; }

        public double AttackerId { get; set; }

        public double DefenderId { get; set; }

        public GameFightStartingMessage()
        {
        }

        public GameFightStartingMessage(byte fightType, short fightId, double attackerId, double defenderId)
        {
            FightType = fightType;
            FightId = fightId;
            AttackerId = attackerId;
            DefenderId = defenderId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(FightType);
            writer.WriteVarShort(FightId);
            writer.WriteDouble(AttackerId);
            writer.WriteDouble(DefenderId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightType = reader.ReadByte();
            FightId = reader.ReadVarShort();
            AttackerId = reader.ReadDouble();
            DefenderId = reader.ReadDouble();
        }
    }
}