namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightReadyMessage : NetworkMessage
    {
        public override short Protocol => 708;

        public bool IsReady { get; set; }

        public GameFightReadyMessage()
        {
        }

        public GameFightReadyMessage(bool isReady)
        {
            IsReady = isReady;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(IsReady);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IsReady = reader.ReadBoolean();
        }
    }
}