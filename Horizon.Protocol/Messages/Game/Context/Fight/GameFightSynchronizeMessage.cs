using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightSynchronizeMessage : NetworkMessage
    {
        public override short Protocol => 5921;

        public GameFightFighterInformations[] Fighters { get; set; }

        public GameFightSynchronizeMessage()
        {
        }

        public GameFightSynchronizeMessage(GameFightFighterInformations[] fighters)
        {
            Fighters = fighters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Fighters.Length);
            for (var i = 0; i < Fighters.Length; i++)
            {
                writer.WriteShort(Fighters[i].Protocol);
                Fighters[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Fighters = new GameFightFighterInformations[reader.ReadShort()];
            for (var i = 0; i < Fighters.Length; i++)
            {
                Fighters[i] = ProtocolTypesManager.Instance<GameFightFighterInformations>(reader.ReadShort());
                Fighters[i].Deserialize(reader);
            }
        }
    }
}