using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightSpectatorJoinMessage : GameFightJoinMessage
    {
        public override short Protocol => 6504;

        public NamedPartyTeam[] NamedPartyTeams { get; set; }

        public GameFightSpectatorJoinMessage()
        {
        }

        public GameFightSpectatorJoinMessage(bool isTeamPhase, bool canBeCancelled, bool canSayReady, bool isFightStarted, short timeMaxBeforeFightStart, byte fightType, NamedPartyTeam[] namedPartyTeams) : base(isTeamPhase, canBeCancelled, canSayReady, isFightStarted, timeMaxBeforeFightStart, fightType)
        {
            NamedPartyTeams = namedPartyTeams;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(NamedPartyTeams.Length);
            for (var i = 0; i < NamedPartyTeams.Length; i++)
            {
                NamedPartyTeams[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NamedPartyTeams = new NamedPartyTeam[reader.ReadShort()];
            for (var i = 0; i < NamedPartyTeams.Length; i++)
            {
                NamedPartyTeams[i] = new NamedPartyTeam();
                NamedPartyTeams[i].Deserialize(reader);
            }
        }
    }
}