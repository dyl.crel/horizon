namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightNewRoundMessage : NetworkMessage
    {
        public override short Protocol => 6239;

        public int RoundNumber { get; set; }

        public GameFightNewRoundMessage()
        {
        }

        public GameFightNewRoundMessage(int roundNumber)
        {
            RoundNumber = roundNumber;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(RoundNumber);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RoundNumber = reader.ReadVarInt();
        }
    }
}