namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightJoinMessage : NetworkMessage
    {
        public override short Protocol => 702;

        public bool IsTeamPhase { get; set; }

        public bool CanBeCancelled { get; set; }

        public bool CanSayReady { get; set; }

        public bool IsFightStarted { get; set; }

        public short TimeMaxBeforeFightStart { get; set; }

        public byte FightType { get; set; }

        public GameFightJoinMessage()
        {
        }

        public GameFightJoinMessage(bool isTeamPhase, bool canBeCancelled, bool canSayReady, bool isFightStarted, short timeMaxBeforeFightStart, byte fightType)
        {
            IsTeamPhase = isTeamPhase;
            CanBeCancelled = canBeCancelled;
            CanSayReady = canSayReady;
            IsFightStarted = isFightStarted;
            TimeMaxBeforeFightStart = timeMaxBeforeFightStart;
            FightType = fightType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, IsTeamPhase);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, CanBeCancelled);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, CanSayReady);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, IsFightStarted);
            writer.WriteByte(flag0);
            writer.WriteShort(TimeMaxBeforeFightStart);
            writer.WriteByte(FightType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            IsTeamPhase = BooleanByteWrapper.GetFlag(flag0, 0);
            CanBeCancelled = BooleanByteWrapper.GetFlag(flag0, 1);
            CanSayReady = BooleanByteWrapper.GetFlag(flag0, 2);
            IsFightStarted = BooleanByteWrapper.GetFlag(flag0, 3);
            TimeMaxBeforeFightStart = reader.ReadShort();
            FightType = reader.ReadByte();
        }
    }
}