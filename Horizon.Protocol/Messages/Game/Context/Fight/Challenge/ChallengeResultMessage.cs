namespace Horizon.Protocol.Messages.Game.Context.Fight.Challenge
{
    public class ChallengeResultMessage : NetworkMessage
    {
        public override short Protocol => 6019;

        public short ChallengeId { get; set; }

        public bool Success { get; set; }

        public ChallengeResultMessage()
        {
        }

        public ChallengeResultMessage(short challengeId, bool success)
        {
            ChallengeId = challengeId;
            Success = success;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
            writer.WriteBoolean(Success);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ChallengeId = reader.ReadVarShort();
            Success = reader.ReadBoolean();
        }
    }
}