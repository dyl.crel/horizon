namespace Horizon.Protocol.Messages.Game.Context.Fight.Challenge
{
    public class ChallengeTargetsListMessage : NetworkMessage
    {
        public override short Protocol => 5613;

        public double[] TargetIds { get; set; }

        public short[] TargetCells { get; set; }

        public ChallengeTargetsListMessage()
        {
        }

        public ChallengeTargetsListMessage(double[] targetIds, short[] targetCells)
        {
            TargetIds = targetIds;
            TargetCells = targetCells;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(TargetIds.Length);
            for (var i = 0; i < TargetIds.Length; i++)
            {
                writer.WriteDouble(TargetIds[i]);
            }
            writer.WriteShort(TargetCells.Length);
            for (var i = 0; i < TargetCells.Length; i++)
            {
                writer.WriteShort(TargetCells[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TargetIds = new double[reader.ReadShort()];
            for (var i = 0; i < TargetIds.Length; i++)
            {
                TargetIds[i] = reader.ReadDouble();
            }
            TargetCells = new short[reader.ReadShort()];
            for (var i = 0; i < TargetCells.Length; i++)
            {
                TargetCells[i] = reader.ReadShort();
            }
        }
    }
}