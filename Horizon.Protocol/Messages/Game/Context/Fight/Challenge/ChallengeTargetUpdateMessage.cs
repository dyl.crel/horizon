namespace Horizon.Protocol.Messages.Game.Context.Fight.Challenge
{
    public class ChallengeTargetUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6123;

        public short ChallengeId { get; set; }

        public double TargetId { get; set; }

        public ChallengeTargetUpdateMessage()
        {
        }

        public ChallengeTargetUpdateMessage(short challengeId, double targetId)
        {
            ChallengeId = challengeId;
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ChallengeId = reader.ReadVarShort();
            TargetId = reader.ReadDouble();
        }
    }
}