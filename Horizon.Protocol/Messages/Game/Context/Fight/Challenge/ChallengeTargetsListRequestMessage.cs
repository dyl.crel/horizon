namespace Horizon.Protocol.Messages.Game.Context.Fight.Challenge
{
    public class ChallengeTargetsListRequestMessage : NetworkMessage
    {
        public override short Protocol => 5614;

        public short ChallengeId { get; set; }

        public ChallengeTargetsListRequestMessage()
        {
        }

        public ChallengeTargetsListRequestMessage(short challengeId)
        {
            ChallengeId = challengeId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ChallengeId = reader.ReadVarShort();
        }
    }
}