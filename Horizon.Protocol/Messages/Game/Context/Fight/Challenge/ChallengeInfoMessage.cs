namespace Horizon.Protocol.Messages.Game.Context.Fight.Challenge
{
    public class ChallengeInfoMessage : NetworkMessage
    {
        public override short Protocol => 6022;

        public short ChallengeId { get; set; }

        public double TargetId { get; set; }

        public int XpBonus { get; set; }

        public int DropBonus { get; set; }

        public ChallengeInfoMessage()
        {
        }

        public ChallengeInfoMessage(short challengeId, double targetId, int xpBonus, int dropBonus)
        {
            ChallengeId = challengeId;
            TargetId = targetId;
            XpBonus = xpBonus;
            DropBonus = dropBonus;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(XpBonus);
            writer.WriteVarInt(DropBonus);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ChallengeId = reader.ReadVarShort();
            TargetId = reader.ReadDouble();
            XpBonus = reader.ReadVarInt();
            DropBonus = reader.ReadVarInt();
        }
    }
}