namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPauseMessage : NetworkMessage
    {
        public override short Protocol => 6754;

        public bool IsPaused { get; set; }

        public GameFightPauseMessage()
        {
        }

        public GameFightPauseMessage(bool isPaused)
        {
            IsPaused = isPaused;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(IsPaused);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IsPaused = reader.ReadBoolean();
        }
    }
}