namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementSwapPositionsCancelMessage : NetworkMessage
    {
        public override short Protocol => 6543;

        public int RequestId { get; set; }

        public GameFightPlacementSwapPositionsCancelMessage()
        {
        }

        public GameFightPlacementSwapPositionsCancelMessage(int requestId)
        {
            RequestId = requestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(RequestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadInt();
        }
    }
}