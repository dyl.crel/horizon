using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Fight.Arena
{
    public class ArenaFighterLeaveMessage : NetworkMessage
    {
        public override short Protocol => 6700;

        public CharacterBasicMinimalInformations Leaver { get; set; }

        public ArenaFighterLeaveMessage()
        {
        }

        public ArenaFighterLeaveMessage(CharacterBasicMinimalInformations leaver)
        {
            Leaver = leaver;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Leaver.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Leaver = new CharacterBasicMinimalInformations();
            Leaver.Deserialize(reader);
        }
    }
}