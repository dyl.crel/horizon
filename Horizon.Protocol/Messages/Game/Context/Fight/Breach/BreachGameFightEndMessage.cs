using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Fight.Breach
{
    public class BreachGameFightEndMessage : GameFightEndMessage
    {
        public override short Protocol => 6809;

        public int Budget { get; set; }

        public BreachGameFightEndMessage()
        {
        }

        public BreachGameFightEndMessage(int duration, short ageBonus, short lootShareLimitMalus, FightResultListEntry[] results, NamedPartyTeamWithOutcome[] namedPartyTeamsOutcomes, int budget) : base(duration, ageBonus, lootShareLimitMalus, results, namedPartyTeamsOutcomes)
        {
            Budget = budget;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Budget);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Budget = reader.ReadInt();
        }
    }
}