namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnResumeMessage : GameFightTurnStartMessage
    {
        public override short Protocol => 6307;

        public int RemainingTime { get; set; }

        public GameFightTurnResumeMessage()
        {
        }

        public GameFightTurnResumeMessage(double id, int waitTime, int remainingTime) : base(id, waitTime)
        {
            RemainingTime = remainingTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(RemainingTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            RemainingTime = reader.ReadVarInt();
        }
    }
}