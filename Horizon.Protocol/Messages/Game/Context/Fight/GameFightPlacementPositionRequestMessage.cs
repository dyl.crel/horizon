namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementPositionRequestMessage : NetworkMessage
    {
        public override short Protocol => 704;

        public short CellId { get; set; }

        public GameFightPlacementPositionRequestMessage()
        {
        }

        public GameFightPlacementPositionRequestMessage(short cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
        }
    }
}