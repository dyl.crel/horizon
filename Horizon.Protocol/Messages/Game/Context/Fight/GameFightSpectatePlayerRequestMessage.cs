namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightSpectatePlayerRequestMessage : NetworkMessage
    {
        public override short Protocol => 6474;

        public long PlayerId { get; set; }

        public GameFightSpectatePlayerRequestMessage()
        {
        }

        public GameFightSpectatePlayerRequestMessage(long playerId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
        }
    }
}