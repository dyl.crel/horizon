namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightHumanReadyStateMessage : NetworkMessage
    {
        public override short Protocol => 740;

        public long CharacterId { get; set; }

        public bool IsReady { get; set; }

        public GameFightHumanReadyStateMessage()
        {
        }

        public GameFightHumanReadyStateMessage(long characterId, bool isReady)
        {
            CharacterId = characterId;
            IsReady = isReady;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(CharacterId);
            writer.WriteBoolean(IsReady);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CharacterId = reader.ReadVarLong();
            IsReady = reader.ReadBoolean();
        }
    }
}