namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightLeaveMessage : NetworkMessage
    {
        public override short Protocol => 721;

        public double CharId { get; set; }

        public GameFightLeaveMessage()
        {
        }

        public GameFightLeaveMessage(double charId)
        {
            CharId = charId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(CharId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CharId = reader.ReadDouble();
        }
    }
}