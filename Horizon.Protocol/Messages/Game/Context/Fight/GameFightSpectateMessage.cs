using Horizon.Protocol.Types.Game.Action.Fight;
using Horizon.Protocol.Types.Game.Actions.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightSpectateMessage : NetworkMessage
    {
        public override short Protocol => 6069;

        public FightDispellableEffectExtendedInformations[] Effects { get; set; }

        public GameActionMark[] Marks { get; set; }

        public short GameTurn { get; set; }

        public int FightStart { get; set; }

        public Types.Game.Idol.Idol[] Idols { get; set; }

        public GameFightSpectateMessage()
        {
        }

        public GameFightSpectateMessage(FightDispellableEffectExtendedInformations[] effects, GameActionMark[] marks, short gameTurn, int fightStart, Types.Game.Idol.Idol[] idols)
        {
            Effects = effects;
            Marks = marks;
            GameTurn = gameTurn;
            FightStart = fightStart;
            Idols = idols;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i].Serialize(writer);
            }
            writer.WriteShort(Marks.Length);
            for (var i = 0; i < Marks.Length; i++)
            {
                Marks[i].Serialize(writer);
            }
            writer.WriteVarShort(GameTurn);
            writer.WriteInt(FightStart);
            writer.WriteShort(Idols.Length);
            for (var i = 0; i < Idols.Length; i++)
            {
                Idols[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Effects = new FightDispellableEffectExtendedInformations[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = new FightDispellableEffectExtendedInformations();
                Effects[i].Deserialize(reader);
            }
            Marks = new GameActionMark[reader.ReadShort()];
            for (var i = 0; i < Marks.Length; i++)
            {
                Marks[i] = new GameActionMark();
                Marks[i].Deserialize(reader);
            }
            GameTurn = reader.ReadVarShort();
            FightStart = reader.ReadInt();
            Idols = new Types.Game.Idol.Idol[reader.ReadShort()];
            for (var i = 0; i < Idols.Length; i++)
            {
                Idols[i] = new Types.Game.Idol.Idol();
                Idols[i].Deserialize(reader);
            }
        }
    }
}