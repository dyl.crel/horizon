namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnReadyMessage : NetworkMessage
    {
        public override short Protocol => 716;

        public bool IsReady { get; set; }

        public GameFightTurnReadyMessage()
        {
        }

        public GameFightTurnReadyMessage(bool isReady)
        {
            IsReady = isReady;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(IsReady);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IsReady = reader.ReadBoolean();
        }
    }
}