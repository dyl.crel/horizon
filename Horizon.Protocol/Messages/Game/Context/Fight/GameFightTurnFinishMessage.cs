namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnFinishMessage : NetworkMessage
    {
        public override short Protocol => 718;

        public bool IsAfk { get; set; }

        public GameFightTurnFinishMessage()
        {
        }

        public GameFightTurnFinishMessage(bool isAfk)
        {
            IsAfk = isAfk;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(IsAfk);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IsAfk = reader.ReadBoolean();
        }
    }
}