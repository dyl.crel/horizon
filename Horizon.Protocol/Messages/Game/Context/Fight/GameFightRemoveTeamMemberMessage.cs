namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightRemoveTeamMemberMessage : NetworkMessage
    {
        public override short Protocol => 711;

        public short FightId { get; set; }

        public byte TeamId { get; set; }

        public double CharId { get; set; }

        public GameFightRemoveTeamMemberMessage()
        {
        }

        public GameFightRemoveTeamMemberMessage(short fightId, byte teamId, double charId)
        {
            FightId = fightId;
            TeamId = teamId;
            CharId = charId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteByte(TeamId);
            writer.WriteDouble(CharId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            TeamId = reader.ReadByte();
            CharId = reader.ReadDouble();
        }
    }
}