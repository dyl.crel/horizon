namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightNewWaveMessage : NetworkMessage
    {
        public override short Protocol => 6490;

        public byte Id { get; set; }

        public byte TeamId { get; set; }

        public short NbTurnBeforeNextWave { get; set; }

        public GameFightNewWaveMessage()
        {
        }

        public GameFightNewWaveMessage(byte id, byte teamId, short nbTurnBeforeNextWave)
        {
            Id = id;
            TeamId = teamId;
            NbTurnBeforeNextWave = nbTurnBeforeNextWave;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Id);
            writer.WriteByte(TeamId);
            writer.WriteShort(NbTurnBeforeNextWave);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadByte();
            TeamId = reader.ReadByte();
            NbTurnBeforeNextWave = reader.ReadShort();
        }
    }
}