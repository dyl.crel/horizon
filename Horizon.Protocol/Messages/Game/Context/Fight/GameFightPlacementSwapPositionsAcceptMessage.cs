namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementSwapPositionsAcceptMessage : NetworkMessage
    {
        public override short Protocol => 6547;

        public int RequestId { get; set; }

        public GameFightPlacementSwapPositionsAcceptMessage()
        {
        }

        public GameFightPlacementSwapPositionsAcceptMessage(int requestId)
        {
            RequestId = requestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(RequestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadInt();
        }
    }
}