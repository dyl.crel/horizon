using Horizon.Protocol.Types.Game.Character.Characteristic;
using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class SlaveSwitchContextMessage : NetworkMessage
    {
        public override short Protocol => 6214;

        public double MasterId { get; set; }

        public double SlaveId { get; set; }

        public SpellItem[] SlaveSpells { get; set; }

        public CharacterCharacteristicsInformations SlaveStats { get; set; }

        public Types.Game.Shortcut.Shortcut[] Shortcuts { get; set; }

        public SlaveSwitchContextMessage()
        {
        }

        public SlaveSwitchContextMessage(double masterId, double slaveId, SpellItem[] slaveSpells, CharacterCharacteristicsInformations slaveStats, Types.Game.Shortcut.Shortcut[] shortcuts)
        {
            MasterId = masterId;
            SlaveId = slaveId;
            SlaveSpells = slaveSpells;
            SlaveStats = slaveStats;
            Shortcuts = shortcuts;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MasterId);
            writer.WriteDouble(SlaveId);
            writer.WriteShort(SlaveSpells.Length);
            for (var i = 0; i < SlaveSpells.Length; i++)
            {
                SlaveSpells[i].Serialize(writer);
            }
            SlaveStats.Serialize(writer);
            writer.WriteShort(Shortcuts.Length);
            for (var i = 0; i < Shortcuts.Length; i++)
            {
                writer.WriteShort(Shortcuts[i].Protocol);
                Shortcuts[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MasterId = reader.ReadDouble();
            SlaveId = reader.ReadDouble();
            SlaveSpells = new SpellItem[reader.ReadShort()];
            for (var i = 0; i < SlaveSpells.Length; i++)
            {
                SlaveSpells[i] = new SpellItem();
                SlaveSpells[i].Deserialize(reader);
            }
            SlaveStats = new CharacterCharacteristicsInformations();
            SlaveStats.Deserialize(reader);
            Shortcuts = new Types.Game.Shortcut.Shortcut[reader.ReadShort()];
            for (var i = 0; i < Shortcuts.Length; i++)
            {
                Shortcuts[i] = ProtocolTypesManager.Instance<Types.Game.Shortcut.Shortcut>(reader.ReadShort());
                Shortcuts[i].Deserialize(reader);
            }
        }
    }
}