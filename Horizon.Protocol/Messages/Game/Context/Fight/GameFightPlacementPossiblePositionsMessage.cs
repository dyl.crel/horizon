namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementPossiblePositionsMessage : NetworkMessage
    {
        public override short Protocol => 703;

        public short[] PositionsForChallengers { get; set; }

        public short[] PositionsForDefenders { get; set; }

        public byte TeamNumber { get; set; }

        public GameFightPlacementPossiblePositionsMessage()
        {
        }

        public GameFightPlacementPossiblePositionsMessage(short[] positionsForChallengers, short[] positionsForDefenders, byte teamNumber)
        {
            PositionsForChallengers = positionsForChallengers;
            PositionsForDefenders = positionsForDefenders;
            TeamNumber = teamNumber;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PositionsForChallengers.Length);
            for (var i = 0; i < PositionsForChallengers.Length; i++)
            {
                writer.WriteVarShort(PositionsForChallengers[i]);
            }
            writer.WriteShort(PositionsForDefenders.Length);
            for (var i = 0; i < PositionsForDefenders.Length; i++)
            {
                writer.WriteVarShort(PositionsForDefenders[i]);
            }
            writer.WriteByte(TeamNumber);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PositionsForChallengers = new short[reader.ReadShort()];
            for (var i = 0; i < PositionsForChallengers.Length; i++)
            {
                PositionsForChallengers[i] = reader.ReadVarShort();
            }
            PositionsForDefenders = new short[reader.ReadShort()];
            for (var i = 0; i < PositionsForDefenders.Length; i++)
            {
                PositionsForDefenders[i] = reader.ReadVarShort();
            }
            TeamNumber = reader.ReadByte();
        }
    }
}