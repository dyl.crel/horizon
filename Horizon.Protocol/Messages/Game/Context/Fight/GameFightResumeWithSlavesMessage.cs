using Horizon.Protocol.Types.Game.Action.Fight;
using Horizon.Protocol.Types.Game.Actions.Fight;
using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightResumeWithSlavesMessage : GameFightResumeMessage
    {
        public override short Protocol => 6215;

        public GameFightResumeSlaveInfo[] SlavesInfo { get; set; }

        public GameFightResumeWithSlavesMessage()
        {
        }

        public GameFightResumeWithSlavesMessage(FightDispellableEffectExtendedInformations[] effects, GameActionMark[] marks, short gameTurn, int fightStart, Types.Game.Idol.Idol[] idols, GameFightSpellCooldown[] spellCooldowns, byte summonCount, byte bombCount, GameFightResumeSlaveInfo[] slavesInfo) : base(effects, marks, gameTurn, fightStart, idols, spellCooldowns, summonCount, bombCount)
        {
            SlavesInfo = slavesInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(SlavesInfo.Length);
            for (var i = 0; i < SlavesInfo.Length; i++)
            {
                SlavesInfo[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SlavesInfo = new GameFightResumeSlaveInfo[reader.ReadShort()];
            for (var i = 0; i < SlavesInfo.Length; i++)
            {
                SlavesInfo[i] = new GameFightResumeSlaveInfo();
                SlavesInfo[i].Deserialize(reader);
            }
        }
    }
}