namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightJoinRequestMessage : NetworkMessage
    {
        public override short Protocol => 701;

        public double FighterId { get; set; }

        public short FightId { get; set; }

        public GameFightJoinRequestMessage()
        {
        }

        public GameFightJoinRequestMessage(double fighterId, short fightId)
        {
            FighterId = fighterId;
            FightId = fightId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(FighterId);
            writer.WriteVarShort(FightId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FighterId = reader.ReadDouble();
            FightId = reader.ReadVarShort();
        }
    }
}