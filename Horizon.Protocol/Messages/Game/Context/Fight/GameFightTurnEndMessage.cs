namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightTurnEndMessage : NetworkMessage
    {
        public override short Protocol => 719;

        public double Id { get; set; }

        public GameFightTurnEndMessage()
        {
        }

        public GameFightTurnEndMessage(double id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
        }
    }
}