namespace Horizon.Protocol.Messages.Game.Context.Fight
{
    public class GameFightPlacementSwapPositionsCancelledMessage : NetworkMessage
    {
        public override short Protocol => 6546;

        public int RequestId { get; set; }

        public double CancellerId { get; set; }

        public GameFightPlacementSwapPositionsCancelledMessage()
        {
        }

        public GameFightPlacementSwapPositionsCancelledMessage(int requestId, double cancellerId)
        {
            RequestId = requestId;
            CancellerId = cancellerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(RequestId);
            writer.WriteDouble(CancellerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequestId = reader.ReadInt();
            CancellerId = reader.ReadDouble();
        }
    }
}