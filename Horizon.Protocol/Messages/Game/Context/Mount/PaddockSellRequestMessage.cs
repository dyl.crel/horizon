namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class PaddockSellRequestMessage : NetworkMessage
    {
        public override short Protocol => 5953;

        public long Price { get; set; }

        public bool ForSale { get; set; }

        public PaddockSellRequestMessage()
        {
        }

        public PaddockSellRequestMessage(long price, bool forSale)
        {
            Price = price;
            ForSale = forSale;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Price);
            writer.WriteBoolean(ForSale);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Price = reader.ReadVarLong();
            ForSale = reader.ReadBoolean();
        }
    }
}