namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class GameDataPaddockObjectRemoveMessage : NetworkMessage
    {
        public override short Protocol => 5993;

        public short CellId { get; set; }

        public GameDataPaddockObjectRemoveMessage()
        {
        }

        public GameDataPaddockObjectRemoveMessage(short cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
        }
    }
}