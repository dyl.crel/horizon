namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountSterilizedMessage : NetworkMessage
    {
        public override short Protocol => 5977;

        public int MountId { get; set; }

        public MountSterilizedMessage()
        {
        }

        public MountSterilizedMessage(int mountId)
        {
            MountId = mountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountId = reader.ReadVarInt();
        }
    }
}