using Horizon.Protocol.Types.Game.Paddock;

namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class GameDataPaddockObjectListAddMessage : NetworkMessage
    {
        public override short Protocol => 5992;

        public PaddockItem[] PaddockItemDescription { get; set; }

        public GameDataPaddockObjectListAddMessage()
        {
        }

        public GameDataPaddockObjectListAddMessage(PaddockItem[] paddockItemDescription)
        {
            PaddockItemDescription = paddockItemDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PaddockItemDescription.Length);
            for (var i = 0; i < PaddockItemDescription.Length; i++)
            {
                PaddockItemDescription[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PaddockItemDescription = new PaddockItem[reader.ReadShort()];
            for (var i = 0; i < PaddockItemDescription.Length; i++)
            {
                PaddockItemDescription[i] = new PaddockItem();
                PaddockItemDescription[i].Deserialize(reader);
            }
        }
    }
}