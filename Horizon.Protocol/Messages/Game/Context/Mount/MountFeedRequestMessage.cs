namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountFeedRequestMessage : NetworkMessage
    {
        public override short Protocol => 6189;

        public int MountUid { get; set; }

        public byte MountLocation { get; set; }

        public int MountFoodUid { get; set; }

        public int Quantity { get; set; }

        public MountFeedRequestMessage()
        {
        }

        public MountFeedRequestMessage(int mountUid, byte mountLocation, int mountFoodUid, int quantity)
        {
            MountUid = mountUid;
            MountLocation = mountLocation;
            MountFoodUid = mountFoodUid;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MountUid);
            writer.WriteByte(MountLocation);
            writer.WriteVarInt(MountFoodUid);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountUid = reader.ReadVarInt();
            MountLocation = reader.ReadByte();
            MountFoodUid = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}