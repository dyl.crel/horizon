namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountReleasedMessage : NetworkMessage
    {
        public override short Protocol => 6308;

        public int MountId { get; set; }

        public MountReleasedMessage()
        {
        }

        public MountReleasedMessage(int mountId)
        {
            MountId = mountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountId = reader.ReadVarInt();
        }
    }
}