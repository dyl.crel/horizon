namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountDataErrorMessage : NetworkMessage
    {
        public override short Protocol => 6172;

        public byte Reason { get; set; }

        public MountDataErrorMessage()
        {
        }

        public MountDataErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}