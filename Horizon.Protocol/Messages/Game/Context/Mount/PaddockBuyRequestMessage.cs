namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class PaddockBuyRequestMessage : NetworkMessage
    {
        public override short Protocol => 5951;

        public long ProposedPrice { get; set; }

        public PaddockBuyRequestMessage()
        {
        }

        public PaddockBuyRequestMessage(long proposedPrice)
        {
            ProposedPrice = proposedPrice;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(ProposedPrice);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ProposedPrice = reader.ReadVarLong();
        }
    }
}