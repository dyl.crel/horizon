using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountSetMessage : NetworkMessage
    {
        public override short Protocol => 5968;

        public MountClientData MountData { get; set; }

        public MountSetMessage()
        {
        }

        public MountSetMessage(MountClientData mountData)
        {
            MountData = mountData;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            MountData.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountData = new MountClientData();
            MountData.Deserialize(reader);
        }
    }
}