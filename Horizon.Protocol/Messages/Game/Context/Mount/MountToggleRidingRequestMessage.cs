namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountToggleRidingRequestMessage : NetworkMessage
    {
        public override short Protocol => 5976;

        public MountToggleRidingRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}