namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountHarnessDissociateRequestMessage : NetworkMessage
    {
        public override short Protocol => 6696;

        public MountHarnessDissociateRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}