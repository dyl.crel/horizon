namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountEquipedErrorMessage : NetworkMessage
    {
        public override short Protocol => 5963;

        public byte ErrorType { get; set; }

        public MountEquipedErrorMessage()
        {
        }

        public MountEquipedErrorMessage(byte errorType)
        {
            ErrorType = errorType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ErrorType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ErrorType = reader.ReadByte();
        }
    }
}