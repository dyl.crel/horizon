namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountRidingMessage : NetworkMessage
    {
        public override short Protocol => 5967;

        public bool IsRiding { get; set; }

        public bool IsAutopilot { get; set; }

        public MountRidingMessage()
        {
        }

        public MountRidingMessage(bool isRiding, bool isAutopilot)
        {
            IsRiding = isRiding;
            IsAutopilot = isAutopilot;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, IsRiding);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsAutopilot);
            writer.WriteByte(flag0);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            IsRiding = BooleanByteWrapper.GetFlag(flag0, 0);
            IsAutopilot = BooleanByteWrapper.GetFlag(flag0, 1);
        }
    }
}