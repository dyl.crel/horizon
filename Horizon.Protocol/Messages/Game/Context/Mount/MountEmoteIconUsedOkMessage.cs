namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountEmoteIconUsedOkMessage : NetworkMessage
    {
        public override short Protocol => 5978;

        public int MountId { get; set; }

        public byte ReactionType { get; set; }

        public MountEmoteIconUsedOkMessage()
        {
        }

        public MountEmoteIconUsedOkMessage(int mountId, byte reactionType)
        {
            MountId = mountId;
            ReactionType = reactionType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MountId);
            writer.WriteByte(ReactionType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountId = reader.ReadVarInt();
            ReactionType = reader.ReadByte();
        }
    }
}