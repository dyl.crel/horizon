namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class PaddockRemoveItemRequestMessage : NetworkMessage
    {
        public override short Protocol => 5958;

        public short CellId { get; set; }

        public PaddockRemoveItemRequestMessage()
        {
        }

        public PaddockRemoveItemRequestMessage(short cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
        }
    }
}