namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountUnSetMessage : NetworkMessage
    {
        public override short Protocol => 5982;

        public MountUnSetMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}