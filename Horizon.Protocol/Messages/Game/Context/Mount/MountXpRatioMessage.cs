namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountXpRatioMessage : NetworkMessage
    {
        public override short Protocol => 5970;

        public byte Ratio { get; set; }

        public MountXpRatioMessage()
        {
        }

        public MountXpRatioMessage(byte ratio)
        {
            Ratio = ratio;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Ratio);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Ratio = reader.ReadByte();
        }
    }
}