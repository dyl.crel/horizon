namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountRenameRequestMessage : NetworkMessage
    {
        public override short Protocol => 5987;

        public string Name { get; set; }

        public int MountId { get; set; }

        public MountRenameRequestMessage()
        {
        }

        public MountRenameRequestMessage(string name, int mountId)
        {
            Name = name;
            MountId = mountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteVarInt(MountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            MountId = reader.ReadVarInt();
        }
    }
}