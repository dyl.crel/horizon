using Horizon.Protocol.Types.Game.Paddock;

namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class GameDataPaddockObjectAddMessage : NetworkMessage
    {
        public override short Protocol => 5990;

        public PaddockItem PaddockItemDescription { get; set; }

        public GameDataPaddockObjectAddMessage()
        {
        }

        public GameDataPaddockObjectAddMessage(PaddockItem paddockItemDescription)
        {
            PaddockItemDescription = paddockItemDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            PaddockItemDescription.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PaddockItemDescription = new PaddockItem();
            PaddockItemDescription.Deserialize(reader);
        }
    }
}