namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class PaddockMoveItemRequestMessage : NetworkMessage
    {
        public override short Protocol => 6052;

        public short OldCellId { get; set; }

        public short NewCellId { get; set; }

        public PaddockMoveItemRequestMessage()
        {
        }

        public PaddockMoveItemRequestMessage(short oldCellId, short newCellId)
        {
            OldCellId = oldCellId;
            NewCellId = newCellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(OldCellId);
            writer.WriteVarShort(NewCellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            OldCellId = reader.ReadVarShort();
            NewCellId = reader.ReadVarShort();
        }
    }
}