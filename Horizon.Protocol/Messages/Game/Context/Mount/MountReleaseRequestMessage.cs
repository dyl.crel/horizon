namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountReleaseRequestMessage : NetworkMessage
    {
        public override short Protocol => 5980;

        public MountReleaseRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}