namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountInformationInPaddockRequestMessage : NetworkMessage
    {
        public override short Protocol => 5975;

        public int MapRideId { get; set; }

        public MountInformationInPaddockRequestMessage()
        {
        }

        public MountInformationInPaddockRequestMessage(int mapRideId)
        {
            MapRideId = mapRideId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MapRideId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapRideId = reader.ReadVarInt();
        }
    }
}