namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountRenamedMessage : NetworkMessage
    {
        public override short Protocol => 5983;

        public int MountId { get; set; }

        public string Name { get; set; }

        public MountRenamedMessage()
        {
        }

        public MountRenamedMessage(int mountId, string name)
        {
            MountId = mountId;
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MountId);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountId = reader.ReadVarInt();
            Name = reader.ReadUTF();
        }
    }
}