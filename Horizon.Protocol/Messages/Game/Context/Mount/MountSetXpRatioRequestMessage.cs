namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountSetXpRatioRequestMessage : NetworkMessage
    {
        public override short Protocol => 5989;

        public byte XpRatio { get; set; }

        public MountSetXpRatioRequestMessage()
        {
        }

        public MountSetXpRatioRequestMessage(byte xpRatio)
        {
            XpRatio = xpRatio;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(XpRatio);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            XpRatio = reader.ReadByte();
        }
    }
}