namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountHarnessColorsUpdateRequestMessage : NetworkMessage
    {
        public override short Protocol => 6697;

        public bool UseHarnessColors { get; set; }

        public MountHarnessColorsUpdateRequestMessage()
        {
        }

        public MountHarnessColorsUpdateRequestMessage(bool useHarnessColors)
        {
            UseHarnessColors = useHarnessColors;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(UseHarnessColors);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            UseHarnessColors = reader.ReadBoolean();
        }
    }
}