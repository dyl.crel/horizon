namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountInformationRequestMessage : NetworkMessage
    {
        public override short Protocol => 5972;

        public double Id { get; set; }

        public double Time { get; set; }

        public MountInformationRequestMessage()
        {
        }

        public MountInformationRequestMessage(double id, double time)
        {
            Id = id;
            Time = time;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
            writer.WriteDouble(Time);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
            Time = reader.ReadDouble();
        }
    }
}