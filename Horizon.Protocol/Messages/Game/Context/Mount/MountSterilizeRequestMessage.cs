namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountSterilizeRequestMessage : NetworkMessage
    {
        public override short Protocol => 5962;

        public MountSterilizeRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}