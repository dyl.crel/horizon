namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class PaddockBuyResultMessage : NetworkMessage
    {
        public override short Protocol => 6516;

        public double PaddockId { get; set; }

        public bool Bought { get; set; }

        public long RealPrice { get; set; }

        public PaddockBuyResultMessage()
        {
        }

        public PaddockBuyResultMessage(double paddockId, bool bought, long realPrice)
        {
            PaddockId = paddockId;
            Bought = bought;
            RealPrice = realPrice;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(PaddockId);
            writer.WriteBoolean(Bought);
            writer.WriteVarLong(RealPrice);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PaddockId = reader.ReadDouble();
            Bought = reader.ReadBoolean();
            RealPrice = reader.ReadVarLong();
        }
    }
}