using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Context.Mount
{
    public class MountDataMessage : NetworkMessage
    {
        public override short Protocol => 5973;

        public MountClientData MountData { get; set; }

        public MountDataMessage()
        {
        }

        public MountDataMessage(MountClientData mountData)
        {
            MountData = mountData;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            MountData.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountData = new MountClientData();
            MountData.Deserialize(reader);
        }
    }
}