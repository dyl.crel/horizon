namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextRemoveElementMessage : NetworkMessage
    {
        public override short Protocol => 251;

        public double Id { get; set; }

        public GameContextRemoveElementMessage()
        {
        }

        public GameContextRemoveElementMessage(double id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
        }
    }
}