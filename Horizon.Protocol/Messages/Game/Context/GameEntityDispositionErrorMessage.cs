namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameEntityDispositionErrorMessage : NetworkMessage
    {
        public override short Protocol => 5695;

        public GameEntityDispositionErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}