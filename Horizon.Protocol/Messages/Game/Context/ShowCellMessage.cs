namespace Horizon.Protocol.Messages.Game.Context
{
    public class ShowCellMessage : NetworkMessage
    {
        public override short Protocol => 5612;

        public double SourceId { get; set; }

        public short CellId { get; set; }

        public ShowCellMessage()
        {
        }

        public ShowCellMessage(double sourceId, short cellId)
        {
            SourceId = sourceId;
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(SourceId);
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SourceId = reader.ReadDouble();
            CellId = reader.ReadVarShort();
        }
    }
}