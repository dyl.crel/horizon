namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameCautiousMapMovementMessage : GameMapMovementMessage
    {
        public override short Protocol => 6497;

        public GameCautiousMapMovementMessage()
        {
        }

        public GameCautiousMapMovementMessage(short[] keyMovements, short forcedDirection, double actorId) : base(keyMovements, forcedDirection, actorId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}