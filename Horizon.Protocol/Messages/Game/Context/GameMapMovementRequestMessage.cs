namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapMovementRequestMessage : NetworkMessage
    {
        public override short Protocol => 950;

        public short[] KeyMovements { get; set; }

        public double MapId { get; set; }

        public GameMapMovementRequestMessage()
        {
        }

        public GameMapMovementRequestMessage(short[] keyMovements, double mapId)
        {
            KeyMovements = keyMovements;
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(KeyMovements.Length);
            for (var i = 0; i < KeyMovements.Length; i++)
            {
                writer.WriteShort(KeyMovements[i]);
            }
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            KeyMovements = new short[reader.ReadShort()];
            for (var i = 0; i < KeyMovements.Length; i++)
            {
                KeyMovements[i] = reader.ReadShort();
            }
            MapId = reader.ReadDouble();
        }
    }
}