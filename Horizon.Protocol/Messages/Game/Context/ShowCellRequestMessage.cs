namespace Horizon.Protocol.Messages.Game.Context
{
    public class ShowCellRequestMessage : NetworkMessage
    {
        public override short Protocol => 5611;

        public short CellId { get; set; }

        public ShowCellRequestMessage()
        {
        }

        public ShowCellRequestMessage(short cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
        }
    }
}