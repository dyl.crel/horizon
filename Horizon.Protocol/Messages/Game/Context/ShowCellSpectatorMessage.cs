namespace Horizon.Protocol.Messages.Game.Context
{
    public class ShowCellSpectatorMessage : ShowCellMessage
    {
        public override short Protocol => 6158;

        public string PlayerName { get; set; }

        public ShowCellSpectatorMessage()
        {
        }

        public ShowCellSpectatorMessage(double sourceId, short cellId, string playerName) : base(sourceId, cellId)
        {
            PlayerName = playerName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(PlayerName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerName = reader.ReadUTF();
        }
    }
}