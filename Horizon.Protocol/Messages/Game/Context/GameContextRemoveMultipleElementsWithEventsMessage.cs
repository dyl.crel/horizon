namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextRemoveMultipleElementsWithEventsMessage : GameContextRemoveMultipleElementsMessage
    {
        public override short Protocol => 6416;

        public byte[] ElementEventIds { get; set; }

        public GameContextRemoveMultipleElementsWithEventsMessage()
        {
        }

        public GameContextRemoveMultipleElementsWithEventsMessage(double[] elementsIds, byte[] elementEventIds) : base(elementsIds)
        {
            ElementEventIds = elementEventIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(ElementEventIds.Length);
            for (var i = 0; i < ElementEventIds.Length; i++)
            {
                writer.WriteByte(ElementEventIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ElementEventIds = new byte[reader.ReadShort()];
            for (var i = 0; i < ElementEventIds.Length; i++)
            {
                ElementEventIds[i] = reader.ReadByte();
            }
        }
    }
}