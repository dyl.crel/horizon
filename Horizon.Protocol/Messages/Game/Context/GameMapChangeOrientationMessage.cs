using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapChangeOrientationMessage : NetworkMessage
    {
        public override short Protocol => 946;

        public ActorOrientation Orientation { get; set; }

        public GameMapChangeOrientationMessage()
        {
        }

        public GameMapChangeOrientationMessage(ActorOrientation orientation)
        {
            Orientation = orientation;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Orientation.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Orientation = new ActorOrientation();
            Orientation.Deserialize(reader);
        }
    }
}