namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextRemoveMultipleElementsMessage : NetworkMessage
    {
        public override short Protocol => 252;

        public double[] ElementsIds { get; set; }

        public GameContextRemoveMultipleElementsMessage()
        {
        }

        public GameContextRemoveMultipleElementsMessage(double[] elementsIds)
        {
            ElementsIds = elementsIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ElementsIds.Length);
            for (var i = 0; i < ElementsIds.Length; i++)
            {
                writer.WriteDouble(ElementsIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ElementsIds = new double[reader.ReadShort()];
            for (var i = 0; i < ElementsIds.Length; i++)
            {
                ElementsIds[i] = reader.ReadDouble();
            }
        }
    }
}