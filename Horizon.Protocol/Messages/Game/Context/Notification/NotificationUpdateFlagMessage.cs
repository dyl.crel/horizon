namespace Horizon.Protocol.Messages.Game.Context.Notification
{
    public class NotificationUpdateFlagMessage : NetworkMessage
    {
        public override short Protocol => 6090;

        public short Index { get; set; }

        public NotificationUpdateFlagMessage()
        {
        }

        public NotificationUpdateFlagMessage(short index)
        {
            Index = index;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Index);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Index = reader.ReadVarShort();
        }
    }
}