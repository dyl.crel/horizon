namespace Horizon.Protocol.Messages.Game.Context.Notification
{
    public class NotificationResetMessage : NetworkMessage
    {
        public override short Protocol => 6089;

        public NotificationResetMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}