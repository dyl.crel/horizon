namespace Horizon.Protocol.Messages.Game.Context.Notification
{
    public class NotificationListMessage : NetworkMessage
    {
        public override short Protocol => 6087;

        public int[] Flags { get; set; }

        public NotificationListMessage()
        {
        }

        public NotificationListMessage(int[] flags)
        {
            Flags = flags;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Flags.Length);
            for (var i = 0; i < Flags.Length; i++)
            {
                writer.WriteVarInt(Flags[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Flags = new int[reader.ReadShort()];
            for (var i = 0; i < Flags.Length; i++)
            {
                Flags[i] = reader.ReadVarInt();
            }
        }
    }
}