namespace Horizon.Protocol.Messages.Game.Context.Notification
{
    public class NotificationByServerMessage : NetworkMessage
    {
        public override short Protocol => 6103;

        public short Id { get; set; }

        public string[] Parameters { get; set; }

        public bool ForceOpen { get; set; }

        public NotificationByServerMessage()
        {
        }

        public NotificationByServerMessage(short id, string[] parameters, bool forceOpen)
        {
            Id = id;
            Parameters = parameters;
            ForceOpen = forceOpen;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteShort(Parameters.Length);
            for (var i = 0; i < Parameters.Length; i++)
            {
                writer.WriteUTF(Parameters[i]);
            }
            writer.WriteBoolean(ForceOpen);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
            Parameters = new string[reader.ReadShort()];
            for (var i = 0; i < Parameters.Length; i++)
            {
                Parameters[i] = reader.ReadUTF();
            }
            ForceOpen = reader.ReadBoolean();
        }
    }
}