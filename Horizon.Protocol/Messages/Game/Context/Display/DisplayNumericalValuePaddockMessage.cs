namespace Horizon.Protocol.Messages.Game.Context.Display
{
    public class DisplayNumericalValuePaddockMessage : NetworkMessage
    {
        public override short Protocol => 6563;

        public int RideId { get; set; }

        public int Value { get; set; }

        public byte Type { get; set; }

        public DisplayNumericalValuePaddockMessage()
        {
        }

        public DisplayNumericalValuePaddockMessage(int rideId, int value, byte type)
        {
            RideId = rideId;
            Value = value;
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(RideId);
            writer.WriteInt(Value);
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RideId = reader.ReadInt();
            Value = reader.ReadInt();
            Type = reader.ReadByte();
        }
    }
}