namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextCreateErrorMessage : NetworkMessage
    {
        public override short Protocol => 6024;

        public GameContextCreateErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}