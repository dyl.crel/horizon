namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextKickMessage : NetworkMessage
    {
        public override short Protocol => 6081;

        public double TargetId { get; set; }

        public GameContextKickMessage()
        {
        }

        public GameContextKickMessage(double targetId)
        {
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TargetId = reader.ReadDouble();
        }
    }
}