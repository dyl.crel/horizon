namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextCreateRequestMessage : NetworkMessage
    {
        public override short Protocol => 250;

        public GameContextCreateRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}