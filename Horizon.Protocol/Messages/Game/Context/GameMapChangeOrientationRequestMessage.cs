namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapChangeOrientationRequestMessage : NetworkMessage
    {
        public override short Protocol => 945;

        public byte Direction { get; set; }

        public GameMapChangeOrientationRequestMessage()
        {
        }

        public GameMapChangeOrientationRequestMessage(byte direction)
        {
            Direction = direction;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Direction);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Direction = reader.ReadByte();
        }
    }
}