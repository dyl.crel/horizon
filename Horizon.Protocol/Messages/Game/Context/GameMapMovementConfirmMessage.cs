namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapMovementConfirmMessage : NetworkMessage
    {
        public override short Protocol => 952;

        public GameMapMovementConfirmMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}