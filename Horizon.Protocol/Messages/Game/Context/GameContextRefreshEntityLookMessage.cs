using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextRefreshEntityLookMessage : NetworkMessage
    {
        public override short Protocol => 5637;

        public double Id { get; set; }

        public EntityLook Look { get; set; }

        public GameContextRefreshEntityLookMessage()
        {
        }

        public GameContextRefreshEntityLookMessage(double id, EntityLook look)
        {
            Id = id;
            Look = look;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
            Look.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
            Look = new EntityLook();
            Look.Deserialize(reader);
        }
    }
}