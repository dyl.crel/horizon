namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameCautiousMapMovementRequestMessage : GameMapMovementRequestMessage
    {
        public override short Protocol => 6496;

        public GameCautiousMapMovementRequestMessage()
        {
        }

        public GameCautiousMapMovementRequestMessage(short[] keyMovements, double mapId) : base(keyMovements, mapId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}