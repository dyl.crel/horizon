namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextReadyMessage : NetworkMessage
    {
        public override short Protocol => 6071;

        public double MapId { get; set; }

        public GameContextReadyMessage()
        {
        }

        public GameContextReadyMessage(double mapId)
        {
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
        }
    }
}