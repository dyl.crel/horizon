namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapMovementCancelMessage : NetworkMessage
    {
        public override short Protocol => 953;

        public short CellId { get; set; }

        public GameMapMovementCancelMessage()
        {
        }

        public GameMapMovementCancelMessage(short cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
        }
    }
}