namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextRemoveElementWithEventMessage : GameContextRemoveElementMessage
    {
        public override short Protocol => 6412;

        public byte ElementEventId { get; set; }

        public GameContextRemoveElementWithEventMessage()
        {
        }

        public GameContextRemoveElementWithEventMessage(double id, byte elementEventId) : base(id)
        {
            ElementEventId = elementEventId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(ElementEventId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ElementEventId = reader.ReadByte();
        }
    }
}