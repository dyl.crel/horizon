namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextCreateMessage : NetworkMessage
    {
        public override short Protocol => 200;

        public byte Context { get; set; }

        public GameContextCreateMessage()
        {
        }

        public GameContextCreateMessage(byte context)
        {
            Context = context;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Context);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Context = reader.ReadByte();
        }
    }
}