namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameMapMovementMessage : NetworkMessage
    {
        public override short Protocol => 951;

        public short[] KeyMovements { get; set; }

        public short ForcedDirection { get; set; }

        public double ActorId { get; set; }

        public GameMapMovementMessage()
        {
        }

        public GameMapMovementMessage(short[] keyMovements, short forcedDirection, double actorId)
        {
            KeyMovements = keyMovements;
            ForcedDirection = forcedDirection;
            ActorId = actorId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(KeyMovements.Length);
            for (var i = 0; i < KeyMovements.Length; i++)
            {
                writer.WriteShort(KeyMovements[i]);
            }
            writer.WriteShort(ForcedDirection);
            writer.WriteDouble(ActorId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            KeyMovements = new short[reader.ReadShort()];
            for (var i = 0; i < KeyMovements.Length; i++)
            {
                KeyMovements[i] = reader.ReadShort();
            }
            ForcedDirection = reader.ReadShort();
            ActorId = reader.ReadDouble();
        }
    }
}