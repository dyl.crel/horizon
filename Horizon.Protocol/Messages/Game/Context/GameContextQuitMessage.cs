namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextQuitMessage : NetworkMessage
    {
        public override short Protocol => 255;

        public GameContextQuitMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}