using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameEntityDispositionMessage : NetworkMessage
    {
        public override short Protocol => 5693;

        public IdentifiedEntityDispositionInformations Disposition { get; set; }

        public GameEntityDispositionMessage()
        {
        }

        public GameEntityDispositionMessage(IdentifiedEntityDispositionInformations disposition)
        {
            Disposition = disposition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Disposition.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Disposition = new IdentifiedEntityDispositionInformations();
            Disposition.Deserialize(reader);
        }
    }
}