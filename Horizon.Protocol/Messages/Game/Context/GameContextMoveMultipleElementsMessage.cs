using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextMoveMultipleElementsMessage : NetworkMessage
    {
        public override short Protocol => 254;

        public EntityMovementInformations[] Movements { get; set; }

        public GameContextMoveMultipleElementsMessage()
        {
        }

        public GameContextMoveMultipleElementsMessage(EntityMovementInformations[] movements)
        {
            Movements = movements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Movements.Length);
            for (var i = 0; i < Movements.Length; i++)
            {
                Movements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Movements = new EntityMovementInformations[reader.ReadShort()];
            for (var i = 0; i < Movements.Length; i++)
            {
                Movements[i] = new EntityMovementInformations();
                Movements[i].Deserialize(reader);
            }
        }
    }
}