namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapRunningFightDetailsRequestMessage : NetworkMessage
    {
        public override short Protocol => 5750;

        public short FightId { get; set; }

        public MapRunningFightDetailsRequestMessage()
        {
        }

        public MapRunningFightDetailsRequestMessage(short fightId)
        {
            FightId = fightId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
        }
    }
}