namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapFightCountMessage : NetworkMessage
    {
        public override short Protocol => 210;

        public short FightCount { get; set; }

        public MapFightCountMessage()
        {
        }

        public MapFightCountMessage(short fightCount)
        {
            FightCount = fightCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightCount = reader.ReadVarShort();
        }
    }
}