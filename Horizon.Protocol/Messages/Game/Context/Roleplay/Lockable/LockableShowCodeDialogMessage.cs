namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableShowCodeDialogMessage : NetworkMessage
    {
        public override short Protocol => 5740;

        public bool ChangeOrUse { get; set; }

        public byte CodeSize { get; set; }

        public LockableShowCodeDialogMessage()
        {
        }

        public LockableShowCodeDialogMessage(bool changeOrUse, byte codeSize)
        {
            ChangeOrUse = changeOrUse;
            CodeSize = codeSize;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(ChangeOrUse);
            writer.WriteByte(CodeSize);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ChangeOrUse = reader.ReadBoolean();
            CodeSize = reader.ReadByte();
        }
    }
}