namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableStateUpdateStorageMessage : LockableStateUpdateAbstractMessage
    {
        public override short Protocol => 5669;

        public double MapId { get; set; }

        public int ElementId { get; set; }

        public LockableStateUpdateStorageMessage()
        {
        }

        public LockableStateUpdateStorageMessage(bool locked, double mapId, int elementId) : base(locked)
        {
            MapId = mapId;
            ElementId = elementId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(MapId);
            writer.WriteVarInt(ElementId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MapId = reader.ReadDouble();
            ElementId = reader.ReadVarInt();
        }
    }
}