namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableCodeResultMessage : NetworkMessage
    {
        public override short Protocol => 5672;

        public byte Result { get; set; }

        public LockableCodeResultMessage()
        {
        }

        public LockableCodeResultMessage(byte result)
        {
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Result = reader.ReadByte();
        }
    }
}