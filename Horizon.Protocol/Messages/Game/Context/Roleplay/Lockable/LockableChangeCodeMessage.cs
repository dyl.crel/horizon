namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableChangeCodeMessage : NetworkMessage
    {
        public override short Protocol => 5666;

        public string Code { get; set; }

        public LockableChangeCodeMessage()
        {
        }

        public LockableChangeCodeMessage(string code)
        {
            Code = code;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Code);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Code = reader.ReadUTF();
        }
    }
}