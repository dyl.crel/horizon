namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableStateUpdateHouseDoorMessage : LockableStateUpdateAbstractMessage
    {
        public override short Protocol => 5668;

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public bool SecondHand { get; set; }

        public LockableStateUpdateHouseDoorMessage()
        {
        }

        public LockableStateUpdateHouseDoorMessage(bool locked, int houseId, int instanceId, bool secondHand) : base(locked)
        {
            HouseId = houseId;
            InstanceId = instanceId;
            SecondHand = secondHand;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
            SecondHand = reader.ReadBoolean();
        }
    }
}