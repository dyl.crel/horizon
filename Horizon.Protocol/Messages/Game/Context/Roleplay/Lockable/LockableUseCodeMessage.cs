namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableUseCodeMessage : NetworkMessage
    {
        public override short Protocol => 5667;

        public string Code { get; set; }

        public LockableUseCodeMessage()
        {
        }

        public LockableUseCodeMessage(string code)
        {
            Code = code;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Code);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Code = reader.ReadUTF();
        }
    }
}