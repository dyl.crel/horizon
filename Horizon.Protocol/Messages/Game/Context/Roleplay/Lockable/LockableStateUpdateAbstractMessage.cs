namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable
{
    public class LockableStateUpdateAbstractMessage : NetworkMessage
    {
        public override short Protocol => 5671;

        public bool Locked { get; set; }

        public LockableStateUpdateAbstractMessage()
        {
        }

        public LockableStateUpdateAbstractMessage(bool locked)
        {
            Locked = locked;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Locked);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Locked = reader.ReadBoolean();
        }
    }
}