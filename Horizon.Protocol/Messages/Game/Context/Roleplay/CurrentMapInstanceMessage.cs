namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class CurrentMapInstanceMessage : CurrentMapMessage
    {
        public override short Protocol => 6738;

        public double InstantiatedMapId { get; set; }

        public CurrentMapInstanceMessage()
        {
        }

        public CurrentMapInstanceMessage(double mapId, string mapKey, double instantiatedMapId) : base(mapId, mapKey)
        {
            InstantiatedMapId = instantiatedMapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(InstantiatedMapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            InstantiatedMapId = reader.ReadDouble();
        }
    }
}