using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapRunningFightDetailsExtendedMessage : MapRunningFightDetailsMessage
    {
        public override short Protocol => 6500;

        public NamedPartyTeam[] NamedPartyTeams { get; set; }

        public MapRunningFightDetailsExtendedMessage()
        {
        }

        public MapRunningFightDetailsExtendedMessage(short fightId, GameFightFighterLightInformations[] attackers, GameFightFighterLightInformations[] defenders, NamedPartyTeam[] namedPartyTeams) : base(fightId, attackers, defenders)
        {
            NamedPartyTeams = namedPartyTeams;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(NamedPartyTeams.Length);
            for (var i = 0; i < NamedPartyTeams.Length; i++)
            {
                NamedPartyTeams[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NamedPartyTeams = new NamedPartyTeam[reader.ReadShort()];
            for (var i = 0; i < NamedPartyTeams.Length; i++)
            {
                NamedPartyTeams[i] = new NamedPartyTeam();
                NamedPartyTeams[i].Deserialize(reader);
            }
        }
    }
}