using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapComplementaryInformationsWithCoordsMessage : MapComplementaryInformationsDataMessage
    {
        public override short Protocol => 6268;

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public MapComplementaryInformationsWithCoordsMessage()
        {
        }

        public MapComplementaryInformationsWithCoordsMessage(short subAreaId, double mapId, HouseInformations[] houses, GameRolePlayActorInformations[] actors, InteractiveElement[] interactiveElements, StatedElement[] statedElements, MapObstacle[] obstacles, FightCommonInformations[] fights, bool hasAggressiveMonsters, FightStartingPositions fightStartPositions, short worldX, short worldY) : base(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions)
        {
            WorldX = worldX;
            WorldY = worldY;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
        }
    }
}