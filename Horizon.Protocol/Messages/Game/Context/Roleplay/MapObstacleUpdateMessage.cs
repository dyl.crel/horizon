using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapObstacleUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6051;

        public MapObstacle[] Obstacles { get; set; }

        public MapObstacleUpdateMessage()
        {
        }

        public MapObstacleUpdateMessage(MapObstacle[] obstacles)
        {
            Obstacles = obstacles;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Obstacles.Length);
            for (var i = 0; i < Obstacles.Length; i++)
            {
                Obstacles[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Obstacles = new MapObstacle[reader.ReadShort()];
            for (var i = 0; i < Obstacles.Length; i++)
            {
                Obstacles[i] = new MapObstacle();
                Obstacles[i].Deserialize(reader);
            }
        }
    }
}