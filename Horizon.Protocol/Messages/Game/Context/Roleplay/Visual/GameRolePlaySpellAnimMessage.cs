namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Visual
{
    public class GameRolePlaySpellAnimMessage : NetworkMessage
    {
        public override short Protocol => 6114;

        public long CasterId { get; set; }

        public short TargetCellId { get; set; }

        public short SpellId { get; set; }

        public short SpellLevel { get; set; }

        public GameRolePlaySpellAnimMessage()
        {
        }

        public GameRolePlaySpellAnimMessage(long casterId, short targetCellId, short spellId, short spellLevel)
        {
            CasterId = casterId;
            TargetCellId = targetCellId;
            SpellId = spellId;
            SpellLevel = spellLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(CasterId);
            writer.WriteVarShort(TargetCellId);
            writer.WriteVarShort(SpellId);
            writer.WriteShort(SpellLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CasterId = reader.ReadVarLong();
            TargetCellId = reader.ReadVarShort();
            SpellId = reader.ReadVarShort();
            SpellLevel = reader.ReadShort();
        }
    }
}