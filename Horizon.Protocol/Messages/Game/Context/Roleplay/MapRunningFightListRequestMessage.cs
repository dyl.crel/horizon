namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapRunningFightListRequestMessage : NetworkMessage
    {
        public override short Protocol => 5742;

        public MapRunningFightListRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}