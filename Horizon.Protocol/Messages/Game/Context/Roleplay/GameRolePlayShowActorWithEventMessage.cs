using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class GameRolePlayShowActorWithEventMessage : GameRolePlayShowActorMessage
    {
        public override short Protocol => 6407;

        public byte ActorEventId { get; set; }

        public GameRolePlayShowActorWithEventMessage()
        {
        }

        public GameRolePlayShowActorWithEventMessage(GameRolePlayActorInformations informations, byte actorEventId) : base(informations)
        {
            ActorEventId = actorEventId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(ActorEventId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ActorEventId = reader.ReadByte();
        }
    }
}