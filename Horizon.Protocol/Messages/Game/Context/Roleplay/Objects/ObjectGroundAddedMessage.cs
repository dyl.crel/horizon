namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Objects
{
    public class ObjectGroundAddedMessage : NetworkMessage
    {
        public override short Protocol => 3017;

        public short CellId { get; set; }

        public short ObjectGID { get; set; }

        public ObjectGroundAddedMessage()
        {
        }

        public ObjectGroundAddedMessage(short cellId, short objectGID)
        {
            CellId = cellId;
            ObjectGID = objectGID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteVarShort(ObjectGID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
            ObjectGID = reader.ReadVarShort();
        }
    }
}