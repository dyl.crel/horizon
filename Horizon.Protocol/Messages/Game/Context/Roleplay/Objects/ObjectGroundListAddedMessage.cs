namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Objects
{
    public class ObjectGroundListAddedMessage : NetworkMessage
    {
        public override short Protocol => 5925;

        public short[] Cells { get; set; }

        public short[] ReferenceIds { get; set; }

        public ObjectGroundListAddedMessage()
        {
        }

        public ObjectGroundListAddedMessage(short[] cells, short[] referenceIds)
        {
            Cells = cells;
            ReferenceIds = referenceIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Cells.Length);
            for (var i = 0; i < Cells.Length; i++)
            {
                writer.WriteVarShort(Cells[i]);
            }
            writer.WriteShort(ReferenceIds.Length);
            for (var i = 0; i < ReferenceIds.Length; i++)
            {
                writer.WriteVarShort(ReferenceIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Cells = new short[reader.ReadShort()];
            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i] = reader.ReadVarShort();
            }
            ReferenceIds = new short[reader.ReadShort()];
            for (var i = 0; i < ReferenceIds.Length; i++)
            {
                ReferenceIds[i] = reader.ReadVarShort();
            }
        }
    }
}