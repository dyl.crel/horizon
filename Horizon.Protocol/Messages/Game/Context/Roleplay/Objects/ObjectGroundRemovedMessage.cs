namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Objects
{
    public class ObjectGroundRemovedMessage : NetworkMessage
    {
        public override short Protocol => 3014;

        public short Cell { get; set; }

        public ObjectGroundRemovedMessage()
        {
        }

        public ObjectGroundRemovedMessage(short cell)
        {
            Cell = cell;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Cell);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Cell = reader.ReadVarShort();
        }
    }
}