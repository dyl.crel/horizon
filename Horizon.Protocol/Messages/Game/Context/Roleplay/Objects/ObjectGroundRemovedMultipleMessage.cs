namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Objects
{
    public class ObjectGroundRemovedMultipleMessage : NetworkMessage
    {
        public override short Protocol => 5944;

        public short[] Cells { get; set; }

        public ObjectGroundRemovedMultipleMessage()
        {
        }

        public ObjectGroundRemovedMultipleMessage(short[] cells)
        {
            Cells = cells;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Cells.Length);
            for (var i = 0; i < Cells.Length; i++)
            {
                writer.WriteVarShort(Cells[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Cells = new short[reader.ReadShort()];
            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i] = reader.ReadVarShort();
            }
        }
    }
}