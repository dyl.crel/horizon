using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class GameRolePlayShowMultipleActorsMessage : NetworkMessage
    {
        public override short Protocol => 6712;

        public GameRolePlayActorInformations[] InformationsList { get; set; }

        public GameRolePlayShowMultipleActorsMessage()
        {
        }

        public GameRolePlayShowMultipleActorsMessage(GameRolePlayActorInformations[] informationsList)
        {
            InformationsList = informationsList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(InformationsList.Length);
            for (var i = 0; i < InformationsList.Length; i++)
            {
                writer.WriteShort(InformationsList[i].Protocol);
                InformationsList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InformationsList = new GameRolePlayActorInformations[reader.ReadShort()];
            for (var i = 0; i < InformationsList.Length; i++)
            {
                InformationsList[i] = ProtocolTypesManager.Instance<GameRolePlayActorInformations>(reader.ReadShort());
                InformationsList[i].Deserialize(reader);
            }
        }
    }
}