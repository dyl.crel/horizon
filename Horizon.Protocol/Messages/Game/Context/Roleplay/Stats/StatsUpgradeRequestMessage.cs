namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Stats
{
    public class StatsUpgradeRequestMessage : NetworkMessage
    {
        public override short Protocol => 5610;

        public bool UseAdditionnal { get; set; }

        public byte StatId { get; set; }

        public short BoostPoint { get; set; }

        public StatsUpgradeRequestMessage()
        {
        }

        public StatsUpgradeRequestMessage(bool useAdditionnal, byte statId, short boostPoint)
        {
            UseAdditionnal = useAdditionnal;
            StatId = statId;
            BoostPoint = boostPoint;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(UseAdditionnal);
            writer.WriteByte(StatId);
            writer.WriteVarShort(BoostPoint);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            UseAdditionnal = reader.ReadBoolean();
            StatId = reader.ReadByte();
            BoostPoint = reader.ReadVarShort();
        }
    }
}