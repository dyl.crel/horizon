namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Stats
{
    public class StatsUpgradeResultMessage : NetworkMessage
    {
        public override short Protocol => 5609;

        public byte Result { get; set; }

        public short NbCharacBoost { get; set; }

        public StatsUpgradeResultMessage()
        {
        }

        public StatsUpgradeResultMessage(byte result, short nbCharacBoost)
        {
            Result = result;
            NbCharacBoost = nbCharacBoost;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Result);
            writer.WriteVarShort(NbCharacBoost);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Result = reader.ReadByte();
            NbCharacBoost = reader.ReadVarShort();
        }
    }
}