namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapInformationsRequestMessage : NetworkMessage
    {
        public override short Protocol => 225;

        public double MapId { get; set; }

        public MapInformationsRequestMessage()
        {
        }

        public MapInformationsRequestMessage(double mapId)
        {
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
        }
    }
}