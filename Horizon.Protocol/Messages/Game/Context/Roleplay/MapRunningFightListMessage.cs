using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapRunningFightListMessage : NetworkMessage
    {
        public override short Protocol => 5743;

        public FightExternalInformations[] Fights { get; set; }

        public MapRunningFightListMessage()
        {
        }

        public MapRunningFightListMessage(FightExternalInformations[] fights)
        {
            Fights = fights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Fights.Length);
            for (var i = 0; i < Fights.Length; i++)
            {
                Fights[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Fights = new FightExternalInformations[reader.ReadShort()];
            for (var i = 0; i < Fights.Length; i++)
            {
                Fights[i] = new FightExternalInformations();
                Fights[i].Deserialize(reader);
            }
        }
    }
}