namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Delay
{
    public class GameRolePlayDelayedActionFinishedMessage : NetworkMessage
    {
        public override short Protocol => 6150;

        public double DelayedCharacterId { get; set; }

        public byte DelayTypeId { get; set; }

        public GameRolePlayDelayedActionFinishedMessage()
        {
        }

        public GameRolePlayDelayedActionFinishedMessage(double delayedCharacterId, byte delayTypeId)
        {
            DelayedCharacterId = delayedCharacterId;
            DelayTypeId = delayTypeId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DelayedCharacterId);
            writer.WriteByte(DelayTypeId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DelayedCharacterId = reader.ReadDouble();
            DelayTypeId = reader.ReadByte();
        }
    }
}