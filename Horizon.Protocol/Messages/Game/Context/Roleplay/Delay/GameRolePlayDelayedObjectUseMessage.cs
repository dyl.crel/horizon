namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Delay
{
    public class GameRolePlayDelayedObjectUseMessage : GameRolePlayDelayedActionMessage
    {
        public override short Protocol => 6425;

        public short ObjectGID { get; set; }

        public GameRolePlayDelayedObjectUseMessage()
        {
        }

        public GameRolePlayDelayedObjectUseMessage(double delayedCharacterId, byte delayTypeId, double delayEndTime, short objectGID) : base(delayedCharacterId, delayTypeId, delayEndTime)
        {
            ObjectGID = objectGID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarShort();
        }
    }
}