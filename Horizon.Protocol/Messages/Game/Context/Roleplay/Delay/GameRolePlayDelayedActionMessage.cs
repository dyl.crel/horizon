namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Delay
{
    public class GameRolePlayDelayedActionMessage : NetworkMessage
    {
        public override short Protocol => 6153;

        public double DelayedCharacterId { get; set; }

        public byte DelayTypeId { get; set; }

        public double DelayEndTime { get; set; }

        public GameRolePlayDelayedActionMessage()
        {
        }

        public GameRolePlayDelayedActionMessage(double delayedCharacterId, byte delayTypeId, double delayEndTime)
        {
            DelayedCharacterId = delayedCharacterId;
            DelayTypeId = delayTypeId;
            DelayEndTime = delayEndTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DelayedCharacterId);
            writer.WriteByte(DelayTypeId);
            writer.WriteDouble(DelayEndTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DelayedCharacterId = reader.ReadDouble();
            DelayTypeId = reader.ReadByte();
            DelayEndTime = reader.ReadDouble();
        }
    }
}