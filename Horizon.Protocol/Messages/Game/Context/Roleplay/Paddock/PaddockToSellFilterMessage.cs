namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Paddock
{
    public class PaddockToSellFilterMessage : NetworkMessage
    {
        public override short Protocol => 6161;

        public int AreaId { get; set; }

        public byte AtLeastNbMount { get; set; }

        public byte AtLeastNbMachine { get; set; }

        public long MaxPrice { get; set; }

        public byte OrderBy { get; set; }

        public PaddockToSellFilterMessage()
        {
        }

        public PaddockToSellFilterMessage(int areaId, byte atLeastNbMount, byte atLeastNbMachine, long maxPrice, byte orderBy)
        {
            AreaId = areaId;
            AtLeastNbMount = atLeastNbMount;
            AtLeastNbMachine = atLeastNbMachine;
            MaxPrice = maxPrice;
            OrderBy = orderBy;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AreaId);
            writer.WriteByte(AtLeastNbMount);
            writer.WriteByte(AtLeastNbMachine);
            writer.WriteVarLong(MaxPrice);
            writer.WriteByte(OrderBy);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AreaId = reader.ReadInt();
            AtLeastNbMount = reader.ReadByte();
            AtLeastNbMachine = reader.ReadByte();
            MaxPrice = reader.ReadVarLong();
            OrderBy = reader.ReadByte();
        }
    }
}