namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Paddock
{
    public class PaddockSellBuyDialogMessage : NetworkMessage
    {
        public override short Protocol => 6018;

        public bool Bsell { get; set; }

        public int OwnerId { get; set; }

        public long Price { get; set; }

        public PaddockSellBuyDialogMessage()
        {
        }

        public PaddockSellBuyDialogMessage(bool bsell, int ownerId, long price)
        {
            Bsell = bsell;
            OwnerId = ownerId;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Bsell);
            writer.WriteVarInt(OwnerId);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Bsell = reader.ReadBoolean();
            OwnerId = reader.ReadVarInt();
            Price = reader.ReadVarLong();
        }
    }
}