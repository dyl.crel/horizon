namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Paddock
{
    public class GameDataPlayFarmObjectAnimationMessage : NetworkMessage
    {
        public override short Protocol => 6026;

        public short[] CellId { get; set; }

        public GameDataPlayFarmObjectAnimationMessage()
        {
        }

        public GameDataPlayFarmObjectAnimationMessage(short[] cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CellId.Length);
            for (var i = 0; i < CellId.Length; i++)
            {
                writer.WriteVarShort(CellId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = new short[reader.ReadShort()];
            for (var i = 0; i < CellId.Length; i++)
            {
                CellId[i] = reader.ReadVarShort();
            }
        }
    }
}