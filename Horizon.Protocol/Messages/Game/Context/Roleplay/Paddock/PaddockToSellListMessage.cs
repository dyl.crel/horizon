using Horizon.Protocol.Types.Game.Paddock;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Paddock
{
    public class PaddockToSellListMessage : NetworkMessage
    {
        public override short Protocol => 6138;

        public short PageIndex { get; set; }

        public short TotalPage { get; set; }

        public PaddockInformationsForSell[] PaddockList { get; set; }

        public PaddockToSellListMessage()
        {
        }

        public PaddockToSellListMessage(short pageIndex, short totalPage, PaddockInformationsForSell[] paddockList)
        {
            PageIndex = pageIndex;
            TotalPage = totalPage;
            PaddockList = paddockList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(PageIndex);
            writer.WriteVarShort(TotalPage);
            writer.WriteShort(PaddockList.Length);
            for (var i = 0; i < PaddockList.Length; i++)
            {
                PaddockList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PageIndex = reader.ReadVarShort();
            TotalPage = reader.ReadVarShort();
            PaddockList = new PaddockInformationsForSell[reader.ReadShort()];
            for (var i = 0; i < PaddockList.Length; i++)
            {
                PaddockList[i] = new PaddockInformationsForSell();
                PaddockList[i].Deserialize(reader);
            }
        }
    }
}