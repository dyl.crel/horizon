using Horizon.Protocol.Types.Game.Paddock;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Paddock
{
    public class PaddockPropertiesMessage : NetworkMessage
    {
        public override short Protocol => 5824;

        public PaddockInstancesInformations Properties { get; set; }

        public PaddockPropertiesMessage()
        {
        }

        public PaddockPropertiesMessage(PaddockInstancesInformations properties)
        {
            Properties = properties;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Properties.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Properties = new PaddockInstancesInformations();
            Properties.Deserialize(reader);
        }
    }
}