namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Paddock
{
    public class PaddockToSellListRequestMessage : NetworkMessage
    {
        public override short Protocol => 6141;

        public short PageIndex { get; set; }

        public PaddockToSellListRequestMessage()
        {
        }

        public PaddockToSellListRequestMessage(short pageIndex)
        {
            PageIndex = pageIndex;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(PageIndex);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PageIndex = reader.ReadVarShort();
        }
    }
}