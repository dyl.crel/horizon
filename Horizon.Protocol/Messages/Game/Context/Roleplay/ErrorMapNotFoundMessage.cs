namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class ErrorMapNotFoundMessage : NetworkMessage
    {
        public override short Protocol => 6197;

        public double MapId { get; set; }

        public ErrorMapNotFoundMessage()
        {
        }

        public ErrorMapNotFoundMessage(double mapId)
        {
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
        }
    }
}