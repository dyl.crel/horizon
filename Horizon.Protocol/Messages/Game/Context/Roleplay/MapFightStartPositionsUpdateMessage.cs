using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapFightStartPositionsUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6716;

        public double MapId { get; set; }

        public FightStartingPositions FightStartPositions { get; set; }

        public MapFightStartPositionsUpdateMessage()
        {
        }

        public MapFightStartPositionsUpdateMessage(double mapId, FightStartingPositions fightStartPositions)
        {
            MapId = mapId;
            FightStartPositions = fightStartPositions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
            FightStartPositions.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
            FightStartPositions = new FightStartingPositions();
            FightStartPositions.Deserialize(reader);
        }
    }
}