namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class StopToListenRunningFightRequestMessage : NetworkMessage
    {
        public override short Protocol => 6124;

        public StopToListenRunningFightRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}