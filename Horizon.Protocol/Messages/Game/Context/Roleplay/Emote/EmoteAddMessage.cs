namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmoteAddMessage : NetworkMessage
    {
        public override short Protocol => 5644;

        public byte EmoteId { get; set; }

        public EmoteAddMessage()
        {
        }

        public EmoteAddMessage(byte emoteId)
        {
            EmoteId = emoteId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(EmoteId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EmoteId = reader.ReadByte();
        }
    }
}