namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmotePlayAbstractMessage : NetworkMessage
    {
        public override short Protocol => 5690;

        public byte EmoteId { get; set; }

        public double EmoteStartTime { get; set; }

        public EmotePlayAbstractMessage()
        {
        }

        public EmotePlayAbstractMessage(byte emoteId, double emoteStartTime)
        {
            EmoteId = emoteId;
            EmoteStartTime = emoteStartTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(EmoteId);
            writer.WriteDouble(EmoteStartTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EmoteId = reader.ReadByte();
            EmoteStartTime = reader.ReadDouble();
        }
    }
}