namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmotePlayRequestMessage : NetworkMessage
    {
        public override short Protocol => 5685;

        public byte EmoteId { get; set; }

        public EmotePlayRequestMessage()
        {
        }

        public EmotePlayRequestMessage(byte emoteId)
        {
            EmoteId = emoteId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(EmoteId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EmoteId = reader.ReadByte();
        }
    }
}