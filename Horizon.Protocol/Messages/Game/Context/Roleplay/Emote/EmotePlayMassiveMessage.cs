namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmotePlayMassiveMessage : EmotePlayAbstractMessage
    {
        public override short Protocol => 5691;

        public double[] ActorIds { get; set; }

        public EmotePlayMassiveMessage()
        {
        }

        public EmotePlayMassiveMessage(byte emoteId, double emoteStartTime, double[] actorIds) : base(emoteId, emoteStartTime)
        {
            ActorIds = actorIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(ActorIds.Length);
            for (var i = 0; i < ActorIds.Length; i++)
            {
                writer.WriteDouble(ActorIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ActorIds = new double[reader.ReadShort()];
            for (var i = 0; i < ActorIds.Length; i++)
            {
                ActorIds[i] = reader.ReadDouble();
            }
        }
    }
}