namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmotePlayMessage : EmotePlayAbstractMessage
    {
        public override short Protocol => 5683;

        public double ActorId { get; set; }

        public int AccountId { get; set; }

        public EmotePlayMessage()
        {
        }

        public EmotePlayMessage(byte emoteId, double emoteStartTime, double actorId, int accountId) : base(emoteId, emoteStartTime)
        {
            ActorId = actorId;
            AccountId = accountId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(ActorId);
            writer.WriteInt(AccountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ActorId = reader.ReadDouble();
            AccountId = reader.ReadInt();
        }
    }
}