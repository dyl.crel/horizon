namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmotePlayErrorMessage : NetworkMessage
    {
        public override short Protocol => 5688;

        public byte EmoteId { get; set; }

        public EmotePlayErrorMessage()
        {
        }

        public EmotePlayErrorMessage(byte emoteId)
        {
            EmoteId = emoteId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(EmoteId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EmoteId = reader.ReadByte();
        }
    }
}