namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmoteRemoveMessage : NetworkMessage
    {
        public override short Protocol => 5687;

        public byte EmoteId { get; set; }

        public EmoteRemoveMessage()
        {
        }

        public EmoteRemoveMessage(byte emoteId)
        {
            EmoteId = emoteId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(EmoteId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EmoteId = reader.ReadByte();
        }
    }
}