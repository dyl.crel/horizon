namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Emote
{
    public class EmoteListMessage : NetworkMessage
    {
        public override short Protocol => 5689;

        public byte[] EmoteIds { get; set; }

        public EmoteListMessage()
        {
        }

        public EmoteListMessage(byte[] emoteIds)
        {
            EmoteIds = emoteIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(EmoteIds.Length);
            for (var i = 0; i < EmoteIds.Length; i++)
            {
                writer.WriteByte(EmoteIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EmoteIds = new byte[reader.ReadShort()];
            for (var i = 0; i < EmoteIds.Length; i++)
            {
                EmoteIds[i] = reader.ReadByte();
            }
        }
    }
}