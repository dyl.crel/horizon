namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class NpcDialogReplyMessage : NetworkMessage
    {
        public override short Protocol => 5616;

        public int ReplyId { get; set; }

        public NpcDialogReplyMessage()
        {
        }

        public NpcDialogReplyMessage(int replyId)
        {
            ReplyId = replyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ReplyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ReplyId = reader.ReadVarInt();
        }
    }
}