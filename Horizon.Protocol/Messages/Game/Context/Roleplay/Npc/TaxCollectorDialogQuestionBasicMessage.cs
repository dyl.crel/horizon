using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class TaxCollectorDialogQuestionBasicMessage : NetworkMessage
    {
        public override short Protocol => 5619;

        public BasicGuildInformations GuildInfo { get; set; }

        public TaxCollectorDialogQuestionBasicMessage()
        {
        }

        public TaxCollectorDialogQuestionBasicMessage(BasicGuildInformations guildInfo)
        {
            GuildInfo = guildInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            GuildInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildInfo = new BasicGuildInformations();
            GuildInfo.Deserialize(reader);
        }
    }
}