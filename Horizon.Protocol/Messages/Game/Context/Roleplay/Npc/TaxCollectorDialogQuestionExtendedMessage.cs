using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class TaxCollectorDialogQuestionExtendedMessage : TaxCollectorDialogQuestionBasicMessage
    {
        public override short Protocol => 5615;

        public short MaxPods { get; set; }

        public short Prospecting { get; set; }

        public short Wisdom { get; set; }

        public byte TaxCollectorsCount { get; set; }

        public int TaxCollectorAttack { get; set; }

        public long Kamas { get; set; }

        public long Experience { get; set; }

        public int Pods { get; set; }

        public long ItemsValue { get; set; }

        public TaxCollectorDialogQuestionExtendedMessage()
        {
        }

        public TaxCollectorDialogQuestionExtendedMessage(BasicGuildInformations guildInfo, short maxPods, short prospecting, short wisdom, byte taxCollectorsCount, int taxCollectorAttack, long kamas, long experience, int pods, long itemsValue) : base(guildInfo)
        {
            MaxPods = maxPods;
            Prospecting = prospecting;
            Wisdom = wisdom;
            TaxCollectorsCount = taxCollectorsCount;
            TaxCollectorAttack = taxCollectorAttack;
            Kamas = kamas;
            Experience = experience;
            Pods = pods;
            ItemsValue = itemsValue;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(MaxPods);
            writer.WriteVarShort(Prospecting);
            writer.WriteVarShort(Wisdom);
            writer.WriteByte(TaxCollectorsCount);
            writer.WriteInt(TaxCollectorAttack);
            writer.WriteVarLong(Kamas);
            writer.WriteVarLong(Experience);
            writer.WriteVarInt(Pods);
            writer.WriteVarLong(ItemsValue);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MaxPods = reader.ReadVarShort();
            Prospecting = reader.ReadVarShort();
            Wisdom = reader.ReadVarShort();
            TaxCollectorsCount = reader.ReadByte();
            TaxCollectorAttack = reader.ReadInt();
            Kamas = reader.ReadVarLong();
            Experience = reader.ReadVarLong();
            Pods = reader.ReadVarInt();
            ItemsValue = reader.ReadVarLong();
        }
    }
}