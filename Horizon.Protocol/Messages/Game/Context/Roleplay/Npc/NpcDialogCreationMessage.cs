namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class NpcDialogCreationMessage : NetworkMessage
    {
        public override short Protocol => 5618;

        public double MapId { get; set; }

        public int NpcId { get; set; }

        public NpcDialogCreationMessage()
        {
        }

        public NpcDialogCreationMessage(double mapId, int npcId)
        {
            MapId = mapId;
            NpcId = npcId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteInt(NpcId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
            NpcId = reader.ReadInt();
        }
    }
}