namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class EntityTalkMessage : NetworkMessage
    {
        public override short Protocol => 6110;

        public double EntityId { get; set; }

        public short TextId { get; set; }

        public string[] Parameters { get; set; }

        public EntityTalkMessage()
        {
        }

        public EntityTalkMessage(double entityId, short textId, string[] parameters)
        {
            EntityId = entityId;
            TextId = textId;
            Parameters = parameters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(EntityId);
            writer.WriteVarShort(TextId);
            writer.WriteShort(Parameters.Length);
            for (var i = 0; i < Parameters.Length; i++)
            {
                writer.WriteUTF(Parameters[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EntityId = reader.ReadDouble();
            TextId = reader.ReadVarShort();
            Parameters = new string[reader.ReadShort()];
            for (var i = 0; i < Parameters.Length; i++)
            {
                Parameters[i] = reader.ReadUTF();
            }
        }
    }
}