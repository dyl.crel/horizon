using Horizon.Protocol.Types.Game.Context.Roleplay.Quest;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class MapNpcsQuestStatusUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5642;

        public double MapId { get; set; }

        public int[] NpcsIdsWithQuest { get; set; }

        public GameRolePlayNpcQuestFlag[] QuestFlags { get; set; }

        public int[] NpcsIdsWithoutQuest { get; set; }

        public MapNpcsQuestStatusUpdateMessage()
        {
        }

        public MapNpcsQuestStatusUpdateMessage(double mapId, int[] npcsIdsWithQuest, GameRolePlayNpcQuestFlag[] questFlags, int[] npcsIdsWithoutQuest)
        {
            MapId = mapId;
            NpcsIdsWithQuest = npcsIdsWithQuest;
            QuestFlags = questFlags;
            NpcsIdsWithoutQuest = npcsIdsWithoutQuest;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteShort(NpcsIdsWithQuest.Length);
            for (var i = 0; i < NpcsIdsWithQuest.Length; i++)
            {
                writer.WriteInt(NpcsIdsWithQuest[i]);
            }
            writer.WriteShort(QuestFlags.Length);
            for (var i = 0; i < QuestFlags.Length; i++)
            {
                QuestFlags[i].Serialize(writer);
            }
            writer.WriteShort(NpcsIdsWithoutQuest.Length);
            for (var i = 0; i < NpcsIdsWithoutQuest.Length; i++)
            {
                writer.WriteInt(NpcsIdsWithoutQuest[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
            NpcsIdsWithQuest = new int[reader.ReadShort()];
            for (var i = 0; i < NpcsIdsWithQuest.Length; i++)
            {
                NpcsIdsWithQuest[i] = reader.ReadInt();
            }
            QuestFlags = new GameRolePlayNpcQuestFlag[reader.ReadShort()];
            for (var i = 0; i < QuestFlags.Length; i++)
            {
                QuestFlags[i] = new GameRolePlayNpcQuestFlag();
                QuestFlags[i].Deserialize(reader);
            }
            NpcsIdsWithoutQuest = new int[reader.ReadShort()];
            for (var i = 0; i < NpcsIdsWithoutQuest.Length; i++)
            {
                NpcsIdsWithoutQuest[i] = reader.ReadInt();
            }
        }
    }
}