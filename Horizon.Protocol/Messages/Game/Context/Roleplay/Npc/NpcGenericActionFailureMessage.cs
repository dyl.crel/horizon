namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class NpcGenericActionFailureMessage : NetworkMessage
    {
        public override short Protocol => 5900;

        public NpcGenericActionFailureMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}