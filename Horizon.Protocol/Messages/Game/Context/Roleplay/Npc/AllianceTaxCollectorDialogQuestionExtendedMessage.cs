using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class AllianceTaxCollectorDialogQuestionExtendedMessage : TaxCollectorDialogQuestionExtendedMessage
    {
        public override short Protocol => 6445;

        public BasicNamedAllianceInformations Alliance { get; set; }

        public AllianceTaxCollectorDialogQuestionExtendedMessage()
        {
        }

        public AllianceTaxCollectorDialogQuestionExtendedMessage(BasicGuildInformations guildInfo, short maxPods, short prospecting, short wisdom, byte taxCollectorsCount, int taxCollectorAttack, long kamas, long experience, int pods, long itemsValue, BasicNamedAllianceInformations alliance) : base(guildInfo, maxPods, prospecting, wisdom, taxCollectorsCount, taxCollectorAttack, kamas, experience, pods, itemsValue)
        {
            Alliance = alliance;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Alliance.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Alliance = new BasicNamedAllianceInformations();
            Alliance.Deserialize(reader);
        }
    }
}