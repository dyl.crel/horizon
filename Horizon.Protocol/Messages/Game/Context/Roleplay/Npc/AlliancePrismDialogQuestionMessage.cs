namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class AlliancePrismDialogQuestionMessage : NetworkMessage
    {
        public override short Protocol => 6448;

        public AlliancePrismDialogQuestionMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}