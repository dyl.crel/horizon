namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class NpcGenericActionRequestMessage : NetworkMessage
    {
        public override short Protocol => 5898;

        public int NpcId { get; set; }

        public byte NpcActionId { get; set; }

        public double NpcMapId { get; set; }

        public NpcGenericActionRequestMessage()
        {
        }

        public NpcGenericActionRequestMessage(int npcId, byte npcActionId, double npcMapId)
        {
            NpcId = npcId;
            NpcActionId = npcActionId;
            NpcMapId = npcMapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(NpcId);
            writer.WriteByte(NpcActionId);
            writer.WriteDouble(NpcMapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NpcId = reader.ReadInt();
            NpcActionId = reader.ReadByte();
            NpcMapId = reader.ReadDouble();
        }
    }
}