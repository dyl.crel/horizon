namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class NpcDialogQuestionMessage : NetworkMessage
    {
        public override short Protocol => 5617;

        public int MessageId { get; set; }

        public string[] DialogParams { get; set; }

        public int[] VisibleReplies { get; set; }

        public NpcDialogQuestionMessage()
        {
        }

        public NpcDialogQuestionMessage(int messageId, string[] dialogParams, int[] visibleReplies)
        {
            MessageId = messageId;
            DialogParams = dialogParams;
            VisibleReplies = visibleReplies;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(MessageId);
            writer.WriteShort(DialogParams.Length);
            for (var i = 0; i < DialogParams.Length; i++)
            {
                writer.WriteUTF(DialogParams[i]);
            }
            writer.WriteShort(VisibleReplies.Length);
            for (var i = 0; i < VisibleReplies.Length; i++)
            {
                writer.WriteVarInt(VisibleReplies[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MessageId = reader.ReadVarInt();
            DialogParams = new string[reader.ReadShort()];
            for (var i = 0; i < DialogParams.Length; i++)
            {
                DialogParams[i] = reader.ReadUTF();
            }
            VisibleReplies = new int[reader.ReadShort()];
            for (var i = 0; i < VisibleReplies.Length; i++)
            {
                VisibleReplies[i] = reader.ReadVarInt();
            }
        }
    }
}