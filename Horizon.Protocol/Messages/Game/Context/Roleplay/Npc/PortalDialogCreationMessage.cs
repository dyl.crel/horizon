namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Npc
{
    public class PortalDialogCreationMessage : NpcDialogCreationMessage
    {
        public override short Protocol => 6737;

        public int Type { get; set; }

        public PortalDialogCreationMessage()
        {
        }

        public PortalDialogCreationMessage(double mapId, int npcId, int type) : base(mapId, npcId)
        {
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Type = reader.ReadInt();
        }
    }
}