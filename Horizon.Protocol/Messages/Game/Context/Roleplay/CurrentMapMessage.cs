namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class CurrentMapMessage : NetworkMessage
    {
        public override short Protocol => 220;

        public double MapId { get; set; }

        public string MapKey { get; set; }

        public CurrentMapMessage()
        {
        }

        public CurrentMapMessage(double mapId, string mapKey)
        {
            MapId = mapId;
            MapKey = mapKey;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteUTF(MapKey);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
            MapKey = reader.ReadUTF();
        }
    }
}