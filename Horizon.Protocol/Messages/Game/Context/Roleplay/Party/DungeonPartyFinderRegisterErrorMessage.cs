namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderRegisterErrorMessage : NetworkMessage
    {
        public override short Protocol => 6243;

        public DungeonPartyFinderRegisterErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}