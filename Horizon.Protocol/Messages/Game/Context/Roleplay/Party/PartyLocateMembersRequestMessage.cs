namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyLocateMembersRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 5587;

        public PartyLocateMembersRequestMessage()
        {
        }

        public PartyLocateMembersRequestMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}