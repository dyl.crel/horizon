using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationDetailsMessage : AbstractPartyMessage
    {
        public override short Protocol => 6263;

        public byte PartyType { get; set; }

        public string PartyName { get; set; }

        public long FromId { get; set; }

        public string FromName { get; set; }

        public long LeaderId { get; set; }

        public PartyInvitationMemberInformations[] Members { get; set; }

        public PartyGuestInformations[] Guests { get; set; }

        public PartyInvitationDetailsMessage()
        {
        }

        public PartyInvitationDetailsMessage(int partyId, byte partyType, string partyName, long fromId, string fromName, long leaderId, PartyInvitationMemberInformations[] members, PartyGuestInformations[] guests) : base(partyId)
        {
            PartyType = partyType;
            PartyName = partyName;
            FromId = fromId;
            FromName = fromName;
            LeaderId = leaderId;
            Members = members;
            Guests = guests;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PartyType);
            writer.WriteUTF(PartyName);
            writer.WriteVarLong(FromId);
            writer.WriteUTF(FromName);
            writer.WriteVarLong(LeaderId);
            writer.WriteShort(Members.Length);
            for (var i = 0; i < Members.Length; i++)
            {
                writer.WriteShort(Members[i].Protocol);
                Members[i].Serialize(writer);
            }
            writer.WriteShort(Guests.Length);
            for (var i = 0; i < Guests.Length; i++)
            {
                Guests[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PartyType = reader.ReadByte();
            PartyName = reader.ReadUTF();
            FromId = reader.ReadVarLong();
            FromName = reader.ReadUTF();
            LeaderId = reader.ReadVarLong();
            Members = new PartyInvitationMemberInformations[reader.ReadShort()];
            for (var i = 0; i < Members.Length; i++)
            {
                Members[i] = ProtocolTypesManager.Instance<PartyInvitationMemberInformations>(reader.ReadShort());
                Members[i].Deserialize(reader);
            }
            Guests = new PartyGuestInformations[reader.ReadShort()];
            for (var i = 0; i < Guests.Length; i++)
            {
                Guests[i] = new PartyGuestInformations();
                Guests[i].Deserialize(reader);
            }
        }
    }
}