namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyDeletedMessage : AbstractPartyMessage
    {
        public override short Protocol => 6261;

        public PartyDeletedMessage()
        {
        }

        public PartyDeletedMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}