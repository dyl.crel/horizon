namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationCancelledForGuestMessage : AbstractPartyMessage
    {
        public override short Protocol => 6256;

        public long CancelerId { get; set; }

        public PartyInvitationCancelledForGuestMessage()
        {
        }

        public PartyInvitationCancelledForGuestMessage(int partyId, long cancelerId) : base(partyId)
        {
            CancelerId = cancelerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(CancelerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CancelerId = reader.ReadVarLong();
        }
    }
}