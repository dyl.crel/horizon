namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyCannotJoinErrorMessage : AbstractPartyMessage
    {
        public override short Protocol => 5583;

        public byte Reason { get; set; }

        public PartyCannotJoinErrorMessage()
        {
        }

        public PartyCannotJoinErrorMessage(int partyId, byte reason) : base(partyId)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Reason = reader.ReadByte();
        }
    }
}