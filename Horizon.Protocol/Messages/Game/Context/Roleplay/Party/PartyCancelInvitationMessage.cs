namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyCancelInvitationMessage : AbstractPartyMessage
    {
        public override short Protocol => 6254;

        public long GuestId { get; set; }

        public PartyCancelInvitationMessage()
        {
        }

        public PartyCancelInvitationMessage(int partyId, long guestId) : base(partyId)
        {
            GuestId = guestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(GuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuestId = reader.ReadVarLong();
        }
    }
}