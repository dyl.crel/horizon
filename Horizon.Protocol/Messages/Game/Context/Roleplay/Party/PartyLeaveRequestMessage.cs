namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyLeaveRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 5593;

        public PartyLeaveRequestMessage()
        {
        }

        public PartyLeaveRequestMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}