using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyLocateMembersMessage : AbstractPartyMessage
    {
        public override short Protocol => 5595;

        public PartyMemberGeoPosition[] Geopositions { get; set; }

        public PartyLocateMembersMessage()
        {
        }

        public PartyLocateMembersMessage(int partyId, PartyMemberGeoPosition[] geopositions) : base(partyId)
        {
            Geopositions = geopositions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Geopositions.Length);
            for (var i = 0; i < Geopositions.Length; i++)
            {
                Geopositions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Geopositions = new PartyMemberGeoPosition[reader.ReadShort()];
            for (var i = 0; i < Geopositions.Length; i++)
            {
                Geopositions[i] = new PartyMemberGeoPosition();
                Geopositions[i].Deserialize(reader);
            }
        }
    }
}