namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationRequestMessage : NetworkMessage
    {
        public override short Protocol => 5585;

        public string Name { get; set; }

        public PartyInvitationRequestMessage()
        {
        }

        public PartyInvitationRequestMessage(string name)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
        }
    }
}