namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyLeaderUpdateMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 5578;

        public long PartyLeaderId { get; set; }

        public PartyLeaderUpdateMessage()
        {
        }

        public PartyLeaderUpdateMessage(int partyId, long partyLeaderId) : base(partyId)
        {
            PartyLeaderId = partyLeaderId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PartyLeaderId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PartyLeaderId = reader.ReadVarLong();
        }
    }
}