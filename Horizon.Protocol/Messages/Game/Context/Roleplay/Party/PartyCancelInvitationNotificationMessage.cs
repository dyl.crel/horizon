namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyCancelInvitationNotificationMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 6251;

        public long CancelerId { get; set; }

        public long GuestId { get; set; }

        public PartyCancelInvitationNotificationMessage()
        {
        }

        public PartyCancelInvitationNotificationMessage(int partyId, long cancelerId, long guestId) : base(partyId)
        {
            CancelerId = cancelerId;
            GuestId = guestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(CancelerId);
            writer.WriteVarLong(GuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CancelerId = reader.ReadVarLong();
            GuestId = reader.ReadVarLong();
        }
    }
}