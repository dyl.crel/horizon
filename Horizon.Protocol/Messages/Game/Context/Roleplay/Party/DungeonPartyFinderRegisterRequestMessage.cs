namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderRegisterRequestMessage : NetworkMessage
    {
        public override short Protocol => 6249;

        public short[] DungeonIds { get; set; }

        public DungeonPartyFinderRegisterRequestMessage()
        {
        }

        public DungeonPartyFinderRegisterRequestMessage(short[] dungeonIds)
        {
            DungeonIds = dungeonIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(DungeonIds.Length);
            for (var i = 0; i < DungeonIds.Length; i++)
            {
                writer.WriteVarShort(DungeonIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonIds = new short[reader.ReadShort()];
            for (var i = 0; i < DungeonIds.Length; i++)
            {
                DungeonIds[i] = reader.ReadVarShort();
            }
        }
    }
}