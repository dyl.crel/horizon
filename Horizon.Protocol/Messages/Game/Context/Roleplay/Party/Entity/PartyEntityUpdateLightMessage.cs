namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party.Entity
{
    public class PartyEntityUpdateLightMessage : PartyUpdateLightMessage
    {
        public override short Protocol => 6781;

        public byte IndexId { get; set; }

        public PartyEntityUpdateLightMessage()
        {
        }

        public PartyEntityUpdateLightMessage(int partyId, long id, int lifePoints, int maxLifePoints, short prospecting, byte regenRate, byte indexId) : base(partyId, id, lifePoints, maxLifePoints, prospecting, regenRate)
        {
            IndexId = indexId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(IndexId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            IndexId = reader.ReadByte();
        }
    }
}