namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyLeaveMessage : AbstractPartyMessage
    {
        public override short Protocol => 5594;

        public PartyLeaveMessage()
        {
        }

        public PartyLeaveMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}