namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationMessage : AbstractPartyMessage
    {
        public override short Protocol => 5586;

        public byte PartyType { get; set; }

        public string PartyName { get; set; }

        public byte MaxParticipants { get; set; }

        public long FromId { get; set; }

        public string FromName { get; set; }

        public long ToId { get; set; }

        public PartyInvitationMessage()
        {
        }

        public PartyInvitationMessage(int partyId, byte partyType, string partyName, byte maxParticipants, long fromId, string fromName, long toId) : base(partyId)
        {
            PartyType = partyType;
            PartyName = partyName;
            MaxParticipants = maxParticipants;
            FromId = fromId;
            FromName = fromName;
            ToId = toId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PartyType);
            writer.WriteUTF(PartyName);
            writer.WriteByte(MaxParticipants);
            writer.WriteVarLong(FromId);
            writer.WriteUTF(FromName);
            writer.WriteVarLong(ToId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PartyType = reader.ReadByte();
            PartyName = reader.ReadUTF();
            MaxParticipants = reader.ReadByte();
            FromId = reader.ReadVarLong();
            FromName = reader.ReadUTF();
            ToId = reader.ReadVarLong();
        }
    }
}