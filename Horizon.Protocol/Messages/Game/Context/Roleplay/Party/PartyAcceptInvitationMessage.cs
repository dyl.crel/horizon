namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyAcceptInvitationMessage : AbstractPartyMessage
    {
        public override short Protocol => 5580;

        public PartyAcceptInvitationMessage()
        {
        }

        public PartyAcceptInvitationMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}