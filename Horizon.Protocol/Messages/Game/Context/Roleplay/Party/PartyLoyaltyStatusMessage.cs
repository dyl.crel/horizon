namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyLoyaltyStatusMessage : AbstractPartyMessage
    {
        public override short Protocol => 6270;

        public bool Loyal { get; set; }

        public PartyLoyaltyStatusMessage()
        {
        }

        public PartyLoyaltyStatusMessage(int partyId, bool loyal) : base(partyId)
        {
            Loyal = loyal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Loyal);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Loyal = reader.ReadBoolean();
        }
    }
}