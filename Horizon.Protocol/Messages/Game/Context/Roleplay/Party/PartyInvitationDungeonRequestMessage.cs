namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationDungeonRequestMessage : PartyInvitationRequestMessage
    {
        public override short Protocol => 6245;

        public short DungeonId { get; set; }

        public PartyInvitationDungeonRequestMessage()
        {
        }

        public PartyInvitationDungeonRequestMessage(string name, short dungeonId) : base(name)
        {
            DungeonId = dungeonId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(DungeonId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DungeonId = reader.ReadVarShort();
        }
    }
}