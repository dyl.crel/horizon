namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyFollowStatusUpdateMessage : AbstractPartyMessage
    {
        public override short Protocol => 5581;

        public bool Success { get; set; }

        public bool IsFollowed { get; set; }

        public long FollowedId { get; set; }

        public PartyFollowStatusUpdateMessage()
        {
        }

        public PartyFollowStatusUpdateMessage(int partyId, bool success, bool isFollowed, long followedId) : base(partyId)
        {
            Success = success;
            IsFollowed = isFollowed;
            FollowedId = followedId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Success);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsFollowed);
            writer.WriteByte(flag0);
            writer.WriteVarLong(FollowedId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag0, 0);
            IsFollowed = BooleanByteWrapper.GetFlag(flag0, 1);
            FollowedId = reader.ReadVarLong();
        }
    }
}