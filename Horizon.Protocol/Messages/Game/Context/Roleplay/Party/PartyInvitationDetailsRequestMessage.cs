namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationDetailsRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 6264;

        public PartyInvitationDetailsRequestMessage()
        {
        }

        public PartyInvitationDetailsRequestMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}