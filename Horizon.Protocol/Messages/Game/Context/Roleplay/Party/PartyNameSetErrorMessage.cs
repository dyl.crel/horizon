namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyNameSetErrorMessage : AbstractPartyMessage
    {
        public override short Protocol => 6501;

        public byte Result { get; set; }

        public PartyNameSetErrorMessage()
        {
        }

        public PartyNameSetErrorMessage(int partyId, byte result) : base(partyId)
        {
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Result = reader.ReadByte();
        }
    }
}