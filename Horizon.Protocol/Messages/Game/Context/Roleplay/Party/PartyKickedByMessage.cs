namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyKickedByMessage : AbstractPartyMessage
    {
        public override short Protocol => 5590;

        public long KickerId { get; set; }

        public PartyKickedByMessage()
        {
        }

        public PartyKickedByMessage(int partyId, long kickerId) : base(partyId)
        {
            KickerId = kickerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(KickerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            KickerId = reader.ReadVarLong();
        }
    }
}