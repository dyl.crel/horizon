namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderListenRequestMessage : NetworkMessage
    {
        public override short Protocol => 6246;

        public short DungeonId { get; set; }

        public DungeonPartyFinderListenRequestMessage()
        {
        }

        public DungeonPartyFinderListenRequestMessage(short dungeonId)
        {
            DungeonId = dungeonId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
        }
    }
}