using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyJoinMessage : AbstractPartyMessage
    {
        public override short Protocol => 5576;

        public byte PartyType { get; set; }

        public long PartyLeaderId { get; set; }

        public byte MaxParticipants { get; set; }

        public PartyMemberInformations[] Members { get; set; }

        public PartyGuestInformations[] Guests { get; set; }

        public bool Restricted { get; set; }

        public string PartyName { get; set; }

        public PartyJoinMessage()
        {
        }

        public PartyJoinMessage(int partyId, byte partyType, long partyLeaderId, byte maxParticipants, PartyMemberInformations[] members, PartyGuestInformations[] guests, bool restricted, string partyName) : base(partyId)
        {
            PartyType = partyType;
            PartyLeaderId = partyLeaderId;
            MaxParticipants = maxParticipants;
            Members = members;
            Guests = guests;
            Restricted = restricted;
            PartyName = partyName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PartyType);
            writer.WriteVarLong(PartyLeaderId);
            writer.WriteByte(MaxParticipants);
            writer.WriteShort(Members.Length);
            for (var i = 0; i < Members.Length; i++)
            {
                writer.WriteShort(Members[i].Protocol);
                Members[i].Serialize(writer);
            }
            writer.WriteShort(Guests.Length);
            for (var i = 0; i < Guests.Length; i++)
            {
                Guests[i].Serialize(writer);
            }
            writer.WriteBoolean(Restricted);
            writer.WriteUTF(PartyName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PartyType = reader.ReadByte();
            PartyLeaderId = reader.ReadVarLong();
            MaxParticipants = reader.ReadByte();
            Members = new PartyMemberInformations[reader.ReadShort()];
            for (var i = 0; i < Members.Length; i++)
            {
                Members[i] = ProtocolTypesManager.Instance<PartyMemberInformations>(reader.ReadShort());
                Members[i].Deserialize(reader);
            }
            Guests = new PartyGuestInformations[reader.ReadShort()];
            for (var i = 0; i < Guests.Length; i++)
            {
                Guests[i] = new PartyGuestInformations();
                Guests[i].Deserialize(reader);
            }
            Restricted = reader.ReadBoolean();
            PartyName = reader.ReadUTF();
        }
    }
}