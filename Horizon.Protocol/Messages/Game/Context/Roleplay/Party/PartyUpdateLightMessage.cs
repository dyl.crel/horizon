namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyUpdateLightMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 6054;

        public long Id { get; set; }

        public int LifePoints { get; set; }

        public int MaxLifePoints { get; set; }

        public short Prospecting { get; set; }

        public byte RegenRate { get; set; }

        public PartyUpdateLightMessage()
        {
        }

        public PartyUpdateLightMessage(int partyId, long id, int lifePoints, int maxLifePoints, short prospecting, byte regenRate) : base(partyId)
        {
            Id = id;
            LifePoints = lifePoints;
            MaxLifePoints = maxLifePoints;
            Prospecting = prospecting;
            RegenRate = regenRate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Id);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(Prospecting);
            writer.WriteByte(RegenRate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Id = reader.ReadVarLong();
            LifePoints = reader.ReadVarInt();
            MaxLifePoints = reader.ReadVarInt();
            Prospecting = reader.ReadVarShort();
            RegenRate = reader.ReadByte();
        }
    }
}