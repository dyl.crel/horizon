namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyPledgeLoyaltyRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 6269;

        public bool Loyal { get; set; }

        public PartyPledgeLoyaltyRequestMessage()
        {
        }

        public PartyPledgeLoyaltyRequestMessage(int partyId, bool loyal) : base(partyId)
        {
            Loyal = loyal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Loyal);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Loyal = reader.ReadBoolean();
        }
    }
}