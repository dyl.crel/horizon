namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyRefuseInvitationMessage : AbstractPartyMessage
    {
        public override short Protocol => 5582;

        public PartyRefuseInvitationMessage()
        {
        }

        public PartyRefuseInvitationMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}