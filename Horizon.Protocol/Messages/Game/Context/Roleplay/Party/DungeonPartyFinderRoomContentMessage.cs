using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderRoomContentMessage : NetworkMessage
    {
        public override short Protocol => 6247;

        public short DungeonId { get; set; }

        public DungeonPartyFinderPlayer[] Players { get; set; }

        public DungeonPartyFinderRoomContentMessage()
        {
        }

        public DungeonPartyFinderRoomContentMessage(short dungeonId, DungeonPartyFinderPlayer[] players)
        {
            DungeonId = dungeonId;
            Players = players;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteShort(Players.Length);
            for (var i = 0; i < Players.Length; i++)
            {
                Players[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            Players = new DungeonPartyFinderPlayer[reader.ReadShort()];
            for (var i = 0; i < Players.Length; i++)
            {
                Players[i] = new DungeonPartyFinderPlayer();
                Players[i].Deserialize(reader);
            }
        }
    }
}