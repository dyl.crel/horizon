namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class AbstractPartyMemberInFightMessage : AbstractPartyMessage
    {
        public override short Protocol => 6825;

        public byte Reason { get; set; }

        public long MemberId { get; set; }

        public int MemberAccountId { get; set; }

        public string MemberName { get; set; }

        public short FightId { get; set; }

        public short TimeBeforeFightStart { get; set; }

        public AbstractPartyMemberInFightMessage()
        {
        }

        public AbstractPartyMemberInFightMessage(int partyId, byte reason, long memberId, int memberAccountId, string memberName, short fightId, short timeBeforeFightStart) : base(partyId)
        {
            Reason = reason;
            MemberId = memberId;
            MemberAccountId = memberAccountId;
            MemberName = memberName;
            FightId = fightId;
            TimeBeforeFightStart = timeBeforeFightStart;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Reason);
            writer.WriteVarLong(MemberId);
            writer.WriteInt(MemberAccountId);
            writer.WriteUTF(MemberName);
            writer.WriteVarShort(FightId);
            writer.WriteVarShort(TimeBeforeFightStart);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Reason = reader.ReadByte();
            MemberId = reader.ReadVarLong();
            MemberAccountId = reader.ReadInt();
            MemberName = reader.ReadUTF();
            FightId = reader.ReadVarShort();
            TimeBeforeFightStart = reader.ReadVarShort();
        }
    }
}