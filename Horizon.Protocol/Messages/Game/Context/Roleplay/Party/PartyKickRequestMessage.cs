namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyKickRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 5592;

        public long PlayerId { get; set; }

        public PartyKickRequestMessage()
        {
        }

        public PartyKickRequestMessage(int partyId, long playerId) : base(partyId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}