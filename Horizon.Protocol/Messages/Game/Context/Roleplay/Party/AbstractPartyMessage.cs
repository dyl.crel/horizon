namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class AbstractPartyMessage : NetworkMessage
    {
        public override short Protocol => 6274;

        public int PartyId { get; set; }

        public AbstractPartyMessage()
        {
        }

        public AbstractPartyMessage(int partyId)
        {
            PartyId = partyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(PartyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PartyId = reader.ReadVarInt();
        }
    }
}