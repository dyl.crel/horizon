using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationDungeonDetailsMessage : PartyInvitationDetailsMessage
    {
        public override short Protocol => 6262;

        public short DungeonId { get; set; }

        public bool[] PlayersDungeonReady { get; set; }

        public PartyInvitationDungeonDetailsMessage()
        {
        }

        public PartyInvitationDungeonDetailsMessage(int partyId, byte partyType, string partyName, long fromId, string fromName, long leaderId, PartyInvitationMemberInformations[] members, PartyGuestInformations[] guests, short dungeonId, bool[] playersDungeonReady) : base(partyId, partyType, partyName, fromId, fromName, leaderId, members, guests)
        {
            DungeonId = dungeonId;
            PlayersDungeonReady = playersDungeonReady;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(DungeonId);
            writer.WriteShort(PlayersDungeonReady.Length);
            for (var i = 0; i < PlayersDungeonReady.Length; i++)
            {
                writer.WriteBoolean(PlayersDungeonReady[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DungeonId = reader.ReadVarShort();
            PlayersDungeonReady = new bool[reader.ReadShort()];
            for (var i = 0; i < PlayersDungeonReady.Length; i++)
            {
                PlayersDungeonReady[i] = reader.ReadBoolean();
            }
        }
    }
}