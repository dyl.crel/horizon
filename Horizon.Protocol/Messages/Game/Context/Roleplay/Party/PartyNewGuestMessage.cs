using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyNewGuestMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 6260;

        public PartyGuestInformations Guest { get; set; }

        public PartyNewGuestMessage()
        {
        }

        public PartyNewGuestMessage(int partyId, PartyGuestInformations guest) : base(partyId)
        {
            Guest = guest;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Guest.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Guest = new PartyGuestInformations();
            Guest.Deserialize(reader);
        }
    }
}