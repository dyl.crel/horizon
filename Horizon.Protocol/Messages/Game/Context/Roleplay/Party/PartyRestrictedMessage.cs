namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyRestrictedMessage : AbstractPartyMessage
    {
        public override short Protocol => 6175;

        public bool Restricted { get; set; }

        public PartyRestrictedMessage()
        {
        }

        public PartyRestrictedMessage(int partyId, bool restricted) : base(partyId)
        {
            Restricted = restricted;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Restricted);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Restricted = reader.ReadBoolean();
        }
    }
}