namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyFollowMemberRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 5577;

        public long PlayerId { get; set; }

        public PartyFollowMemberRequestMessage()
        {
        }

        public PartyFollowMemberRequestMessage(int partyId, long playerId) : base(partyId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}