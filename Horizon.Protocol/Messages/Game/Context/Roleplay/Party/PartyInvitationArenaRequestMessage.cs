namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationArenaRequestMessage : PartyInvitationRequestMessage
    {
        public override short Protocol => 6283;

        public PartyInvitationArenaRequestMessage()
        {
        }

        public PartyInvitationArenaRequestMessage(string name) : base(name)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}