namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyRefuseInvitationNotificationMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 5596;

        public long GuestId { get; set; }

        public PartyRefuseInvitationNotificationMessage()
        {
        }

        public PartyRefuseInvitationNotificationMessage(int partyId, long guestId) : base(partyId)
        {
            GuestId = guestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(GuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuestId = reader.ReadVarLong();
        }
    }
}