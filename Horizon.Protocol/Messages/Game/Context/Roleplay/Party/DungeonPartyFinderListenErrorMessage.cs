namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderListenErrorMessage : NetworkMessage
    {
        public override short Protocol => 6248;

        public short DungeonId { get; set; }

        public DungeonPartyFinderListenErrorMessage()
        {
        }

        public DungeonPartyFinderListenErrorMessage(short dungeonId)
        {
            DungeonId = dungeonId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
        }
    }
}