namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party.Breach
{
    public class PartyMemberInBreachFightMessage : AbstractPartyMemberInFightMessage
    {
        public override short Protocol => 6824;

        public int Floor { get; set; }

        public byte Room { get; set; }

        public PartyMemberInBreachFightMessage()
        {
        }

        public PartyMemberInBreachFightMessage(int partyId, byte reason, long memberId, int memberAccountId, string memberName, short fightId, short timeBeforeFightStart, int floor, byte room) : base(partyId, reason, memberId, memberAccountId, memberName, fightId, timeBeforeFightStart)
        {
            Floor = floor;
            Room = room;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Floor);
            writer.WriteByte(Room);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Floor = reader.ReadVarInt();
            Room = reader.ReadByte();
        }
    }
}