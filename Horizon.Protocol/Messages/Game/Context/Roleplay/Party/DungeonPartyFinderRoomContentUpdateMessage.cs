using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderRoomContentUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6250;

        public short DungeonId { get; set; }

        public DungeonPartyFinderPlayer[] AddedPlayers { get; set; }

        public long[] RemovedPlayersIds { get; set; }

        public DungeonPartyFinderRoomContentUpdateMessage()
        {
        }

        public DungeonPartyFinderRoomContentUpdateMessage(short dungeonId, DungeonPartyFinderPlayer[] addedPlayers, long[] removedPlayersIds)
        {
            DungeonId = dungeonId;
            AddedPlayers = addedPlayers;
            RemovedPlayersIds = removedPlayersIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteShort(AddedPlayers.Length);
            for (var i = 0; i < AddedPlayers.Length; i++)
            {
                AddedPlayers[i].Serialize(writer);
            }
            writer.WriteShort(RemovedPlayersIds.Length);
            for (var i = 0; i < RemovedPlayersIds.Length; i++)
            {
                writer.WriteVarLong(RemovedPlayersIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            AddedPlayers = new DungeonPartyFinderPlayer[reader.ReadShort()];
            for (var i = 0; i < AddedPlayers.Length; i++)
            {
                AddedPlayers[i] = new DungeonPartyFinderPlayer();
                AddedPlayers[i].Deserialize(reader);
            }
            RemovedPlayersIds = new long[reader.ReadShort()];
            for (var i = 0; i < RemovedPlayersIds.Length; i++)
            {
                RemovedPlayersIds[i] = reader.ReadVarLong();
            }
        }
    }
}