namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class AbstractPartyEventMessage : AbstractPartyMessage
    {
        public override short Protocol => 6273;

        public AbstractPartyEventMessage()
        {
        }

        public AbstractPartyEventMessage(int partyId) : base(partyId)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}