namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderAvailableDungeonsRequestMessage : NetworkMessage
    {
        public override short Protocol => 6240;

        public DungeonPartyFinderAvailableDungeonsRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}