namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyMemberEjectedMessage : PartyMemberRemoveMessage
    {
        public override short Protocol => 6252;

        public long KickerId { get; set; }

        public PartyMemberEjectedMessage()
        {
        }

        public PartyMemberEjectedMessage(int partyId, long leavingPlayerId, long kickerId) : base(partyId, leavingPlayerId)
        {
            KickerId = kickerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(KickerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            KickerId = reader.ReadVarLong();
        }
    }
}