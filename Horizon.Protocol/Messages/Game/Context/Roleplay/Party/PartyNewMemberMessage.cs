using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyNewMemberMessage : PartyUpdateMessage
    {
        public override short Protocol => 6306;

        public PartyNewMemberMessage()
        {
        }

        public PartyNewMemberMessage(int partyId, PartyMemberInformations memberInformations) : base(partyId, memberInformations)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}