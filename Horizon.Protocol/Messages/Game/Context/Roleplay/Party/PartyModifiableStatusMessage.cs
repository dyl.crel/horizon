namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyModifiableStatusMessage : AbstractPartyMessage
    {
        public override short Protocol => 6277;

        public bool Enabled { get; set; }

        public PartyModifiableStatusMessage()
        {
        }

        public PartyModifiableStatusMessage(int partyId, bool enabled) : base(partyId)
        {
            Enabled = enabled;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Enabled);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Enabled = reader.ReadBoolean();
        }
    }
}