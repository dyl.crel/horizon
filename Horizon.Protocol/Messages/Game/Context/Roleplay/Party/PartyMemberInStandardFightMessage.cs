using Horizon.Protocol.Types.Game.Context;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyMemberInStandardFightMessage : AbstractPartyMemberInFightMessage
    {
        public override short Protocol => 6826;

        public MapCoordinatesExtended FightMap { get; set; }

        public PartyMemberInStandardFightMessage()
        {
        }

        public PartyMemberInStandardFightMessage(int partyId, byte reason, long memberId, int memberAccountId, string memberName, short fightId, short timeBeforeFightStart, MapCoordinatesExtended fightMap) : base(partyId, reason, memberId, memberAccountId, memberName, fightId, timeBeforeFightStart)
        {
            FightMap = fightMap;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            FightMap.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FightMap = new MapCoordinatesExtended();
            FightMap.Deserialize(reader);
        }
    }
}