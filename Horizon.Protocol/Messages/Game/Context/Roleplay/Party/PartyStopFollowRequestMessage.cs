namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyStopFollowRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 5574;

        public long PlayerId { get; set; }

        public PartyStopFollowRequestMessage()
        {
        }

        public PartyStopFollowRequestMessage(int partyId, long playerId) : base(partyId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}