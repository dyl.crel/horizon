namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyInvitationDungeonMessage : PartyInvitationMessage
    {
        public override short Protocol => 6244;

        public short DungeonId { get; set; }

        public PartyInvitationDungeonMessage()
        {
        }

        public PartyInvitationDungeonMessage(int partyId, byte partyType, string partyName, byte maxParticipants, long fromId, string fromName, long toId, short dungeonId) : base(partyId, partyType, partyName, maxParticipants, fromId, fromName, toId)
        {
            DungeonId = dungeonId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(DungeonId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DungeonId = reader.ReadVarShort();
        }
    }
}