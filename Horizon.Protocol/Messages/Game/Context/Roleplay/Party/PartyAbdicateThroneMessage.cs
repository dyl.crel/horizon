namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyAbdicateThroneMessage : AbstractPartyMessage
    {
        public override short Protocol => 6080;

        public long PlayerId { get; set; }

        public PartyAbdicateThroneMessage()
        {
        }

        public PartyAbdicateThroneMessage(int partyId, long playerId) : base(partyId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}