namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyNameSetRequestMessage : AbstractPartyMessage
    {
        public override short Protocol => 6503;

        public string PartyName { get; set; }

        public PartyNameSetRequestMessage()
        {
        }

        public PartyNameSetRequestMessage(int partyId, string partyName) : base(partyId)
        {
            PartyName = partyName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(PartyName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PartyName = reader.ReadUTF();
        }
    }
}