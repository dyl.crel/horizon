namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyFollowThisMemberRequestMessage : PartyFollowMemberRequestMessage
    {
        public override short Protocol => 5588;

        public bool Enabled { get; set; }

        public PartyFollowThisMemberRequestMessage()
        {
        }

        public PartyFollowThisMemberRequestMessage(int partyId, long playerId, bool enabled) : base(partyId, playerId)
        {
            Enabled = enabled;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Enabled);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Enabled = reader.ReadBoolean();
        }
    }
}