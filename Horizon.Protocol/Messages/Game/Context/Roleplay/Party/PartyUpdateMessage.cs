using Horizon.Protocol.Types.Game.Context.Roleplay.Party;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyUpdateMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 5575;

        public PartyMemberInformations MemberInformations { get; set; }

        public PartyUpdateMessage()
        {
        }

        public PartyUpdateMessage(int partyId, PartyMemberInformations memberInformations) : base(partyId)
        {
            MemberInformations = memberInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MemberInformations.Protocol);
            MemberInformations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MemberInformations = ProtocolTypesManager.Instance<PartyMemberInformations>(reader.ReadShort());
            MemberInformations.Deserialize(reader);
        }
    }
}