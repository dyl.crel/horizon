namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Party
{
    public class PartyMemberRemoveMessage : AbstractPartyEventMessage
    {
        public override short Protocol => 5579;

        public long LeavingPlayerId { get; set; }

        public PartyMemberRemoveMessage()
        {
        }

        public PartyMemberRemoveMessage(int partyId, long leavingPlayerId) : base(partyId)
        {
            LeavingPlayerId = leavingPlayerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(LeavingPlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LeavingPlayerId = reader.ReadVarLong();
        }
    }
}