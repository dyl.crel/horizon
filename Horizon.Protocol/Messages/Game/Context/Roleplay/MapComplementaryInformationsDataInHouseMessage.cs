using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapComplementaryInformationsDataInHouseMessage : MapComplementaryInformationsDataMessage
    {
        public override short Protocol => 6130;

        public HouseInformationsInside CurrentHouse { get; set; }

        public MapComplementaryInformationsDataInHouseMessage()
        {
        }

        public MapComplementaryInformationsDataInHouseMessage(short subAreaId, double mapId, HouseInformations[] houses, GameRolePlayActorInformations[] actors, InteractiveElement[] interactiveElements, StatedElement[] statedElements, MapObstacle[] obstacles, FightCommonInformations[] fights, bool hasAggressiveMonsters, FightStartingPositions fightStartPositions, HouseInformationsInside currentHouse) : base(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions)
        {
            CurrentHouse = currentHouse;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            CurrentHouse.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CurrentHouse = new HouseInformationsInside();
            CurrentHouse.Deserialize(reader);
        }
    }
}