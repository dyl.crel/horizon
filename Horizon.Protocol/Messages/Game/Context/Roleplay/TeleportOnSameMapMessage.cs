namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class TeleportOnSameMapMessage : NetworkMessage
    {
        public override short Protocol => 6048;

        public double TargetId { get; set; }

        public short CellId { get; set; }

        public TeleportOnSameMapMessage()
        {
        }

        public TeleportOnSameMapMessage(double targetId, short cellId)
        {
            TargetId = targetId;
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(TargetId);
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TargetId = reader.ReadDouble();
            CellId = reader.ReadVarShort();
        }
    }
}