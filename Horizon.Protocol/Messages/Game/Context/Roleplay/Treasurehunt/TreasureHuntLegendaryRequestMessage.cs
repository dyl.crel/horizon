namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntLegendaryRequestMessage : NetworkMessage
    {
        public override short Protocol => 6499;

        public short LegendaryId { get; set; }

        public TreasureHuntLegendaryRequestMessage()
        {
        }

        public TreasureHuntLegendaryRequestMessage(short legendaryId)
        {
            LegendaryId = legendaryId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(LegendaryId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            LegendaryId = reader.ReadVarShort();
        }
    }
}