namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntFinishedMessage : NetworkMessage
    {
        public override short Protocol => 6483;

        public byte QuestType { get; set; }

        public TreasureHuntFinishedMessage()
        {
        }

        public TreasureHuntFinishedMessage(byte questType)
        {
            QuestType = questType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
        }
    }
}