namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntFlagRequestAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6507;

        public byte QuestType { get; set; }

        public byte Result { get; set; }

        public byte Index { get; set; }

        public TreasureHuntFlagRequestAnswerMessage()
        {
        }

        public TreasureHuntFlagRequestAnswerMessage(byte questType, byte result, byte index)
        {
            QuestType = questType;
            Result = result;
            Index = index;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
            writer.WriteByte(Result);
            writer.WriteByte(Index);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
            Result = reader.ReadByte();
            Index = reader.ReadByte();
        }
    }
}