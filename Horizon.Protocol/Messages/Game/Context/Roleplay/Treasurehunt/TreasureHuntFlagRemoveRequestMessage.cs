namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntFlagRemoveRequestMessage : NetworkMessage
    {
        public override short Protocol => 6510;

        public byte QuestType { get; set; }

        public byte Index { get; set; }

        public TreasureHuntFlagRemoveRequestMessage()
        {
        }

        public TreasureHuntFlagRemoveRequestMessage(byte questType, byte index)
        {
            QuestType = questType;
            Index = index;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
            writer.WriteByte(Index);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
            Index = reader.ReadByte();
        }
    }
}