namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntDigRequestMessage : NetworkMessage
    {
        public override short Protocol => 6485;

        public byte QuestType { get; set; }

        public TreasureHuntDigRequestMessage()
        {
        }

        public TreasureHuntDigRequestMessage(byte questType)
        {
            QuestType = questType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
        }
    }
}