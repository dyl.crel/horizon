namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class PortalUseRequestMessage : NetworkMessage
    {
        public override short Protocol => 6492;

        public int PortalId { get; set; }

        public PortalUseRequestMessage()
        {
        }

        public PortalUseRequestMessage(int portalId)
        {
            PortalId = portalId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(PortalId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PortalId = reader.ReadVarInt();
        }
    }
}