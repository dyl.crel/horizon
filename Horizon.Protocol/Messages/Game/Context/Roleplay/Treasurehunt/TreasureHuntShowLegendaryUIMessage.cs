namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntShowLegendaryUIMessage : NetworkMessage
    {
        public override short Protocol => 6498;

        public short[] AvailableLegendaryIds { get; set; }

        public TreasureHuntShowLegendaryUIMessage()
        {
        }

        public TreasureHuntShowLegendaryUIMessage(short[] availableLegendaryIds)
        {
            AvailableLegendaryIds = availableLegendaryIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(AvailableLegendaryIds.Length);
            for (var i = 0; i < AvailableLegendaryIds.Length; i++)
            {
                writer.WriteVarShort(AvailableLegendaryIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AvailableLegendaryIds = new short[reader.ReadShort()];
            for (var i = 0; i < AvailableLegendaryIds.Length; i++)
            {
                AvailableLegendaryIds[i] = reader.ReadVarShort();
            }
        }
    }
}