namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntGiveUpRequestMessage : NetworkMessage
    {
        public override short Protocol => 6487;

        public byte QuestType { get; set; }

        public TreasureHuntGiveUpRequestMessage()
        {
        }

        public TreasureHuntGiveUpRequestMessage(byte questType)
        {
            QuestType = questType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
        }
    }
}