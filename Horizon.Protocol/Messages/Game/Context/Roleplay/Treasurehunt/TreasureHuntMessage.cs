using Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntMessage : NetworkMessage
    {
        public override short Protocol => 6486;

        public byte QuestType { get; set; }

        public double StartMapId { get; set; }

        public TreasureHuntStep[] KnownStepsList { get; set; }

        public byte TotalStepCount { get; set; }

        public int CheckPointCurrent { get; set; }

        public int CheckPointTotal { get; set; }

        public int AvailableRetryCount { get; set; }

        public TreasureHuntFlag[] Flags { get; set; }

        public TreasureHuntMessage()
        {
        }

        public TreasureHuntMessage(byte questType, double startMapId, TreasureHuntStep[] knownStepsList, byte totalStepCount, int checkPointCurrent, int checkPointTotal, int availableRetryCount, TreasureHuntFlag[] flags)
        {
            QuestType = questType;
            StartMapId = startMapId;
            KnownStepsList = knownStepsList;
            TotalStepCount = totalStepCount;
            CheckPointCurrent = checkPointCurrent;
            CheckPointTotal = checkPointTotal;
            AvailableRetryCount = availableRetryCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
            writer.WriteDouble(StartMapId);
            writer.WriteShort(KnownStepsList.Length);
            for (var i = 0; i < KnownStepsList.Length; i++)
            {
                writer.WriteShort(KnownStepsList[i].Protocol);
                KnownStepsList[i].Serialize(writer);
            }
            writer.WriteByte(TotalStepCount);
            writer.WriteVarInt(CheckPointCurrent);
            writer.WriteVarInt(CheckPointTotal);
            writer.WriteInt(AvailableRetryCount);
            writer.WriteShort(Flags.Length);
            for (var i = 0; i < Flags.Length; i++)
            {
                Flags[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
            StartMapId = reader.ReadDouble();
            KnownStepsList = new TreasureHuntStep[reader.ReadShort()];
            for (var i = 0; i < KnownStepsList.Length; i++)
            {
                KnownStepsList[i] = ProtocolTypesManager.Instance<TreasureHuntStep>(reader.ReadShort());
                KnownStepsList[i].Deserialize(reader);
            }
            TotalStepCount = reader.ReadByte();
            CheckPointCurrent = reader.ReadVarInt();
            CheckPointTotal = reader.ReadVarInt();
            AvailableRetryCount = reader.ReadInt();
            Flags = new TreasureHuntFlag[reader.ReadShort()];
            for (var i = 0; i < Flags.Length; i++)
            {
                Flags[i] = new TreasureHuntFlag();
                Flags[i].Deserialize(reader);
            }
        }
    }
}