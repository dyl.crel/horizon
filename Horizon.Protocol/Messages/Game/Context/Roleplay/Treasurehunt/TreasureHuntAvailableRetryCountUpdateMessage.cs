namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntAvailableRetryCountUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6491;

        public byte QuestType { get; set; }

        public int AvailableRetryCount { get; set; }

        public TreasureHuntAvailableRetryCountUpdateMessage()
        {
        }

        public TreasureHuntAvailableRetryCountUpdateMessage(byte questType, int availableRetryCount)
        {
            QuestType = questType;
            AvailableRetryCount = availableRetryCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
            writer.WriteInt(AvailableRetryCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
            AvailableRetryCount = reader.ReadInt();
        }
    }
}