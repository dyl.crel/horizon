namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntDigRequestAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6484;

        public byte QuestType { get; set; }

        public byte Result { get; set; }

        public TreasureHuntDigRequestAnswerMessage()
        {
        }

        public TreasureHuntDigRequestAnswerMessage(byte questType, byte result)
        {
            QuestType = questType;
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(QuestType);
            writer.WriteByte(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestType = reader.ReadByte();
            Result = reader.ReadByte();
        }
    }
}