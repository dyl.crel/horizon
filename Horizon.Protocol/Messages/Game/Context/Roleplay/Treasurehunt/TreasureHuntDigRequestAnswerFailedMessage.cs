namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntDigRequestAnswerFailedMessage : TreasureHuntDigRequestAnswerMessage
    {
        public override short Protocol => 6509;

        public byte WrongFlagCount { get; set; }

        public TreasureHuntDigRequestAnswerFailedMessage()
        {
        }

        public TreasureHuntDigRequestAnswerFailedMessage(byte questType, byte result, byte wrongFlagCount) : base(questType, result)
        {
            WrongFlagCount = wrongFlagCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(WrongFlagCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WrongFlagCount = reader.ReadByte();
        }
    }
}