namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Document
{
    public class ComicReadingBeginMessage : NetworkMessage
    {
        public override short Protocol => 6536;

        public short ComicId { get; set; }

        public ComicReadingBeginMessage()
        {
        }

        public ComicReadingBeginMessage(short comicId)
        {
            ComicId = comicId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ComicId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ComicId = reader.ReadVarShort();
        }
    }
}