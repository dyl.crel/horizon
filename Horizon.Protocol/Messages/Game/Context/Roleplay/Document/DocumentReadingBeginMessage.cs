namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Document
{
    public class DocumentReadingBeginMessage : NetworkMessage
    {
        public override short Protocol => 5675;

        public short DocumentId { get; set; }

        public DocumentReadingBeginMessage()
        {
        }

        public DocumentReadingBeginMessage(short documentId)
        {
            DocumentId = documentId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DocumentId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DocumentId = reader.ReadVarShort();
        }
    }
}