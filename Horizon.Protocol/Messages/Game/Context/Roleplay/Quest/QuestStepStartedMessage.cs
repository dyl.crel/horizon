namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestStepStartedMessage : NetworkMessage
    {
        public override short Protocol => 6096;

        public short QuestId { get; set; }

        public short StepId { get; set; }

        public QuestStepStartedMessage()
        {
        }

        public QuestStepStartedMessage(short questId, short stepId)
        {
            QuestId = questId;
            StepId = stepId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
            writer.WriteVarShort(StepId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
            StepId = reader.ReadVarShort();
        }
    }
}