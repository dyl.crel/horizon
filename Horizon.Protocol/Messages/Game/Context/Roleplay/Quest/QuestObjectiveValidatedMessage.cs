namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestObjectiveValidatedMessage : NetworkMessage
    {
        public override short Protocol => 6098;

        public short QuestId { get; set; }

        public short ObjectiveId { get; set; }

        public QuestObjectiveValidatedMessage()
        {
        }

        public QuestObjectiveValidatedMessage(short questId, short objectiveId)
        {
            QuestId = questId;
            ObjectiveId = objectiveId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
            writer.WriteVarShort(ObjectiveId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
            ObjectiveId = reader.ReadVarShort();
        }
    }
}