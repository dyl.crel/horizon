namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestStepValidatedMessage : NetworkMessage
    {
        public override short Protocol => 6099;

        public short QuestId { get; set; }

        public short StepId { get; set; }

        public QuestStepValidatedMessage()
        {
        }

        public QuestStepValidatedMessage(short questId, short stepId)
        {
            QuestId = questId;
            StepId = stepId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
            writer.WriteVarShort(StepId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
            StepId = reader.ReadVarShort();
        }
    }
}