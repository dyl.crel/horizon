namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestListRequestMessage : NetworkMessage
    {
        public override short Protocol => 5623;

        public QuestListRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}