namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestStartedMessage : NetworkMessage
    {
        public override short Protocol => 6091;

        public short QuestId { get; set; }

        public QuestStartedMessage()
        {
        }

        public QuestStartedMessage(short questId)
        {
            QuestId = questId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
        }
    }
}