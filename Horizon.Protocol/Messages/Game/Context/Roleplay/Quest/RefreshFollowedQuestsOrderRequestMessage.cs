namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class RefreshFollowedQuestsOrderRequestMessage : NetworkMessage
    {
        public override short Protocol => 6722;

        public short[] Quests { get; set; }

        public RefreshFollowedQuestsOrderRequestMessage()
        {
        }

        public RefreshFollowedQuestsOrderRequestMessage(short[] quests)
        {
            Quests = quests;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Quests.Length);
            for (var i = 0; i < Quests.Length; i++)
            {
                writer.WriteVarShort(Quests[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quests = new short[reader.ReadShort()];
            for (var i = 0; i < Quests.Length; i++)
            {
                Quests[i] = reader.ReadVarShort();
            }
        }
    }
}