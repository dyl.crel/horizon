using Horizon.Protocol.Types.Game.Context.Roleplay.Quest;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestListMessage : NetworkMessage
    {
        public override short Protocol => 5626;

        public short[] FinishedQuestsIds { get; set; }

        public short[] FinishedQuestsCounts { get; set; }

        public QuestActiveInformations[] ActiveQuests { get; set; }

        public short[] ReinitDoneQuestsIds { get; set; }

        public QuestListMessage()
        {
        }

        public QuestListMessage(short[] finishedQuestsIds, short[] finishedQuestsCounts, QuestActiveInformations[] activeQuests, short[] reinitDoneQuestsIds)
        {
            FinishedQuestsIds = finishedQuestsIds;
            FinishedQuestsCounts = finishedQuestsCounts;
            ActiveQuests = activeQuests;
            ReinitDoneQuestsIds = reinitDoneQuestsIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FinishedQuestsIds.Length);
            for (var i = 0; i < FinishedQuestsIds.Length; i++)
            {
                writer.WriteVarShort(FinishedQuestsIds[i]);
            }
            writer.WriteShort(FinishedQuestsCounts.Length);
            for (var i = 0; i < FinishedQuestsCounts.Length; i++)
            {
                writer.WriteVarShort(FinishedQuestsCounts[i]);
            }
            writer.WriteShort(ActiveQuests.Length);
            for (var i = 0; i < ActiveQuests.Length; i++)
            {
                writer.WriteShort(ActiveQuests[i].Protocol);
                ActiveQuests[i].Serialize(writer);
            }
            writer.WriteShort(ReinitDoneQuestsIds.Length);
            for (var i = 0; i < ReinitDoneQuestsIds.Length; i++)
            {
                writer.WriteVarShort(ReinitDoneQuestsIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FinishedQuestsIds = new short[reader.ReadShort()];
            for (var i = 0; i < FinishedQuestsIds.Length; i++)
            {
                FinishedQuestsIds[i] = reader.ReadVarShort();
            }
            FinishedQuestsCounts = new short[reader.ReadShort()];
            for (var i = 0; i < FinishedQuestsCounts.Length; i++)
            {
                FinishedQuestsCounts[i] = reader.ReadVarShort();
            }
            ActiveQuests = new QuestActiveInformations[reader.ReadShort()];
            for (var i = 0; i < ActiveQuests.Length; i++)
            {
                ActiveQuests[i] = ProtocolTypesManager.Instance<QuestActiveInformations>(reader.ReadShort());
                ActiveQuests[i].Deserialize(reader);
            }
            ReinitDoneQuestsIds = new short[reader.ReadShort()];
            for (var i = 0; i < ReinitDoneQuestsIds.Length; i++)
            {
                ReinitDoneQuestsIds[i] = reader.ReadVarShort();
            }
        }
    }
}