namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class GuidedModeQuitRequestMessage : NetworkMessage
    {
        public override short Protocol => 6092;

        public GuidedModeQuitRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}