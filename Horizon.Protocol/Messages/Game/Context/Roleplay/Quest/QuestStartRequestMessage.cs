namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestStartRequestMessage : NetworkMessage
    {
        public override short Protocol => 5643;

        public short QuestId { get; set; }

        public QuestStartRequestMessage()
        {
        }

        public QuestStartRequestMessage(short questId)
        {
            QuestId = questId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
        }
    }
}