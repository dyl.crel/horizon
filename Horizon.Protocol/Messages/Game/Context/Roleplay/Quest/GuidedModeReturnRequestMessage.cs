namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class GuidedModeReturnRequestMessage : NetworkMessage
    {
        public override short Protocol => 6088;

        public GuidedModeReturnRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}