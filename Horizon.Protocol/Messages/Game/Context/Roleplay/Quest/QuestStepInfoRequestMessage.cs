namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestStepInfoRequestMessage : NetworkMessage
    {
        public override short Protocol => 5622;

        public short QuestId { get; set; }

        public QuestStepInfoRequestMessage()
        {
        }

        public QuestStepInfoRequestMessage(short questId)
        {
            QuestId = questId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
        }
    }
}