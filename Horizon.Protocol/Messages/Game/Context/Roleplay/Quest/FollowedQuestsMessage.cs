using Horizon.Protocol.Types.Game.Context.Roleplay.Quest;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class FollowedQuestsMessage : NetworkMessage
    {
        public override short Protocol => 6717;

        public QuestActiveDetailedInformations[] Quests { get; set; }

        public FollowedQuestsMessage()
        {
        }

        public FollowedQuestsMessage(QuestActiveDetailedInformations[] quests)
        {
            Quests = quests;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Quests.Length);
            for (var i = 0; i < Quests.Length; i++)
            {
                Quests[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quests = new QuestActiveDetailedInformations[reader.ReadShort()];
            for (var i = 0; i < Quests.Length; i++)
            {
                Quests[i] = new QuestActiveDetailedInformations();
                Quests[i].Deserialize(reader);
            }
        }
    }
}