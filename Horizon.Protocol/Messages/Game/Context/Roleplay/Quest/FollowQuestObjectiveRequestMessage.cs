namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class FollowQuestObjectiveRequestMessage : NetworkMessage
    {
        public override short Protocol => 6724;

        public short QuestId { get; set; }

        public short ObjectiveId { get; set; }

        public FollowQuestObjectiveRequestMessage()
        {
        }

        public FollowQuestObjectiveRequestMessage(short questId, short objectiveId)
        {
            QuestId = questId;
            ObjectiveId = objectiveId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
            writer.WriteShort(ObjectiveId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
            ObjectiveId = reader.ReadShort();
        }
    }
}