using Horizon.Protocol.Types.Game.Context.Roleplay.Quest;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestStepInfoMessage : NetworkMessage
    {
        public override short Protocol => 5625;

        public QuestActiveInformations Infos { get; set; }

        public QuestStepInfoMessage()
        {
        }

        public QuestStepInfoMessage(QuestActiveInformations infos)
        {
            Infos = infos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Infos.Protocol);
            Infos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Infos = ProtocolTypesManager.Instance<QuestActiveInformations>(reader.ReadShort());
            Infos.Deserialize(reader);
        }
    }
}