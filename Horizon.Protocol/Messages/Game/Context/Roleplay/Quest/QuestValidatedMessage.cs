namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Quest
{
    public class QuestValidatedMessage : NetworkMessage
    {
        public override short Protocol => 6097;

        public short QuestId { get; set; }

        public QuestValidatedMessage()
        {
        }

        public QuestValidatedMessage(short questId)
        {
            QuestId = questId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
        }
    }
}