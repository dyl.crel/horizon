using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapRunningFightDetailsMessage : NetworkMessage
    {
        public override short Protocol => 5751;

        public short FightId { get; set; }

        public GameFightFighterLightInformations[] Attackers { get; set; }

        public GameFightFighterLightInformations[] Defenders { get; set; }

        public MapRunningFightDetailsMessage()
        {
        }

        public MapRunningFightDetailsMessage(short fightId, GameFightFighterLightInformations[] attackers, GameFightFighterLightInformations[] defenders)
        {
            FightId = fightId;
            Attackers = attackers;
            Defenders = defenders;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteShort(Attackers.Length);
            for (var i = 0; i < Attackers.Length; i++)
            {
                writer.WriteShort(Attackers[i].Protocol);
                Attackers[i].Serialize(writer);
            }
            writer.WriteShort(Defenders.Length);
            for (var i = 0; i < Defenders.Length; i++)
            {
                writer.WriteShort(Defenders[i].Protocol);
                Defenders[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            Attackers = new GameFightFighterLightInformations[reader.ReadShort()];
            for (var i = 0; i < Attackers.Length; i++)
            {
                Attackers[i] = ProtocolTypesManager.Instance<GameFightFighterLightInformations>(reader.ReadShort());
                Attackers[i].Deserialize(reader);
            }
            Defenders = new GameFightFighterLightInformations[reader.ReadShort()];
            for (var i = 0; i < Defenders.Length; i++)
            {
                Defenders[i] = ProtocolTypesManager.Instance<GameFightFighterLightInformations>(reader.ReadShort());
                Defenders[i].Deserialize(reader);
            }
        }
    }
}