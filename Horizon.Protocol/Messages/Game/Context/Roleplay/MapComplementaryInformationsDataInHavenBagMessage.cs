using Horizon.Protocol.Types.Game.Character;
using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapComplementaryInformationsDataInHavenBagMessage : MapComplementaryInformationsDataMessage
    {
        public override short Protocol => 6622;

        public CharacterMinimalInformations OwnerInformations { get; set; }

        public byte Theme { get; set; }

        public byte RoomId { get; set; }

        public byte MaxRoomId { get; set; }

        public MapComplementaryInformationsDataInHavenBagMessage()
        {
        }

        public MapComplementaryInformationsDataInHavenBagMessage(short subAreaId, double mapId, HouseInformations[] houses, GameRolePlayActorInformations[] actors, InteractiveElement[] interactiveElements, StatedElement[] statedElements, MapObstacle[] obstacles, FightCommonInformations[] fights, bool hasAggressiveMonsters, FightStartingPositions fightStartPositions, CharacterMinimalInformations ownerInformations, byte theme, byte roomId, byte maxRoomId) : base(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions)
        {
            OwnerInformations = ownerInformations;
            Theme = theme;
            RoomId = roomId;
            MaxRoomId = maxRoomId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            OwnerInformations.Serialize(writer);
            writer.WriteByte(Theme);
            writer.WriteByte(RoomId);
            writer.WriteByte(MaxRoomId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            OwnerInformations = new CharacterMinimalInformations();
            OwnerInformations.Deserialize(reader);
            Theme = reader.ReadByte();
            RoomId = reader.ReadByte();
            MaxRoomId = reader.ReadByte();
        }
    }
}