namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayRemoveChallengeMessage : NetworkMessage
    {
        public override short Protocol => 300;

        public short FightId { get; set; }

        public GameRolePlayRemoveChallengeMessage()
        {
        }

        public GameRolePlayRemoveChallengeMessage(short fightId)
        {
            FightId = fightId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
        }
    }
}