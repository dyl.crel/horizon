namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayPlayerFightFriendlyAnswerMessage : NetworkMessage
    {
        public override short Protocol => 5732;

        public short FightId { get; set; }

        public bool Accept { get; set; }

        public GameRolePlayPlayerFightFriendlyAnswerMessage()
        {
        }

        public GameRolePlayPlayerFightFriendlyAnswerMessage(short fightId, bool accept)
        {
            FightId = fightId;
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            Accept = reader.ReadBoolean();
        }
    }
}