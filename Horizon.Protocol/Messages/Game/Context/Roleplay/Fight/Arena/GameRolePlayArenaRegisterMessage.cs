namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaRegisterMessage : NetworkMessage
    {
        public override short Protocol => 6280;

        public int BattleMode { get; set; }

        public GameRolePlayArenaRegisterMessage()
        {
        }

        public GameRolePlayArenaRegisterMessage(int battleMode)
        {
            BattleMode = battleMode;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(BattleMode);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BattleMode = reader.ReadInt();
        }
    }
}