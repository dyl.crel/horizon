using Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaUpdatePlayerInfosMessage : NetworkMessage
    {
        public override short Protocol => 6301;

        public ArenaRankInfos Solo { get; set; }

        public GameRolePlayArenaUpdatePlayerInfosMessage()
        {
        }

        public GameRolePlayArenaUpdatePlayerInfosMessage(ArenaRankInfos solo)
        {
            Solo = solo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Solo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Solo = new ArenaRankInfos();
            Solo.Deserialize(reader);
        }
    }
}