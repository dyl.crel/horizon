namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaFighterStatusMessage : NetworkMessage
    {
        public override short Protocol => 6281;

        public short FightId { get; set; }

        public double PlayerId { get; set; }

        public bool Accepted { get; set; }

        public GameRolePlayArenaFighterStatusMessage()
        {
        }

        public GameRolePlayArenaFighterStatusMessage(short fightId, double playerId, bool accepted)
        {
            FightId = fightId;
            PlayerId = playerId;
            Accepted = accepted;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteDouble(PlayerId);
            writer.WriteBoolean(Accepted);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            PlayerId = reader.ReadDouble();
            Accepted = reader.ReadBoolean();
        }
    }
}