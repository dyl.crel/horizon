namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaRegistrationStatusMessage : NetworkMessage
    {
        public override short Protocol => 6284;

        public bool Registered { get; set; }

        public byte Step { get; set; }

        public int BattleMode { get; set; }

        public GameRolePlayArenaRegistrationStatusMessage()
        {
        }

        public GameRolePlayArenaRegistrationStatusMessage(bool registered, byte step, int battleMode)
        {
            Registered = registered;
            Step = step;
            BattleMode = battleMode;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Registered);
            writer.WriteByte(Step);
            writer.WriteInt(BattleMode);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Registered = reader.ReadBoolean();
            Step = reader.ReadByte();
            BattleMode = reader.ReadInt();
        }
    }
}