namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaSwitchToFightServerMessage : NetworkMessage
    {
        public override short Protocol => 6575;

        public string Address { get; set; }

        public short[] Ports { get; set; }

        public byte[] Ticket { get; set; }

        public GameRolePlayArenaSwitchToFightServerMessage()
        {
        }

        public GameRolePlayArenaSwitchToFightServerMessage(string address, short[] ports, byte[] ticket)
        {
            Address = address;
            Ports = ports;
            Ticket = ticket;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Address);
            writer.WriteShort(Ports.Length);
            for (var i = 0; i < Ports.Length; i++)
            {
                writer.WriteShort(Ports[i]);
            }
            writer.WriteVarInt(Ticket.Length);
            for (var i = 0; i < Ticket.Length; i++)
            {
                writer.WriteByte(Ticket[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Address = reader.ReadUTF();
            Ports = new short[reader.ReadShort()];
            for (var i = 0; i < Ports.Length; i++)
            {
                Ports[i] = reader.ReadShort();
            }
            Ticket = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Ticket.Length; i++)
            {
                Ticket[i] = reader.ReadByte();
            }
        }
    }
}