namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaFightAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6279;

        public short FightId { get; set; }

        public bool Accept { get; set; }

        public GameRolePlayArenaFightAnswerMessage()
        {
        }

        public GameRolePlayArenaFightAnswerMessage(short fightId, bool accept)
        {
            FightId = fightId;
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            Accept = reader.ReadBoolean();
        }
    }
}