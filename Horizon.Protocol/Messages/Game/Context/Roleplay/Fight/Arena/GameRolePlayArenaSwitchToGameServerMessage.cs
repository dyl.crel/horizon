namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaSwitchToGameServerMessage : NetworkMessage
    {
        public override short Protocol => 6574;

        public bool ValidToken { get; set; }

        public byte[] Ticket { get; set; }

        public short HomeServerId { get; set; }

        public GameRolePlayArenaSwitchToGameServerMessage()
        {
        }

        public GameRolePlayArenaSwitchToGameServerMessage(bool validToken, byte[] ticket, short homeServerId)
        {
            ValidToken = validToken;
            Ticket = ticket;
            HomeServerId = homeServerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(ValidToken);
            writer.WriteVarInt(Ticket.Length);
            for (var i = 0; i < Ticket.Length; i++)
            {
                writer.WriteByte(Ticket[i]);
            }
            writer.WriteShort(HomeServerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ValidToken = reader.ReadBoolean();
            Ticket = new byte[reader.ReadVarInt()];
            for (var i = 0; i < Ticket.Length; i++)
            {
                Ticket[i] = reader.ReadByte();
            }
            HomeServerId = reader.ReadShort();
        }
    }
}