namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaLeagueRewardsMessage : NetworkMessage
    {
        public override short Protocol => 6785;

        public short SeasonId { get; set; }

        public short LeagueId { get; set; }

        public int LadderPosition { get; set; }

        public bool EndSeasonReward { get; set; }

        public GameRolePlayArenaLeagueRewardsMessage()
        {
        }

        public GameRolePlayArenaLeagueRewardsMessage(short seasonId, short leagueId, int ladderPosition, bool endSeasonReward)
        {
            SeasonId = seasonId;
            LeagueId = leagueId;
            LadderPosition = ladderPosition;
            EndSeasonReward = endSeasonReward;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SeasonId);
            writer.WriteVarShort(LeagueId);
            writer.WriteInt(LadderPosition);
            writer.WriteBoolean(EndSeasonReward);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SeasonId = reader.ReadVarShort();
            LeagueId = reader.ReadVarShort();
            LadderPosition = reader.ReadInt();
            EndSeasonReward = reader.ReadBoolean();
        }
    }
}