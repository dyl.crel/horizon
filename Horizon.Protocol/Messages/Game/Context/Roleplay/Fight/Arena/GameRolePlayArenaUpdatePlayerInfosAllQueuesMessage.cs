using Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage : GameRolePlayArenaUpdatePlayerInfosMessage
    {
        public override short Protocol => 6728;

        public ArenaRankInfos Team { get; set; }

        public ArenaRankInfos Duel { get; set; }

        public GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage()
        {
        }

        public GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage(ArenaRankInfos solo, ArenaRankInfos team, ArenaRankInfos duel) : base(solo)
        {
            Team = team;
            Duel = duel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Team.Serialize(writer);
            Duel.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Team = new ArenaRankInfos();
            Team.Deserialize(reader);
            Duel = new ArenaRankInfos();
            Duel.Deserialize(reader);
        }
    }
}