using Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaInvitationCandidatesAnswer : NetworkMessage
    {
        public override short Protocol => 6783;

        public LeagueFriendInformations[] Candidates { get; set; }

        public GameRolePlayArenaInvitationCandidatesAnswer()
        {
        }

        public GameRolePlayArenaInvitationCandidatesAnswer(LeagueFriendInformations[] candidates)
        {
            Candidates = candidates;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Candidates.Length);
            for (var i = 0; i < Candidates.Length; i++)
            {
                Candidates[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Candidates = new LeagueFriendInformations[reader.ReadShort()];
            for (var i = 0; i < Candidates.Length; i++)
            {
                Candidates[i] = new LeagueFriendInformations();
                Candidates[i].Deserialize(reader);
            }
        }
    }
}