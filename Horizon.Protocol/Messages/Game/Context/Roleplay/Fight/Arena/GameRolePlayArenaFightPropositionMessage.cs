namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight.Arena
{
    public class GameRolePlayArenaFightPropositionMessage : NetworkMessage
    {
        public override short Protocol => 6276;

        public short FightId { get; set; }

        public double[] AlliesId { get; set; }

        public short Duration { get; set; }

        public GameRolePlayArenaFightPropositionMessage()
        {
        }

        public GameRolePlayArenaFightPropositionMessage(short fightId, double[] alliesId, short duration)
        {
            FightId = fightId;
            AlliesId = alliesId;
            Duration = duration;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteShort(AlliesId.Length);
            for (var i = 0; i < AlliesId.Length; i++)
            {
                writer.WriteDouble(AlliesId[i]);
            }
            writer.WriteVarShort(Duration);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            AlliesId = new double[reader.ReadShort()];
            for (var i = 0; i < AlliesId.Length; i++)
            {
                AlliesId[i] = reader.ReadDouble();
            }
            Duration = reader.ReadVarShort();
        }
    }
}