namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayMonsterNotAngryAtPlayerMessage : NetworkMessage
    {
        public override short Protocol => 6742;

        public long PlayerId { get; set; }

        public double MonsterGroupId { get; set; }

        public GameRolePlayMonsterNotAngryAtPlayerMessage()
        {
        }

        public GameRolePlayMonsterNotAngryAtPlayerMessage(long playerId, double monsterGroupId)
        {
            PlayerId = playerId;
            MonsterGroupId = monsterGroupId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteDouble(MonsterGroupId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
            MonsterGroupId = reader.ReadDouble();
        }
    }
}