namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayPlayerFightFriendlyRequestedMessage : NetworkMessage
    {
        public override short Protocol => 5937;

        public short FightId { get; set; }

        public long SourceId { get; set; }

        public long TargetId { get; set; }

        public GameRolePlayPlayerFightFriendlyRequestedMessage()
        {
        }

        public GameRolePlayPlayerFightFriendlyRequestedMessage(short fightId, long sourceId, long targetId)
        {
            FightId = fightId;
            SourceId = sourceId;
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteVarLong(SourceId);
            writer.WriteVarLong(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            SourceId = reader.ReadVarLong();
            TargetId = reader.ReadVarLong();
        }
    }
}