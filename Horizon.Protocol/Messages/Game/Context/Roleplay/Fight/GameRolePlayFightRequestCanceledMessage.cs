namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayFightRequestCanceledMessage : NetworkMessage
    {
        public override short Protocol => 5822;

        public short FightId { get; set; }

        public double SourceId { get; set; }

        public double TargetId { get; set; }

        public GameRolePlayFightRequestCanceledMessage()
        {
        }

        public GameRolePlayFightRequestCanceledMessage(short fightId, double sourceId, double targetId)
        {
            FightId = fightId;
            SourceId = sourceId;
            TargetId = targetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteDouble(SourceId);
            writer.WriteDouble(TargetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            SourceId = reader.ReadDouble();
            TargetId = reader.ReadDouble();
        }
    }
}