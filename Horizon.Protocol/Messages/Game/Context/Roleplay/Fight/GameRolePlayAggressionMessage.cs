namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayAggressionMessage : NetworkMessage
    {
        public override short Protocol => 6073;

        public long AttackerId { get; set; }

        public long DefenderId { get; set; }

        public GameRolePlayAggressionMessage()
        {
        }

        public GameRolePlayAggressionMessage(long attackerId, long defenderId)
        {
            AttackerId = attackerId;
            DefenderId = defenderId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(AttackerId);
            writer.WriteVarLong(DefenderId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AttackerId = reader.ReadVarLong();
            DefenderId = reader.ReadVarLong();
        }
    }
}