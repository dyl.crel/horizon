using Horizon.Protocol.Types.Game.Context.Fight;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayShowChallengeMessage : NetworkMessage
    {
        public override short Protocol => 301;

        public FightCommonInformations CommonsInfos { get; set; }

        public GameRolePlayShowChallengeMessage()
        {
        }

        public GameRolePlayShowChallengeMessage(FightCommonInformations commonsInfos)
        {
            CommonsInfos = commonsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            CommonsInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CommonsInfos = new FightCommonInformations();
            CommonsInfos.Deserialize(reader);
        }
    }
}