namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayMonsterAngryAtPlayerMessage : NetworkMessage
    {
        public override short Protocol => 6741;

        public long PlayerId { get; set; }

        public double MonsterGroupId { get; set; }

        public double AngryStartTime { get; set; }

        public double AttackTime { get; set; }

        public GameRolePlayMonsterAngryAtPlayerMessage()
        {
        }

        public GameRolePlayMonsterAngryAtPlayerMessage(long playerId, double monsterGroupId, double angryStartTime, double attackTime)
        {
            PlayerId = playerId;
            MonsterGroupId = monsterGroupId;
            AngryStartTime = angryStartTime;
            AttackTime = attackTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteDouble(MonsterGroupId);
            writer.WriteDouble(AngryStartTime);
            writer.WriteDouble(AttackTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
            MonsterGroupId = reader.ReadDouble();
            AngryStartTime = reader.ReadDouble();
            AttackTime = reader.ReadDouble();
        }
    }
}