namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayAttackMonsterRequestMessage : NetworkMessage
    {
        public override short Protocol => 6191;

        public double MonsterGroupId { get; set; }

        public GameRolePlayAttackMonsterRequestMessage()
        {
        }

        public GameRolePlayAttackMonsterRequestMessage(double monsterGroupId)
        {
            MonsterGroupId = monsterGroupId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MonsterGroupId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MonsterGroupId = reader.ReadDouble();
        }
    }
}