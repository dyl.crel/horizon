namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayPlayerFightFriendlyAnsweredMessage : NetworkMessage
    {
        public override short Protocol => 5733;

        public short FightId { get; set; }

        public long SourceId { get; set; }

        public long TargetId { get; set; }

        public bool Accept { get; set; }

        public GameRolePlayPlayerFightFriendlyAnsweredMessage()
        {
        }

        public GameRolePlayPlayerFightFriendlyAnsweredMessage(short fightId, long sourceId, long targetId, bool accept)
        {
            FightId = fightId;
            SourceId = sourceId;
            TargetId = targetId;
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteVarLong(SourceId);
            writer.WriteVarLong(TargetId);
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            SourceId = reader.ReadVarLong();
            TargetId = reader.ReadVarLong();
            Accept = reader.ReadBoolean();
        }
    }
}