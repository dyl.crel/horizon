namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Fight
{
    public class GameRolePlayPlayerFightRequestMessage : NetworkMessage
    {
        public override short Protocol => 5731;

        public long TargetId { get; set; }

        public short TargetCellId { get; set; }

        public bool Friendly { get; set; }

        public GameRolePlayPlayerFightRequestMessage()
        {
        }

        public GameRolePlayPlayerFightRequestMessage(long targetId, short targetCellId, bool friendly)
        {
            TargetId = targetId;
            TargetCellId = targetCellId;
            Friendly = friendly;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(TargetId);
            writer.WriteShort(TargetCellId);
            writer.WriteBoolean(Friendly);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TargetId = reader.ReadVarLong();
            TargetCellId = reader.ReadShort();
            Friendly = reader.ReadBoolean();
        }
    }
}