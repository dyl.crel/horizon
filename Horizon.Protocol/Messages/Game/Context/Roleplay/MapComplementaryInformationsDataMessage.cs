using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class MapComplementaryInformationsDataMessage : NetworkMessage
    {
        public override short Protocol => 226;

        public short SubAreaId { get; set; }

        public double MapId { get; set; }

        public HouseInformations[] Houses { get; set; }

        public GameRolePlayActorInformations[] Actors { get; set; }

        public InteractiveElement[] InteractiveElements { get; set; }

        public StatedElement[] StatedElements { get; set; }

        public MapObstacle[] Obstacles { get; set; }

        public FightCommonInformations[] Fights { get; set; }

        public bool HasAggressiveMonsters { get; set; }

        public FightStartingPositions FightStartPositions { get; set; }

        public MapComplementaryInformationsDataMessage()
        {
        }

        public MapComplementaryInformationsDataMessage(short subAreaId, double mapId, HouseInformations[] houses, GameRolePlayActorInformations[] actors, InteractiveElement[] interactiveElements, StatedElement[] statedElements, MapObstacle[] obstacles, FightCommonInformations[] fights, bool hasAggressiveMonsters, FightStartingPositions fightStartPositions)
        {
            SubAreaId = subAreaId;
            MapId = mapId;
            Houses = houses;
            Actors = actors;
            InteractiveElements = interactiveElements;
            StatedElements = statedElements;
            Obstacles = obstacles;
            Fights = fights;
            HasAggressiveMonsters = hasAggressiveMonsters;
            FightStartPositions = fightStartPositions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteDouble(MapId);
            writer.WriteShort(Houses.Length);
            for (var i = 0; i < Houses.Length; i++)
            {
                writer.WriteShort(Houses[i].Protocol);
                Houses[i].Serialize(writer);
            }
            writer.WriteShort(Actors.Length);
            for (var i = 0; i < Actors.Length; i++)
            {
                writer.WriteShort(Actors[i].Protocol);
                Actors[i].Serialize(writer);
            }
            writer.WriteShort(InteractiveElements.Length);
            for (var i = 0; i < InteractiveElements.Length; i++)
            {
                writer.WriteShort(InteractiveElements[i].Protocol);
                InteractiveElements[i].Serialize(writer);
            }
            writer.WriteShort(StatedElements.Length);
            for (var i = 0; i < StatedElements.Length; i++)
            {
                StatedElements[i].Serialize(writer);
            }
            writer.WriteShort(Obstacles.Length);
            for (var i = 0; i < Obstacles.Length; i++)
            {
                Obstacles[i].Serialize(writer);
            }
            writer.WriteShort(Fights.Length);
            for (var i = 0; i < Fights.Length; i++)
            {
                Fights[i].Serialize(writer);
            }
            writer.WriteBoolean(HasAggressiveMonsters);
            FightStartPositions.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            MapId = reader.ReadDouble();
            Houses = new HouseInformations[reader.ReadShort()];
            for (var i = 0; i < Houses.Length; i++)
            {
                Houses[i] = ProtocolTypesManager.Instance<HouseInformations>(reader.ReadShort());
                Houses[i].Deserialize(reader);
            }
            Actors = new GameRolePlayActorInformations[reader.ReadShort()];
            for (var i = 0; i < Actors.Length; i++)
            {
                Actors[i] = ProtocolTypesManager.Instance<GameRolePlayActorInformations>(reader.ReadShort());
                Actors[i].Deserialize(reader);
            }
            InteractiveElements = new InteractiveElement[reader.ReadShort()];
            for (var i = 0; i < InteractiveElements.Length; i++)
            {
                InteractiveElements[i] = ProtocolTypesManager.Instance<InteractiveElement>(reader.ReadShort());
                InteractiveElements[i].Deserialize(reader);
            }
            StatedElements = new StatedElement[reader.ReadShort()];
            for (var i = 0; i < StatedElements.Length; i++)
            {
                StatedElements[i] = new StatedElement();
                StatedElements[i].Deserialize(reader);
            }
            Obstacles = new MapObstacle[reader.ReadShort()];
            for (var i = 0; i < Obstacles.Length; i++)
            {
                Obstacles[i] = new MapObstacle();
                Obstacles[i].Deserialize(reader);
            }
            Fights = new FightCommonInformations[reader.ReadShort()];
            for (var i = 0; i < Fights.Length; i++)
            {
                Fights[i] = new FightCommonInformations();
                Fights[i].Deserialize(reader);
            }
            HasAggressiveMonsters = reader.ReadBoolean();
            FightStartPositions = new FightStartingPositions();
            FightStartPositions.Deserialize(reader);
        }
    }
}