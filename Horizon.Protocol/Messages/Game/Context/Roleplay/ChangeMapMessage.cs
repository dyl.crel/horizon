namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class ChangeMapMessage : NetworkMessage
    {
        public override short Protocol => 221;

        public double MapId { get; set; }

        public bool Autopilot { get; set; }

        public ChangeMapMessage()
        {
        }

        public ChangeMapMessage(double mapId, bool autopilot)
        {
            MapId = mapId;
            Autopilot = autopilot;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteBoolean(Autopilot);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
            Autopilot = reader.ReadBoolean();
        }
    }
}