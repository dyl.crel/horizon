namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class HavenBagPackListMessage : NetworkMessage
    {
        public override short Protocol => 6620;

        public byte[] PackIds { get; set; }

        public HavenBagPackListMessage()
        {
        }

        public HavenBagPackListMessage(byte[] packIds)
        {
            PackIds = packIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PackIds.Length);
            for (var i = 0; i < PackIds.Length; i++)
            {
                writer.WriteByte(PackIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PackIds = new byte[reader.ReadShort()];
            for (var i = 0; i < PackIds.Length; i++)
            {
                PackIds[i] = reader.ReadByte();
            }
        }
    }
}