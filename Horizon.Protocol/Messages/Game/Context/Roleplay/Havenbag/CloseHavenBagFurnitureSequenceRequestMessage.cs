namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class CloseHavenBagFurnitureSequenceRequestMessage : NetworkMessage
    {
        public override short Protocol => 6621;

        public CloseHavenBagFurnitureSequenceRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}