namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class EditHavenBagCancelRequestMessage : NetworkMessage
    {
        public override short Protocol => 6619;

        public EditHavenBagCancelRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}