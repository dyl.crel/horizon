namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class RoomAvailableUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6630;

        public byte NbRoom { get; set; }

        public RoomAvailableUpdateMessage()
        {
        }

        public RoomAvailableUpdateMessage(byte nbRoom)
        {
            NbRoom = nbRoom;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(NbRoom);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NbRoom = reader.ReadByte();
        }
    }
}