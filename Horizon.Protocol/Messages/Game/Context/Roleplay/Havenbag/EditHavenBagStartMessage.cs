namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class EditHavenBagStartMessage : NetworkMessage
    {
        public override short Protocol => 6632;

        public EditHavenBagStartMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}