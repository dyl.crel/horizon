namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class OpenHavenBagFurnitureSequenceRequestMessage : NetworkMessage
    {
        public override short Protocol => 6635;

        public OpenHavenBagFurnitureSequenceRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}