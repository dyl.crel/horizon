namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class EnterHavenBagRequestMessage : NetworkMessage
    {
        public override short Protocol => 6636;

        public long HavenBagOwner { get; set; }

        public EnterHavenBagRequestMessage()
        {
        }

        public EnterHavenBagRequestMessage(long havenBagOwner)
        {
            HavenBagOwner = havenBagOwner;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(HavenBagOwner);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HavenBagOwner = reader.ReadVarLong();
        }
    }
}