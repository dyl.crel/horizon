using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class HavenBagFurnituresMessage : NetworkMessage
    {
        public override short Protocol => 6634;

        public HavenBagFurnitureInformation[] FurnituresInfos { get; set; }

        public HavenBagFurnituresMessage()
        {
        }

        public HavenBagFurnituresMessage(HavenBagFurnitureInformation[] furnituresInfos)
        {
            FurnituresInfos = furnituresInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(FurnituresInfos.Length);
            for (var i = 0; i < FurnituresInfos.Length; i++)
            {
                FurnituresInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FurnituresInfos = new HavenBagFurnitureInformation[reader.ReadShort()];
            for (var i = 0; i < FurnituresInfos.Length; i++)
            {
                FurnituresInfos[i] = new HavenBagFurnitureInformation();
                FurnituresInfos[i].Deserialize(reader);
            }
        }
    }
}