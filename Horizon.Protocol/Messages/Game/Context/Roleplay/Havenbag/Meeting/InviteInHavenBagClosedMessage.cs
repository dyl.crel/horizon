using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class InviteInHavenBagClosedMessage : NetworkMessage
    {
        public override short Protocol => 6645;

        public CharacterMinimalInformations HostInformations { get; set; }

        public InviteInHavenBagClosedMessage()
        {
        }

        public InviteInHavenBagClosedMessage(CharacterMinimalInformations hostInformations)
        {
            HostInformations = hostInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            HostInformations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HostInformations = new CharacterMinimalInformations();
            HostInformations.Deserialize(reader);
        }
    }
}