namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class HavenBagPermissionsUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6713;

        public int Permissions { get; set; }

        public HavenBagPermissionsUpdateMessage()
        {
        }

        public HavenBagPermissionsUpdateMessage(int permissions)
        {
            Permissions = permissions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Permissions);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Permissions = reader.ReadInt();
        }
    }
}