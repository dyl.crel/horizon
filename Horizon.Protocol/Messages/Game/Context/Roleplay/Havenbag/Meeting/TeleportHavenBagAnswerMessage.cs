namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class TeleportHavenBagAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6646;

        public bool Accept { get; set; }

        public TeleportHavenBagAnswerMessage()
        {
        }

        public TeleportHavenBagAnswerMessage(bool accept)
        {
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Accept = reader.ReadBoolean();
        }
    }
}