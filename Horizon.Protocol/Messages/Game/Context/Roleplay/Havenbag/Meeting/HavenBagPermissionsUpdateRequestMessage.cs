namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class HavenBagPermissionsUpdateRequestMessage : NetworkMessage
    {
        public override short Protocol => 6714;

        public int Permissions { get; set; }

        public HavenBagPermissionsUpdateRequestMessage()
        {
        }

        public HavenBagPermissionsUpdateRequestMessage(int permissions)
        {
            Permissions = permissions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Permissions);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Permissions = reader.ReadInt();
        }
    }
}