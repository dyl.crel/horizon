using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class InviteInHavenBagMessage : NetworkMessage
    {
        public override short Protocol => 6642;

        public CharacterMinimalInformations GuestInformations { get; set; }

        public bool Accept { get; set; }

        public InviteInHavenBagMessage()
        {
        }

        public InviteInHavenBagMessage(CharacterMinimalInformations guestInformations, bool accept)
        {
            GuestInformations = guestInformations;
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            GuestInformations.Serialize(writer);
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuestInformations = new CharacterMinimalInformations();
            GuestInformations.Deserialize(reader);
            Accept = reader.ReadBoolean();
        }
    }
}