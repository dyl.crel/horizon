using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class InviteInHavenBagOfferMessage : NetworkMessage
    {
        public override short Protocol => 6643;

        public CharacterMinimalInformations HostInformations { get; set; }

        public int TimeLeftBeforeCancel { get; set; }

        public InviteInHavenBagOfferMessage()
        {
        }

        public InviteInHavenBagOfferMessage(CharacterMinimalInformations hostInformations, int timeLeftBeforeCancel)
        {
            HostInformations = hostInformations;
            TimeLeftBeforeCancel = timeLeftBeforeCancel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            HostInformations.Serialize(writer);
            writer.WriteVarInt(TimeLeftBeforeCancel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HostInformations = new CharacterMinimalInformations();
            HostInformations.Deserialize(reader);
            TimeLeftBeforeCancel = reader.ReadVarInt();
        }
    }
}