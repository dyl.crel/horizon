namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag.Meeting
{
    public class TeleportHavenBagRequestMessage : NetworkMessage
    {
        public override short Protocol => 6647;

        public long GuestId { get; set; }

        public TeleportHavenBagRequestMessage()
        {
        }

        public TeleportHavenBagRequestMessage(long guestId)
        {
            GuestId = guestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(GuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuestId = reader.ReadVarLong();
        }
    }
}