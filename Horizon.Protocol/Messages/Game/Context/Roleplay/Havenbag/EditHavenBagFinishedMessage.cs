namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class EditHavenBagFinishedMessage : NetworkMessage
    {
        public override short Protocol => 6628;

        public EditHavenBagFinishedMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}