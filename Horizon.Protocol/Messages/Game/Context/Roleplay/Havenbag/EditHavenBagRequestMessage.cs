namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class EditHavenBagRequestMessage : NetworkMessage
    {
        public override short Protocol => 6626;

        public EditHavenBagRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}