namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class HavenBagFurnituresRequestMessage : NetworkMessage
    {
        public override short Protocol => 6637;

        public short[] CellIds { get; set; }

        public int[] FunitureIds { get; set; }

        public byte[] Orientations { get; set; }

        public HavenBagFurnituresRequestMessage()
        {
        }

        public HavenBagFurnituresRequestMessage(short[] cellIds, int[] funitureIds, byte[] orientations)
        {
            CellIds = cellIds;
            FunitureIds = funitureIds;
            Orientations = orientations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CellIds.Length);
            for (var i = 0; i < CellIds.Length; i++)
            {
                writer.WriteVarShort(CellIds[i]);
            }
            writer.WriteShort(FunitureIds.Length);
            for (var i = 0; i < FunitureIds.Length; i++)
            {
                writer.WriteInt(FunitureIds[i]);
            }
            writer.WriteShort(Orientations.Length);
            for (var i = 0; i < Orientations.Length; i++)
            {
                writer.WriteByte(Orientations[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellIds = new short[reader.ReadShort()];
            for (var i = 0; i < CellIds.Length; i++)
            {
                CellIds[i] = reader.ReadVarShort();
            }
            FunitureIds = new int[reader.ReadShort()];
            for (var i = 0; i < FunitureIds.Length; i++)
            {
                FunitureIds[i] = reader.ReadInt();
            }
            Orientations = new byte[reader.ReadShort()];
            for (var i = 0; i < Orientations.Length; i++)
            {
                Orientations[i] = reader.ReadByte();
            }
        }
    }
}