namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class ChangeHavenBagRoomRequestMessage : NetworkMessage
    {
        public override short Protocol => 6638;

        public byte RoomId { get; set; }

        public ChangeHavenBagRoomRequestMessage()
        {
        }

        public ChangeHavenBagRoomRequestMessage(byte roomId)
        {
            RoomId = roomId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(RoomId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RoomId = reader.ReadByte();
        }
    }
}