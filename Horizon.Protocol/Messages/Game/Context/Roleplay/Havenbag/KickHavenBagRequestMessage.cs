namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class KickHavenBagRequestMessage : NetworkMessage
    {
        public override short Protocol => 6652;

        public long GuestId { get; set; }

        public KickHavenBagRequestMessage()
        {
        }

        public KickHavenBagRequestMessage(long guestId)
        {
            GuestId = guestId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(GuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuestId = reader.ReadVarLong();
        }
    }
}