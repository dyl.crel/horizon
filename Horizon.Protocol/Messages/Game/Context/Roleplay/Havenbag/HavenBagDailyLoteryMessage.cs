namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class HavenBagDailyLoteryMessage : NetworkMessage
    {
        public override short Protocol => 6644;

        public byte ReturnType { get; set; }

        public string GameActionId { get; set; }

        public HavenBagDailyLoteryMessage()
        {
        }

        public HavenBagDailyLoteryMessage(byte returnType, string gameActionId)
        {
            ReturnType = returnType;
            GameActionId = gameActionId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ReturnType);
            writer.WriteUTF(GameActionId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ReturnType = reader.ReadByte();
            GameActionId = reader.ReadUTF();
        }
    }
}