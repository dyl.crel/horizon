namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class ChangeThemeRequestMessage : NetworkMessage
    {
        public override short Protocol => 6639;

        public byte Theme { get; set; }

        public ChangeThemeRequestMessage()
        {
        }

        public ChangeThemeRequestMessage(byte theme)
        {
            Theme = theme;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Theme);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Theme = reader.ReadByte();
        }
    }
}