namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Havenbag
{
    public class ExitHavenBagRequestMessage : NetworkMessage
    {
        public override short Protocol => 6631;

        public ExitHavenBagRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}