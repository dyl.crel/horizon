using Horizon.Protocol.Types.Game.House;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HousePropertiesMessage : NetworkMessage
    {
        public override short Protocol => 5734;

        public int HouseId { get; set; }

        public int[] DoorsOnMap { get; set; }

        public HouseInstanceInformations Properties { get; set; }

        public HousePropertiesMessage()
        {
        }

        public HousePropertiesMessage(int houseId, int[] doorsOnMap, HouseInstanceInformations properties)
        {
            HouseId = houseId;
            DoorsOnMap = doorsOnMap;
            Properties = properties;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteShort(DoorsOnMap.Length);
            for (var i = 0; i < DoorsOnMap.Length; i++)
            {
                writer.WriteInt(DoorsOnMap[i]);
            }
            writer.WriteShort(Properties.Protocol);
            Properties.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            DoorsOnMap = new int[reader.ReadShort()];
            for (var i = 0; i < DoorsOnMap.Length; i++)
            {
                DoorsOnMap[i] = reader.ReadInt();
            }
            Properties = ProtocolTypesManager.Instance<HouseInstanceInformations>(reader.ReadShort());
            Properties.Deserialize(reader);
        }
    }
}