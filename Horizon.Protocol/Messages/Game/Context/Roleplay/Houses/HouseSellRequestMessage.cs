namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseSellRequestMessage : NetworkMessage
    {
        public override short Protocol => 5697;

        public int InstanceId { get; set; }

        public long Amount { get; set; }

        public bool ForSale { get; set; }

        public HouseSellRequestMessage()
        {
        }

        public HouseSellRequestMessage(int instanceId, long amount, bool forSale)
        {
            InstanceId = instanceId;
            Amount = amount;
            ForSale = forSale;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(InstanceId);
            writer.WriteVarLong(Amount);
            writer.WriteBoolean(ForSale);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InstanceId = reader.ReadInt();
            Amount = reader.ReadVarLong();
            ForSale = reader.ReadBoolean();
        }
    }
}