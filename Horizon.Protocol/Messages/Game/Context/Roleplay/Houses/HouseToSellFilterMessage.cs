namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseToSellFilterMessage : NetworkMessage
    {
        public override short Protocol => 6137;

        public int AreaId { get; set; }

        public byte AtLeastNbRoom { get; set; }

        public byte AtLeastNbChest { get; set; }

        public short SkillRequested { get; set; }

        public long MaxPrice { get; set; }

        public byte OrderBy { get; set; }

        public HouseToSellFilterMessage()
        {
        }

        public HouseToSellFilterMessage(int areaId, byte atLeastNbRoom, byte atLeastNbChest, short skillRequested, long maxPrice, byte orderBy)
        {
            AreaId = areaId;
            AtLeastNbRoom = atLeastNbRoom;
            AtLeastNbChest = atLeastNbChest;
            SkillRequested = skillRequested;
            MaxPrice = maxPrice;
            OrderBy = orderBy;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AreaId);
            writer.WriteByte(AtLeastNbRoom);
            writer.WriteByte(AtLeastNbChest);
            writer.WriteVarShort(SkillRequested);
            writer.WriteVarLong(MaxPrice);
            writer.WriteByte(OrderBy);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AreaId = reader.ReadInt();
            AtLeastNbRoom = reader.ReadByte();
            AtLeastNbChest = reader.ReadByte();
            SkillRequested = reader.ReadVarShort();
            MaxPrice = reader.ReadVarLong();
            OrderBy = reader.ReadByte();
        }
    }
}