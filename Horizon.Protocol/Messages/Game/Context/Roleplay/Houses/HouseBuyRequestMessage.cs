namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseBuyRequestMessage : NetworkMessage
    {
        public override short Protocol => 5738;

        public long ProposedPrice { get; set; }

        public HouseBuyRequestMessage()
        {
        }

        public HouseBuyRequestMessage(long proposedPrice)
        {
            ProposedPrice = proposedPrice;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(ProposedPrice);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ProposedPrice = reader.ReadVarLong();
        }
    }
}