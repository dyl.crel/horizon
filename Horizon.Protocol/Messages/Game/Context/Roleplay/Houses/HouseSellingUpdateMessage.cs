namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseSellingUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6727;

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public bool SecondHand { get; set; }

        public long RealPrice { get; set; }

        public string BuyerName { get; set; }

        public HouseSellingUpdateMessage()
        {
        }

        public HouseSellingUpdateMessage(int houseId, int instanceId, bool secondHand, long realPrice, string buyerName)
        {
            HouseId = houseId;
            InstanceId = instanceId;
            SecondHand = secondHand;
            RealPrice = realPrice;
            BuyerName = buyerName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            writer.WriteVarLong(RealPrice);
            writer.WriteUTF(BuyerName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
            SecondHand = reader.ReadBoolean();
            RealPrice = reader.ReadVarLong();
            BuyerName = reader.ReadUTF();
        }
    }
}