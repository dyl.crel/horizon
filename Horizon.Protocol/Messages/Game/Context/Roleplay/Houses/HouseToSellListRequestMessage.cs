namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseToSellListRequestMessage : NetworkMessage
    {
        public override short Protocol => 6139;

        public short PageIndex { get; set; }

        public HouseToSellListRequestMessage()
        {
        }

        public HouseToSellListRequestMessage(short pageIndex)
        {
            PageIndex = pageIndex;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(PageIndex);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PageIndex = reader.ReadVarShort();
        }
    }
}