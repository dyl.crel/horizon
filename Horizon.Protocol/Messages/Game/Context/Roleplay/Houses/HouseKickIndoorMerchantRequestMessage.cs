namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseKickIndoorMerchantRequestMessage : NetworkMessage
    {
        public override short Protocol => 5661;

        public short CellId { get; set; }

        public HouseKickIndoorMerchantRequestMessage()
        {
        }

        public HouseKickIndoorMerchantRequestMessage(short cellId)
        {
            CellId = cellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
        }
    }
}