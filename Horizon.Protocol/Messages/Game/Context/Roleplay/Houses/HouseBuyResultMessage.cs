namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseBuyResultMessage : NetworkMessage
    {
        public override short Protocol => 5735;

        public bool SecondHand { get; set; }

        public bool Bought { get; set; }

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public long RealPrice { get; set; }

        public HouseBuyResultMessage()
        {
        }

        public HouseBuyResultMessage(bool secondHand, bool bought, int houseId, int instanceId, long realPrice)
        {
            SecondHand = secondHand;
            Bought = bought;
            HouseId = houseId;
            InstanceId = instanceId;
            RealPrice = realPrice;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, SecondHand);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Bought);
            writer.WriteByte(flag0);
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteVarLong(RealPrice);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            SecondHand = BooleanByteWrapper.GetFlag(flag0, 0);
            Bought = BooleanByteWrapper.GetFlag(flag0, 1);
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
            RealPrice = reader.ReadVarLong();
        }
    }
}