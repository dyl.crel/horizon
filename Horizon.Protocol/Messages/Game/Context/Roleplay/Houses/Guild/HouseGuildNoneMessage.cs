namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses.Guild
{
    public class HouseGuildNoneMessage : NetworkMessage
    {
        public override short Protocol => 5701;

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public bool SecondHand { get; set; }

        public HouseGuildNoneMessage()
        {
        }

        public HouseGuildNoneMessage(int houseId, int instanceId, bool secondHand)
        {
            HouseId = houseId;
            InstanceId = instanceId;
            SecondHand = secondHand;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
            SecondHand = reader.ReadBoolean();
        }
    }
}