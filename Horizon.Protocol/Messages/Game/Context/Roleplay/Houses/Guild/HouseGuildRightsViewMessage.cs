namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses.Guild
{
    public class HouseGuildRightsViewMessage : NetworkMessage
    {
        public override short Protocol => 5700;

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public HouseGuildRightsViewMessage()
        {
        }

        public HouseGuildRightsViewMessage(int houseId, int instanceId)
        {
            HouseId = houseId;
            InstanceId = instanceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
        }
    }
}