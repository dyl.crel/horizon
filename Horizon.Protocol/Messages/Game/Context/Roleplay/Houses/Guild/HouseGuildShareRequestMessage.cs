namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses.Guild
{
    public class HouseGuildShareRequestMessage : NetworkMessage
    {
        public override short Protocol => 5704;

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public bool Enable { get; set; }

        public int Rights { get; set; }

        public HouseGuildShareRequestMessage()
        {
        }

        public HouseGuildShareRequestMessage(int houseId, int instanceId, bool enable, int rights)
        {
            HouseId = houseId;
            InstanceId = instanceId;
            Enable = enable;
            Rights = rights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(Enable);
            writer.WriteVarInt(Rights);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
            Enable = reader.ReadBoolean();
            Rights = reader.ReadVarInt();
        }
    }
}