using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses.Guild
{
    public class HouseGuildRightsMessage : NetworkMessage
    {
        public override short Protocol => 5703;

        public int HouseId { get; set; }

        public int InstanceId { get; set; }

        public bool SecondHand { get; set; }

        public GuildInformations GuildInfo { get; set; }

        public int Rights { get; set; }

        public HouseGuildRightsMessage()
        {
        }

        public HouseGuildRightsMessage(int houseId, int instanceId, bool secondHand, GuildInformations guildInfo, int rights)
        {
            HouseId = houseId;
            InstanceId = instanceId;
            SecondHand = secondHand;
            GuildInfo = guildInfo;
            Rights = rights;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            GuildInfo.Serialize(writer);
            writer.WriteVarInt(Rights);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            InstanceId = reader.ReadInt();
            SecondHand = reader.ReadBoolean();
            GuildInfo = new GuildInformations();
            GuildInfo.Deserialize(reader);
            Rights = reader.ReadVarInt();
        }
    }
}