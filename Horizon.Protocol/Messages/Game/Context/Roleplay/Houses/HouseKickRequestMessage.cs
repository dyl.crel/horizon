namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseKickRequestMessage : NetworkMessage
    {
        public override short Protocol => 5698;

        public long Id { get; set; }

        public HouseKickRequestMessage()
        {
        }

        public HouseKickRequestMessage(long id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarLong();
        }
    }
}