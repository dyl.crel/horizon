using Horizon.Protocol.Types.Game.House;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseToSellListMessage : NetworkMessage
    {
        public override short Protocol => 6140;

        public short PageIndex { get; set; }

        public short TotalPage { get; set; }

        public HouseInformationsForSell[] HouseList { get; set; }

        public HouseToSellListMessage()
        {
        }

        public HouseToSellListMessage(short pageIndex, short totalPage, HouseInformationsForSell[] houseList)
        {
            PageIndex = pageIndex;
            TotalPage = totalPage;
            HouseList = houseList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(PageIndex);
            writer.WriteVarShort(TotalPage);
            writer.WriteShort(HouseList.Length);
            for (var i = 0; i < HouseList.Length; i++)
            {
                HouseList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PageIndex = reader.ReadVarShort();
            TotalPage = reader.ReadVarShort();
            HouseList = new HouseInformationsForSell[reader.ReadShort()];
            for (var i = 0; i < HouseList.Length; i++)
            {
                HouseList[i] = new HouseInformationsForSell();
                HouseList[i].Deserialize(reader);
            }
        }
    }
}