using Horizon.Protocol.Types.Game.House;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class AccountHouseMessage : NetworkMessage
    {
        public override short Protocol => 6315;

        public AccountHouseInformations[] Houses { get; set; }

        public AccountHouseMessage()
        {
        }

        public AccountHouseMessage(AccountHouseInformations[] houses)
        {
            Houses = houses;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Houses.Length);
            for (var i = 0; i < Houses.Length; i++)
            {
                Houses[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Houses = new AccountHouseInformations[reader.ReadShort()];
            for (var i = 0; i < Houses.Length; i++)
            {
                Houses[i] = new AccountHouseInformations();
                Houses[i].Deserialize(reader);
            }
        }
    }
}