namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseSellFromInsideRequestMessage : HouseSellRequestMessage
    {
        public override short Protocol => 5884;

        public HouseSellFromInsideRequestMessage()
        {
        }

        public HouseSellFromInsideRequestMessage(int instanceId, long amount, bool forSale) : base(instanceId, amount, forSale)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}