using Horizon.Protocol.Messages.Game.Context.Roleplay.Lockable;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Houses
{
    public class HouseLockFromInsideRequestMessage : LockableChangeCodeMessage
    {
        public override short Protocol => 5885;

        public HouseLockFromInsideRequestMessage()
        {
        }

        public HouseLockFromInsideRequestMessage(string code) : base(code)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}