namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Purchasable
{
    public class PurchasableDialogMessage : NetworkMessage
    {
        public override short Protocol => 5739;

        public bool BuyOrSell { get; set; }

        public bool SecondHand { get; set; }

        public double PurchasableId { get; set; }

        public int PurchasableInstanceId { get; set; }

        public long Price { get; set; }

        public PurchasableDialogMessage()
        {
        }

        public PurchasableDialogMessage(bool buyOrSell, bool secondHand, double purchasableId, int purchasableInstanceId, long price)
        {
            BuyOrSell = buyOrSell;
            SecondHand = secondHand;
            PurchasableId = purchasableId;
            PurchasableInstanceId = purchasableInstanceId;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, BuyOrSell);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, SecondHand);
            writer.WriteByte(flag0);
            writer.WriteDouble(PurchasableId);
            writer.WriteInt(PurchasableInstanceId);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            BuyOrSell = BooleanByteWrapper.GetFlag(flag0, 0);
            SecondHand = BooleanByteWrapper.GetFlag(flag0, 1);
            PurchasableId = reader.ReadDouble();
            PurchasableInstanceId = reader.ReadInt();
            Price = reader.ReadVarLong();
        }
    }
}