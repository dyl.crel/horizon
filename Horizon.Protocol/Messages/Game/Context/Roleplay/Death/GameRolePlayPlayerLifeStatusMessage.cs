namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Death
{
    public class GameRolePlayPlayerLifeStatusMessage : NetworkMessage
    {
        public override short Protocol => 5996;

        public byte State { get; set; }

        public double PhenixMapId { get; set; }

        public GameRolePlayPlayerLifeStatusMessage()
        {
        }

        public GameRolePlayPlayerLifeStatusMessage(byte state, double phenixMapId)
        {
            State = state;
            PhenixMapId = phenixMapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(State);
            writer.WriteDouble(PhenixMapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            State = reader.ReadByte();
            PhenixMapId = reader.ReadDouble();
        }
    }
}