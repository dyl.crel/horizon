namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Death
{
    public class WarnOnPermaDeathMessage : NetworkMessage
    {
        public override short Protocol => 6512;

        public bool Enable { get; set; }

        public WarnOnPermaDeathMessage()
        {
        }

        public WarnOnPermaDeathMessage(bool enable)
        {
            Enable = enable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enable = reader.ReadBoolean();
        }
    }
}