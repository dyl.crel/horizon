namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Death
{
    public class GameRolePlayGameOverMessage : NetworkMessage
    {
        public override short Protocol => 746;

        public GameRolePlayGameOverMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}