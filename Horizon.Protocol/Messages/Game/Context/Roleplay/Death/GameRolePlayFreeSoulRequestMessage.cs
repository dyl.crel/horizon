namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Death
{
    public class GameRolePlayFreeSoulRequestMessage : NetworkMessage
    {
        public override short Protocol => 745;

        public GameRolePlayFreeSoulRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}