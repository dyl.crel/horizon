namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachTeleportRequestMessage : NetworkMessage
    {
        public override short Protocol => 6817;

        public BreachTeleportRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}