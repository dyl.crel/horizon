using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachBonusMessage : NetworkMessage
    {
        public override short Protocol => 6800;

        public ObjectEffectInteger Bonus { get; set; }

        public BreachBonusMessage()
        {
        }

        public BreachBonusMessage(ObjectEffectInteger bonus)
        {
            Bonus = bonus;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Bonus.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Bonus = new ObjectEffectInteger();
            Bonus.Deserialize(reader);
        }
    }
}