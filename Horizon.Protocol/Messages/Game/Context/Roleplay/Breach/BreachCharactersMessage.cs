namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachCharactersMessage : NetworkMessage
    {
        public override short Protocol => 6811;

        public long[] Characters { get; set; }

        public BreachCharactersMessage()
        {
        }

        public BreachCharactersMessage(long[] characters)
        {
            Characters = characters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Characters.Length);
            for (var i = 0; i < Characters.Length; i++)
            {
                writer.WriteVarLong(Characters[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Characters = new long[reader.ReadShort()];
            for (var i = 0; i < Characters.Length; i++)
            {
                Characters[i] = reader.ReadVarLong();
            }
        }
    }
}