namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachExitRequestMessage : NetworkMessage
    {
        public override short Protocol => 6815;

        public BreachExitRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}