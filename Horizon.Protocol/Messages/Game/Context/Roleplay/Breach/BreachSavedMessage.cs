namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachSavedMessage : NetworkMessage
    {
        public override short Protocol => 6798;

        public bool Saved { get; set; }

        public BreachSavedMessage()
        {
        }

        public BreachSavedMessage(bool saved)
        {
            Saved = saved;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Saved);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Saved = reader.ReadBoolean();
        }
    }
}