namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachInvitationRequestMessage : NetworkMessage
    {
        public override short Protocol => 6794;

        public long Guest { get; set; }

        public BreachInvitationRequestMessage()
        {
        }

        public BreachInvitationRequestMessage(long guest)
        {
            Guest = guest;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Guest);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Guest = reader.ReadVarLong();
        }
    }
}