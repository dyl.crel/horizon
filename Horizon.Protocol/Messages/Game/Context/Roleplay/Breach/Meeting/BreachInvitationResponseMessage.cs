using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachInvitationResponseMessage : NetworkMessage
    {
        public override short Protocol => 6792;

        public CharacterMinimalInformations Guest { get; set; }

        public bool Accept { get; set; }

        public BreachInvitationResponseMessage()
        {
        }

        public BreachInvitationResponseMessage(CharacterMinimalInformations guest, bool accept)
        {
            Guest = guest;
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Guest.Serialize(writer);
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Guest = new CharacterMinimalInformations();
            Guest.Deserialize(reader);
            Accept = reader.ReadBoolean();
        }
    }
}