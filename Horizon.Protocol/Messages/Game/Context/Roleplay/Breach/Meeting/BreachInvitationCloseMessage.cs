using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachInvitationCloseMessage : NetworkMessage
    {
        public override short Protocol => 6790;

        public CharacterMinimalInformations Host { get; set; }

        public BreachInvitationCloseMessage()
        {
        }

        public BreachInvitationCloseMessage(CharacterMinimalInformations host)
        {
            Host = host;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Host.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Host = new CharacterMinimalInformations();
            Host.Deserialize(reader);
        }
    }
}