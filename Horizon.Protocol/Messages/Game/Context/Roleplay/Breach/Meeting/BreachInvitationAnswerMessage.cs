namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachInvitationAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6795;

        public bool Accept { get; set; }

        public BreachInvitationAnswerMessage()
        {
        }

        public BreachInvitationAnswerMessage(bool accept)
        {
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Accept = reader.ReadBoolean();
        }
    }
}