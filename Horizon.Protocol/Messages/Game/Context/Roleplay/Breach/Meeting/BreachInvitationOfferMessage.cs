using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachInvitationOfferMessage : NetworkMessage
    {
        public override short Protocol => 6805;

        public CharacterMinimalInformations Host { get; set; }

        public int TimeLeftBeforeCancel { get; set; }

        public BreachInvitationOfferMessage()
        {
        }

        public BreachInvitationOfferMessage(CharacterMinimalInformations host, int timeLeftBeforeCancel)
        {
            Host = host;
            TimeLeftBeforeCancel = timeLeftBeforeCancel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Host.Serialize(writer);
            writer.WriteVarInt(TimeLeftBeforeCancel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Host = new CharacterMinimalInformations();
            Host.Deserialize(reader);
            TimeLeftBeforeCancel = reader.ReadVarInt();
        }
    }
}