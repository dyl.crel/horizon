using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachKickResponseMessage : NetworkMessage
    {
        public override short Protocol => 6789;

        public CharacterMinimalInformations Target { get; set; }

        public bool Kicked { get; set; }

        public BreachKickResponseMessage()
        {
        }

        public BreachKickResponseMessage(CharacterMinimalInformations target, bool kicked)
        {
            Target = target;
            Kicked = kicked;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Target.Serialize(writer);
            writer.WriteBoolean(Kicked);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Target = new CharacterMinimalInformations();
            Target.Deserialize(reader);
            Kicked = reader.ReadBoolean();
        }
    }
}