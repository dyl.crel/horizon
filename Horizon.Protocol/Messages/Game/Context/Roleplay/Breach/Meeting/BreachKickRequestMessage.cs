namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Meeting
{
    public class BreachKickRequestMessage : NetworkMessage
    {
        public override short Protocol => 6804;

        public long Target { get; set; }

        public BreachKickRequestMessage()
        {
        }

        public BreachKickRequestMessage(long target)
        {
            Target = target;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Target);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Target = reader.ReadVarLong();
        }
    }
}