namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachErosionMessage : NetworkMessage
    {
        public override short Protocol => 6802;

        public int Erosion { get; set; }

        public BreachErosionMessage()
        {
        }

        public BreachErosionMessage(int erosion)
        {
            Erosion = erosion;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Erosion);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Erosion = reader.ReadVarInt();
        }
    }
}