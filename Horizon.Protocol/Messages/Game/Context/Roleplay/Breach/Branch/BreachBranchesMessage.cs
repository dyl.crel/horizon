using Horizon.Protocol.Types.Game.Context.Roleplay.Breach;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Branch
{
    public class BreachBranchesMessage : NetworkMessage
    {
        public override short Protocol => 6812;

        public ExtendedBreachBranch[] Branches { get; set; }

        public BreachBranchesMessage()
        {
        }

        public BreachBranchesMessage(ExtendedBreachBranch[] branches)
        {
            Branches = branches;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Branches.Length);
            for (var i = 0; i < Branches.Length; i++)
            {
                Branches[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Branches = new ExtendedBreachBranch[reader.ReadShort()];
            for (var i = 0; i < Branches.Length; i++)
            {
                Branches[i] = new ExtendedBreachBranch();
                Branches[i].Deserialize(reader);
            }
        }
    }
}