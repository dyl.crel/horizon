namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Reward
{
    public class BreachRewardBoughtMessage : NetworkMessage
    {
        public override short Protocol => 6797;

        public int Id { get; set; }

        public bool Bought { get; set; }

        public BreachRewardBoughtMessage()
        {
        }

        public BreachRewardBoughtMessage(int id, bool bought)
        {
            Id = id;
            Bought = bought;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Id);
            writer.WriteBoolean(Bought);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarInt();
            Bought = reader.ReadBoolean();
        }
    }
}