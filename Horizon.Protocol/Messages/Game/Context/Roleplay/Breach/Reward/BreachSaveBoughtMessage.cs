namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Reward
{
    public class BreachSaveBoughtMessage : NetworkMessage
    {
        public override short Protocol => 6788;

        public bool Bought { get; set; }

        public BreachSaveBoughtMessage()
        {
        }

        public BreachSaveBoughtMessage(bool bought)
        {
            Bought = bought;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Bought);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Bought = reader.ReadBoolean();
        }
    }
}