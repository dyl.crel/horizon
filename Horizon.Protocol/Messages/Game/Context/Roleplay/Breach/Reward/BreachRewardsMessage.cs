using Horizon.Protocol.Types.Game.Context.Roleplay.Breach;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Reward
{
    public class BreachRewardsMessage : NetworkMessage
    {
        public override short Protocol => 6813;

        public BreachReward[] Rewards { get; set; }

        public BreachReward Save { get; set; }

        public BreachRewardsMessage()
        {
        }

        public BreachRewardsMessage(BreachReward[] rewards, BreachReward save)
        {
            Rewards = rewards;
            Save = save;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Rewards.Length);
            for (var i = 0; i < Rewards.Length; i++)
            {
                Rewards[i].Serialize(writer);
            }
            Save.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Rewards = new BreachReward[reader.ReadShort()];
            for (var i = 0; i < Rewards.Length; i++)
            {
                Rewards[i] = new BreachReward();
                Rewards[i].Deserialize(reader);
            }
            Save = new BreachReward();
            Save.Deserialize(reader);
        }
    }
}