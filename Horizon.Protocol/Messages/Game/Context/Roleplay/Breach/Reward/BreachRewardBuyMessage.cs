namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Reward
{
    public class BreachRewardBuyMessage : NetworkMessage
    {
        public override short Protocol => 6803;

        public int Id { get; set; }

        public BreachRewardBuyMessage()
        {
        }

        public BreachRewardBuyMessage(int id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarInt();
        }
    }
}