namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach.Reward
{
    public class BreachSaveBuyMessage : NetworkMessage
    {
        public override short Protocol => 6787;

        public BreachSaveBuyMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}