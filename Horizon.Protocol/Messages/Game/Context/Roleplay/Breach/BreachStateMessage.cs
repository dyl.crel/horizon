using Horizon.Protocol.Types.Game.Character;
using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachStateMessage : NetworkMessage
    {
        public override short Protocol => 6799;

        public CharacterMinimalInformations Owner { get; set; }

        public ObjectEffectInteger[] Bonuses { get; set; }

        public int Erosion { get; set; }

        public int Bugdet { get; set; }

        public bool Saved { get; set; }

        public BreachStateMessage()
        {
        }

        public BreachStateMessage(CharacterMinimalInformations owner, ObjectEffectInteger[] bonuses, int erosion, int bugdet, bool saved)
        {
            Owner = owner;
            Bonuses = bonuses;
            Erosion = erosion;
            Bugdet = bugdet;
            Saved = saved;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Owner.Serialize(writer);
            writer.WriteShort(Bonuses.Length);
            for (var i = 0; i < Bonuses.Length; i++)
            {
                Bonuses[i].Serialize(writer);
            }
            writer.WriteVarInt(Erosion);
            writer.WriteVarInt(Bugdet);
            writer.WriteBoolean(Saved);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Owner = new CharacterMinimalInformations();
            Owner.Deserialize(reader);
            Bonuses = new ObjectEffectInteger[reader.ReadShort()];
            for (var i = 0; i < Bonuses.Length; i++)
            {
                Bonuses[i] = new ObjectEffectInteger();
                Bonuses[i].Deserialize(reader);
            }
            Erosion = reader.ReadVarInt();
            Bugdet = reader.ReadVarInt();
            Saved = reader.ReadBoolean();
        }
    }
}