namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachExitResponseMessage : NetworkMessage
    {
        public override short Protocol => 6814;

        public bool Exited { get; set; }

        public BreachExitResponseMessage()
        {
        }

        public BreachExitResponseMessage(bool exited)
        {
            Exited = exited;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Exited);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Exited = reader.ReadBoolean();
        }
    }
}