using Horizon.Protocol.Types.Game.Context.Fight;
using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Context.Roleplay.Breach;
using Horizon.Protocol.Types.Game.House;
using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class MapComplementaryInformationsBreachMessage : MapComplementaryInformationsDataMessage
    {
        public override short Protocol => 6791;

        public int Floor { get; set; }

        public byte Room { get; set; }

        public BreachBranch[] Branches { get; set; }

        public MapComplementaryInformationsBreachMessage()
        {
        }

        public MapComplementaryInformationsBreachMessage(short subAreaId, double mapId, HouseInformations[] houses, GameRolePlayActorInformations[] actors, InteractiveElement[] interactiveElements, StatedElement[] statedElements, MapObstacle[] obstacles, FightCommonInformations[] fights, bool hasAggressiveMonsters, FightStartingPositions fightStartPositions, int floor, byte room, BreachBranch[] branches) : base(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions)
        {
            Floor = floor;
            Room = room;
            Branches = branches;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Floor);
            writer.WriteByte(Room);
            writer.WriteShort(Branches.Length);
            for (var i = 0; i < Branches.Length; i++)
            {
                Branches[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Floor = reader.ReadVarInt();
            Room = reader.ReadByte();
            Branches = new BreachBranch[reader.ReadShort()];
            for (var i = 0; i < Branches.Length; i++)
            {
                Branches[i] = new BreachBranch();
                Branches[i].Deserialize(reader);
            }
        }
    }
}