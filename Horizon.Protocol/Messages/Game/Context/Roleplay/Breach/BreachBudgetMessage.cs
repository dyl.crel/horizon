namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachBudgetMessage : NetworkMessage
    {
        public override short Protocol => 6786;

        public int Bugdet { get; set; }

        public BreachBudgetMessage()
        {
        }

        public BreachBudgetMessage(int bugdet)
        {
            Bugdet = bugdet;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Bugdet);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Bugdet = reader.ReadVarInt();
        }
    }
}