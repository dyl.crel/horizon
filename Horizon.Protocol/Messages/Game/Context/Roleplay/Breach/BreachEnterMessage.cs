namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachEnterMessage : NetworkMessage
    {
        public override short Protocol => 6810;

        public long Owner { get; set; }

        public BreachEnterMessage()
        {
        }

        public BreachEnterMessage(long owner)
        {
            Owner = owner;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Owner);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Owner = reader.ReadVarLong();
        }
    }
}