namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Breach
{
    public class BreachTeleportResponseMessage : NetworkMessage
    {
        public override short Protocol => 6816;

        public bool Teleported { get; set; }

        public BreachTeleportResponseMessage()
        {
        }

        public BreachTeleportResponseMessage(bool teleported)
        {
            Teleported = teleported;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Teleported);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Teleported = reader.ReadBoolean();
        }
    }
}