using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay
{
    public class GameRolePlayShowActorMessage : NetworkMessage
    {
        public override short Protocol => 5632;

        public GameRolePlayActorInformations Informations { get; set; }

        public GameRolePlayShowActorMessage()
        {
        }

        public GameRolePlayShowActorMessage(GameRolePlayActorInformations informations)
        {
            Informations = informations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Informations.Protocol);
            Informations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Informations = ProtocolTypesManager.Instance<GameRolePlayActorInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
    }
}