namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryEntryRequestMessage : NetworkMessage
    {
        public override short Protocol => 6043;

        public long PlayerId { get; set; }

        public JobCrafterDirectoryEntryRequestMessage()
        {
        }

        public JobCrafterDirectoryEntryRequestMessage(long playerId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
        }
    }
}