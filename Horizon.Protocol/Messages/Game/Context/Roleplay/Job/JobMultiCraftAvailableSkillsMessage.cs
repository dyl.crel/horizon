namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobMultiCraftAvailableSkillsMessage : JobAllowMultiCraftRequestMessage
    {
        public override short Protocol => 5747;

        public long PlayerId { get; set; }

        public short[] Skills { get; set; }

        public JobMultiCraftAvailableSkillsMessage()
        {
        }

        public JobMultiCraftAvailableSkillsMessage(bool enabled, long playerId, short[] skills) : base(enabled)
        {
            PlayerId = playerId;
            Skills = skills;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteShort(Skills.Length);
            for (var i = 0; i < Skills.Length; i++)
            {
                writer.WriteVarShort(Skills[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
            Skills = new short[reader.ReadShort()];
            for (var i = 0; i < Skills.Length; i++)
            {
                Skills[i] = reader.ReadVarShort();
            }
        }
    }
}