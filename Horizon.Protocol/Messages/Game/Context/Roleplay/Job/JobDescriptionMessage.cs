using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobDescriptionMessage : NetworkMessage
    {
        public override short Protocol => 5655;

        public JobDescription[] JobsDescription { get; set; }

        public JobDescriptionMessage()
        {
        }

        public JobDescriptionMessage(JobDescription[] jobsDescription)
        {
            JobsDescription = jobsDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(JobsDescription.Length);
            for (var i = 0; i < JobsDescription.Length; i++)
            {
                JobsDescription[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobsDescription = new JobDescription[reader.ReadShort()];
            for (var i = 0; i < JobsDescription.Length; i++)
            {
                JobsDescription[i] = new JobDescription();
                JobsDescription[i].Deserialize(reader);
            }
        }
    }
}