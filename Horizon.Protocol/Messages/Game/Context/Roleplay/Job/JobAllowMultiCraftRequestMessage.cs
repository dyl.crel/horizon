namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobAllowMultiCraftRequestMessage : NetworkMessage
    {
        public override short Protocol => 5748;

        public bool Enabled { get; set; }

        public JobAllowMultiCraftRequestMessage()
        {
        }

        public JobAllowMultiCraftRequestMessage(bool enabled)
        {
            Enabled = enabled;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Enabled);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Enabled = reader.ReadBoolean();
        }
    }
}