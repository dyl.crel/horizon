namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryRemoveMessage : NetworkMessage
    {
        public override short Protocol => 5653;

        public byte JobId { get; set; }

        public long PlayerId { get; set; }

        public JobCrafterDirectoryRemoveMessage()
        {
        }

        public JobCrafterDirectoryRemoveMessage(byte jobId, long playerId)
        {
            JobId = jobId;
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
            PlayerId = reader.ReadVarLong();
        }
    }
}