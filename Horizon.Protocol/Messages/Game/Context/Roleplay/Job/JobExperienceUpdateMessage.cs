using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobExperienceUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5654;

        public JobExperience ExperiencesUpdate { get; set; }

        public JobExperienceUpdateMessage()
        {
        }

        public JobExperienceUpdateMessage(JobExperience experiencesUpdate)
        {
            ExperiencesUpdate = experiencesUpdate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            ExperiencesUpdate.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExperiencesUpdate = new JobExperience();
            ExperiencesUpdate.Deserialize(reader);
        }
    }
}