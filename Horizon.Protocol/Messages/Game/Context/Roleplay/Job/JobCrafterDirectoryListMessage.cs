using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryListMessage : NetworkMessage
    {
        public override short Protocol => 6046;

        public JobCrafterDirectoryListEntry[] ListEntries { get; set; }

        public JobCrafterDirectoryListMessage()
        {
        }

        public JobCrafterDirectoryListMessage(JobCrafterDirectoryListEntry[] listEntries)
        {
            ListEntries = listEntries;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ListEntries.Length);
            for (var i = 0; i < ListEntries.Length; i++)
            {
                ListEntries[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ListEntries = new JobCrafterDirectoryListEntry[reader.ReadShort()];
            for (var i = 0; i < ListEntries.Length; i++)
            {
                ListEntries[i] = new JobCrafterDirectoryListEntry();
                ListEntries[i].Deserialize(reader);
            }
        }
    }
}