using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryDefineSettingsMessage : NetworkMessage
    {
        public override short Protocol => 5649;

        public JobCrafterDirectorySettings Settings { get; set; }

        public JobCrafterDirectoryDefineSettingsMessage()
        {
        }

        public JobCrafterDirectoryDefineSettingsMessage(JobCrafterDirectorySettings settings)
        {
            Settings = settings;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Settings.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Settings = new JobCrafterDirectorySettings();
            Settings.Deserialize(reader);
        }
    }
}