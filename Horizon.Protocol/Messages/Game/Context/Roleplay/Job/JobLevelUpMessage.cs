using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobLevelUpMessage : NetworkMessage
    {
        public override short Protocol => 5656;

        public byte NewLevel { get; set; }

        public JobDescription JobsDescription { get; set; }

        public JobLevelUpMessage()
        {
        }

        public JobLevelUpMessage(byte newLevel, JobDescription jobsDescription)
        {
            NewLevel = newLevel;
            JobsDescription = jobsDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(NewLevel);
            JobsDescription.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NewLevel = reader.ReadByte();
            JobsDescription = new JobDescription();
            JobsDescription.Deserialize(reader);
        }
    }
}