using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryAddMessage : NetworkMessage
    {
        public override short Protocol => 5651;

        public JobCrafterDirectoryListEntry ListEntry { get; set; }

        public JobCrafterDirectoryAddMessage()
        {
        }

        public JobCrafterDirectoryAddMessage(JobCrafterDirectoryListEntry listEntry)
        {
            ListEntry = listEntry;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            ListEntry.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ListEntry = new JobCrafterDirectoryListEntry();
            ListEntry.Deserialize(reader);
        }
    }
}