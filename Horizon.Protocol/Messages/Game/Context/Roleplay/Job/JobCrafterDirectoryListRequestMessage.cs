namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryListRequestMessage : NetworkMessage
    {
        public override short Protocol => 6047;

        public byte JobId { get; set; }

        public JobCrafterDirectoryListRequestMessage()
        {
        }

        public JobCrafterDirectoryListRequestMessage(byte jobId)
        {
            JobId = jobId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
        }
    }
}