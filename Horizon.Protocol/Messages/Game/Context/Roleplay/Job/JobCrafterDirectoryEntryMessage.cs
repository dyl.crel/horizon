using Horizon.Protocol.Types.Game.Context.Roleplay.Job;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryEntryMessage : NetworkMessage
    {
        public override short Protocol => 6044;

        public JobCrafterDirectoryEntryPlayerInfo PlayerInfo { get; set; }

        public JobCrafterDirectoryEntryJobInfo[] JobInfoList { get; set; }

        public EntityLook PlayerLook { get; set; }

        public JobCrafterDirectoryEntryMessage()
        {
        }

        public JobCrafterDirectoryEntryMessage(JobCrafterDirectoryEntryPlayerInfo playerInfo, JobCrafterDirectoryEntryJobInfo[] jobInfoList, EntityLook playerLook)
        {
            PlayerInfo = playerInfo;
            JobInfoList = jobInfoList;
            PlayerLook = playerLook;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            PlayerInfo.Serialize(writer);
            writer.WriteShort(JobInfoList.Length);
            for (var i = 0; i < JobInfoList.Length; i++)
            {
                JobInfoList[i].Serialize(writer);
            }
            PlayerLook.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerInfo = new JobCrafterDirectoryEntryPlayerInfo();
            PlayerInfo.Deserialize(reader);
            JobInfoList = new JobCrafterDirectoryEntryJobInfo[reader.ReadShort()];
            for (var i = 0; i < JobInfoList.Length; i++)
            {
                JobInfoList[i] = new JobCrafterDirectoryEntryJobInfo();
                JobInfoList[i].Deserialize(reader);
            }
            PlayerLook = new EntityLook();
            PlayerLook.Deserialize(reader);
        }
    }
}