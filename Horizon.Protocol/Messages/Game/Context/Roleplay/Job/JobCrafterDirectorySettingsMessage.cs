using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectorySettingsMessage : NetworkMessage
    {
        public override short Protocol => 5652;

        public JobCrafterDirectorySettings[] CraftersSettings { get; set; }

        public JobCrafterDirectorySettingsMessage()
        {
        }

        public JobCrafterDirectorySettingsMessage(JobCrafterDirectorySettings[] craftersSettings)
        {
            CraftersSettings = craftersSettings;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CraftersSettings.Length);
            for (var i = 0; i < CraftersSettings.Length; i++)
            {
                CraftersSettings[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CraftersSettings = new JobCrafterDirectorySettings[reader.ReadShort()];
            for (var i = 0; i < CraftersSettings.Length; i++)
            {
                CraftersSettings[i] = new JobCrafterDirectorySettings();
                CraftersSettings[i].Deserialize(reader);
            }
        }
    }
}