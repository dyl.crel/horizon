using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobExperienceOtherPlayerUpdateMessage : JobExperienceUpdateMessage
    {
        public override short Protocol => 6599;

        public long PlayerId { get; set; }

        public JobExperienceOtherPlayerUpdateMessage()
        {
        }

        public JobExperienceOtherPlayerUpdateMessage(JobExperience experiencesUpdate, long playerId) : base(experiencesUpdate)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}