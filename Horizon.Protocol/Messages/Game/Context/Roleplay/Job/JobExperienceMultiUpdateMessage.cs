using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobExperienceMultiUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5809;

        public JobExperience[] ExperiencesUpdate { get; set; }

        public JobExperienceMultiUpdateMessage()
        {
        }

        public JobExperienceMultiUpdateMessage(JobExperience[] experiencesUpdate)
        {
            ExperiencesUpdate = experiencesUpdate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ExperiencesUpdate.Length);
            for (var i = 0; i < ExperiencesUpdate.Length; i++)
            {
                ExperiencesUpdate[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExperiencesUpdate = new JobExperience[reader.ReadShort()];
            for (var i = 0; i < ExperiencesUpdate.Length; i++)
            {
                ExperiencesUpdate[i] = new JobExperience();
                ExperiencesUpdate[i].Deserialize(reader);
            }
        }
    }
}