using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Job
{
    public class JobBookSubscriptionMessage : NetworkMessage
    {
        public override short Protocol => 6593;

        public JobBookSubscription[] Subscriptions { get; set; }

        public JobBookSubscriptionMessage()
        {
        }

        public JobBookSubscriptionMessage(JobBookSubscription[] subscriptions)
        {
            Subscriptions = subscriptions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Subscriptions.Length);
            for (var i = 0; i < Subscriptions.Length; i++)
            {
                Subscriptions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Subscriptions = new JobBookSubscription[reader.ReadShort()];
            for (var i = 0; i < Subscriptions.Length; i++)
            {
                Subscriptions[i] = new JobBookSubscription();
                Subscriptions[i].Deserialize(reader);
            }
        }
    }
}