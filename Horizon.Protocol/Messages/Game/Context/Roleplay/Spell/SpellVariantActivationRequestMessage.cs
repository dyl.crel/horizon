namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Spell
{
    public class SpellVariantActivationRequestMessage : NetworkMessage
    {
        public override short Protocol => 6707;

        public short SpellId { get; set; }

        public SpellVariantActivationRequestMessage()
        {
        }

        public SpellVariantActivationRequestMessage(short spellId)
        {
            SpellId = spellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadVarShort();
        }
    }
}