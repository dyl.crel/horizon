namespace Horizon.Protocol.Messages.Game.Context.Roleplay.Spell
{
    public class SpellVariantActivationMessage : NetworkMessage
    {
        public override short Protocol => 6705;

        public short SpellId { get; set; }

        public bool Result { get; set; }

        public SpellVariantActivationMessage()
        {
        }

        public SpellVariantActivationMessage(short spellId, bool result)
        {
            SpellId = spellId;
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteBoolean(Result);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadVarShort();
            Result = reader.ReadBoolean();
        }
    }
}