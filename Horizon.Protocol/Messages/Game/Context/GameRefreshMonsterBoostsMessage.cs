using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameRefreshMonsterBoostsMessage : NetworkMessage
    {
        public override short Protocol => 6618;

        public MonsterBoosts[] MonsterBoosts { get; set; }

        public MonsterBoosts[] FamilyBoosts { get; set; }

        public GameRefreshMonsterBoostsMessage()
        {
        }

        public GameRefreshMonsterBoostsMessage(MonsterBoosts[] monsterBoosts, MonsterBoosts[] familyBoosts)
        {
            MonsterBoosts = monsterBoosts;
            FamilyBoosts = familyBoosts;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(MonsterBoosts.Length);
            for (var i = 0; i < MonsterBoosts.Length; i++)
            {
                MonsterBoosts[i].Serialize(writer);
            }
            writer.WriteShort(FamilyBoosts.Length);
            for (var i = 0; i < FamilyBoosts.Length; i++)
            {
                FamilyBoosts[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MonsterBoosts = new MonsterBoosts[reader.ReadShort()];
            for (var i = 0; i < MonsterBoosts.Length; i++)
            {
                MonsterBoosts[i] = new MonsterBoosts();
                MonsterBoosts[i].Deserialize(reader);
            }
            FamilyBoosts = new MonsterBoosts[reader.ReadShort()];
            for (var i = 0; i < FamilyBoosts.Length; i++)
            {
                FamilyBoosts[i] = new MonsterBoosts();
                FamilyBoosts[i].Deserialize(reader);
            }
        }
    }
}