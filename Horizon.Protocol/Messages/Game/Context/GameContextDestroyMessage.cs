namespace Horizon.Protocol.Messages.Game.Context
{
    public class GameContextDestroyMessage : NetworkMessage
    {
        public override short Protocol => 201;

        public GameContextDestroyMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}