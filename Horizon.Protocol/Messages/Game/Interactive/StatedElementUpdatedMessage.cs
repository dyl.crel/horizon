using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class StatedElementUpdatedMessage : NetworkMessage
    {
        public override short Protocol => 5709;

        public StatedElement StatedElement { get; set; }

        public StatedElementUpdatedMessage()
        {
        }

        public StatedElementUpdatedMessage(StatedElement statedElement)
        {
            StatedElement = statedElement;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            StatedElement.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            StatedElement = new StatedElement();
            StatedElement.Deserialize(reader);
        }
    }
}