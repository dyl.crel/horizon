namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class InteractiveUseErrorMessage : NetworkMessage
    {
        public override short Protocol => 6384;

        public int ElemId { get; set; }

        public int SkillInstanceUid { get; set; }

        public InteractiveUseErrorMessage()
        {
        }

        public InteractiveUseErrorMessage(int elemId, int skillInstanceUid)
        {
            ElemId = elemId;
            SkillInstanceUid = skillInstanceUid;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ElemId);
            writer.WriteVarInt(SkillInstanceUid);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ElemId = reader.ReadVarInt();
            SkillInstanceUid = reader.ReadVarInt();
        }
    }
}