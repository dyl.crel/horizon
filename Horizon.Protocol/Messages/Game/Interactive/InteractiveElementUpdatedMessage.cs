using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class InteractiveElementUpdatedMessage : NetworkMessage
    {
        public override short Protocol => 5708;

        public InteractiveElement InteractiveElement { get; set; }

        public InteractiveElementUpdatedMessage()
        {
        }

        public InteractiveElementUpdatedMessage(InteractiveElement interactiveElement)
        {
            InteractiveElement = interactiveElement;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            InteractiveElement.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InteractiveElement = new InteractiveElement();
            InteractiveElement.Deserialize(reader);
        }
    }
}