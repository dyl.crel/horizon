namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class InteractiveUsedMessage : NetworkMessage
    {
        public override short Protocol => 5745;

        public long EntityId { get; set; }

        public int ElemId { get; set; }

        public short SkillId { get; set; }

        public short Duration { get; set; }

        public bool CanMove { get; set; }

        public InteractiveUsedMessage()
        {
        }

        public InteractiveUsedMessage(long entityId, int elemId, short skillId, short duration, bool canMove)
        {
            EntityId = entityId;
            ElemId = elemId;
            SkillId = skillId;
            Duration = duration;
            CanMove = canMove;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(EntityId);
            writer.WriteVarInt(ElemId);
            writer.WriteVarShort(SkillId);
            writer.WriteVarShort(Duration);
            writer.WriteBoolean(CanMove);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            EntityId = reader.ReadVarLong();
            ElemId = reader.ReadVarInt();
            SkillId = reader.ReadVarShort();
            Duration = reader.ReadVarShort();
            CanMove = reader.ReadBoolean();
        }
    }
}