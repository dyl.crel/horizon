using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class StatedMapUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5716;

        public StatedElement[] StatedElements { get; set; }

        public StatedMapUpdateMessage()
        {
        }

        public StatedMapUpdateMessage(StatedElement[] statedElements)
        {
            StatedElements = statedElements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(StatedElements.Length);
            for (var i = 0; i < StatedElements.Length; i++)
            {
                StatedElements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            StatedElements = new StatedElement[reader.ReadShort()];
            for (var i = 0; i < StatedElements.Length; i++)
            {
                StatedElements[i] = new StatedElement();
                StatedElements[i].Deserialize(reader);
            }
        }
    }
}