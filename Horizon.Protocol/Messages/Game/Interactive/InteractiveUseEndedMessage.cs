namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class InteractiveUseEndedMessage : NetworkMessage
    {
        public override short Protocol => 6112;

        public int ElemId { get; set; }

        public short SkillId { get; set; }

        public InteractiveUseEndedMessage()
        {
        }

        public InteractiveUseEndedMessage(int elemId, short skillId)
        {
            ElemId = elemId;
            SkillId = skillId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ElemId);
            writer.WriteVarShort(SkillId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ElemId = reader.ReadVarInt();
            SkillId = reader.ReadVarShort();
        }
    }
}