namespace Horizon.Protocol.Messages.Game.Interactive.Zaap
{
    public class ZaapListMessage : TeleportDestinationsListMessage
    {
        public override short Protocol => 1604;

        public double SpawnMapId { get; set; }

        public ZaapListMessage()
        {
        }

        public ZaapListMessage(byte teleporterType, double[] mapIds, short[] subAreaIds, short[] costs, byte[] destTeleporterType, double spawnMapId) : base(teleporterType, mapIds, subAreaIds, costs, destTeleporterType)
        {
            SpawnMapId = spawnMapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(SpawnMapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SpawnMapId = reader.ReadDouble();
        }
    }
}