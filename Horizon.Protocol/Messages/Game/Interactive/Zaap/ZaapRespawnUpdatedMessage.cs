namespace Horizon.Protocol.Messages.Game.Interactive.Zaap
{
    public class ZaapRespawnUpdatedMessage : NetworkMessage
    {
        public override short Protocol => 6571;

        public double MapId { get; set; }

        public ZaapRespawnUpdatedMessage()
        {
        }

        public ZaapRespawnUpdatedMessage(double mapId)
        {
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
        }
    }
}