namespace Horizon.Protocol.Messages.Game.Interactive.Zaap
{
    public class TeleportDestinationsListMessage : NetworkMessage
    {
        public override short Protocol => 5960;

        public byte TeleporterType { get; set; }

        public double[] MapIds { get; set; }

        public short[] SubAreaIds { get; set; }

        public short[] Costs { get; set; }

        public byte[] DestTeleporterType { get; set; }

        public TeleportDestinationsListMessage()
        {
        }

        public TeleportDestinationsListMessage(byte teleporterType, double[] mapIds, short[] subAreaIds, short[] costs, byte[] destTeleporterType)
        {
            TeleporterType = teleporterType;
            MapIds = mapIds;
            SubAreaIds = subAreaIds;
            Costs = costs;
            DestTeleporterType = destTeleporterType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(TeleporterType);
            writer.WriteShort(MapIds.Length);
            for (var i = 0; i < MapIds.Length; i++)
            {
                writer.WriteDouble(MapIds[i]);
            }
            writer.WriteShort(SubAreaIds.Length);
            for (var i = 0; i < SubAreaIds.Length; i++)
            {
                writer.WriteVarShort(SubAreaIds[i]);
            }
            writer.WriteShort(Costs.Length);
            for (var i = 0; i < Costs.Length; i++)
            {
                writer.WriteVarShort(Costs[i]);
            }
            writer.WriteShort(DestTeleporterType.Length);
            for (var i = 0; i < DestTeleporterType.Length; i++)
            {
                writer.WriteByte(DestTeleporterType[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TeleporterType = reader.ReadByte();
            MapIds = new double[reader.ReadShort()];
            for (var i = 0; i < MapIds.Length; i++)
            {
                MapIds[i] = reader.ReadDouble();
            }
            SubAreaIds = new short[reader.ReadShort()];
            for (var i = 0; i < SubAreaIds.Length; i++)
            {
                SubAreaIds[i] = reader.ReadVarShort();
            }
            Costs = new short[reader.ReadShort()];
            for (var i = 0; i < Costs.Length; i++)
            {
                Costs[i] = reader.ReadVarShort();
            }
            DestTeleporterType = new byte[reader.ReadShort()];
            for (var i = 0; i < DestTeleporterType.Length; i++)
            {
                DestTeleporterType[i] = reader.ReadByte();
            }
        }
    }
}