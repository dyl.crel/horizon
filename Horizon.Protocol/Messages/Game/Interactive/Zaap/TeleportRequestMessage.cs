namespace Horizon.Protocol.Messages.Game.Interactive.Zaap
{
    public class TeleportRequestMessage : NetworkMessage
    {
        public override short Protocol => 5961;

        public byte TeleporterType { get; set; }

        public double MapId { get; set; }

        public TeleportRequestMessage()
        {
        }

        public TeleportRequestMessage(byte teleporterType, double mapId)
        {
            TeleporterType = teleporterType;
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(TeleporterType);
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TeleporterType = reader.ReadByte();
            MapId = reader.ReadDouble();
        }
    }
}