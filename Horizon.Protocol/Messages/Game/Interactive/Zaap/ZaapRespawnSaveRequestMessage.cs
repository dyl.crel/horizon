namespace Horizon.Protocol.Messages.Game.Interactive.Zaap
{
    public class ZaapRespawnSaveRequestMessage : NetworkMessage
    {
        public override short Protocol => 6572;

        public ZaapRespawnSaveRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}