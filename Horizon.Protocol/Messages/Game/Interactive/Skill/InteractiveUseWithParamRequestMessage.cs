namespace Horizon.Protocol.Messages.Game.Interactive.Skill
{
    public class InteractiveUseWithParamRequestMessage : InteractiveUseRequestMessage
    {
        public override short Protocol => 6715;

        public int Id { get; set; }

        public InteractiveUseWithParamRequestMessage()
        {
        }

        public InteractiveUseWithParamRequestMessage(int elemId, int skillInstanceUid, int id) : base(elemId, skillInstanceUid)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Id = reader.ReadInt();
        }
    }
}