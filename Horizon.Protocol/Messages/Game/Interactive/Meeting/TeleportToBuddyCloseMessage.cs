namespace Horizon.Protocol.Messages.Game.Interactive.Meeting
{
    public class TeleportToBuddyCloseMessage : NetworkMessage
    {
        public override short Protocol => 6303;

        public short DungeonId { get; set; }

        public long BuddyId { get; set; }

        public TeleportToBuddyCloseMessage()
        {
        }

        public TeleportToBuddyCloseMessage(short dungeonId, long buddyId)
        {
            DungeonId = dungeonId;
            BuddyId = buddyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteVarLong(BuddyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            BuddyId = reader.ReadVarLong();
        }
    }
}