namespace Horizon.Protocol.Messages.Game.Interactive.Meeting
{
    public class TeleportToBuddyOfferMessage : NetworkMessage
    {
        public override short Protocol => 6287;

        public short DungeonId { get; set; }

        public long BuddyId { get; set; }

        public int TimeLeft { get; set; }

        public TeleportToBuddyOfferMessage()
        {
        }

        public TeleportToBuddyOfferMessage(short dungeonId, long buddyId, int timeLeft)
        {
            DungeonId = dungeonId;
            BuddyId = buddyId;
            TimeLeft = timeLeft;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteVarLong(BuddyId);
            writer.WriteVarInt(TimeLeft);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            BuddyId = reader.ReadVarLong();
            TimeLeft = reader.ReadVarInt();
        }
    }
}