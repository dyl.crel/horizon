namespace Horizon.Protocol.Messages.Game.Interactive.Meeting
{
    public class TeleportToBuddyAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6293;

        public short DungeonId { get; set; }

        public long BuddyId { get; set; }

        public bool Accept { get; set; }

        public TeleportToBuddyAnswerMessage()
        {
        }

        public TeleportToBuddyAnswerMessage(short dungeonId, long buddyId, bool accept)
        {
            DungeonId = dungeonId;
            BuddyId = buddyId;
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteVarLong(BuddyId);
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            BuddyId = reader.ReadVarLong();
            Accept = reader.ReadBoolean();
        }
    }
}