namespace Horizon.Protocol.Messages.Game.Interactive.Meeting
{
    public class TeleportBuddiesAnswerMessage : NetworkMessage
    {
        public override short Protocol => 6294;

        public bool Accept { get; set; }

        public TeleportBuddiesAnswerMessage()
        {
        }

        public TeleportBuddiesAnswerMessage(bool accept)
        {
            Accept = accept;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Accept);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Accept = reader.ReadBoolean();
        }
    }
}