namespace Horizon.Protocol.Messages.Game.Interactive.Meeting
{
    public class TeleportBuddiesMessage : NetworkMessage
    {
        public override short Protocol => 6289;

        public short DungeonId { get; set; }

        public TeleportBuddiesMessage()
        {
        }

        public TeleportBuddiesMessage(short dungeonId)
        {
            DungeonId = dungeonId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
        }
    }
}