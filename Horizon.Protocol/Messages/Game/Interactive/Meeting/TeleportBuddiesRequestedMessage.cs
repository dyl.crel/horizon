namespace Horizon.Protocol.Messages.Game.Interactive.Meeting
{
    public class TeleportBuddiesRequestedMessage : NetworkMessage
    {
        public override short Protocol => 6302;

        public short DungeonId { get; set; }

        public long InviterId { get; set; }

        public long[] InvalidBuddiesIds { get; set; }

        public TeleportBuddiesRequestedMessage()
        {
        }

        public TeleportBuddiesRequestedMessage(short dungeonId, long inviterId, long[] invalidBuddiesIds)
        {
            DungeonId = dungeonId;
            InviterId = inviterId;
            InvalidBuddiesIds = invalidBuddiesIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteVarLong(InviterId);
            writer.WriteShort(InvalidBuddiesIds.Length);
            for (var i = 0; i < InvalidBuddiesIds.Length; i++)
            {
                writer.WriteVarLong(InvalidBuddiesIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DungeonId = reader.ReadVarShort();
            InviterId = reader.ReadVarLong();
            InvalidBuddiesIds = new long[reader.ReadShort()];
            for (var i = 0; i < InvalidBuddiesIds.Length; i++)
            {
                InvalidBuddiesIds[i] = reader.ReadVarLong();
            }
        }
    }
}