using Horizon.Protocol.Types.Game.Interactive;

namespace Horizon.Protocol.Messages.Game.Interactive
{
    public class InteractiveMapUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5002;

        public InteractiveElement[] InteractiveElements { get; set; }

        public InteractiveMapUpdateMessage()
        {
        }

        public InteractiveMapUpdateMessage(InteractiveElement[] interactiveElements)
        {
            InteractiveElements = interactiveElements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(InteractiveElements.Length);
            for (var i = 0; i < InteractiveElements.Length; i++)
            {
                writer.WriteShort(InteractiveElements[i].Protocol);
                InteractiveElements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InteractiveElements = new InteractiveElement[reader.ReadShort()];
            for (var i = 0; i < InteractiveElements.Length; i++)
            {
                InteractiveElements[i] = ProtocolTypesManager.Instance<InteractiveElement>(reader.ReadShort());
                InteractiveElements[i].Deserialize(reader);
            }
        }
    }
}