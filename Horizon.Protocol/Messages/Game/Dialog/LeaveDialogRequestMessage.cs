namespace Horizon.Protocol.Messages.Game.Dialog
{
    public class LeaveDialogRequestMessage : NetworkMessage
    {
        public override short Protocol => 5501;

        public LeaveDialogRequestMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}