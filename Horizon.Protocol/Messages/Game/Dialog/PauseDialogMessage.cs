namespace Horizon.Protocol.Messages.Game.Dialog
{
    public class PauseDialogMessage : NetworkMessage
    {
        public override short Protocol => 6012;

        public byte DialogType { get; set; }

        public PauseDialogMessage()
        {
        }

        public PauseDialogMessage(byte dialogType)
        {
            DialogType = dialogType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(DialogType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DialogType = reader.ReadByte();
        }
    }
}