namespace Horizon.Protocol.Messages.Game.Dialog
{
    public class LeaveDialogMessage : NetworkMessage
    {
        public override short Protocol => 5502;

        public byte DialogType { get; set; }

        public LeaveDialogMessage()
        {
        }

        public LeaveDialogMessage(byte dialogType)
        {
            DialogType = dialogType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(DialogType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DialogType = reader.ReadByte();
        }
    }
}