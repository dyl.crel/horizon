namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeAcceptMessage : NetworkMessage
    {
        public override short Protocol => 5508;

        public ExchangeAcceptMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}