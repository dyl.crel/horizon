namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeShowVendorTaxMessage : NetworkMessage
    {
        public override short Protocol => 5783;

        public ExchangeShowVendorTaxMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}