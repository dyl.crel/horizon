namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkEvolutiveObjectRecycleTradeMessage : NetworkMessage
    {
        public override short Protocol => 6778;

        public ExchangeStartOkEvolutiveObjectRecycleTradeMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}