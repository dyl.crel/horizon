using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeShopStockMovementUpdatedMessage : NetworkMessage
    {
        public override short Protocol => 5909;

        public ObjectItemToSell ObjectInfo { get; set; }

        public ExchangeShopStockMovementUpdatedMessage()
        {
        }

        public ExchangeShopStockMovementUpdatedMessage(ObjectItemToSell objectInfo)
        {
            ObjectInfo = objectInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            ObjectInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectInfo = new ObjectItemToSell();
            ObjectInfo.Deserialize(reader);
        }
    }
}