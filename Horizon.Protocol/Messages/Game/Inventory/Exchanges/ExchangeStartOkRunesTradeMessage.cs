namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkRunesTradeMessage : NetworkMessage
    {
        public override short Protocol => 6567;

        public ExchangeStartOkRunesTradeMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}