namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkNpcTradeMessage : NetworkMessage
    {
        public override short Protocol => 5785;

        public double NpcId { get; set; }

        public ExchangeStartOkNpcTradeMessage()
        {
        }

        public ExchangeStartOkNpcTradeMessage(double npcId)
        {
            NpcId = npcId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(NpcId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NpcId = reader.ReadDouble();
        }
    }
}