using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkMountWithOutPaddockMessage : NetworkMessage
    {
        public override short Protocol => 5991;

        public MountClientData[] StabledMountsDescription { get; set; }

        public ExchangeStartOkMountWithOutPaddockMessage()
        {
        }

        public ExchangeStartOkMountWithOutPaddockMessage(MountClientData[] stabledMountsDescription)
        {
            StabledMountsDescription = stabledMountsDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(StabledMountsDescription.Length);
            for (var i = 0; i < StabledMountsDescription.Length; i++)
            {
                StabledMountsDescription[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            StabledMountsDescription = new MountClientData[reader.ReadShort()];
            for (var i = 0; i < StabledMountsDescription.Length; i++)
            {
                StabledMountsDescription[i] = new MountClientData();
                StabledMountsDescription[i].Deserialize(reader);
            }
        }
    }
}