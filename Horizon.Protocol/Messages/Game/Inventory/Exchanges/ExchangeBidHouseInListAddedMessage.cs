using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseInListAddedMessage : NetworkMessage
    {
        public override short Protocol => 5949;

        public int ItemUID { get; set; }

        public int ObjGenericId { get; set; }

        public ObjectEffect[] Effects { get; set; }

        public long[] Prices { get; set; }

        public ExchangeBidHouseInListAddedMessage()
        {
        }

        public ExchangeBidHouseInListAddedMessage(int itemUID, int objGenericId, ObjectEffect[] effects, long[] prices)
        {
            ItemUID = itemUID;
            ObjGenericId = objGenericId;
            Effects = effects;
            Prices = prices;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(ItemUID);
            writer.WriteInt(ObjGenericId);
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                writer.WriteShort(Effects[i].Protocol);
                Effects[i].Serialize(writer);
            }
            writer.WriteShort(Prices.Length);
            for (var i = 0; i < Prices.Length; i++)
            {
                writer.WriteVarLong(Prices[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ItemUID = reader.ReadInt();
            ObjGenericId = reader.ReadInt();
            Effects = new ObjectEffect[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = ProtocolTypesManager.Instance<ObjectEffect>(reader.ReadShort());
                Effects[i].Deserialize(reader);
            }
            Prices = new long[reader.ReadShort()];
            for (var i = 0; i < Prices.Length; i++)
            {
                Prices[i] = reader.ReadVarLong();
            }
        }
    }
}