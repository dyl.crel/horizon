namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ItemNoMoreAvailableMessage : NetworkMessage
    {
        public override short Protocol => 5769;

        public ItemNoMoreAvailableMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}