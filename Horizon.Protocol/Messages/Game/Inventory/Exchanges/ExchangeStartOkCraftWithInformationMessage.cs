namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkCraftWithInformationMessage : ExchangeStartOkCraftMessage
    {
        public override short Protocol => 5941;

        public int SkillId { get; set; }

        public ExchangeStartOkCraftWithInformationMessage()
        {
        }

        public ExchangeStartOkCraftWithInformationMessage(int skillId)
        {
            SkillId = skillId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(SkillId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SkillId = reader.ReadVarInt();
        }
    }
}