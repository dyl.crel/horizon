namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectModifyPricedMessage : ExchangeObjectMovePricedMessage
    {
        public override short Protocol => 6238;

        public ExchangeObjectModifyPricedMessage()
        {
        }

        public ExchangeObjectModifyPricedMessage(int objectUID, int quantity, long price) : base(objectUID, quantity, price)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}