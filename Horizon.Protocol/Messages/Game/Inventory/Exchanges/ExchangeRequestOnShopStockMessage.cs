namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeRequestOnShopStockMessage : NetworkMessage
    {
        public override short Protocol => 5753;

        public ExchangeRequestOnShopStockMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}