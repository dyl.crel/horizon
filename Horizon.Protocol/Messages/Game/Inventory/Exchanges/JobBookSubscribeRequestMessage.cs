namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class JobBookSubscribeRequestMessage : NetworkMessage
    {
        public override short Protocol => 6592;

        public byte[] JobIds { get; set; }

        public JobBookSubscribeRequestMessage()
        {
        }

        public JobBookSubscribeRequestMessage(byte[] jobIds)
        {
            JobIds = jobIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(JobIds.Length);
            for (var i = 0; i < JobIds.Length; i++)
            {
                writer.WriteByte(JobIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobIds = new byte[reader.ReadShort()];
            for (var i = 0; i < JobIds.Length; i++)
            {
                JobIds[i] = reader.ReadByte();
            }
        }
    }
}