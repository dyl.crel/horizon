namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountStableErrorMessage : NetworkMessage
    {
        public override short Protocol => 5981;

        public ExchangeMountStableErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}