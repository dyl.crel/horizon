namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBuyMessage : NetworkMessage
    {
        public override short Protocol => 5774;

        public int ObjectToBuyId { get; set; }

        public int Quantity { get; set; }

        public ExchangeBuyMessage()
        {
        }

        public ExchangeBuyMessage(int objectToBuyId, int quantity)
        {
            ObjectToBuyId = objectToBuyId;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectToBuyId);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectToBuyId = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}