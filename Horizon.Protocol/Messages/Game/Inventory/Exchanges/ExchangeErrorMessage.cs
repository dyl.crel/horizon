namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeErrorMessage : NetworkMessage
    {
        public override short Protocol => 5513;

        public byte ErrorType { get; set; }

        public ExchangeErrorMessage()
        {
        }

        public ExchangeErrorMessage(byte errorType)
        {
            ErrorType = errorType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ErrorType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ErrorType = reader.ReadByte();
        }
    }
}