using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartedMountStockMessage : NetworkMessage
    {
        public override short Protocol => 5984;

        public ObjectItem[] ObjectsInfos { get; set; }

        public ExchangeStartedMountStockMessage()
        {
        }

        public ExchangeStartedMountStockMessage(ObjectItem[] objectsInfos)
        {
            ObjectsInfos = objectsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectsInfos.Length);
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectsInfos = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i] = new ObjectItem();
                ObjectsInfos[i].Deserialize(reader);
            }
        }
    }
}