namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeSellOkMessage : NetworkMessage
    {
        public override short Protocol => 5792;

        public ExchangeSellOkMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}