namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectUseInWorkshopMessage : NetworkMessage
    {
        public override short Protocol => 6004;

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public ExchangeObjectUseInWorkshopMessage()
        {
        }

        public ExchangeObjectUseInWorkshopMessage(int objectUID, int quantity)
        {
            ObjectUID = objectUID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}