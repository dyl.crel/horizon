namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseGenericItemAddedMessage : NetworkMessage
    {
        public override short Protocol => 5947;

        public short ObjGenericId { get; set; }

        public ExchangeBidHouseGenericItemAddedMessage()
        {
        }

        public ExchangeBidHouseGenericItemAddedMessage(short objGenericId)
        {
            ObjGenericId = objGenericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ObjGenericId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjGenericId = reader.ReadVarShort();
        }
    }
}