namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class RecycleResultMessage : NetworkMessage
    {
        public override short Protocol => 6601;

        public int NuggetsForPrism { get; set; }

        public int NuggetsForPlayer { get; set; }

        public RecycleResultMessage()
        {
        }

        public RecycleResultMessage(int nuggetsForPrism, int nuggetsForPlayer)
        {
            NuggetsForPrism = nuggetsForPrism;
            NuggetsForPlayer = nuggetsForPlayer;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(NuggetsForPrism);
            writer.WriteVarInt(NuggetsForPlayer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NuggetsForPrism = reader.ReadVarInt();
            NuggetsForPlayer = reader.ReadVarInt();
        }
    }
}