namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftResultWithObjectIdMessage : ExchangeCraftResultMessage
    {
        public override short Protocol => 6000;

        public short ObjectGenericId { get; set; }

        public ExchangeCraftResultWithObjectIdMessage()
        {
        }

        public ExchangeCraftResultWithObjectIdMessage(byte craftResult, short objectGenericId) : base(craftResult)
        {
            ObjectGenericId = objectGenericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGenericId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectGenericId = reader.ReadVarShort();
        }
    }
}