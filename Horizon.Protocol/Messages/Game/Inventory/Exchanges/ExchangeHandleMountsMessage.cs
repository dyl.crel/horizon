namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeHandleMountsMessage : NetworkMessage
    {
        public override short Protocol => 6752;

        public byte ActionType { get; set; }

        public int[] RidesId { get; set; }

        public ExchangeHandleMountsMessage()
        {
        }

        public ExchangeHandleMountsMessage(byte actionType, int[] ridesId)
        {
            ActionType = actionType;
            RidesId = ridesId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ActionType);
            writer.WriteShort(RidesId.Length);
            for (var i = 0; i < RidesId.Length; i++)
            {
                writer.WriteVarInt(RidesId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActionType = reader.ReadByte();
            RidesId = new int[reader.ReadShort()];
            for (var i = 0; i < RidesId.Length; i++)
            {
                RidesId[i] = reader.ReadVarInt();
            }
        }
    }
}