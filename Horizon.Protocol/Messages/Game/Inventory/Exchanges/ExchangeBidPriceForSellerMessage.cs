namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidPriceForSellerMessage : ExchangeBidPriceMessage
    {
        public override short Protocol => 6464;

        public bool AllIdentical { get; set; }

        public long[] MinimalPrices { get; set; }

        public ExchangeBidPriceForSellerMessage()
        {
        }

        public ExchangeBidPriceForSellerMessage(short genericId, long averagePrice, bool allIdentical, long[] minimalPrices) : base(genericId, averagePrice)
        {
            AllIdentical = allIdentical;
            MinimalPrices = minimalPrices;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(AllIdentical);
            writer.WriteShort(MinimalPrices.Length);
            for (var i = 0; i < MinimalPrices.Length; i++)
            {
                writer.WriteVarLong(MinimalPrices[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllIdentical = reader.ReadBoolean();
            MinimalPrices = new long[reader.ReadShort()];
            for (var i = 0; i < MinimalPrices.Length; i++)
            {
                MinimalPrices[i] = reader.ReadVarLong();
            }
        }
    }
}