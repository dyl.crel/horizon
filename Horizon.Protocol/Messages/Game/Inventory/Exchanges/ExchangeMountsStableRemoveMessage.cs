namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountsStableRemoveMessage : NetworkMessage
    {
        public override short Protocol => 6556;

        public int[] MountsId { get; set; }

        public ExchangeMountsStableRemoveMessage()
        {
        }

        public ExchangeMountsStableRemoveMessage(int[] mountsId)
        {
            MountsId = mountsId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(MountsId.Length);
            for (var i = 0; i < MountsId.Length; i++)
            {
                writer.WriteVarInt(MountsId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountsId = new int[reader.ReadShort()];
            for (var i = 0; i < MountsId.Length; i++)
            {
                MountsId[i] = reader.ReadVarInt();
            }
        }
    }
}