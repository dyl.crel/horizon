namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountsPaddockRemoveMessage : NetworkMessage
    {
        public override short Protocol => 6559;

        public int[] MountsId { get; set; }

        public ExchangeMountsPaddockRemoveMessage()
        {
        }

        public ExchangeMountsPaddockRemoveMessage(int[] mountsId)
        {
            MountsId = mountsId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(MountsId.Length);
            for (var i = 0; i < MountsId.Length; i++)
            {
                writer.WriteVarInt(MountsId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountsId = new int[reader.ReadShort()];
            for (var i = 0; i < MountsId.Length; i++)
            {
                MountsId[i] = reader.ReadVarInt();
            }
        }
    }
}