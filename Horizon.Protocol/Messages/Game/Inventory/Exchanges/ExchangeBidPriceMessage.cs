namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidPriceMessage : NetworkMessage
    {
        public override short Protocol => 5755;

        public short GenericId { get; set; }

        public long AveragePrice { get; set; }

        public ExchangeBidPriceMessage()
        {
        }

        public ExchangeBidPriceMessage(short genericId, long averagePrice)
        {
            GenericId = genericId;
            AveragePrice = averagePrice;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(GenericId);
            writer.WriteVarLong(AveragePrice);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GenericId = reader.ReadVarShort();
            AveragePrice = reader.ReadVarLong();
        }
    }
}