namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseBuyResultMessage : NetworkMessage
    {
        public override short Protocol => 6272;

        public int Uid { get; set; }

        public bool Bought { get; set; }

        public ExchangeBidHouseBuyResultMessage()
        {
        }

        public ExchangeBidHouseBuyResultMessage(int uid, bool bought)
        {
            Uid = uid;
            Bought = bought;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Uid);
            writer.WriteBoolean(Bought);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Uid = reader.ReadVarInt();
            Bought = reader.ReadBoolean();
        }
    }
}