namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftResultMessage : NetworkMessage
    {
        public override short Protocol => 5790;

        public byte CraftResult { get; set; }

        public ExchangeCraftResultMessage()
        {
        }

        public ExchangeCraftResultMessage(byte craftResult)
        {
            CraftResult = craftResult;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(CraftResult);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CraftResult = reader.ReadByte();
        }
    }
}