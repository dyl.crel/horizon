namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeIsReadyMessage : NetworkMessage
    {
        public override short Protocol => 5509;

        public double Id { get; set; }

        public bool Ready { get; set; }

        public ExchangeIsReadyMessage()
        {
        }

        public ExchangeIsReadyMessage(double id, bool ready)
        {
            Id = id;
            Ready = ready;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
            writer.WriteBoolean(Ready);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
            Ready = reader.ReadBoolean();
        }
    }
}