namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartedWithPodsMessage : ExchangeStartedMessage
    {
        public override short Protocol => 6129;

        public double FirstCharacterId { get; set; }

        public int FirstCharacterCurrentWeight { get; set; }

        public int FirstCharacterMaxWeight { get; set; }

        public double SecondCharacterId { get; set; }

        public int SecondCharacterCurrentWeight { get; set; }

        public int SecondCharacterMaxWeight { get; set; }

        public ExchangeStartedWithPodsMessage()
        {
        }

        public ExchangeStartedWithPodsMessage(byte exchangeType, double firstCharacterId, int firstCharacterCurrentWeight, int firstCharacterMaxWeight, double secondCharacterId, int secondCharacterCurrentWeight, int secondCharacterMaxWeight) : base(exchangeType)
        {
            FirstCharacterId = firstCharacterId;
            FirstCharacterCurrentWeight = firstCharacterCurrentWeight;
            FirstCharacterMaxWeight = firstCharacterMaxWeight;
            SecondCharacterId = secondCharacterId;
            SecondCharacterCurrentWeight = secondCharacterCurrentWeight;
            SecondCharacterMaxWeight = secondCharacterMaxWeight;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(FirstCharacterId);
            writer.WriteVarInt(FirstCharacterCurrentWeight);
            writer.WriteVarInt(FirstCharacterMaxWeight);
            writer.WriteDouble(SecondCharacterId);
            writer.WriteVarInt(SecondCharacterCurrentWeight);
            writer.WriteVarInt(SecondCharacterMaxWeight);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FirstCharacterId = reader.ReadDouble();
            FirstCharacterCurrentWeight = reader.ReadVarInt();
            FirstCharacterMaxWeight = reader.ReadVarInt();
            SecondCharacterId = reader.ReadDouble();
            SecondCharacterCurrentWeight = reader.ReadVarInt();
            SecondCharacterMaxWeight = reader.ReadVarInt();
        }
    }
}