namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeWeightMessage : NetworkMessage
    {
        public override short Protocol => 5793;

        public int CurrentWeight { get; set; }

        public int MaxWeight { get; set; }

        public ExchangeWeightMessage()
        {
        }

        public ExchangeWeightMessage(int currentWeight, int maxWeight)
        {
            CurrentWeight = currentWeight;
            MaxWeight = maxWeight;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(CurrentWeight);
            writer.WriteVarInt(MaxWeight);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CurrentWeight = reader.ReadVarInt();
            MaxWeight = reader.ReadVarInt();
        }
    }
}