namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseItemRemoveOkMessage : NetworkMessage
    {
        public override short Protocol => 5946;

        public int SellerId { get; set; }

        public ExchangeBidHouseItemRemoveOkMessage()
        {
        }

        public ExchangeBidHouseItemRemoveOkMessage(int sellerId)
        {
            SellerId = sellerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(SellerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SellerId = reader.ReadInt();
        }
    }
}