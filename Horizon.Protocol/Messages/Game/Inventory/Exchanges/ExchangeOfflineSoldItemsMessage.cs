using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeOfflineSoldItemsMessage : NetworkMessage
    {
        public override short Protocol => 6613;

        public ObjectItemGenericQuantityPrice[] BidHouseItems { get; set; }

        public ObjectItemGenericQuantityPrice[] MerchantItems { get; set; }

        public ExchangeOfflineSoldItemsMessage()
        {
        }

        public ExchangeOfflineSoldItemsMessage(ObjectItemGenericQuantityPrice[] bidHouseItems, ObjectItemGenericQuantityPrice[] merchantItems)
        {
            BidHouseItems = bidHouseItems;
            MerchantItems = merchantItems;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(BidHouseItems.Length);
            for (var i = 0; i < BidHouseItems.Length; i++)
            {
                BidHouseItems[i].Serialize(writer);
            }
            writer.WriteShort(MerchantItems.Length);
            for (var i = 0; i < MerchantItems.Length; i++)
            {
                MerchantItems[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BidHouseItems = new ObjectItemGenericQuantityPrice[reader.ReadShort()];
            for (var i = 0; i < BidHouseItems.Length; i++)
            {
                BidHouseItems[i] = new ObjectItemGenericQuantityPrice();
                BidHouseItems[i].Deserialize(reader);
            }
            MerchantItems = new ObjectItemGenericQuantityPrice[reader.ReadShort()];
            for (var i = 0; i < MerchantItems.Length; i++)
            {
                MerchantItems[i] = new ObjectItemGenericQuantityPrice();
                MerchantItems[i].Deserialize(reader);
            }
        }
    }
}