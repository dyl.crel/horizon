namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectMoveKamaMessage : NetworkMessage
    {
        public override short Protocol => 5520;

        public long Quantity { get; set; }

        public ExchangeObjectMoveKamaMessage()
        {
        }

        public ExchangeObjectMoveKamaMessage(long quantity)
        {
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quantity = reader.ReadVarLong();
        }
    }
}