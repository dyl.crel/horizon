using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkNpcShopMessage : NetworkMessage
    {
        public override short Protocol => 5761;

        public double NpcSellerId { get; set; }

        public short TokenId { get; set; }

        public ObjectItemToSellInNpcShop[] ObjectsInfos { get; set; }

        public ExchangeStartOkNpcShopMessage()
        {
        }

        public ExchangeStartOkNpcShopMessage(double npcSellerId, short tokenId, ObjectItemToSellInNpcShop[] objectsInfos)
        {
            NpcSellerId = npcSellerId;
            TokenId = tokenId;
            ObjectsInfos = objectsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(NpcSellerId);
            writer.WriteVarShort(TokenId);
            writer.WriteShort(ObjectsInfos.Length);
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            NpcSellerId = reader.ReadDouble();
            TokenId = reader.ReadVarShort();
            ObjectsInfos = new ObjectItemToSellInNpcShop[reader.ReadShort()];
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i] = new ObjectItemToSellInNpcShop();
                ObjectsInfos[i].Deserialize(reader);
            }
        }
    }
}