using Horizon.Protocol.Types.Game.Inventory.Exchanges;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class EvolutiveObjectRecycleResultMessage : NetworkMessage
    {
        public override short Protocol => 6779;

        public RecycledItem[] RecycledItems { get; set; }

        public EvolutiveObjectRecycleResultMessage()
        {
        }

        public EvolutiveObjectRecycleResultMessage(RecycledItem[] recycledItems)
        {
            RecycledItems = recycledItems;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(RecycledItems.Length);
            for (var i = 0; i < RecycledItems.Length; i++)
            {
                RecycledItems[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RecycledItems = new RecycledItem[reader.ReadShort()];
            for (var i = 0; i < RecycledItems.Length; i++)
            {
                RecycledItems[i] = new RecycledItem();
                RecycledItems[i].Deserialize(reader);
            }
        }
    }
}