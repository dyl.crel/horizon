using Horizon.Protocol.Messages.Game.Dialog;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeLeaveMessage : LeaveDialogMessage
    {
        public override short Protocol => 5628;

        public bool Success { get; set; }

        public ExchangeLeaveMessage()
        {
        }

        public ExchangeLeaveMessage(byte dialogType, bool success) : base(dialogType)
        {
            Success = success;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Success);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Success = reader.ReadBoolean();
        }
    }
}