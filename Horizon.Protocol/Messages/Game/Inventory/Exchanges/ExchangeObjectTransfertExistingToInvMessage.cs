namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectTransfertExistingToInvMessage : NetworkMessage
    {
        public override short Protocol => 6326;

        public ExchangeObjectTransfertExistingToInvMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}