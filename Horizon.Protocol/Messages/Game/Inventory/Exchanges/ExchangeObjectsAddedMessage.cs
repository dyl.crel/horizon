using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectsAddedMessage : ExchangeObjectMessage
    {
        public override short Protocol => 6535;

        public ObjectItem[] Object { get; set; }

        public ExchangeObjectsAddedMessage()
        {
        }

        public ExchangeObjectsAddedMessage(bool remote, ObjectItem[] @object) : base(remote)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Object.Length);
            for (var i = 0; i < Object.Length; i++)
            {
                Object[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Object = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < Object.Length; i++)
            {
                Object[i] = new ObjectItem();
                Object[i].Deserialize(reader);
            }
        }
    }
}