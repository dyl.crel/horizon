namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeSetCraftRecipeMessage : NetworkMessage
    {
        public override short Protocol => 6389;

        public short ObjectGID { get; set; }

        public ExchangeSetCraftRecipeMessage()
        {
        }

        public ExchangeSetCraftRecipeMessage(short objectGID)
        {
            ObjectGID = objectGID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ObjectGID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectGID = reader.ReadVarShort();
        }
    }
}