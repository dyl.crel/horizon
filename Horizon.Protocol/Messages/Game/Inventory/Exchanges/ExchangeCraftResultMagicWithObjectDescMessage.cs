using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftResultMagicWithObjectDescMessage : ExchangeCraftResultWithObjectDescMessage
    {
        public override short Protocol => 6188;

        public byte MagicPoolStatus { get; set; }

        public ExchangeCraftResultMagicWithObjectDescMessage()
        {
        }

        public ExchangeCraftResultMagicWithObjectDescMessage(byte craftResult, ObjectItemNotInContainer objectInfo, byte magicPoolStatus) : base(craftResult, objectInfo)
        {
            MagicPoolStatus = magicPoolStatus;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(MagicPoolStatus);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MagicPoolStatus = reader.ReadByte();
        }
    }
}