namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectMovePricedMessage : ExchangeObjectMoveMessage
    {
        public override short Protocol => 5514;

        public long Price { get; set; }

        public ExchangeObjectMovePricedMessage()
        {
        }

        public ExchangeObjectMovePricedMessage(int objectUID, int quantity, long price) : base(objectUID, quantity)
        {
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Price = reader.ReadVarLong();
        }
    }
}