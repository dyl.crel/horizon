namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftCountRequestMessage : NetworkMessage
    {
        public override short Protocol => 6597;

        public int Count { get; set; }

        public ExchangeCraftCountRequestMessage()
        {
        }

        public ExchangeCraftCountRequestMessage(int count)
        {
            Count = count;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Count);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Count = reader.ReadVarInt();
        }
    }
}