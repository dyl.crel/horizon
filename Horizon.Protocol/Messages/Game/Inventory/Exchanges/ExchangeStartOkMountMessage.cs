using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkMountMessage : ExchangeStartOkMountWithOutPaddockMessage
    {
        public override short Protocol => 5979;

        public MountClientData[] PaddockedMountsDescription { get; set; }

        public ExchangeStartOkMountMessage()
        {
        }

        public ExchangeStartOkMountMessage(MountClientData[] stabledMountsDescription, MountClientData[] paddockedMountsDescription) : base(stabledMountsDescription)
        {
            PaddockedMountsDescription = paddockedMountsDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(PaddockedMountsDescription.Length);
            for (var i = 0; i < PaddockedMountsDescription.Length; i++)
            {
                PaddockedMountsDescription[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PaddockedMountsDescription = new MountClientData[reader.ReadShort()];
            for (var i = 0; i < PaddockedMountsDescription.Length; i++)
            {
                PaddockedMountsDescription[i] = new MountClientData();
                PaddockedMountsDescription[i].Deserialize(reader);
            }
        }
    }
}