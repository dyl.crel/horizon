namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeRequestOnMountStockMessage : NetworkMessage
    {
        public override short Protocol => 5986;

        public ExchangeRequestOnMountStockMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}