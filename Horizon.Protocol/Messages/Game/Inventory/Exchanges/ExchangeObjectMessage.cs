namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectMessage : NetworkMessage
    {
        public override short Protocol => 5515;

        public bool Remote { get; set; }

        public ExchangeObjectMessage()
        {
        }

        public ExchangeObjectMessage(bool remote)
        {
            Remote = remote;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Remote);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Remote = reader.ReadBoolean();
        }
    }
}