namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftInformationObjectMessage : ExchangeCraftResultWithObjectIdMessage
    {
        public override short Protocol => 5794;

        public long PlayerId { get; set; }

        public ExchangeCraftInformationObjectMessage()
        {
        }

        public ExchangeCraftInformationObjectMessage(byte craftResult, short objectGenericId, long playerId) : base(craftResult, objectGenericId)
        {
            PlayerId = playerId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
        }
    }
}