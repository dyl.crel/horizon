namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeRequestOnTaxCollectorMessage : NetworkMessage
    {
        public override short Protocol => 5779;

        public ExchangeRequestOnTaxCollectorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}