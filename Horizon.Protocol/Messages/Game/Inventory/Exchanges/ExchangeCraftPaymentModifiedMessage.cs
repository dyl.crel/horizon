namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftPaymentModifiedMessage : NetworkMessage
    {
        public override short Protocol => 6578;

        public long GoldSum { get; set; }

        public ExchangeCraftPaymentModifiedMessage()
        {
        }

        public ExchangeCraftPaymentModifiedMessage(long goldSum)
        {
            GoldSum = goldSum;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(GoldSum);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GoldSum = reader.ReadVarLong();
        }
    }
}