namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeOkMultiCraftMessage : NetworkMessage
    {
        public override short Protocol => 5768;

        public long InitiatorId { get; set; }

        public long OtherId { get; set; }

        public byte Role { get; set; }

        public ExchangeOkMultiCraftMessage()
        {
        }

        public ExchangeOkMultiCraftMessage(long initiatorId, long otherId, byte role)
        {
            InitiatorId = initiatorId;
            OtherId = otherId;
            Role = role;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(InitiatorId);
            writer.WriteVarLong(OtherId);
            writer.WriteByte(Role);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InitiatorId = reader.ReadVarLong();
            OtherId = reader.ReadVarLong();
            Role = reader.ReadByte();
        }
    }
}