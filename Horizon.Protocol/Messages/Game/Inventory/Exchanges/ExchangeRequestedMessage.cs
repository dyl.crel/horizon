namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeRequestedMessage : NetworkMessage
    {
        public override short Protocol => 5522;

        public byte ExchangeType { get; set; }

        public ExchangeRequestedMessage()
        {
        }

        public ExchangeRequestedMessage(byte exchangeType)
        {
            ExchangeType = exchangeType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ExchangeType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExchangeType = reader.ReadByte();
        }
    }
}