using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountsStableAddMessage : NetworkMessage
    {
        public override short Protocol => 6555;

        public MountClientData[] MountDescription { get; set; }

        public ExchangeMountsStableAddMessage()
        {
        }

        public ExchangeMountsStableAddMessage(MountClientData[] mountDescription)
        {
            MountDescription = mountDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(MountDescription.Length);
            for (var i = 0; i < MountDescription.Length; i++)
            {
                MountDescription[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountDescription = new MountClientData[reader.ReadShort()];
            for (var i = 0; i < MountDescription.Length; i++)
            {
                MountDescription[i] = new MountClientData();
                MountDescription[i].Deserialize(reader);
            }
        }
    }
}