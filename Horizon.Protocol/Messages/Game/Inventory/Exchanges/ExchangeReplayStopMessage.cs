namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeReplayStopMessage : NetworkMessage
    {
        public override short Protocol => 6001;

        public ExchangeReplayStopMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}