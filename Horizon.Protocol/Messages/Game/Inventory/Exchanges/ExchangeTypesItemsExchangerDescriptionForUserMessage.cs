using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeTypesItemsExchangerDescriptionForUserMessage : NetworkMessage
    {
        public override short Protocol => 5752;

        public BidExchangerObjectInfo[] ItemTypeDescriptions { get; set; }

        public ExchangeTypesItemsExchangerDescriptionForUserMessage()
        {
        }

        public ExchangeTypesItemsExchangerDescriptionForUserMessage(BidExchangerObjectInfo[] itemTypeDescriptions)
        {
            ItemTypeDescriptions = itemTypeDescriptions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ItemTypeDescriptions.Length);
            for (var i = 0; i < ItemTypeDescriptions.Length; i++)
            {
                ItemTypeDescriptions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ItemTypeDescriptions = new BidExchangerObjectInfo[reader.ReadShort()];
            for (var i = 0; i < ItemTypeDescriptions.Length; i++)
            {
                ItemTypeDescriptions[i] = new BidExchangerObjectInfo();
                ItemTypeDescriptions[i].Deserialize(reader);
            }
        }
    }
}