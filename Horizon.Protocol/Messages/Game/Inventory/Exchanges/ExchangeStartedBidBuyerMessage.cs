using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartedBidBuyerMessage : NetworkMessage
    {
        public override short Protocol => 5904;

        public SellerBuyerDescriptor BuyerDescriptor { get; set; }

        public ExchangeStartedBidBuyerMessage()
        {
        }

        public ExchangeStartedBidBuyerMessage(SellerBuyerDescriptor buyerDescriptor)
        {
            BuyerDescriptor = buyerDescriptor;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            BuyerDescriptor.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BuyerDescriptor = new SellerBuyerDescriptor();
            BuyerDescriptor.Deserialize(reader);
        }
    }
}