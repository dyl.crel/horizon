namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkJobIndexMessage : NetworkMessage
    {
        public override short Protocol => 5819;

        public int[] Jobs { get; set; }

        public ExchangeStartOkJobIndexMessage()
        {
        }

        public ExchangeStartOkJobIndexMessage(int[] jobs)
        {
            Jobs = jobs;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Jobs.Length);
            for (var i = 0; i < Jobs.Length; i++)
            {
                writer.WriteVarInt(Jobs[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Jobs = new int[reader.ReadShort()];
            for (var i = 0; i < Jobs.Length; i++)
            {
                Jobs[i] = reader.ReadVarInt();
            }
        }
    }
}