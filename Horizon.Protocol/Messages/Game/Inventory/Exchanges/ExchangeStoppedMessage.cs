namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStoppedMessage : NetworkMessage
    {
        public override short Protocol => 6589;

        public long Id { get; set; }

        public ExchangeStoppedMessage()
        {
        }

        public ExchangeStoppedMessage(long id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarLong();
        }
    }
}