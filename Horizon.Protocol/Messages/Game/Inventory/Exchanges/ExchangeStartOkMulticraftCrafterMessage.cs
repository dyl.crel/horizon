namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkMulticraftCrafterMessage : NetworkMessage
    {
        public override short Protocol => 5818;

        public int SkillId { get; set; }

        public ExchangeStartOkMulticraftCrafterMessage()
        {
        }

        public ExchangeStartOkMulticraftCrafterMessage(int skillId)
        {
            SkillId = skillId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(SkillId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SkillId = reader.ReadVarInt();
        }
    }
}