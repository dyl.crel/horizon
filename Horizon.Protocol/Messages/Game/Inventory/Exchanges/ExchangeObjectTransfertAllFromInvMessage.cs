namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectTransfertAllFromInvMessage : NetworkMessage
    {
        public override short Protocol => 6184;

        public ExchangeObjectTransfertAllFromInvMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}