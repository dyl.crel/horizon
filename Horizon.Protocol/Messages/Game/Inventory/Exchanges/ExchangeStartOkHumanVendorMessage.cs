using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkHumanVendorMessage : NetworkMessage
    {
        public override short Protocol => 5767;

        public double SellerId { get; set; }

        public ObjectItemToSellInHumanVendorShop[] ObjectsInfos { get; set; }

        public ExchangeStartOkHumanVendorMessage()
        {
        }

        public ExchangeStartOkHumanVendorMessage(double sellerId, ObjectItemToSellInHumanVendorShop[] objectsInfos)
        {
            SellerId = sellerId;
            ObjectsInfos = objectsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(SellerId);
            writer.WriteShort(ObjectsInfos.Length);
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SellerId = reader.ReadDouble();
            ObjectsInfos = new ObjectItemToSellInHumanVendorShop[reader.ReadShort()];
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i] = new ObjectItemToSellInHumanVendorShop();
                ObjectsInfos[i].Deserialize(reader);
            }
        }
    }
}