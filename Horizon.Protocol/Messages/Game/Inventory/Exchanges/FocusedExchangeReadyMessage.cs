namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class FocusedExchangeReadyMessage : ExchangeReadyMessage
    {
        public override short Protocol => 6701;

        public int FocusActionId { get; set; }

        public FocusedExchangeReadyMessage()
        {
        }

        public FocusedExchangeReadyMessage(bool ready, short step, int focusActionId) : base(ready, step)
        {
            FocusActionId = focusActionId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(FocusActionId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FocusActionId = reader.ReadVarInt();
        }
    }
}