namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeOnHumanVendorRequestMessage : NetworkMessage
    {
        public override short Protocol => 5772;

        public long HumanVendorId { get; set; }

        public short HumanVendorCell { get; set; }

        public ExchangeOnHumanVendorRequestMessage()
        {
        }

        public ExchangeOnHumanVendorRequestMessage(long humanVendorId, short humanVendorCell)
        {
            HumanVendorId = humanVendorId;
            HumanVendorCell = humanVendorCell;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(HumanVendorId);
            writer.WriteVarShort(HumanVendorCell);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HumanVendorId = reader.ReadVarLong();
            HumanVendorCell = reader.ReadVarShort();
        }
    }
}