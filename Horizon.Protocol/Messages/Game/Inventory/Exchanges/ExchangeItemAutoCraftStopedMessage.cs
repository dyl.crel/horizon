namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeItemAutoCraftStopedMessage : NetworkMessage
    {
        public override short Protocol => 5810;

        public byte Reason { get; set; }

        public ExchangeItemAutoCraftStopedMessage()
        {
        }

        public ExchangeItemAutoCraftStopedMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}