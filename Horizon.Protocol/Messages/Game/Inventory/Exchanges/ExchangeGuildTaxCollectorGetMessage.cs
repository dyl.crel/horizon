using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeGuildTaxCollectorGetMessage : NetworkMessage
    {
        public override short Protocol => 5762;

        public string CollectorName { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public string UserName { get; set; }

        public long CallerId { get; set; }

        public string CallerName { get; set; }

        public double Experience { get; set; }

        public short Pods { get; set; }

        public ObjectItemGenericQuantity[] ObjectsInfos { get; set; }

        public ExchangeGuildTaxCollectorGetMessage()
        {
        }

        public ExchangeGuildTaxCollectorGetMessage(string collectorName, short worldX, short worldY, double mapId, short subAreaId, string userName, long callerId, string callerName, double experience, short pods, ObjectItemGenericQuantity[] objectsInfos)
        {
            CollectorName = collectorName;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            UserName = userName;
            CallerId = callerId;
            CallerName = callerName;
            Experience = experience;
            Pods = pods;
            ObjectsInfos = objectsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(CollectorName);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteUTF(UserName);
            writer.WriteVarLong(CallerId);
            writer.WriteUTF(CallerName);
            writer.WriteDouble(Experience);
            writer.WriteVarShort(Pods);
            writer.WriteShort(ObjectsInfos.Length);
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CollectorName = reader.ReadUTF();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            UserName = reader.ReadUTF();
            CallerId = reader.ReadVarLong();
            CallerName = reader.ReadUTF();
            Experience = reader.ReadDouble();
            Pods = reader.ReadVarShort();
            ObjectsInfos = new ObjectItemGenericQuantity[reader.ReadShort()];
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i] = new ObjectItemGenericQuantity();
                ObjectsInfos[i].Deserialize(reader);
            }
        }
    }
}