namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseInListRemovedMessage : NetworkMessage
    {
        public override short Protocol => 5950;

        public int ItemUID { get; set; }

        public ExchangeBidHouseInListRemovedMessage()
        {
        }

        public ExchangeBidHouseInListRemovedMessage(int itemUID)
        {
            ItemUID = itemUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(ItemUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ItemUID = reader.ReadInt();
        }
    }
}