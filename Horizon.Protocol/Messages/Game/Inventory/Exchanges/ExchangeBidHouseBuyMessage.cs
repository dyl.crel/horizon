namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseBuyMessage : NetworkMessage
    {
        public override short Protocol => 5804;

        public int Uid { get; set; }

        public int Qty { get; set; }

        public long Price { get; set; }

        public ExchangeBidHouseBuyMessage()
        {
        }

        public ExchangeBidHouseBuyMessage(int uid, int qty, long price)
        {
            Uid = uid;
            Qty = qty;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Uid);
            writer.WriteVarInt(Qty);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Uid = reader.ReadVarInt();
            Qty = reader.ReadVarInt();
            Price = reader.ReadVarLong();
        }
    }
}