using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountsPaddockAddMessage : NetworkMessage
    {
        public override short Protocol => 6561;

        public MountClientData[] MountDescription { get; set; }

        public ExchangeMountsPaddockAddMessage()
        {
        }

        public ExchangeMountsPaddockAddMessage(MountClientData[] mountDescription)
        {
            MountDescription = mountDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(MountDescription.Length);
            for (var i = 0; i < MountDescription.Length; i++)
            {
                MountDescription[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MountDescription = new MountClientData[reader.ReadShort()];
            for (var i = 0; i < MountDescription.Length; i++)
            {
                MountDescription[i] = new MountClientData();
                MountDescription[i].Deserialize(reader);
            }
        }
    }
}