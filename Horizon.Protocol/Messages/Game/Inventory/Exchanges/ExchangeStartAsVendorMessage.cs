namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartAsVendorMessage : NetworkMessage
    {
        public override short Protocol => 5775;

        public ExchangeStartAsVendorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}