namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartedMessage : NetworkMessage
    {
        public override short Protocol => 5512;

        public byte ExchangeType { get; set; }

        public ExchangeStartedMessage()
        {
        }

        public ExchangeStartedMessage(byte exchangeType)
        {
            ExchangeType = exchangeType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ExchangeType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExchangeType = reader.ReadByte();
        }
    }
}