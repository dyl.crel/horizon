namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountSterilizeFromPaddockMessage : NetworkMessage
    {
        public override short Protocol => 6056;

        public string Name { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public string Sterilizator { get; set; }

        public ExchangeMountSterilizeFromPaddockMessage()
        {
        }

        public ExchangeMountSterilizeFromPaddockMessage(string name, short worldX, short worldY, string sterilizator)
        {
            Name = name;
            WorldX = worldX;
            WorldY = worldY;
            Sterilizator = sterilizator;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteUTF(Sterilizator);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            Sterilizator = reader.ReadUTF();
        }
    }
}