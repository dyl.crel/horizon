namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeReplyTaxVendorMessage : NetworkMessage
    {
        public override short Protocol => 5787;

        public long ObjectValue { get; set; }

        public long TotalTaxValue { get; set; }

        public ExchangeReplyTaxVendorMessage()
        {
        }

        public ExchangeReplyTaxVendorMessage(long objectValue, long totalTaxValue)
        {
            ObjectValue = objectValue;
            TotalTaxValue = totalTaxValue;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(ObjectValue);
            writer.WriteVarLong(TotalTaxValue);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectValue = reader.ReadVarLong();
            TotalTaxValue = reader.ReadVarLong();
        }
    }
}