namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeShopStockMultiMovementRemovedMessage : NetworkMessage
    {
        public override short Protocol => 6037;

        public int[] ObjectIdList { get; set; }

        public ExchangeShopStockMultiMovementRemovedMessage()
        {
        }

        public ExchangeShopStockMultiMovementRemovedMessage(int[] objectIdList)
        {
            ObjectIdList = objectIdList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectIdList.Length);
            for (var i = 0; i < ObjectIdList.Length; i++)
            {
                writer.WriteVarInt(ObjectIdList[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectIdList = new int[reader.ReadShort()];
            for (var i = 0; i < ObjectIdList.Length; i++)
            {
                ObjectIdList[i] = reader.ReadVarInt();
            }
        }
    }
}