using Horizon.Protocol.Types.Game.Context.Roleplay.Job;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class DecraftResultMessage : NetworkMessage
    {
        public override short Protocol => 6569;

        public DecraftedItemStackInfo[] Results { get; set; }

        public DecraftResultMessage()
        {
        }

        public DecraftResultMessage(DecraftedItemStackInfo[] results)
        {
            Results = results;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Results.Length);
            for (var i = 0; i < Results.Length; i++)
            {
                Results[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Results = new DecraftedItemStackInfo[reader.ReadShort()];
            for (var i = 0; i < Results.Length; i++)
            {
                Results[i] = new DecraftedItemStackInfo();
                Results[i].Deserialize(reader);
            }
        }
    }
}