using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartedBidSellerMessage : NetworkMessage
    {
        public override short Protocol => 5905;

        public SellerBuyerDescriptor SellerDescriptor { get; set; }

        public ObjectItemToSellInBid[] ObjectsInfos { get; set; }

        public ExchangeStartedBidSellerMessage()
        {
        }

        public ExchangeStartedBidSellerMessage(SellerBuyerDescriptor sellerDescriptor, ObjectItemToSellInBid[] objectsInfos)
        {
            SellerDescriptor = sellerDescriptor;
            ObjectsInfos = objectsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            SellerDescriptor.Serialize(writer);
            writer.WriteShort(ObjectsInfos.Length);
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SellerDescriptor = new SellerBuyerDescriptor();
            SellerDescriptor.Deserialize(reader);
            ObjectsInfos = new ObjectItemToSellInBid[reader.ReadShort()];
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i] = new ObjectItemToSellInBid();
                ObjectsInfos[i].Deserialize(reader);
            }
        }
    }
}