namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeReadyMessage : NetworkMessage
    {
        public override short Protocol => 5511;

        public bool Ready { get; set; }

        public short Step { get; set; }

        public ExchangeReadyMessage()
        {
        }

        public ExchangeReadyMessage(bool ready, short step)
        {
            Ready = ready;
            Step = step;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Ready);
            writer.WriteVarShort(Step);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Ready = reader.ReadBoolean();
            Step = reader.ReadVarShort();
        }
    }
}