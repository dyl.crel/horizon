namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeRequestedTradeMessage : ExchangeRequestedMessage
    {
        public override short Protocol => 5523;

        public long Source { get; set; }

        public long Target { get; set; }

        public ExchangeRequestedTradeMessage()
        {
        }

        public ExchangeRequestedTradeMessage(byte exchangeType, long source, long target) : base(exchangeType)
        {
            Source = source;
            Target = target;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Source);
            writer.WriteVarLong(Target);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Source = reader.ReadVarLong();
            Target = reader.ReadVarLong();
        }
    }
}