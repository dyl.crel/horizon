namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangePlayerMultiCraftRequestMessage : ExchangeRequestMessage
    {
        public override short Protocol => 5784;

        public long Target { get; set; }

        public int SkillId { get; set; }

        public ExchangePlayerMultiCraftRequestMessage()
        {
        }

        public ExchangePlayerMultiCraftRequestMessage(byte exchangeType, long target, int skillId) : base(exchangeType)
        {
            Target = target;
            SkillId = skillId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Target);
            writer.WriteVarInt(SkillId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Target = reader.ReadVarLong();
            SkillId = reader.ReadVarInt();
        }
    }
}