namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeTypesExchangerDescriptionForUserMessage : NetworkMessage
    {
        public override short Protocol => 5765;

        public int[] TypeDescription { get; set; }

        public ExchangeTypesExchangerDescriptionForUserMessage()
        {
        }

        public ExchangeTypesExchangerDescriptionForUserMessage(int[] typeDescription)
        {
            TypeDescription = typeDescription;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(TypeDescription.Length);
            for (var i = 0; i < TypeDescription.Length; i++)
            {
                writer.WriteVarInt(TypeDescription[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TypeDescription = new int[reader.ReadShort()];
            for (var i = 0; i < TypeDescription.Length; i++)
            {
                TypeDescription[i] = reader.ReadVarInt();
            }
        }
    }
}