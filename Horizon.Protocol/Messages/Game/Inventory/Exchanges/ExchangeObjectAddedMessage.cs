using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectAddedMessage : ExchangeObjectMessage
    {
        public override short Protocol => 5516;

        public ObjectItem Object { get; set; }

        public ExchangeObjectAddedMessage()
        {
        }

        public ExchangeObjectAddedMessage(bool remote, ObjectItem @object) : base(remote)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Object.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Object = new ObjectItem();
            Object.Deserialize(reader);
        }
    }
}