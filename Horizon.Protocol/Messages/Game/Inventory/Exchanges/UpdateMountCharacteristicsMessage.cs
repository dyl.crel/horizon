using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class UpdateMountCharacteristicsMessage : NetworkMessage
    {
        public override short Protocol => 6753;

        public int RideId { get; set; }

        public UpdateMountCharacteristic[] BoostToUpdateList { get; set; }

        public UpdateMountCharacteristicsMessage()
        {
        }

        public UpdateMountCharacteristicsMessage(int rideId, UpdateMountCharacteristic[] boostToUpdateList)
        {
            RideId = rideId;
            BoostToUpdateList = boostToUpdateList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(RideId);
            writer.WriteShort(BoostToUpdateList.Length);
            for (var i = 0; i < BoostToUpdateList.Length; i++)
            {
                writer.WriteShort(BoostToUpdateList[i].Protocol);
                BoostToUpdateList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RideId = reader.ReadVarInt();
            BoostToUpdateList = new UpdateMountCharacteristic[reader.ReadShort()];
            for (var i = 0; i < BoostToUpdateList.Length; i++)
            {
                BoostToUpdateList[i] = ProtocolTypesManager.Instance<UpdateMountCharacteristic>(reader.ReadShort());
                BoostToUpdateList[i].Deserialize(reader);
            }
        }
    }
}