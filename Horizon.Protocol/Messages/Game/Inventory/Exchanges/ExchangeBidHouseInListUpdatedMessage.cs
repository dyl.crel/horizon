using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseInListUpdatedMessage : ExchangeBidHouseInListAddedMessage
    {
        public override short Protocol => 6337;

        public ExchangeBidHouseInListUpdatedMessage()
        {
        }

        public ExchangeBidHouseInListUpdatedMessage(int itemUID, int objGenericId, ObjectEffect[] effects, long[] prices) : base(itemUID, objGenericId, effects, prices)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}