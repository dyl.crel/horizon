namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangePlayerRequestMessage : ExchangeRequestMessage
    {
        public override short Protocol => 5773;

        public long Target { get; set; }

        public ExchangePlayerRequestMessage()
        {
        }

        public ExchangePlayerRequestMessage(byte exchangeType, long target) : base(exchangeType)
        {
            Target = target;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Target);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Target = reader.ReadVarLong();
        }
    }
}