namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseListMessage : NetworkMessage
    {
        public override short Protocol => 5807;

        public short Id { get; set; }

        public ExchangeBidHouseListMessage()
        {
        }

        public ExchangeBidHouseListMessage(short id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
        }
    }
}