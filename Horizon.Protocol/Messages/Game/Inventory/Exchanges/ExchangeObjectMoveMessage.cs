namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectMoveMessage : NetworkMessage
    {
        public override short Protocol => 5518;

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public ExchangeObjectMoveMessage()
        {
        }

        public ExchangeObjectMoveMessage(int objectUID, int quantity)
        {
            ObjectUID = objectUID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}