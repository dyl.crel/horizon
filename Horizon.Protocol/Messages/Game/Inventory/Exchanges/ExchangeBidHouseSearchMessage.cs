namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseSearchMessage : NetworkMessage
    {
        public override short Protocol => 5806;

        public int Type { get; set; }

        public short GenId { get; set; }

        public ExchangeBidHouseSearchMessage()
        {
        }

        public ExchangeBidHouseSearchMessage(int type, short genId)
        {
            Type = type;
            GenId = genId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Type);
            writer.WriteVarShort(GenId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadVarInt();
            GenId = reader.ReadVarShort();
        }
    }
}