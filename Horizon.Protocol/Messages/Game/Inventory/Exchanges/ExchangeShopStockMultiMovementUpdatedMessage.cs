using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeShopStockMultiMovementUpdatedMessage : NetworkMessage
    {
        public override short Protocol => 6038;

        public ObjectItemToSell[] ObjectInfoList { get; set; }

        public ExchangeShopStockMultiMovementUpdatedMessage()
        {
        }

        public ExchangeShopStockMultiMovementUpdatedMessage(ObjectItemToSell[] objectInfoList)
        {
            ObjectInfoList = objectInfoList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectInfoList.Length);
            for (var i = 0; i < ObjectInfoList.Length; i++)
            {
                ObjectInfoList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectInfoList = new ObjectItemToSell[reader.ReadShort()];
            for (var i = 0; i < ObjectInfoList.Length; i++)
            {
                ObjectInfoList[i] = new ObjectItemToSell();
                ObjectInfoList[i].Deserialize(reader);
            }
        }
    }
}