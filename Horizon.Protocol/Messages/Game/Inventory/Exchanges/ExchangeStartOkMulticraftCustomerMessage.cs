namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkMulticraftCustomerMessage : NetworkMessage
    {
        public override short Protocol => 5817;

        public int SkillId { get; set; }

        public byte CrafterJobLevel { get; set; }

        public ExchangeStartOkMulticraftCustomerMessage()
        {
        }

        public ExchangeStartOkMulticraftCustomerMessage(int skillId, byte crafterJobLevel)
        {
            SkillId = skillId;
            CrafterJobLevel = crafterJobLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(SkillId);
            writer.WriteByte(CrafterJobLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SkillId = reader.ReadVarInt();
            CrafterJobLevel = reader.ReadByte();
        }
    }
}