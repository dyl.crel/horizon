using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeShopStockStartedMessage : NetworkMessage
    {
        public override short Protocol => 5910;

        public ObjectItemToSell[] ObjectsInfos { get; set; }

        public ExchangeShopStockStartedMessage()
        {
        }

        public ExchangeShopStockStartedMessage(ObjectItemToSell[] objectsInfos)
        {
            ObjectsInfos = objectsInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectsInfos.Length);
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectsInfos = new ObjectItemToSell[reader.ReadShort()];
            for (var i = 0; i < ObjectsInfos.Length; i++)
            {
                ObjectsInfos[i] = new ObjectItemToSell();
                ObjectsInfos[i].Deserialize(reader);
            }
        }
    }
}