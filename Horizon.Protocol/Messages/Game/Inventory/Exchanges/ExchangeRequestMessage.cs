namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeRequestMessage : NetworkMessage
    {
        public override short Protocol => 5505;

        public byte ExchangeType { get; set; }

        public ExchangeRequestMessage()
        {
        }

        public ExchangeRequestMessage(byte exchangeType)
        {
            ExchangeType = exchangeType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ExchangeType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ExchangeType = reader.ReadByte();
        }
    }
}