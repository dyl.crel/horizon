namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountsTakenFromPaddockMessage : NetworkMessage
    {
        public override short Protocol => 6554;

        public string Name { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public string Ownername { get; set; }

        public ExchangeMountsTakenFromPaddockMessage()
        {
        }

        public ExchangeMountsTakenFromPaddockMessage(string name, short worldX, short worldY, string ownername)
        {
            Name = name;
            WorldX = worldX;
            WorldY = worldY;
            Ownername = ownername;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteUTF(Ownername);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            Ownername = reader.ReadUTF();
        }
    }
}