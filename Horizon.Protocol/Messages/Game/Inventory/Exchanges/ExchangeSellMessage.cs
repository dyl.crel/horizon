namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeSellMessage : NetworkMessage
    {
        public override short Protocol => 5778;

        public int ObjectToSellId { get; set; }

        public int Quantity { get; set; }

        public ExchangeSellMessage()
        {
        }

        public ExchangeSellMessage(int objectToSellId, int quantity)
        {
            ObjectToSellId = objectToSellId;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectToSellId);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectToSellId = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}