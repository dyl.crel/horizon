using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseItemAddOkMessage : NetworkMessage
    {
        public override short Protocol => 5945;

        public ObjectItemToSellInBid ItemInfo { get; set; }

        public ExchangeBidHouseItemAddOkMessage()
        {
        }

        public ExchangeBidHouseItemAddOkMessage(ObjectItemToSellInBid itemInfo)
        {
            ItemInfo = itemInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            ItemInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ItemInfo = new ObjectItemToSellInBid();
            ItemInfo.Deserialize(reader);
        }
    }
}