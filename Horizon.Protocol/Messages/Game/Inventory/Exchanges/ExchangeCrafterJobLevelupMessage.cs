namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCrafterJobLevelupMessage : NetworkMessage
    {
        public override short Protocol => 6598;

        public byte CrafterJobLevel { get; set; }

        public ExchangeCrafterJobLevelupMessage()
        {
        }

        public ExchangeCrafterJobLevelupMessage(byte crafterJobLevel)
        {
            CrafterJobLevel = crafterJobLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(CrafterJobLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CrafterJobLevel = reader.ReadByte();
        }
    }
}