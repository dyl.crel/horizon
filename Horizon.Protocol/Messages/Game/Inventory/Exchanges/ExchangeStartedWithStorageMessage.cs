namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartedWithStorageMessage : ExchangeStartedMessage
    {
        public override short Protocol => 6236;

        public int StorageMaxSlot { get; set; }

        public ExchangeStartedWithStorageMessage()
        {
        }

        public ExchangeStartedWithStorageMessage(byte exchangeType, int storageMaxSlot) : base(exchangeType)
        {
            StorageMaxSlot = storageMaxSlot;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(StorageMaxSlot);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            StorageMaxSlot = reader.ReadVarInt();
        }
    }
}