namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseGenericItemRemovedMessage : NetworkMessage
    {
        public override short Protocol => 5948;

        public short ObjGenericId { get; set; }

        public ExchangeBidHouseGenericItemRemovedMessage()
        {
        }

        public ExchangeBidHouseGenericItemRemovedMessage(short objGenericId)
        {
            ObjGenericId = objGenericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ObjGenericId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjGenericId = reader.ReadVarShort();
        }
    }
}