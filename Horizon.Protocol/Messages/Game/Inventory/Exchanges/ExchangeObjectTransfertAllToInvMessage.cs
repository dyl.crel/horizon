namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectTransfertAllToInvMessage : NetworkMessage
    {
        public override short Protocol => 6032;

        public ExchangeObjectTransfertAllToInvMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}