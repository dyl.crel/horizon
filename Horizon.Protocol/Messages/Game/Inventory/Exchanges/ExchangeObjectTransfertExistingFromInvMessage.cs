namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectTransfertExistingFromInvMessage : NetworkMessage
    {
        public override short Protocol => 6325;

        public ExchangeObjectTransfertExistingFromInvMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}