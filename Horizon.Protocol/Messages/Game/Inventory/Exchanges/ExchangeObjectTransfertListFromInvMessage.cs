namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectTransfertListFromInvMessage : NetworkMessage
    {
        public override short Protocol => 6183;

        public int[] Ids { get; set; }

        public ExchangeObjectTransfertListFromInvMessage()
        {
        }

        public ExchangeObjectTransfertListFromInvMessage(int[] ids)
        {
            Ids = ids;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Ids.Length);
            for (var i = 0; i < Ids.Length; i++)
            {
                writer.WriteVarInt(Ids[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Ids = new int[reader.ReadShort()];
            for (var i = 0; i < Ids.Length; i++)
            {
                Ids[i] = reader.ReadVarInt();
            }
        }
    }
}