namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHousePriceMessage : NetworkMessage
    {
        public override short Protocol => 5805;

        public short GenId { get; set; }

        public ExchangeBidHousePriceMessage()
        {
        }

        public ExchangeBidHousePriceMessage(short genId)
        {
            GenId = genId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(GenId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GenId = reader.ReadVarShort();
        }
    }
}