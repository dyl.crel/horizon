namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeMountFreeFromPaddockMessage : NetworkMessage
    {
        public override short Protocol => 6055;

        public string Name { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public string Liberator { get; set; }

        public ExchangeMountFreeFromPaddockMessage()
        {
        }

        public ExchangeMountFreeFromPaddockMessage(string name, short worldX, short worldY, string liberator)
        {
            Name = name;
            WorldX = worldX;
            WorldY = worldY;
            Liberator = liberator;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteUTF(Liberator);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            Liberator = reader.ReadUTF();
        }
    }
}