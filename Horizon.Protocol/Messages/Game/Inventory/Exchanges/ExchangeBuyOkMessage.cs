namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBuyOkMessage : NetworkMessage
    {
        public override short Protocol => 5759;

        public ExchangeBuyOkMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}