namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidSearchOkMessage : NetworkMessage
    {
        public override short Protocol => 5802;

        public ExchangeBidSearchOkMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}