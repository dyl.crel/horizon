namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeStartOkRecycleTradeMessage : NetworkMessage
    {
        public override short Protocol => 6600;

        public short PercentToPrism { get; set; }

        public short PercentToPlayer { get; set; }

        public ExchangeStartOkRecycleTradeMessage()
        {
        }

        public ExchangeStartOkRecycleTradeMessage(short percentToPrism, short percentToPlayer)
        {
            PercentToPrism = percentToPrism;
            PercentToPlayer = percentToPlayer;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PercentToPrism);
            writer.WriteShort(PercentToPlayer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PercentToPrism = reader.ReadShort();
            PercentToPlayer = reader.ReadShort();
        }
    }
}