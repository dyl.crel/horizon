namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseTypeMessage : NetworkMessage
    {
        public override short Protocol => 5803;

        public int Type { get; set; }

        public bool Follow { get; set; }

        public ExchangeBidHouseTypeMessage()
        {
        }

        public ExchangeBidHouseTypeMessage(int type, bool follow)
        {
            Type = type;
            Follow = follow;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Type);
            writer.WriteBoolean(Follow);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadVarInt();
            Follow = reader.ReadBoolean();
        }
    }
}