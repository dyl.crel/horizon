namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeShopStockMovementRemovedMessage : NetworkMessage
    {
        public override short Protocol => 5907;

        public int ObjectId { get; set; }

        public ExchangeShopStockMovementRemovedMessage()
        {
        }

        public ExchangeShopStockMovementRemovedMessage(int objectId)
        {
            ObjectId = objectId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectId = reader.ReadVarInt();
        }
    }
}