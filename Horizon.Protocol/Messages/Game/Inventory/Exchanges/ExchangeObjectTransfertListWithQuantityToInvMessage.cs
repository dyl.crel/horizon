namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeObjectTransfertListWithQuantityToInvMessage : NetworkMessage
    {
        public override short Protocol => 6470;

        public int[] Ids { get; set; }

        public int[] Qtys { get; set; }

        public ExchangeObjectTransfertListWithQuantityToInvMessage()
        {
        }

        public ExchangeObjectTransfertListWithQuantityToInvMessage(int[] ids, int[] qtys)
        {
            Ids = ids;
            Qtys = qtys;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Ids.Length);
            for (var i = 0; i < Ids.Length; i++)
            {
                writer.WriteVarInt(Ids[i]);
            }
            writer.WriteShort(Qtys.Length);
            for (var i = 0; i < Qtys.Length; i++)
            {
                writer.WriteVarInt(Qtys[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Ids = new int[reader.ReadShort()];
            for (var i = 0; i < Ids.Length; i++)
            {
                Ids[i] = reader.ReadVarInt();
            }
            Qtys = new int[reader.ReadShort()];
            for (var i = 0; i < Qtys.Length; i++)
            {
                Qtys[i] = reader.ReadVarInt();
            }
        }
    }
}