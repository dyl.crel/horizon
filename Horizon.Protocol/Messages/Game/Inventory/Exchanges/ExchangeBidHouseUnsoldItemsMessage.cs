using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeBidHouseUnsoldItemsMessage : NetworkMessage
    {
        public override short Protocol => 6612;

        public ObjectItemGenericQuantity[] Items { get; set; }

        public ExchangeBidHouseUnsoldItemsMessage()
        {
        }

        public ExchangeBidHouseUnsoldItemsMessage(ObjectItemGenericQuantity[] items)
        {
            Items = items;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Items.Length);
            for (var i = 0; i < Items.Length; i++)
            {
                Items[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Items = new ObjectItemGenericQuantity[reader.ReadShort()];
            for (var i = 0; i < Items.Length; i++)
            {
                Items[i] = new ObjectItemGenericQuantity();
                Items[i].Deserialize(reader);
            }
        }
    }
}