namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeWaitingResultMessage : NetworkMessage
    {
        public override short Protocol => 5786;

        public bool Bwait { get; set; }

        public ExchangeWaitingResultMessage()
        {
        }

        public ExchangeWaitingResultMessage(bool bwait)
        {
            Bwait = bwait;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Bwait);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Bwait = reader.ReadBoolean();
        }
    }
}