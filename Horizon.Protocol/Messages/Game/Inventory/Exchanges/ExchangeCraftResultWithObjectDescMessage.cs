using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftResultWithObjectDescMessage : ExchangeCraftResultMessage
    {
        public override short Protocol => 5999;

        public ObjectItemNotInContainer ObjectInfo { get; set; }

        public ExchangeCraftResultWithObjectDescMessage()
        {
        }

        public ExchangeCraftResultWithObjectDescMessage(byte craftResult, ObjectItemNotInContainer objectInfo) : base(craftResult)
        {
            ObjectInfo = objectInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            ObjectInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectInfo = new ObjectItemNotInContainer();
            ObjectInfo.Deserialize(reader);
        }
    }
}