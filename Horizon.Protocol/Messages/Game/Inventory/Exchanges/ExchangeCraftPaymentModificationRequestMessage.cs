namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftPaymentModificationRequestMessage : NetworkMessage
    {
        public override short Protocol => 6579;

        public long Quantity { get; set; }

        public ExchangeCraftPaymentModificationRequestMessage()
        {
        }

        public ExchangeCraftPaymentModificationRequestMessage(long quantity)
        {
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quantity = reader.ReadVarLong();
        }
    }
}