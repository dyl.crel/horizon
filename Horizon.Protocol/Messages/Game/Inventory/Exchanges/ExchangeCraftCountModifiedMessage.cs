namespace Horizon.Protocol.Messages.Game.Inventory.Exchanges
{
    public class ExchangeCraftCountModifiedMessage : NetworkMessage
    {
        public override short Protocol => 6595;

        public int Count { get; set; }

        public ExchangeCraftCountModifiedMessage()
        {
        }

        public ExchangeCraftCountModifiedMessage(int count)
        {
            Count = count;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Count);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Count = reader.ReadVarInt();
        }
    }
}