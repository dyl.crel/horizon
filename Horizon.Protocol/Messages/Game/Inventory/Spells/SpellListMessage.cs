using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Spells
{
    public class SpellListMessage : NetworkMessage
    {
        public override short Protocol => 1200;

        public bool SpellPrevisualization { get; set; }

        public SpellItem[] Spells { get; set; }

        public SpellListMessage()
        {
        }

        public SpellListMessage(bool spellPrevisualization, SpellItem[] spells)
        {
            SpellPrevisualization = spellPrevisualization;
            Spells = spells;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(SpellPrevisualization);
            writer.WriteShort(Spells.Length);
            for (var i = 0; i < Spells.Length; i++)
            {
                Spells[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellPrevisualization = reader.ReadBoolean();
            Spells = new SpellItem[reader.ReadShort()];
            for (var i = 0; i < Spells.Length; i++)
            {
                Spells[i] = new SpellItem();
                Spells[i].Deserialize(reader);
            }
        }
    }
}