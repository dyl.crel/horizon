using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class GoldAddedMessage : NetworkMessage
    {
        public override short Protocol => 6030;

        public GoldItem Gold { get; set; }

        public GoldAddedMessage()
        {
        }

        public GoldAddedMessage(GoldItem gold)
        {
            Gold = gold;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Gold.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Gold = new GoldItem();
            Gold.Deserialize(reader);
        }
    }
}