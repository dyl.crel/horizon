namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class InventoryWeightMessage : NetworkMessage
    {
        public override short Protocol => 3009;

        public int Weight { get; set; }

        public int WeightMax { get; set; }

        public InventoryWeightMessage()
        {
        }

        public InventoryWeightMessage(int weight, int weightMax)
        {
            Weight = weight;
            WeightMax = weightMax;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Weight);
            writer.WriteVarInt(WeightMax);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Weight = reader.ReadVarInt();
            WeightMax = reader.ReadVarInt();
        }
    }
}