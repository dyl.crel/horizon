namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class LivingObjectChangeSkinRequestMessage : NetworkMessage
    {
        public override short Protocol => 5725;

        public int LivingUID { get; set; }

        public byte LivingPosition { get; set; }

        public int SkinId { get; set; }

        public LivingObjectChangeSkinRequestMessage()
        {
        }

        public LivingObjectChangeSkinRequestMessage(int livingUID, byte livingPosition, int skinId)
        {
            LivingUID = livingUID;
            LivingPosition = livingPosition;
            SkinId = skinId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(LivingUID);
            writer.WriteByte(LivingPosition);
            writer.WriteVarInt(SkinId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            LivingUID = reader.ReadVarInt();
            LivingPosition = reader.ReadByte();
            SkinId = reader.ReadVarInt();
        }
    }
}