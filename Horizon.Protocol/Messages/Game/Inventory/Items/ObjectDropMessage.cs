namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectDropMessage : NetworkMessage
    {
        public override short Protocol => 3005;

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public ObjectDropMessage()
        {
        }

        public ObjectDropMessage(int objectUID, int quantity)
        {
            ObjectUID = objectUID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}