using Horizon.Protocol.Messages.Game.Inventory.Exchanges;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeObjectsRemovedMessage : ExchangeObjectMessage
    {
        public override short Protocol => 6532;

        public int[] ObjectUID { get; set; }

        public ExchangeObjectsRemovedMessage()
        {
        }

        public ExchangeObjectsRemovedMessage(bool remote, int[] objectUID) : base(remote)
        {
            ObjectUID = objectUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(ObjectUID.Length);
            for (var i = 0; i < ObjectUID.Length; i++)
            {
                writer.WriteVarInt(ObjectUID[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectUID = new int[reader.ReadShort()];
            for (var i = 0; i < ObjectUID.Length; i++)
            {
                ObjectUID[i] = reader.ReadVarInt();
            }
        }
    }
}