using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class InventoryContentMessage : NetworkMessage
    {
        public override short Protocol => 3016;

        public ObjectItem[] Objects { get; set; }

        public long Kamas { get; set; }

        public InventoryContentMessage()
        {
        }

        public InventoryContentMessage(ObjectItem[] objects, long kamas)
        {
            Objects = objects;
            Kamas = kamas;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Objects.Length);
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i].Serialize(writer);
            }
            writer.WriteVarLong(Kamas);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Objects = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i] = new ObjectItem();
                Objects[i].Deserialize(reader);
            }
            Kamas = reader.ReadVarLong();
        }
    }
}