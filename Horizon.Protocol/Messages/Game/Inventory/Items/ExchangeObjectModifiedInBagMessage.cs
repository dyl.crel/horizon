using Horizon.Protocol.Messages.Game.Inventory.Exchanges;
using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeObjectModifiedInBagMessage : ExchangeObjectMessage
    {
        public override short Protocol => 6008;

        public ObjectItem Object { get; set; }

        public ExchangeObjectModifiedInBagMessage()
        {
        }

        public ExchangeObjectModifiedInBagMessage(bool remote, ObjectItem @object) : base(remote)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Object.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Object = new ObjectItem();
            Object.Deserialize(reader);
        }
    }
}