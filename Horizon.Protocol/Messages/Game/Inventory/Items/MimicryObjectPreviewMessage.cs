using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class MimicryObjectPreviewMessage : NetworkMessage
    {
        public override short Protocol => 6458;

        public ObjectItem Result { get; set; }

        public MimicryObjectPreviewMessage()
        {
        }

        public MimicryObjectPreviewMessage(ObjectItem result)
        {
            Result = result;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Result.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Result = new ObjectItem();
            Result.Deserialize(reader);
        }
    }
}