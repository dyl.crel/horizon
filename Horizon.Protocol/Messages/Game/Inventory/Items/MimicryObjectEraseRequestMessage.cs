namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class MimicryObjectEraseRequestMessage : NetworkMessage
    {
        public override short Protocol => 6457;

        public int HostUID { get; set; }

        public byte HostPos { get; set; }

        public MimicryObjectEraseRequestMessage()
        {
        }

        public MimicryObjectEraseRequestMessage(int hostUID, byte hostPos)
        {
            HostUID = hostUID;
            HostPos = hostPos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HostUID);
            writer.WriteByte(HostPos);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HostUID = reader.ReadVarInt();
            HostPos = reader.ReadByte();
        }
    }
}