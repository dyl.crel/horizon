using Horizon.Protocol.Messages.Game.Inventory.Exchanges;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeKamaModifiedMessage : ExchangeObjectMessage
    {
        public override short Protocol => 5521;

        public long Quantity { get; set; }

        public ExchangeKamaModifiedMessage()
        {
        }

        public ExchangeKamaModifiedMessage(bool remote, long quantity) : base(remote)
        {
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Quantity = reader.ReadVarLong();
        }
    }
}