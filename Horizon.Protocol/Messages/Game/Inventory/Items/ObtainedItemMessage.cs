namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObtainedItemMessage : NetworkMessage
    {
        public override short Protocol => 6519;

        public short GenericId { get; set; }

        public int BaseQuantity { get; set; }

        public ObtainedItemMessage()
        {
        }

        public ObtainedItemMessage(short genericId, int @baseQuantity)
        {
            GenericId = genericId;
            BaseQuantity = @baseQuantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(GenericId);
            writer.WriteVarInt(BaseQuantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GenericId = reader.ReadVarShort();
            BaseQuantity = reader.ReadVarInt();
        }
    }
}