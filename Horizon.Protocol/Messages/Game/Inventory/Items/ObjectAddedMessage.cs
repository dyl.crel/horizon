using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectAddedMessage : NetworkMessage
    {
        public override short Protocol => 3025;

        public ObjectItem Object { get; set; }

        public byte Origin { get; set; }

        public ObjectAddedMessage()
        {
        }

        public ObjectAddedMessage(ObjectItem @object, byte origin)
        {
            Object = @object;
            Origin = origin;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Object.Serialize(writer);
            writer.WriteByte(Origin);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Object = new ObjectItem();
            Object.Deserialize(reader);
            Origin = reader.ReadByte();
        }
    }
}