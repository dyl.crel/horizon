namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectUseOnCharacterMessage : ObjectUseMessage
    {
        public override short Protocol => 3003;

        public long CharacterId { get; set; }

        public ObjectUseOnCharacterMessage()
        {
        }

        public ObjectUseOnCharacterMessage(int objectUID, long characterId) : base(objectUID)
        {
            CharacterId = characterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(CharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CharacterId = reader.ReadVarLong();
        }
    }
}