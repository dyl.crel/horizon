namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class LivingObjectMessageRequestMessage : NetworkMessage
    {
        public override short Protocol => 6066;

        public short MsgId { get; set; }

        public string[] Parameters { get; set; }

        public int LivingObject { get; set; }

        public LivingObjectMessageRequestMessage()
        {
        }

        public LivingObjectMessageRequestMessage(short msgId, string[] parameters, int livingObject)
        {
            MsgId = msgId;
            Parameters = parameters;
            LivingObject = livingObject;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(MsgId);
            writer.WriteShort(Parameters.Length);
            for (var i = 0; i < Parameters.Length; i++)
            {
                writer.WriteUTF(Parameters[i]);
            }
            writer.WriteVarInt(LivingObject);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MsgId = reader.ReadVarShort();
            Parameters = new string[reader.ReadShort()];
            for (var i = 0; i < Parameters.Length; i++)
            {
                Parameters[i] = reader.ReadUTF();
            }
            LivingObject = reader.ReadVarInt();
        }
    }
}