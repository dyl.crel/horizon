namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class MimicryObjectFeedAndAssociateRequestMessage : SymbioticObjectAssociateRequestMessage
    {
        public override short Protocol => 6460;

        public int FoodUID { get; set; }

        public byte FoodPos { get; set; }

        public bool Preview { get; set; }

        public MimicryObjectFeedAndAssociateRequestMessage()
        {
        }

        public MimicryObjectFeedAndAssociateRequestMessage(int symbioteUID, byte symbiotePos, int hostUID, byte hostPos, int foodUID, byte foodPos, bool preview) : base(symbioteUID, symbiotePos, hostUID, hostPos)
        {
            FoodUID = foodUID;
            FoodPos = foodPos;
            Preview = preview;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(FoodUID);
            writer.WriteByte(FoodPos);
            writer.WriteBoolean(Preview);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FoodUID = reader.ReadVarInt();
            FoodPos = reader.ReadByte();
            Preview = reader.ReadBoolean();
        }
    }
}