namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class SymbioticObjectAssociatedMessage : NetworkMessage
    {
        public override short Protocol => 6527;

        public int HostUID { get; set; }

        public SymbioticObjectAssociatedMessage()
        {
        }

        public SymbioticObjectAssociatedMessage(int hostUID)
        {
            HostUID = hostUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HostUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HostUID = reader.ReadVarInt();
        }
    }
}