namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class LivingObjectMessageMessage : NetworkMessage
    {
        public override short Protocol => 6065;

        public short MsgId { get; set; }

        public int TimeStamp { get; set; }

        public string Owner { get; set; }

        public short ObjectGenericId { get; set; }

        public LivingObjectMessageMessage()
        {
        }

        public LivingObjectMessageMessage(short msgId, int timeStamp, string owner, short objectGenericId)
        {
            MsgId = msgId;
            TimeStamp = timeStamp;
            Owner = owner;
            ObjectGenericId = objectGenericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(MsgId);
            writer.WriteInt(TimeStamp);
            writer.WriteUTF(Owner);
            writer.WriteVarShort(ObjectGenericId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MsgId = reader.ReadVarShort();
            TimeStamp = reader.ReadInt();
            Owner = reader.ReadUTF();
            ObjectGenericId = reader.ReadVarShort();
        }
    }
}