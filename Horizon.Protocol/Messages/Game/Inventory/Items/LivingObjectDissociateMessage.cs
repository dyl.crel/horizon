namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class LivingObjectDissociateMessage : NetworkMessage
    {
        public override short Protocol => 5723;

        public int LivingUID { get; set; }

        public byte LivingPosition { get; set; }

        public LivingObjectDissociateMessage()
        {
        }

        public LivingObjectDissociateMessage(int livingUID, byte livingPosition)
        {
            LivingUID = livingUID;
            LivingPosition = livingPosition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(LivingUID);
            writer.WriteByte(LivingPosition);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            LivingUID = reader.ReadVarInt();
            LivingPosition = reader.ReadByte();
        }
    }
}