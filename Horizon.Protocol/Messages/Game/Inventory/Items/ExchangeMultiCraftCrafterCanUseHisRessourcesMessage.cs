namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeMultiCraftCrafterCanUseHisRessourcesMessage : NetworkMessage
    {
        public override short Protocol => 6020;

        public bool Allowed { get; set; }

        public ExchangeMultiCraftCrafterCanUseHisRessourcesMessage()
        {
        }

        public ExchangeMultiCraftCrafterCanUseHisRessourcesMessage(bool allowed)
        {
            Allowed = allowed;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Allowed);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Allowed = reader.ReadBoolean();
        }
    }
}