namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectJobAddedMessage : NetworkMessage
    {
        public override short Protocol => 6014;

        public byte JobId { get; set; }

        public ObjectJobAddedMessage()
        {
        }

        public ObjectJobAddedMessage(byte jobId)
        {
            JobId = jobId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
        }
    }
}