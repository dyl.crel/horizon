namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectErrorMessage : NetworkMessage
    {
        public override short Protocol => 3004;

        public byte Reason { get; set; }

        public ObjectErrorMessage()
        {
        }

        public ObjectErrorMessage(byte reason)
        {
            Reason = reason;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Reason);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Reason = reader.ReadByte();
        }
    }
}