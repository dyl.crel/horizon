using Horizon.Protocol.Messages.Game.Inventory.Exchanges;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeObjectRemovedFromBagMessage : ExchangeObjectMessage
    {
        public override short Protocol => 6010;

        public int ObjectUID { get; set; }

        public ExchangeObjectRemovedFromBagMessage()
        {
        }

        public ExchangeObjectRemovedFromBagMessage(bool remote, int objectUID) : base(remote)
        {
            ObjectUID = objectUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(ObjectUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectUID = reader.ReadVarInt();
        }
    }
}