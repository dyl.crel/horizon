using Horizon.Protocol.Messages.Game.Inventory.Exchanges;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangePodsModifiedMessage : ExchangeObjectMessage
    {
        public override short Protocol => 6670;

        public int CurrentWeight { get; set; }

        public int MaxWeight { get; set; }

        public ExchangePodsModifiedMessage()
        {
        }

        public ExchangePodsModifiedMessage(bool remote, int currentWeight, int maxWeight) : base(remote)
        {
            CurrentWeight = currentWeight;
            MaxWeight = maxWeight;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(CurrentWeight);
            writer.WriteVarInt(MaxWeight);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CurrentWeight = reader.ReadVarInt();
            MaxWeight = reader.ReadVarInt();
        }
    }
}