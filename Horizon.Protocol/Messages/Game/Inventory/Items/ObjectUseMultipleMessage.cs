namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectUseMultipleMessage : ObjectUseMessage
    {
        public override short Protocol => 6234;

        public int Quantity { get; set; }

        public ObjectUseMultipleMessage()
        {
        }

        public ObjectUseMultipleMessage(int objectUID, int quantity) : base(objectUID)
        {
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Quantity = reader.ReadVarInt();
        }
    }
}