namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectDeleteMessage : NetworkMessage
    {
        public override short Protocol => 3022;

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public ObjectDeleteMessage()
        {
        }

        public ObjectDeleteMessage(int objectUID, int quantity)
        {
            ObjectUID = objectUID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}