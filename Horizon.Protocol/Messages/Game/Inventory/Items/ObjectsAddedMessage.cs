using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectsAddedMessage : NetworkMessage
    {
        public override short Protocol => 6033;

        public ObjectItem[] Object { get; set; }

        public ObjectsAddedMessage()
        {
        }

        public ObjectsAddedMessage(ObjectItem[] @object)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Object.Length);
            for (var i = 0; i < Object.Length; i++)
            {
                Object[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Object = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < Object.Length; i++)
            {
                Object[i] = new ObjectItem();
                Object[i].Deserialize(reader);
            }
        }
    }
}