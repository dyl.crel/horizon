namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObtainedItemWithBonusMessage : ObtainedItemMessage
    {
        public override short Protocol => 6520;

        public int BonusQuantity { get; set; }

        public ObtainedItemWithBonusMessage()
        {
        }

        public ObtainedItemWithBonusMessage(short genericId, int @baseQuantity, int bonusQuantity) : base(genericId, @baseQuantity)
        {
            BonusQuantity = bonusQuantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(BonusQuantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            BonusQuantity = reader.ReadVarInt();
        }
    }
}