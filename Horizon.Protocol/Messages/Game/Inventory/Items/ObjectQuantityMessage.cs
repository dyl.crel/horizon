namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectQuantityMessage : NetworkMessage
    {
        public override short Protocol => 3023;

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public byte Origin { get; set; }

        public ObjectQuantityMessage()
        {
        }

        public ObjectQuantityMessage(int objectUID, int quantity, byte origin)
        {
            ObjectUID = objectUID;
            Quantity = quantity;
            Origin = origin;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
            writer.WriteByte(Origin);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
            Origin = reader.ReadByte();
        }
    }
}