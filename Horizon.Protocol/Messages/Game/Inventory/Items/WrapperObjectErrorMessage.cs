namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class WrapperObjectErrorMessage : SymbioticObjectErrorMessage
    {
        public override short Protocol => 6529;

        public WrapperObjectErrorMessage()
        {
        }

        public WrapperObjectErrorMessage(byte reason, byte errorCode) : base(reason, errorCode)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}