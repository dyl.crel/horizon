namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage : NetworkMessage
    {
        public override short Protocol => 6021;

        public bool Allow { get; set; }

        public ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage()
        {
        }

        public ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage(bool allow)
        {
            Allow = allow;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteBoolean(Allow);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Allow = reader.ReadBoolean();
        }
    }
}