namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class MimicryObjectAssociatedMessage : SymbioticObjectAssociatedMessage
    {
        public override short Protocol => 6462;

        public MimicryObjectAssociatedMessage()
        {
        }

        public MimicryObjectAssociatedMessage(int hostUID) : base(hostUID)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}