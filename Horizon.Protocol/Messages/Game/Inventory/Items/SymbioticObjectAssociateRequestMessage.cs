namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class SymbioticObjectAssociateRequestMessage : NetworkMessage
    {
        public override short Protocol => 6522;

        public int SymbioteUID { get; set; }

        public byte SymbiotePos { get; set; }

        public int HostUID { get; set; }

        public byte HostPos { get; set; }

        public SymbioticObjectAssociateRequestMessage()
        {
        }

        public SymbioticObjectAssociateRequestMessage(int symbioteUID, byte symbiotePos, int hostUID, byte hostPos)
        {
            SymbioteUID = symbioteUID;
            SymbiotePos = symbiotePos;
            HostUID = hostUID;
            HostPos = hostPos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(SymbioteUID);
            writer.WriteByte(SymbiotePos);
            writer.WriteVarInt(HostUID);
            writer.WriteByte(HostPos);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SymbioteUID = reader.ReadVarInt();
            SymbiotePos = reader.ReadByte();
            HostUID = reader.ReadVarInt();
            HostPos = reader.ReadByte();
        }
    }
}