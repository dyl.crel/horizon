namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class SymbioticObjectErrorMessage : ObjectErrorMessage
    {
        public override short Protocol => 6526;

        public byte ErrorCode { get; set; }

        public SymbioticObjectErrorMessage()
        {
        }

        public SymbioticObjectErrorMessage(byte reason, byte errorCode) : base(reason)
        {
            ErrorCode = errorCode;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(ErrorCode);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ErrorCode = reader.ReadByte();
        }
    }
}