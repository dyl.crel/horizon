namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectUseMessage : NetworkMessage
    {
        public override short Protocol => 3019;

        public int ObjectUID { get; set; }

        public ObjectUseMessage()
        {
        }

        public ObjectUseMessage(int objectUID)
        {
            ObjectUID = objectUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
        }
    }
}