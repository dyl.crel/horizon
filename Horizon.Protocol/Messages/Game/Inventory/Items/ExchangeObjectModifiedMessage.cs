using Horizon.Protocol.Messages.Game.Inventory.Exchanges;
using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ExchangeObjectModifiedMessage : ExchangeObjectMessage
    {
        public override short Protocol => 5519;

        public ObjectItem Object { get; set; }

        public ExchangeObjectModifiedMessage()
        {
        }

        public ExchangeObjectModifiedMessage(bool remote, ObjectItem @object) : base(remote)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Object.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Object = new ObjectItem();
            Object.Deserialize(reader);
        }
    }
}