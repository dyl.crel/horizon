namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectDeletedMessage : NetworkMessage
    {
        public override short Protocol => 3024;

        public int ObjectUID { get; set; }

        public ObjectDeletedMessage()
        {
        }

        public ObjectDeletedMessage(int objectUID)
        {
            ObjectUID = objectUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
        }
    }
}