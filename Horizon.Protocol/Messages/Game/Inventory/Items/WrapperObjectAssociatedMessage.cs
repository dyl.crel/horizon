namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class WrapperObjectAssociatedMessage : SymbioticObjectAssociatedMessage
    {
        public override short Protocol => 6523;

        public WrapperObjectAssociatedMessage()
        {
        }

        public WrapperObjectAssociatedMessage(int hostUID) : base(hostUID)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}