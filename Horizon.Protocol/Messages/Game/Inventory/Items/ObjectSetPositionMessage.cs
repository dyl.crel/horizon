namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectSetPositionMessage : NetworkMessage
    {
        public override short Protocol => 3021;

        public int ObjectUID { get; set; }

        public short Position { get; set; }

        public int Quantity { get; set; }

        public ObjectSetPositionMessage()
        {
        }

        public ObjectSetPositionMessage(int objectUID, short position, int quantity)
        {
            ObjectUID = objectUID;
            Position = position;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteShort(Position);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Position = reader.ReadShort();
            Quantity = reader.ReadVarInt();
        }
    }
}