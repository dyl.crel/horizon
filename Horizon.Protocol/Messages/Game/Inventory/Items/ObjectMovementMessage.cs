namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectMovementMessage : NetworkMessage
    {
        public override short Protocol => 3010;

        public int ObjectUID { get; set; }

        public short Position { get; set; }

        public ObjectMovementMessage()
        {
        }

        public ObjectMovementMessage(int objectUID, short position)
        {
            ObjectUID = objectUID;
            Position = position;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteShort(Position);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Position = reader.ReadShort();
        }
    }
}