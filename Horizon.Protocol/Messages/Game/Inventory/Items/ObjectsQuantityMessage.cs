using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectsQuantityMessage : NetworkMessage
    {
        public override short Protocol => 6206;

        public ObjectItemQuantity[] ObjectsUIDAndQty { get; set; }

        public ObjectsQuantityMessage()
        {
        }

        public ObjectsQuantityMessage(ObjectItemQuantity[] objectsUIDAndQty)
        {
            ObjectsUIDAndQty = objectsUIDAndQty;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectsUIDAndQty.Length);
            for (var i = 0; i < ObjectsUIDAndQty.Length; i++)
            {
                ObjectsUIDAndQty[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectsUIDAndQty = new ObjectItemQuantity[reader.ReadShort()];
            for (var i = 0; i < ObjectsUIDAndQty.Length; i++)
            {
                ObjectsUIDAndQty[i] = new ObjectItemQuantity();
                ObjectsUIDAndQty[i].Deserialize(reader);
            }
        }
    }
}