using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class SetUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5503;

        public short SetId { get; set; }

        public short[] SetObjects { get; set; }

        public ObjectEffect[] SetEffects { get; set; }

        public SetUpdateMessage()
        {
        }

        public SetUpdateMessage(short setId, short[] setObjects, ObjectEffect[] setEffects)
        {
            SetId = setId;
            SetObjects = setObjects;
            SetEffects = setEffects;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SetId);
            writer.WriteShort(SetObjects.Length);
            for (var i = 0; i < SetObjects.Length; i++)
            {
                writer.WriteVarShort(SetObjects[i]);
            }
            writer.WriteShort(SetEffects.Length);
            for (var i = 0; i < SetEffects.Length; i++)
            {
                writer.WriteShort(SetEffects[i].Protocol);
                SetEffects[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SetId = reader.ReadVarShort();
            SetObjects = new short[reader.ReadShort()];
            for (var i = 0; i < SetObjects.Length; i++)
            {
                SetObjects[i] = reader.ReadVarShort();
            }
            SetEffects = new ObjectEffect[reader.ReadShort()];
            for (var i = 0; i < SetEffects.Length; i++)
            {
                SetEffects[i] = ProtocolTypesManager.Instance<ObjectEffect>(reader.ReadShort());
                SetEffects[i].Deserialize(reader);
            }
        }
    }
}