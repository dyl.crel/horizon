using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectModifiedMessage : NetworkMessage
    {
        public override short Protocol => 3029;

        public ObjectItem Object { get; set; }

        public ObjectModifiedMessage()
        {
        }

        public ObjectModifiedMessage(ObjectItem @object)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Object.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Object = new ObjectItem();
            Object.Deserialize(reader);
        }
    }
}