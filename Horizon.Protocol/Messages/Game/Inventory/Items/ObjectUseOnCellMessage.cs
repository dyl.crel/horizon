namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectUseOnCellMessage : ObjectUseMessage
    {
        public override short Protocol => 3013;

        public short Cells { get; set; }

        public ObjectUseOnCellMessage()
        {
        }

        public ObjectUseOnCellMessage(int objectUID, short cells) : base(objectUID)
        {
            Cells = cells;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Cells);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Cells = reader.ReadVarShort();
        }
    }
}