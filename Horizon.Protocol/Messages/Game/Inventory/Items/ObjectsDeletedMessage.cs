namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class ObjectsDeletedMessage : NetworkMessage
    {
        public override short Protocol => 6034;

        public int[] ObjectUID { get; set; }

        public ObjectsDeletedMessage()
        {
        }

        public ObjectsDeletedMessage(int[] objectUID)
        {
            ObjectUID = objectUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectUID.Length);
            for (var i = 0; i < ObjectUID.Length; i++)
            {
                writer.WriteVarInt(ObjectUID[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = new int[reader.ReadShort()];
            for (var i = 0; i < ObjectUID.Length; i++)
            {
                ObjectUID[i] = reader.ReadVarInt();
            }
        }
    }
}