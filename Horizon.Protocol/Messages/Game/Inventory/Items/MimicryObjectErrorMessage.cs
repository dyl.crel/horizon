namespace Horizon.Protocol.Messages.Game.Inventory.Items
{
    public class MimicryObjectErrorMessage : SymbioticObjectErrorMessage
    {
        public override short Protocol => 6461;

        public bool Preview { get; set; }

        public MimicryObjectErrorMessage()
        {
        }

        public MimicryObjectErrorMessage(byte reason, byte errorCode, bool preview) : base(reason, errorCode)
        {
            Preview = preview;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Preview);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Preview = reader.ReadBoolean();
        }
    }
}