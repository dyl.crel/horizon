namespace Horizon.Protocol.Messages.Game.Inventory.Storage
{
    public class StorageObjectRemoveMessage : NetworkMessage
    {
        public override short Protocol => 5648;

        public int ObjectUID { get; set; }

        public StorageObjectRemoveMessage()
        {
        }

        public StorageObjectRemoveMessage(int objectUID)
        {
            ObjectUID = objectUID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
        }
    }
}