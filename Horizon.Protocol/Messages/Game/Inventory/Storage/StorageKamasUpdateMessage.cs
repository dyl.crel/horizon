namespace Horizon.Protocol.Messages.Game.Inventory.Storage
{
    public class StorageKamasUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5645;

        public long KamasTotal { get; set; }

        public StorageKamasUpdateMessage()
        {
        }

        public StorageKamasUpdateMessage(long kamasTotal)
        {
            KamasTotal = kamasTotal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(KamasTotal);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            KamasTotal = reader.ReadVarLong();
        }
    }
}