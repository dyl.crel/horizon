using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Storage
{
    public class StorageObjectsUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6036;

        public ObjectItem[] ObjectList { get; set; }

        public StorageObjectsUpdateMessage()
        {
        }

        public StorageObjectsUpdateMessage(ObjectItem[] objectList)
        {
            ObjectList = objectList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectList.Length);
            for (var i = 0; i < ObjectList.Length; i++)
            {
                ObjectList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectList = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < ObjectList.Length; i++)
            {
                ObjectList[i] = new ObjectItem();
                ObjectList[i].Deserialize(reader);
            }
        }
    }
}