namespace Horizon.Protocol.Messages.Game.Inventory.Storage
{
    public class StorageObjectsRemoveMessage : NetworkMessage
    {
        public override short Protocol => 6035;

        public int[] ObjectUIDList { get; set; }

        public StorageObjectsRemoveMessage()
        {
        }

        public StorageObjectsRemoveMessage(int[] objectUIDList)
        {
            ObjectUIDList = objectUIDList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(ObjectUIDList.Length);
            for (var i = 0; i < ObjectUIDList.Length; i++)
            {
                writer.WriteVarInt(ObjectUIDList[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUIDList = new int[reader.ReadShort()];
            for (var i = 0; i < ObjectUIDList.Length; i++)
            {
                ObjectUIDList[i] = reader.ReadVarInt();
            }
        }
    }
}