using Horizon.Protocol.Messages.Game.Inventory.Items;
using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Storage
{
    public class StorageInventoryContentMessage : InventoryContentMessage
    {
        public override short Protocol => 5646;

        public StorageInventoryContentMessage()
        {
        }

        public StorageInventoryContentMessage(ObjectItem[] objects, long kamas) : base(objects, kamas)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}