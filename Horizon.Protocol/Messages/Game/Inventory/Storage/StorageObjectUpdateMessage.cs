using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Messages.Game.Inventory.Storage
{
    public class StorageObjectUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5647;

        public ObjectItem Object { get; set; }

        public StorageObjectUpdateMessage()
        {
        }

        public StorageObjectUpdateMessage(ObjectItem @object)
        {
            Object = @object;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Object.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Object = new ObjectItem();
            Object.Deserialize(reader);
        }
    }
}