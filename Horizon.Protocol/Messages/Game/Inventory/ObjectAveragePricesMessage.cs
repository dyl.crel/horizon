namespace Horizon.Protocol.Messages.Game.Inventory
{
    public class ObjectAveragePricesMessage : NetworkMessage
    {
        public override short Protocol => 6335;

        public short[] Ids { get; set; }

        public long[] AvgPrices { get; set; }

        public ObjectAveragePricesMessage()
        {
        }

        public ObjectAveragePricesMessage(short[] ids, long[] avgPrices)
        {
            Ids = ids;
            AvgPrices = avgPrices;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Ids.Length);
            for (var i = 0; i < Ids.Length; i++)
            {
                writer.WriteVarShort(Ids[i]);
            }
            writer.WriteShort(AvgPrices.Length);
            for (var i = 0; i < AvgPrices.Length; i++)
            {
                writer.WriteVarLong(AvgPrices[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Ids = new short[reader.ReadShort()];
            for (var i = 0; i < Ids.Length; i++)
            {
                Ids[i] = reader.ReadVarShort();
            }
            AvgPrices = new long[reader.ReadShort()];
            for (var i = 0; i < AvgPrices.Length; i++)
            {
                AvgPrices[i] = reader.ReadVarLong();
            }
        }
    }
}