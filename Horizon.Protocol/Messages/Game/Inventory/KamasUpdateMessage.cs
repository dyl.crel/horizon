namespace Horizon.Protocol.Messages.Game.Inventory
{
    public class KamasUpdateMessage : NetworkMessage
    {
        public override short Protocol => 5537;

        public long KamasTotal { get; set; }

        public KamasUpdateMessage()
        {
        }

        public KamasUpdateMessage(long kamasTotal)
        {
            KamasTotal = kamasTotal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(KamasTotal);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            KamasTotal = reader.ReadVarLong();
        }
    }
}