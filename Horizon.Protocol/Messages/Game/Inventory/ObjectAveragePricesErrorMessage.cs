namespace Horizon.Protocol.Messages.Game.Inventory
{
    public class ObjectAveragePricesErrorMessage : NetworkMessage
    {
        public override short Protocol => 6336;

        public ObjectAveragePricesErrorMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}