namespace Horizon.Protocol.Messages.Game.Inventory
{
    public class ObjectAveragePricesGetMessage : NetworkMessage
    {
        public override short Protocol => 6334;

        public ObjectAveragePricesGetMessage()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}