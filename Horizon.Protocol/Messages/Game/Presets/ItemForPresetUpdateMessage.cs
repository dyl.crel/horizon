using Horizon.Protocol.Types.Game.Presets;

namespace Horizon.Protocol.Messages.Game.Presets
{
    public class ItemForPresetUpdateMessage : NetworkMessage
    {
        public override short Protocol => 6760;

        public short PresetId { get; set; }

        public ItemForPreset PresetItem { get; set; }

        public ItemForPresetUpdateMessage()
        {
        }

        public ItemForPresetUpdateMessage(short presetId, ItemForPreset presetItem)
        {
            PresetId = presetId;
            PresetItem = presetItem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PresetId);
            PresetItem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PresetId = reader.ReadShort();
            PresetItem = new ItemForPreset();
            PresetItem.Deserialize(reader);
        }
    }
}