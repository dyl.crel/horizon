namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetUseRequestMessage : NetworkMessage
    {
        public override short Protocol => 6759;

        public short PresetId { get; set; }

        public PresetUseRequestMessage()
        {
        }

        public PresetUseRequestMessage(short presetId)
        {
            PresetId = presetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PresetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PresetId = reader.ReadShort();
        }
    }
}