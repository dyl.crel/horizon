namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetDeleteRequestMessage : NetworkMessage
    {
        public override short Protocol => 6755;

        public short PresetId { get; set; }

        public PresetDeleteRequestMessage()
        {
        }

        public PresetDeleteRequestMessage(short presetId)
        {
            PresetId = presetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PresetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PresetId = reader.ReadShort();
        }
    }
}