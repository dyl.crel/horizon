namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetUseResultMessage : NetworkMessage
    {
        public override short Protocol => 6747;

        public short PresetId { get; set; }

        public byte Code { get; set; }

        public PresetUseResultMessage()
        {
        }

        public PresetUseResultMessage(short presetId, byte code)
        {
            PresetId = presetId;
            Code = code;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PresetId);
            writer.WriteByte(Code);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PresetId = reader.ReadShort();
            Code = reader.ReadByte();
        }
    }
}