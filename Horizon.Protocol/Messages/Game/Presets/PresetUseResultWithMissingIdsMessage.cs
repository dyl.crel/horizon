namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetUseResultWithMissingIdsMessage : PresetUseResultMessage
    {
        public override short Protocol => 6757;

        public short[] MissingIds { get; set; }

        public PresetUseResultWithMissingIdsMessage()
        {
        }

        public PresetUseResultWithMissingIdsMessage(short presetId, byte code, short[] missingIds) : base(presetId, code)
        {
            MissingIds = missingIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MissingIds.Length);
            for (var i = 0; i < MissingIds.Length; i++)
            {
                writer.WriteVarShort(MissingIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MissingIds = new short[reader.ReadShort()];
            for (var i = 0; i < MissingIds.Length; i++)
            {
                MissingIds[i] = reader.ReadVarShort();
            }
        }
    }
}