namespace Horizon.Protocol.Messages.Game.Presets
{
    public class CharacterPresetSaveRequestMessage : PresetSaveRequestMessage
    {
        public override short Protocol => 6756;

        public string Name { get; set; }

        public CharacterPresetSaveRequestMessage()
        {
        }

        public CharacterPresetSaveRequestMessage(short presetId, byte symbolId, bool updateData, string name) : base(presetId, symbolId, updateData)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
        }
    }
}