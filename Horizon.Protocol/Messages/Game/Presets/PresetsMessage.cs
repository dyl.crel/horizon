using Horizon.Protocol.Types.Game.Presets;

namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetsMessage : NetworkMessage
    {
        public override short Protocol => 6750;

        public Preset[] Presets { get; set; }

        public PresetsMessage()
        {
        }

        public PresetsMessage(Preset[] presets)
        {
            Presets = presets;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Presets.Length);
            for (var i = 0; i < Presets.Length; i++)
            {
                writer.WriteShort(Presets[i].Protocol);
                Presets[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Presets = new Preset[reader.ReadShort()];
            for (var i = 0; i < Presets.Length; i++)
            {
                Presets[i] = ProtocolTypesManager.Instance<Preset>(reader.ReadShort());
                Presets[i].Deserialize(reader);
            }
        }
    }
}