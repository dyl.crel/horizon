namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetSaveRequestMessage : NetworkMessage
    {
        public override short Protocol => 6761;

        public short PresetId { get; set; }

        public byte SymbolId { get; set; }

        public bool UpdateData { get; set; }

        public PresetSaveRequestMessage()
        {
        }

        public PresetSaveRequestMessage(short presetId, byte symbolId, bool updateData)
        {
            PresetId = presetId;
            SymbolId = symbolId;
            UpdateData = updateData;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PresetId);
            writer.WriteByte(SymbolId);
            writer.WriteBoolean(UpdateData);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PresetId = reader.ReadShort();
            SymbolId = reader.ReadByte();
            UpdateData = reader.ReadBoolean();
        }
    }
}