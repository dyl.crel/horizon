namespace Horizon.Protocol.Messages.Game.Presets
{
    public class IdolsPresetSaveRequestMessage : PresetSaveRequestMessage
    {
        public override short Protocol => 6758;

        public IdolsPresetSaveRequestMessage()
        {
        }

        public IdolsPresetSaveRequestMessage(short presetId, byte symbolId, bool updateData) : base(presetId, symbolId, updateData)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}