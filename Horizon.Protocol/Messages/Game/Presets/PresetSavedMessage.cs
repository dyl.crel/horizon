using Horizon.Protocol.Types.Game.Presets;

namespace Horizon.Protocol.Messages.Game.Presets
{
    public class PresetSavedMessage : NetworkMessage
    {
        public override short Protocol => 6763;

        public short PresetId { get; set; }

        public Preset Preset { get; set; }

        public PresetSavedMessage()
        {
        }

        public PresetSavedMessage(short presetId, Preset preset)
        {
            PresetId = presetId;
            Preset = preset;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PresetId);
            writer.WriteShort(Preset.Protocol);
            Preset.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PresetId = reader.ReadShort();
            Preset = ProtocolTypesManager.Instance<Preset>(reader.ReadShort());
            Preset.Deserialize(reader);
        }
    }
}