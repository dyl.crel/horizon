namespace Horizon.Protocol.Messages.Game.Ui
{
    public class ClientUIOpenedByObjectMessage : ClientUIOpenedMessage
    {
        public override short Protocol => 6463;

        public int Uid { get; set; }

        public ClientUIOpenedByObjectMessage()
        {
        }

        public ClientUIOpenedByObjectMessage(byte type, int uid) : base(type)
        {
            Uid = uid;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Uid);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Uid = reader.ReadVarInt();
        }
    }
}