namespace Horizon.Protocol.Messages.Game.Ui
{
    public class ClientUIOpenedMessage : NetworkMessage
    {
        public override short Protocol => 6459;

        public byte Type { get; set; }

        public ClientUIOpenedMessage()
        {
        }

        public ClientUIOpenedMessage(byte type)
        {
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
        }
    }
}