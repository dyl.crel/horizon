using Horizon.Protocol.Messages.Debug;

namespace Horizon.Protocol.Messages.Wtf
{
    public class ClientYouAreDrunkMessage : DebugInClientMessage
    {
        public override short Protocol => 6594;

        public ClientYouAreDrunkMessage()
        {
        }

        public ClientYouAreDrunkMessage(byte level, string message) : base(level, message)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}