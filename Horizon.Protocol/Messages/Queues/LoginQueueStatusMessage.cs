namespace Horizon.Protocol.Messages.Queues
{
    public class LoginQueueStatusMessage : NetworkMessage
    {
        public override short Protocol => 10;

        public short Position { get; set; }

        public short Total { get; set; }

        public LoginQueueStatusMessage()
        {
        }

        public LoginQueueStatusMessage(short position, short total)
        {
            Position = position;
            Total = total;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Position);
            writer.WriteShort(Total);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Position = reader.ReadShort();
            Total = reader.ReadShort();
        }
    }
}