namespace Horizon.Protocol.Messages.Queues
{
    public class QueueStatusMessage : NetworkMessage
    {
        public override short Protocol => 6100;

        public short Position { get; set; }

        public short Total { get; set; }

        public QueueStatusMessage()
        {
        }

        public QueueStatusMessage(short position, short total)
        {
            Position = position;
            Total = total;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Position);
            writer.WriteShort(Total);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Position = reader.ReadShort();
            Total = reader.ReadShort();
        }
    }
}