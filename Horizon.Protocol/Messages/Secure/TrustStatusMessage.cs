namespace Horizon.Protocol.Messages.Secure
{
    public class TrustStatusMessage : NetworkMessage
    {
        public override short Protocol => 6267;

        public bool Trusted { get; set; }

        public bool Certified { get; set; }

        public TrustStatusMessage()
        {
        }

        public TrustStatusMessage(bool trusted, bool certified)
        {
            Trusted = trusted;
            Certified = certified;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Trusted);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Certified);
            writer.WriteByte(flag0);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Trusted = BooleanByteWrapper.GetFlag(flag0, 0);
            Certified = BooleanByteWrapper.GetFlag(flag0, 1);
        }
    }
}