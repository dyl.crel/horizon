namespace Horizon.Protocol.Messages.Handshake
{
    public class ProtocolRequired : NetworkMessage
    {
        public override short Protocol => 1;

        public int RequiredVersion { get; set; }

        public int CurrentVersion { get; set; }

        public ProtocolRequired()
        {
        }

        public ProtocolRequired(int requiredVersion, int currentVersion)
        {
            RequiredVersion = requiredVersion;
            CurrentVersion = currentVersion;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(RequiredVersion);
            writer.WriteInt(CurrentVersion);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            RequiredVersion = reader.ReadInt();
            CurrentVersion = reader.ReadInt();
        }
    }
}