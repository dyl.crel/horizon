namespace Horizon.Protocol.Types.Common.Basic
{
    public class StatisticDataInt : StatisticData
    {
        public override short Protocol => 485;

        public int Value { get; set; }

        public StatisticDataInt()
        {
        }

        public StatisticDataInt(int value)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadInt();
        }
    }
}