namespace Horizon.Protocol.Types.Common.Basic
{
    public class StatisticDataShort : StatisticData
    {
        public override short Protocol => 488;

        public short Value { get; set; }

        public StatisticDataShort()
        {
        }

        public StatisticDataShort(short value)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadShort();
        }
    }
}