namespace Horizon.Protocol.Types.Common.Basic
{
    public class StatisticData : NetworkType
    {
        public override short Protocol => 484;

        public StatisticData()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}