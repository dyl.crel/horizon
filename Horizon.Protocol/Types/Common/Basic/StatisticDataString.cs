namespace Horizon.Protocol.Types.Common.Basic
{
    public class StatisticDataString : StatisticData
    {
        public override short Protocol => 487;

        public string Value { get; set; }

        public StatisticDataString()
        {
        }

        public StatisticDataString(string value)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadUTF();
        }
    }
}