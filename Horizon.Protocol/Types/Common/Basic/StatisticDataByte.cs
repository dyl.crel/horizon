namespace Horizon.Protocol.Types.Common.Basic
{
    public class StatisticDataByte : StatisticData
    {
        public override short Protocol => 486;

        public byte Value { get; set; }

        public StatisticDataByte()
        {
        }

        public StatisticDataByte(byte value)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadByte();
        }
    }
}