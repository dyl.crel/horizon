namespace Horizon.Protocol.Types.Common.Basic
{
    public class StatisticDataBoolean : StatisticData
    {
        public override short Protocol => 482;

        public bool Value { get; set; }

        public StatisticDataBoolean()
        {
        }

        public StatisticDataBoolean(bool value)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadBoolean();
        }
    }
}