namespace Horizon.Protocol.Types.Connection
{
    public class GameServerInformations : NetworkType
    {
        public override short Protocol => 25;

        public bool IsMonoAccount { get; set; }

        public bool IsSelectable { get; set; }

        public short Id { get; set; }

        public byte Type { get; set; }

        public byte Status { get; set; }

        public byte Completion { get; set; }

        public byte CharactersCount { get; set; }

        public byte CharactersSlots { get; set; }

        public double Date { get; set; }

        public GameServerInformations()
        {
        }

        public GameServerInformations(bool isMonoAccount, bool isSelectable, short id, byte type, byte status, byte completion, byte charactersCount, byte charactersSlots, double date)
        {
            IsMonoAccount = isMonoAccount;
            IsSelectable = isSelectable;
            Id = id;
            Type = type;
            Status = status;
            Completion = completion;
            CharactersCount = charactersCount;
            CharactersSlots = charactersSlots;
            Date = date;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, IsMonoAccount);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsSelectable);
            writer.WriteByte(flag0);
            writer.WriteVarShort(Id);
            writer.WriteByte(Type);
            writer.WriteByte(Status);
            writer.WriteByte(Completion);
            writer.WriteByte(CharactersCount);
            writer.WriteByte(CharactersSlots);
            writer.WriteDouble(Date);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            IsMonoAccount = BooleanByteWrapper.GetFlag(flag0, 0);
            IsSelectable = BooleanByteWrapper.GetFlag(flag0, 1);
            Id = reader.ReadVarShort();
            Type = reader.ReadByte();
            Status = reader.ReadByte();
            Completion = reader.ReadByte();
            CharactersCount = reader.ReadByte();
            CharactersSlots = reader.ReadByte();
            Date = reader.ReadDouble();
        }
    }
}