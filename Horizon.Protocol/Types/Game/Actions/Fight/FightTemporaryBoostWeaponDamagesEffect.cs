namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class FightTemporaryBoostWeaponDamagesEffect : FightTemporaryBoostEffect
    {
        public override short Protocol => 211;

        public short WeaponTypeId { get; set; }

        public FightTemporaryBoostWeaponDamagesEffect()
        {
        }

        public FightTemporaryBoostWeaponDamagesEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid, short delta, short weaponTypeId) : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta)
        {
            WeaponTypeId = weaponTypeId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WeaponTypeId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WeaponTypeId = reader.ReadShort();
        }
    }
}