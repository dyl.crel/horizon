namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class GameActionMarkedCell : NetworkType
    {
        public override short Protocol => 85;

        public short CellId { get; set; }

        public byte ZoneSize { get; set; }

        public int CellColor { get; set; }

        public byte CellsType { get; set; }

        public GameActionMarkedCell()
        {
        }

        public GameActionMarkedCell(short cellId, byte zoneSize, int cellColor, byte cellsType)
        {
            CellId = cellId;
            ZoneSize = zoneSize;
            CellColor = cellColor;
            CellsType = cellsType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteByte(ZoneSize);
            writer.WriteInt(CellColor);
            writer.WriteByte(CellsType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
            ZoneSize = reader.ReadByte();
            CellColor = reader.ReadInt();
            CellsType = reader.ReadByte();
        }
    }
}