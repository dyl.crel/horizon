namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class FightTemporaryBoostStateEffect : FightTemporaryBoostEffect
    {
        public override short Protocol => 214;

        public short StateId { get; set; }

        public FightTemporaryBoostStateEffect()
        {
        }

        public FightTemporaryBoostStateEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid, short delta, short stateId) : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta)
        {
            StateId = stateId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(StateId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            StateId = reader.ReadShort();
        }
    }
}