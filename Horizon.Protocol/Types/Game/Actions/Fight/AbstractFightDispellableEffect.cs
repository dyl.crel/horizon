namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class AbstractFightDispellableEffect : NetworkType
    {
        public override short Protocol => 206;

        public int Uid { get; set; }

        public double TargetId { get; set; }

        public short TurnDuration { get; set; }

        public byte Dispelable { get; set; }

        public short SpellId { get; set; }

        public int EffectId { get; set; }

        public int ParentBoostUid { get; set; }

        public AbstractFightDispellableEffect()
        {
        }

        public AbstractFightDispellableEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid)
        {
            Uid = uid;
            TargetId = targetId;
            TurnDuration = turnDuration;
            Dispelable = dispelable;
            SpellId = spellId;
            EffectId = effectId;
            ParentBoostUid = parentBoostUid;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Uid);
            writer.WriteDouble(TargetId);
            writer.WriteShort(TurnDuration);
            writer.WriteByte(Dispelable);
            writer.WriteVarShort(SpellId);
            writer.WriteVarInt(EffectId);
            writer.WriteVarInt(ParentBoostUid);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Uid = reader.ReadVarInt();
            TargetId = reader.ReadDouble();
            TurnDuration = reader.ReadShort();
            Dispelable = reader.ReadByte();
            SpellId = reader.ReadVarShort();
            EffectId = reader.ReadVarInt();
            ParentBoostUid = reader.ReadVarInt();
        }
    }
}