namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class FightTemporaryBoostEffect : AbstractFightDispellableEffect
    {
        public override short Protocol => 209;

        public short Delta { get; set; }

        public FightTemporaryBoostEffect()
        {
        }

        public FightTemporaryBoostEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid, short delta) : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid)
        {
            Delta = delta;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Delta);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Delta = reader.ReadShort();
        }
    }
}