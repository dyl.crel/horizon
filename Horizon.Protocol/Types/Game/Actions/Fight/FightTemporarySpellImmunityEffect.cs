namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class FightTemporarySpellImmunityEffect : AbstractFightDispellableEffect
    {
        public override short Protocol => 366;

        public int ImmuneSpellId { get; set; }

        public FightTemporarySpellImmunityEffect()
        {
        }

        public FightTemporarySpellImmunityEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid, int immuneSpellId) : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid)
        {
            ImmuneSpellId = immuneSpellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(ImmuneSpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ImmuneSpellId = reader.ReadInt();
        }
    }
}