namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class FightTemporarySpellBoostEffect : FightTemporaryBoostEffect
    {
        public override short Protocol => 207;

        public short BoostedSpellId { get; set; }

        public FightTemporarySpellBoostEffect()
        {
        }

        public FightTemporarySpellBoostEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid, short delta, short boostedSpellId) : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta)
        {
            BoostedSpellId = boostedSpellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(BoostedSpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            BoostedSpellId = reader.ReadVarShort();
        }
    }
}