namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class FightTriggeredEffect : AbstractFightDispellableEffect
    {
        public override short Protocol => 210;

        public int Param1 { get; set; }

        public int Param2 { get; set; }

        public int Param3 { get; set; }

        public short Delay { get; set; }

        public FightTriggeredEffect()
        {
        }

        public FightTriggeredEffect(int uid, double targetId, short turnDuration, byte dispelable, short spellId, int effectId, int parentBoostUid, int param1, int param2, int param3, short delay) : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid)
        {
            Param1 = param1;
            Param2 = param2;
            Param3 = param3;
            Delay = delay;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Param1);
            writer.WriteInt(Param2);
            writer.WriteInt(Param3);
            writer.WriteShort(Delay);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Param1 = reader.ReadInt();
            Param2 = reader.ReadInt();
            Param3 = reader.ReadInt();
            Delay = reader.ReadShort();
        }
    }
}