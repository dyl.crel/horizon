namespace Horizon.Protocol.Types.Game.Actions.Fight
{
    public class GameActionMark : NetworkType
    {
        public override short Protocol => 351;

        public double MarkAuthorId { get; set; }

        public byte MarkTeamId { get; set; }

        public int MarkSpellId { get; set; }

        public short MarkSpellLevel { get; set; }

        public short MarkId { get; set; }

        public byte MarkType { get; set; }

        public short MarkimpactCell { get; set; }

        public GameActionMarkedCell[] Cells { get; set; }

        public bool Active { get; set; }

        public GameActionMark()
        {
        }

        public GameActionMark(double markAuthorId, byte markTeamId, int markSpellId, short markSpellLevel, short markId, byte markType, short markimpactCell, GameActionMarkedCell[] cells, bool active)
        {
            MarkAuthorId = markAuthorId;
            MarkTeamId = markTeamId;
            MarkSpellId = markSpellId;
            MarkSpellLevel = markSpellLevel;
            MarkId = markId;
            MarkType = markType;
            MarkimpactCell = markimpactCell;
            Cells = cells;
            Active = active;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MarkAuthorId);
            writer.WriteByte(MarkTeamId);
            writer.WriteInt(MarkSpellId);
            writer.WriteShort(MarkSpellLevel);
            writer.WriteShort(MarkId);
            writer.WriteByte(MarkType);
            writer.WriteShort(MarkimpactCell);
            writer.WriteShort(Cells.Length);
            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i].Serialize(writer);
            }
            writer.WriteBoolean(Active);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MarkAuthorId = reader.ReadDouble();
            MarkTeamId = reader.ReadByte();
            MarkSpellId = reader.ReadInt();
            MarkSpellLevel = reader.ReadShort();
            MarkId = reader.ReadShort();
            MarkType = reader.ReadByte();
            MarkimpactCell = reader.ReadShort();
            Cells = new GameActionMarkedCell[reader.ReadShort()];
            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i] = new GameActionMarkedCell();
                Cells[i].Deserialize(reader);
            }
            Active = reader.ReadBoolean();
        }
    }
}