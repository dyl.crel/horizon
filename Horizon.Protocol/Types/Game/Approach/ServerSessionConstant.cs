namespace Horizon.Protocol.Types.Game.Approach
{
    public class ServerSessionConstant : NetworkType
    {
        public override short Protocol => 430;

        public short Id { get; set; }

        public ServerSessionConstant()
        {
        }

        public ServerSessionConstant(short id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
        }
    }
}