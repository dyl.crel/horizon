namespace Horizon.Protocol.Types.Game.Approach
{
    public class ServerSessionConstantString : ServerSessionConstant
    {
        public override short Protocol => 436;

        public string Value { get; set; }

        public ServerSessionConstantString()
        {
        }

        public ServerSessionConstantString(short id, string value) : base(id)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadUTF();
        }
    }
}