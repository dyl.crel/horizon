namespace Horizon.Protocol.Types.Game.Approach
{
    public class ServerSessionConstantInteger : ServerSessionConstant
    {
        public override short Protocol => 433;

        public int Value { get; set; }

        public ServerSessionConstantInteger()
        {
        }

        public ServerSessionConstantInteger(short id, int value) : base(id)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadInt();
        }
    }
}