namespace Horizon.Protocol.Types.Game.Approach
{
    public class ServerSessionConstantLong : ServerSessionConstant
    {
        public override short Protocol => 429;

        public double Value { get; set; }

        public ServerSessionConstantLong()
        {
        }

        public ServerSessionConstantLong(short id, double value) : base(id)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadDouble();
        }
    }
}