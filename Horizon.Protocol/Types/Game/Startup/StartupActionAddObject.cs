using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Types.Game.Startup
{
    public class StartupActionAddObject : NetworkType
    {
        public override short Protocol => 52;

        public int Uid { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public string DescUrl { get; set; }

        public string PictureUrl { get; set; }

        public ObjectItemInformationWithQuantity[] Items { get; set; }

        public StartupActionAddObject()
        {
        }

        public StartupActionAddObject(int uid, string title, string text, string descUrl, string pictureUrl, ObjectItemInformationWithQuantity[] items)
        {
            Uid = uid;
            Title = title;
            Text = text;
            DescUrl = descUrl;
            PictureUrl = pictureUrl;
            Items = items;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Uid);
            writer.WriteUTF(Title);
            writer.WriteUTF(Text);
            writer.WriteUTF(DescUrl);
            writer.WriteUTF(PictureUrl);
            writer.WriteShort(Items.Length);
            for (var i = 0; i < Items.Length; i++)
            {
                Items[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Uid = reader.ReadInt();
            Title = reader.ReadUTF();
            Text = reader.ReadUTF();
            DescUrl = reader.ReadUTF();
            PictureUrl = reader.ReadUTF();
            Items = new ObjectItemInformationWithQuantity[reader.ReadShort()];
            for (var i = 0; i < Items.Length; i++)
            {
                Items[i] = new ObjectItemInformationWithQuantity();
                Items[i].Deserialize(reader);
            }
        }
    }
}