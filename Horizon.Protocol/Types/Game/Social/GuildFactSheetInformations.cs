using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Social
{
    public class GuildFactSheetInformations : GuildInformations
    {
        public override short Protocol => 424;

        public long LeaderId { get; set; }

        public short NbMembers { get; set; }

        public GuildFactSheetInformations()
        {
        }

        public GuildFactSheetInformations(int guildId, string guildName, byte guildLevel, GuildEmblem guildEmblem, long leaderId, short nbMembers) : base(guildId, guildName, guildLevel, guildEmblem)
        {
            LeaderId = leaderId;
            NbMembers = nbMembers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(LeaderId);
            writer.WriteVarShort(NbMembers);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LeaderId = reader.ReadVarLong();
            NbMembers = reader.ReadVarShort();
        }
    }
}