namespace Horizon.Protocol.Types.Game.Social
{
    public class AllianceVersatileInformations : NetworkType
    {
        public override short Protocol => 432;

        public int AllianceId { get; set; }

        public short NbGuilds { get; set; }

        public short NbMembers { get; set; }

        public short NbSubarea { get; set; }

        public AllianceVersatileInformations()
        {
        }

        public AllianceVersatileInformations(int allianceId, short nbGuilds, short nbMembers, short nbSubarea)
        {
            AllianceId = allianceId;
            NbGuilds = nbGuilds;
            NbMembers = nbMembers;
            NbSubarea = nbSubarea;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(AllianceId);
            writer.WriteVarShort(NbGuilds);
            writer.WriteVarShort(NbMembers);
            writer.WriteVarShort(NbSubarea);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AllianceId = reader.ReadVarInt();
            NbGuilds = reader.ReadVarShort();
            NbMembers = reader.ReadVarShort();
            NbSubarea = reader.ReadVarShort();
        }
    }
}