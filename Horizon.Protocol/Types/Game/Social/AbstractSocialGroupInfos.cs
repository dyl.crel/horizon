namespace Horizon.Protocol.Types.Game.Social
{
    public class AbstractSocialGroupInfos : NetworkType
    {
        public override short Protocol => 416;

        public AbstractSocialGroupInfos()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}