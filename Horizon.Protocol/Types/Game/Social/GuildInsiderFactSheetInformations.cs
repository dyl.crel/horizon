using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Social
{
    public class GuildInsiderFactSheetInformations : GuildFactSheetInformations
    {
        public override short Protocol => 423;

        public string LeaderName { get; set; }

        public short NbConnectedMembers { get; set; }

        public byte NbTaxCollectors { get; set; }

        public int LastActivity { get; set; }

        public GuildInsiderFactSheetInformations()
        {
        }

        public GuildInsiderFactSheetInformations(int guildId, string guildName, byte guildLevel, GuildEmblem guildEmblem, long leaderId, short nbMembers, string leaderName, short nbConnectedMembers, byte nbTaxCollectors, int lastActivity) : base(guildId, guildName, guildLevel, guildEmblem, leaderId, nbMembers)
        {
            LeaderName = leaderName;
            NbConnectedMembers = nbConnectedMembers;
            NbTaxCollectors = nbTaxCollectors;
            LastActivity = lastActivity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(LeaderName);
            writer.WriteVarShort(NbConnectedMembers);
            writer.WriteByte(NbTaxCollectors);
            writer.WriteInt(LastActivity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LeaderName = reader.ReadUTF();
            NbConnectedMembers = reader.ReadVarShort();
            NbTaxCollectors = reader.ReadByte();
            LastActivity = reader.ReadInt();
        }
    }
}