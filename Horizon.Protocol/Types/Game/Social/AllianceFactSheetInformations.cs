using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Social
{
    public class AllianceFactSheetInformations : AllianceInformations
    {
        public override short Protocol => 421;

        public int CreationDate { get; set; }

        public AllianceFactSheetInformations()
        {
        }

        public AllianceFactSheetInformations(int allianceId, string allianceTag, string allianceName, GuildEmblem allianceEmblem, int creationDate) : base(allianceId, allianceTag, allianceName, allianceEmblem)
        {
            CreationDate = creationDate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(CreationDate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CreationDate = reader.ReadInt();
        }
    }
}