namespace Horizon.Protocol.Types.Game.Social
{
    public class GuildInAllianceVersatileInformations : GuildVersatileInformations
    {
        public override short Protocol => 437;

        public int AllianceId { get; set; }

        public GuildInAllianceVersatileInformations()
        {
        }

        public GuildInAllianceVersatileInformations(int guildId, long leaderId, byte guildLevel, byte nbMembers, int allianceId) : base(guildId, leaderId, guildLevel, nbMembers)
        {
            AllianceId = allianceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(AllianceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceId = reader.ReadVarInt();
        }
    }
}