using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Social
{
    public class AlliancedGuildFactSheetInformations : GuildInformations
    {
        public override short Protocol => 422;

        public BasicNamedAllianceInformations AllianceInfos { get; set; }

        public AlliancedGuildFactSheetInformations()
        {
        }

        public AlliancedGuildFactSheetInformations(int guildId, string guildName, byte guildLevel, GuildEmblem guildEmblem, BasicNamedAllianceInformations allianceInfos) : base(guildId, guildName, guildLevel, guildEmblem)
        {
            AllianceInfos = allianceInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AllianceInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceInfos = new BasicNamedAllianceInformations();
            AllianceInfos.Deserialize(reader);
        }
    }
}