namespace Horizon.Protocol.Types.Game.Social
{
    public class GuildVersatileInformations : NetworkType
    {
        public override short Protocol => 435;

        public int GuildId { get; set; }

        public long LeaderId { get; set; }

        public byte GuildLevel { get; set; }

        public byte NbMembers { get; set; }

        public GuildVersatileInformations()
        {
        }

        public GuildVersatileInformations(int guildId, long leaderId, byte guildLevel, byte nbMembers)
        {
            GuildId = guildId;
            LeaderId = leaderId;
            GuildLevel = guildLevel;
            NbMembers = nbMembers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(GuildId);
            writer.WriteVarLong(LeaderId);
            writer.WriteByte(GuildLevel);
            writer.WriteByte(NbMembers);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildId = reader.ReadVarInt();
            LeaderId = reader.ReadVarLong();
            GuildLevel = reader.ReadByte();
            NbMembers = reader.ReadByte();
        }
    }
}