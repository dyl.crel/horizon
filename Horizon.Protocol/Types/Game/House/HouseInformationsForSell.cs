namespace Horizon.Protocol.Types.Game.House
{
    public class HouseInformationsForSell : NetworkType
    {
        public override short Protocol => 221;

        public int InstanceId { get; set; }

        public bool SecondHand { get; set; }

        public int ModelId { get; set; }

        public string OwnerName { get; set; }

        public bool OwnerConnected { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public short SubAreaId { get; set; }

        public byte NbRoom { get; set; }

        public byte NbChest { get; set; }

        public int[] SkillListIds { get; set; }

        public bool IsLocked { get; set; }

        public long Price { get; set; }

        public HouseInformationsForSell()
        {
        }

        public HouseInformationsForSell(int instanceId, bool secondHand, int modelId, string ownerName, bool ownerConnected, short worldX, short worldY, short subAreaId, byte nbRoom, byte nbChest, int[] skillListIds, bool isLocked, long price)
        {
            InstanceId = instanceId;
            SecondHand = secondHand;
            ModelId = modelId;
            OwnerName = ownerName;
            OwnerConnected = ownerConnected;
            WorldX = worldX;
            WorldY = worldY;
            SubAreaId = subAreaId;
            NbRoom = nbRoom;
            NbChest = nbChest;
            SkillListIds = skillListIds;
            IsLocked = isLocked;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            writer.WriteVarInt(ModelId);
            writer.WriteUTF(OwnerName);
            writer.WriteBoolean(OwnerConnected);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteVarShort(SubAreaId);
            writer.WriteByte(NbRoom);
            writer.WriteByte(NbChest);
            writer.WriteShort(SkillListIds.Length);
            for (var i = 0; i < SkillListIds.Length; i++)
            {
                writer.WriteInt(SkillListIds[i]);
            }
            writer.WriteBoolean(IsLocked);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            InstanceId = reader.ReadInt();
            SecondHand = reader.ReadBoolean();
            ModelId = reader.ReadVarInt();
            OwnerName = reader.ReadUTF();
            OwnerConnected = reader.ReadBoolean();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            SubAreaId = reader.ReadVarShort();
            NbRoom = reader.ReadByte();
            NbChest = reader.ReadByte();
            SkillListIds = new int[reader.ReadShort()];
            for (var i = 0; i < SkillListIds.Length; i++)
            {
                SkillListIds[i] = reader.ReadInt();
            }
            IsLocked = reader.ReadBoolean();
            Price = reader.ReadVarLong();
        }
    }
}