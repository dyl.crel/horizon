namespace Horizon.Protocol.Types.Game.House
{
    public class HouseInformationsForGuild : HouseInformations
    {
        public override short Protocol => 170;

        public int InstanceId { get; set; }

        public bool SecondHand { get; set; }

        public string OwnerName { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public int[] SkillListIds { get; set; }

        public int GuildshareParams { get; set; }

        public HouseInformationsForGuild()
        {
        }

        public HouseInformationsForGuild(int houseId, short modelId, int instanceId, bool secondHand, string ownerName, short worldX, short worldY, double mapId, short subAreaId, int[] skillListIds, int guildshareParams) : base(houseId, modelId)
        {
            InstanceId = instanceId;
            SecondHand = secondHand;
            OwnerName = ownerName;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            SkillListIds = skillListIds;
            GuildshareParams = guildshareParams;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            writer.WriteUTF(OwnerName);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteShort(SkillListIds.Length);
            for (var i = 0; i < SkillListIds.Length; i++)
            {
                writer.WriteInt(SkillListIds[i]);
            }
            writer.WriteVarInt(GuildshareParams);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            InstanceId = reader.ReadInt();
            SecondHand = reader.ReadBoolean();
            OwnerName = reader.ReadUTF();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            SkillListIds = new int[reader.ReadShort()];
            for (var i = 0; i < SkillListIds.Length; i++)
            {
                SkillListIds[i] = reader.ReadInt();
            }
            GuildshareParams = reader.ReadVarInt();
        }
    }
}