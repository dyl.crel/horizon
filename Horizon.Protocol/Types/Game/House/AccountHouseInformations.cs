namespace Horizon.Protocol.Types.Game.House
{
    public class AccountHouseInformations : HouseInformations
    {
        public override short Protocol => 390;

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public HouseInstanceInformations HouseInfos { get; set; }

        public AccountHouseInformations()
        {
        }

        public AccountHouseInformations(int houseId, short modelId, short worldX, short worldY, double mapId, short subAreaId, HouseInstanceInformations houseInfos) : base(houseId, modelId)
        {
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            HouseInfos = houseInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteShort(HouseInfos.Protocol);
            HouseInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            HouseInfos = ProtocolTypesManager.Instance<HouseInstanceInformations>(reader.ReadShort());
            HouseInfos.Deserialize(reader);
        }
    }
}