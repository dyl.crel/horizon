namespace Horizon.Protocol.Types.Game.House
{
    public class HouseInformations : NetworkType
    {
        public override short Protocol => 111;

        public int HouseId { get; set; }

        public short ModelId { get; set; }

        public HouseInformations()
        {
        }

        public HouseInformations(int houseId, short modelId)
        {
            HouseId = houseId;
            ModelId = modelId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteVarShort(ModelId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            HouseId = reader.ReadVarInt();
            ModelId = reader.ReadVarShort();
        }
    }
}