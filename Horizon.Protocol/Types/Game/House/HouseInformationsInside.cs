namespace Horizon.Protocol.Types.Game.House
{
    public class HouseInformationsInside : HouseInformations
    {
        public override short Protocol => 218;

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public HouseInstanceInformations HouseInfos { get; set; }

        public HouseInformationsInside()
        {
        }

        public HouseInformationsInside(int houseId, short modelId, short worldX, short worldY, HouseInstanceInformations houseInfos) : base(houseId, modelId)
        {
            WorldX = worldX;
            WorldY = worldY;
            HouseInfos = houseInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteShort(HouseInfos.Protocol);
            HouseInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            HouseInfos = ProtocolTypesManager.Instance<HouseInstanceInformations>(reader.ReadShort());
            HouseInfos.Deserialize(reader);
        }
    }
}