namespace Horizon.Protocol.Types.Game.House
{
    public class HouseInstanceInformations : NetworkType
    {
        public override short Protocol => 511;

        public bool SecondHand { get; set; }

        public bool IsLocked { get; set; }

        public bool IsSaleLocked { get; set; }

        public int InstanceId { get; set; }

        public string OwnerName { get; set; }

        public long Price { get; set; }

        public HouseInstanceInformations()
        {
        }

        public HouseInstanceInformations(bool secondHand, bool isLocked, bool isSaleLocked, int instanceId, string ownerName, long price)
        {
            SecondHand = secondHand;
            IsLocked = isLocked;
            IsSaleLocked = isSaleLocked;
            InstanceId = instanceId;
            OwnerName = ownerName;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, SecondHand);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsLocked);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, IsSaleLocked);
            writer.WriteByte(flag0);
            writer.WriteInt(InstanceId);
            writer.WriteUTF(OwnerName);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            SecondHand = BooleanByteWrapper.GetFlag(flag0, 0);
            IsLocked = BooleanByteWrapper.GetFlag(flag0, 1);
            IsSaleLocked = BooleanByteWrapper.GetFlag(flag0, 2);
            InstanceId = reader.ReadInt();
            OwnerName = reader.ReadUTF();
            Price = reader.ReadVarLong();
        }
    }
}