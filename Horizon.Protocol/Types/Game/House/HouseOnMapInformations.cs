namespace Horizon.Protocol.Types.Game.House
{
    public class HouseOnMapInformations : HouseInformations
    {
        public override short Protocol => 510;

        public int[] DoorsOnMap { get; set; }

        public HouseInstanceInformations[] HouseInstances { get; set; }

        public HouseOnMapInformations()
        {
        }

        public HouseOnMapInformations(int houseId, short modelId, int[] doorsOnMap, HouseInstanceInformations[] houseInstances) : base(houseId, modelId)
        {
            DoorsOnMap = doorsOnMap;
            HouseInstances = houseInstances;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(DoorsOnMap.Length);
            for (var i = 0; i < DoorsOnMap.Length; i++)
            {
                writer.WriteInt(DoorsOnMap[i]);
            }
            writer.WriteShort(HouseInstances.Length);
            for (var i = 0; i < HouseInstances.Length; i++)
            {
                HouseInstances[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DoorsOnMap = new int[reader.ReadShort()];
            for (var i = 0; i < DoorsOnMap.Length; i++)
            {
                DoorsOnMap[i] = reader.ReadInt();
            }
            HouseInstances = new HouseInstanceInformations[reader.ReadShort()];
            for (var i = 0; i < HouseInstances.Length; i++)
            {
                HouseInstances[i] = new HouseInstanceInformations();
                HouseInstances[i].Deserialize(reader);
            }
        }
    }
}