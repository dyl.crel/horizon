using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.House
{
    public class HouseGuildedInformations : HouseInstanceInformations
    {
        public override short Protocol => 512;

        public GuildInformations GuildInfo { get; set; }

        public HouseGuildedInformations()
        {
        }

        public HouseGuildedInformations(bool secondHand, bool isLocked, bool isSaleLocked, int instanceId, string ownerName, long price, GuildInformations guildInfo) : base(secondHand, isLocked, isSaleLocked, instanceId, ownerName, price)
        {
            GuildInfo = guildInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            GuildInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuildInfo = new GuildInformations();
            GuildInfo.Deserialize(reader);
        }
    }
}