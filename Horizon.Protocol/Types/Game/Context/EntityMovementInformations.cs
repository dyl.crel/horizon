namespace Horizon.Protocol.Types.Game.Context
{
    public class EntityMovementInformations : NetworkType
    {
        public override short Protocol => 63;

        public int Id { get; set; }

        public byte[] Steps { get; set; }

        public EntityMovementInformations()
        {
        }

        public EntityMovementInformations(int id, byte[] steps)
        {
            Id = id;
            Steps = steps;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Id);
            writer.WriteShort(Steps.Length);
            for (var i = 0; i < Steps.Length; i++)
            {
                writer.WriteByte(Steps[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadInt();
            Steps = new byte[reader.ReadShort()];
            for (var i = 0; i < Steps.Length; i++)
            {
                Steps[i] = reader.ReadByte();
            }
        }
    }
}