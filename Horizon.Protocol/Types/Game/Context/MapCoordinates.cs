namespace Horizon.Protocol.Types.Game.Context
{
    public class MapCoordinates : NetworkType
    {
        public override short Protocol => 174;

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public MapCoordinates()
        {
        }

        public MapCoordinates(short worldX, short worldY)
        {
            WorldX = worldX;
            WorldY = worldY;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
        }
    }
}