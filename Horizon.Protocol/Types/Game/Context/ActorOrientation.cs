namespace Horizon.Protocol.Types.Game.Context
{
    public class ActorOrientation : NetworkType
    {
        public override short Protocol => 353;

        public double Id { get; set; }

        public byte Direction { get; set; }

        public ActorOrientation()
        {
        }

        public ActorOrientation(double id, byte direction)
        {
            Id = id;
            Direction = direction;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
            writer.WriteByte(Direction);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
            Direction = reader.ReadByte();
        }
    }
}