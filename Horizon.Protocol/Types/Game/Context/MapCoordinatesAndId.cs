namespace Horizon.Protocol.Types.Game.Context
{
    public class MapCoordinatesAndId : MapCoordinates
    {
        public override short Protocol => 392;

        public double MapId { get; set; }

        public MapCoordinatesAndId()
        {
        }

        public MapCoordinatesAndId(short worldX, short worldY, double mapId) : base(worldX, worldY)
        {
            MapId = mapId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(MapId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MapId = reader.ReadDouble();
        }
    }
}