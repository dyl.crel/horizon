namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightExternalInformations : NetworkType
    {
        public override short Protocol => 117;

        public short FightId { get; set; }

        public byte FightType { get; set; }

        public int FightStart { get; set; }

        public bool FightSpectatorLocked { get; set; }

        public FightTeamLightInformations[] FightTeams { get; set; }

        public FightOptionsInformations[] FightTeamsOptions { get; set; }

        public FightExternalInformations()
        {
        }

        public FightExternalInformations(short fightId, byte fightType, int fightStart, bool fightSpectatorLocked, FightTeamLightInformations[] fightTeams, FightOptionsInformations[] fightTeamsOptions)
        {
            FightId = fightId;
            FightType = fightType;
            FightStart = fightStart;
            FightSpectatorLocked = fightSpectatorLocked;
            FightTeams = fightTeams;
            FightTeamsOptions = fightTeamsOptions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteByte(FightType);
            writer.WriteInt(FightStart);
            writer.WriteBoolean(FightSpectatorLocked);
            for (var i = 0; i < 2; i++)
            {
                FightTeams[i].Serialize(writer);
            }

            for (var i = 0; i < 2; i++)
            {
                FightTeamsOptions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            FightType = reader.ReadByte();
            FightStart = reader.ReadInt();
            FightSpectatorLocked = reader.ReadBoolean();
            FightTeams = new FightTeamLightInformations[2];
            for (var i = 0; i < FightTeams.Length; i++)
            {
                FightTeams[i] = new FightTeamLightInformations();
                FightTeams[i].Deserialize(reader);
            }
            FightTeamsOptions = new FightOptionsInformations[2];
            for (var i = 0; i < FightTeamsOptions.Length; i++)
            {
                FightTeamsOptions[i] = new FightOptionsInformations();
                FightTeamsOptions[i].Deserialize(reader);
            }
        }
    }
}