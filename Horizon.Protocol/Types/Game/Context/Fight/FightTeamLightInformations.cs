namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamLightInformations : AbstractFightTeamInformations
    {
        public override short Protocol => 115;

        public bool HasFriend { get; set; }

        public bool HasGuildMember { get; set; }

        public bool HasAllianceMember { get; set; }

        public bool HasGroupMember { get; set; }

        public bool HasMyTaxCollector { get; set; }

        public byte TeamMembersCount { get; set; }

        public int MeanLevel { get; set; }

        public FightTeamLightInformations()
        {
        }

        public FightTeamLightInformations(byte teamId, double leaderId, sbyte teamSide, byte teamTypeId, byte nbWaves, bool hasFriend, bool hasGuildMember, bool hasAllianceMember, bool hasGroupMember, bool hasMyTaxCollector, byte teamMembersCount, int meanLevel) : base(teamId, leaderId, teamSide, teamTypeId, nbWaves)
        {
            HasFriend = hasFriend;
            HasGuildMember = hasGuildMember;
            HasAllianceMember = hasAllianceMember;
            HasGroupMember = hasGroupMember;
            HasMyTaxCollector = hasMyTaxCollector;
            TeamMembersCount = teamMembersCount;
            MeanLevel = meanLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, HasFriend);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, HasGuildMember);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, HasAllianceMember);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, HasGroupMember);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 4, HasMyTaxCollector);
            writer.WriteByte(flag0);
            writer.WriteByte(TeamMembersCount);
            writer.WriteVarInt(MeanLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            HasFriend = BooleanByteWrapper.GetFlag(flag0, 0);
            HasGuildMember = BooleanByteWrapper.GetFlag(flag0, 1);
            HasAllianceMember = BooleanByteWrapper.GetFlag(flag0, 2);
            HasGroupMember = BooleanByteWrapper.GetFlag(flag0, 3);
            HasMyTaxCollector = BooleanByteWrapper.GetFlag(flag0, 4);
            TeamMembersCount = reader.ReadByte();
            MeanLevel = reader.ReadVarInt();
        }
    }
}