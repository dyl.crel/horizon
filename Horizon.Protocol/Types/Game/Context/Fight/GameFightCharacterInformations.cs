using Horizon.Protocol.Types.Game.Character.Alignment;
using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightCharacterInformations : GameFightFighterNamedInformations
    {
        public override short Protocol => 46;

        public short Level { get; set; }

        public ActorAlignmentInformations AlignmentInfos { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public GameFightCharacterInformations()
        {
        }

        public GameFightCharacterInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions, string name, PlayerStatus status, short leagueId, int ladderPosition, bool hiddenInPrefight, short level, ActorAlignmentInformations alignmentInfos, byte breed, bool sex) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions, name, status, leagueId, ladderPosition, hiddenInPrefight)
        {
            Level = level;
            AlignmentInfos = alignmentInfos;
            Breed = breed;
            Sex = sex;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
            AlignmentInfos.Serialize(writer);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarShort();
            AlignmentInfos = new ActorAlignmentInformations();
            AlignmentInfos.Deserialize(reader);
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
        }
    }
}