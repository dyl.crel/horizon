namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultListEntry : NetworkType
    {
        public override short Protocol => 16;

        public short Outcome { get; set; }

        public byte Wave { get; set; }

        public FightLoot Rewards { get; set; }

        public FightResultListEntry()
        {
        }

        public FightResultListEntry(short outcome, byte wave, FightLoot rewards)
        {
            Outcome = outcome;
            Wave = wave;
            Rewards = rewards;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Outcome);
            writer.WriteByte(Wave);
            Rewards.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Outcome = reader.ReadVarShort();
            Wave = reader.ReadByte();
            Rewards = new FightLoot();
            Rewards.Deserialize(reader);
        }
    }
}