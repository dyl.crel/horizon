namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightStartingPositions : NetworkType
    {
        public override short Protocol => 513;

        public short[] PositionsForChallengers { get; set; }

        public short[] PositionsForDefenders { get; set; }

        public FightStartingPositions()
        {
        }

        public FightStartingPositions(short[] positionsForChallengers, short[] positionsForDefenders)
        {
            PositionsForChallengers = positionsForChallengers;
            PositionsForDefenders = positionsForDefenders;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(PositionsForChallengers.Length);
            for (var i = 0; i < PositionsForChallengers.Length; i++)
            {
                writer.WriteVarShort(PositionsForChallengers[i]);
            }
            writer.WriteShort(PositionsForDefenders.Length);
            for (var i = 0; i < PositionsForDefenders.Length; i++)
            {
                writer.WriteVarShort(PositionsForDefenders[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PositionsForChallengers = new short[reader.ReadShort()];
            for (var i = 0; i < PositionsForChallengers.Length; i++)
            {
                PositionsForChallengers[i] = reader.ReadVarShort();
            }
            PositionsForDefenders = new short[reader.ReadShort()];
            for (var i = 0; i < PositionsForDefenders.Length; i++)
            {
                PositionsForDefenders[i] = reader.ReadVarShort();
            }
        }
    }
}