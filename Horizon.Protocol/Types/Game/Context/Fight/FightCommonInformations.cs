namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightCommonInformations : NetworkType
    {
        public override short Protocol => 43;

        public short FightId { get; set; }

        public byte FightType { get; set; }

        public FightTeamInformations[] FightTeams { get; set; }

        public short[] FightTeamsPositions { get; set; }

        public FightOptionsInformations[] FightTeamsOptions { get; set; }

        public FightCommonInformations()
        {
        }

        public FightCommonInformations(short fightId, byte fightType, FightTeamInformations[] fightTeams, short[] fightTeamsPositions, FightOptionsInformations[] fightTeamsOptions)
        {
            FightId = fightId;
            FightType = fightType;
            FightTeams = fightTeams;
            FightTeamsPositions = fightTeamsPositions;
            FightTeamsOptions = fightTeamsOptions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteByte(FightType);
            writer.WriteShort(FightTeams.Length);
            for (var i = 0; i < FightTeams.Length; i++)
            {
                writer.WriteShort(FightTeams[i].Protocol);
                FightTeams[i].Serialize(writer);
            }
            writer.WriteShort(FightTeamsPositions.Length);
            for (var i = 0; i < FightTeamsPositions.Length; i++)
            {
                writer.WriteVarShort(FightTeamsPositions[i]);
            }
            writer.WriteShort(FightTeamsOptions.Length);
            for (var i = 0; i < FightTeamsOptions.Length; i++)
            {
                FightTeamsOptions[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FightId = reader.ReadVarShort();
            FightType = reader.ReadByte();
            FightTeams = new FightTeamInformations[reader.ReadShort()];
            for (var i = 0; i < FightTeams.Length; i++)
            {
                FightTeams[i] = ProtocolTypesManager.Instance<FightTeamInformations>(reader.ReadShort());
                FightTeams[i].Deserialize(reader);
            }
            FightTeamsPositions = new short[reader.ReadShort()];
            for (var i = 0; i < FightTeamsPositions.Length; i++)
            {
                FightTeamsPositions[i] = reader.ReadVarShort();
            }
            FightTeamsOptions = new FightOptionsInformations[reader.ReadShort()];
            for (var i = 0; i < FightTeamsOptions.Length; i++)
            {
                FightTeamsOptions[i] = new FightOptionsInformations();
                FightTeamsOptions[i].Deserialize(reader);
            }
        }
    }
}