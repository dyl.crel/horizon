namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamMemberEntityInformation : FightTeamMemberInformations
    {
        public override short Protocol => 549;

        public byte EntityModelId { get; set; }

        public short Level { get; set; }

        public double MasterId { get; set; }

        public FightTeamMemberEntityInformation()
        {
        }

        public FightTeamMemberEntityInformation(double id, byte entityModelId, short level, double masterId) : base(id)
        {
            EntityModelId = entityModelId;
            Level = level;
            MasterId = masterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(EntityModelId);
            writer.WriteVarShort(Level);
            writer.WriteDouble(MasterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            EntityModelId = reader.ReadByte();
            Level = reader.ReadVarShort();
            MasterId = reader.ReadDouble();
        }
    }
}