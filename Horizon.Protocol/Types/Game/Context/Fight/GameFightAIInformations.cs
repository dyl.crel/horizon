using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightAIInformations : GameFightFighterInformations
    {
        public override short Protocol => 151;

        public GameFightAIInformations()
        {
        }

        public GameFightAIInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}