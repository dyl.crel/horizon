namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightSpellCooldown : NetworkType
    {
        public override short Protocol => 205;

        public int SpellId { get; set; }

        public byte Cooldown { get; set; }

        public GameFightSpellCooldown()
        {
        }

        public GameFightSpellCooldown(int spellId, byte cooldown)
        {
            SpellId = spellId;
            Cooldown = cooldown;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(SpellId);
            writer.WriteByte(Cooldown);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadInt();
            Cooldown = reader.ReadByte();
        }
    }
}