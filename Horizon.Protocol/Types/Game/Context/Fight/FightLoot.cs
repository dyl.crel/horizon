namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightLoot : NetworkType
    {
        public override short Protocol => 41;

        public short[] Objects { get; set; }

        public long Kamas { get; set; }

        public FightLoot()
        {
        }

        public FightLoot(short[] objects, long kamas)
        {
            Objects = objects;
            Kamas = kamas;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Objects.Length);
            for (var i = 0; i < Objects.Length; i++)
            {
                writer.WriteVarShort(Objects[i]);
            }
            writer.WriteVarLong(Kamas);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Objects = new short[reader.ReadShort()];
            for (var i = 0; i < Objects.Length; i++)
            {
                Objects[i] = reader.ReadVarShort();
            }
            Kamas = reader.ReadVarLong();
        }
    }
}