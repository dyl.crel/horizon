namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamMemberTaxCollectorInformations : FightTeamMemberInformations
    {
        public override short Protocol => 177;

        public short FirstNameId { get; set; }

        public short LastNameId { get; set; }

        public byte Level { get; set; }

        public int GuildId { get; set; }

        public double Uid { get; set; }

        public FightTeamMemberTaxCollectorInformations()
        {
        }

        public FightTeamMemberTaxCollectorInformations(double id, short firstNameId, short lastNameId, byte level, int guildId, double uid) : base(id)
        {
            FirstNameId = firstNameId;
            LastNameId = lastNameId;
            Level = level;
            GuildId = guildId;
            Uid = uid;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteByte(Level);
            writer.WriteVarInt(GuildId);
            writer.WriteDouble(Uid);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FirstNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
            Level = reader.ReadByte();
            GuildId = reader.ReadVarInt();
            Uid = reader.ReadDouble();
        }
    }
}