namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultPvpData : FightResultAdditionalData
    {
        public override short Protocol => 190;

        public byte Grade { get; set; }

        public short MinHonorForGrade { get; set; }

        public short MaxHonorForGrade { get; set; }

        public short Honor { get; set; }

        public short HonorDelta { get; set; }

        public FightResultPvpData()
        {
        }

        public FightResultPvpData(byte grade, short minHonorForGrade, short maxHonorForGrade, short honor, short honorDelta)
        {
            Grade = grade;
            MinHonorForGrade = minHonorForGrade;
            MaxHonorForGrade = maxHonorForGrade;
            Honor = honor;
            HonorDelta = honorDelta;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Grade);
            writer.WriteVarShort(MinHonorForGrade);
            writer.WriteVarShort(MaxHonorForGrade);
            writer.WriteVarShort(Honor);
            writer.WriteVarShort(HonorDelta);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Grade = reader.ReadByte();
            MinHonorForGrade = reader.ReadVarShort();
            MaxHonorForGrade = reader.ReadVarShort();
            Honor = reader.ReadVarShort();
            HonorDelta = reader.ReadVarShort();
        }
    }
}