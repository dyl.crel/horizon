namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterLightInformations : NetworkType
    {
        public override short Protocol => 413;

        public bool Sex { get; set; }

        public bool Alive { get; set; }

        public double Id { get; set; }

        public byte Wave { get; set; }

        public short Level { get; set; }

        public byte Breed { get; set; }

        public GameFightFighterLightInformations()
        {
        }

        public GameFightFighterLightInformations(bool sex, bool alive, double id, byte wave, short level, byte breed)
        {
            Sex = sex;
            Alive = alive;
            Id = id;
            Wave = wave;
            Level = level;
            Breed = breed;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Sex);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, Alive);
            writer.WriteByte(flag0);
            writer.WriteDouble(Id);
            writer.WriteByte(Wave);
            writer.WriteVarShort(Level);
            writer.WriteByte(Breed);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag0, 0);
            Alive = BooleanByteWrapper.GetFlag(flag0, 1);
            Id = reader.ReadDouble();
            Wave = reader.ReadByte();
            Level = reader.ReadVarShort();
            Breed = reader.ReadByte();
        }
    }
}