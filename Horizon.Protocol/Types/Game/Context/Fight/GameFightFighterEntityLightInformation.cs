namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterEntityLightInformation : GameFightFighterLightInformations
    {
        public override short Protocol => 548;

        public byte EntityModelId { get; set; }

        public double MasterId { get; set; }

        public GameFightFighterEntityLightInformation()
        {
        }

        public GameFightFighterEntityLightInformation(bool sex, bool alive, double id, byte wave, short level, byte breed, byte entityModelId, double masterId) : base(sex, alive, id, wave, level, breed)
        {
            EntityModelId = entityModelId;
            MasterId = masterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(EntityModelId);
            writer.WriteDouble(MasterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            EntityModelId = reader.ReadByte();
            MasterId = reader.ReadDouble();
        }
    }
}