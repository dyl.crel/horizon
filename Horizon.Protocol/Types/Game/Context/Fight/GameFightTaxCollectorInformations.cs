using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightTaxCollectorInformations : GameFightAIInformations
    {
        public override short Protocol => 48;

        public short FirstNameId { get; set; }

        public short LastNameId { get; set; }

        public byte Level { get; set; }

        public GameFightTaxCollectorInformations()
        {
        }

        public GameFightTaxCollectorInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive,  GameFightMinimalStats stats, short[] previousPositions, short firstNameId, short lastNameId, byte level) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            FirstNameId = firstNameId;
            LastNameId = lastNameId;
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteByte(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FirstNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
            Level = reader.ReadByte();
        }
    }
}