namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterNamedLightInformations : GameFightFighterLightInformations
    {
        public override short Protocol => 456;

        public string Name { get; set; }

        public GameFightFighterNamedLightInformations()
        {
        }

        public GameFightFighterNamedLightInformations(bool sex, bool alive, double id, byte wave, short level, byte breed, string name) : base(sex, alive, id, wave, level, breed)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
        }
    }
}