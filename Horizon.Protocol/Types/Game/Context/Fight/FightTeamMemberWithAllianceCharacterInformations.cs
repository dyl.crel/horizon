using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamMemberWithAllianceCharacterInformations : FightTeamMemberCharacterInformations
    {
        public override short Protocol => 426;

        public BasicAllianceInformations AllianceInfos { get; set; }

        public FightTeamMemberWithAllianceCharacterInformations()
        {
        }

        public FightTeamMemberWithAllianceCharacterInformations(double id, string name, short level, BasicAllianceInformations allianceInfos) : base(id, name, level)
        {
            AllianceInfos = allianceInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AllianceInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceInfos = new BasicAllianceInformations();
            AllianceInfos.Deserialize(reader);
        }
    }
}