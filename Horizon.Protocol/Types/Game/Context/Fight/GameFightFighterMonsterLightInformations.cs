namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterMonsterLightInformations : GameFightFighterLightInformations
    {
        public override short Protocol => 455;

        public short CreatureGenericId { get; set; }

        public GameFightFighterMonsterLightInformations()
        {
        }

        public GameFightFighterMonsterLightInformations(bool sex, bool alive, double id, byte wave, short level, byte breed, short creatureGenericId) : base(sex, alive, id, wave, level, breed)
        {
            CreatureGenericId = creatureGenericId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CreatureGenericId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CreatureGenericId = reader.ReadVarShort();
        }
    }
}