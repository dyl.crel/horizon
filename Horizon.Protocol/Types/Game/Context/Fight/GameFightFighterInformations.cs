using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterInformations : GameContextActorInformations
    {
        public override short Protocol => 143;

        public byte TeamId { get; set; }

        public byte Wave { get; set; }

        public bool Alive { get; set; }

        public GameFightMinimalStats Stats { get; set; }

        public short[] PreviousPositions { get; set; }

        public GameFightFighterInformations()
        {
        }

        public GameFightFighterInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions) : base(contextualId, look, disposition)
        {
            TeamId = teamId;
            Wave = wave;
            Alive = alive;
            Stats = stats;
            PreviousPositions = previousPositions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(TeamId);
            writer.WriteByte(Wave);
            writer.WriteBoolean(Alive);
            writer.WriteShort(Stats.Protocol);
            Stats.Serialize(writer);
            writer.WriteShort(PreviousPositions.Length);
            for (var i = 0; i < PreviousPositions.Length; i++)
            {
                writer.WriteVarShort(PreviousPositions[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TeamId = reader.ReadByte();
            Wave = reader.ReadByte();
            Alive = reader.ReadBoolean();
            Stats = ProtocolTypesManager.Instance<GameFightMinimalStats>(reader.ReadShort());
            Stats.Deserialize(reader);
            PreviousPositions = new short[reader.ReadShort()];
            for (var i = 0; i < PreviousPositions.Length; i++)
            {
                PreviousPositions[i] = reader.ReadVarShort();
            }
        }
    }
}