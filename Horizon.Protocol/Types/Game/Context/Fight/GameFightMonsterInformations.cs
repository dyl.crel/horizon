using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightMonsterInformations : GameFightAIInformations
    {
        public override short Protocol => 29;

        public short CreatureGenericId { get; set; }

        public byte CreatureGrade { get; set; }

        public short CreatureLevel { get; set; }

        public GameFightMonsterInformations()
        {
        }

        public GameFightMonsterInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions, short creatureGenericId, byte creatureGrade, short creatureLevel) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            CreatureGenericId = creatureGenericId;
            CreatureGrade = creatureGrade;
            CreatureLevel = creatureLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CreatureGenericId);
            writer.WriteByte(CreatureGrade);
            writer.WriteShort(CreatureLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CreatureGenericId = reader.ReadVarShort();
            CreatureGrade = reader.ReadByte();
            CreatureLevel = reader.ReadShort();
        }
    }
}