namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightOptionsInformations : NetworkType
    {
        public override short Protocol => 20;

        public bool IsSecret { get; set; }

        public bool IsRestrictedToPartyOnly { get; set; }

        public bool IsClosed { get; set; }

        public bool IsAskingForHelp { get; set; }

        public FightOptionsInformations()
        {
        }

        public FightOptionsInformations(bool isSecret, bool isRestrictedToPartyOnly, bool isClosed, bool isAskingForHelp)
        {
            IsSecret = isSecret;
            IsRestrictedToPartyOnly = isRestrictedToPartyOnly;
            IsClosed = isClosed;
            IsAskingForHelp = isAskingForHelp;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, IsSecret);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsRestrictedToPartyOnly);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, IsClosed);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, IsAskingForHelp);
            writer.WriteByte(flag0);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            IsSecret = BooleanByteWrapper.GetFlag(flag0, 0);
            IsRestrictedToPartyOnly = BooleanByteWrapper.GetFlag(flag0, 1);
            IsClosed = BooleanByteWrapper.GetFlag(flag0, 2);
            IsAskingForHelp = BooleanByteWrapper.GetFlag(flag0, 3);
        }
    }
}