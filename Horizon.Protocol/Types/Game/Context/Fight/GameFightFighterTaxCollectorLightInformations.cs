namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterTaxCollectorLightInformations : GameFightFighterLightInformations
    {
        public override short Protocol => 457;

        public short FirstNameId { get; set; }

        public short LastNameId { get; set; }

        public GameFightFighterTaxCollectorLightInformations()
        {
        }

        public GameFightFighterTaxCollectorLightInformations(bool sex, bool alive, double id, byte wave, short level, byte breed, short firstNameId, short lastNameId) : base(sex, alive, id, wave, level, breed)
        {
            FirstNameId = firstNameId;
            LastNameId = lastNameId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FirstNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
        }
    }
}