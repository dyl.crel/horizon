namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultPlayerListEntry : FightResultFighterListEntry
    {
        public override short Protocol => 24;

        public short Level { get; set; }

        public FightResultAdditionalData[] Additional { get; set; }

        public FightResultPlayerListEntry()
        {
        }

        public FightResultPlayerListEntry(short outcome, byte wave, FightLoot rewards, double id, bool alive, short level, FightResultAdditionalData[] additional) : base(outcome, wave, rewards, id, alive)
        {
            Level = level;
            Additional = additional;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
            writer.WriteShort(Additional.Length);
            for (var i = 0; i < Additional.Length; i++)
            {
                writer.WriteShort(Additional[i].Protocol);
                Additional[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarShort();
            Additional = new FightResultAdditionalData[reader.ReadShort()];
            for (var i = 0; i < Additional.Length; i++)
            {
                Additional[i] = ProtocolTypesManager.Instance<FightResultAdditionalData>(reader.ReadShort());
                Additional[i].Deserialize(reader);
            }
        }
    }
}