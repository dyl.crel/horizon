namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightAllianceTeamInformations : FightTeamInformations
    {
        public override short Protocol => 439;

        public byte Relation { get; set; }

        public FightAllianceTeamInformations()
        {
        }

        public FightAllianceTeamInformations(byte teamId, double leaderId, sbyte teamSide, byte teamTypeId, byte nbWaves, FightTeamMemberInformations[] teamMembers, byte relation) : base(teamId, leaderId, teamSide, teamTypeId, nbWaves, teamMembers)
        {
            Relation = relation;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Relation);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Relation = reader.ReadByte();
        }
    }
}