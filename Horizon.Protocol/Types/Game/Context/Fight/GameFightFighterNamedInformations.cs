using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightFighterNamedInformations : GameFightFighterInformations
    {
        public override short Protocol => 158;

        public string Name { get; set; }

        public PlayerStatus Status { get; set; }

        public short LeagueId { get; set; }

        public int LadderPosition { get; set; }

        public bool HiddenInPrefight { get; set; }

        public GameFightFighterNamedInformations()
        {
        }

        public GameFightFighterNamedInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions, string name, PlayerStatus status, short leagueId, int ladderPosition, bool hiddenInPrefight) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            Name = name;
            Status = status;
            LeagueId = leagueId;
            LadderPosition = ladderPosition;
            HiddenInPrefight = hiddenInPrefight;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            Status.Serialize(writer);
            writer.WriteVarShort(LeagueId);
            writer.WriteInt(LadderPosition);
            writer.WriteBoolean(HiddenInPrefight);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Status = new PlayerStatus();
            Status.Deserialize(reader);
            LeagueId = reader.ReadVarShort();
            LadderPosition = reader.ReadInt();
            HiddenInPrefight = reader.ReadBoolean();
        }
    }
}