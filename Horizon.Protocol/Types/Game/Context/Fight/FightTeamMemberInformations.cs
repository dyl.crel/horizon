namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamMemberInformations : NetworkType
    {
        public override short Protocol => 44;

        public double Id { get; set; }

        public FightTeamMemberInformations()
        {
        }

        public FightTeamMemberInformations(double id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadDouble();
        }
    }
}