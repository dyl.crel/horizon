using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultTaxCollectorListEntry : FightResultFighterListEntry
    {
        public override short Protocol => 84;

        public byte Level { get; set; }

        public BasicGuildInformations GuildInfo { get; set; }

        public int ExperienceForGuild { get; set; }

        public FightResultTaxCollectorListEntry()
        {
        }

        public FightResultTaxCollectorListEntry(short outcome, byte wave, FightLoot rewards, double id, bool alive, byte level, BasicGuildInformations guildInfo, int experienceForGuild) : base(outcome, wave, rewards, id, alive)
        {
            Level = level;
            GuildInfo = guildInfo;
            ExperienceForGuild = experienceForGuild;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Level);
            GuildInfo.Serialize(writer);
            writer.WriteInt(ExperienceForGuild);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadByte();
            GuildInfo = new BasicGuildInformations();
            GuildInfo.Deserialize(reader);
            ExperienceForGuild = reader.ReadInt();
        }
    }
}