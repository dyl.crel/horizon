namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamInformations : AbstractFightTeamInformations
    {
        public override short Protocol => 33;

        public FightTeamMemberInformations[] TeamMembers { get; set; }

        public FightTeamInformations()
        {
        }

        public FightTeamInformations(byte teamId, double leaderId, sbyte teamSide, byte teamTypeId, byte nbWaves, FightTeamMemberInformations[] teamMembers) : base(teamId, leaderId, teamSide, teamTypeId, nbWaves)
        {
            TeamMembers = teamMembers;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(TeamMembers.Length);
            for (var i = 0; i < TeamMembers.Length; i++)
            {
                writer.WriteShort(TeamMembers[i].Protocol);
                TeamMembers[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TeamMembers = new FightTeamMemberInformations[reader.ReadShort()];
            for (var i = 0; i < TeamMembers.Length; i++)
            {
                TeamMembers[i] = ProtocolTypesManager.Instance<FightTeamMemberInformations>(reader.ReadShort());
                TeamMembers[i].Deserialize(reader);
            }
        }
    }
}