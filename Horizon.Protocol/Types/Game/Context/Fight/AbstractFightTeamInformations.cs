namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class AbstractFightTeamInformations : NetworkType
    {
        public override short Protocol => 116;

        public byte TeamId { get; set; }

        public double LeaderId { get; set; }

        public sbyte TeamSide { get; set; }

        public byte TeamTypeId { get; set; }

        public byte NbWaves { get; set; }

        public AbstractFightTeamInformations()
        {
        }

        public AbstractFightTeamInformations(byte teamId, double leaderId, sbyte teamSide, byte teamTypeId, byte nbWaves)
        {
            TeamId = teamId;
            LeaderId = leaderId;
            TeamSide = teamSide;
            TeamTypeId = teamTypeId;
            NbWaves = nbWaves;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(TeamId);
            writer.WriteDouble(LeaderId);
            writer.WriteSByte(TeamSide);
            writer.WriteByte(TeamTypeId);
            writer.WriteByte(NbWaves);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TeamId = reader.ReadByte();
            LeaderId = reader.ReadDouble();
            TeamSide = reader.ReadSByte();
            TeamTypeId = reader.ReadByte();
            NbWaves = reader.ReadByte();
        }
    }
}