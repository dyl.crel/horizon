namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamMemberCharacterInformations : FightTeamMemberInformations
    {
        public override short Protocol => 13;

        public string Name { get; set; }

        public short Level { get; set; }

        public FightTeamMemberCharacterInformations()
        {
        }

        public FightTeamMemberCharacterInformations(double id, string name, short level) : base(id)
        {
            Name = name;
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            writer.WriteVarShort(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Level = reader.ReadVarShort();
        }
    }
}