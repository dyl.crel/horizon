namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultFighterListEntry : FightResultListEntry
    {
        public override short Protocol => 189;

        public double Id { get; set; }

        public bool Alive { get; set; }

        public FightResultFighterListEntry()
        {
        }

        public FightResultFighterListEntry(short outcome, byte wave, FightLoot rewards, double id, bool alive) : base(outcome, wave, rewards)
        {
            Id = id;
            Alive = alive;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(Id);
            writer.WriteBoolean(Alive);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Id = reader.ReadDouble();
            Alive = reader.ReadBoolean();
        }
    }
}