namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultExperienceData : FightResultAdditionalData
    {
        public override short Protocol => 192;

        public bool ShowExperience { get; set; }

        public bool ShowExperienceLevelFloor { get; set; }

        public bool ShowExperienceNextLevelFloor { get; set; }

        public bool ShowExperienceFightDelta { get; set; }

        public bool ShowExperienceForGuild { get; set; }

        public bool ShowExperienceForMount { get; set; }

        public bool IsIncarnationExperience { get; set; }

        public long Experience { get; set; }

        public long ExperienceLevelFloor { get; set; }

        public long ExperienceNextLevelFloor { get; set; }

        public long ExperienceFightDelta { get; set; }

        public long ExperienceForGuild { get; set; }

        public long ExperienceForMount { get; set; }

        public byte RerollExperienceMul { get; set; }

        public FightResultExperienceData()
        {
        }

        public FightResultExperienceData(bool showExperience, bool showExperienceLevelFloor, bool showExperienceNextLevelFloor, bool showExperienceFightDelta, bool showExperienceForGuild, bool showExperienceForMount, bool isIncarnationExperience, long experience, long experienceLevelFloor, long experienceNextLevelFloor, long experienceFightDelta, long experienceForGuild, long experienceForMount, byte rerollExperienceMul)
        {
            ShowExperience = showExperience;
            ShowExperienceLevelFloor = showExperienceLevelFloor;
            ShowExperienceNextLevelFloor = showExperienceNextLevelFloor;
            ShowExperienceFightDelta = showExperienceFightDelta;
            ShowExperienceForGuild = showExperienceForGuild;
            ShowExperienceForMount = showExperienceForMount;
            IsIncarnationExperience = isIncarnationExperience;
            Experience = experience;
            ExperienceLevelFloor = experienceLevelFloor;
            ExperienceNextLevelFloor = experienceNextLevelFloor;
            ExperienceFightDelta = experienceFightDelta;
            ExperienceForGuild = experienceForGuild;
            ExperienceForMount = experienceForMount;
            RerollExperienceMul = rerollExperienceMul;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, ShowExperience);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, ShowExperienceLevelFloor);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, ShowExperienceNextLevelFloor);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, ShowExperienceFightDelta);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 4, ShowExperienceForGuild);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 5, ShowExperienceForMount);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 6, IsIncarnationExperience);
            writer.WriteByte(flag0);
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExperienceLevelFloor);
            writer.WriteVarLong(ExperienceNextLevelFloor);
            writer.WriteVarLong(ExperienceFightDelta);
            writer.WriteVarLong(ExperienceForGuild);
            writer.WriteVarLong(ExperienceForMount);
            writer.WriteByte(RerollExperienceMul);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            ShowExperience = BooleanByteWrapper.GetFlag(flag0, 0);
            ShowExperienceLevelFloor = BooleanByteWrapper.GetFlag(flag0, 1);
            ShowExperienceNextLevelFloor = BooleanByteWrapper.GetFlag(flag0, 2);
            ShowExperienceFightDelta = BooleanByteWrapper.GetFlag(flag0, 3);
            ShowExperienceForGuild = BooleanByteWrapper.GetFlag(flag0, 4);
            ShowExperienceForMount = BooleanByteWrapper.GetFlag(flag0, 5);
            IsIncarnationExperience = BooleanByteWrapper.GetFlag(flag0, 6);
            Experience = reader.ReadVarLong();
            ExperienceLevelFloor = reader.ReadVarLong();
            ExperienceNextLevelFloor = reader.ReadVarLong();
            ExperienceFightDelta = reader.ReadVarLong();
            ExperienceForGuild = reader.ReadVarLong();
            ExperienceForMount = reader.ReadVarLong();
            RerollExperienceMul = reader.ReadByte();
        }
    }
}