namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightTeamMemberMonsterInformations : FightTeamMemberInformations
    {
        public override short Protocol => 6;

        public int MonsterId { get; set; }

        public byte Grade { get; set; }

        public FightTeamMemberMonsterInformations()
        {
        }

        public FightTeamMemberMonsterInformations(double id, int monsterId, byte grade) : base(id)
        {
            MonsterId = monsterId;
            Grade = grade;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(MonsterId);
            writer.WriteByte(Grade);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MonsterId = reader.ReadInt();
            Grade = reader.ReadByte();
        }
    }
}