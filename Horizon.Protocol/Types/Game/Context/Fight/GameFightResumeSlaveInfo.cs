namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightResumeSlaveInfo : NetworkType
    {
        public override short Protocol => 364;

        public double SlaveId { get; set; }

        public GameFightSpellCooldown[] SpellCooldowns { get; set; }

        public byte SummonCount { get; set; }

        public byte BombCount { get; set; }

        public GameFightResumeSlaveInfo()
        {
        }

        public GameFightResumeSlaveInfo(double slaveId, GameFightSpellCooldown[] spellCooldowns, byte summonCount, byte bombCount)
        {
            SlaveId = slaveId;
            SpellCooldowns = spellCooldowns;
            SummonCount = summonCount;
            BombCount = bombCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(SlaveId);
            writer.WriteShort(SpellCooldowns.Length);
            for (var i = 0; i < SpellCooldowns.Length; i++)
            {
                SpellCooldowns[i].Serialize(writer);
            }
            writer.WriteByte(SummonCount);
            writer.WriteByte(BombCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SlaveId = reader.ReadDouble();
            SpellCooldowns = new GameFightSpellCooldown[reader.ReadShort()];
            for (var i = 0; i < SpellCooldowns.Length; i++)
            {
                SpellCooldowns[i] = new GameFightSpellCooldown();
                SpellCooldowns[i].Deserialize(reader);
            }
            SummonCount = reader.ReadByte();
            BombCount = reader.ReadByte();
        }
    }
}