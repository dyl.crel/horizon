namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultAdditionalData : NetworkType
    {
        public override short Protocol => 191;

        public FightResultAdditionalData()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}