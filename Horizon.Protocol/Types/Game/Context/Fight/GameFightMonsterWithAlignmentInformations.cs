using Horizon.Protocol.Types.Game.Character.Alignment;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightMonsterWithAlignmentInformations : GameFightMonsterInformations
    {
        public override short Protocol => 203;

        public ActorAlignmentInformations AlignmentInfos { get; set; }

        public GameFightMonsterWithAlignmentInformations()
        {
        }

        public GameFightMonsterWithAlignmentInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions, short creatureGenericId, byte creatureGrade, short creatureLevel, ActorAlignmentInformations alignmentInfos) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions, creatureGenericId, creatureGrade, creatureLevel)
        {
            AlignmentInfos = alignmentInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AlignmentInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AlignmentInfos = new ActorAlignmentInformations();
            AlignmentInfos.Deserialize(reader);
        }
    }
}