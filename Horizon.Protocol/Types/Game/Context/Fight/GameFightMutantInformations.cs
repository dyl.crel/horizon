using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightMutantInformations : GameFightFighterNamedInformations
    {
        public override short Protocol => 50;

        public byte PowerLevel { get; set; }

        public GameFightMutantInformations()
        {
        }

        public GameFightMutantInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions, string name, PlayerStatus status, short leagueId, int ladderPosition, bool hiddenInPrefight, byte powerLevel) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions, name, status, leagueId, ladderPosition, hiddenInPrefight)
        {
            PowerLevel = powerLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PowerLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PowerLevel = reader.ReadByte();
        }
    }
}