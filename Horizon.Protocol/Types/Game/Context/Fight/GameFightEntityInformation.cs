using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class GameFightEntityInformation : GameFightFighterInformations
    {
        public override short Protocol => 551;

        public byte EntityModelId { get; set; }

        public short Level { get; set; }

        public double MasterId { get; set; }

        public GameFightEntityInformation()
        {
        }

        public GameFightEntityInformation(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte teamId, byte wave, bool alive, GameFightMinimalStats stats, short[] previousPositions, byte entityModelId, short level, double masterId) : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            EntityModelId = entityModelId;
            Level = level;
            MasterId = masterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(EntityModelId);
            writer.WriteVarShort(Level);
            writer.WriteDouble(MasterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            EntityModelId = reader.ReadByte();
            Level = reader.ReadVarShort();
            MasterId = reader.ReadDouble();
        }
    }
}