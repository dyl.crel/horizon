namespace Horizon.Protocol.Types.Game.Context.Fight
{
    public class FightResultMutantListEntry : FightResultFighterListEntry
    {
        public override short Protocol => 216;

        public short Level { get; set; }

        public FightResultMutantListEntry()
        {
        }

        public FightResultMutantListEntry(short outcome, byte wave, FightLoot rewards, double id, bool alive, short level) : base(outcome, wave, rewards, id, alive)
        {
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarShort();
        }
    }
}