using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Context
{
    public class TaxCollectorStaticInformations : NetworkType
    {
        public override short Protocol => 147;

        public short FirstNameId { get; set; }

        public short LastNameId { get; set; }

        public GuildInformations GuildIdentity { get; set; }

        public TaxCollectorStaticInformations()
        {
        }

        public TaxCollectorStaticInformations(short firstNameId, short lastNameId, GuildInformations guildIdentity)
        {
            FirstNameId = firstNameId;
            LastNameId = lastNameId;
            GuildIdentity = guildIdentity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            GuildIdentity.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FirstNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
            GuildIdentity = new GuildInformations();
            GuildIdentity.Deserialize(reader);
        }
    }
}