using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context
{
    public class GameRolePlayTaxCollectorInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 148;

        public byte GuildLevel { get; set; }

        public int TaxCollectorAttack { get; set; }

        public TaxCollectorStaticInformations Identification { get; set; }

        public GameRolePlayTaxCollectorInformations()
        {
        }

        public GameRolePlayTaxCollectorInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, byte guildLevel, int taxCollectorAttack, TaxCollectorStaticInformations identification) : base(contextualId, look, disposition)
        {
            GuildLevel = guildLevel;
            TaxCollectorAttack = taxCollectorAttack;
            Identification = identification;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(GuildLevel);
            writer.WriteInt(TaxCollectorAttack);
            writer.WriteShort(Identification.Protocol);
            Identification.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuildLevel = reader.ReadByte();
            TaxCollectorAttack = reader.ReadInt();
            Identification = ProtocolTypesManager.Instance<TaxCollectorStaticInformations>(reader.ReadShort());
            Identification.Deserialize(reader);
        }
    }
}