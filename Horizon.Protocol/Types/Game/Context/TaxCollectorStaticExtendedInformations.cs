using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Context
{
    public class TaxCollectorStaticExtendedInformations : TaxCollectorStaticInformations
    {
        public override short Protocol => 440;

        public AllianceInformations AllianceIdentity { get; set; }

        public TaxCollectorStaticExtendedInformations()
        {
        }

        public TaxCollectorStaticExtendedInformations(short firstNameId, short lastNameId, GuildInformations guildIdentity, AllianceInformations allianceIdentity) : base(firstNameId, lastNameId, guildIdentity)
        {
            AllianceIdentity = allianceIdentity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AllianceIdentity.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceIdentity = new AllianceInformations();
            AllianceIdentity.Deserialize(reader);
        }
    }
}