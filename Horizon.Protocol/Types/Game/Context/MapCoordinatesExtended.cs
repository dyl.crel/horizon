namespace Horizon.Protocol.Types.Game.Context
{
    public class MapCoordinatesExtended : MapCoordinatesAndId
    {
        public override short Protocol => 176;

        public short SubAreaId { get; set; }

        public MapCoordinatesExtended()
        {
        }

        public MapCoordinatesExtended(short worldX, short worldY, double mapId, short subAreaId) : base(worldX, worldY, mapId)
        {
            SubAreaId = subAreaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SubAreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SubAreaId = reader.ReadVarShort();
        }
    }
}