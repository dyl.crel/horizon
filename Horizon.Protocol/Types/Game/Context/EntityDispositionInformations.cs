namespace Horizon.Protocol.Types.Game.Context
{
    public class EntityDispositionInformations : NetworkType
    {
        public override short Protocol => 60;

        public short CellId { get; set; }

        public byte Direction { get; set; }

        public EntityDispositionInformations()
        {
        }

        public EntityDispositionInformations(short cellId, byte direction)
        {
            CellId = cellId;
            Direction = direction;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(CellId);
            writer.WriteByte(Direction);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadShort();
            Direction = reader.ReadByte();
        }
    }
}