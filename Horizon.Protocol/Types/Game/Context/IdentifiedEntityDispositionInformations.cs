namespace Horizon.Protocol.Types.Game.Context
{
    public class IdentifiedEntityDispositionInformations : EntityDispositionInformations
    {
        public override short Protocol => 107;

        public double Id { get; set; }

        public IdentifiedEntityDispositionInformations()
        {
        }

        public IdentifiedEntityDispositionInformations(short cellId, byte direction, double id) : base(cellId, direction)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Id = reader.ReadDouble();
        }
    }
}