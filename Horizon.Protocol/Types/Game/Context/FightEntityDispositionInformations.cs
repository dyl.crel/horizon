namespace Horizon.Protocol.Types.Game.Context
{
    public class FightEntityDispositionInformations : EntityDispositionInformations
    {
        public override short Protocol => 217;

        public double CarryingCharacterId { get; set; }

        public FightEntityDispositionInformations()
        {
        }

        public FightEntityDispositionInformations(short cellId, byte direction, double carryingCharacterId) : base(cellId, direction)
        {
            CarryingCharacterId = carryingCharacterId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(CarryingCharacterId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CarryingCharacterId = reader.ReadDouble();
        }
    }
}