using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context
{
    public class GameContextActorInformations : NetworkType
    {
        public override short Protocol => 150;

        public double ContextualId { get; set; }

        public EntityLook Look { get; set; }

        public EntityDispositionInformations Disposition { get; set; }

        public GameContextActorInformations()
        {
        }

        public GameContextActorInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition)
        {
            ContextualId = contextualId;
            Look = look;
            Disposition = disposition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(ContextualId);
            Look.Serialize(writer);
            writer.WriteShort(Disposition.Protocol);
            Disposition.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ContextualId = reader.ReadDouble();
            Look = new EntityLook();
            Look.Deserialize(reader);
            Disposition = ProtocolTypesManager.Instance<EntityDispositionInformations>(reader.ReadShort());
            Disposition.Deserialize(reader);
        }
    }
}