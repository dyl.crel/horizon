using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class AllianceInformations : BasicNamedAllianceInformations
    {
        public override short Protocol => 417;

        public GuildEmblem AllianceEmblem { get; set; }

        public AllianceInformations()
        {
        }

        public AllianceInformations(int allianceId, string allianceTag, string allianceName, GuildEmblem allianceEmblem) : base(allianceId, allianceTag, allianceName)
        {
            AllianceEmblem = allianceEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AllianceEmblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceEmblem = new GuildEmblem();
            AllianceEmblem.Deserialize(reader);
        }
    }
}