namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class AtlasPointsInformations : NetworkType
    {
        public override short Protocol => 175;

        public byte Type { get; set; }

        public MapCoordinatesExtended[] Coords { get; set; }

        public AtlasPointsInformations()
        {
        }

        public AtlasPointsInformations(byte type, MapCoordinatesExtended[] coords)
        {
            Type = type;
            Coords = coords;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
            writer.WriteShort(Coords.Length);
            for (var i = 0; i < Coords.Length; i++)
            {
                Coords[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
            Coords = new MapCoordinatesExtended[reader.ReadShort()];
            for (var i = 0; i < Coords.Length; i++)
            {
                Coords[i] = new MapCoordinatesExtended();
                Coords[i].Deserialize(reader);
            }
        }
    }
}