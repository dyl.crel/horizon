using Horizon.Protocol.Types.Game.Look;
using Horizon.Protocol.Types.Game.Prism;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayPrismInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 161;

        public PrismInformation Prism { get; set; }

        public GameRolePlayPrismInformations()
        {
        }

        public GameRolePlayPrismInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, PrismInformation prism) : base(contextualId, look, disposition)
        {
            Prism = prism;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Prism.Protocol);
            Prism.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Prism = ProtocolTypesManager.Instance<PrismInformation>(reader.ReadShort());
            Prism.Deserialize(reader);
        }
    }
}