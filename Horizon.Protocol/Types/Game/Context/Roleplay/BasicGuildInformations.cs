using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class BasicGuildInformations : AbstractSocialGroupInfos
    {
        public override short Protocol => 365;

        public int GuildId { get; set; }

        public string GuildName { get; set; }

        public byte GuildLevel { get; set; }

        public BasicGuildInformations()
        {
        }

        public BasicGuildInformations(int guildId, string guildName, byte guildLevel)
        {
            GuildId = guildId;
            GuildName = guildName;
            GuildLevel = guildLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(GuildId);
            writer.WriteUTF(GuildName);
            writer.WriteByte(GuildLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuildId = reader.ReadVarInt();
            GuildName = reader.ReadUTF();
            GuildLevel = reader.ReadByte();
        }
    }
}