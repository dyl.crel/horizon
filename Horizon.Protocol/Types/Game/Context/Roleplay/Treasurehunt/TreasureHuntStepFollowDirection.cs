namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntStepFollowDirection : TreasureHuntStep
    {
        public override short Protocol => 468;

        public byte Direction { get; set; }

        public short MapCount { get; set; }

        public TreasureHuntStepFollowDirection()
        {
        }

        public TreasureHuntStepFollowDirection(byte direction, short mapCount)
        {
            Direction = direction;
            MapCount = mapCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Direction);
            writer.WriteVarShort(MapCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Direction = reader.ReadByte();
            MapCount = reader.ReadVarShort();
        }
    }
}