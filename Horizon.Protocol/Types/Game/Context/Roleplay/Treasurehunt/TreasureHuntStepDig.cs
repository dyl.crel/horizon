namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntStepDig : TreasureHuntStep
    {
        public override short Protocol => 465;

        public TreasureHuntStepDig()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}