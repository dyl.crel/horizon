namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class PortalInformation : NetworkType
    {
        public override short Protocol => 466;

        public int PortalId { get; set; }

        public short AreaId { get; set; }

        public PortalInformation()
        {
        }

        public PortalInformation(int portalId, short areaId)
        {
            PortalId = portalId;
            AreaId = areaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(PortalId);
            writer.WriteShort(AreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PortalId = reader.ReadInt();
            AreaId = reader.ReadShort();
        }
    }
}