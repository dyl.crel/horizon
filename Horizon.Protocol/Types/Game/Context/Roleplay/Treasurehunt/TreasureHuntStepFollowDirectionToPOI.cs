namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntStepFollowDirectionToPOI : TreasureHuntStep
    {
        public override short Protocol => 461;

        public byte Direction { get; set; }

        public short PoiLabelId { get; set; }

        public TreasureHuntStepFollowDirectionToPOI()
        {
        }

        public TreasureHuntStepFollowDirectionToPOI(byte direction, short poiLabelId)
        {
            Direction = direction;
            PoiLabelId = poiLabelId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Direction);
            writer.WriteVarShort(PoiLabelId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Direction = reader.ReadByte();
            PoiLabelId = reader.ReadVarShort();
        }
    }
}