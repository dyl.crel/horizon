namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntStepFollowDirectionToHint : TreasureHuntStep
    {
        public override short Protocol => 472;

        public byte Direction { get; set; }

        public short NpcId { get; set; }

        public TreasureHuntStepFollowDirectionToHint()
        {
        }

        public TreasureHuntStepFollowDirectionToHint(byte direction, short npcId)
        {
            Direction = direction;
            NpcId = npcId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Direction);
            writer.WriteVarShort(NpcId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Direction = reader.ReadByte();
            NpcId = reader.ReadVarShort();
        }
    }
}