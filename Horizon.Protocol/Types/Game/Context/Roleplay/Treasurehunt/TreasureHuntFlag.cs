namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntFlag : NetworkType
    {
        public override short Protocol => 473;

        public double MapId { get; set; }

        public byte State { get; set; }

        public TreasureHuntFlag()
        {
        }

        public TreasureHuntFlag(double mapId, byte state)
        {
            MapId = mapId;
            State = state;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteByte(State);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MapId = reader.ReadDouble();
            State = reader.ReadByte();
        }
    }
}