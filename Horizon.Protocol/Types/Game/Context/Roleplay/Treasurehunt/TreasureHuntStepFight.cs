namespace Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt
{
    public class TreasureHuntStepFight : TreasureHuntStep
    {
        public override short Protocol => 462;

        public TreasureHuntStepFight()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}