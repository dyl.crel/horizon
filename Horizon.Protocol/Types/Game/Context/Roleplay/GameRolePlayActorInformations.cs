using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayActorInformations : GameContextActorInformations
    {
        public override short Protocol => 141;

        public GameRolePlayActorInformations()
        {
        }

        public GameRolePlayActorInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition) : base(contextualId, look, disposition)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}