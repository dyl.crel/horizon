namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class BasicNamedAllianceInformations : BasicAllianceInformations
    {
        public override short Protocol => 418;

        public string AllianceName { get; set; }

        public BasicNamedAllianceInformations()
        {
        }

        public BasicNamedAllianceInformations(int allianceId, string allianceTag, string allianceName) : base(allianceId, allianceTag)
        {
            AllianceName = allianceName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(AllianceName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceName = reader.ReadUTF();
        }
    }
}