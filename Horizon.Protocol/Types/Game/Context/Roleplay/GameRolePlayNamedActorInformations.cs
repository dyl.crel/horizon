using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayNamedActorInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 154;

        public string Name { get; set; }

        public GameRolePlayNamedActorInformations()
        {
        }

        public GameRolePlayNamedActorInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, string name) : base(contextualId, look, disposition)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
        }
    }
}