using Horizon.Protocol.Types.Game.Social;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class BasicAllianceInformations : AbstractSocialGroupInfos
    {
        public override short Protocol => 419;

        public int AllianceId { get; set; }

        public string AllianceTag { get; set; }

        public BasicAllianceInformations()
        {
        }

        public BasicAllianceInformations(int allianceId, string allianceTag)
        {
            AllianceId = allianceId;
            AllianceTag = allianceTag;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(AllianceId);
            writer.WriteUTF(AllianceTag);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceId = reader.ReadVarInt();
            AllianceTag = reader.ReadUTF();
        }
    }
}