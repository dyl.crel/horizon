namespace Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena
{
    public class ArenaRanking : NetworkType
    {
        public override short Protocol => 554;

        public short Rank { get; set; }

        public short BestRank { get; set; }

        public ArenaRanking()
        {
        }

        public ArenaRanking(short rank, short bestRank)
        {
            Rank = rank;
            BestRank = bestRank;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Rank);
            writer.WriteVarShort(BestRank);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Rank = reader.ReadVarShort();
            BestRank = reader.ReadVarShort();
        }
    }
}