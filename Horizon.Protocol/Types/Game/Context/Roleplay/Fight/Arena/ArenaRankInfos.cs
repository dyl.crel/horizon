namespace Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena
{
    public class ArenaRankInfos : NetworkType
    {
        public override short Protocol => 499;

        public ArenaRanking Ranking { get; set; }

        public ArenaLeagueRanking LeagueRanking { get; set; }

        public short VictoryCount { get; set; }

        public short Fightcount { get; set; }

        public short NumFightNeededForLadder { get; set; }

        public ArenaRankInfos()
        {
        }

        public ArenaRankInfos(ArenaRanking ranking, ArenaLeagueRanking leagueRanking, short victoryCount, short fightcount, short numFightNeededForLadder)
        {
            Ranking = ranking;
            LeagueRanking = leagueRanking;
            VictoryCount = victoryCount;
            Fightcount = fightcount;
            NumFightNeededForLadder = numFightNeededForLadder;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            if (Ranking == null)
            {
                writer.WriteByte(0);
            }
            else
            {
                writer.WriteByte(1);
                Ranking.Serialize(writer);
            }

            if (LeagueRanking == null)
            {
                writer.WriteByte(0);
            }
            else
            {
                writer.WriteByte(1);
                LeagueRanking.Serialize(writer);
            }

            writer.WriteVarShort(VictoryCount);
            writer.WriteVarShort(Fightcount);
            writer.WriteShort(NumFightNeededForLadder);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            if (reader.ReadByte() == 0)
            {
                Ranking = null;
            }
            else
            {
                Ranking = new ArenaRanking();
                Ranking.Deserialize(reader);
            }

            if (reader.ReadByte() == 0)
            {
                LeagueRanking = null;
            }
            else
            {
                LeagueRanking = new ArenaLeagueRanking();
                LeagueRanking.Deserialize(reader);
            }

            VictoryCount = reader.ReadVarShort();
            Fightcount = reader.ReadVarShort();
            NumFightNeededForLadder = reader.ReadShort();
        }
    }
}