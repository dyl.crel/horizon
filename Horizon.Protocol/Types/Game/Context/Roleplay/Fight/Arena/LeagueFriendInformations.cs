using Horizon.Protocol.Types.Game.Friend;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena
{
    public class LeagueFriendInformations : AbstractContactInformations
    {
        public override short Protocol => 555;

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public short Level { get; set; }

        public short LeagueId { get; set; }

        public short TotalLeaguePoints { get; set; }

        public int LadderPosition { get; set; }

        public LeagueFriendInformations()
        {
        }

        public LeagueFriendInformations(int accountId, string accountName, long playerId, string playerName, byte breed, bool sex, short level, short leagueId, short totalLeaguePoints, int ladderPosition) : base(accountId, accountName)
        {
            PlayerId = playerId;
            PlayerName = playerName;
            Breed = breed;
            Sex = sex;
            Level = level;
            LeagueId = leagueId;
            TotalLeaguePoints = totalLeaguePoints;
            LadderPosition = ladderPosition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(Level);
            writer.WriteVarShort(LeagueId);
            writer.WriteVarShort(TotalLeaguePoints);
            writer.WriteInt(LadderPosition);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            Level = reader.ReadVarShort();
            LeagueId = reader.ReadVarShort();
            TotalLeaguePoints = reader.ReadVarShort();
            LadderPosition = reader.ReadInt();
        }
    }
}