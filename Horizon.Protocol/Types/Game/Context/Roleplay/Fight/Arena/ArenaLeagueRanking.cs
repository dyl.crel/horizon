namespace Horizon.Protocol.Types.Game.Context.Roleplay.Fight.Arena
{
    public class ArenaLeagueRanking : NetworkType
    {
        public override short Protocol => 553;

        public short Rank { get; set; }

        public short LeagueId { get; set; }

        public short LeaguePoints { get; set; }

        public short TotalLeaguePoints { get; set; }

        public int LadderPosition { get; set; }

        public ArenaLeagueRanking()
        {
        }

        public ArenaLeagueRanking(short rank, short leagueId, short leaguePoints, short totalLeaguePoints, int ladderPosition)
        {
            Rank = rank;
            LeagueId = leagueId;
            LeaguePoints = leaguePoints;
            TotalLeaguePoints = totalLeaguePoints;
            LadderPosition = ladderPosition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Rank);
            writer.WriteVarShort(LeagueId);
            writer.WriteVarShort(LeaguePoints);
            writer.WriteVarShort(TotalLeaguePoints);
            writer.WriteInt(LadderPosition);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Rank = reader.ReadVarShort();
            LeagueId = reader.ReadVarShort();
            LeaguePoints = reader.ReadVarShort();
            TotalLeaguePoints = reader.ReadVarShort();
            LadderPosition = reader.ReadInt();
        }
    }
}