using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionFollowers : HumanOption
    {
        public override short Protocol => 410;

        public IndexedEntityLook[] FollowingCharactersLook { get; set; }

        public HumanOptionFollowers()
        {
        }

        public HumanOptionFollowers(IndexedEntityLook[] followingCharactersLook)
        {
            FollowingCharactersLook = followingCharactersLook;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(FollowingCharactersLook.Length);
            for (var i = 0; i < FollowingCharactersLook.Length; i++)
            {
                FollowingCharactersLook[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            FollowingCharactersLook = new IndexedEntityLook[reader.ReadShort()];
            for (var i = 0; i < FollowingCharactersLook.Length; i++)
            {
                FollowingCharactersLook[i] = new IndexedEntityLook();
                FollowingCharactersLook[i].Deserialize(reader);
            }
        }
    }
}