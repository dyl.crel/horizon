using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GuildInAllianceInformations : GuildInformations
    {
        public override short Protocol => 420;

        public byte NbMembers { get; set; }

        public int JoinDate { get; set; }

        public GuildInAllianceInformations()
        {
        }

        public GuildInAllianceInformations(int guildId, string guildName, byte guildLevel, GuildEmblem guildEmblem, byte nbMembers, int joinDate) : base(guildId, guildName, guildLevel, guildEmblem)
        {
            NbMembers = nbMembers;
            JoinDate = joinDate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(NbMembers);
            writer.WriteInt(JoinDate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NbMembers = reader.ReadByte();
            JoinDate = reader.ReadInt();
        }
    }
}