namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionObjectUse : HumanOption
    {
        public override short Protocol => 449;

        public byte DelayTypeId { get; set; }

        public double DelayEndTime { get; set; }

        public short ObjectGID { get; set; }

        public HumanOptionObjectUse()
        {
        }

        public HumanOptionObjectUse(byte delayTypeId, double delayEndTime, short objectGID)
        {
            DelayTypeId = delayTypeId;
            DelayEndTime = delayEndTime;
            ObjectGID = objectGID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(DelayTypeId);
            writer.WriteDouble(DelayEndTime);
            writer.WriteVarShort(ObjectGID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DelayTypeId = reader.ReadByte();
            DelayEndTime = reader.ReadDouble();
            ObjectGID = reader.ReadVarShort();
        }
    }
}