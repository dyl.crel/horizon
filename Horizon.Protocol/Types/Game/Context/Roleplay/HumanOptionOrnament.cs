namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionOrnament : HumanOption
    {
        public override short Protocol => 411;

        public short OrnamentId { get; set; }

        public short Level { get; set; }

        public short LeagueId { get; set; }

        public int LadderPosition { get; set; }

        public HumanOptionOrnament()
        {
        }

        public HumanOptionOrnament(short ornamentId, short level, short leagueId, int ladderPosition)
        {
            OrnamentId = ornamentId;
            Level = level;
            LeagueId = leagueId;
            LadderPosition = ladderPosition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(OrnamentId);
            writer.WriteVarShort(Level);
            writer.WriteVarShort(LeagueId);
            writer.WriteInt(LadderPosition);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            OrnamentId = reader.ReadVarShort();
            Level = reader.ReadVarShort();
            LeagueId = reader.ReadVarShort();
            LadderPosition = reader.ReadInt();
        }
    }
}