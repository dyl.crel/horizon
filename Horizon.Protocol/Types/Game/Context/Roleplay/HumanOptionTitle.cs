namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionTitle : HumanOption
    {
        public override short Protocol => 408;

        public short TitleId { get; set; }

        public string TitleParam { get; set; }

        public HumanOptionTitle()
        {
        }

        public HumanOptionTitle(short titleId, string titleParam)
        {
            TitleId = titleId;
            TitleParam = titleParam;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(TitleId);
            writer.WriteUTF(TitleParam);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            TitleId = reader.ReadVarShort();
            TitleParam = reader.ReadUTF();
        }
    }
}