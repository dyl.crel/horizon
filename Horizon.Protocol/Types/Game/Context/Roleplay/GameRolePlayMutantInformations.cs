using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayMutantInformations : GameRolePlayHumanoidInformations
    {
        public override short Protocol => 3;

        public short MonsterId { get; set; }

        public byte PowerLevel { get; set; }

        public GameRolePlayMutantInformations()
        {
        }

        public GameRolePlayMutantInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, string name, HumanInformations humanoidInfo, int accountId, short monsterId, byte powerLevel) : base(contextualId, look, disposition, name, humanoidInfo, accountId)
        {
            MonsterId = monsterId;
            PowerLevel = powerLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(MonsterId);
            writer.WriteByte(PowerLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MonsterId = reader.ReadVarShort();
            PowerLevel = reader.ReadByte();
        }
    }
}