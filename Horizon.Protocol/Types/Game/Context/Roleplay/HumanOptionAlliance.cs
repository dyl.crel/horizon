namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionAlliance : HumanOption
    {
        public override short Protocol => 425;

        public AllianceInformations AllianceInformations { get; set; }

        public byte Aggressable { get; set; }

        public HumanOptionAlliance()
        {
        }

        public HumanOptionAlliance(AllianceInformations allianceInformations, byte aggressable)
        {
            AllianceInformations = allianceInformations;
            Aggressable = aggressable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AllianceInformations.Serialize(writer);
            writer.WriteByte(Aggressable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AllianceInformations = new AllianceInformations();
            AllianceInformations.Deserialize(reader);
            Aggressable = reader.ReadByte();
        }
    }
}