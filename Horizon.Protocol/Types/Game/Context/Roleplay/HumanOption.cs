namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOption : NetworkType
    {
        public override short Protocol => 406;

        public HumanOption()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}