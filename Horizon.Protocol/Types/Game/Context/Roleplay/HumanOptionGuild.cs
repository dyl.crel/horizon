namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionGuild : HumanOption
    {
        public override short Protocol => 409;

        public GuildInformations GuildInformations { get; set; }

        public HumanOptionGuild()
        {
        }

        public HumanOptionGuild(GuildInformations guildInformations)
        {
            GuildInformations = guildInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            GuildInformations.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuildInformations = new GuildInformations();
            GuildInformations.Deserialize(reader);
        }
    }
}