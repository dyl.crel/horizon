namespace Horizon.Protocol.Types.Game.Context.Roleplay.Quest
{
    public class QuestObjectiveInformations : NetworkType
    {
        public override short Protocol => 385;

        public short ObjectiveId { get; set; }

        public bool ObjectiveStatus { get; set; }

        public string[] DialogParams { get; set; }

        public QuestObjectiveInformations()
        {
        }

        public QuestObjectiveInformations(short objectiveId, bool objectiveStatus, string[] dialogParams)
        {
            ObjectiveId = objectiveId;
            ObjectiveStatus = objectiveStatus;
            DialogParams = dialogParams;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ObjectiveId);
            writer.WriteBoolean(ObjectiveStatus);
            writer.WriteShort(DialogParams.Length);
            for (var i = 0; i < DialogParams.Length; i++)
            {
                writer.WriteUTF(DialogParams[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectiveId = reader.ReadVarShort();
            ObjectiveStatus = reader.ReadBoolean();
            DialogParams = new string[reader.ReadShort()];
            for (var i = 0; i < DialogParams.Length; i++)
            {
                DialogParams[i] = reader.ReadUTF();
            }
        }
    }
}