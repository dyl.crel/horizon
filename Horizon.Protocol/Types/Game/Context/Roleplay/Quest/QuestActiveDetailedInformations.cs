namespace Horizon.Protocol.Types.Game.Context.Roleplay.Quest
{
    public class QuestActiveDetailedInformations : QuestActiveInformations
    {
        public override short Protocol => 382;

        public short StepId { get; set; }

        public QuestObjectiveInformations[] Objectives { get; set; }

        public QuestActiveDetailedInformations()
        {
        }

        public QuestActiveDetailedInformations(short questId, short stepId, QuestObjectiveInformations[] objectives) : base(questId)
        {
            StepId = stepId;
            Objectives = objectives;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(StepId);
            writer.WriteShort(Objectives.Length);
            for (var i = 0; i < Objectives.Length; i++)
            {
                writer.WriteShort(Objectives[i].Protocol);
                Objectives[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            StepId = reader.ReadVarShort();
            Objectives = new QuestObjectiveInformations[reader.ReadShort()];
            for (var i = 0; i < Objectives.Length; i++)
            {
                Objectives[i] = ProtocolTypesManager.Instance<QuestObjectiveInformations>(reader.ReadShort());
                Objectives[i].Deserialize(reader);
            }
        }
    }
}