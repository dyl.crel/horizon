namespace Horizon.Protocol.Types.Game.Context.Roleplay.Quest
{
    public class QuestObjectiveInformationsWithCompletion : QuestObjectiveInformations
    {
        public override short Protocol => 386;

        public short CurCompletion { get; set; }

        public short MaxCompletion { get; set; }

        public QuestObjectiveInformationsWithCompletion()
        {
        }

        public QuestObjectiveInformationsWithCompletion(short objectiveId, bool objectiveStatus, string[] dialogParams, short curCompletion, short maxCompletion) : base(objectiveId, objectiveStatus, dialogParams)
        {
            CurCompletion = curCompletion;
            MaxCompletion = maxCompletion;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CurCompletion);
            writer.WriteVarShort(MaxCompletion);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            CurCompletion = reader.ReadVarShort();
            MaxCompletion = reader.ReadVarShort();
        }
    }
}