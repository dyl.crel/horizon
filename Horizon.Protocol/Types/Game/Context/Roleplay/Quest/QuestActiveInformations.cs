namespace Horizon.Protocol.Types.Game.Context.Roleplay.Quest
{
    public class QuestActiveInformations : NetworkType
    {
        public override short Protocol => 381;

        public short QuestId { get; set; }

        public QuestActiveInformations()
        {
        }

        public QuestActiveInformations(short questId)
        {
            QuestId = questId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(QuestId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestId = reader.ReadVarShort();
        }
    }
}