namespace Horizon.Protocol.Types.Game.Context.Roleplay.Quest
{
    public class GameRolePlayNpcQuestFlag : NetworkType
    {
        public override short Protocol => 384;

        public short[] QuestsToValidId { get; set; }

        public short[] QuestsToStartId { get; set; }

        public GameRolePlayNpcQuestFlag()
        {
        }

        public GameRolePlayNpcQuestFlag(short[] questsToValidId, short[] questsToStartId)
        {
            QuestsToValidId = questsToValidId;
            QuestsToStartId = questsToStartId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(QuestsToValidId.Length);
            for (var i = 0; i < QuestsToValidId.Length; i++)
            {
                writer.WriteVarShort(QuestsToValidId[i]);
            }
            writer.WriteShort(QuestsToStartId.Length);
            for (var i = 0; i < QuestsToStartId.Length; i++)
            {
                writer.WriteVarShort(QuestsToStartId[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            QuestsToValidId = new short[reader.ReadShort()];
            for (var i = 0; i < QuestsToValidId.Length; i++)
            {
                QuestsToValidId[i] = reader.ReadVarShort();
            }
            QuestsToStartId = new short[reader.ReadShort()];
            for (var i = 0; i < QuestsToStartId.Length; i++)
            {
                QuestsToStartId[i] = reader.ReadVarShort();
            }
        }
    }
}