namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobBookSubscription : NetworkType
    {
        public override short Protocol => 500;

        public byte JobId { get; set; }

        public bool Subscribed { get; set; }

        public JobBookSubscription()
        {
        }

        public JobBookSubscription(byte jobId, bool subscribed)
        {
            JobId = jobId;
            Subscribed = subscribed;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
            writer.WriteBoolean(Subscribed);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
            Subscribed = reader.ReadBoolean();
        }
    }
}