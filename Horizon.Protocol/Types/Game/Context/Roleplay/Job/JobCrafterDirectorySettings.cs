namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectorySettings : NetworkType
    {
        public override short Protocol => 97;

        public byte JobId { get; set; }

        public byte MinLevel { get; set; }

        public bool Free { get; set; }

        public JobCrafterDirectorySettings()
        {
        }

        public JobCrafterDirectorySettings(byte jobId, byte minLevel, bool free)
        {
            JobId = jobId;
            MinLevel = minLevel;
            Free = free;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
            writer.WriteByte(MinLevel);
            writer.WriteBoolean(Free);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
            MinLevel = reader.ReadByte();
            Free = reader.ReadBoolean();
        }
    }
}