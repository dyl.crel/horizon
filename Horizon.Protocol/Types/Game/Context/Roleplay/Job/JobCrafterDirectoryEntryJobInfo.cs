namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryEntryJobInfo : NetworkType
    {
        public override short Protocol => 195;

        public byte JobId { get; set; }

        public byte JobLevel { get; set; }

        public bool Free { get; set; }

        public byte MinLevel { get; set; }

        public JobCrafterDirectoryEntryJobInfo()
        {
        }

        public JobCrafterDirectoryEntryJobInfo(byte jobId, byte jobLevel, bool free, byte minLevel)
        {
            JobId = jobId;
            JobLevel = jobLevel;
            Free = free;
            MinLevel = minLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
            writer.WriteByte(JobLevel);
            writer.WriteBoolean(Free);
            writer.WriteByte(MinLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
            JobLevel = reader.ReadByte();
            Free = reader.ReadBoolean();
            MinLevel = reader.ReadByte();
        }
    }
}