namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class DecraftedItemStackInfo : NetworkType
    {
        public override short Protocol => 481;

        public int ObjectUID { get; set; }

        public float BonusMin { get; set; }

        public float BonusMax { get; set; }

        public short[] RunesId { get; set; }

        public int[] RunesQty { get; set; }

        public DecraftedItemStackInfo()
        {
        }

        public DecraftedItemStackInfo(int objectUID, float bonusMin, float bonusMax, short[] runesId, int[] runesQty)
        {
            ObjectUID = objectUID;
            BonusMin = bonusMin;
            BonusMax = bonusMax;
            RunesId = runesId;
            RunesQty = runesQty;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteFloat(BonusMin);
            writer.WriteFloat(BonusMax);
            writer.WriteShort(RunesId.Length);
            for (var i = 0; i < RunesId.Length; i++)
            {
                writer.WriteVarShort(RunesId[i]);
            }
            writer.WriteShort(RunesQty.Length);
            for (var i = 0; i < RunesQty.Length; i++)
            {
                writer.WriteVarInt(RunesQty[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            BonusMin = reader.ReadFloat();
            BonusMax = reader.ReadFloat();
            RunesId = new short[reader.ReadShort()];
            for (var i = 0; i < RunesId.Length; i++)
            {
                RunesId[i] = reader.ReadVarShort();
            }
            RunesQty = new int[reader.ReadShort()];
            for (var i = 0; i < RunesQty.Length; i++)
            {
                RunesQty[i] = reader.ReadVarInt();
            }
        }
    }
}