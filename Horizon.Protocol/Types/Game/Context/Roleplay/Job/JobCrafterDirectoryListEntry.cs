namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryListEntry : NetworkType
    {
        public override short Protocol => 196;

        public JobCrafterDirectoryEntryPlayerInfo PlayerInfo { get; set; }

        public JobCrafterDirectoryEntryJobInfo JobInfo { get; set; }

        public JobCrafterDirectoryListEntry()
        {
        }

        public JobCrafterDirectoryListEntry(JobCrafterDirectoryEntryPlayerInfo playerInfo, JobCrafterDirectoryEntryJobInfo jobInfo)
        {
            PlayerInfo = playerInfo;
            JobInfo = jobInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            PlayerInfo.Serialize(writer);
            JobInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerInfo = new JobCrafterDirectoryEntryPlayerInfo();
            PlayerInfo.Deserialize(reader);
            JobInfo = new JobCrafterDirectoryEntryJobInfo();
            JobInfo.Deserialize(reader);
        }
    }
}