using Horizon.Protocol.Types.Game.Interactive.Skill;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobDescription : NetworkType
    {
        public override short Protocol => 101;

        public byte JobId { get; set; }

        public SkillActionDescription[] Skills { get; set; }

        public JobDescription()
        {
        }

        public JobDescription(byte jobId, SkillActionDescription[] skills)
        {
            JobId = jobId;
            Skills = skills;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
            writer.WriteShort(Skills.Length);
            for (var i = 0; i < Skills.Length; i++)
            {
                writer.WriteShort(Skills[i].Protocol);
                Skills[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
            Skills = new SkillActionDescription[reader.ReadShort()];
            for (var i = 0; i < Skills.Length; i++)
            {
                Skills[i] = ProtocolTypesManager.Instance<SkillActionDescription>(reader.ReadShort());
                Skills[i].Deserialize(reader);
            }
        }
    }
}