namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobExperience : NetworkType
    {
        public override short Protocol => 98;

        public byte JobId { get; set; }

        public byte JobLevel { get; set; }

        public long JobXP { get; set; }

        public long JobXpLevelFloor { get; set; }

        public long JobXpNextLevelFloor { get; set; }

        public JobExperience()
        {
        }

        public JobExperience(byte jobId, byte jobLevel, long jobXP, long jobXpLevelFloor, long jobXpNextLevelFloor)
        {
            JobId = jobId;
            JobLevel = jobLevel;
            JobXP = jobXP;
            JobXpLevelFloor = jobXpLevelFloor;
            JobXpNextLevelFloor = jobXpNextLevelFloor;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(JobId);
            writer.WriteByte(JobLevel);
            writer.WriteVarLong(JobXP);
            writer.WriteVarLong(JobXpLevelFloor);
            writer.WriteVarLong(JobXpNextLevelFloor);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            JobId = reader.ReadByte();
            JobLevel = reader.ReadByte();
            JobXP = reader.ReadVarLong();
            JobXpLevelFloor = reader.ReadVarLong();
            JobXpNextLevelFloor = reader.ReadVarLong();
        }
    }
}