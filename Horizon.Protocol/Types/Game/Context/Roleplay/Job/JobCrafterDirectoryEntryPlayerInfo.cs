using Horizon.Protocol.Types.Game.Character.Status;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Job
{
    public class JobCrafterDirectoryEntryPlayerInfo : NetworkType
    {
        public override short Protocol => 194;

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public byte AlignmentSide { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public bool IsInWorkshop { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public PlayerStatus Status { get; set; }

        public JobCrafterDirectoryEntryPlayerInfo()
        {
        }

        public JobCrafterDirectoryEntryPlayerInfo(long playerId, string playerName, byte alignmentSide, byte breed, bool sex, bool isInWorkshop, short worldX, short worldY, double mapId, short subAreaId, PlayerStatus status)
        {
            PlayerId = playerId;
            PlayerName = playerName;
            AlignmentSide = alignmentSide;
            Breed = breed;
            Sex = sex;
            IsInWorkshop = isInWorkshop;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteByte(AlignmentSide);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteBoolean(IsInWorkshop);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
            AlignmentSide = reader.ReadByte();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            IsInWorkshop = reader.ReadBoolean();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}