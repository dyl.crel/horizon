using Horizon.Protocol.Types.Game.Character.Alignment;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayCharacterInformations : GameRolePlayHumanoidInformations
    {
        public override short Protocol => 36;

        public ActorAlignmentInformations AlignmentInfos { get; set; }

        public GameRolePlayCharacterInformations()
        {
        }

        public GameRolePlayCharacterInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, string name, HumanInformations humanoidInfo, int accountId, ActorAlignmentInformations alignmentInfos) : base(contextualId, look, disposition, name, humanoidInfo, accountId)
        {
            AlignmentInfos = alignmentInfos;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            AlignmentInfos.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AlignmentInfos = new ActorAlignmentInformations();
            AlignmentInfos.Deserialize(reader);
        }
    }
}