using Horizon.Protocol.Types.Game.Context.Roleplay.Quest;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayNpcWithQuestInformations : GameRolePlayNpcInformations
    {
        public override short Protocol => 383;

        public GameRolePlayNpcQuestFlag QuestFlag { get; set; }

        public GameRolePlayNpcWithQuestInformations()
        {
        }

        public GameRolePlayNpcWithQuestInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, short npcId, bool sex, short specialArtworkId, GameRolePlayNpcQuestFlag questFlag) : base(contextualId, look, disposition, npcId, sex, specialArtworkId)
        {
            QuestFlag = questFlag;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            QuestFlag.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            QuestFlag = new GameRolePlayNpcQuestFlag();
            QuestFlag.Deserialize(reader);
        }
    }
}