namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionEmote : HumanOption
    {
        public override short Protocol => 407;

        public byte EmoteId { get; set; }

        public double EmoteStartTime { get; set; }

        public HumanOptionEmote()
        {
        }

        public HumanOptionEmote(byte emoteId, double emoteStartTime)
        {
            EmoteId = emoteId;
            EmoteStartTime = emoteStartTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(EmoteId);
            writer.WriteDouble(EmoteStartTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            EmoteId = reader.ReadByte();
            EmoteStartTime = reader.ReadDouble();
        }
    }
}