using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayNpcInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 156;

        public short NpcId { get; set; }

        public bool Sex { get; set; }

        public short SpecialArtworkId { get; set; }

        public GameRolePlayNpcInformations()
        {
        }

        public GameRolePlayNpcInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, short npcId, bool sex, short specialArtworkId) : base(contextualId, look, disposition)
        {
            NpcId = npcId;
            Sex = sex;
            SpecialArtworkId = specialArtworkId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(NpcId);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(SpecialArtworkId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NpcId = reader.ReadVarShort();
            Sex = reader.ReadBoolean();
            SpecialArtworkId = reader.ReadVarShort();
        }
    }
}