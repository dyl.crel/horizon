namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class ObjectItemInRolePlay : NetworkType
    {
        public override short Protocol => 198;

        public short CellId { get; set; }

        public short ObjectGID { get; set; }

        public ObjectItemInRolePlay()
        {
        }

        public ObjectItemInRolePlay(short cellId, short objectGID)
        {
            CellId = cellId;
            ObjectGID = objectGID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteVarShort(ObjectGID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
            ObjectGID = reader.ReadVarShort();
        }
    }
}