using Horizon.Protocol.Types.Game.Context.Roleplay.Treasurehunt;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayPortalInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 467;

        public PortalInformation Portal { get; set; }

        public GameRolePlayPortalInformations()
        {
        }

        public GameRolePlayPortalInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, PortalInformation portal) : base(contextualId, look, disposition)
        {
            Portal = portal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Portal.Protocol);
            Portal.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Portal = ProtocolTypesManager.Instance<PortalInformation>(reader.ReadShort());
            Portal.Deserialize(reader);
        }
    }
}