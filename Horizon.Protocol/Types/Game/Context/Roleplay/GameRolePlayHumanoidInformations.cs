using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayHumanoidInformations : GameRolePlayNamedActorInformations
    {
        public override short Protocol => 159;

        public int AccountId { get; set; }

        public HumanInformations HumanoidInfo { get; set; }

        public GameRolePlayHumanoidInformations()
        {
        }

        public GameRolePlayHumanoidInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, string name, HumanInformations humanoidInfo, int accountId) : base(contextualId, look, disposition, name)
        {
            AccountId = accountId;
            HumanoidInfo = humanoidInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(HumanoidInfo.Protocol);
            HumanoidInfo.Serialize(writer);
            writer.WriteInt(AccountId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            HumanoidInfo = ProtocolTypesManager.Instance<HumanInformations>(reader.ReadShort());
            HumanoidInfo.Deserialize(reader);
            AccountId = reader.ReadInt();
        }
    }
}