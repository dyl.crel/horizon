namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class AlternativeMonstersInGroupLightInformations : NetworkType
    {
        public override short Protocol => 394;

        public int PlayerCount { get; set; }

        public MonsterInGroupLightInformations[] Monsters { get; set; }

        public AlternativeMonstersInGroupLightInformations()
        {
        }

        public AlternativeMonstersInGroupLightInformations(int playerCount, MonsterInGroupLightInformations[] monsters)
        {
            PlayerCount = playerCount;
            Monsters = monsters;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(PlayerCount);
            writer.WriteShort(Monsters.Length);
            for (var i = 0; i < Monsters.Length; i++)
            {
                Monsters[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerCount = reader.ReadInt();
            Monsters = new MonsterInGroupLightInformations[reader.ReadShort()];
            for (var i = 0; i < Monsters.Length; i++)
            {
                Monsters[i] = new MonsterInGroupLightInformations();
                Monsters[i].Deserialize(reader);
            }
        }
    }
}