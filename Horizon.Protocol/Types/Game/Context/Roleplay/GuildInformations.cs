using Horizon.Protocol.Types.Game.Guild;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GuildInformations : BasicGuildInformations
    {
        public override short Protocol => 127;

        public GuildEmblem GuildEmblem { get; set; }

        public GuildInformations()
        {
        }

        public GuildInformations(int guildId, string guildName, byte guildLevel, GuildEmblem guildEmblem) : base(guildId, guildName, guildLevel)
        {
            GuildEmblem = guildEmblem;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            GuildEmblem.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            GuildEmblem = new GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
    }
}