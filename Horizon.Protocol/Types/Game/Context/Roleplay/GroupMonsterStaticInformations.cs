namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GroupMonsterStaticInformations : NetworkType
    {
        public override short Protocol => 140;

        public MonsterInGroupLightInformations MainCreatureLightInfos { get; set; }

        public MonsterInGroupInformations[] Underlings { get; set; }

        public GroupMonsterStaticInformations()
        {
        }

        public GroupMonsterStaticInformations(MonsterInGroupLightInformations mainCreatureLightInfos, MonsterInGroupInformations[] underlings)
        {
            MainCreatureLightInfos = mainCreatureLightInfos;
            Underlings = underlings;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            MainCreatureLightInfos.Serialize(writer);
            writer.WriteShort(Underlings.Length);
            for (var i = 0; i < Underlings.Length; i++)
            {
                Underlings[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MainCreatureLightInfos = new MonsterInGroupLightInformations();
            MainCreatureLightInfos.Deserialize(reader);
            Underlings = new MonsterInGroupInformations[reader.ReadShort()];
            for (var i = 0; i < Underlings.Length; i++)
            {
                Underlings[i] = new MonsterInGroupInformations();
                Underlings[i].Deserialize(reader);
            }
        }
    }
}