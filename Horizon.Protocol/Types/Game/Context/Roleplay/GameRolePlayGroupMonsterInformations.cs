using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayGroupMonsterInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 160;

        public bool KeyRingBonus { get; set; }

        public bool HasHardcoreDrop { get; set; }

        public bool HasAVARewardToken { get; set; }

        public GroupMonsterStaticInformations StaticInfos { get; set; }

        public double CreationTime { get; set; }

        public int AgeBonusRate { get; set; }

        public sbyte LootShare { get; set; }

        public sbyte AlignmentSide { get; set; }

        public GameRolePlayGroupMonsterInformations()
        {
        }

        public GameRolePlayGroupMonsterInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, bool keyRingBonus, bool hasHardcoreDrop, bool hasAVARewardToken, GroupMonsterStaticInformations staticInfos, double creationTime, int ageBonusRate, sbyte lootShare, sbyte alignmentSide) : base(contextualId, look, disposition)
        {
            KeyRingBonus = keyRingBonus;
            HasHardcoreDrop = hasHardcoreDrop;
            HasAVARewardToken = hasAVARewardToken;
            StaticInfos = staticInfos;
            CreationTime = creationTime;
            AgeBonusRate = ageBonusRate;
            LootShare = lootShare;
            AlignmentSide = alignmentSide;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, KeyRingBonus);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, HasHardcoreDrop);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, HasAVARewardToken);
            writer.WriteByte(flag0);
            writer.WriteShort(StaticInfos.Protocol);
            StaticInfos.Serialize(writer);
            writer.WriteDouble(CreationTime);
            writer.WriteInt(AgeBonusRate);
            writer.WriteSByte(LootShare);
            writer.WriteSByte(AlignmentSide);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            KeyRingBonus = BooleanByteWrapper.GetFlag(flag0, 0);
            HasHardcoreDrop = BooleanByteWrapper.GetFlag(flag0, 1);
            HasAVARewardToken = BooleanByteWrapper.GetFlag(flag0, 2);
            StaticInfos = ProtocolTypesManager.Instance<GroupMonsterStaticInformations>(reader.ReadShort());
            StaticInfos.Deserialize(reader);
            CreationTime = reader.ReadDouble();
            AgeBonusRate = reader.ReadInt();
            LootShare = reader.ReadSByte();
            AlignmentSide = reader.ReadSByte();
        }
    }
}