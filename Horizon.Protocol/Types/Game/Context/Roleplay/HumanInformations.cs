using Horizon.Protocol.Types.Game.Character.Restriction;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanInformations : NetworkType
    {
        public override short Protocol => 157;

        public ActorRestrictionsInformations Restrictions { get; set; }

        public bool Sex { get; set; }

        public HumanOption[] Options { get; set; }

        public HumanInformations()
        {
        }

        public HumanInformations(ActorRestrictionsInformations restrictions, bool sex, HumanOption[] options)
        {
            Restrictions = restrictions;
            Sex = sex;
            Options = options;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Restrictions.Serialize(writer);
            writer.WriteBoolean(Sex);
            writer.WriteShort(Options.Length);
            for (var i = 0; i < Options.Length; i++)
            {
                writer.WriteShort(Options[i].Protocol);
                Options[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Restrictions = new ActorRestrictionsInformations();
            Restrictions.Deserialize(reader);
            Sex = reader.ReadBoolean();
            Options = new HumanOption[reader.ReadShort()];
            for (var i = 0; i < Options.Length; i++)
            {
                Options[i] = ProtocolTypesManager.Instance<HumanOption>(reader.ReadShort());
                Options[i].Deserialize(reader);
            }
        }
    }
}