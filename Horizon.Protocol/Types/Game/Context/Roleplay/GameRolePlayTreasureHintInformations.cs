using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayTreasureHintInformations : GameRolePlayActorInformations
    {
        public override short Protocol => 471;

        public short NpcId { get; set; }

        public GameRolePlayTreasureHintInformations()
        {
        }

        public GameRolePlayTreasureHintInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, short npcId) : base(contextualId, look, disposition)
        {
            NpcId = npcId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(NpcId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NpcId = reader.ReadVarShort();
        }
    }
}