namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class NamedPartyTeamWithOutcome : NetworkType
    {
        public override short Protocol => 470;

        public NamedPartyTeam Team { get; set; }

        public short Outcome { get; set; }

        public NamedPartyTeamWithOutcome()
        {
        }

        public NamedPartyTeamWithOutcome(NamedPartyTeam team, short outcome)
        {
            Team = team;
            Outcome = outcome;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Team.Serialize(writer);
            writer.WriteVarShort(Outcome);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Team = new NamedPartyTeam();
            Team.Deserialize(reader);
            Outcome = reader.ReadVarShort();
        }
    }
}