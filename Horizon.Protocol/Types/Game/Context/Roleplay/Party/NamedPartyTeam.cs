namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class NamedPartyTeam : NetworkType
    {
        public override short Protocol => 469;

        public byte TeamId { get; set; }

        public string PartyName { get; set; }

        public NamedPartyTeam()
        {
        }

        public NamedPartyTeam(byte teamId, string partyName)
        {
            TeamId = teamId;
            PartyName = partyName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(TeamId);
            writer.WriteUTF(PartyName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TeamId = reader.ReadByte();
            PartyName = reader.ReadUTF();
        }
    }
}