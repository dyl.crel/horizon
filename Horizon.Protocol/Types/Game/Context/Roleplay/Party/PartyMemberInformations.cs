using Horizon.Protocol.Types.Game.Character.Choice;
using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party.Entity;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class PartyMemberInformations : CharacterBaseInformations
    {
        public override short Protocol => 90;

        public int LifePoints { get; set; }

        public int MaxLifePoints { get; set; }

        public short Prospecting { get; set; }

        public byte RegenRate { get; set; }

        public short Initiative { get; set; }

        public byte AlignmentSide { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public PartyEntityBaseInformation[] Entities { get; set; }

        public PlayerStatus Status { get; set; }

        public PartyMemberInformations()
        {
        }

        public PartyMemberInformations(long id, string name, short level, EntityLook entityLook, byte breed, bool sex, int lifePoints, int maxLifePoints, short prospecting, byte regenRate, short initiative, byte alignmentSide, short worldX, short worldY, double mapId, short subAreaId, PartyEntityBaseInformation[] entities, PlayerStatus status) : base(id, name, level, entityLook, breed, sex)
        {
            LifePoints = lifePoints;
            MaxLifePoints = maxLifePoints;
            Prospecting = prospecting;
            RegenRate = regenRate;
            Initiative = initiative;
            AlignmentSide = alignmentSide;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            Entities = entities;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(Prospecting);
            writer.WriteByte(RegenRate);
            writer.WriteVarShort(Initiative);
            writer.WriteByte(AlignmentSide);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteShort(Entities.Length);
            for (var i = 0; i < Entities.Length; i++)
            {
                writer.WriteShort(Entities[i].Protocol);
                Entities[i].Serialize(writer);
            }
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LifePoints = reader.ReadVarInt();
            MaxLifePoints = reader.ReadVarInt();
            Prospecting = reader.ReadVarShort();
            RegenRate = reader.ReadByte();
            Initiative = reader.ReadVarShort();
            AlignmentSide = reader.ReadByte();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            Entities = new PartyEntityBaseInformation[reader.ReadShort()];
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i] = ProtocolTypesManager.Instance<PartyEntityBaseInformation>(reader.ReadShort());
                Entities[i].Deserialize(reader);
            }
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}