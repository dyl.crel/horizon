using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party.Entity;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class PartyGuestInformations : NetworkType
    {
        public override short Protocol => 374;

        public long GuestId { get; set; }

        public long HostId { get; set; }

        public string Name { get; set; }

        public EntityLook GuestLook { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public PartyEntityBaseInformation[] Entities { get; set; }

        public PlayerStatus Status { get; set; }

        public PartyGuestInformations()
        {
        }

        public PartyGuestInformations(long guestId, long hostId, string name, EntityLook guestLook, byte breed, bool sex, PartyEntityBaseInformation[] entities, PlayerStatus status)
        {
            GuestId = guestId;
            HostId = hostId;
            Name = name;
            GuestLook = guestLook;
            Breed = breed;
            Sex = sex;
            Entities = entities;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(GuestId);
            writer.WriteVarLong(HostId);
            writer.WriteUTF(Name);
            GuestLook.Serialize(writer);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteShort(Entities.Length);
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i].Serialize(writer);
            }
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuestId = reader.ReadVarLong();
            HostId = reader.ReadVarLong();
            Name = reader.ReadUTF();
            GuestLook = new EntityLook();
            GuestLook.Deserialize(reader);
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            Entities = new PartyEntityBaseInformation[reader.ReadShort()];
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i] = new PartyEntityBaseInformation();
                Entities[i].Deserialize(reader);
            }
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}