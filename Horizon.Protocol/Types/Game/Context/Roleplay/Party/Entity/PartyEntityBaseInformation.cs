using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party.Entity
{
    public class PartyEntityBaseInformation : NetworkType
    {
        public override short Protocol => 552;

        public byte IndexId { get; set; }

        public byte EntityModelId { get; set; }

        public EntityLook EntityLook { get; set; }

        public PartyEntityBaseInformation()
        {
        }

        public PartyEntityBaseInformation(byte indexId, byte entityModelId, EntityLook entityLook)
        {
            IndexId = indexId;
            EntityModelId = entityModelId;
            EntityLook = entityLook;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(IndexId);
            writer.WriteByte(EntityModelId);
            EntityLook.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            IndexId = reader.ReadByte();
            EntityModelId = reader.ReadByte();
            EntityLook = new EntityLook();
            EntityLook.Deserialize(reader);
        }
    }
}