using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party.Entity
{
    public class PartyEntityMemberInformation : PartyEntityBaseInformation
    {
        public override short Protocol => 550;

        public short Initiative { get; set; }

        public int LifePoints { get; set; }

        public int MaxLifePoints { get; set; }

        public short Prospecting { get; set; }

        public byte RegenRate { get; set; }

        public PartyEntityMemberInformation()
        {
        }

        public PartyEntityMemberInformation(byte indexId, byte entityModelId, EntityLook entityLook, short initiative, int lifePoints, int maxLifePoints, short prospecting, byte regenRate) : base(indexId, entityModelId, entityLook)
        {
            Initiative = initiative;
            LifePoints = lifePoints;
            MaxLifePoints = maxLifePoints;
            Prospecting = prospecting;
            RegenRate = regenRate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Initiative);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(Prospecting);
            writer.WriteByte(RegenRate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Initiative = reader.ReadVarShort();
            LifePoints = reader.ReadVarInt();
            MaxLifePoints = reader.ReadVarInt();
            Prospecting = reader.ReadVarShort();
            RegenRate = reader.ReadByte();
        }
    }
}