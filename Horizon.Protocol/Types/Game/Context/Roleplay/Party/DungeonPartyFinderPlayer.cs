namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class DungeonPartyFinderPlayer : NetworkType
    {
        public override short Protocol => 373;

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public short Level { get; set; }

        public DungeonPartyFinderPlayer()
        {
        }

        public DungeonPartyFinderPlayer(long playerId, string playerName, byte breed, bool sex, short level)
        {
            PlayerId = playerId;
            PlayerName = playerName;
            Breed = breed;
            Sex = sex;
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            Level = reader.ReadVarShort();
        }
    }
}