namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class PartyMemberGeoPosition : NetworkType
    {
        public override short Protocol => 378;

        public int MemberId { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public PartyMemberGeoPosition()
        {
        }

        public PartyMemberGeoPosition(int memberId, short worldX, short worldY, double mapId, short subAreaId)
        {
            MemberId = memberId;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(MemberId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MemberId = reader.ReadInt();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
        }
    }
}