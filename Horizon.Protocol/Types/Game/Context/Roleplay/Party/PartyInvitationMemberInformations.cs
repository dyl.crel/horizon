using Horizon.Protocol.Types.Game.Character.Choice;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party.Entity;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class PartyInvitationMemberInformations : CharacterBaseInformations
    {
        public override short Protocol => 376;

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public PartyEntityBaseInformation[] Entities { get; set; }

        public PartyInvitationMemberInformations()
        {
        }

        public PartyInvitationMemberInformations(long id, string name, short level, EntityLook entityLook, byte breed, bool sex, short worldX, short worldY, double mapId, short subAreaId, PartyEntityBaseInformation[] entities) : base(id, name, level, entityLook, breed, sex)
        {
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            Entities = entities;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteShort(Entities.Length);
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            Entities = new PartyEntityBaseInformation[reader.ReadShort()];
            for (var i = 0; i < Entities.Length; i++)
            {
                Entities[i] = new PartyEntityBaseInformation();
                Entities[i].Deserialize(reader);
            }
        }
    }
}