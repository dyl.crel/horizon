using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Context.Roleplay.Party.Entity;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay.Party
{
    public class PartyMemberArenaInformations : PartyMemberInformations
    {
        public override short Protocol => 391;

        public short Rank { get; set; }

        public PartyMemberArenaInformations()
        {
        }

        public PartyMemberArenaInformations(long id, string name, short level, EntityLook entityLook, byte breed, bool sex, int lifePoints, int maxLifePoints, short prospecting, byte regenRate, short initiative, byte alignmentSide, short worldX, short worldY, double mapId, short subAreaId, PartyEntityBaseInformation[] entities, PlayerStatus status, short rank) : base(id, name, level, entityLook, breed, sex, lifePoints, maxLifePoints, prospecting, regenRate, initiative, alignmentSide, worldX, worldY, mapId, subAreaId, entities, status)
        {
            Rank = rank;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Rank);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Rank = reader.ReadVarShort();
        }
    }
}