namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GroupMonsterStaticInformationsWithAlternatives : GroupMonsterStaticInformations
    {
        public override short Protocol => 396;

        public AlternativeMonstersInGroupLightInformations[] Alternatives { get; set; }

        public GroupMonsterStaticInformationsWithAlternatives()
        {
        }

        public GroupMonsterStaticInformationsWithAlternatives(MonsterInGroupLightInformations mainCreatureLightInfos, MonsterInGroupInformations[] underlings, AlternativeMonstersInGroupLightInformations[] alternatives) : base(mainCreatureLightInfos, underlings)
        {
            Alternatives = alternatives;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Alternatives.Length);
            for (var i = 0; i < Alternatives.Length; i++)
            {
                Alternatives[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Alternatives = new AlternativeMonstersInGroupLightInformations[reader.ReadShort()];
            for (var i = 0; i < Alternatives.Length; i++)
            {
                Alternatives[i] = new AlternativeMonstersInGroupLightInformations();
                Alternatives[i].Deserialize(reader);
            }
        }
    }
}