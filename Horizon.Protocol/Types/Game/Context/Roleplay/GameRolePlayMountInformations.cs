using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayMountInformations : GameRolePlayNamedActorInformations
    {
        public override short Protocol => 180;

        public string OwnerName { get; set; }

        public byte Level { get; set; }

        public GameRolePlayMountInformations()
        {
        }

        public GameRolePlayMountInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, string name, string ownerName, byte level) : base(contextualId, look, disposition, name)
        {
            OwnerName = ownerName;
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(OwnerName);
            writer.WriteByte(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            OwnerName = reader.ReadUTF();
            Level = reader.ReadByte();
        }
    }
}