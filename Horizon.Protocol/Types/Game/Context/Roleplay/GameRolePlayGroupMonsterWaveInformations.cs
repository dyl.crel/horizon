using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayGroupMonsterWaveInformations : GameRolePlayGroupMonsterInformations
    {
        public override short Protocol => 464;

        public byte NbWaves { get; set; }

        public GroupMonsterStaticInformations[] Alternatives { get; set; }

        public GameRolePlayGroupMonsterWaveInformations()
        {
        }

        public GameRolePlayGroupMonsterWaveInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, bool keyRingBonus, bool hasHardcoreDrop, bool hasAVARewardToken, GroupMonsterStaticInformations staticInfos, double creationTime, int ageBonusRate, sbyte lootShare, sbyte alignmentSide, byte nbWaves, GroupMonsterStaticInformations[] alternatives) : base(contextualId, look, disposition, keyRingBonus, hasHardcoreDrop, hasAVARewardToken, staticInfos, creationTime, ageBonusRate, lootShare, alignmentSide)
        {
            NbWaves = nbWaves;
            Alternatives = alternatives;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(NbWaves);
            writer.WriteShort(Alternatives.Length);
            for (var i = 0; i < Alternatives.Length; i++)
            {
                writer.WriteShort(Alternatives[i].Protocol);
                Alternatives[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NbWaves = reader.ReadByte();
            Alternatives = new GroupMonsterStaticInformations[reader.ReadShort()];
            for (var i = 0; i < Alternatives.Length; i++)
            {
                Alternatives[i] = ProtocolTypesManager.Instance<GroupMonsterStaticInformations>(reader.ReadShort());
                Alternatives[i].Deserialize(reader);
            }
        }
    }
}