namespace Horizon.Protocol.Types.Game.Context.Roleplay.Breach
{
    public class BreachBranch : NetworkType
    {
        public override short Protocol => 558;

        public byte Room { get; set; }

        public int Element { get; set; }

        public MonsterInGroupLightInformations[] Bosses { get; set; }

        public BreachBranch()
        {
        }

        public BreachBranch(byte room, int element, MonsterInGroupLightInformations[] bosses)
        {
            Room = room;
            Element = element;
            Bosses = bosses;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Room);
            writer.WriteInt(Element);
            writer.WriteShort(Bosses.Length);
            for (var i = 0; i < Bosses.Length; i++)
            {
                Bosses[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Room = reader.ReadByte();
            Element = reader.ReadInt();
            Bosses = new MonsterInGroupLightInformations[reader.ReadShort()];
            for (var i = 0; i < Bosses.Length; i++)
            {
                Bosses[i] = new MonsterInGroupLightInformations();
                Bosses[i].Deserialize(reader);
            }
        }
    }
}