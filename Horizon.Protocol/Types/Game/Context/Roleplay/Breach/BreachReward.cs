namespace Horizon.Protocol.Types.Game.Context.Roleplay.Breach
{
    public class BreachReward : NetworkType
    {
        public override short Protocol => 559;

        public int Id { get; set; }

        public byte[] BuyLocks { get; set; }

        public string BuyCriterion { get; set; }

        public bool Bought { get; set; }

        public int Price { get; set; }

        public BreachReward()
        {
        }

        public BreachReward(int id, byte[] buyLocks, string buyCriterion, bool bought, int price)
        {
            Id = id;
            BuyLocks = buyLocks;
            BuyCriterion = buyCriterion;
            Bought = bought;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Id);
            writer.WriteShort(BuyLocks.Length);
            for (var i = 0; i < BuyLocks.Length; i++)
            {
                writer.WriteByte(BuyLocks[i]);
            }
            writer.WriteUTF(BuyCriterion);
            writer.WriteBoolean(Bought);
            writer.WriteVarInt(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarInt();
            BuyLocks = new byte[reader.ReadShort()];
            for (var i = 0; i < BuyLocks.Length; i++)
            {
                BuyLocks[i] = reader.ReadByte();
            }
            BuyCriterion = reader.ReadUTF();
            Bought = reader.ReadBoolean();
            Price = reader.ReadVarInt();
        }
    }
}