namespace Horizon.Protocol.Types.Game.Context.Roleplay.Breach
{
    public class ExtendedBreachBranch : BreachBranch
    {
        public override short Protocol => 560;

        public MonsterInGroupLightInformations[] Monsters { get; set; }

        public BreachReward[] Rewards { get; set; }

        public int Modifier { get; set; }

        public int Prize { get; set; }

        public ExtendedBreachBranch()
        {
        }

        public ExtendedBreachBranch(byte room, int element, MonsterInGroupLightInformations[] bosses, MonsterInGroupLightInformations[] monsters, BreachReward[] rewards, int modifier, int prize) : base(room, element, bosses)
        {
            Monsters = monsters;
            Rewards = rewards;
            Modifier = modifier;
            Prize = prize;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Monsters.Length);
            for (var i = 0; i < Monsters.Length; i++)
            {
                Monsters[i].Serialize(writer);
            }
            writer.WriteShort(Rewards.Length);
            for (var i = 0; i < Rewards.Length; i++)
            {
                Rewards[i].Serialize(writer);
            }
            writer.WriteVarInt(Modifier);
            writer.WriteVarInt(Prize);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Monsters = new MonsterInGroupLightInformations[reader.ReadShort()];
            for (var i = 0; i < Monsters.Length; i++)
            {
                Monsters[i] = new MonsterInGroupLightInformations();
                Monsters[i].Deserialize(reader);
            }
            Rewards = new BreachReward[reader.ReadShort()];
            for (var i = 0; i < Rewards.Length; i++)
            {
                Rewards[i] = new BreachReward();
                Rewards[i].Deserialize(reader);
            }
            Modifier = reader.ReadVarInt();
            Prize = reader.ReadVarInt();
        }
    }
}