namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class MonsterInGroupLightInformations : NetworkType
    {
        public override short Protocol => 395;

        public int GenericId { get; set; }

        public byte Grade { get; set; }

        public short Level { get; set; }

        public MonsterInGroupLightInformations()
        {
        }

        public MonsterInGroupLightInformations(int genericId, byte grade, short level)
        {
            GenericId = genericId;
            Grade = grade;
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(GenericId);
            writer.WriteByte(Grade);
            writer.WriteShort(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GenericId = reader.ReadInt();
            Grade = reader.ReadByte();
            Level = reader.ReadShort();
        }
    }
}