namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class MonsterBoosts : NetworkType
    {
        public override short Protocol => 497;

        public int Id { get; set; }

        public short XpBoost { get; set; }

        public short DropBoost { get; set; }

        public MonsterBoosts()
        {
        }

        public MonsterBoosts(int id, short xpBoost, short dropBoost)
        {
            Id = id;
            XpBoost = xpBoost;
            DropBoost = dropBoost;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Id);
            writer.WriteVarShort(XpBoost);
            writer.WriteVarShort(DropBoost);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarInt();
            XpBoost = reader.ReadVarShort();
            DropBoost = reader.ReadVarShort();
        }
    }
}