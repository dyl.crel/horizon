using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class GameRolePlayMerchantInformations : GameRolePlayNamedActorInformations
    {
        public override short Protocol => 129;

        public byte SellType { get; set; }

        public HumanOption[] Options { get; set; }

        public GameRolePlayMerchantInformations()
        {
        }

        public GameRolePlayMerchantInformations(double contextualId, EntityLook look, EntityDispositionInformations disposition, string name, byte sellType, HumanOption[] options) : base(contextualId, look, disposition, name)
        {
            SellType = sellType;
            Options = options;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(SellType);
            writer.WriteShort(Options.Length);
            for (var i = 0; i < Options.Length; i++)
            {
                writer.WriteShort(Options[i].Protocol);
                Options[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SellType = reader.ReadByte();
            Options = new HumanOption[reader.ReadShort()];
            for (var i = 0; i < Options.Length; i++)
            {
                Options[i] = ProtocolTypesManager.Instance<HumanOption>(reader.ReadShort());
                Options[i].Deserialize(reader);
            }
        }
    }
}