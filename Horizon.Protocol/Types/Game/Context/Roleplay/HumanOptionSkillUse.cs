namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class HumanOptionSkillUse : HumanOption
    {
        public override short Protocol => 495;

        public int ElementId { get; set; }

        public short SkillId { get; set; }

        public double SkillEndTime { get; set; }

        public HumanOptionSkillUse()
        {
        }

        public HumanOptionSkillUse(int elementId, short skillId, double skillEndTime)
        {
            ElementId = elementId;
            SkillId = skillId;
            SkillEndTime = skillEndTime;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(ElementId);
            writer.WriteVarShort(SkillId);
            writer.WriteDouble(SkillEndTime);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ElementId = reader.ReadVarInt();
            SkillId = reader.ReadVarShort();
            SkillEndTime = reader.ReadDouble();
        }
    }
}