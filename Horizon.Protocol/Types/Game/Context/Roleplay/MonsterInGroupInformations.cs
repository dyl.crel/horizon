using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Context.Roleplay
{
    public class MonsterInGroupInformations : MonsterInGroupLightInformations
    {
        public override short Protocol => 144;

        public EntityLook Look { get; set; }

        public MonsterInGroupInformations()
        {
        }

        public MonsterInGroupInformations(int genericId, byte grade, short level, EntityLook look) : base(genericId, grade, level)
        {
            Look = look;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Look.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Look = new EntityLook();
            Look.Deserialize(reader);
        }
    }
}