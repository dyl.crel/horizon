namespace Horizon.Protocol.Types.Game.Mount
{
    public class UpdateMountBooleanCharacteristic : UpdateMountCharacteristic
    {
        public override short Protocol => 538;

        public bool Value { get; set; }

        public UpdateMountBooleanCharacteristic()
        {
        }

        public UpdateMountBooleanCharacteristic(byte type, bool value) : base(type)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadBoolean();
        }
    }
}