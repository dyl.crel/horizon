namespace Horizon.Protocol.Types.Game.Mount
{
    public class UpdateMountCharacteristic : NetworkType
    {
        public override short Protocol => 536;

        public byte Type { get; set; }

        public UpdateMountCharacteristic()
        {
        }

        public UpdateMountCharacteristic(byte type)
        {
            Type = type;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
        }
    }
}