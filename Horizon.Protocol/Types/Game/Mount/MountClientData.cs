using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Mount
{
    public class MountClientData : NetworkType
    {
        public override short Protocol => 178;

        public bool Sex { get; set; }

        public bool IsRideable { get; set; }

        public bool IsWild { get; set; }

        public bool IsFecondationReady { get; set; }

        public bool UseHarnessColors { get; set; }

        public double Id { get; set; }

        public int Model { get; set; }

        public int[] Ancestor { get; set; }

        public int[] Behaviors { get; set; }

        public string Name { get; set; }

        public int OwnerId { get; set; }

        public long Experience { get; set; }

        public long ExperienceForLevel { get; set; }

        public double ExperienceForNextLevel { get; set; }

        public byte Level { get; set; }

        public int MaxPods { get; set; }

        public int Stamina { get; set; }

        public int StaminaMax { get; set; }

        public int Maturity { get; set; }

        public int MaturityForAdult { get; set; }

        public int Energy { get; set; }

        public int EnergyMax { get; set; }

        public int Serenity { get; set; }

        public int AggressivityMax { get; set; }

        public int SerenityMax { get; set; }

        public int Love { get; set; }

        public int LoveMax { get; set; }

        public int FecondationTime { get; set; }

        public int BoostLimiter { get; set; }

        public double BoostMax { get; set; }

        public int ReproductionCount { get; set; }

        public int ReproductionCountMax { get; set; }

        public short HarnessGID { get; set; }

        public ObjectEffectInteger[] EffectList { get; set; }

        public MountClientData()
        {
        }

        public MountClientData(bool sex, bool isRideable, bool isWild, bool isFecondationReady, bool useHarnessColors, double id, int model, int[] ancestor, int[] behaviors, string name, int ownerId, long experience, long experienceForLevel, double experienceForNextLevel, byte level, int maxPods, int stamina, int staminaMax, int maturity, int maturityForAdult, int energy, int energyMax, int serenity, int aggressivityMax, int serenityMax, int love, int loveMax, int fecondationTime, int boostLimiter, double boostMax, int reproductionCount, int reproductionCountMax, short harnessGID, ObjectEffectInteger[] effectList)
        {
            Sex = sex;
            IsRideable = isRideable;
            IsWild = isWild;
            IsFecondationReady = isFecondationReady;
            UseHarnessColors = useHarnessColors;
            Id = id;
            Model = model;
            Ancestor = ancestor;
            Behaviors = behaviors;
            Name = name;
            OwnerId = ownerId;
            Experience = experience;
            ExperienceForLevel = experienceForLevel;
            ExperienceForNextLevel = experienceForNextLevel;
            Level = level;
            MaxPods = maxPods;
            Stamina = stamina;
            StaminaMax = staminaMax;
            Maturity = maturity;
            MaturityForAdult = maturityForAdult;
            Energy = energy;
            EnergyMax = energyMax;
            Serenity = serenity;
            AggressivityMax = aggressivityMax;
            SerenityMax = serenityMax;
            Love = love;
            LoveMax = loveMax;
            FecondationTime = fecondationTime;
            BoostLimiter = boostLimiter;
            BoostMax = boostMax;
            ReproductionCount = reproductionCount;
            ReproductionCountMax = reproductionCountMax;
            HarnessGID = harnessGID;
            EffectList = effectList;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Sex);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsRideable);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, IsWild);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, IsFecondationReady);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 4, UseHarnessColors);
            writer.WriteByte(flag0);
            writer.WriteDouble(Id);
            writer.WriteVarInt(Model);
            writer.WriteShort(Ancestor.Length);
            for (var i = 0; i < Ancestor.Length; i++)
            {
                writer.WriteInt(Ancestor[i]);
            }
            writer.WriteShort(Behaviors.Length);
            for (var i = 0; i < Behaviors.Length; i++)
            {
                writer.WriteInt(Behaviors[i]);
            }
            writer.WriteUTF(Name);
            writer.WriteInt(OwnerId);
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExperienceForLevel);
            writer.WriteDouble(ExperienceForNextLevel);
            writer.WriteByte(Level);
            writer.WriteVarInt(MaxPods);
            writer.WriteVarInt(Stamina);
            writer.WriteVarInt(StaminaMax);
            writer.WriteVarInt(Maturity);
            writer.WriteVarInt(MaturityForAdult);
            writer.WriteVarInt(Energy);
            writer.WriteVarInt(EnergyMax);
            writer.WriteInt(Serenity);
            writer.WriteInt(AggressivityMax);
            writer.WriteVarInt(SerenityMax);
            writer.WriteVarInt(Love);
            writer.WriteVarInt(LoveMax);
            writer.WriteInt(FecondationTime);
            writer.WriteInt(BoostLimiter);
            writer.WriteDouble(BoostMax);
            writer.WriteInt(ReproductionCount);
            writer.WriteVarInt(ReproductionCountMax);
            writer.WriteVarShort(HarnessGID);
            writer.WriteShort(EffectList.Length);
            for (var i = 0; i < EffectList.Length; i++)
            {
                EffectList[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag0, 0);
            IsRideable = BooleanByteWrapper.GetFlag(flag0, 1);
            IsWild = BooleanByteWrapper.GetFlag(flag0, 2);
            IsFecondationReady = BooleanByteWrapper.GetFlag(flag0, 3);
            UseHarnessColors = BooleanByteWrapper.GetFlag(flag0, 4);
            Id = reader.ReadDouble();
            Model = reader.ReadVarInt();
            Ancestor = new int[reader.ReadShort()];
            for (var i = 0; i < Ancestor.Length; i++)
            {
                Ancestor[i] = reader.ReadInt();
            }
            Behaviors = new int[reader.ReadShort()];
            for (var i = 0; i < Behaviors.Length; i++)
            {
                Behaviors[i] = reader.ReadInt();
            }
            Name = reader.ReadUTF();
            OwnerId = reader.ReadInt();
            Experience = reader.ReadVarLong();
            ExperienceForLevel = reader.ReadVarLong();
            ExperienceForNextLevel = reader.ReadDouble();
            Level = reader.ReadByte();
            MaxPods = reader.ReadVarInt();
            Stamina = reader.ReadVarInt();
            StaminaMax = reader.ReadVarInt();
            Maturity = reader.ReadVarInt();
            MaturityForAdult = reader.ReadVarInt();
            Energy = reader.ReadVarInt();
            EnergyMax = reader.ReadVarInt();
            Serenity = reader.ReadInt();
            AggressivityMax = reader.ReadInt();
            SerenityMax = reader.ReadVarInt();
            Love = reader.ReadVarInt();
            LoveMax = reader.ReadVarInt();
            FecondationTime = reader.ReadInt();
            BoostLimiter = reader.ReadInt();
            BoostMax = reader.ReadDouble();
            ReproductionCount = reader.ReadInt();
            ReproductionCountMax = reader.ReadVarInt();
            HarnessGID = reader.ReadVarShort();
            EffectList = new ObjectEffectInteger[reader.ReadShort()];
            for (var i = 0; i < EffectList.Length; i++)
            {
                EffectList[i] = new ObjectEffectInteger();
                EffectList[i].Deserialize(reader);
            }
        }
    }
}