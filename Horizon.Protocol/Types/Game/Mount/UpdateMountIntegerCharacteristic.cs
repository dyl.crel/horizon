namespace Horizon.Protocol.Types.Game.Mount
{
    public class UpdateMountIntegerCharacteristic : UpdateMountCharacteristic
    {
        public override short Protocol => 537;

        public int Value { get; set; }

        public UpdateMountIntegerCharacteristic()
        {
        }

        public UpdateMountIntegerCharacteristic(byte type, int value) : base(type)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadInt();
        }
    }
}