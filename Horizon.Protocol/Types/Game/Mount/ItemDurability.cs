namespace Horizon.Protocol.Types.Game.Mount
{
    public class ItemDurability : NetworkType
    {
        public override short Protocol => 168;

        public short Durability { get; set; }

        public short DurabilityMax { get; set; }

        public ItemDurability()
        {
        }

        public ItemDurability(short durability, short durabilityMax)
        {
            Durability = durability;
            DurabilityMax = durabilityMax;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Durability);
            writer.WriteShort(DurabilityMax);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Durability = reader.ReadShort();
            DurabilityMax = reader.ReadShort();
        }
    }
}