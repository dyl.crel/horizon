using Horizon.Protocol.Types.Game.Character.Status;

namespace Horizon.Protocol.Types.Game.Friend
{
    public class AcquaintanceOnlineInformation : AcquaintanceInformation
    {
        public override short Protocol => 562;

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public short MoodSmileyId { get; set; }

        public PlayerStatus Status { get; set; }

        public AcquaintanceOnlineInformation()
        {
        }

        public AcquaintanceOnlineInformation(int accountId, string accountName, byte playerState, long playerId, string playerName, short moodSmileyId, PlayerStatus status) : base(accountId, accountName, playerState)
        {
            PlayerId = playerId;
            PlayerName = playerName;
            MoodSmileyId = moodSmileyId;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarShort(MoodSmileyId);
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
            MoodSmileyId = reader.ReadVarShort();
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}