using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Friend
{
    public class FriendSpouseOnlineInformations : FriendSpouseInformations
    {
        public override short Protocol => 93;

        public bool InFight { get; set; }

        public bool FollowSpouse { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public FriendSpouseOnlineInformations()
        {
        }

        public FriendSpouseOnlineInformations(int spouseAccountId, long spouseId, string spouseName, short spouseLevel, byte breed, byte sex, EntityLook spouseEntityLook, GuildInformations guildInfo, byte alignmentSide, bool inFight, bool followSpouse, double mapId, short subAreaId) : base(spouseAccountId, spouseId, spouseName, spouseLevel, breed, sex, spouseEntityLook, guildInfo, alignmentSide)
        {
            InFight = inFight;
            FollowSpouse = followSpouse;
            MapId = mapId;
            SubAreaId = subAreaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, InFight);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, FollowSpouse);
            writer.WriteByte(flag0);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            InFight = BooleanByteWrapper.GetFlag(flag0, 0);
            FollowSpouse = BooleanByteWrapper.GetFlag(flag0, 1);
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
        }
    }
}