namespace Horizon.Protocol.Types.Game.Friend
{
    public class AcquaintanceInformation : AbstractContactInformations
    {
        public override short Protocol => 561;

        public byte PlayerState { get; set; }

        public AcquaintanceInformation()
        {
        }

        public AcquaintanceInformation(int accountId, string accountName, byte playerState) : base(accountId, accountName)
        {
            PlayerState = playerState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PlayerState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerState = reader.ReadByte();
        }
    }
}