using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Friend
{
    public class FriendSpouseInformations : NetworkType
    {
        public override short Protocol => 77;

        public int SpouseAccountId { get; set; }

        public long SpouseId { get; set; }

        public string SpouseName { get; set; }

        public short SpouseLevel { get; set; }

        public byte Breed { get; set; }

        public byte Sex { get; set; }

        public EntityLook SpouseEntityLook { get; set; }

        public GuildInformations GuildInfo { get; set; }

        public byte AlignmentSide { get; set; }

        public FriendSpouseInformations()
        {
        }

        public FriendSpouseInformations(int spouseAccountId, long spouseId, string spouseName, short spouseLevel, byte breed, byte sex, EntityLook spouseEntityLook, GuildInformations guildInfo, byte alignmentSide)
        {
            SpouseAccountId = spouseAccountId;
            SpouseId = spouseId;
            SpouseName = spouseName;
            SpouseLevel = spouseLevel;
            Breed = breed;
            Sex = sex;
            SpouseEntityLook = spouseEntityLook;
            GuildInfo = guildInfo;
            AlignmentSide = alignmentSide;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(SpouseAccountId);
            writer.WriteVarLong(SpouseId);
            writer.WriteUTF(SpouseName);
            writer.WriteVarShort(SpouseLevel);
            writer.WriteByte(Breed);
            writer.WriteByte(Sex);
            SpouseEntityLook.Serialize(writer);
            GuildInfo.Serialize(writer);
            writer.WriteByte(AlignmentSide);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpouseAccountId = reader.ReadInt();
            SpouseId = reader.ReadVarLong();
            SpouseName = reader.ReadUTF();
            SpouseLevel = reader.ReadVarShort();
            Breed = reader.ReadByte();
            Sex = reader.ReadByte();
            SpouseEntityLook = new EntityLook();
            SpouseEntityLook.Deserialize(reader);
            GuildInfo = new GuildInformations();
            GuildInfo.Deserialize(reader);
            AlignmentSide = reader.ReadByte();
        }
    }
}