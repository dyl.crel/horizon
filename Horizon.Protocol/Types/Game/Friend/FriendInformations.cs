namespace Horizon.Protocol.Types.Game.Friend
{
    public class FriendInformations : AbstractContactInformations
    {
        public override short Protocol => 78;

        public byte PlayerState { get; set; }

        public short LastConnection { get; set; }

        public int AchievementPoints { get; set; }

        public short LeagueId { get; set; }

        public int LadderPosition { get; set; }

        public FriendInformations()
        {
        }

        public FriendInformations(int accountId, string accountName, byte playerState, short lastConnection, int achievementPoints, short leagueId, int ladderPosition) : base(accountId, accountName)
        {
            PlayerState = playerState;
            LastConnection = lastConnection;
            AchievementPoints = achievementPoints;
            LeagueId = leagueId;
            LadderPosition = ladderPosition;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PlayerState);
            writer.WriteVarShort(LastConnection);
            writer.WriteInt(AchievementPoints);
            writer.WriteVarShort(LeagueId);
            writer.WriteInt(LadderPosition);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerState = reader.ReadByte();
            LastConnection = reader.ReadVarShort();
            AchievementPoints = reader.ReadInt();
            LeagueId = reader.ReadVarShort();
            LadderPosition = reader.ReadInt();
        }
    }
}