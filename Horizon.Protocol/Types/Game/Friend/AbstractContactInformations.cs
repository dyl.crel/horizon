namespace Horizon.Protocol.Types.Game.Friend
{
    public class AbstractContactInformations : NetworkType
    {
        public override short Protocol => 380;

        public int AccountId { get; set; }

        public string AccountName { get; set; }

        public AbstractContactInformations()
        {
        }

        public AbstractContactInformations(int accountId, string accountName)
        {
            AccountId = accountId;
            AccountName = accountName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteUTF(AccountName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AccountId = reader.ReadInt();
            AccountName = reader.ReadUTF();
        }
    }
}