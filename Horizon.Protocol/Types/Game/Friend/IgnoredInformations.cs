namespace Horizon.Protocol.Types.Game.Friend
{
    public class IgnoredInformations : AbstractContactInformations
    {
        public override short Protocol => 106;

        public IgnoredInformations()
        {
        }

        public IgnoredInformations(int accountId, string accountName) : base(accountId, accountName)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}