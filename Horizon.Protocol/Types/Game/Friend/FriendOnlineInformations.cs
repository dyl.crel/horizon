using Horizon.Protocol.Types.Game.Character.Status;
using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Friend
{
    public class FriendOnlineInformations : FriendInformations
    {
        public override short Protocol => 92;

        public bool Sex { get; set; }

        public bool HavenBagShared { get; set; }

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public short Level { get; set; }

        public byte AlignmentSide { get; set; }

        public byte Breed { get; set; }

        public GuildInformations GuildInfo { get; set; }

        public short MoodSmileyId { get; set; }

        public PlayerStatus Status { get; set; }

        public FriendOnlineInformations()
        {
        }

        public FriendOnlineInformations(int accountId, string accountName, byte playerState, short lastConnection, int achievementPoints, short leagueId, int ladderPosition, bool sex, bool havenBagShared, long playerId, string playerName, short level, byte alignmentSide, byte breed, GuildInformations guildInfo, short moodSmileyId, PlayerStatus status) : base(accountId, accountName, playerState, lastConnection, achievementPoints, leagueId, ladderPosition)
        {
            Sex = sex;
            HavenBagShared = havenBagShared;
            PlayerId = playerId;
            PlayerName = playerName;
            Level = level;
            AlignmentSide = alignmentSide;
            Breed = breed;
            GuildInfo = guildInfo;
            MoodSmileyId = moodSmileyId;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Sex);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, HavenBagShared);
            writer.WriteByte(flag0);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarShort(Level);
            writer.WriteByte(AlignmentSide);
            writer.WriteByte(Breed);
            GuildInfo.Serialize(writer);
            writer.WriteVarShort(MoodSmileyId);
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag0, 0);
            HavenBagShared = BooleanByteWrapper.GetFlag(flag0, 1);
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
            Level = reader.ReadVarShort();
            AlignmentSide = reader.ReadByte();
            Breed = reader.ReadByte();
            GuildInfo = new GuildInformations();
            GuildInfo.Deserialize(reader);
            MoodSmileyId = reader.ReadVarShort();
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}