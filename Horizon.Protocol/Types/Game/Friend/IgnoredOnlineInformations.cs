namespace Horizon.Protocol.Types.Game.Friend
{
    public class IgnoredOnlineInformations : IgnoredInformations
    {
        public override short Protocol => 105;

        public long PlayerId { get; set; }

        public string PlayerName { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public IgnoredOnlineInformations()
        {
        }

        public IgnoredOnlineInformations(int accountId, string accountName, long playerId, string playerName, byte breed, bool sex) : base(accountId, accountName)
        {
            PlayerId = playerId;
            PlayerName = playerName;
            Breed = breed;
            Sex = sex;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarLong();
            PlayerName = reader.ReadUTF();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
        }
    }
}