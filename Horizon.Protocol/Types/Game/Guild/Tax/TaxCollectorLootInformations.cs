namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorLootInformations : TaxCollectorComplementaryInformations
    {
        public override short Protocol => 372;

        public long Kamas { get; set; }

        public long Experience { get; set; }

        public int Pods { get; set; }

        public long ItemsValue { get; set; }

        public TaxCollectorLootInformations()
        {
        }

        public TaxCollectorLootInformations(long kamas, long experience, int pods, long itemsValue)
        {
            Kamas = kamas;
            Experience = experience;
            Pods = pods;
            ItemsValue = itemsValue;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Kamas);
            writer.WriteVarLong(Experience);
            writer.WriteVarInt(Pods);
            writer.WriteVarLong(ItemsValue);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Kamas = reader.ReadVarLong();
            Experience = reader.ReadVarLong();
            Pods = reader.ReadVarInt();
            ItemsValue = reader.ReadVarLong();
        }
    }
}