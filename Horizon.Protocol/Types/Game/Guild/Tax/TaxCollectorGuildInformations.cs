using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorGuildInformations : TaxCollectorComplementaryInformations
    {
        public override short Protocol => 446;

        public BasicGuildInformations Guild { get; set; }

        public TaxCollectorGuildInformations()
        {
        }

        public TaxCollectorGuildInformations(BasicGuildInformations guild)
        {
            Guild = guild;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Guild.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Guild = new BasicGuildInformations();
            Guild.Deserialize(reader);
        }
    }
}