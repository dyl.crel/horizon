namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorComplementaryInformations : NetworkType
    {
        public override short Protocol => 448;

        public TaxCollectorComplementaryInformations()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}