using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorInformations : NetworkType
    {
        public override short Protocol => 167;

        public double UniqueId { get; set; }

        public short FirtNameId { get; set; }

        public short LastNameId { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public short SubAreaId { get; set; }

        public byte State { get; set; }

        public EntityLook Look { get; set; }

        public TaxCollectorComplementaryInformations[] Complements { get; set; }

        public TaxCollectorInformations()
        {
        }

        public TaxCollectorInformations(double uniqueId, short firtNameId, short lastNameId, short worldX, short worldY, short subAreaId, byte state, EntityLook look, TaxCollectorComplementaryInformations[] complements)
        {
            UniqueId = uniqueId;
            FirtNameId = firtNameId;
            LastNameId = lastNameId;
            WorldX = worldX;
            WorldY = worldY;
            SubAreaId = subAreaId;
            State = state;
            Look = look;
            Complements = complements;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(UniqueId);
            writer.WriteVarShort(FirtNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteVarShort(SubAreaId);
            writer.WriteByte(State);
            Look.Serialize(writer);
            writer.WriteShort(Complements.Length);
            for (var i = 0; i < Complements.Length; i++)
            {
                writer.WriteShort(Complements[i].Protocol);
                Complements[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            UniqueId = reader.ReadDouble();
            FirtNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            SubAreaId = reader.ReadVarShort();
            State = reader.ReadByte();
            Look = new EntityLook();
            Look.Deserialize(reader);
            Complements = new TaxCollectorComplementaryInformations[reader.ReadShort()];
            for (var i = 0; i < Complements.Length; i++)
            {
                Complements[i] = ProtocolTypesManager.Instance<TaxCollectorComplementaryInformations>(reader.ReadShort());
                Complements[i].Deserialize(reader);
            }
        }
    }
}