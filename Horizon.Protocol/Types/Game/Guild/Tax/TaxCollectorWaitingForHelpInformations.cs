using Horizon.Protocol.Types.Game.Fight;

namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorWaitingForHelpInformations : TaxCollectorComplementaryInformations
    {
        public override short Protocol => 447;

        public ProtectedEntityWaitingForHelpInfo WaitingForHelpInfo { get; set; }

        public TaxCollectorWaitingForHelpInformations()
        {
        }

        public TaxCollectorWaitingForHelpInformations(ProtectedEntityWaitingForHelpInfo waitingForHelpInfo)
        {
            WaitingForHelpInfo = waitingForHelpInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            WaitingForHelpInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WaitingForHelpInfo = new ProtectedEntityWaitingForHelpInfo();
            WaitingForHelpInfo.Deserialize(reader);
        }
    }
}