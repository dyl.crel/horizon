using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorFightersInformation : NetworkType
    {
        public override short Protocol => 169;

        public double CollectorId { get; set; }

        public CharacterMinimalPlusLookInformations[] AllyCharactersInformations { get; set; }

        public CharacterMinimalPlusLookInformations[] EnemyCharactersInformations { get; set; }

        public TaxCollectorFightersInformation()
        {
        }

        public TaxCollectorFightersInformation(double collectorId, CharacterMinimalPlusLookInformations[] allyCharactersInformations, CharacterMinimalPlusLookInformations[] enemyCharactersInformations)
        {
            CollectorId = collectorId;
            AllyCharactersInformations = allyCharactersInformations;
            EnemyCharactersInformations = enemyCharactersInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(CollectorId);
            writer.WriteShort(AllyCharactersInformations.Length);
            for (var i = 0; i < AllyCharactersInformations.Length; i++)
            {
                writer.WriteShort(AllyCharactersInformations[i].Protocol);
                AllyCharactersInformations[i].Serialize(writer);
            }
            writer.WriteShort(EnemyCharactersInformations.Length);
            for (var i = 0; i < EnemyCharactersInformations.Length; i++)
            {
                writer.WriteShort(EnemyCharactersInformations[i].Protocol);
                EnemyCharactersInformations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CollectorId = reader.ReadDouble();
            AllyCharactersInformations = new CharacterMinimalPlusLookInformations[reader.ReadShort()];
            for (var i = 0; i < AllyCharactersInformations.Length; i++)
            {
                AllyCharactersInformations[i] = ProtocolTypesManager.Instance<CharacterMinimalPlusLookInformations>(reader.ReadShort());
                AllyCharactersInformations[i].Deserialize(reader);
            }
            EnemyCharactersInformations = new CharacterMinimalPlusLookInformations[reader.ReadShort()];
            for (var i = 0; i < EnemyCharactersInformations.Length; i++)
            {
                EnemyCharactersInformations[i] = ProtocolTypesManager.Instance<CharacterMinimalPlusLookInformations>(reader.ReadShort());
                EnemyCharactersInformations[i].Deserialize(reader);
            }
        }
    }
}