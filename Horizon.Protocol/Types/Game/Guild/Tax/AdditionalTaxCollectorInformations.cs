namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class AdditionalTaxCollectorInformations : NetworkType
    {
        public override short Protocol => 165;

        public string CollectorCallerName { get; set; }

        public int Date { get; set; }

        public AdditionalTaxCollectorInformations()
        {
        }

        public AdditionalTaxCollectorInformations(string collectorCallerName, int date)
        {
            CollectorCallerName = collectorCallerName;
            Date = date;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(CollectorCallerName);
            writer.WriteInt(Date);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CollectorCallerName = reader.ReadUTF();
            Date = reader.ReadInt();
        }
    }
}