namespace Horizon.Protocol.Types.Game.Guild.Tax
{
    public class TaxCollectorBasicInformations : NetworkType
    {
        public override short Protocol => 96;

        public short FirstNameId { get; set; }

        public short LastNameId { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public TaxCollectorBasicInformations()
        {
        }

        public TaxCollectorBasicInformations(short firstNameId, short lastNameId, short worldX, short worldY, double mapId, short subAreaId)
        {
            FirstNameId = firstNameId;
            LastNameId = lastNameId;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FirstNameId = reader.ReadVarShort();
            LastNameId = reader.ReadVarShort();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
        }
    }
}