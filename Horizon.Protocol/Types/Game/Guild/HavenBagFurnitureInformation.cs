namespace Horizon.Protocol.Types.Game.Guild
{
    public class HavenBagFurnitureInformation : NetworkType
    {
        public override short Protocol => 498;

        public short CellId { get; set; }

        public int FunitureId { get; set; }

        public byte Orientation { get; set; }

        public HavenBagFurnitureInformation()
        {
        }

        public HavenBagFurnitureInformation(short cellId, int funitureId, byte orientation)
        {
            CellId = cellId;
            FunitureId = funitureId;
            Orientation = orientation;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteInt(FunitureId);
            writer.WriteByte(Orientation);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            CellId = reader.ReadVarShort();
            FunitureId = reader.ReadInt();
            Orientation = reader.ReadByte();
        }
    }
}