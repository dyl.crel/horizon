namespace Horizon.Protocol.Types.Game.Guild
{
    public class GuildEmblem : NetworkType
    {
        public override short Protocol => 87;

        public short SymbolShape { get; set; }

        public int SymbolColor { get; set; }

        public byte BackgroundShape { get; set; }

        public int BackgroundColor { get; set; }

        public GuildEmblem()
        {
        }

        public GuildEmblem(short symbolShape, int symbolColor, byte backgroundShape, int backgroundColor)
        {
            SymbolShape = symbolShape;
            SymbolColor = symbolColor;
            BackgroundShape = backgroundShape;
            BackgroundColor = backgroundColor;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SymbolShape);
            writer.WriteInt(SymbolColor);
            writer.WriteByte(BackgroundShape);
            writer.WriteInt(BackgroundColor);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SymbolShape = reader.ReadVarShort();
            SymbolColor = reader.ReadInt();
            BackgroundShape = reader.ReadByte();
            BackgroundColor = reader.ReadInt();
        }
    }
}