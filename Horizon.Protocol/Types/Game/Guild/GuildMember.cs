using Horizon.Protocol.Types.Game.Character;
using Horizon.Protocol.Types.Game.Character.Status;

namespace Horizon.Protocol.Types.Game.Guild
{
    public class GuildMember : CharacterMinimalInformations
    {
        public override short Protocol => 88;

        public bool Sex { get; set; }

        public bool HavenBagShared { get; set; }

        public byte Breed { get; set; }

        public short Rank { get; set; }

        public long GivenExperience { get; set; }

        public byte ExperienceGivenPercent { get; set; }

        public int Rights { get; set; }

        public byte Connected { get; set; }

        public byte AlignmentSide { get; set; }

        public short HoursSinceLastConnection { get; set; }

        public short MoodSmileyId { get; set; }

        public int AccountId { get; set; }

        public int AchievementPoints { get; set; }

        public PlayerStatus Status { get; set; }

        public GuildMember()
        {
        }

        public GuildMember(long id, string name, short level, bool sex, bool havenBagShared, byte breed, short rank, long givenExperience, byte experienceGivenPercent, int rights, byte connected, byte alignmentSide, short hoursSinceLastConnection, short moodSmileyId, int accountId, int achievementPoints, PlayerStatus status) : base(id, name, level)
        {
            Sex = sex;
            HavenBagShared = havenBagShared;
            Breed = breed;
            Rank = rank;
            GivenExperience = givenExperience;
            ExperienceGivenPercent = experienceGivenPercent;
            Rights = rights;
            Connected = connected;
            AlignmentSide = alignmentSide;
            HoursSinceLastConnection = hoursSinceLastConnection;
            MoodSmileyId = moodSmileyId;
            AccountId = accountId;
            AchievementPoints = achievementPoints;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Sex);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, HavenBagShared);
            writer.WriteByte(flag0);
            writer.WriteByte(Breed);
            writer.WriteVarShort(Rank);
            writer.WriteVarLong(GivenExperience);
            writer.WriteByte(ExperienceGivenPercent);
            writer.WriteVarInt(Rights);
            writer.WriteByte(Connected);
            writer.WriteByte(AlignmentSide);
            writer.WriteShort(HoursSinceLastConnection);
            writer.WriteVarShort(MoodSmileyId);
            writer.WriteInt(AccountId);
            writer.WriteInt(AchievementPoints);
            writer.WriteShort(Status.Protocol);
            Status.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag0, 0);
            HavenBagShared = BooleanByteWrapper.GetFlag(flag0, 1);
            Breed = reader.ReadByte();
            Rank = reader.ReadVarShort();
            GivenExperience = reader.ReadVarLong();
            ExperienceGivenPercent = reader.ReadByte();
            Rights = reader.ReadVarInt();
            Connected = reader.ReadByte();
            AlignmentSide = reader.ReadByte();
            HoursSinceLastConnection = reader.ReadShort();
            MoodSmileyId = reader.ReadVarShort();
            AccountId = reader.ReadInt();
            AchievementPoints = reader.ReadInt();
            Status = ProtocolTypesManager.Instance<PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
    }
}