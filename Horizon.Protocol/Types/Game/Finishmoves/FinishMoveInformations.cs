namespace Horizon.Protocol.Types.Game.Finishmoves
{
    public class FinishMoveInformations : NetworkType
    {
        public override short Protocol => 506;

        public int FinishMoveId { get; set; }

        public bool FinishMoveState { get; set; }

        public FinishMoveInformations()
        {
        }

        public FinishMoveInformations(int finishMoveId, bool finishMoveState)
        {
            FinishMoveId = finishMoveId;
            FinishMoveState = finishMoveState;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(FinishMoveId);
            writer.WriteBoolean(FinishMoveState);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            FinishMoveId = reader.ReadInt();
            FinishMoveState = reader.ReadBoolean();
        }
    }
}