namespace Horizon.Protocol.Types.Game.Look
{
    public class SubEntity : NetworkType
    {
        public override short Protocol => 54;

        public byte BindingPointCategory { get; set; }

        public byte BindingPointIndex { get; set; }

        public EntityLook SubEntityLook { get; set; }

        public SubEntity()
        {
        }

        public SubEntity(byte bindingPointCategory, byte bindingPointIndex, EntityLook subEntityLook)
        {
            BindingPointCategory = bindingPointCategory;
            BindingPointIndex = bindingPointIndex;
            SubEntityLook = subEntityLook;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(BindingPointCategory);
            writer.WriteByte(BindingPointIndex);
            SubEntityLook.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BindingPointCategory = reader.ReadByte();
            BindingPointIndex = reader.ReadByte();
            SubEntityLook = new EntityLook();
            SubEntityLook.Deserialize(reader);
        }
    }
}