namespace Horizon.Protocol.Types.Game.Look
{
    public class IndexedEntityLook : NetworkType
    {
        public override short Protocol => 405;

        public EntityLook Look { get; set; }

        public byte Index { get; set; }

        public IndexedEntityLook()
        {
        }

        public IndexedEntityLook(EntityLook look, byte index)
        {
            Look = look;
            Index = index;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            Look.Serialize(writer);
            writer.WriteByte(Index);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Look = new EntityLook();
            Look.Deserialize(reader);
            Index = reader.ReadByte();
        }
    }
}