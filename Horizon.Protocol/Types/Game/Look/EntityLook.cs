namespace Horizon.Protocol.Types.Game.Look
{
    public class EntityLook : NetworkType
    {
        public override short Protocol => 55;

        public short BonesId { get; set; }

        public short[] Skins { get; set; }

        public int[] IndexedColors { get; set; }

        public short[] Scales { get; set; }

        public SubEntity[] Subentities { get; set; }

        public EntityLook()
        {
        }

        public EntityLook(short bonesId, short[] skins, int[] indexedColors, short[] scales, SubEntity[] subentities)
        {
            BonesId = bonesId;
            Skins = skins;
            IndexedColors = indexedColors;
            Scales = scales;
            Subentities = subentities;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(BonesId);
            writer.WriteShort(Skins.Length);
            for (var i = 0; i < Skins.Length; i++)
            {
                writer.WriteVarShort(Skins[i]);
            }
            writer.WriteShort(IndexedColors.Length);
            for (var i = 0; i < IndexedColors.Length; i++)
            {
                writer.WriteInt(IndexedColors[i]);
            }
            writer.WriteShort(Scales.Length);
            for (var i = 0; i < Scales.Length; i++)
            {
                writer.WriteVarShort(Scales[i]);
            }
            writer.WriteShort(Subentities.Length);
            for (var i = 0; i < Subentities.Length; i++)
            {
                Subentities[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            BonesId = reader.ReadVarShort();
            Skins = new short[reader.ReadShort()];
            for (var i = 0; i < Skins.Length; i++)
            {
                Skins[i] = reader.ReadVarShort();
            }
            IndexedColors = new int[reader.ReadShort()];
            for (var i = 0; i < IndexedColors.Length; i++)
            {
                IndexedColors[i] = reader.ReadInt();
            }
            Scales = new short[reader.ReadShort()];
            for (var i = 0; i < Scales.Length; i++)
            {
                Scales[i] = reader.ReadVarShort();
            }
            Subentities = new SubEntity[reader.ReadShort()];
            for (var i = 0; i < Subentities.Length; i++)
            {
                Subentities[i] = new SubEntity();
                Subentities[i].Deserialize(reader);
            }
        }
    }
}