namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemGenericQuantityPrice : ObjectItemGenericQuantity
    {
        public override short Protocol => 494;

        public long Price { get; set; }

        public ObjectItemGenericQuantityPrice()
        {
        }

        public ObjectItemGenericQuantityPrice(short objectGID, int quantity, long price) : base(objectGID, quantity)
        {
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Price = reader.ReadVarLong();
        }
    }
}