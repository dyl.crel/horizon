using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemToSellInBid : ObjectItemToSell
    {
        public override short Protocol => 164;

        public int UnsoldDelay { get; set; }

        public ObjectItemToSellInBid()
        {
        }

        public ObjectItemToSellInBid(short objectGID, ObjectEffect[] effects, int objectUID, int quantity, long objectPrice, int unsoldDelay) : base(objectGID, effects, objectUID, quantity, objectPrice)
        {
            UnsoldDelay = unsoldDelay;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(UnsoldDelay);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            UnsoldDelay = reader.ReadInt();
        }
    }
}