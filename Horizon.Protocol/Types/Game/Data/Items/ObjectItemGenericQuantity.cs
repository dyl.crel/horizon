namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemGenericQuantity : Item
    {
        public override short Protocol => 483;

        public short ObjectGID { get; set; }

        public int Quantity { get; set; }

        public ObjectItemGenericQuantity()
        {
        }

        public ObjectItemGenericQuantity(short objectGID, int quantity)
        {
            ObjectGID = objectGID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarShort();
            Quantity = reader.ReadVarInt();
        }
    }
}