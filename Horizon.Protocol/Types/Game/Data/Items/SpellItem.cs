namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class SpellItem : Item
    {
        public override short Protocol => 49;

        public int SpellId { get; set; }

        public short SpellLevel { get; set; }

        public SpellItem()
        {
        }

        public SpellItem(int spellId, short spellLevel)
        {
            SpellId = spellId;
            SpellLevel = spellLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(SpellId);
            writer.WriteShort(SpellLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadInt();
            SpellLevel = reader.ReadShort();
        }
    }
}