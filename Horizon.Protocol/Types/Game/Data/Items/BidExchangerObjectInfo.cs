using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class BidExchangerObjectInfo : NetworkType
    {
        public override short Protocol => 122;

        public int ObjectUID { get; set; }

        public ObjectEffect[] Effects { get; set; }

        public long[] Prices { get; set; }

        public BidExchangerObjectInfo()
        {
        }

        public BidExchangerObjectInfo(int objectUID, ObjectEffect[] effects, long[] prices)
        {
            ObjectUID = objectUID;
            Effects = effects;
            Prices = prices;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                writer.WriteShort(Effects[i].Protocol);
                Effects[i].Serialize(writer);
            }
            writer.WriteShort(Prices.Length);
            for (var i = 0; i < Prices.Length; i++)
            {
                writer.WriteVarLong(Prices[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObjectUID = reader.ReadVarInt();
            Effects = new ObjectEffect[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = ProtocolTypesManager.Instance<ObjectEffect>(reader.ReadShort());
                Effects[i].Deserialize(reader);
            }
            Prices = new long[reader.ReadShort()];
            for (var i = 0; i < Prices.Length; i++)
            {
                Prices[i] = reader.ReadVarLong();
            }
        }
    }
}