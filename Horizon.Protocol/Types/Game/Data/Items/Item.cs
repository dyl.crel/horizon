namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class Item : NetworkType
    {
        public override short Protocol => 7;

        public Item()
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
        }

        public override void Deserialize(BigEndianReader reader)
        {
        }
    }
}