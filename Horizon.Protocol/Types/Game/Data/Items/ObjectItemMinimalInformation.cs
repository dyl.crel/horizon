using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemMinimalInformation : Item
    {
        public override short Protocol => 124;

        public short ObjectGID { get; set; }

        public ObjectEffect[] Effects { get; set; }

        public ObjectItemMinimalInformation()
        {
        }

        public ObjectItemMinimalInformation(short objectGID, ObjectEffect[] effects)
        {
            ObjectGID = objectGID;
            Effects = effects;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                writer.WriteShort(Effects[i].Protocol);
                Effects[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarShort();
            Effects = new ObjectEffect[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = ProtocolTypesManager.Instance<ObjectEffect>(reader.ReadShort());
                Effects[i].Deserialize(reader);
            }
        }
    }
}