using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemInformationWithQuantity : ObjectItemMinimalInformation
    {
        public override short Protocol => 387;

        public int Quantity { get; set; }

        public ObjectItemInformationWithQuantity()
        {
        }

        public ObjectItemInformationWithQuantity(short objectGID, ObjectEffect[] effects, int quantity) : base(objectGID, effects)
        {
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Quantity = reader.ReadVarInt();
        }
    }
}