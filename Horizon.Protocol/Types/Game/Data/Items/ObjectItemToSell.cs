using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemToSell : Item
    {
        public override short Protocol => 120;

        public short ObjectGID { get; set; }

        public ObjectEffect[] Effects { get; set; }

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public long ObjectPrice { get; set; }

        public ObjectItemToSell()
        {
        }

        public ObjectItemToSell(short objectGID, ObjectEffect[] effects, int objectUID, int quantity, long objectPrice)
        {
            ObjectGID = objectGID;
            Effects = effects;
            ObjectUID = objectUID;
            Quantity = quantity;
            ObjectPrice = objectPrice;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                writer.WriteShort(Effects[i].Protocol);
                Effects[i].Serialize(writer);
            }
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
            writer.WriteVarLong(ObjectPrice);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarShort();
            Effects = new ObjectEffect[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = ProtocolTypesManager.Instance<ObjectEffect>(reader.ReadShort());
                Effects[i].Deserialize(reader);
            }
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
            ObjectPrice = reader.ReadVarLong();
        }
    }
}