namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectDuration : ObjectEffect
    {
        public override short Protocol => 75;

        public short Days { get; set; }

        public byte Hours { get; set; }

        public byte Minutes { get; set; }

        public ObjectEffectDuration()
        {
        }

        public ObjectEffectDuration(short actionId, short days, byte hours, byte minutes) : base(actionId)
        {
            Days = days;
            Hours = hours;
            Minutes = minutes;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Days);
            writer.WriteByte(Hours);
            writer.WriteByte(Minutes);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Days = reader.ReadVarShort();
            Hours = reader.ReadByte();
            Minutes = reader.ReadByte();
        }
    }
}