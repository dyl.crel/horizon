namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectInteger : ObjectEffect
    {
        public override short Protocol => 70;

        public int Value { get; set; }

        public ObjectEffectInteger()
        {
        }

        public ObjectEffectInteger(short actionId, int value) : base(actionId)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadVarInt();
        }
    }
}