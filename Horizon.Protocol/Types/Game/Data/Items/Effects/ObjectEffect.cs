namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffect : NetworkType
    {
        public override short Protocol => 76;

        public short ActionId { get; set; }

        public ObjectEffect()
        {
        }

        public ObjectEffect(short actionId)
        {
            ActionId = actionId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ActionId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActionId = reader.ReadVarShort();
        }
    }
}