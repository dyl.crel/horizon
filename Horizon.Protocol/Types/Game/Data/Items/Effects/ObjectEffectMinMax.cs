namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectMinMax : ObjectEffect
    {
        public override short Protocol => 82;

        public int Min { get; set; }

        public int Max { get; set; }

        public ObjectEffectMinMax()
        {
        }

        public ObjectEffectMinMax(short actionId, int min, int max) : base(actionId)
        {
            Min = min;
            Max = max;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Min);
            writer.WriteVarInt(Max);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Min = reader.ReadVarInt();
            Max = reader.ReadVarInt();
        }
    }
}