namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectDice : ObjectEffect
    {
        public override short Protocol => 73;

        public int DiceNum { get; set; }

        public int DiceSide { get; set; }

        public int DiceConst { get; set; }

        public ObjectEffectDice()
        {
        }

        public ObjectEffectDice(short actionId, int diceNum, int diceSide, int diceConst) : base(actionId)
        {
            DiceNum = diceNum;
            DiceSide = diceSide;
            DiceConst = diceConst;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(DiceNum);
            writer.WriteVarInt(DiceSide);
            writer.WriteVarInt(DiceConst);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DiceNum = reader.ReadVarInt();
            DiceSide = reader.ReadVarInt();
            DiceConst = reader.ReadVarInt();
        }
    }
}