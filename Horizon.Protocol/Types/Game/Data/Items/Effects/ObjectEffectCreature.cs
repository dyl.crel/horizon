namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectCreature : ObjectEffect
    {
        public override short Protocol => 71;

        public short MonsterFamilyId { get; set; }

        public ObjectEffectCreature()
        {
        }

        public ObjectEffectCreature(short actionId, short monsterFamilyId) : base(actionId)
        {
            MonsterFamilyId = monsterFamilyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(MonsterFamilyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MonsterFamilyId = reader.ReadVarShort();
        }
    }
}