namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectDate : ObjectEffect
    {
        public override short Protocol => 72;

        public short Year { get; set; }

        public byte Month { get; set; }

        public byte Day { get; set; }

        public byte Hour { get; set; }

        public byte Minute { get; set; }

        public ObjectEffectDate()
        {
        }

        public ObjectEffectDate(short actionId, short year, byte month, byte day, byte hour, byte minute) : base(actionId)
        {
            Year = year;
            Month = month;
            Day = day;
            Hour = hour;
            Minute = minute;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Year);
            writer.WriteByte(Month);
            writer.WriteByte(Day);
            writer.WriteByte(Hour);
            writer.WriteByte(Minute);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Year = reader.ReadVarShort();
            Month = reader.ReadByte();
            Day = reader.ReadByte();
            Hour = reader.ReadByte();
            Minute = reader.ReadByte();
        }
    }
}