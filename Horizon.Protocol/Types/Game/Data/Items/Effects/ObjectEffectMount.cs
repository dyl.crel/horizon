namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectMount : ObjectEffect
    {
        public override short Protocol => 179;

        public bool Sex { get; set; }

        public bool IsRideable { get; set; }

        public bool IsFeconded { get; set; }

        public bool IsFecondationReady { get; set; }

        public long Id { get; set; }

        public long ExpirationDate { get; set; }

        public int Model { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public byte Level { get; set; }

        public int ReproductionCount { get; set; }

        public int ReproductionCountMax { get; set; }

        public ObjectEffectInteger[] Effects { get; set; }

        public int[] Capacities { get; set; }

        public ObjectEffectMount()
        {
        }

        public ObjectEffectMount(short actionId, bool sex, bool isRideable, bool isFeconded, bool isFecondationReady, long id, long expirationDate, int model, string name, string owner, byte level, int reproductionCount, int reproductionCountMax, ObjectEffectInteger[] effects, int[] capacities) : base(actionId)
        {
            Sex = sex;
            IsRideable = isRideable;
            IsFeconded = isFeconded;
            IsFecondationReady = isFecondationReady;
            Id = id;
            ExpirationDate = expirationDate;
            Model = model;
            Name = name;
            Owner = owner;
            Level = level;
            ReproductionCount = reproductionCount;
            ReproductionCountMax = reproductionCountMax;
            Effects = effects;
            Capacities = capacities;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, Sex);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, IsRideable);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, IsFeconded);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, IsFecondationReady);
            writer.WriteByte(flag0);
            writer.WriteVarLong(Id);
            writer.WriteVarLong(ExpirationDate);
            writer.WriteVarInt(Model);
            writer.WriteUTF(Name);
            writer.WriteUTF(Owner);
            writer.WriteByte(Level);
            writer.WriteVarInt(ReproductionCount);
            writer.WriteVarInt(ReproductionCountMax);
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i].Serialize(writer);
            }
            writer.WriteShort(Capacities.Length);
            for (var i = 0; i < Capacities.Length; i++)
            {
                writer.WriteVarInt(Capacities[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            var flag0 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag0, 0);
            IsRideable = BooleanByteWrapper.GetFlag(flag0, 1);
            IsFeconded = BooleanByteWrapper.GetFlag(flag0, 2);
            IsFecondationReady = BooleanByteWrapper.GetFlag(flag0, 3);
            Id = reader.ReadVarLong();
            ExpirationDate = reader.ReadVarLong();
            Model = reader.ReadVarInt();
            Name = reader.ReadUTF();
            Owner = reader.ReadUTF();
            Level = reader.ReadByte();
            ReproductionCount = reader.ReadVarInt();
            ReproductionCountMax = reader.ReadVarInt();
            Effects = new ObjectEffectInteger[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = new ObjectEffectInteger();
                Effects[i].Deserialize(reader);
            }
            Capacities = new int[reader.ReadShort()];
            for (var i = 0; i < Capacities.Length; i++)
            {
                Capacities[i] = reader.ReadVarInt();
            }
        }
    }
}