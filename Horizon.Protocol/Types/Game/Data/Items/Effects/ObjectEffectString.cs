namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectString : ObjectEffect
    {
        public override short Protocol => 74;

        public string Value { get; set; }

        public ObjectEffectString()
        {
        }

        public ObjectEffectString(short actionId, string value) : base(actionId)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadUTF();
        }
    }
}