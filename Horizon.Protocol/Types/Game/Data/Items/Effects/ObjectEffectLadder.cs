namespace Horizon.Protocol.Types.Game.Data.Items.Effects
{
    public class ObjectEffectLadder : ObjectEffectCreature
    {
        public override short Protocol => 81;

        public int MonsterCount { get; set; }

        public ObjectEffectLadder()
        {
        }

        public ObjectEffectLadder(short actionId, short monsterFamilyId, int monsterCount) : base(actionId, monsterFamilyId)
        {
            MonsterCount = monsterCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(MonsterCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            MonsterCount = reader.ReadVarInt();
        }
    }
}