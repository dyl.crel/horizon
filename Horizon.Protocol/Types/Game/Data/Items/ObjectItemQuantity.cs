namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemQuantity : Item
    {
        public override short Protocol => 119;

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public ObjectItemQuantity()
        {
        }

        public ObjectItemQuantity(int objectUID, int quantity)
        {
            ObjectUID = objectUID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}