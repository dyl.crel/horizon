namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class SellerBuyerDescriptor : NetworkType
    {
        public override short Protocol => 121;

        public int[] Quantities { get; set; }

        public int[] Types { get; set; }

        public float TaxPercentage { get; set; }

        public float TaxModificationPercentage { get; set; }

        public byte MaxItemLevel { get; set; }

        public int MaxItemPerAccount { get; set; }

        public int NpcContextualId { get; set; }

        public short UnsoldDelay { get; set; }

        public SellerBuyerDescriptor()
        {
        }

        public SellerBuyerDescriptor(int[] quantities, int[] types, float taxPercentage, float taxModificationPercentage, byte maxItemLevel, int maxItemPerAccount, int npcContextualId, short unsoldDelay)
        {
            Quantities = quantities;
            Types = types;
            TaxPercentage = taxPercentage;
            TaxModificationPercentage = taxModificationPercentage;
            MaxItemLevel = maxItemLevel;
            MaxItemPerAccount = maxItemPerAccount;
            NpcContextualId = npcContextualId;
            UnsoldDelay = unsoldDelay;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Quantities.Length);
            for (var i = 0; i < Quantities.Length; i++)
            {
                writer.WriteVarInt(Quantities[i]);
            }
            writer.WriteShort(Types.Length);
            for (var i = 0; i < Types.Length; i++)
            {
                writer.WriteVarInt(Types[i]);
            }
            writer.WriteFloat(TaxPercentage);
            writer.WriteFloat(TaxModificationPercentage);
            writer.WriteByte(MaxItemLevel);
            writer.WriteVarInt(MaxItemPerAccount);
            writer.WriteInt(NpcContextualId);
            writer.WriteVarShort(UnsoldDelay);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Quantities = new int[reader.ReadShort()];
            for (var i = 0; i < Quantities.Length; i++)
            {
                Quantities[i] = reader.ReadVarInt();
            }
            Types = new int[reader.ReadShort()];
            for (var i = 0; i < Types.Length; i++)
            {
                Types[i] = reader.ReadVarInt();
            }
            TaxPercentage = reader.ReadFloat();
            TaxModificationPercentage = reader.ReadFloat();
            MaxItemLevel = reader.ReadByte();
            MaxItemPerAccount = reader.ReadVarInt();
            NpcContextualId = reader.ReadInt();
            UnsoldDelay = reader.ReadVarShort();
        }
    }
}