using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItem : Item
    {
        public override short Protocol => 37;

        public short Position { get; set; }

        public short ObjectGID { get; set; }

        public ObjectEffect[] Effects { get; set; }

        public int ObjectUID { get; set; }

        public int Quantity { get; set; }

        public ObjectItem()
        {
        }

        public ObjectItem(short position, short objectGID, ObjectEffect[] effects, int objectUID, int quantity)
        {
            Position = position;
            ObjectGID = objectGID;
            Effects = effects;
            ObjectUID = objectUID;
            Quantity = quantity;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Position);
            writer.WriteVarShort(ObjectGID);
            writer.WriteShort(Effects.Length);
            for (var i = 0; i < Effects.Length; i++)
            {
                writer.WriteShort(Effects[i].Protocol);
                Effects[i].Serialize(writer);
            }
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Position = reader.ReadShort();
            ObjectGID = reader.ReadVarShort();
            Effects = new ObjectEffect[reader.ReadShort()];
            for (var i = 0; i < Effects.Length; i++)
            {
                Effects[i] = ProtocolTypesManager.Instance<ObjectEffect>(reader.ReadShort());
                Effects[i].Deserialize(reader);
            }
            ObjectUID = reader.ReadVarInt();
            Quantity = reader.ReadVarInt();
        }
    }
}