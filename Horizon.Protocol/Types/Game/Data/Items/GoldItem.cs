namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class GoldItem : Item
    {
        public override short Protocol => 123;

        public long Sum { get; set; }

        public GoldItem()
        {
        }

        public GoldItem(long sum)
        {
            Sum = sum;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Sum);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Sum = reader.ReadVarLong();
        }
    }
}