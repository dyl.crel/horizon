using Horizon.Protocol.Types.Game.Data.Items.Effects;

namespace Horizon.Protocol.Types.Game.Data.Items
{
    public class ObjectItemToSellInNpcShop : ObjectItemMinimalInformation
    {
        public override short Protocol => 352;

        public long ObjectPrice { get; set; }

        public string BuyCriterion { get; set; }

        public ObjectItemToSellInNpcShop()
        {
        }

        public ObjectItemToSellInNpcShop(short objectGID, ObjectEffect[] effects, long objectPrice, string buyCriterion) : base(objectGID, effects)
        {
            ObjectPrice = objectPrice;
            BuyCriterion = buyCriterion;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(ObjectPrice);
            writer.WriteUTF(BuyCriterion);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ObjectPrice = reader.ReadVarLong();
            BuyCriterion = reader.ReadUTF();
        }
    }
}