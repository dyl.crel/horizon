namespace Horizon.Protocol.Types.Game.Inventory.Exchanges
{
    public class RecycledItem : NetworkType
    {
        public override short Protocol => 547;

        public short Id { get; set; }

        public int Qty { get; set; }

        public RecycledItem()
        {
        }

        public RecycledItem(short id, int qty)
        {
            Id = id;
            Qty = qty;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteInt(Qty);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
            Qty = reader.ReadInt();
        }
    }
}