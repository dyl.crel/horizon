namespace Horizon.Protocol.Types.Game.Idol
{
    public class Idol : NetworkType
    {
        public override short Protocol => 489;

        public short Id { get; set; }

        public short XpBonusPercent { get; set; }

        public short DropBonusPercent { get; set; }

        public Idol()
        {
        }

        public Idol(short id, short xpBonusPercent, short dropBonusPercent)
        {
            Id = id;
            XpBonusPercent = xpBonusPercent;
            DropBonusPercent = dropBonusPercent;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteVarShort(XpBonusPercent);
            writer.WriteVarShort(DropBonusPercent);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
            XpBonusPercent = reader.ReadVarShort();
            DropBonusPercent = reader.ReadVarShort();
        }
    }
}