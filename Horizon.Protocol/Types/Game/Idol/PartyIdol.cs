namespace Horizon.Protocol.Types.Game.Idol
{
    public class PartyIdol : Idol
    {
        public override short Protocol => 490;

        public long[] OwnersIds { get; set; }

        public PartyIdol()
        {
        }

        public PartyIdol(short id, short xpBonusPercent, short dropBonusPercent, long[] ownersIds) : base(id, xpBonusPercent, dropBonusPercent)
        {
            OwnersIds = ownersIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(OwnersIds.Length);
            for (var i = 0; i < OwnersIds.Length; i++)
            {
                writer.WriteVarLong(OwnersIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            OwnersIds = new long[reader.ReadShort()];
            for (var i = 0; i < OwnersIds.Length; i++)
            {
                OwnersIds[i] = reader.ReadVarLong();
            }
        }
    }
}