namespace Horizon.Protocol.Types.Game.Dare
{
    public class DareCriteria : NetworkType
    {
        public override short Protocol => 501;

        public byte Type { get; set; }

        public int[] Params { get; set; }

        public DareCriteria()
        {
        }

        public DareCriteria(byte type, int[] @params)
        {
            Type = type;
            Params = @params;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
            writer.WriteShort(Params.Length);
            for (var i = 0; i < Params.Length; i++)
            {
                writer.WriteInt(Params[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
            Params = new int[reader.ReadShort()];
            for (var i = 0; i < Params.Length; i++)
            {
                Params[i] = reader.ReadInt();
            }
        }
    }
}