namespace Horizon.Protocol.Types.Game.Dare
{
    public class DareReward : NetworkType
    {
        public override short Protocol => 505;

        public byte Type { get; set; }

        public short MonsterId { get; set; }

        public long Kamas { get; set; }

        public double DareId { get; set; }

        public DareReward()
        {
        }

        public DareReward(byte type, short monsterId, long kamas, double dareId)
        {
            Type = type;
            MonsterId = monsterId;
            Kamas = kamas;
            DareId = dareId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Type);
            writer.WriteVarShort(MonsterId);
            writer.WriteVarLong(Kamas);
            writer.WriteDouble(DareId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Type = reader.ReadByte();
            MonsterId = reader.ReadVarShort();
            Kamas = reader.ReadVarLong();
            DareId = reader.ReadDouble();
        }
    }
}