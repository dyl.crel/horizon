using Horizon.Protocol.Types.Game.Character;

namespace Horizon.Protocol.Types.Game.Dare
{
    public class DareInformations : NetworkType
    {
        public override short Protocol => 502;

        public double DareId { get; set; }

        public CharacterBasicMinimalInformations Creator { get; set; }

        public long SubscriptionFee { get; set; }

        public long Jackpot { get; set; }

        public short MaxCountWinners { get; set; }

        public double EndDate { get; set; }

        public bool IsPrivate { get; set; }

        public int GuildId { get; set; }

        public int AllianceId { get; set; }

        public DareCriteria[] Criterions { get; set; }

        public double StartDate { get; set; }

        public DareInformations()
        {
        }

        public DareInformations(double dareId, CharacterBasicMinimalInformations creator, long subscriptionFee, long jackpot, short maxCountWinners, double endDate, bool isPrivate, int guildId, int allianceId, DareCriteria[] criterions, double startDate)
        {
            DareId = dareId;
            Creator = creator;
            SubscriptionFee = subscriptionFee;
            Jackpot = jackpot;
            MaxCountWinners = maxCountWinners;
            EndDate = endDate;
            IsPrivate = isPrivate;
            GuildId = guildId;
            AllianceId = allianceId;
            Criterions = criterions;
            StartDate = startDate;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
            Creator.Serialize(writer);
            writer.WriteVarLong(SubscriptionFee);
            writer.WriteVarLong(Jackpot);
            writer.WriteShort(MaxCountWinners);
            writer.WriteDouble(EndDate);
            writer.WriteBoolean(IsPrivate);
            writer.WriteVarInt(GuildId);
            writer.WriteVarInt(AllianceId);
            writer.WriteShort(Criterions.Length);
            for (var i = 0; i < Criterions.Length; i++)
            {
                Criterions[i].Serialize(writer);
            }
            writer.WriteDouble(StartDate);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
            Creator = new CharacterBasicMinimalInformations();
            Creator.Deserialize(reader);
            SubscriptionFee = reader.ReadVarLong();
            Jackpot = reader.ReadVarLong();
            MaxCountWinners = reader.ReadShort();
            EndDate = reader.ReadDouble();
            IsPrivate = reader.ReadBoolean();
            GuildId = reader.ReadVarInt();
            AllianceId = reader.ReadVarInt();
            Criterions = new DareCriteria[reader.ReadShort()];
            for (var i = 0; i < Criterions.Length; i++)
            {
                Criterions[i] = new DareCriteria();
                Criterions[i].Deserialize(reader);
            }
            StartDate = reader.ReadDouble();
        }
    }
}