namespace Horizon.Protocol.Types.Game.Dare
{
    public class DareVersatileInformations : NetworkType
    {
        public override short Protocol => 504;

        public double DareId { get; set; }

        public int CountEntrants { get; set; }

        public int CountWinners { get; set; }

        public DareVersatileInformations()
        {
        }

        public DareVersatileInformations(double dareId, int countEntrants, int countWinners)
        {
            DareId = dareId;
            CountEntrants = countEntrants;
            CountWinners = countWinners;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteDouble(DareId);
            writer.WriteInt(CountEntrants);
            writer.WriteInt(CountWinners);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            DareId = reader.ReadDouble();
            CountEntrants = reader.ReadInt();
            CountWinners = reader.ReadInt();
        }
    }
}