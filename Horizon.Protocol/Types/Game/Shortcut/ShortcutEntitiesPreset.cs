namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutEntitiesPreset : Shortcut
    {
        public override short Protocol => 544;

        public short PresetId { get; set; }

        public ShortcutEntitiesPreset()
        {
        }

        public ShortcutEntitiesPreset(byte slot, short presetId) : base(slot)
        {
            PresetId = presetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(PresetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PresetId = reader.ReadShort();
        }
    }
}