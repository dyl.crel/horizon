namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutSpell : Shortcut
    {
        public override short Protocol => 368;

        public short SpellId { get; set; }

        public ShortcutSpell()
        {
        }

        public ShortcutSpell(byte slot, short spellId) : base(slot)
        {
            SpellId = spellId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SpellId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadVarShort();
        }
    }
}