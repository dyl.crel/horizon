namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutEmote : Shortcut
    {
        public override short Protocol => 389;

        public byte EmoteId { get; set; }

        public ShortcutEmote()
        {
        }

        public ShortcutEmote(byte slot, byte emoteId) : base(slot)
        {
            EmoteId = emoteId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(EmoteId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            EmoteId = reader.ReadByte();
        }
    }
}