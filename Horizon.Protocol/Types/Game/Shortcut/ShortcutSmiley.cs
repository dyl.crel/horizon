namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutSmiley : Shortcut
    {
        public override short Protocol => 388;

        public short SmileyId { get; set; }

        public ShortcutSmiley()
        {
        }

        public ShortcutSmiley(byte slot, short smileyId) : base(slot)
        {
            SmileyId = smileyId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SmileyId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            SmileyId = reader.ReadVarShort();
        }
    }
}