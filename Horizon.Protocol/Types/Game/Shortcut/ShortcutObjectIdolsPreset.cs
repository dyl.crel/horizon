namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutObjectIdolsPreset : ShortcutObject
    {
        public override short Protocol => 492;

        public short PresetId { get; set; }

        public ShortcutObjectIdolsPreset()
        {
        }

        public ShortcutObjectIdolsPreset(byte slot, short presetId) : base(slot)
        {
            PresetId = presetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(PresetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PresetId = reader.ReadShort();
        }
    }
}