namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class Shortcut : NetworkType
    {
        public override short Protocol => 369;

        public byte Slot { get; set; }

        public Shortcut()
        {
        }

        public Shortcut(byte slot)
        {
            Slot = slot;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Slot);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Slot = reader.ReadByte();
        }
    }
}