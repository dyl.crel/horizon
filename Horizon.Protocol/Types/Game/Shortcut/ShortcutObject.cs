namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutObject : Shortcut
    {
        public override short Protocol => 367;

        public ShortcutObject()
        {
        }

        public ShortcutObject(byte slot) : base(slot)
        {
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
        }
    }
}