namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutObjectItem : ShortcutObject
    {
        public override short Protocol => 371;

        public int ItemUID { get; set; }

        public int ItemGID { get; set; }

        public ShortcutObjectItem()
        {
        }

        public ShortcutObjectItem(byte slot, int itemUID, int itemGID) : base(slot)
        {
            ItemUID = itemUID;
            ItemGID = itemGID;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(ItemUID);
            writer.WriteInt(ItemGID);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            ItemUID = reader.ReadInt();
            ItemGID = reader.ReadInt();
        }
    }
}