namespace Horizon.Protocol.Types.Game.Shortcut
{
    public class ShortcutObjectPreset : ShortcutObject
    {
        public override short Protocol => 370;

        public short PresetId { get; set; }

        public ShortcutObjectPreset()
        {
        }

        public ShortcutObjectPreset(byte slot, short presetId) : base(slot)
        {
            PresetId = presetId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(PresetId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PresetId = reader.ReadShort();
        }
    }
}