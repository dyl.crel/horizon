namespace Horizon.Protocol.Types.Game.Entity
{
    public class EntityInformation : NetworkType
    {
        public override short Protocol => 546;

        public short Id { get; set; }

        public int Experience { get; set; }

        public bool Status { get; set; }

        public EntityInformation()
        {
        }

        public EntityInformation(short id, int experience, bool status)
        {
            Id = id;
            Experience = experience;
            Status = status;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteVarInt(Experience);
            writer.WriteBoolean(Status);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
            Experience = reader.ReadVarInt();
            Status = reader.ReadBoolean();
        }
    }
}