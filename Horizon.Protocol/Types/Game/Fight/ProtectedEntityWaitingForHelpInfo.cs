namespace Horizon.Protocol.Types.Game.Fight
{
    public class ProtectedEntityWaitingForHelpInfo : NetworkType
    {
        public override short Protocol => 186;

        public int TimeLeftBeforeFight { get; set; }

        public int WaitTimeForPlacement { get; set; }

        public byte NbPositionForDefensors { get; set; }

        public ProtectedEntityWaitingForHelpInfo()
        {
        }

        public ProtectedEntityWaitingForHelpInfo(int timeLeftBeforeFight, int waitTimeForPlacement, byte nbPositionForDefensors)
        {
            TimeLeftBeforeFight = timeLeftBeforeFight;
            WaitTimeForPlacement = waitTimeForPlacement;
            NbPositionForDefensors = nbPositionForDefensors;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(TimeLeftBeforeFight);
            writer.WriteInt(WaitTimeForPlacement);
            writer.WriteByte(NbPositionForDefensors);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TimeLeftBeforeFight = reader.ReadInt();
            WaitTimeForPlacement = reader.ReadInt();
            NbPositionForDefensors = reader.ReadByte();
        }
    }
}