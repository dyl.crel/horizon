using Horizon.Protocol.Types.Game.Character;
using Horizon.Protocol.Types.Game.Fight;

namespace Horizon.Protocol.Types.Game.Prism
{
    public class PrismFightersInformation : NetworkType
    {
        public override short Protocol => 443;

        public short SubAreaId { get; set; }

        public ProtectedEntityWaitingForHelpInfo WaitingForHelpInfo { get; set; }

        public CharacterMinimalPlusLookInformations[] AllyCharactersInformations { get; set; }

        public CharacterMinimalPlusLookInformations[] EnemyCharactersInformations { get; set; }

        public PrismFightersInformation()
        {
        }

        public PrismFightersInformation(short subAreaId, ProtectedEntityWaitingForHelpInfo waitingForHelpInfo, CharacterMinimalPlusLookInformations[] allyCharactersInformations, CharacterMinimalPlusLookInformations[] enemyCharactersInformations)
        {
            SubAreaId = subAreaId;
            WaitingForHelpInfo = waitingForHelpInfo;
            AllyCharactersInformations = allyCharactersInformations;
            EnemyCharactersInformations = enemyCharactersInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            WaitingForHelpInfo.Serialize(writer);
            writer.WriteShort(AllyCharactersInformations.Length);
            for (var i = 0; i < AllyCharactersInformations.Length; i++)
            {
                writer.WriteShort(AllyCharactersInformations[i].Protocol);
                AllyCharactersInformations[i].Serialize(writer);
            }
            writer.WriteShort(EnemyCharactersInformations.Length);
            for (var i = 0; i < EnemyCharactersInformations.Length; i++)
            {
                writer.WriteShort(EnemyCharactersInformations[i].Protocol);
                EnemyCharactersInformations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            WaitingForHelpInfo = new ProtectedEntityWaitingForHelpInfo();
            WaitingForHelpInfo.Deserialize(reader);
            AllyCharactersInformations = new CharacterMinimalPlusLookInformations[reader.ReadShort()];
            for (var i = 0; i < AllyCharactersInformations.Length; i++)
            {
                AllyCharactersInformations[i] = ProtocolTypesManager.Instance<CharacterMinimalPlusLookInformations>(reader.ReadShort());
                AllyCharactersInformations[i].Deserialize(reader);
            }
            EnemyCharactersInformations = new CharacterMinimalPlusLookInformations[reader.ReadShort()];
            for (var i = 0; i < EnemyCharactersInformations.Length; i++)
            {
                EnemyCharactersInformations[i] = ProtocolTypesManager.Instance<CharacterMinimalPlusLookInformations>(reader.ReadShort());
                EnemyCharactersInformations[i].Deserialize(reader);
            }
        }
    }
}