namespace Horizon.Protocol.Types.Game.Prism
{
    public class PrismSubareaEmptyInfo : NetworkType
    {
        public override short Protocol => 438;

        public short SubAreaId { get; set; }

        public int AllianceId { get; set; }

        public PrismSubareaEmptyInfo()
        {
        }

        public PrismSubareaEmptyInfo(short subAreaId, int allianceId)
        {
            SubAreaId = subAreaId;
            AllianceId = allianceId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteVarInt(AllianceId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SubAreaId = reader.ReadVarShort();
            AllianceId = reader.ReadVarInt();
        }
    }
}