namespace Horizon.Protocol.Types.Game.Prism
{
    public class PrismGeolocalizedInformation : PrismSubareaEmptyInfo
    {
        public override short Protocol => 434;

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public PrismInformation Prism { get; set; }

        public PrismGeolocalizedInformation()
        {
        }

        public PrismGeolocalizedInformation(short subAreaId, int allianceId, short worldX, short worldY, double mapId, PrismInformation prism) : base(subAreaId, allianceId)
        {
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            Prism = prism;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteShort(Prism.Protocol);
            Prism.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            Prism = ProtocolTypesManager.Instance<PrismInformation>(reader.ReadShort());
            Prism.Deserialize(reader);
        }
    }
}