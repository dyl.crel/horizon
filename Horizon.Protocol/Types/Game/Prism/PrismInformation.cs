namespace Horizon.Protocol.Types.Game.Prism
{
    public class PrismInformation : NetworkType
    {
        public override short Protocol => 428;

        public byte TypeId { get; set; }

        public byte State { get; set; }

        public int NextVulnerabilityDate { get; set; }

        public int PlacementDate { get; set; }

        public int RewardTokenCount { get; set; }

        public PrismInformation()
        {
        }

        public PrismInformation(byte typeId, byte state, int nextVulnerabilityDate, int placementDate, int rewardTokenCount)
        {
            TypeId = typeId;
            State = state;
            NextVulnerabilityDate = nextVulnerabilityDate;
            PlacementDate = placementDate;
            RewardTokenCount = rewardTokenCount;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(TypeId);
            writer.WriteByte(State);
            writer.WriteInt(NextVulnerabilityDate);
            writer.WriteInt(PlacementDate);
            writer.WriteVarInt(RewardTokenCount);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            TypeId = reader.ReadByte();
            State = reader.ReadByte();
            NextVulnerabilityDate = reader.ReadInt();
            PlacementDate = reader.ReadInt();
            RewardTokenCount = reader.ReadVarInt();
        }
    }
}