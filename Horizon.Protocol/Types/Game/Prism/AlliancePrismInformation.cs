using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Prism
{
    public class AlliancePrismInformation : PrismInformation
    {
        public override short Protocol => 427;

        public AllianceInformations Alliance { get; set; }

        public AlliancePrismInformation()
        {
        }

        public AlliancePrismInformation(byte typeId, byte state, int nextVulnerabilityDate, int placementDate, int rewardTokenCount, AllianceInformations alliance) : base(typeId, state, nextVulnerabilityDate, placementDate, rewardTokenCount)
        {
            Alliance = alliance;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Alliance.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Alliance = new AllianceInformations();
            Alliance.Deserialize(reader);
        }
    }
}