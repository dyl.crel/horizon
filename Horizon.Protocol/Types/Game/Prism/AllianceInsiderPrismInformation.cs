using Horizon.Protocol.Types.Game.Data.Items;

namespace Horizon.Protocol.Types.Game.Prism
{
    public class AllianceInsiderPrismInformation : PrismInformation
    {
        public override short Protocol => 431;

        public int LastTimeSlotModificationDate { get; set; }

        public int LastTimeSlotModificationAuthorGuildId { get; set; }

        public long LastTimeSlotModificationAuthorId { get; set; }

        public string LastTimeSlotModificationAuthorName { get; set; }

        public ObjectItem[] ModulesObjects { get; set; }

        public AllianceInsiderPrismInformation()
        {
        }

        public AllianceInsiderPrismInformation(byte typeId, byte state, int nextVulnerabilityDate, int placementDate, int rewardTokenCount, int lastTimeSlotModificationDate, int lastTimeSlotModificationAuthorGuildId, long lastTimeSlotModificationAuthorId, string lastTimeSlotModificationAuthorName, ObjectItem[] modulesObjects) : base(typeId, state, nextVulnerabilityDate, placementDate, rewardTokenCount)
        {
            LastTimeSlotModificationDate = lastTimeSlotModificationDate;
            LastTimeSlotModificationAuthorGuildId = lastTimeSlotModificationAuthorGuildId;
            LastTimeSlotModificationAuthorId = lastTimeSlotModificationAuthorId;
            LastTimeSlotModificationAuthorName = lastTimeSlotModificationAuthorName;
            ModulesObjects = modulesObjects;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(LastTimeSlotModificationDate);
            writer.WriteVarInt(LastTimeSlotModificationAuthorGuildId);
            writer.WriteVarLong(LastTimeSlotModificationAuthorId);
            writer.WriteUTF(LastTimeSlotModificationAuthorName);
            writer.WriteShort(ModulesObjects.Length);
            for (var i = 0; i < ModulesObjects.Length; i++)
            {
                ModulesObjects[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            LastTimeSlotModificationDate = reader.ReadInt();
            LastTimeSlotModificationAuthorGuildId = reader.ReadVarInt();
            LastTimeSlotModificationAuthorId = reader.ReadVarLong();
            LastTimeSlotModificationAuthorName = reader.ReadUTF();
            ModulesObjects = new ObjectItem[reader.ReadShort()];
            for (var i = 0; i < ModulesObjects.Length; i++)
            {
                ModulesObjects[i] = new ObjectItem();
                ModulesObjects[i].Deserialize(reader);
            }
        }
    }
}