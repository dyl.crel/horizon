using Horizon.Protocol.Types.Game.Actions.Fight;

namespace Horizon.Protocol.Types.Game.Action.Fight
{
    public class FightDispellableEffectExtendedInformations : NetworkType
    {
        public override short Protocol => 208;

        public short ActionId { get; set; }

        public double SourceId { get; set; }

        public AbstractFightDispellableEffect Effect { get; set; }

        public FightDispellableEffectExtendedInformations()
        {
        }

        public FightDispellableEffectExtendedInformations(short actionId, double sourceId, AbstractFightDispellableEffect effect)
        {
            ActionId = actionId;
            SourceId = sourceId;
            Effect = effect;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ActionId);
            writer.WriteDouble(SourceId);
            writer.WriteShort(Effect.Protocol);
            Effect.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ActionId = reader.ReadVarShort();
            SourceId = reader.ReadDouble();
            Effect = ProtocolTypesManager.Instance<AbstractFightDispellableEffect>(reader.ReadShort());
            Effect.Deserialize(reader);
        }
    }
}