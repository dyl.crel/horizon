namespace Horizon.Protocol.Types.Game.Interactive
{
    public class InteractiveElement : NetworkType
    {
        public override short Protocol => 80;

        public int ElementId { get; set; }

        public int ElementTypeId { get; set; }

        public InteractiveElementSkill[] EnabledSkills { get; set; }

        public InteractiveElementSkill[] DisabledSkills { get; set; }

        public bool OnCurrentMap { get; set; }

        public InteractiveElement()
        {
        }

        public InteractiveElement(int elementId, int elementTypeId, InteractiveElementSkill[] enabledSkills, InteractiveElementSkill[] disabledSkills, bool onCurrentMap)
        {
            ElementId = elementId;
            ElementTypeId = elementTypeId;
            EnabledSkills = enabledSkills;
            DisabledSkills = disabledSkills;
            OnCurrentMap = onCurrentMap;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(ElementId);
            writer.WriteInt(ElementTypeId);
            writer.WriteShort(EnabledSkills.Length);
            for (var i = 0; i < EnabledSkills.Length; i++)
            {
                writer.WriteShort(EnabledSkills[i].Protocol);
                EnabledSkills[i].Serialize(writer);
            }
            writer.WriteShort(DisabledSkills.Length);
            for (var i = 0; i < DisabledSkills.Length; i++)
            {
                writer.WriteShort(DisabledSkills[i].Protocol);
                DisabledSkills[i].Serialize(writer);
            }
            writer.WriteBoolean(OnCurrentMap);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ElementId = reader.ReadInt();
            ElementTypeId = reader.ReadInt();
            EnabledSkills = new InteractiveElementSkill[reader.ReadShort()];
            for (var i = 0; i < EnabledSkills.Length; i++)
            {
                EnabledSkills[i] = ProtocolTypesManager.Instance<InteractiveElementSkill>(reader.ReadShort());
                EnabledSkills[i].Deserialize(reader);
            }
            DisabledSkills = new InteractiveElementSkill[reader.ReadShort()];
            for (var i = 0; i < DisabledSkills.Length; i++)
            {
                DisabledSkills[i] = ProtocolTypesManager.Instance<InteractiveElementSkill>(reader.ReadShort());
                DisabledSkills[i].Deserialize(reader);
            }
            OnCurrentMap = reader.ReadBoolean();
        }
    }
}