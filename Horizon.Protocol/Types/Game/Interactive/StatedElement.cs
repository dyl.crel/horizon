namespace Horizon.Protocol.Types.Game.Interactive
{
    public class StatedElement : NetworkType
    {
        public override short Protocol => 108;

        public int ElementId { get; set; }

        public short ElementCellId { get; set; }

        public int ElementState { get; set; }

        public bool OnCurrentMap { get; set; }

        public StatedElement()
        {
        }

        public StatedElement(int elementId, short elementCellId, int elementState, bool onCurrentMap)
        {
            ElementId = elementId;
            ElementCellId = elementCellId;
            ElementState = elementState;
            OnCurrentMap = onCurrentMap;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(ElementId);
            writer.WriteVarShort(ElementCellId);
            writer.WriteVarInt(ElementState);
            writer.WriteBoolean(OnCurrentMap);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ElementId = reader.ReadInt();
            ElementCellId = reader.ReadVarShort();
            ElementState = reader.ReadVarInt();
            OnCurrentMap = reader.ReadBoolean();
        }
    }
}