namespace Horizon.Protocol.Types.Game.Interactive
{
    public class InteractiveElementSkill : NetworkType
    {
        public override short Protocol => 219;

        public int SkillId { get; set; }

        public int SkillInstanceUid { get; set; }

        public InteractiveElementSkill()
        {
        }

        public InteractiveElementSkill(int skillId, int skillInstanceUid)
        {
            SkillId = skillId;
            SkillInstanceUid = skillInstanceUid;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(SkillId);
            writer.WriteInt(SkillInstanceUid);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SkillId = reader.ReadVarInt();
            SkillInstanceUid = reader.ReadInt();
        }
    }
}