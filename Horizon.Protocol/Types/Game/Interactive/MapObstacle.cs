namespace Horizon.Protocol.Types.Game.Interactive
{
    public class MapObstacle : NetworkType
    {
        public override short Protocol => 200;

        public short ObstacleCellId { get; set; }

        public byte State { get; set; }

        public MapObstacle()
        {
        }

        public MapObstacle(short obstacleCellId, byte state)
        {
            ObstacleCellId = obstacleCellId;
            State = state;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ObstacleCellId);
            writer.WriteByte(State);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ObstacleCellId = reader.ReadVarShort();
            State = reader.ReadByte();
        }
    }
}