namespace Horizon.Protocol.Types.Game.Interactive
{
    public class InteractiveElementWithAgeBonus : InteractiveElement
    {
        public override short Protocol => 398;

        public short AgeBonus { get; set; }

        public InteractiveElementWithAgeBonus()
        {
        }

        public InteractiveElementWithAgeBonus(int elementId, int elementTypeId, InteractiveElementSkill[] enabledSkills, InteractiveElementSkill[] disabledSkills, bool onCurrentMap, short ageBonus) : base(elementId, elementTypeId, enabledSkills, disabledSkills, onCurrentMap)
        {
            AgeBonus = ageBonus;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(AgeBonus);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            AgeBonus = reader.ReadShort();
        }
    }
}