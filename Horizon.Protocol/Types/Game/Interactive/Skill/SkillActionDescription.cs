namespace Horizon.Protocol.Types.Game.Interactive.Skill
{
    public class SkillActionDescription : NetworkType
    {
        public override short Protocol => 102;

        public short SkillId { get; set; }

        public SkillActionDescription()
        {
        }

        public SkillActionDescription(short skillId)
        {
            SkillId = skillId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SkillId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SkillId = reader.ReadVarShort();
        }
    }
}