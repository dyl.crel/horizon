namespace Horizon.Protocol.Types.Game.Interactive.Skill
{
    public class SkillActionDescriptionCollect : SkillActionDescriptionTimed
    {
        public override short Protocol => 99;

        public short Min { get; set; }

        public short Max { get; set; }

        public SkillActionDescriptionCollect()
        {
        }

        public SkillActionDescriptionCollect(short skillId, byte time, short min, short max) : base(skillId, time)
        {
            Min = min;
            Max = max;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Min);
            writer.WriteVarShort(Max);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Min = reader.ReadVarShort();
            Max = reader.ReadVarShort();
        }
    }
}