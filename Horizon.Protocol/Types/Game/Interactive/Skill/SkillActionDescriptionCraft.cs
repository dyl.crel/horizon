namespace Horizon.Protocol.Types.Game.Interactive.Skill
{
    public class SkillActionDescriptionCraft : SkillActionDescription
    {
        public override short Protocol => 100;

        public byte Probability { get; set; }

        public SkillActionDescriptionCraft()
        {
        }

        public SkillActionDescriptionCraft(short skillId, byte probability) : base(skillId)
        {
            Probability = probability;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Probability);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Probability = reader.ReadByte();
        }
    }
}