namespace Horizon.Protocol.Types.Game.Interactive.Skill
{
    public class SkillActionDescriptionTimed : SkillActionDescription
    {
        public override short Protocol => 103;

        public byte Time { get; set; }

        public SkillActionDescriptionTimed()
        {
        }

        public SkillActionDescriptionTimed(short skillId, byte time) : base(skillId)
        {
            Time = time;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Time);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Time = reader.ReadByte();
        }
    }
}