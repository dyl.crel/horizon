namespace Horizon.Protocol.Types.Game.Interactive
{
    public class InteractiveElementNamedSkill : InteractiveElementSkill
    {
        public override short Protocol => 220;

        public int NameId { get; set; }

        public InteractiveElementNamedSkill()
        {
        }

        public InteractiveElementNamedSkill(int skillId, int skillInstanceUid, int nameId) : base(skillId, skillInstanceUid)
        {
            NameId = nameId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(NameId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            NameId = reader.ReadVarInt();
        }
    }
}