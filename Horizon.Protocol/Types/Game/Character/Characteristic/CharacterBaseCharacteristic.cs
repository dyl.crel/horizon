namespace Horizon.Protocol.Types.Game.Character.Characteristic
{
    public class CharacterBaseCharacteristic : NetworkType
    {
        public override short Protocol => 4;

        public short Base { get; set; }

        public short Additionnal { get; set; }

        public short ObjectsAndMountBonus { get; set; }

        public short AlignGiftBonus { get; set; }

        public short ContextModif { get; set; }

        public CharacterBaseCharacteristic()
        {
        }

        public CharacterBaseCharacteristic(short @base, short additionnal, short objectsAndMountBonus, short alignGiftBonus, short contextModif)
        {
            Base = @base;
            Additionnal = additionnal;
            ObjectsAndMountBonus = objectsAndMountBonus;
            AlignGiftBonus = alignGiftBonus;
            ContextModif = contextModif;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Base);
            writer.WriteVarShort(Additionnal);
            writer.WriteVarShort(ObjectsAndMountBonus);
            writer.WriteVarShort(AlignGiftBonus);
            writer.WriteVarShort(ContextModif);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Base = reader.ReadVarShort();
            Additionnal = reader.ReadVarShort();
            ObjectsAndMountBonus = reader.ReadVarShort();
            AlignGiftBonus = reader.ReadVarShort();
            ContextModif = reader.ReadVarShort();
        }
    }
}