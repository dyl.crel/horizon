namespace Horizon.Protocol.Types.Game.Character.Characteristic
{
    public class CharacterSpellModification : NetworkType
    {
        public override short Protocol => 215;

        public byte ModificationType { get; set; }

        public short SpellId { get; set; }

        public CharacterBaseCharacteristic Value { get; set; }

        public CharacterSpellModification()
        {
        }

        public CharacterSpellModification(byte modificationType, short spellId, CharacterBaseCharacteristic value)
        {
            ModificationType = modificationType;
            SpellId = spellId;
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(ModificationType);
            writer.WriteVarShort(SpellId);
            Value.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ModificationType = reader.ReadByte();
            SpellId = reader.ReadVarShort();
            Value = new CharacterBaseCharacteristic();
            Value.Deserialize(reader);
        }
    }
}