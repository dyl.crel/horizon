namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterMinimalGuildPublicInformations : CharacterMinimalInformations
    {
        public override short Protocol => 556;

        public int Rank { get; set; }

        public CharacterMinimalGuildPublicInformations()
        {
        }

        public CharacterMinimalGuildPublicInformations(long id, string name, short level, int rank) : base(id, name, level)
        {
            Rank = rank;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Rank);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Rank = reader.ReadVarInt();
        }
    }
}