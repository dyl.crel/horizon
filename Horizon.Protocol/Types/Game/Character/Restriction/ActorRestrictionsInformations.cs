namespace Horizon.Protocol.Types.Game.Character.Restriction
{
    public class ActorRestrictionsInformations : NetworkType
    {
        public override short Protocol => 204;

        public bool CantBeAggressed { get; set; }

        public bool CantBeChallenged { get; set; }

        public bool CantTrade { get; set; }

        public bool CantBeAttackedByMutant { get; set; }

        public bool CantRun { get; set; }

        public bool ForceSlowWalk { get; set; }

        public bool CantMinimize { get; set; }

        public bool CantMove { get; set; }

        public bool CantAggress { get; set; }

        public bool CantChallenge { get; set; }

        public bool CantExchange { get; set; }

        public bool CantAttack { get; set; }

        public bool CantChat { get; set; }

        public bool CantBeMerchant { get; set; }

        public bool CantUseObject { get; set; }

        public bool CantUseTaxCollector { get; set; }

        public bool CantUseInteractive { get; set; }

        public bool CantSpeakToNPC { get; set; }

        public bool CantChangeZone { get; set; }

        public bool CantAttackMonster { get; set; }

        public bool CantWalk8Directions { get; set; }

        public ActorRestrictionsInformations()
        {
        }

        public ActorRestrictionsInformations(bool cantBeAggressed, bool cantBeChallenged, bool cantTrade, bool cantBeAttackedByMutant, bool cantRun, bool forceSlowWalk, bool cantMinimize, bool cantMove, bool cantAggress, bool cantChallenge, bool cantExchange, bool cantAttack, bool cantChat, bool cantBeMerchant, bool cantUseObject, bool cantUseTaxCollector, bool cantUseInteractive, bool cantSpeakToNPC, bool cantChangeZone, bool cantAttackMonster, bool cantWalk8Directions)
        {
            CantBeAggressed = cantBeAggressed;
            CantBeChallenged = cantBeChallenged;
            CantTrade = cantTrade;
            CantBeAttackedByMutant = cantBeAttackedByMutant;
            CantRun = cantRun;
            ForceSlowWalk = forceSlowWalk;
            CantMinimize = cantMinimize;
            CantMove = cantMove;
            CantAggress = cantAggress;
            CantChallenge = cantChallenge;
            CantExchange = cantExchange;
            CantAttack = cantAttack;
            CantChat = cantChat;
            CantBeMerchant = cantBeMerchant;
            CantUseObject = cantUseObject;
            CantUseTaxCollector = cantUseTaxCollector;
            CantUseInteractive = cantUseInteractive;
            CantSpeakToNPC = cantSpeakToNPC;
            CantChangeZone = cantChangeZone;
            CantAttackMonster = cantAttackMonster;
            CantWalk8Directions = cantWalk8Directions;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            byte flag0 = 0;
            flag0 = BooleanByteWrapper.SetFlag(flag0, 0, CantBeAggressed);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 1, CantBeChallenged);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 2, CantTrade);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 3, CantBeAttackedByMutant);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 4, CantRun);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 5, ForceSlowWalk);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 6, CantMinimize);
            flag0 = BooleanByteWrapper.SetFlag(flag0, 7, CantMove);
            writer.WriteByte(flag0);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, CantAggress);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, CantChallenge);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, CantExchange);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, CantAttack);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 4, CantChat);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 5, CantBeMerchant);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 6, CantUseObject);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 7, CantUseTaxCollector);
            writer.WriteByte(flag1);
            byte flag2 = 0;
            flag2 = BooleanByteWrapper.SetFlag(flag2, 0, CantUseInteractive);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 1, CantSpeakToNPC);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 2, CantChangeZone);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 3, CantAttackMonster);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 4, CantWalk8Directions);
            writer.WriteByte(flag2);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            var flag0 = reader.ReadByte();
            CantBeAggressed = BooleanByteWrapper.GetFlag(flag0, 0);
            CantBeChallenged = BooleanByteWrapper.GetFlag(flag0, 1);
            CantTrade = BooleanByteWrapper.GetFlag(flag0, 2);
            CantBeAttackedByMutant = BooleanByteWrapper.GetFlag(flag0, 3);
            CantRun = BooleanByteWrapper.GetFlag(flag0, 4);
            ForceSlowWalk = BooleanByteWrapper.GetFlag(flag0, 5);
            CantMinimize = BooleanByteWrapper.GetFlag(flag0, 6);
            CantMove = BooleanByteWrapper.GetFlag(flag0, 7);
            var flag1 = reader.ReadByte();
            CantAggress = BooleanByteWrapper.GetFlag(flag1, 0);
            CantChallenge = BooleanByteWrapper.GetFlag(flag1, 1);
            CantExchange = BooleanByteWrapper.GetFlag(flag1, 2);
            CantAttack = BooleanByteWrapper.GetFlag(flag1, 3);
            CantChat = BooleanByteWrapper.GetFlag(flag1, 4);
            CantBeMerchant = BooleanByteWrapper.GetFlag(flag1, 5);
            CantUseObject = BooleanByteWrapper.GetFlag(flag1, 6);
            CantUseTaxCollector = BooleanByteWrapper.GetFlag(flag1, 7);
            var flag2 = reader.ReadByte();
            CantUseInteractive = BooleanByteWrapper.GetFlag(flag2, 0);
            CantSpeakToNPC = BooleanByteWrapper.GetFlag(flag2, 1);
            CantChangeZone = BooleanByteWrapper.GetFlag(flag2, 2);
            CantAttackMonster = BooleanByteWrapper.GetFlag(flag2, 3);
            CantWalk8Directions = BooleanByteWrapper.GetFlag(flag2, 4);
        }
    }
}