namespace Horizon.Protocol.Types.Game.Character.Status
{
    public class PlayerStatusExtended : PlayerStatus
    {
        public override short Protocol => 414;

        public string Message { get; set; }

        public PlayerStatusExtended()
        {
        }

        public PlayerStatusExtended(byte statusId, string message) : base(statusId)
        {
            Message = message;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Message);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Message = reader.ReadUTF();
        }
    }
}