namespace Horizon.Protocol.Types.Game.Character.Status
{
    public class PlayerStatus : NetworkType
    {
        public override short Protocol => 415;

        public byte StatusId { get; set; }

        public PlayerStatus()
        {
        }

        public PlayerStatus(byte statusId)
        {
            StatusId = statusId;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(StatusId);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            StatusId = reader.ReadByte();
        }
    }
}