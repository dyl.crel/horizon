using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Character.Choice
{
    public class CharacterHardcoreOrEpicInformations : CharacterBaseInformations
    {
        public override short Protocol => 474;

        public byte DeathState { get; set; }

        public short DeathCount { get; set; }

        public short DeathMaxLevel { get; set; }

        public CharacterHardcoreOrEpicInformations()
        {
        }

        public CharacterHardcoreOrEpicInformations(long id, string name, short level, EntityLook entityLook, byte breed, bool sex, byte deathState, short deathCount, short deathMaxLevel) : base(id, name, level, entityLook, breed, sex)
        {
            DeathState = deathState;
            DeathCount = deathCount;
            DeathMaxLevel = deathMaxLevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(DeathState);
            writer.WriteVarShort(DeathCount);
            writer.WriteVarShort(DeathMaxLevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            DeathState = reader.ReadByte();
            DeathCount = reader.ReadVarShort();
            DeathMaxLevel = reader.ReadVarShort();
        }
    }
}