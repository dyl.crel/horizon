namespace Horizon.Protocol.Types.Game.Character.Choice
{
    public class CharacterToRemodelInformations : CharacterRemodelingInformation
    {
        public override short Protocol => 477;

        public byte PossibleChangeMask { get; set; }

        public byte MandatoryChangeMask { get; set; }

        public CharacterToRemodelInformations()
        {
        }

        public CharacterToRemodelInformations(long id, string name, byte breed, bool sex, short cosmeticId, int[] colors, byte possibleChangeMask, byte mandatoryChangeMask) : base(id, name, breed, sex, cosmeticId, colors)
        {
            PossibleChangeMask = possibleChangeMask;
            MandatoryChangeMask = mandatoryChangeMask;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(PossibleChangeMask);
            writer.WriteByte(MandatoryChangeMask);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PossibleChangeMask = reader.ReadByte();
            MandatoryChangeMask = reader.ReadByte();
        }
    }
}