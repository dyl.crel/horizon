namespace Horizon.Protocol.Types.Game.Character.Choice
{
    public class RemodelingInformation : NetworkType
    {
        public override short Protocol => 480;

        public string Name { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public short CosmeticId { get; set; }

        public int[] Colors { get; set; }

        public RemodelingInformation()
        {
        }

        public RemodelingInformation(string name, byte breed, bool sex, short cosmeticId, int[] colors)
        {
            Name = name;
            Breed = breed;
            Sex = sex;
            CosmeticId = cosmeticId;
            Colors = colors;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(CosmeticId);
            writer.WriteShort(Colors.Length);
            for (var i = 0; i < Colors.Length; i++)
            {
                writer.WriteInt(Colors[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Name = reader.ReadUTF();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            CosmeticId = reader.ReadVarShort();
            Colors = new int[reader.ReadShort()];
            for (var i = 0; i < Colors.Length; i++)
            {
                Colors[i] = reader.ReadInt();
            }
        }
    }
}