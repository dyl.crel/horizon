using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Character.Choice
{
    public class CharacterBaseInformations : CharacterMinimalPlusLookInformations
    {
        public override short Protocol => 45;

        public bool Sex { get; set; }

        public CharacterBaseInformations()
        {
        }

        public CharacterBaseInformations(long id, string name, short level, EntityLook entityLook, byte breed, bool sex) : base(id, name, level, entityLook, breed)
        {
            Sex = sex;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Sex);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Sex = reader.ReadBoolean();
        }
    }
}