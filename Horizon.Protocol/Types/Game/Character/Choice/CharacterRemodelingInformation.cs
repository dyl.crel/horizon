namespace Horizon.Protocol.Types.Game.Character.Choice
{
    public class CharacterRemodelingInformation : AbstractCharacterInformation
    {
        public override short Protocol => 479;

        public string Name { get; set; }

        public byte Breed { get; set; }

        public bool Sex { get; set; }

        public short CosmeticId { get; set; }

        public int[] Colors { get; set; }

        public CharacterRemodelingInformation()
        {
        }

        public CharacterRemodelingInformation(long id, string name, byte breed, bool sex, short cosmeticId, int[] colors) : base(id)
        {
            Name = name;
            Breed = breed;
            Sex = sex;
            CosmeticId = cosmeticId;
            Colors = colors;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            writer.WriteByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(CosmeticId);
            writer.WriteShort(Colors.Length);
            for (var i = 0; i < Colors.Length; i++)
            {
                writer.WriteInt(Colors[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Breed = reader.ReadByte();
            Sex = reader.ReadBoolean();
            CosmeticId = reader.ReadVarShort();
            Colors = new int[reader.ReadShort()];
            for (var i = 0; i < Colors.Length; i++)
            {
                Colors[i] = reader.ReadInt();
            }
        }
    }
}