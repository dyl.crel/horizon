using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterMinimalPlusLookInformations : CharacterMinimalInformations
    {
        public override short Protocol => 163;

        public EntityLook EntityLook { get; set; }

        public byte Breed { get; set; }

        public CharacterMinimalPlusLookInformations()
        {
        }

        public CharacterMinimalPlusLookInformations(long id, string name, short level, EntityLook entityLook, byte breed) : base(id, name, level)
        {
            EntityLook = entityLook;
            Breed = breed;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            EntityLook.Serialize(writer);
            writer.WriteByte(Breed);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            EntityLook = new EntityLook();
            EntityLook.Deserialize(reader);
            Breed = reader.ReadByte();
        }
    }
}