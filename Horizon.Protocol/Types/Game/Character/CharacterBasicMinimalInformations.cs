namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterBasicMinimalInformations : AbstractCharacterInformation
    {
        public override short Protocol => 503;

        public string Name { get; set; }

        public CharacterBasicMinimalInformations()
        {
        }

        public CharacterBasicMinimalInformations(long id, string name) : base(id)
        {
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
        }
    }
}