namespace Horizon.Protocol.Types.Game.Character
{
    public class AbstractCharacterInformation : NetworkType
    {
        public override short Protocol => 400;

        public long Id { get; set; }

        public AbstractCharacterInformation()
        {
        }

        public AbstractCharacterInformation(long id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarLong();
        }
    }
}