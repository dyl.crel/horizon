namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterMinimalInformations : CharacterBasicMinimalInformations
    {
        public override short Protocol => 110;

        public short Level { get; set; }

        public CharacterMinimalInformations()
        {
        }

        public CharacterMinimalInformations(long id, string name, short level) : base(id, name)
        {
            Level = level;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarShort();
        }
    }
}