using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterMinimalAllianceInformations : CharacterMinimalGuildInformations
    {
        public override short Protocol => 444;

        public BasicAllianceInformations Alliance { get; set; }

        public CharacterMinimalAllianceInformations()
        {
        }

        public CharacterMinimalAllianceInformations(long id, string name, short level, EntityLook entityLook, byte breed, BasicGuildInformations guild, BasicAllianceInformations alliance) : base(id, name, level, entityLook, breed, guild)
        {
            Alliance = alliance;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Alliance.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Alliance = new BasicAllianceInformations();
            Alliance.Deserialize(reader);
        }
    }
}