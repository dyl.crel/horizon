using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterMinimalPlusLookAndGradeInformations : CharacterMinimalPlusLookInformations
    {
        public override short Protocol => 193;

        public int Grade { get; set; }

        public CharacterMinimalPlusLookAndGradeInformations()
        {
        }

        public CharacterMinimalPlusLookAndGradeInformations(long id, string name, short level, EntityLook entityLook, byte breed, int grade) : base(id, name, level, entityLook, breed)
        {
            Grade = grade;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Grade);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Grade = reader.ReadVarInt();
        }
    }
}