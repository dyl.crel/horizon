using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Character
{
    public class CharacterMinimalGuildInformations : CharacterMinimalPlusLookInformations
    {
        public override short Protocol => 445;

        public BasicGuildInformations Guild { get; set; }

        public CharacterMinimalGuildInformations()
        {
        }

        public CharacterMinimalGuildInformations(long id, string name, short level, EntityLook entityLook, byte breed, BasicGuildInformations guild) : base(id, name, level, entityLook, breed)
        {
            Guild = guild;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Guild.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Guild = new BasicGuildInformations();
            Guild.Deserialize(reader);
        }
    }
}