namespace Horizon.Protocol.Types.Game.Character.Alignment
{
    public class ActorAlignmentInformations : NetworkType
    {
        public override short Protocol => 201;

        public byte AlignmentSide { get; set; }

        public byte AlignmentValue { get; set; }

        public byte AlignmentGrade { get; set; }

        public double CharacterPower { get; set; }

        public ActorAlignmentInformations()
        {
        }

        public ActorAlignmentInformations(byte alignmentSide, byte alignmentValue, byte alignmentGrade, double characterPower)
        {
            AlignmentSide = alignmentSide;
            AlignmentValue = alignmentValue;
            AlignmentGrade = alignmentGrade;
            CharacterPower = characterPower;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(AlignmentSide);
            writer.WriteByte(AlignmentValue);
            writer.WriteByte(AlignmentGrade);
            writer.WriteDouble(CharacterPower);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            AlignmentSide = reader.ReadByte();
            AlignmentValue = reader.ReadByte();
            AlignmentGrade = reader.ReadByte();
            CharacterPower = reader.ReadDouble();
        }
    }
}