namespace Horizon.Protocol.Types.Game.Character.Alignment
{
    public class ActorExtendedAlignmentInformations : ActorAlignmentInformations
    {
        public override short Protocol => 202;

        public short Honor { get; set; }

        public short HonorGradeFloor { get; set; }

        public short HonorNextGradeFloor { get; set; }

        public byte Aggressable { get; set; }

        public ActorExtendedAlignmentInformations()
        {
        }

        public ActorExtendedAlignmentInformations(byte alignmentSide, byte alignmentValue, byte alignmentGrade, double characterPower, short honor, short honorGradeFloor, short honorNextGradeFloor, byte aggressable) : base(alignmentSide, alignmentValue, alignmentGrade, characterPower)
        {
            Honor = honor;
            HonorGradeFloor = honorGradeFloor;
            HonorNextGradeFloor = honorNextGradeFloor;
            Aggressable = aggressable;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Honor);
            writer.WriteVarShort(HonorGradeFloor);
            writer.WriteVarShort(HonorNextGradeFloor);
            writer.WriteByte(Aggressable);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Honor = reader.ReadVarShort();
            HonorGradeFloor = reader.ReadVarShort();
            HonorNextGradeFloor = reader.ReadVarShort();
            Aggressable = reader.ReadByte();
        }
    }
}