namespace Horizon.Protocol.Types.Game.Achievement
{
    public class AchievementAchievedRewardable : AchievementAchieved
    {
        public override short Protocol => 515;

        public short Finishedlevel { get; set; }

        public AchievementAchievedRewardable()
        {
        }

        public AchievementAchievedRewardable(short id, long achievedBy, short finishedlevel) : base(id, achievedBy)
        {
            Finishedlevel = finishedlevel;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Finishedlevel);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Finishedlevel = reader.ReadVarShort();
        }
    }
}