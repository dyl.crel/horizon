namespace Horizon.Protocol.Types.Game.Achievement
{
    public class AchievementStartedObjective : AchievementObjective
    {
        public override short Protocol => 402;

        public short Value { get; set; }

        public AchievementStartedObjective()
        {
        }

        public AchievementStartedObjective(int id, short maxValue, short value) : base(id, maxValue)
        {
            Value = value;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Value);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadVarShort();
        }
    }
}