namespace Horizon.Protocol.Types.Game.Achievement
{
    public class Achievement : NetworkType
    {
        public override short Protocol => 363;

        public short Id { get; set; }

        public AchievementObjective[] FinishedObjective { get; set; }

        public AchievementStartedObjective[] StartedObjectives { get; set; }

        public Achievement()
        {
        }

        public Achievement(short id, AchievementObjective[] finishedObjective, AchievementStartedObjective[] startedObjectives)
        {
            Id = id;
            FinishedObjective = finishedObjective;
            StartedObjectives = startedObjectives;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteShort(FinishedObjective.Length);
            for (var i = 0; i < FinishedObjective.Length; i++)
            {
                FinishedObjective[i].Serialize(writer);
            }
            writer.WriteShort(StartedObjectives.Length);
            for (var i = 0; i < StartedObjectives.Length; i++)
            {
                StartedObjectives[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
            FinishedObjective = new AchievementObjective[reader.ReadShort()];
            for (var i = 0; i < FinishedObjective.Length; i++)
            {
                FinishedObjective[i] = new AchievementObjective();
                FinishedObjective[i].Deserialize(reader);
            }
            StartedObjectives = new AchievementStartedObjective[reader.ReadShort()];
            for (var i = 0; i < StartedObjectives.Length; i++)
            {
                StartedObjectives[i] = new AchievementStartedObjective();
                StartedObjectives[i].Deserialize(reader);
            }
        }
    }
}