namespace Horizon.Protocol.Types.Game.Achievement
{
    public class AchievementObjective : NetworkType
    {
        public override short Protocol => 404;

        public int Id { get; set; }

        public short MaxValue { get; set; }

        public AchievementObjective()
        {
        }

        public AchievementObjective(int id, short maxValue)
        {
            Id = id;
            MaxValue = maxValue;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarInt(Id);
            writer.WriteVarShort(MaxValue);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarInt();
            MaxValue = reader.ReadVarShort();
        }
    }
}