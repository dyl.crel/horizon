namespace Horizon.Protocol.Types.Game.Achievement
{
    public class AchievementAchieved : NetworkType
    {
        public override short Protocol => 514;

        public short Id { get; set; }

        public long AchievedBy { get; set; }

        public AchievementAchieved()
        {
        }

        public AchievementAchieved(short id, long achievedBy)
        {
            Id = id;
            AchievedBy = achievedBy;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteVarLong(AchievedBy);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadVarShort();
            AchievedBy = reader.ReadVarLong();
        }
    }
}