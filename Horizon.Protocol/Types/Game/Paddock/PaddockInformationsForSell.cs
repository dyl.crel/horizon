namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockInformationsForSell : NetworkType
    {
        public override short Protocol => 222;

        public string GuildOwner { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public short SubAreaId { get; set; }

        public byte NbMount { get; set; }

        public byte NbObject { get; set; }

        public long Price { get; set; }

        public PaddockInformationsForSell()
        {
        }

        public PaddockInformationsForSell(string guildOwner, short worldX, short worldY, short subAreaId, byte nbMount, byte nbObject, long price)
        {
            GuildOwner = guildOwner;
            WorldX = worldX;
            WorldY = worldY;
            SubAreaId = subAreaId;
            NbMount = nbMount;
            NbObject = nbObject;
            Price = price;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(GuildOwner);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteVarShort(SubAreaId);
            writer.WriteByte(NbMount);
            writer.WriteByte(NbObject);
            writer.WriteVarLong(Price);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            GuildOwner = reader.ReadUTF();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            SubAreaId = reader.ReadVarShort();
            NbMount = reader.ReadByte();
            NbObject = reader.ReadByte();
            Price = reader.ReadVarLong();
        }
    }
}