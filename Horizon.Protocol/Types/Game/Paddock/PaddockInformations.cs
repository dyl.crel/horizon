namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockInformations : NetworkType
    {
        public override short Protocol => 132;

        public short MaxOutdoorMount { get; set; }

        public short MaxItems { get; set; }

        public PaddockInformations()
        {
        }

        public PaddockInformations(short maxOutdoorMount, short maxItems)
        {
            MaxOutdoorMount = maxOutdoorMount;
            MaxItems = maxItems;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(MaxOutdoorMount);
            writer.WriteVarShort(MaxItems);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            MaxOutdoorMount = reader.ReadVarShort();
            MaxItems = reader.ReadVarShort();
        }
    }
}