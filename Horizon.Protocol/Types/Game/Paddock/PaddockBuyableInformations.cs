namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockBuyableInformations : NetworkType
    {
        public override short Protocol => 130;

        public long Price { get; set; }

        public bool Locked { get; set; }

        public PaddockBuyableInformations()
        {
        }

        public PaddockBuyableInformations(long price, bool locked)
        {
            Price = price;
            Locked = locked;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarLong(Price);
            writer.WriteBoolean(Locked);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Price = reader.ReadVarLong();
            Locked = reader.ReadBoolean();
        }
    }
}