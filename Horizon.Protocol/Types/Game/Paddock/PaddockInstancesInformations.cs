namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockInstancesInformations : PaddockInformations
    {
        public override short Protocol => 509;

        public PaddockBuyableInformations[] Paddocks { get; set; }

        public PaddockInstancesInformations()
        {
        }

        public PaddockInstancesInformations(short maxOutdoorMount, short maxItems, PaddockBuyableInformations[] paddocks) : base(maxOutdoorMount, maxItems)
        {
            Paddocks = paddocks;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Paddocks.Length);
            for (var i = 0; i < Paddocks.Length; i++)
            {
                writer.WriteShort(Paddocks[i].Protocol);
                Paddocks[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Paddocks = new PaddockBuyableInformations[reader.ReadShort()];
            for (var i = 0; i < Paddocks.Length; i++)
            {
                Paddocks[i] = ProtocolTypesManager.Instance<PaddockBuyableInformations>(reader.ReadShort());
                Paddocks[i].Deserialize(reader);
            }
        }
    }
}