namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockContentInformations : PaddockInformations
    {
        public override short Protocol => 183;

        public double PaddockId { get; set; }

        public short WorldX { get; set; }

        public short WorldY { get; set; }

        public double MapId { get; set; }

        public short SubAreaId { get; set; }

        public bool Abandonned { get; set; }

        public MountInformationsForPaddock[] MountsInformations { get; set; }

        public PaddockContentInformations()
        {
        }

        public PaddockContentInformations(short maxOutdoorMount, short maxItems, double paddockId, short worldX, short worldY, double mapId, short subAreaId, bool abandonned, MountInformationsForPaddock[] mountsInformations) : base(maxOutdoorMount, maxItems)
        {
            PaddockId = paddockId;
            WorldX = worldX;
            WorldY = worldY;
            MapId = mapId;
            SubAreaId = subAreaId;
            Abandonned = abandonned;
            MountsInformations = mountsInformations;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(PaddockId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteBoolean(Abandonned);
            writer.WriteShort(MountsInformations.Length);
            for (var i = 0; i < MountsInformations.Length; i++)
            {
                MountsInformations[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            PaddockId = reader.ReadDouble();
            WorldX = reader.ReadShort();
            WorldY = reader.ReadShort();
            MapId = reader.ReadDouble();
            SubAreaId = reader.ReadVarShort();
            Abandonned = reader.ReadBoolean();
            MountsInformations = new MountInformationsForPaddock[reader.ReadShort()];
            for (var i = 0; i < MountsInformations.Length; i++)
            {
                MountsInformations[i] = new MountInformationsForPaddock();
                MountsInformations[i].Deserialize(reader);
            }
        }
    }
}