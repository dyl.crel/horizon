namespace Horizon.Protocol.Types.Game.Paddock
{
    public class MountInformationsForPaddock : NetworkType
    {
        public override short Protocol => 184;

        public short ModelId { get; set; }

        public string Name { get; set; }

        public string OwnerName { get; set; }

        public MountInformationsForPaddock()
        {
        }

        public MountInformationsForPaddock(short modelId, string name, string ownerName)
        {
            ModelId = modelId;
            Name = name;
            OwnerName = ownerName;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(ModelId);
            writer.WriteUTF(Name);
            writer.WriteUTF(OwnerName);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            ModelId = reader.ReadVarShort();
            Name = reader.ReadUTF();
            OwnerName = reader.ReadUTF();
        }
    }
}