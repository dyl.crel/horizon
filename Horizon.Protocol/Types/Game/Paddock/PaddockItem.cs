using Horizon.Protocol.Types.Game.Context.Roleplay;
using Horizon.Protocol.Types.Game.Mount;

namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockItem : ObjectItemInRolePlay
    {
        public override short Protocol => 185;

        public ItemDurability Durability { get; set; }

        public PaddockItem()
        {
        }

        public PaddockItem(short cellId, short objectGID, ItemDurability durability) : base(cellId, objectGID)
        {
            Durability = durability;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            Durability.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Durability = new ItemDurability();
            Durability.Deserialize(reader);
        }
    }
}