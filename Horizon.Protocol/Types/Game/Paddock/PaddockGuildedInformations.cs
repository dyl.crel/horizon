using Horizon.Protocol.Types.Game.Context.Roleplay;

namespace Horizon.Protocol.Types.Game.Paddock
{
    public class PaddockGuildedInformations : PaddockBuyableInformations
    {
        public override short Protocol => 508;

        public bool Deserted { get; set; }

        public GuildInformations GuildInfo { get; set; }

        public PaddockGuildedInformations()
        {
        }

        public PaddockGuildedInformations(long price, bool locked, bool deserted, GuildInformations guildInfo) : base(price, locked)
        {
            Deserted = deserted;
            GuildInfo = guildInfo;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Deserted);
            GuildInfo.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Deserted = reader.ReadBoolean();
            GuildInfo = new GuildInformations();
            GuildInfo.Deserialize(reader);
        }
    }
}