namespace Horizon.Protocol.Types.Game.Presets
{
    public class CharacterBuildPreset : PresetsContainerPreset
    {
        public override short Protocol => 534;

        public short IconId { get; set; }

        public string Name { get; set; }

        public CharacterBuildPreset()
        {
        }

        public CharacterBuildPreset(short id, Preset[] presets, short iconId, string name) : base(id, presets)
        {
            IconId = iconId;
            Name = name;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(IconId);
            writer.WriteUTF(Name);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            IconId = reader.ReadShort();
            Name = reader.ReadUTF();
        }
    }
}