namespace Horizon.Protocol.Types.Game.Presets
{
    public class ItemForPreset : NetworkType
    {
        public override short Protocol => 540;

        public short Position { get; set; }

        public short ObjGid { get; set; }

        public int ObjUid { get; set; }

        public ItemForPreset()
        {
        }

        public ItemForPreset(short position, short objGid, int objUid)
        {
            Position = position;
            ObjGid = objGid;
            ObjUid = objUid;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Position);
            writer.WriteVarShort(ObjGid);
            writer.WriteVarInt(ObjUid);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Position = reader.ReadShort();
            ObjGid = reader.ReadVarShort();
            ObjUid = reader.ReadVarInt();
        }
    }
}