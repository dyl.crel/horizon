namespace Horizon.Protocol.Types.Game.Presets
{
    public class CharacterCharacteristicForPreset : SimpleCharacterCharacteristicForPreset
    {
        public override short Protocol => 539;

        public short Stuff { get; set; }

        public CharacterCharacteristicForPreset()
        {
        }

        public CharacterCharacteristicForPreset(string keyword, short @base, short additionnal, short stuff) : base(keyword, @base, additionnal)
        {
            Stuff = stuff;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Stuff);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Stuff = reader.ReadVarShort();
        }
    }
}