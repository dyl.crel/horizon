using Horizon.Protocol.Types.Game.Look;

namespace Horizon.Protocol.Types.Game.Presets
{
    public class ItemsPreset : Preset
    {
        public override short Protocol => 517;

        public ItemForPreset[] Items { get; set; }

        public bool MountEquipped { get; set; }

        public EntityLook Look { get; set; }

        public ItemsPreset()
        {
        }

        public ItemsPreset(short id, ItemForPreset[] items, bool mountEquipped, EntityLook look) : base(id)
        {
            Items = items;
            MountEquipped = mountEquipped;
            Look = look;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Items.Length);
            for (var i = 0; i < Items.Length; i++)
            {
                Items[i].Serialize(writer);
            }
            writer.WriteBoolean(MountEquipped);
            Look.Serialize(writer);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Items = new ItemForPreset[reader.ReadShort()];
            for (var i = 0; i < Items.Length; i++)
            {
                Items[i] = new ItemForPreset();
                Items[i].Deserialize(reader);
            }
            MountEquipped = reader.ReadBoolean();
            Look = new EntityLook();
            Look.Deserialize(reader);
        }
    }
}