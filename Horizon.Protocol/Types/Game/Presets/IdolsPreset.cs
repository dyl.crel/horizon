namespace Horizon.Protocol.Types.Game.Presets
{
    public class IdolsPreset : Preset
    {
        public override short Protocol => 491;

        public short IconId { get; set; }

        public short[] IdolIds { get; set; }

        public IdolsPreset()
        {
        }

        public IdolsPreset(short id, short iconId, short[] idolIds) : base(id)
        {
            IconId = iconId;
            IdolIds = idolIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(IconId);
            writer.WriteShort(IdolIds.Length);
            for (var i = 0; i < IdolIds.Length; i++)
            {
                writer.WriteVarShort(IdolIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            IconId = reader.ReadShort();
            IdolIds = new short[reader.ReadShort()];
            for (var i = 0; i < IdolIds.Length; i++)
            {
                IdolIds[i] = reader.ReadVarShort();
            }
        }
    }
}