namespace Horizon.Protocol.Types.Game.Presets
{
    public class Preset : NetworkType
    {
        public override short Protocol => 355;

        public short Id { get; set; }

        public Preset()
        {
        }

        public Preset(short id)
        {
            Id = id;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteShort(Id);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadShort();
        }
    }
}