namespace Horizon.Protocol.Types.Game.Presets
{
    public class SpellsPreset : Preset
    {
        public override short Protocol => 519;

        public SpellForPreset[] Spells { get; set; }

        public SpellsPreset()
        {
        }

        public SpellsPreset(short id, SpellForPreset[] spells) : base(id)
        {
            Spells = spells;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Spells.Length);
            for (var i = 0; i < Spells.Length; i++)
            {
                Spells[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Spells = new SpellForPreset[reader.ReadShort()];
            for (var i = 0; i < Spells.Length; i++)
            {
                Spells[i] = new SpellForPreset();
                Spells[i].Deserialize(reader);
            }
        }
    }
}