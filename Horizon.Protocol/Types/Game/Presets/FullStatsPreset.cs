namespace Horizon.Protocol.Types.Game.Presets
{
    public class FullStatsPreset : Preset
    {
        public override short Protocol => 532;

        public CharacterCharacteristicForPreset[] Stats { get; set; }

        public FullStatsPreset()
        {
        }

        public FullStatsPreset(short id, CharacterCharacteristicForPreset[] stats) : base(id)
        {
            Stats = stats;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Stats.Length);
            for (var i = 0; i < Stats.Length; i++)
            {
                Stats[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Stats = new CharacterCharacteristicForPreset[reader.ReadShort()];
            for (var i = 0; i < Stats.Length; i++)
            {
                Stats[i] = new CharacterCharacteristicForPreset();
                Stats[i].Deserialize(reader);
            }
        }
    }
}