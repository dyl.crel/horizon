namespace Horizon.Protocol.Types.Game.Presets
{
    public class StatsPreset : Preset
    {
        public override short Protocol => 521;

        public SimpleCharacterCharacteristicForPreset[] Stats { get; set; }

        public StatsPreset()
        {
        }

        public StatsPreset(short id, SimpleCharacterCharacteristicForPreset[] stats) : base(id)
        {
            Stats = stats;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Stats.Length);
            for (var i = 0; i < Stats.Length; i++)
            {
                Stats[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Stats = new SimpleCharacterCharacteristicForPreset[reader.ReadShort()];
            for (var i = 0; i < Stats.Length; i++)
            {
                Stats[i] = new SimpleCharacterCharacteristicForPreset();
                Stats[i].Deserialize(reader);
            }
        }
    }
}