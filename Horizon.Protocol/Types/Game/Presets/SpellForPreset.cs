namespace Horizon.Protocol.Types.Game.Presets
{
    public class SpellForPreset : NetworkType
    {
        public override short Protocol => 557;

        public short SpellId { get; set; }

        public short[] Shortcuts { get; set; }

        public SpellForPreset()
        {
        }

        public SpellForPreset(short spellId, short[] shortcuts)
        {
            SpellId = spellId;
            Shortcuts = shortcuts;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteShort(Shortcuts.Length);
            for (var i = 0; i < Shortcuts.Length; i++)
            {
                writer.WriteShort(Shortcuts[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            SpellId = reader.ReadVarShort();
            Shortcuts = new short[reader.ReadShort()];
            for (var i = 0; i < Shortcuts.Length; i++)
            {
                Shortcuts[i] = reader.ReadShort();
            }
        }
    }
}