namespace Horizon.Protocol.Types.Game.Presets
{
    public class EntitiesPreset : Preset
    {
        public override short Protocol => 545;

        public short IconId { get; set; }

        public short[] EntityIds { get; set; }

        public EntitiesPreset()
        {
        }

        public EntitiesPreset(short id, short iconId, short[] entityIds) : base(id)
        {
            IconId = iconId;
            EntityIds = entityIds;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(IconId);
            writer.WriteShort(EntityIds.Length);
            for (var i = 0; i < EntityIds.Length; i++)
            {
                writer.WriteVarShort(EntityIds[i]);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            IconId = reader.ReadShort();
            EntityIds = new short[reader.ReadShort()];
            for (var i = 0; i < EntityIds.Length; i++)
            {
                EntityIds[i] = reader.ReadVarShort();
            }
        }
    }
}