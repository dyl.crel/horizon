namespace Horizon.Protocol.Types.Game.Presets
{
    public class SimpleCharacterCharacteristicForPreset : NetworkType
    {
        public override short Protocol => 541;

        public string Keyword { get; set; }

        public short Base { get; set; }

        public short Additionnal { get; set; }

        public SimpleCharacterCharacteristicForPreset()
        {
        }

        public SimpleCharacterCharacteristicForPreset(string keyword, short @base, short additionnal)
        {
            Keyword = keyword;
            Base = @base;
            Additionnal = additionnal;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteUTF(Keyword);
            writer.WriteVarShort(Base);
            writer.WriteVarShort(Additionnal);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Keyword = reader.ReadUTF();
            Base = reader.ReadVarShort();
            Additionnal = reader.ReadVarShort();
        }
    }
}