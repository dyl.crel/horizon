namespace Horizon.Protocol.Types.Game.Presets
{
    public class PresetsContainerPreset : Preset
    {
        public override short Protocol => 520;

        public Preset[] Presets { get; set; }

        public PresetsContainerPreset()
        {
        }

        public PresetsContainerPreset(short id, Preset[] presets) : base(id)
        {
            Presets = presets;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Presets.Length);
            for (var i = 0; i < Presets.Length; i++)
            {
                writer.WriteShort(Presets[i].Protocol);
                Presets[i].Serialize(writer);
            }
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Presets = new Preset[reader.ReadShort()];
            for (var i = 0; i < Presets.Length; i++)
            {
                Presets[i] = ProtocolTypesManager.Instance<Preset>(reader.ReadShort());
                Presets[i].Deserialize(reader);
            }
        }
    }
}