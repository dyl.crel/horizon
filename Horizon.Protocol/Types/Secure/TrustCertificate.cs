namespace Horizon.Protocol.Types.Secure
{
    public class TrustCertificate : NetworkType
    {
        public override short Protocol => 377;

        public int Id { get; set; }

        public string Hash { get; set; }

        public TrustCertificate()
        {
        }

        public TrustCertificate(int id, string hash)
        {
            Id = id;
            Hash = hash;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteInt(Id);
            writer.WriteUTF(Hash);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Id = reader.ReadInt();
            Hash = reader.ReadUTF();
        }
    }
}