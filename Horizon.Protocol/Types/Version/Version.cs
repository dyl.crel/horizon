namespace Horizon.Protocol.Types.Version
{
    public class Version : NetworkType
    {
        public override short Protocol => 11;

        public byte Major { get; set; }

        public byte Minor { get; set; }

        public byte Release { get; set; }

        public int Revision { get; set; }

        public byte Patch { get; set; }

        public byte BuildType { get; set; }

        public Version()
        {
        }

        public Version(byte major, byte minor, byte release, int revision, byte patch, byte buildType)
        {
            Major = major;
            Minor = minor;
            Release = release;
            Revision = revision;
            Patch = patch;
            BuildType = buildType;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            writer.WriteByte(Major);
            writer.WriteByte(Minor);
            writer.WriteByte(Release);
            writer.WriteInt(Revision);
            writer.WriteByte(Patch);
            writer.WriteByte(BuildType);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            Major = reader.ReadByte();
            Minor = reader.ReadByte();
            Release = reader.ReadByte();
            Revision = reader.ReadInt();
            Patch = reader.ReadByte();
            BuildType = reader.ReadByte();
        }
    }
}