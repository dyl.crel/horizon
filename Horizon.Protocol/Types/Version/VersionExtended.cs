namespace Horizon.Protocol.Types.Version
{
    public class VersionExtended : Version
    {
        public override short Protocol => 393;

        public byte Install { get; set; }

        public byte Technology { get; set; }

        public VersionExtended()
        {
        }

        public VersionExtended(byte major, byte minor, byte release, int revision, byte patch, byte buildType, byte install, byte technology) : base(major, minor, release, revision, patch, buildType)
        {
            Install = install;
            Technology = technology;
        }

        public override void Serialize(BigEndianWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Install);
            writer.WriteByte(Technology);
        }

        public override void Deserialize(BigEndianReader reader)
        {
            base.Deserialize(reader);
            Install = reader.ReadByte();
            Technology = reader.ReadByte();
        }
    }
}