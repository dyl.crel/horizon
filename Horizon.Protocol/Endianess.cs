﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace Horizon.Protocol
{
    internal sealed class FinalInt64
    {
        public long Low { get; set; }

        public long High { get; set; }

        public FinalInt64()
        {
        }

        public FinalInt64(long low, long high)
        {
            Low = low;
            High = high;
        }

        public static FinalInt64 FromNumber(long Long)
        {
            return new FinalInt64(Long, (long)Math.Floor(Long / 4294967296.0));
        }

        public long ToNumber()
        {
            return High * 4294967296 + Low;
        }
    }

    internal static class BooleanByteWrapper
    {
        public static byte SetFlag(byte flag, byte offset, bool value)
        {
            return value ? (byte)(flag | (1 << offset)) : (byte)(flag & 255 - (1 << offset));
        }

        public static bool GetFlag(byte flag, byte offset)
        {
            return (flag & (byte)(1 << offset)) != 0;
        }
    }

    internal static class ProtocolTypesManager
    {
        private static readonly List<NetworkType> Data = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsSubclassOf(typeof(NetworkType))).Select(x => (NetworkType)Activator.CreateInstance(x)).ToList();

        public static T Instance<T>(short e) where T : NetworkType
        {
            return (T)Activator.CreateInstance(Data.First(x => x.Protocol == e).GetType());
        }
    }

    internal static unsafe class Reinterpret
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long DoubleAsInt64(double value)
            => *(long*)&value;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double Int64AsDouble(long value)
            => *(double*)&value;
    }

    internal static unsafe class BigEndian
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static short ReadInt16(byte* p)
        {
            return (short)(p[0] << 8 | p[1]);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ReadInt32(byte* p)
        {
            return p[0] << 24 | p[1] << 16 | p[2] << 8 | p[3];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long ReadInt64(byte* p)
        {
            var lo = ReadInt32(p);
            var hi = ReadInt32(p + 4);
            return (long)lo << 32 | (uint)hi;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteInt16(byte* p, short value)
        {
            p[0] = (byte)(value >> 8);
            p[1] = (byte)value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteInt32(byte* p, int value)
        {
            p[0] = (byte)(value >> 24);
            p[1] = (byte)(value >> 16);
            p[2] = (byte)(value >> 8);
            p[3] = (byte)value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteInt64(byte* p, long value)
        {
            p[0] = (byte)(value >> 56);
            p[1] = (byte)(value >> 48);
            p[2] = (byte)(value >> 40);
            p[3] = (byte)(value >> 32);
            p[4] = (byte)(value >> 24);
            p[5] = (byte)(value >> 16);
            p[6] = (byte)(value >> 8);
            p[7] = (byte)value;
        }
    }

    public sealed unsafe class BigEndianReader : BinaryReader
    {
        private readonly byte[] _buffer;

        public byte[] Data
        {
            get
            {
                var stream = (MemoryStream)BaseStream;
                return stream.ToArray();
            }
        }

        public BigEndianReader() : base(new MemoryStream(), Encoding.UTF8)
        {
            _buffer = new byte[16];
        }

        public BigEndianReader(byte[] data) : base(new MemoryStream(data), Encoding.UTF8)
        {
            _buffer = new byte[16];
        }

        public BigEndianReader(Stream stream) : base(stream, Encoding.UTF8)
        {
            _buffer = new byte[16];
        }

        public int ReadInt()
        {
            FillBuffer(4);
            fixed (byte* p = _buffer)
                return BigEndian.ReadInt32(p);
        }

        public string ReadUTF()
        {
            return Encoding.UTF8.GetString(ReadBytes(ReadShort()));
        }

        public short ReadShort()
        {
            FillBuffer(2);
            fixed (byte* p = _buffer)
                return BigEndian.ReadInt16(p);
        }

        public override bool ReadBoolean()
        {
            return ReadByte() == 1;
        }

        public override double ReadDouble()
        {
            FillBuffer(8);
            fixed (byte* p = _buffer)
                return Reinterpret.Int64AsDouble(BigEndian.ReadInt64(p));
        }

        public float ReadFloat()
        {
            throw new NotImplementedException();
        }

        public int ReadVarInt()
        {
            var loc1 = 0;
            var loc2 = 0;

            while (loc2 < 32)
            {
                var loc4 = ReadByte();
                var loc3 = (loc4 & 128) == 128;
                if (loc2 > 0)
                {
                    loc1 = loc1 + ((loc4 & 127) << loc2);
                }
                else
                {
                    loc1 = loc1 + (loc4 & 127);
                }

                loc2 = loc2 + 7;
                if (!loc3)
                {
                    return loc1;
                }
            }

            throw new InvalidDataException();
        }

        public short ReadVarShort()
        {
            var loc1 = 0;
            var loc2 = 0;

            while (loc2 < 16)
            {
                int loc4 = ReadByte();

                var loc3 = (loc4 & 128) == 128;

                if (loc2 > 0)
                {
                    loc1 = loc1 + ((loc4 & 127) << loc2);
                }
                else
                {
                    loc1 = loc1 + (loc4 & 127);
                }

                loc2 = loc2 + 7;

                if (loc3) continue;

                if (loc1 > 32767)
                {
                    loc1 = loc1 - 65536;
                }

                return (short)loc1;
            }

            throw new InvalidDataException();
        }

        public long ReadVarLong()
        {
            int loc3;
            var loc2 = new FinalInt64();
            var loc4 = 0;

            while (true)
            {
                loc3 = ReadByte();
                if (loc4 == 28)
                {
                    break;
                }

                if (loc3 >= 128)
                {
                    loc2.Low = loc2.Low | (uint)(loc3 & 127) << loc4;
                    loc4 = loc4 + 7;
                    continue;
                }

                loc2.Low = loc2.Low | (uint)loc3 << loc4;
                return loc2.ToNumber();
            }

            if (loc3 > 128)
            {
                loc3 = loc3 & 127;
                loc2.Low = loc2.Low | (uint)loc3 << loc4;
                loc2.High = (uint)(loc3 >> 4);
                loc4 = 3;
                while (true)
                {
                    loc3 = ReadByte();
                    if (loc4 < 32)
                    {
                        if (loc3 >= 128)
                        {
                            loc2.High = loc2.High | (uint)(loc3 & 127) << loc4;
                        }
                        else
                        {
                            break;
                        }
                    }

                    loc4 = loc4 + 7;
                }

                loc2.High = loc2.High | (uint)loc3 << loc4;
                return loc2.ToNumber();
            }

            loc2.Low = loc2.Low | (uint)loc3 << loc4;
            loc2.High = (uint)(loc3 >> 4);
            return loc2.ToNumber();
        }

        protected override void FillBuffer(int count)
        {
            var read = 0;
            do
            {
                var n = BaseStream.Read(_buffer, read, count - read);
                if (n == 0)
                    throw new EndOfStreamException();
                read += n;
            } while (read < count);
        }
    }

    public sealed unsafe class BigEndianWriter : BinaryWriter
    {
        private readonly byte[] _buffer;

        public byte[] Data
        {
            get
            {
                var stream = (MemoryStream)BaseStream;
                return stream.ToArray();
            }
        }

        public BigEndianWriter() : base(new MemoryStream(), Encoding.UTF8)
        {
            _buffer = new byte[16];
        }

        public BigEndianWriter(byte[] data) : base(new MemoryStream(data), Encoding.UTF8)
        {
            _buffer = new byte[16];
        }

        public void WriteInt(int value)
        {
            fixed (byte* p = _buffer)
                BigEndian.WriteInt32(p, value);
            OutStream.Write(_buffer, 0, 4);
        }

        public void WriteByte(byte value)
        {
            Write(value);
        }

        public void WriteSByte(sbyte value)
        {
            Write(value);
        }

        public void WriteShort(int value)
        {
            fixed (byte* p = _buffer)
                BigEndian.WriteInt16(p, (short)value);
            OutStream.Write(_buffer, 0, 2);
        }

        public void WriteShort(short value)
        {
            fixed (byte* p = _buffer)
                BigEndian.WriteInt16(p, value);
            OutStream.Write(_buffer, 0, 2);
        }

        public void WriteUTF(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            WriteShort(bytes.Length);
            WriteBytes(bytes);
        }

        public void WriteBoolean(bool value)
        {
            Write(value);
        }

        public void WriteBytes(byte[] value)
        {
            Write(value);
        }

        public void WriteDouble(double value)
        {
            fixed (byte* p = _buffer)
                BigEndian.WriteInt64(p, Reinterpret.DoubleAsInt64(value));
            OutStream.Write(_buffer, 0, 8);
        }

        public void WriteFloat(float value)
        {
            throw new NotImplementedException();
        }

        public void WriteVarInt(int value)
        {
            using (var loc2 = new BigEndianWriter())
            {
                if (value >= 0 && value <= 127)
                {
                    loc2.WriteByte((byte)value);
                    WriteBytes(loc2.Data);
                    return;
                }

                var loc3 = value;
                var loc4 = new BigEndianWriter();

                while (loc3 != 0)
                {
                    loc4.WriteByte((byte)(loc3 & 127));

                    var reader = new BigEndianReader(loc4.Data);
                    var loc5 = reader.ReadByte();

                    loc4 = new BigEndianWriter(reader.Data);
                    loc3 = loc3 >> 7;

                    if (loc3 > 0)
                    {
                        loc5 = (byte)(loc5 | 128);
                    }

                    loc2.WriteByte(loc5);
                    reader.Dispose();
                }

                WriteBytes(loc2.Data);
                loc4.Dispose();
            }
        }

        public void WriteVarShort(short value)
        {
            using (var loc2 = new BigEndianWriter())
            {
                if (value >= 0 && value <= 127)
                {
                    loc2.WriteByte((byte)value);
                    WriteBytes(loc2.Data);
                    return;
                }

                var loc3 = value & 65535;
                var loc4 = new BigEndianWriter();

                while (loc3 != 0)
                {
                    loc4.WriteByte((byte)(loc3 & 127));

                    var reader = new BigEndianReader(loc4.Data);
                    int loc5 = reader.ReadByte();

                    loc4 = new BigEndianWriter(reader.Data);
                    loc3 = loc3 >> 7;

                    if (loc3 > 0)
                    {
                        loc5 = loc5 | 128;
                    }

                    loc2.WriteByte((byte)loc5);
                    reader.Dispose();
                }

                WriteBytes(loc2.Data);
                loc4.Dispose();
            }
        }

        public void WriteVarLong(long value)
        {
            var final = FinalInt64.FromNumber(value);

            if (final.High == 0)
            {
                WriteInt32(final.Low);
            }
            else
            {
                for (var I = 0; I < 4; I++)
                {
                    WriteByte((byte)((final.Low & 127) | 128));
                    final.Low = final.Low >> 7;
                }

                if ((final.High & 268435455 << 3) == 0)
                {
                    WriteByte((byte)(final.High << 4 | final.Low));
                }
                else
                {
                    WriteByte((byte)((final.High << 4 | final.Low) & 127 | 128));
                    WriteInt32(final.High >> 3);
                }
            }
        }

        public void WriteInt32(long value)
        {
            while (value >= 128)
            {
                WriteByte((byte)(value & 127 | 128));
                value = value >> 7;
            }

            WriteByte((byte)value);
        }
    }

    public abstract class NetworkMessage
    {
        public abstract short Protocol { get; }

        public abstract void Serialize(BigEndianWriter writer);

        public abstract void Deserialize(BigEndianReader reader);

        public byte[] Bytes
        {
            get
            {
                var writer = new BigEndianWriter();

                Serialize(writer);

                var data = writer.Data;

                writer = new BigEndianWriter();

                var length = ComputeTypeLen(data.Length);

                writer.WriteShort(Protocol << 2 | length);

                if (length == 1)
                {
                    writer.WriteByte((byte)data.Length);
                }
                else if (length == 2)
                {
                    writer.WriteShort((short)data.Length);
                }
                else if (length == 3)
                {
                    writer.WriteByte((byte)(data.Length >> 16 & 255));
                    writer.WriteShort((short)data.Length & 65535);
                }

                writer.WriteBytes(data);

                return writer.Data;
            }
        }

        private static byte ComputeTypeLen(int length)
        {
            return length > 65535 ? (byte)3 : length > 255 ? (byte)2 : length > 0 ? (byte)1 : (byte)0;
        }
    }

    public abstract class NetworkType
    {
        public abstract short Protocol { get; }

        public abstract void Serialize(BigEndianWriter writer);

        public abstract void Deserialize(BigEndianReader reader);
    }
}