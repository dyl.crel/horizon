namespace Horizon.Protocol.Enums
{
    public enum GameContextEnum
    {
        ROLE_PLAY = 1,
        FIGHT = 2
    }
}