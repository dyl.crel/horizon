﻿using System;

namespace Horizon.Helpers
{
    public static class Clock
    {
        public static int Integer => (int)DateTimeOffset.Now.ToUnixTimeSeconds();

        public static long Long => DateTimeOffset.Now.ToUnixTimeMilliseconds();

        public static short Zone => (short)DateTimeOffset.Now.Offset.TotalMinutes;
    }
}