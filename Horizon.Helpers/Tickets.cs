﻿using System;
using System.Linq;
using System.Text;

namespace Horizon.Helpers
{
    public class Tickets
    {
        public static string Random()
        {
            return Guid.NewGuid().ToString("N").ToUpper();
        }

        public static byte[] Encode(string ticket)
        {
            return Encoding.UTF8.GetBytes(ticket);
        }

        public static string Decode(string ticket)
        {
            return Encoding.ASCII.GetString(ticket.Split(',').Select(byte.Parse).ToArray());
        }
    }
}