﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Horizon.Helpers
{
    public static class Extensions
    {
        public static TKey Provide<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> e)
        {
            if (typeof(TKey) == typeof(short))
            {
                if (e.Count > 0)
                {
                    return (TKey)(object)((short)(object)e.Last() + 1);
                }

                return (TKey)(object)(short)1;
            }

            if (typeof(TKey) == typeof(int))
            {
                if (e.Count > 0)
                {
                    return (TKey)(object)((int)(object)e.Last() + 1);
                }

                return (TKey)(object)1;
            }

            throw new NotImplementedException();
        }

        public static string ToHash(this string e)
        {
            using (MD5 provider = MD5.Create())
            {
                var inputBytes = Encoding.ASCII.GetBytes(e);
                var hashBytes = provider.ComputeHash(inputBytes);

                StringBuilder builder = new StringBuilder();
                for (var i = 0; i < hashBytes.Length; i++)
                {
                    builder.Append(hashBytes[i].ToString("X2"));
                }
                return builder.ToString().ToLower();
            }
        }
    }
}